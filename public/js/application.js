/**
 * Application Module
 *   This is where all javascript methods for User Module.
 *
 * Developer's Notes:
 *    - Added User Login method
 *    - Added CreateUser method
 *    - Added ValidateEmailAddress method\
 *    - Added UpdateLoanSessionData
 */
var loader  = $('#loader');

var Application  = function(){

	/**
	 * Loan Application Number
	 * 
	 * @type {Number}
	 */
	this.loanAppNr = 0;


}

/**
 * Create Loan Application
 * 
 * @return {[type]}
 */
Application.prototype.createLoan =  function( isStdLoanApp ) {

	console.log('Creating Loan Application');
	var loaderMsg = 'Preparing Your Account...';

	console.log(isStdLoanApp);

	if( isStdLoanApp == false && isStdLoanApp != 'undefined' ){
		loaderMsg = "This may take up to 30 seconds. <br/>Interruption may result in an application error. <br/>Please wait";   
	}

	$.ajax({
		url  : baseUrl + 'createLoan',
		beforeSend: function(){
				loader.find('span')
					  .html(loaderMsg);

			}, 
		success: function( response ){		

			console.log(response);

			if( response.result == 'failed pull')
				CommonObj.showNotification(response.message, 2 );
			else if ( response.result == 'failed credit')
				window.location	= '/decline/credit';
			else if( response.result == 'failed income')
				window.location	= '/decline/income';
			else
				window.location = '/linkBankAccount';
			
			loader.fadeOut();
		}
	});
}	

Application.prototype.creditCheck =  function() {

	var formData = $( "#UserAccountForm" ).serialize();

	$.ajax({
			url  : baseUrl + 'postCreditCheck', 
			type : "POST", 
			postType: "JSON",
			data : formData, 
			beforeSend: function(){
				loader.find('span')
					  .html('Credit Checking...');		
			},
			success: function( response ){

				// console.log(response);
			 
				if( typeof response.result != 'undefined'){
					if( response.result == 'failed'){						
						CommonObj.showNotification(response.message, 2 );
						loader.fadeOut();
					} else if( response.result == 'failed pull'){						
						CommonObj.showNotification(response.message, 2 );
						loader.fadeOut();
                    } else if( response.result == 'failed time range') {
                        CommonObj.showNotification(response.message, 2 );
                        loader.fadeOut();
					}else{
						App.createLoan(); 
					}
				}
			}
		});

}	



var App = new Application();

$(document).ready(function($) {

	/**
	 * Credit Check Out Form 
	 * 
	 * @return
	 */
	$( "#CreditCheckForm" ).on('submit',function(event){

		event.preventDefault();	

		$.ajax({
			url  : baseUrl + 'postCreditCheck', 
			type : "POST", 
			postType: "JSON",
			data : $(this).serialize(), 
			beforeSend: function(){
				loader.fadeIn()
					  .find('span')
					  .html('Credit Checking...');		
			},
			success: function( response ){

				console.log(response);
			 
				if( typeof response.result != 'undefined'){
					if( response.result == 'failed'){
						loader.fadeOut();
						CommonObj.showNotification(response.message, 2 );
					} else if( response.result == 'failed pull'){
						loader.fadeOut();
						CommonObj.showNotification(response.message, 2 );
                    } else if( response.result == 'failed fraud check'){
						loader.fadeOut();
						window.location = baseUrl + 'disqualification/fraud';
                    } else if( response.result == 'failed time range') {
                        CommonObj.showNotification(response.message, 2 );
                        loader.fadeOut();
					}else{
						App.createLoan(); 
					}
				}
			}
		});
	});	//end of $.ajax

	/**
	 * Rent Own Change
	 * 
	 * @param
	 * @return {[type]}
	 */
	$('#rentOwn').on('change',function(){

		if( $(this).val()=='1' ){
			$('#rentAmount').attr('required','true');
		}else{
			$('#rentAmount').removeAttr('required');
		}
	});

	/**
	 * Rent Option
	 * 
	 * @param
	 * @return
	 */
	$('.rentOption').on('click',function(){

		var option = $(this).attr('id');

		if( option == 'yes' ){
			
			$('#rentAmount').attr('required','required');
			$('#rentAmount').removeAttr('disabled');

		} else {
			
			$('#rentAmount').removeAttr('required');
			$('#rentAmount').attr('disabled','true');
		}
	});

	$('[data-toggle="popover"]').popover();
 
	/**
	 * Format Birthdate
	 * 
	 * @param
	 * @return
	 */
	$('.birthdate').change(function(){

		//Calculate Age
		var age = CommonObj.calculateAge( 
						  $('#month').val()
						, $('#day').val()
						, $('#year').val() 
					);

		$('#age').val(age);
	});

	/**
	 * Validate Housing Situation
	 * 
	 * @param
	 * @return
	 */
	$('#housingSit').change(function(){
		
		var selectedVal = $("#housingSit option:selected").val();

		switch(selectedVal){
			
			case '1':
				$( "#rentAmount" ).addClass('required')
								  .prop( "disabled", false );

				$('#housingSitNotification').hide();
			  	$('#rentAmountDiv').fadeIn();
			  	$('#mortage').fadeOut().addClass('hide');
			  	$('#contactInfoDiv').fadeOut().addClass('hide');
			break;

			case '2':
				$('#mortage').fadeIn().removeClass('hide');
				$('#rentAmountDiv').hide();
				$('#contactInfoDiv').fadeOut().addClass('hide');
				$('#housingSitNotification').fadeIn().find('div.innerText').html('We validate mortgage and home ownership against credit bureau databases.');
			break;
			case '4':
				$('#rentAmount, #contactPhoneNr, #contactName').val('')
												.removeClass('required')
												.prop( "disabled", false );
				$('#housingSitNotification').fadeIn().find('div.innerText').html('If you live with family, friends or have a situation where you pay no rent, please provide a contact who can verify your situation.');
				$('#rentAmountDiv').hide();
				$('#mortage').fadeOut().addClass('hide');
				$('#contactInfoDiv').fadeIn().removeClass('hide');
			break;

			default:
				$('#rentAmount').val('')
								.removeClass('required')
								.prop( "disabled", true );
				$('#rentAmount, #contactPhoneNr, #contactName').val('')
												.addClass('required')
												.prop( "disabled", true ); 
				$('#housingSitNotification').fadeIn().find('div.innerText').html('We validate mortgage and home ownership against credit bureau databases.');
				$('#rentAmountDiv').hide();
				$('#mortage').fadeOut().addClass('hide');
				$('#contactInfoDiv').fadeOut().addClass('hide');
			break;
		
		}

	});

	/**
	 * modal actions
	 */

	$('#ra-modal button.btn-back').click(function(){
		
		$('#ra-modal').modal('hide');
		$('#rentAmount').focus();

	});

	$('#ra-modal button.btn-proceed').click(function(){
		
		$('#rentAmount').removeClass('confirmRentAmount');
		$('#ra-modal').modal('hide');		
		$('#CreditCheckForm').trigger('submit');
		// $('#UserAccountFormSubmit').focus();

	});

	/**
	 * RateRewards Tooltip
	 */
	$('.rwar').tooltip();
	
});