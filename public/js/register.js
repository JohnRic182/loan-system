/**
 * Registration Page Js
 */
$(document).ready(function(){

	// shoud check if the input contains only number adding class on it to includ in validation
	$('#UserAccountForm select.question').change(function(){ 
		// select the answer textbox element
		if( $(this).attr('name') == 'securityQuestion1' ){
			orgElement 		= $("#securityQuestionAnswer1");
			elementHidden 	= $("#securityQuestionAnswer1_");
		}else{
			orgElement 		= $("#securityQuestionAnswer2");
			elementHidden 	= $("#securityQuestionAnswer2_");
		}

		//add validation classes
		if( $(this).val() == 4 ){
			orgElement.addClass('hide').prop('disabled', true);
			elementHidden.removeClass('hide').removeAttr('disabled'); //addClass('answerFormat');
		}else{
			orgElement.removeClass('hide').removeAttr('disabled');
			elementHidden.addClass('hide').prop('disabled', true); //removeClass('answerFormat');	
		}

	});

	// check the user name after inputing the value
	$('#UserAccountForm input.usernameCheck, #resetUsername input.usernameCheck').typing({
	    start: function (event, $elem) {
	        $elem.removeClass('loadingTextboxgif').removeClass('validCheckInput').removeClass('inValidCheckInput');
	    },
	    stop: function (event, $elem) {

	       	var actionUrl 	= baseUrl + $elem.attr('actionUrl');
	    	var username 	= $elem.val();
	    	var $this 		= $elem;
	    	var regex 		= /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[~`!@#$%^&*()_+|=\-{}\[\]:;"'<,>.?\/\\]).{8,}$/;
	    	// var regex 		= /^[~`!@#$%^&*()_+|=\-{}\[\]:;"'<,>.?\/]$/;

	    	$elem.removeClass('validCheckInput').removeClass('inValidCheckInput').addClass('loadingTextboxgif');

	    	if(username != ''){ 
	            if(regex.test(username)){

		            $.post( actionUrl , { username: $this.val() }).done(function( data ) {

				          	if( data.result == 'failed'){

				          		$this.removeClass('loadingTextboxgif')
				          			 .removeClass('validCheckInput')
				          			 .addClass('inValidCheckInput');

				          		$('.duplicateUsername').val(username);	

				          	}else{

				          		$('.duplicateUsername').val('');
				          		$this.removeClass('loadingTextboxgif')
				          			 .removeClass('inValidCheckInput')
				          			 .addClass('validCheckInput');
				          	}

			          	$this.removeClass('loadingTextboxgif');

			        });

	            }else{

	            	$this.removeClass('loadingTextboxgif')
	            		 .removeClass('validCheckInput')
	            		 .addClass('inValidCheckInput');
	            } 
		    }else{     
	        	$elem.removeClass('loadingTextboxgif')
	        		 .removeClass('validCheckInput')
	        		 .addClass('inValidCheckInput');
	        }
	    },
	    delay: 400
	});

	/**
	 * Answer Phone Format
	 */
	$(".answerFormat").mask("999-999-9999");
 	
 	/**
 	 * Email Validation 
 	 */
 	$('.email').on('focusout', function(){ 
 		// User.validateEmailAddress( $(this).val(), this ); 
 	});

 	$('[data-toggle="tooltip"]').tooltip();   
 	
});