
var loader  = $('#loader');

//Initialize Common Object
var Common  = new Common();
 
$(document).ready(function(){

	/**
	 * Is RateReward Program Added
	 * 
	 * @return {Boolean}
	 */
	var isRateRewardProgramAdded = function(){
		return $('input[name=raterewards]:checked').size();
	}

	/**
	 * Idology Dev
	 * 
	 * @return
	 */
	$('.idologyDev').click(function(event){
		event.preventDefault();	
		console.log('idology');

		window.location = baseUrl + 'exclusionCheck/final';
	});

	/**
	 * Idology Init Production
	 *
	 * @return
	 */
 	$('.idology').click(function(event){

		event.preventDefault();	
		console.log('Running Idology Init Check ');

		var isClickedFlag = $('#is-click-flag').val();

		if( isClickedFlag == 1 ){
			window.location = baseUrl + 'exclusionCheck/final';
		}else{
			Common.showNotification('Please select a product to continue', 2);
		}
	});

	/**
	 * Idology Final Production
	 *
	 * @return
	 */
 	$('.idologyFinal').click(function(event){

		event.preventDefault();	
		console.log('Running Idology Final Check');
 
		Common.hideNotification();

		$.ajax({
			url  : '/idologyRun/final', 
			type : "GET", 
			beforeSend: function(){
				loader.fadeIn().find('span').html('Loading Questions...');		
			},
			success: function( response ){
				loader.fadeOut();
				console.log('idologyRunTest results');
				console.log(response);
				var idologyResponse = response;
				$('#employment').css('display','none');
				if( response.result == 'questionTriggered' ){

					$('#questions').css('display','block');
					console.log('questions idologyTest');
					var html = '<input type="hidden" name="idNumber" id="idNumber" value='+ response.idNumber[0] +' />';
					var ctr = 1;
					$.each( response.questions, function( key, value ) {
					  	//alert( key + ": " + value );
					  	//console.log(key + ": " + value);
					  	console.log(key + ": " + value);
					  	html += value[0][0] + '<br />';
					  	html += '<input type="hidden" name="question'+ ctr +'Type" value="' + value[1][0] + '" /><br />';
					  	$.each( value[2], function( key1, answers ) {
					  		//console.log('inside:' + key1 + ": " + answers);
					  		html += '<input type="radio" name="question'+ ctr +'Answer" id="question'+ ctr +'Answer" value="' + answers + '" /> '+ answers +'<br />';
					  	});
					  	html += '<br />';
					  	ctr++;
					});
					ctr = ctr - 1;
					html += '<input type="hidden" id="questionLength" value="' + ctr-- + '" />';
					$('#questions_contents').html(html);
					setTimeout(function(){
						console.log('fail ExpectID IQ Time Out');
                       
                       // save fail to Idology_Result
                       $.ajax({
							url  : '/verification/idologyResult/abandoned', 
							type : "POST", 
							data : idologyResponse, 
							postType: "JSON",
							success: function( response ){
								console.log('Abandoned idology');
							}
						});
                        window.location = baseUrl + 'disqualification/identity';
						
					}, 240000);
				}else if( response.result == 'Failed' ){
					console.log('failed idology Final Check');
					
					window.location = baseUrl + 'disqualification/identity';
				}
			}
		});
	});

	/**
	 * Idology Final Close
	 *
	 */
 	$('.idologyFinalClose').click(function(event){
 		console.log('close Final Idology KBA, proceed to disqualification');
 		// save fail to Idology_Result
 		var idNumber = $('#idNumber').val();
		$.ajax({
			url  : '/verification/closeIdology/'+idNumber, 
			type : "get", 
			success: function( response ){
			}
		});
		window.location = baseUrl + 'disqualification/identity';
	});

	/**
	 * Idoloy Abandon request filter
	 */
	$('#modalQuestions').on('hidden.bs.modal', function () {
	    console.log('fail abandoned');
		window.location = baseUrl + 'disqualification/identity';
	});

	/**
	 * Idology Submit Answers
	 *
	 * @return {[type]}
	 */
 	$('#idologySubmitAnswers').on('submit', function(event){
 		
		console.log('answers');
		event.preventDefault();	
		console.log($(this).serialize());
		
		var answerFields = $(this).serialize();
		console.log(answerFields.indexOf("Answer"));

		var answerCount = (answerFields.match(/Answer/g) || []).length;
		console.log('answerCount='+answerCount);

		var totalCount = $("#questionLength").val();
		console.log('totalCount='+answerCount);
		
		if(answerFields.indexOf("Answer") > 0){
			if(answerCount < totalCount){
				console.log('Incomplete answers');
				$('#notification').html('<p> <strong>Fix the following error(s):</strong> </p><div class="alert alert-danger">Please answer all questions</div>');
				$('html, body').animate({
			        scrollTop: $("#notification").offset().top
			    }, 500);
				event.stopImmediatePropagation();
		        event.preventDefault();		        
		        return false;
			}else{
				$.ajax({
					url  : '/idologySubmitAnswers',
					type : "POST", 
					data : $(this).serialize(), 
					postType: "JSON",
					beforeSend: function(){
							loader.fadeIn().find('span').html('Fraud Checking...');		
					},
					success: function( response ){
						loader.fadeOut();
						console.log(response);
						if( response.result == 'Fail'){
							console.log('fail idologySubmitAnswers disqualification DENIED');
							console.log(response.console);

							// NLS task Denied
							$.ajax({
								url: baseUrl + 'verification/postIdentityVerification/DENIED' ,
								type : "POST", 	
								data : answerFields, 
								postType: "JSON",
								beforeSend: function(){
									loader.fadeIn().find('span').html('Submitting Information...');
								},
								success: function( data ) {

									setTimeout(function(){
										console.log('disqualification/KBA');
										window.location = baseUrl + 'disqualification/KBA';
									},5000);

									console.log('Result');
									console.log(data); 

								}
							});

						}else{
							console.log('pass idologySubmitAnswers, call postIdentityVerification APPROVED');
							console.log(response);

							// redirect to verification steps
							// NLS task Approved
							$.ajax({
								url: baseUrl + 'verification/postIdentityVerification/APPROVED' ,
								type : "POST", 
								data : answerFields, 
								postType: "JSON",
								beforeSend: function(){
									loader.fadeIn().find('span').html('Submitting Information...');
								},
								success: function( data ) {

									setTimeout(function(){
										console.log('verification/steps');
										window.location = baseUrl + 'verification/steps';
									},5000);

									console.log('Result');
									console.log(data); 

								}
							});
						}
					}
				});
			}			
		}else{
			console.log('no answer');
			$('#notification').html('<p> <strong>Fix the following error(s):</strong> </p><div class="alert alert-danger">Please answer all questions</div>');
			$('html, body').animate({
		        scrollTop: $("#notification").offset().top
		    }, 500);
			event.stopImmediatePropagation();
	        event.preventDefault();	        
	        return false;
		}
	});

	
 	/**
 	 * Idology Show Modal 
 	 * 
 	 */
	$('.idologyShowModal').on('click',function(){		
		$("#myModal").modal('show');
		event.preventDefault();	
	});

	/**
	 * Idology Test
	 * 
	 * @return
	 */
	$('#idologyTest').on('click',function(event){
		event.preventDefault();	
		console.log('idologyTest');


		var test 	  = $('.test').val();
 		var firstName = $.trim($('.firstName').val());
        var lastName  = $.trim($('.lastName').val());
        var address   = $.trim($('.address').val());
        var city      = $.trim($('.city').val());
        var state     = $.trim($('.state').val());
        var zip       = $.trim($('.zip').val());
        var ssn       = $.trim($('.ssn').val());
        
		$.ajax({
			url  : '/idologyRunTest', 
			type : "POST", 
			data : {
					firstName: firstName,
					lastName: lastName,
					address: address,
					city: city,
					state: state,
					zip: zip,
					ssn: ssn,
					test: test
					},
			postType: "JSON",
			beforeSend: function(){
					loader.fadeIn().find('span').html('Fraud Checking...');		
			},
			success: function( response ){
				loader.fadeOut();
				console.log('idologyRunTest results');
				console.log(response);
					if( typeof response.result != 'undefined'){
						if( response.result == 'Failed'){
							console.log('fail idologyTest');
							//window.location = baseUrl + 'disqualification/fraud';
						}else if( response.result == 'questionTriggered' ){
							console.log('questions idologyTest');
							var html = '<input type="hidden" name="idNumber" id="idNumber" value='+ response.idNumber[0] +' />';
							var ctr = 1;
							$.each( response.questions, function( key, value ) {
							  	//alert( key + ": " + value );
							  	//console.log(key + ": " + value);
							  	//if(ctr < 5){
							  		console.log(key + ": " + value);
								  	html += value[0][0] + '<br />';
								  	html += '<input type="hidden" name="question'+ ctr +'Type" value="' + value[1][0] + '" /><br />';
								  	$.each( value[2], function( key1, answers ) {
								  		//console.log('inside:' + key1 + ": " + answers);
								  		html += '<input type="radio" name="question'+ ctr +'Answer" id="question'+ ctr +'Answer" value="' + answers + '" /> '+ answers +'<br />';
								  	});
								  	html += '<br />';
							  	//}							  	
							  	ctr++;
							});
							$('.questions').html(html);
							$("#modalQuestions").modal('show');

						}else{
							console.log('pass idologyTest');
							//$('#results').html('Successfully Passed!');
							//window.location = baseUrl + 'exclusionCheck/final';
						}
					}
				$("#myModal").modal('hide');	
			}
		});
	});

});