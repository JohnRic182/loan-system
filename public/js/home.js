/**
 * Custom Js 
 *
 * @author Global Fusion <ascend@globalfusion.net>
 * @since  version 0.1
 *
 * @notes
 * 		- Estimator has been removed for version 1.1.14.16
 */
$( document ).ready(function() {
  	
	/**
	 * Slider Value
	 */
	var sliderVal;

	/**
	 * No UI Slider 
	 * 
	 * @type {Array}
	 */
	$("#slider-tooltip").noUiSlider({
		start: [DEFAULT_SLIDER_VAL],
		step : 100,
		connect: "lower",
		range: {
			'min': MINIMUM_LOAN_AMT, 
			'max': MAXIMUM_LOAN_AMT
		},
		format: wNumb({
			decimals: 0,
			thousand: ',',
			prefix: '$',
		})
	});
 
	/**
	 * Slider Slide Action
	 * 
	 * @param
	 * @return
	 */
	$("#slider-tooltip").on('slide', function(){ 

		sliderVal = $(this).val().replace('$','');
		sliderVal = sliderVal.replace(',', '');
 
 		$('#loanSliderInput').val(sliderVal); 
	});

	//slider Tooltip
	// $("#slider-tooltip").Link('lower').to('-inline-');

	// Tags after '-inline-' are inserted as HTML.
	// noUiSlider writes to the first element it finds.
	$("#slider-tooltip").Link('lower').to('-inline-', function ( value ) {

		// The tooltip HTML is 'this', so additional
		// markup can be inserted here.
		$(this).html(
			'<span class="pull-left glyphicon glyphicon-chevron-left"></span>' +
			'<span class="pull-left">' + value + '</span>' +
			'<span class="pull-left glyphicon glyphicon-chevron-right"></span>' 
		);
	});


	sliderVal = $("#slider-tooltip").val().replace('$','');
	sliderVal = sliderVal.replace(',', '');
 
	//Loan Slider Input
	$('#loanSliderInput').val( sliderVal );
  
	/**
	 * RateRewards Tooltip
	 */
	$('.rwar').tooltip();
});