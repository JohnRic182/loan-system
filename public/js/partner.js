/**
 * User/Applicant Module 
 *   This is where all javascript methods for partners Module.
 *
 * Developer's Notes:
 *    - 
 *    - 
 *    - 
 *    - 
 */

var loader  = $('#loader');

/**
 * LendingTree Modules here
 */
var LendingTree = function(){

}

/**
 * Register a Lending Tree user
 * 
 * @param
 * @return
 */
LendingTree.prototype.createUser = function( param ){

	$.ajax({
		url: baseUrl + 'postRegister',
		type: 'POST',
		dataType: 'JSON',
		data: param,
		beforeSend: function(){
			loader.fadeIn().find('span').html("This may take up to 30 seconds. <br/>Interruption may result in an application error. <br/>Please wait.");
		},
		success: function(response){
			console.log(response);
			
			if( typeof response.result != 'undefined'){
				if( response.result == 'failed'){
					CommonObj.showNotification(response.errorMessage, 2 );
					loader.fadeOut();
				}else{						
					LendingTree.prepareCreditCheck();					
				}
			}
		}
	});
}

/**
 * Update Loan Session Data
 * 
 * @param
 * @return
 */
LendingTree.prototype.updateLoanSessionData = function( param ){

	$.ajax({
		url: baseUrl + 'updateLoanSessionData',
		type: 'POST',
		dataType: 'JSON',
		data: param,
		success: function(response) {
			var formData = $("#lendingTreeLPForm").serialize();
			LendingTree.createUser(formData);
		}
	});
}

/**
 * Prepare Post Credit Check 
 * 
 * @return
 */
LendingTree.prototype.prepareCreditCheck = function( ){

	var formData = $("#lendingTreeLPForm").serialize();

	$.ajax({
		url: baseUrl + 'prepareCreditCheck',
		type: 'POST',
		dataType: 'JSON',
		data: formData,
		success: function(responseData) {
			console.log('prepare Credit Check');
			LendingTree.creditCheck(responseData);
		}
	});
}

/**
 * Check Credit Page
 * 
 * @param  object param
 * @return
 */
LendingTree.prototype.creditCheck = function( param ){
	console.log('Credit Checking');	
	$.ajax({
			url  : baseUrl + 'postCreditCheck', 
			type : "POST", 
			postType: "JSON",
			data : param, 
			beforeSend: function(){
				loader.find('span')
					  .html("This may take up to 30 seconds. <br/>Interruption may result in an application error. <br/>Please wait.");		
			},
			success: function( response ){

				// console.log(response);
			 
				if( typeof response.result != 'undefined'){
					if( response.result == 'failed'){
						loader.fadeOut();
						CommonObj.showNotification(response.message, 2 );
					} else if( response.result == 'failed pull'){
						loader.fadeOut();
						CommonObj.showNotification(response.message, 2 );
                    } else if( response.result == 'failed fraud check'){
						loader.fadeOut();
						window.location = baseUrl + 'disqualification/fraud';
                    } else if( response.result == 'failed time range') {
                        CommonObj.showNotification(response.message, 2 );
                        loader.fadeOut();
					}else{
						App.createLoan(false); 
					}
				}
			}
		});
}

// instantiate LendingTree
var LendingTree = new LendingTree();


//DOM Elements
$(document).ready(function(){

	$("#contactPhoneNr").mask("999-999-9999");

	//lendingTreeLP.blade.php form
	$("#lendingTreeLPForm").submit(function(event){	

		loader.find('span')
					  .html("This may take up to 30 seconds. Interruption may result <br/>in an application error. <br/>Please wait.");	

		var formData = $(this).serialize();

		var LoanParam = { 
			loanPurposeTxt: $('#loanPurposeTxt').val(), 
			loanPurposeId: 	$('#loanPurposeId').val()
		}

		LendingTree.updateLoanSessionData(LoanParam);		

		event.preventDefault();
	});

	//Change Housing situation
	$('#housingSit').change(function(){

			var val 	= $(this).val();
			var mrmObj 	= $("#monthlyRentMortage");
			var rentMortgageWrapper = $("#rent-mortgage-wrapper");

			console.log('housting situation shit');

			//remove disabled 
			mrmObj.prop( "display", 'block' );
			$('.mrmFormGroup').removeClass('hide');
			$('.houseSit-other').addClass('hide');

			//disable default
			$('#contactPhoneNr').prop( "disabled", true );
			$('#contactName').prop( "disabled", true );

			switch( val ){
				
				case "1":				
					mrmObj.attr({ 
						name : "rentAmount", 
						placeholder : "$ Monthly Rent"
					});
					mrmObj.prop( "disabled", false ).attr('alt', 'Monthly Rent');;
					$('.monthlyRentMortagelbl').html('Monthly Rent');	
					rentMortgageWrapper.css('display', 'block');		
				break;			

				case "2":				
					mrmObj.attr({ 
						name : "monthlyMortage", 
						placeholder : "$ Monthly Mortgage"
					});
					mrmObj.prop( "disabled", false ).attr('alt', 'Monthly Mortgage');
					$('.monthlyRentMortagelbl').html('Monthly Mortgage');
					rentMortgageWrapper.css('display', 'block');
				break;

				case "3":
					mrmObj.val('');
					rentMortgageWrapper.css('display', 'none');
					mrmObj.prop( "disabled", true );
				break;

				case "4":
					// $('.houseSit-other').removeClass('hide');
					// $('.mrmFormGroup').addClass('hide');
					// $('#contactPhoneNr').prop( "disabled", false ).val('');
					// $('#contactName').prop( "disabled", false ).val('');
					mrmObj.val('').prop( "disabled", true );
					rentMortgageWrapper.css('display', 'none');
				break;

			}
	});
	
	//Password Tooltip
	$('#password').tooltip({
		'trigger':'manual',
		'title': 'At least 8 characters, including one capital letter and one number, but no special characters.',
		'placement' : 'top'
	}).focus(function(){
		$(this).tooltip('show')
	}).blur(function(){ $(this).tooltip('hide'); });;

	//Annual Gross Income Tooltip
	$("#annualGrossIncome").tooltip({
			'trigger':'manual', 
			'title': 'Alimony, child support, or separate maintenance payments need not be revealed if you do not wish to have it considered as part of your income.', 
			'placement' : 'top'
	}).focus(function(){
		// var state = $('#state').val();
		// if(state == 'CA')
		$(this).tooltip('show')
	}).blur(function(){ $(this).tooltip('hide'); }); 
});