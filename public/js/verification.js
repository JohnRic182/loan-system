/**
 * Verification Js
 */

var $loader  	 = $('#loader');

var Verification =  function(){
	this.message  = {
		missingFile: 'Please upload an image of your identification document',
		missingIncomeFile : "Please upload 2 pay stubs or 1 tax filing form"
	}
}


/**
 * Redirect to Verification Initial Page
 * 
 * @return
 */
Verification.prototype.redirectToSteps = function(){

	setTimeout(function(){
		window.location = baseUrl + 'verification/steps';
	},5000);
}

/**
 * Employment Verification Form
 * 
 * @param  {object} param
 * @return
 */
Verification.prototype.postVerificationForm = function( param, method ) {

	var Vobj = this; 

	$.ajax({
		url: baseUrl + 'verification/' + method,
		type: 'POST',
		dataType: 'json',
		data: param,
		beforeSend: function(){
			$loader.fadeIn()
				  .find('span')
				  .html('Submitting Information...');
		},
		success: function( data ) {

			// console.log(data);

			$loader.fadeOut();

			if( typeof data.result != 'undefined' ) {
				if(data.result == 'success'){
					CommonObj.showNotification(data.message,1);
					Vobj.redirectToSteps();
				}else{
					CommonObj.showNotification(data.message,2);
				}
			} else {
				CommonObj.showNotification(data.message,2);
			}

			console.log('Result');
			console.log(data); 

		}
	});
}

var Vobj = new Verification();
 
var BankOption1 = 0; 
var BankOption2 = 0;

$(document).ready(function(){

	/**
	 * Select File Button
	 */
 	$('.btnSelectFile').click(function(event) {
 		// console.log('file button upload'); 
 		$('#fileupload').trigger('click');
 		return false;
 	});


 	$('.btnSelectFileIncome').click(function(event) {
 		// console.log('file button upload'); 
 		$('#fileupload').trigger('click');
 		return false;
 	});

 	$('.btnSelectOther').click(function(event) {
 		// console.log('file button upload'); 
 		$('#fileuploadOther').trigger('click');
 		return false;
 	});

 	$('#fileupload').change(function(event) {
 		$('.btnSelectFileTax').attr('disabled', 'disabled');
 	});

 	/**
 	 * Select Button for File Tax
 	 */
 	$('.btnSelectFileTax').click(function(event) {
 		// console.log('file button upload'); 
 		$('#fileuploadTax').trigger('click'); 
 		return false;
 	});

 	$('#fileuploadTax').change(function(event) {
 		$('.btnSelectFileIncome').attr('disabled', 'disabled');
 	});
 	//////////////////////////////////////////////////////////////////
 	//                          FUNDING                             //
	//////////////////////////////////////////////////////////////////

 	$('#btnUpload').click(function(event){

    	var isValidResult = ValidateObj.validateForm('FundingVerification'); 
		var formData 	  = $('#FundingVerification').serialize();
		// console.log(formData);
		try{ 
		    if( isValidResult == true && $('#CheckImageData').val() != "" ){ 
		    	CommonObj.hideNotification(); 
				Vobj.postVerificationForm(formData, 'postFundingVerification' ); 
		    }else{
		    	if( $('#CheckImageData').val() == "")
		    		CommonObj.showNotification( Vobj.message.missingFile, 2);  
		    	else
			    	$('#notification').html(isValidResult); 
			}	

		}catch(err){ 
			$('#notification').html(err); 
		}

		return false;
    });

	//////////////////////////////////////////////////////////////////
 	//                          EMPLOYMENT                          //
	//////////////////////////////////////////////////////////////////
 	$('#EmploymenVerificationForm').on('submit', function(){

 		var param = $(this).serialize();

 		Vobj.postVerificationForm(param, 'postEmploymentVerification');

 		return false;
 	});

 	/**
 	 * Employment Button
 	 */
 	$('.btn-employment').on('click', function(event){

 		if($(this).attr('data-value') == 'no'){
 			$('#employmentVerification .employed').slideUp(function(){
 				$('#employmentVerification .unemployed').slideDown();
 			});						
 			$('#SelfEmployed').val('no');
 		}else {
 			$('#employmentVerification .unemployed ').slideUp(function(){
 				$('#employmentVerification .employed').show().slideDown(); 
 			});				
 			$('#SelfEmployed').val('yes');
 		}

 		//remove class 
 		$('.btn-employment').addClass('btn-cta-inactive');

 		//add in active class
 		$(this).removeClass('btn-cta-inactive'); 
 	});


 	//////////////////////////////////////////////////////////////////
 	//                          IDENTITY DOCUMENT                   //
	//////////////////////////////////////////////////////////////////
 	$('.btnIdentityForm').click(function(event){

    	var isValidResult = ValidateObj.validateForm('IdentityVerification'); 
		var formData 	  = $('#IdentityVerification').serialize();

		console.log(formData);
 
		try{ 
		    if( isValidResult == true && $('#ImageData').val() != "" ){ 
		    	CommonObj.hideNotification(); 
				Vobj.postVerificationForm(formData, 'postIdentityDocumentVerification'); 
		    }else{
		    	if( $('#ImageData').val() == "")
		    		CommonObj.showNotification( Vobj.message.missingFile, 2);  
		    	else
			    	$('#notification').html(isValidResult); 
			}	

		}catch(err){ 
			$('#notification').html(err); 
		}

		return false;
   
    });

    //////////////////////////////////////////////////////////////////
 	//                          INCOME                              //
	//////////////////////////////////////////////////////////////////
 	$('.btnIncomeVerification').click(function(event){

    	var isValidResult = ValidateObj.validateForm('incomeVerification'); 
		var formData 	  = $('#incomeVerification').serialize();
		var paystubsCnt   = 0;
		var EmploymentCd  = $('#Employment_Type_Cd').val();

		try{ 

		    if( isValidResult == true && $('#CheckImageData').val() != "" ){ 

		   		//check for employment code, paystub requires 2 paystubs
		   		//Income Tax Filling require only one
		   		if( EmploymentCd == 'Employed' ) {
		    		//check if the user uploads two paystubs
		    		paystubsCnt =  $.parseJSON( $('#CheckImageData').val() ) ;

		    		if( paystubsCnt.length >= 2 ) {
			    		CommonObj.hideNotification(); 
						Vobj.postVerificationForm(formData, 'postIncomeVerification'); 
			    	} else {
			    		CommonObj.showNotification( Vobj.message.missingIncomeFile, 2);
			    	} 
		    	} else{
		    		CommonObj.hideNotification(); 
					Vobj.postVerificationForm(formData, 'postIncomeVerification'); 
		    	}
		    }else{
		    	if( $('#CheckImageData').val() == "")
		    		CommonObj.showNotification( Vobj.message.missingIncomeFile, 2);  
		    	else
			    	$('#notification').html(isValidResult); 
			}	

		}catch(err){ 
			$('#notification').html(err); 
		}

		return false;
   
    });

    //////////////////////////////////////////////////////////////////
 	//                          PAYMENT                             //
	//////////////////////////////////////////////////////////////////
	
	$('.paymentVerification input[type="radio"]').click( function(){

		$('.paymentVerification .form-group').removeClass('active');

		$(this).parents('.form-group').addClass('active');
	})
	
	/**
	 * Payment Verification Submit
	 * 
	 * @param
	 * @return
	 */
 	$('.btnPaymentVerification').click(function(event){

    	var isValidResult = ValidateObj.validateForm('PaymentVerification'); 
		var formData 	  = $('#PaymentVerification').serialize();

		console.log(formData);
 
		try{ 
		    if( isValidResult == true ){ 
		    	CommonObj.hideNotification(); 
				Vobj.postVerificationForm(formData, 'postPaymentVerification'); 
		    }else{ 
			    $('#notification').html(isValidResult); 
			}	

		}catch(err){ 
			$('#notification').html(err); 
		}

		return false;
   
    });

    //////////////////////////////////////////////////////////////////
 	//                          BANK                                //
	//////////////////////////////////////////////////////////////////
 	$('.btnBankVerification').click(function(event){

    	var isValidResult = ValidateObj.validateForm('bankVerification'); 
		var formData 	  = $('#bankVerification').serialize();
		var paystubsCnt   = 0;
		//var EmploymentCd  = $('#Employment_Type_Cd').val();

		try{ 

		    if( isValidResult == true && $('#CheckImageData').val() != "" ){ 

		    	//check if the user uploads two paystubs
		    	paystubsCnt =  $.parseJSON( $('#CheckImageData').val() ) ;
		    	if( paystubsCnt.length >= 3 ) {
		    		CommonObj.hideNotification(); 
					Vobj.postVerificationForm(formData + '&Bank_Type=' + BankOption1, 'postBankVerification'); 
		    	} else {
		    		CommonObj.showNotification( Vobj.message.missingIncomeFile, 2);
		    	} 

		    }else{
		    	if( $('#CheckImageData').val() == "")
		    		CommonObj.showNotification( Vobj.message.missingIncomeFile, 2);  
		    	else
			    	$('#notification').html(isValidResult); 
			}	

		}catch(err){ 
			$('#notification').html(err); 
		}

		return false;
   
    });

	// Bank Verification
	$('.btnLinkBankAccount').click(function(){
    	$('#bank_statements').css('display','none');
    	$('#bank_fastLink').css('display','block');
    	$('.bankOptions').css('display','none');
    	BankOption1 = 1;
    });

    $('.btnUploadStatements').click(function(){
    	$('#bank_fastLink').css('display','none');
    	$('#bank_statements').css('display','block');
    	$('.bankOptions').css('display','none');
    	BankOption1 = 2;
    });




});