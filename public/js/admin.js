/*
|--------------------------------------------------------------------------
| admin Module
|--------------------------------------------------------------------------
|
| This is where admin Module javascript functions
| name is needed to generate the composer.json file for your package.
| You may specify it now so it is used for all of your workbenches.
|
*/

var loader  = $('#loader');

var Funding = function(){ }
var Report = function(){ }

Funding.prototype.update = function( param ) {

	$.ajax({
		url: baseUrl + 'admin/updateFunding',
		type: 'POST',
		dataType: 'JSON',
		data: param,
		beforeSend: function(){
			loader.fadeIn().find('span').html('Updating Records...');
		},
		success: function(response){

		// console.log(response);

		loader.fadeIn().find('span').html('Reloading Records...');

		if(response.result == 'success')
			window.location = baseUrl + 'admin/funding';

		}
	});

	return false;
}


Report.prototype.updateReportData = function(param, url){

	$.ajax({
		url: url,
		type: 'GET',
		data: param,
		beforeSend: function(){
			loader.fadeIn().find('span').html('Updating Records...');
		},
		success: function(response){
				$('#exclusion-report-fieldset .results-tab').html(response);			
				loader.fadeOut();			
		}
	});

	return false;

}

var Funding = new Funding();
var Report = new Report();

$(document).ready(function(){

    $('.partnerStat').click(function() {
    	alert( $(this).val() );
    });

    

    $('#reportFunding input[type="checkbox"]').click(function(){

    	GTM_loan_id_funding = "";

    	$('#reportFunding input[type="checkbox"]').each(function(){

    		if( $(this).is(':checked') )
    			GTM_loan_id_funding += $(this).val() + ',';

    	});


    });

	$('#reportFunding').on('submit', function(){

		var atLeastOneIsChecked = $('#reportFunding :checkbox:checked').length > 0;
	
		if(atLeastOneIsChecked){
			data = $(this).serialize();
			Funding.update(data);
		}

		return false;
	});

	$('#exclusionFilterBtn').click(function(){		

		var param = {startDate : $('#datetimepickerStart input').val(), endDate : $('#datetimepickerEnd input').val()}
		var reportType = $('#reportType').val();

		switch(reportType){

			case 'Report':
				url = baseUrl + 'admin/loadReport'
				Report.updateReportData(param, url);
				exportUrl = baseUrl + 'admin/exportReport/' + param.startDate + '/' + param.endDate;
			break;

			case 'Exclusion_Report':
				url = baseUrl + 'admin/loadExclusion';
				Report.updateReportData(param, url);
				exportUrl = baseUrl + 'admin/exportExclusionReport/' + param.startDate + '/' + param.endDate;				
			break;

			case 'Marketing_Funnel_Report':
				url = baseUrl + 'admin/loadMktgFunnel';
				Report.updateReportData(param, url);
				exportUrl = baseUrl + 'admin/exportMarketingFunnelReport/' + param.startDate + '/' + param.endDate;
			break;
			
			case 'Verification_Activity_Report':
				url = baseUrl + 'admin/loadVerificationActivity'
				Report.updateReportData(param, url);
				exportUrl = baseUrl + 'admin/exportVerificationActivity/' + param.startDate + '/' + param.endDate;
			break;

			case 'SPV_Loan_Report':
				url = baseUrl + 'admin/loadSpvLoan';
				Report.updateReportData(param, url);
				exportUrl = baseUrl + 'admin/exportLoanReport/' + param.startDate + '/' + param.endDate;
			break;

			case 'SPV_Loan_Transfer_Report':
				url = baseUrl + 'admin/loadSpvLoanTransfer';
				Report.updateReportData(param, url);
				exportUrl = baseUrl + 'admin/exportLoanTransferReport/' + param.startDate + '/' + param.endDate;
			break;

			case 'Funded_Loan_Report':
				url = baseUrl + 'admin/loadFundedLoanReport';
				Report.updateReportData(param, url);
				exportUrl = baseUrl + 'admin/exportFundedLoanReport/' + param.startDate + '/' + param.endDate;
			break;

		}

		$('#exclusionExportBtn').attr('href', exportUrl);

	});

	$('.partnerStat').change(function(){
		var pid = $(this).attr('rel');
		var val = this.checked ? 1 : 0;

		$.ajax({
			url: baseUrl + 'admin/savePartnerStatus',
			type: 'POST',
			data: { Partner_Id : pid, Actv_Flag : val  },
			beforeSend: function(){
				loader.fadeIn().find('span').html('Updating Records...');
			},
			success: function(response){
				console.log(response);
					loader.fadeOut();			
			}
		});

	});

	//$('#search_borrower_input').on('mouseleave',function(){
	$('#search_borrower_input').on('keypress',function(e){
		if (e.which == 13) {
			var searchKey = $('#search_borrower_input').val();
			$.ajax({
				url: baseUrl + 'admin/borrowerList/' + searchKey.trim(),
				success: function(response){
					console.log(response);

					if(response.length > 0){
						$('#searchBorrowerBtn').removeAttr('disabled');

						if( $('#borrowerSelection').length ){
							$('#borrowerSelection').remove();
						}				

						var sel = $("<select></select>").attr("id", 'borrowerSelection').attr("name", 'borrowerListDD').attr('class','form-control').attr('required','required');

					    $.each( response, function( key, value ) {
						  	console.log(key + ": " + value);
						  	sel.append('<option data-prodid="' + value.Loan_Prod_Id.trim() + '" value="' + value.Borrower_User_Id + '-' + value.Loan_Id + '">' + value.First_Name + ' ' + value.Last_Name + '</option>');
						});

                        var prodid = sel.find('option:first-child').attr('data-prodid');
                        if (prodid == 1) $('#portalSelection option[value=raterewards]').hide();

						$('.borrowerSelectionPanel').append(sel);
						$('.portalSelectionPanel').css('display','block');
					}else{
						$('#borrowerSelection').remove();
					}
				}
			});
			return false; 
		}
	})

	$('#searchBorrowerBtn').click(function(){
		var searchSelect = $('#borrowerSelection').val();
		var pageSelect 	 = $('#portalSelection').val();
		var s = searchSelect.split('-');
		$.ajax({
			//url: baseUrl + 'raterewardLoan/rewards/' + s[1] + '/0/' + s[0],
			url: baseUrl + 'portal/admin/' + pageSelect + '/' + s[0] + '/' + s[1],
			beforeSend: function(){
				loader.fadeIn().find('span').html('Loading Rewards...');
			},
			success: function(response){
				loader.fadeOut();	
				$('#borrowerRewardContents').html(response);
				$('ul.navbar-nav>li>a').css('color','white');
			}
		});
	});

	$('#searchReopenBtn').click(function(){
		var searchSelect = $('#searchReopenInput').val();
		$.ajax({
			url: baseUrl + 'admin/getLoanDetails/' + searchSelect,
			beforeSend: function(){
				loader.fadeIn().find('span').html('Loading Loan Details...');
			},
			success: function(response){
				loader.fadeOut();	
				console.log(response);
				$('.reopenContentsDiv').css('display','block');
				$.each( response, function( key, value ) {
				    console.log( key + ": " + value.Borrower_User_Id );
				    $('.reopenContentsDiv').css('display','block');
					$('#resultUserId').html(value.Borrower_User_Id);
					$('#resultLoanId').html(value.Loan_Id);
					$('#resultFirstName').html(value.First_Name);
					$('#resultLastName').html(value.Last_Name);
					$('#resultApplicationDate').html(value.Create_Dt);
					$('#resultCurrentBalance').html(value.Current_Interest_Bal_Amt);	
					$('#resultLoanStatus').html(value.Loan_Status_Desc);	
					$('.reopenButton').attr('title',value.Borrower_User_Id+'-'+value.Loan_Id);
				});
			}
		});
	});

	$('.reopenButton').click(function(){
		$('#reopen-modal').modal('show');
	});

	$('#reopenClose').click(function(){
		$('#reopen-modal').modal('hide');
	});

	$('#reopenStart').click(function(){
		$('#reopen-modal').modal('hide');
		var ids = $('.reopenButton').attr('title');
		var s = ids.split('-');
		console.log('uid'+s[0]);
		console.log('lid'+s[1]);
		$.ajax({
			url: baseUrl + 'admin/doReopenProcess',
			type: 'POST',
			dataType: 'JSON',
			data: {'uid': s[0], 'lid':s[1] },
			beforeSend: function(){
				loader.fadeIn().find('span').html('Reopenning Loan..');
			},
			success: function(response){
				loader.fadeOut();	
				console.log(response);
				alert(response.result);
				$('#resultLoanStatus').html(response.status);	
				$('.reopenButton').html('DONE');
				$('.reopenButton').attr('disabled','disabled');
			}
		});

	});

    $(document).on('change', '#borrowerSelection', function(e) {
        e.preventDefault();

        var prodid = $(this).find(':selected').attr('data-prodid');
        if (prodid == 2) $('#portalSelection option[value=raterewards]').show();
        else $('#portalSelection option[value=raterewards]').hide();
    });


});

