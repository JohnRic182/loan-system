/**
 * Custom Js 
 *
 * @author Global Fusion <ascend@globalfusion.net>
 * @since  version 0.1
 */
//$(function(){
$( document ).ready(function() {

	var FLAG = 0;
	$( "#resetPassword" ).on('submit',function(event){

		//validate password and confirm password
		// check empty
		if($("#password").val()==''){
			$('.error-password').addClass('show');
			$('.error-password').html('Password is Required.');
			FLAG = 1;
		// check length	
		}else if(($("#password").val()).length < 8 || ($("#password").val()).length > 20){
			$('.error-password').addClass('show');
			$('.error-password').html('Password should be at least 8 characters and 20 characters long.');
			FLAG = 1;	
		// check for special characters	
		}else if(/^[a-zA-Z0-9- ]*$/.test($("#password").val()) == false) {
		    $('.error-password').addClass('show');
			$('.error-password').html('Password may only contain letter , numbers , upper -lower case and combination.');
			FLAG = 1;	
		}else{
			$('.error-password').removeClass('show');
			FLAG = 0;
		}

		// check empty
		if($("#confirmPassword").val()==''){
			$('.error-confirmPassword').addClass('show');
			$('.error-confirmPassword').html('Password is Required.');
			FLAG = 1;
		// check if same password
		}else if($("#confirmPassword").val() != $("#password").val()){
			$('.error-confirmPassword').addClass('show');
			$('.error-confirmPassword').html('These Password dont match .Please Try again?.');
			FLAG = 1;
		// check length
		}else if(($("#confirmPassword").val()).length < 8 || ($("#confirmPassword").val()).length > 20){
			$('.error-confirmPassword').addClass('show');
			$('.error-confirmPassword').html('Confirm Password should be at least 8 characters and 20 characters long.');
			FLAG = 1;
		// check for special characters	
		}else if(/^[a-zA-Z0-9- ]*$/.test($("#confirmPassword").val()) == false) {
		    $('.error-confirmPassword').addClass('show');
			$('.error-confirmPassword').html('Confirm Password may only contain letter , numbers , upper -lower case and combination.');
			FLAG = 1;			
		}else{	
			FLAG = 0;	
			$('.error-confirmPassword').removeClass('show');
		}


		if(FLAG){
			FLAG = 1;
			event.preventDefault();
		}else{
			//event.preventDefault();
			$('.rentError').removeClass('show');
			FLAG = 0;
		}
	});

 
});