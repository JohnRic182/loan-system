$( document ).ready(function() {
 
	var FLAG = 0;

	var loader  = $('#loader');
 
	$('#run').click(function(){
		
		loader.fadeIn().find('span').html('Exclusion Checking...');
		var uid = $('.uid').val();

		var EMP_STAT = $('.EMP_STAT').val();
		var AI = $('.AI').val();
		var EXST_LN = $('.EXST_LN').val();
		var LN_DECL = $('.LN_DECL').val();
		var TYPE = $('.type').val();

		var G094 = $('.G094').val();
		var G093 = $('.G093').val();
		var G083 = $('.G083').val();
		var G064 = $('.G064').val();
		var G071 = $('.G071').val();
		var G057 = $('.G057').val();
		var BC31 = $('.BC31').val();
		var BR33 = $('.BR33').val();
		var RE33 = $('.RE33').val();
		var MT33 = $('.MT33').val();
		var AT20 = $('.AT20').val();
		var AT11 = $('.AT11').val();
		var BC06 = $('.BC06').val();
		var G098 = $('.G098').val();
                
        var AT01 = $('.AT01').val();
        var AT12 = $('.AT12').val();
        var AT34 = $('.AT34').val();
        var BC09 = $('.BC09').val();
        var BC21 = $('.BC21').val();
        var BI01 = $('.BI01').val();
        var G046 = $('.G046').val();
        var G049 = $('.G049').val();
        var G082 = $('.G082').val();
        var G102 = $('.G102').val();
        var IN12 = $('.IN12').val();
        var IN34 = $('.IN34').val();
        var MT02 = $('.MT02').val();
        var MT20 = $('.MT20').val();
        var MT36 = $('.MT36').val();
        var OF01 = $('.OF01').val();
        var OF20 = $('.OF20').val();
        var OF28 = $('.OF28').val();
        var OF36 = $('.OF36').val();
        var PB21 = $('.PB21').val();
        var RE20 = $('.RE20').val();
        var S019 = $('.S019').val();
        var S043 = $('.S043').val();
        var S114 = $('.S114').val();

        var AT28 = $('.AT28').val();
        var G001 = $('.G001').val();
        var G002 = $('.G002').val();
        var G091 = $('.G091').val();
        var G096 = $('.G098').val();
        var S004 = $('.S004').val();
        var S059 = $('.S059').val();
        var S064 = $('.S064').val();
                
		var TOTAL_NSF = $('.Total_NSF_Cnt').val();
		var Days_since_NSF = $('.Days_Since_Last_NSF_Cnt').val();
		var MO_DEPOSIT_TO_AVERAGE = $('.Avg_Monthly_Deposit_Amt').val();
		var Monthly_Loan_Pay = $('.Monthly_Loan_Pay').val();
		var AVG_BAL_PMT_DATE = $('.Avg_Bal_On_Pmt_Dt_Amt').val();
		var CURR_BAL = $('.Current_Bal_Amt').val();
		var NUM_LOW_BAL_EVENTS = $('.Low_Bal_Event_Cnt').val();
		var NUM_LOW_BAL_DAYS = $('.Low_Bal_Day_Cnt').val();
		$.ajax({
			url  : '/updateExclusion/'+TYPE, 
			type : "POST", 
			data : {
				Employment_Status_Desc:EMP_STAT,
				Annual_Gross_Income_Amt:AI,
				EXST_LN:EXST_LN,
				LN_DECL:LN_DECL,
				G094 : G094,
				G093 : G093,
				G083 : G083,
				G064 : G064,
				G071 : G071,
				G057 : G057,
				BC31 : BC31,
				BR33 : BR33,
				RE33 : RE33,
				MT33 : MT33,
				AT20 : AT20,
				AT11 : AT11,
				BC06 : BC06,
				G098 : G098,

				AT12 : AT12,
				AT01 : AT01,
				AT34 : AT34,
				BC09 : BC09,
				BC21 : BC21,
				BI01 : BI01,
				G046 : G046,
				G049 : G049,
				G082 : G082,
				G102 : G102,
				IN12 : IN12,
				IN34 : IN34,
				MT02 : MT02,
				MT20 : MT20,
				MT36 : MT36,
				OF01 : OF01,
				OF20 : OF20,
				OF28 : OF28,
				OF36 : OF36,
				PB21 : PB21,
				RE20 : RE20,
				S019 : S019,
				S043 : S043,
				S114 : S114,

				Total_NSF_Cnt:TOTAL_NSF,
				Days_Since_Last_NSF_Cnt:Days_since_NSF,
				Avg_Monthly_Deposit_Amt:MO_DEPOSIT_TO_AVERAGE,
				Monthly_Loan_Pay: Monthly_Loan_Pay,
				Avg_Bal_On_Pmt_Dt_Amt:AVG_BAL_PMT_DATE,
				Current_Bal_Amt:CURR_BAL,
				Low_Bal_Event_Cnt:NUM_LOW_BAL_EVENTS,
				Low_Bal_Day_Cnt:NUM_LOW_BAL_DAYS,
                                
                                AT28 : AT28,
                                G001 : G001,
                                G002 : G002,
                                G091 : G091,
                                G096 : G098,
                                S004 : S004,
                                S059 : S059,
                                S064 : S064
			},
			postType: "JSON",
			success: function( response ){
				loader.fadeOut();
				$('#rightpanel').html( response );
			}
		});
	});

	$('#idologyTest').click(function(){
		event.preventDefault();	
		console.log('idologyTest');

		var test 	  = $('.test').val();
 		var firstName = $.trim($('.firstName').val());
        var lastName  = $.trim($('.lastName').val());
        var address   = $.trim($('.address').val());
        var city      = $.trim($('.city').val());
        var state     = $.trim($('.state').val());
        var zip       = $.trim($('.zip').val());
        var ssn       = $.trim($('.ssn').val());

		$.ajax({
			url  : '/idologyRunTest', 
			type : "POST", 
			data : {
					firstName: firstName,
					lastName: lastName,
					address: address,
					city: city,
					state: state,
					zip: zip,
					ssn: ssn,
					test: test
					},
			postType: "JSON",
			beforeSend: function(){
					loader.fadeIn().find('span').html('Fraud Checking...');		
			},
			success: function( response ){
				loader.fadeOut();
				console.log(response);
					if( typeof response.result != 'undefined'){
						if( response.result == 'Failed'){
							console.log('fail idologyTest');
							$('#results').html(response.note);
							//$('#errCodes').html('Error Message: '+response.message);
						}else if( response.result == 'questionTriggered' ){
							console.log('questions idologyTest');
							var html = '<input type="hidden" name="idNumber" id="idNumber" value='+ response.idNumber[0] +' />';
							var ctr = 1;
							$.each( response.questions, function( key, value ) {
							  	//alert( key + ": " + value );
							  	//console.log(key + ": " + value);
							  	console.log(key + ": " + value);
							  	html += value[0][0] + '<br />';
							  	html += '<input type="hidden" name="question'+ ctr +'Type" value="' + value[1][0] + '" /><br />';
							  	$.each( value[2], function( key1, answers ) {
							  		//console.log('inside:' + key1 + ": " + answers);
							  		html += '<input type="radio" name="answer'+ ctr +'" id="question'+ ctr +'Answer" value="' + answers + '" /> '+ answers +'<br />';
							  	});
							  	html += '<br />';
							  	ctr++;
							});
							$('.questions').html(html);
							$("#modalQuestions").modal('show');

						}else{
							console.log('pass idologyTest');
							$('#results').html('Successfully Passed!');
						}
					}
				$("#myModal").modal('hide');	
			}
		});
	});

	$('#idologySubmitAnswers').on('submit', function(){
		console.log('answers');
		event.preventDefault();	
		console.log($(this).serialize());
		$.ajax({
			url  : '/idologySubmitAnswers',
			type : "POST", 
			data : $(this).serialize(), 
			postType: "JSON",
			beforeSend: function(){
					loader.fadeIn().find('span').html('Fraud Checking...');		
			},
			success: function( response ){
				loader.fadeOut();
				console.log(response);
				if( response.result == 'Fail'){
					console.log('fail idologySubmitAnswers');
					console.log(response.console);
					window.location = baseUrl + 'verification/steps';
				}else{
					console.log('pass idologySubmitAnswers');
					console.log(response);
					window.location = baseUrl + 'verification/steps';
				}
			}
		});
	});
  
	$('.rn').click(function(){
		$('#otp-trigger').parent().removeClass('active');
		$('#cdb-trigger').parent().removeClass('active');
		$('#tccs-trigger').parent().removeClass('active');
		$('#sab-trigger').parent().removeClass('active');
		$('#at-trigger').parent().removeClass('active');

		$(this).parent().addClass('active');
		var tid = $(this).attr('id');
		var g = tid.split('-');
		$('.otp').css('display','none');
		
		$('.cdb').css('display','none');
		
		$('.tccs').css('display','none');
		
		$('.sab').css('display','none');
		
		$('.at').css('display','none');
		
		$('.'+g[0]).css('display','block');
		$()
	});


	//$(".tip-rewards").tooltip({});

	// $("#myModal").modal('show');
	

});