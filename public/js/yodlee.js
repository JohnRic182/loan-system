/**
 * Yodlee Javascript Functions
 *
 * @version  1.2
 * @author   Janzell Jurilla
 * 
 * Developer's Note:
 *    - initial updates (12-12-2014)
 *    - removed custom form and added fastlink integration
 *    - avoid using deprecated functions
 */

var loader  = $('#loader');
 
/**
 * Yodlee Class
 */
var Yodlee = function(){

	//Login Attempt
	this.loginAttempt          = 0;
	
	//Sites result
	this.sites                 = {}; 
	
	//Result Wrapper
	this.optionWrapper         = "sitesSearchResult"; 
	
	//Bank account Form
	this.verifyBankAccountForm = 'vefiryBankAccountForm';
	
	//Site login form
	this.addSiteAccountForm    = 'addSiteAccount1';
	
	//MFA Request Form
	this.MFARequestForm        = 'MFARequest';
	
	//Cob session token 
	this.cobSessionToken       = "";
	
	//User session token
	this.userSessionToken      = "";
	
	//Site id
	this.siteId                = '';
	
	//Site account id 
	this.siteAccountId         = ''; 
	
	//MFA request
	this.isMFARefresh          = false;
	
	//MFA Instance Type
	this.MFAObjectInstanceType = ['questionAndAnwer', 'token', 'image'];
	
	//Yodlee Title DOM Id
	this.yodleeTitleActionId   = 'yodlee-title';
	
	//Back Search DOM
	this.backSearchDOM         = $('.back-to-form');
	
	//Bank name string
	this.bankName              = "";
	
	//Check whether the call is from the RateReward Section	
	this.isRRSection           = 'N';
	
	this.isFastLink  = false;

	//Yodlee Notification
	this.notification = {
		bankConnnectSuccess : 'Your account has been successfully added', 
		bankConnnectFailed  : 'Connecting to Bank Failed',
		siteNotFound 		: 'Bank not found'
	};

	this.bankAccountResultCode = {
		fail    : 'BankAccountLinkFail',
		success : 'BankAccountLinkSuccess',
		skip    : 'BankAccountLinkSkip', 
		attempt : 'BankAccountLinkAttempt'

	}

	//Account Status ID
	this.accountStatusId =  { 
		9  : 'Account Refresh already in Progress',
		14 : 'Account has been added',
		13 : 'Adding of Account is in progress'
	}; 
};

/**
 * Create Yodlee Account
 * 
 * @return
 */
Yodlee.prototype.createYodleeAccount = function(){

	var Yobj = this;

	$.ajax({
		url:baseUrl + 'createYodleeAccount',
		type: 'GET',
		// dataType: 'JSON',
		beforeSend: function(){
			loader.fadeIn().find('span').html('Creating your Yodlee Account...');
		},
		success: function(response) {

			var BankAccounHistory;

			console.log(response);
			// loader.fadeOut();
			if( typeof response.result != 'undefined' ) {
				if (response.result == 'success') {
					
					BankAccounHistory = Yobj.checkBankAccountHistory(); 

					if(!BankAccounHistory)
						Yobj.generateFastLinkUrl();
					else{
						CommonObj.showNotification(response.message, 1);
						Yobj.redirectToExclusion();
					}					

				}else{
					CommonObj.showNotification(response.message, 1);
					//Redirect to Exclusion
					Yobj.redirectToExclusion();
				}
			} else {
				CommonObj.showNotification(response.message, 1);
				Yobj.redirectToExclusion();
			} 
		},
		error: function (xhr, ajaxOptions, thrownError) {
        	console.log(xhr.status);
        	console.log(thrownError);
      	}
	});
};


/**
 * checkBankAccountHistory just check if there is an existing bank accont History
 * @return {[type]} [description]
 */
Yodlee.prototype.checkBankAccountHistory = function(){

	console.log('checking bank account history');
	$.ajax({
			url:baseUrl + 'checkBankAccountHistory',
			type: 'GET',
			dataType: 'JSON',
			success: function(response) {

				if(response.resultCount != 0)
					return true;
				else
					return false;
			}
			 
		});

}

/**
 * Generate Fast Link URL
 * 
 * @return
 */
Yodlee.prototype.generateFastLinkUrl = function(){

	var Yobj = this;
 	console.log(Yobj);
	$.ajax({
		url:baseUrl + 'generateFastLinkUrl',
		type: 'GET',
		// dataType: 'JSON',
		beforeSend: function(){
			// loader.fadeIn().find('span').html('Creating your Yodlee Account...');
		},
		success: function(response) {
			console.log(response);
			console.log('testing generateFastLinkUrl');
			
			console.log(Yobj.isFastLink);

			if(Yobj.isFastLink == true )
		 		window.location = baseUrl + 'fastLink'; 
		 	else
		 		window.location = baseUrl + 'verifyBankAccount';
		},
		 error: function (xhr, ajaxOptions, thrownError) {
	        console.log(xhr.status);
	        console.log(thrownError);
	      }
	});
}

/**
 * Create Yodlee Account
 * 
 * @return
 */
Yodlee.prototype.createRRYodleeAccount = function(){

	var Yobj = this;

	$.ajax({
		url:baseUrl + 'createYodleeAccount',
		type: 'GET',
		beforeSend: function(){
		},
		success: function(response) { 
	 		console.log('creating yodlee user account');
			console.log(response);
		},
		 error: function (xhr, ajaxOptions, thrownError) {
	        console.log(xhr.status);
	        console.log(thrownError);
	      }
	});
};


/**
 * Yodlee Site Search
 *
 * @deprecated version 1.1
 * @param  object param
 * @return
 */
Yodlee.prototype.siteSearch = function( param ) {

	console.log('calling yodlee.prototype.siteSearch');

	var Yobj =  this;

	console.log(Yobj);

	//set bank name
	this.bankName =  param.bankName;

	if( param != ""){ 

		$.ajax({
			url  : baseUrl  + 'verifyBankAccount', 
			type : "POST", 
			postType: "JSON",
			data : param, 
			beforeSend: function(){
				loader.fadeIn().find('span').html('Searching banks...');
			}, 
			success: function( response ){

				console.log(response);

				loader.fadeOut();
					
				if( typeof response.sites != 'undefined' && response.sites.length > 0 )
					Yobj.populateSites(response);
				else
					CommonObj.showNotification(Yobj.notification.siteNotFound, 3);
			}
		});
		
	}
};

/**
 * Populate Sites
 * 
 * @deprecated version 1.1
 * @param  object response
 * @return
 */
Yodlee.prototype.populateSites = function(response){
				
	// console.log('calling yodlee.prototype.populateSites');
	// var response =  $.parseJSON(response);

	this.cobSessionToken  = response.cobSessionToken; 
	this.userSessionToken = response.userSessionToken;

	if(typeof response.sites != 'undefined'){
	
		var sitesCount = response.sites.length;

		var wrapper = '<div class="search-title clearfix">Search Results for ' + this.bankName + '</div>'; 
			wrapper += '<p class="clearfix">Please select one of the following:</p>';
			wrapper += '<ul>';
		
		$.each(response.sites, function(index, value ){
			if( index < 5)
				wrapper += '<li id="'+ value.siteId+'" data-site-image-url="'+ value.imageUrl +'"class="cta-link">' + value.bankName + '</li>';
			else
				wrapper += '<li style="display:none" class="more-banks cta-link" id="'+ value.siteId+'">' + value.bankName + '</li>';
		});

		wrapper += '</ul>';

		if( sitesCount > 5 )
			wrapper += '<div class="show-all cta-link">view more</div>';

		$('#bank-wrapper-search').hide();

		this.backSearchDOM.prop('data-url', 'bank-wrapper-search').removeClass('hide').show();

 		$('#' + this.optionWrapper).show();
		$('#' + this.optionWrapper + ' .sites').html(wrapper).fadeIn();

		//binde Sites option 
		this.bindSitesOption(); 
	}
};

/**
 * Bind Sites Option
 *
 * @deprecated version 1.1
 * @return
 */
Yodlee.prototype.bindSitesOption = function(){

	// console.log('calling yodlee.prototype.bindSitesOption');

	var Yobj = this; 

	$('#' + this.optionWrapper + ' .sites ul li').click( function(e){
		Yobj.getSiteLoginForm(this.id, $(this).text());
	});

	$('.show-all').click( function(){

		if( $(this).text() == 'view less'){
			$(this).text('view more');
			$('.more-banks').fadeOut();
		}else{
			$(this).text('view less');
			$('.more-banks').fadeIn();
		}
	})
};

/**
 * Get Site Login Form
 *
 * @deprecated version 1.1
 * @param  {int} siteId
 * @return
 */
Yodlee.prototype.getSiteLoginForm = function(siteId, bankName ) {

	console.log('calling yodlee.prototype.getSiteLoginForm');

	//hide all notifications
	CommonObj.hideNotification();

	//set current site id
	this.siteId = siteId;

	var Yobj = this; 

	if( siteId != "") {

		$.ajax({
			url  : baseUrl  + 'getSiteLoginForm/' + siteId  , 
			postType: "JSON", 
			beforeSend : function(){
				loader.fadeIn().find('span').html('Getting bank login form...');
			},
			success: function( response ){

				console.log(response);

				loader.fadeOut();

				Yobj.saveLoanAppPhase( Yobj.bankAccountResultCode.attempt );

				//show site not found
				if( typeof response.Body.errorOccurred != 'undefined' ){
					CommonObj.showNotification(response.Body.message, 3);
					return false;
				}

				//append login form 
				Yobj.appendLoginForm(response, bankName); 
				//bind authentication submit
				Yobj.bindAuthenticationForm(response);

				//hide the bank wrapper search
				$('#bank-wrapper-search, #sitesSearchResult').hide();
				Yobj.backSearchDOM.prop('data-url', 'bank-wrapper-search').removeClass('hide').show();

			}
		});
	}
};

/**
 * Append Login Form
 *
 * @deprecated version 1.1
 * @param  {obj} loginForm
 * @return
 */
Yodlee.prototype.appendLoginForm = function(loginForm, bankName){
	
	console.log('calling yodlee.prototype.appendLoginForm');

	if( loginForm.Body.componentList != 'undefined' ) {
		
		var componentList = loginForm.Body.componentList; 
		var fieldGroup    = '';
		var inputField    = ""; 
		var fType         = 'text';
  
		$.each(componentList, function(index, e){
			fieldGroup += '<div class="form-group">'; 				// inputField = e
			fieldGroup += '<label for="'+ e.name +'" class="col-sm-2 control-label">' + e.displayName + '</label>';
			inputField  = getInputField(e);

			fieldGroup += '<div class="col-sm-4">'+inputField+'</div>';
			fieldGroup += '</div>';
		});


		//display the site login form 
		$('#site-login-form').show();

		//enable the link bank account
		$('#linkBankAccount').removeProp('disabled').removeClass('btn-cta-inactive').addClass('btn-cta');

		//update the title name
		if( bankName == 'Bank of America'){
			$('#'+ this.yodleeTitleActionId).html('Connecting to ' + bankName );
		} else {
			bankName = bankName.replace(/Bank/g, ''); // this wikk remove a redundunt word bank
			$('#'+ this.yodleeTitleActionId).html('Connecting to ' + bankName + ' Bank');
		}

 		//append the login form 
		$('#'+ this.addSiteAccountForm + ' .fields').html(fieldGroup).fadeIn();
	}

	function getInputField( e ) {

		var inputField = "";
		var fieldType  = e.fieldType.typeName;
		var required;

		required = ( e.isOptional ) ?  '' : 'required' ;

		if( fieldType == 'IF_PASSWORD' ) {
			inputField = '<input ' + required + ' type="password" name="' + e.name + '" class="form-control">';
		}else if(fieldType == 'OPTIONS') {
			inputField = '<select name="' + e.name + '" class="form-control">';
			$.each(e.displayValidValues, function(i,elem){
				inputField += '<option value="'+e.validValues[i]+'">'+ elem + '</option>';
			});

			inputField += '</select>';
		} else {
			inputField = '<input '+ required +' type="text" name="' + e.name + '" class="form-control">';
		}

		return inputField;
	}
};

/**
 * Bind Authentication Form
 *
 * @deprecated version 1.1
 * @param  {obj} e
 * @return
 */
Yodlee.prototype.bindAuthenticationForm = function(e){

	console.log('calling yodlee.prototype.bindAuthenticationForm');

	var Yobj = this; 
	var form = $('#'+ this.addSiteAccountForm );

	form.submit(function(){

		console.log('bindAuthenticationForm');

		var body = JSON.stringify(e); 
		var param  = $(this).serialize(); 
		    param += '&siteId='+Yobj.siteId + '&body='+ body;

		$.ajax({
			url: baseUrl + 'addSiteAccount', 
			type: "POST",
			data: param, 
			dataType: "JSON", 
			beforeSend: function(){
				loader.fadeIn().find('span').html('Adding Site Account Information');
			},
			success:function(response){

				console.log(response);

				loader.fadeOut();
				Yobj.getMFAResponseForSite(response);
			},
			error: function() {
				console.log('adding site authentication failed');
			}
		});

		return false;
	}); 
};

/**
 * Redirect To Exclusion Page
 *
 * 
 * @return
 */
Yodlee.prototype.redirectToExclusion = function( isSuccess ){
	
	// console.log('Redirecting to Exclusion');

	var Yobj = this;

	if( this.isRRSection == true  ) {

		// console.log('closing link Bank Account modal');
		if( typeof isSuccess != 'undefined' && isSuccess == true ) {

			$.getJSON(baseUrl + 'getBankAcctSummaryView', function(data) {
				console.log(data);
				$('#sab-trigger-row').html(data.rewards).fadeIn();
				$('#bankSavings').html(data.summary).fadeIn();
			});
		}
 		
		$('#linkBankAcctModal .close').trigger('click');

	} else {
		setTimeout(function(){
			window.location = baseUrl + 'exclusionCheck/initial';
		},3000);
	}
};

/**
 * Validate Bank Acct Flag
 *
 * @deprecated version 1.1
 * @return
 */
Yodlee.prototype.validateBankAcct = function() {
	$.get(baseUrl + 'validateBankAcctFlag', function(data) {
		if( typeof data != 'undefined' ){
			console.log(data.Bank_Acct_Linked_Flag);
		}
	});
}


/**
 * Save User Transactions into Bank Transactions Tables
 * 
 * @param  {int} memSiteId
 * @return 
 */
Yodlee.prototype.saveUserTransactions = function( memSiteAccId, siteId){

	var Yobj = this; 

	if( memSiteAccId != "") {
		$.ajax({
			url: baseUrl  + 'saveYodleeData',
			type: 'POST',
			dataType: 'JSON',
			data : { 
				memSiteAccId : memSiteAccId , 
				siteId : siteId, 
				isRRSection : Yobj.isRRSection
			},
			beforeSend: function(){
				loader.fadeIn().find('span').html('Saving user transactions');
			},
			success: function(response){
				loader.fadeOut();
				Yobj.redirectToExclusion(true);		
				console.log(response);
				console.log("success");
			}
		})
		.fail(function(response) {

			loader.fadeOut();
			Yobj.redirectToExclusion();
			console.log(response);
			console.log("error");
		})
		.always(function(response) {
			console.log(response);
			console.log("complete");
		});
	}
};

/**
 * Save Loan App Phase
 * 
 * @param  string code
 */
Yodlee.prototype.saveLoanAppPhase = function( code ) {

	console.log('saving loan application phase');

	$.ajax({
		url: baseUrl + 'saveLoanAppPhase',
		type: 'POST',
		data: { subPhaseCode: code },
		success: function( response ) {
			console.log(response);
		}
	})
	.done(function() {
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
}

/**
 *  Get MFA Response For Site
 *
 * @deprecated  version 1.1
 * @param  {string} memSiteId
 * @todo   need to separate the Form Creation
 * @return
 */
Yodlee.prototype.getMFAResponseForSite = function(response ){

	var Yobj       = this;
	var isRedirect = true;
	var isSuccess  = false; 
	var notification, siteStatusId, siteRefreshStatus ;

	//Check Whether the Response is EMPTY
	if( typeof response === 'undefined' ) {

		//CommonObj.showNotification(this.notification.bankConnnectFailed, 2 );
		//check for login attempt
		if( this.loginAttempt == 2 ){
			Yobj.saveLoanAppPhase(Yobj.bankAccountResultCode.fail);
			Yobj.redirectToExclusion();
		}

		this.loginAttempt++; 
		return false;
	}

	//Check whether there's an ERROR occured
	if( typeof response.error !== 'undefined' ) {

		CommonObj.showNotification(response.error, 2);
		
		Yobj.saveLoanAppPhase( Yobj.bankAccountResultCode.fail );
		Yobj.redirectToExclusion();

		return false;
	}

	//Check for the Response Code
	if( typeof response.code !== 'undefined' && response.code != 0 ) {

		CommonObj.showNotification(response);
		//CommonObj.showNotification(this.notification.bankConnnectFailed, 2 );
		//check for login attempt
		if( this.loginAttempt == 2 ){
			Yobj.saveLoanAppPhase(Yobj.bankAccountResultCode.fail);
			Yobj.redirectToExclusion();
		}

		this.loginAttempt++; 
		return false;
	}
 
	//Check if Yodlee need Multifactor Authentication
	if( response.hasOwnProperty('isMessageAvailable') ){

		if( response.isMessageAvailable == true ) { 
  	 		//Check if there's an FieldInfo
			if( response.hasOwnProperty('fieldInfo') ) {
				
				fieldInfo = response.fieldInfo;	

				var fieldGroup      = ''; 
				var fieldInfoString = '';

				if(  fieldInfo.hasOwnProperty('mfaFieldInfoType') &&  fieldInfo.mfaFieldInfoType == 'TOKEN_ID') { 
					inputField  = '<input type="text" maxlenght="'+fieldInfo.maximumLength+'" name="token" id="token" class="form-control">';
					fieldGroup += '<div class="form-group">';
					fieldGroup += '<label for="'+ fieldInfo.displayString +'" class="col-sm-2 control-label">' + fieldInfo.displayString + '</label>';
					fieldGroup += '<div class="col-sm-4">'+inputField+'</div>';
					fieldGroup += '</div>';
				} else if( fieldInfo.hasOwnProperty('questionAndAnswerValues') ) {
					$.each(fieldInfo.questionAndAnswerValues , function(i, e) { 
						inputField  = Yobj.getMFAInputDOM(e);
						fieldGroup += '<div class="form-group">';
						fieldGroup += '<label for="'+ e.metaData +'" class="col-sm-2 control-label">' + e.question + '</label>';
						fieldGroup += '<div class="col-sm-4">'+inputField+'</div>';
						fieldGroup += '</div>';
					});
				}

				//Append Member Site Id and  Field Info Data
				fieldGroup += '<input type="hidden" name="memSiteAccId" value="'+response.memSiteAccId+'"/>';
				fieldGroup += '<div class="form-group">';  
				fieldGroup += '<input type="hidden" name="fieldInfo" value="' + escape(JSON.stringify(fieldInfo)) + '"/>';

				//hide search and site result wrapper
				$('.form-wrapper').hide();
				$('#MFA-form').fadeIn();
				$('#linkBankAccount').hide();
				$('#putMFARequestForSiteBtn').removeClass('hide');
				$('#'+ Yobj.MFARequestForm ).html(fieldGroup).fadeIn();

				Yobj.backSearchDOM.prop('data-url', 'bank-wrapper-search').removeClass('hide').show();
				Yobj.bindPutMFARequestForSite(response.memSiteAccId); 
			}

		} else {


			console.log(response);

			Yobj.saveLoanAppPhase(Yobj.bankAccountResultCode.fail);
			Yobj.redirectToExclusion();
		}
	
	//Normal Authentication
	} else {

		console.log('Normal Authentication');
		
		//Check for an Error Response
		if( typeof response.errorCode != 'undefined' && response.errorCode != 0 ) {
			Yobj.saveLoanAppPhase(Yobj.bankAccountResultCode.fail);
			Yobj.redirectToExclusion();
		}
 
		//Check for Successful Site Accounts 
		if( typeof response.siteAddStatus != 'undefined') {

			siteStatusId = response.siteAddStatus.siteAddStatusId;
			
			//Format Notification
			if( typeof this.accountStatusId[siteStatusId] != 'undefined')
				notification = this.accountStatusId[siteStatusId];
			else 
				notification = response.siteAddStatus.siteAddStatus;

			CommonObj.showNotification(notification, 1);

			isSuccess  = true; 
			isRedirect = false;
		}

		//Check If Exclusion Redirection is TRUE
		if( isRedirect === true )
			Yobj.redirectToExclusion();

		console.log(response);
		console.log('Redirection:' + isRedirect );

		if( typeof response.memSiteAccId != 'undefined' && isSuccess == true ){
			Yobj.saveLoanAppPhase(Yobj.bankAccountResultCode.success);
			Yobj.saveUserTransactions(response.memSiteAccId, Yobj.siteId );
		} else {
			Yobj.saveLoanAppPhase(Yobj.bankAccountResultCode.fail);
			Yobj.redirectToExclusion();
		}
	}
};


/**
 * Get MFA Input Fields 
 *
 * @deprecated version 1.1
 * @param  {obj} param
 * @return 
 */
Yodlee.prototype.getMFAInputDOM = function(param){

	console.log('calling yodlee.prototype.getMFAInputDOM');

	var inputField =  "";

	if( param.responseFieldType != "" ){
		if( param.responseFieldType == "text")
			inputField = '<input type="text" name="'+param.metaData+'" class="form-control">';
	}

	return inputField;
};

/**
 * Bind putMFARequestForSite
 *
 * @deprecated version 1.1
 * @param {integer} memSiteAccId - Member Site Account Id of User
 * @return obj
 */
Yodlee.prototype.bindPutMFARequestForSite = function(memSiteAccId) {
	
	console.log('calling yodlee.prototype.bindPutMFARequestForSite');

	var Yobj = this;

	$('#putMFARequestForSiteBtn').click(function(){

		var param = $('#' + Yobj.MFARequestForm ).serialize();
			param += '&cobSessionToken=' + Yobj.cobSessionToken + '&userSessionToken=' + Yobj.userSessionToken + '&memSiteAccId=' + memSiteAccId ;

		$.ajax({
			url:baseUrl + 'putMFARequestForSite',
			type: 'POST',
			dataType: 'JSON',
			data: param,
			beforeSend: function(){
				loader.fadeIn().find('span').html('Sending MFA Request....');
			}
		})
		.done(function(response) {

			loader.fadeOut();

			//check for primitive object response
			if( typeof response.Body.primitiveObj != 'undefined' ) {

				if( response.Body.primitiveObj == true ){
					CommonObj.showNotification(Yobj.notification.bankConnnectSuccess, 1);
					Yobj.saveUserTransactions(response.memSiteAccId, Yobj.siteId );
				}
				else{
					CommonObj.showNotification(Yobj.notification.bankConnnectFailed, 2);
					Yobj.redirectToExclusion();
				}

			} else {
				Yobj.redirectToExclusion();
			}

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		 
		return false;

	});
};

//Initialize Yodlee
var Yobj = new Yodlee();

$(document).ready(function(){

	/**
	 * Verify Bank Account
	 *
	 * @deprecated version 1.1
	 */
	$('#' + Yobj.verifyBankAccountForm ).submit(function(){

		console.log('calling yodlee.verifyBankAccountForm');

		var bankName = $('#' + Yobj.verifyBankAccountForm  + ' #bankName').val();
		var data     = { bankName: bankName }; 
	
		Yobj.siteSearch(data);

		return false;
	});
 	
 	/**
 	 * Defaults Banks Change
 	 *
 	 * @deprecated version 1.1
 	 */
 	$('#defaultBanks #banks').change( function(){

 		console.log('calling default banks');

 		var siteId 	 = $(this).val();
 		var bankName = $(this).text();

 		Yobj.getSiteLoginForm(siteId, bankName);

 	});

 	/**
 	 * Top 10 Banks
 	 *
 	 * @deprecated version 1.1
 	 */
 	$('.top-banks .banks').click( function(){

 		console.log('calling top banks');

		var bankId   = this.id.split('-');
		var siteId   = bankId[1]; 
		var bankName = $(this).attr('data-bank-name');

		$('.sitename').html(bankName);
			
 		Yobj.getSiteLoginForm(siteId, bankName);

 	});

 	/**
 	 * Back To Previous Form
 	 * 
 	 * @deprecated version 1
 	 */
 	$('.back-to-form').click( function() {

 		console.log('calling back to form action');

 		var formId =  $(this).prop('data-url');

 		if( formId != "#" || formId != "") {
 			
 			$('.form-wrapper').each(function(index, val) {
 				$(this).hide();
 			});

 			if( formId == 'bank-wrapper-search') {
 				$(this).prop('data-url', '#').hide();
 			}

 			//hide mfa response button
 			$('#putMFARequestForSiteBtn').hide();
 			$('#linkBankAccount').addClass('btn-cta-inactive').removeClass('btn-cta');

 			$('#' + formId ).show();
 		}
 
 	});

 	/**
 	 * Link Bank Account Trigger
 	 */
 	$('#linkBankAccount').click(function(){
 		$('#addSiteAccountBtn').trigger('click');
 	});

 	/**
 	 * Create Yodlee Bank Fastlink
 	 * 
 	 * @param  event
 	 * @return
 	 */
 	$('#createYodleeBankFastLink').click(function(event) {
 		Yobj.isFastLink = true;
 		Yobj.createYodleeAccount();
 		return false;
 	});

 	/**
 	 * Create New User Yodlee Account
 	 */
 	$('#createYodleeAccount').click(function(){

 		Yobj.createYodleeAccount();

 		return false;
 	});

 	/**
 	 * Close linkBankAccountReward Modal
 	 */
 	$('#linkBankAccountReward').click(function(){
  	
 		$.getJSON(baseUrl + 'getBankAcctSummaryView', function(data) {
			// console.log(data);
			$('#sab-trigger-row').html(data.rewards).fadeIn();
			$('#bankSavings').html(data.summary).fadeIn();
		});

		$('#linkBankAcctModal .close').trigger('click');
 	});

 	/**
 	 * Skip Linking of Bank Account
 	 * 
 	 * @param
 	 * @return
 	 */
 	$('#dontLinkBankAcct').click(function(event) {
 			
 		Yobj.saveLoanAppPhase( Yobj.bankAccountResultCode.skip );
 		Yobj.redirectToExclusion();
 		return false;
 	});



 	/**
 	 * Create Yodlee Account 
 	 * 
 	 * @module RateReward
 	 */
 	$('#createYodleeAcctRR').click(function(){

 		$('#linkBankAcctModal').modal(
	 		'show'
	 	);

 		console.log('create yodlee account');
 		Yobj.createRRYodleeAccount(); 
 		Yobj.isRRSection = 'Y';
 	});

 	/**
 	 * Skip Linking of Bank Account
 	 * 
 	 * @param
 	 * @return
 	 */
 	$('#rrDontLinkBankAcct, .rr-cta-link').click(function(e) { 
 		$('.close').trigger('click'); 
 		e.preventDefault();
 	});


});
