var Common = function() {}
var Str = function(){} // for string manipulation functions

/**
 * Allow Only Numbers
 * 
 * @param  {object}  evt
 * @return {Boolean}
 */
Common.prototype.isNumberKey = function(evt) {
  // console.log(event.keyCode);
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57)){
     evt.preventDefault();
     evt.stopImmediatePropagation();
     return false;
    }

  return true;
}

/**
 * Show Notification
 * 
 * @param  {string} message
 * @param  {integer} notiType
 * @return {}
 */
Common.prototype.showNotification = function(message, notiType) {
    // console.log('calling Common.prototype.showNotification');
    $('#notification .alert').hide();

    var notification; 

    switch(notiType) {
        case 1:
            notification = '<div class="alert alert-success">' + message + '</div>';
            break;
        case 2: 
            notification = '<div class="alert alert-danger">' + message + '</div>';      
            break
        case 3:
            notification = '<div class="alert alert-warning">' + message + '</div>';
            break;
        default:
            notification = '<div class="alert alert-info">' + message + '</div>';
            break;
    }

    $('#notification').html(notification).fadeIn();     

    $('html, body').animate({
        scrollTop: $("#notification").offset().top
    }, 500);

};

/**
 * Hide Notification
 * 
 * @return
 */
Common.prototype.hideNotification = function(){
    // console.log('calling Common.prototype.hideNotification');
    $('#notification .alert').hide();
};

/**
 * Show Modal Notification
 * 
 * @param  {string} message
 * @param  {integer} notiType
 * @return {}
 */
Common.prototype.showModalNotification = function(message, notiType) {
    
    $('#notificationModal .alert').hide();

    var notification; 

    switch(notiType) {
        case 1:
            notification = '<div class="alert alert-success">' + message + '</div>';
            break;
        case 2: 
            notification = '<div class="alert alert-danger">' + message + '</div>';      
            break
        case 3:
            notification = '<div class="alert alert-warning">' + message + '</div>';
            break;
        default:
            notification = '<div class="alert alert-info">' + message + '</div>';
            break;
    }

    $('#notificationModal').html(notification).fadeIn();     

    $('html, body').animate({
        scrollTop: $("#notificationModal").offset().top
    }, 500);

};

/**
 * Hide Modal Notification
 * 
 * @return
 */
Common.prototype.hideModalNotification = function(){
    $('#notificationModal .alert').hide();
};


/**
 * Add Comma into a Number
 * 
 * @param {[type]} nStr
 */
Common.prototype.addCommas  = function(nStr)
{
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

/**
 * update active steps on loan application
 * 
 * @return
 */
Common.prototype.applicationSteps = function(step){

    $('#navbar ul.step li a').removeClass('active');

}

/**
 * calculate age from today's date
 * 
 * @return int age
  */
Common.prototype.calculateAge = function(birth_month,birth_day,birth_year){

    today_date = new Date();
    today_year = today_date.getFullYear();
    today_month = today_date.getMonth();
    today_day = today_date.getDate();
    age = today_year - birth_year;

    if ( today_month < (birth_month - 1))
    {
        age--;
    }

    if (((birth_month - 1) == today_month) && (today_day < birth_day))
    {
        age--;
    }

    return age;
};


var CommonObj = new Common();

/**
 * Extend Number Format function
 *
 * @todo - add this to common js
 * @param  {[type]} n
 * @param  {[type]} x
 * @return {[type]}
 */
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
};

/**
 * Generate a shortname from a string filename with extension
 * @param  {str} filename filename
 * @return {str}          the shortened filename
 */
Str.prototype.generateShortname = function(filename){

    var charNum = filename.length;
    var firstPart = filename.substring(0, 10);  
    var lastPart = filename.substring( charNum -7, charNum); 
    
    if (charNum > 18)
      filename = firstPart + '...' + lastPart;    

    return filename;
}

/**
 * get the file extension
 * @param  {string} filename string
 * @return {string}          string
 */
Str.prototype.getExtension = function(filename){
  return filename.split('.').pop();
}

var StringObj = new Str();

$(document).ready(function() {

    /**
     * Trigger All 
     * @param  object e
     * @return {Boolean}
     */
    $('.number-only').on('keypress', function(e) {
        CommonObj.isNumberKey(e);
    });

    /**
     * Keyup Trigger
     * 
     * @param  object e
     * @return {Boolean}
     */
    $('.number-only').on('keyup', function(e) {
        CommonObj.isNumberKey(e);
    });

    //trigger back button history -1
    $('.backBtn').click(function(evt){ 
      evt.preventDefault();
      evt.stopImmediatePropagation();
      window.history.back(); 
    });

    /**
    * Trigger All 
    * @param  object e
    * @return {Boolean}
    */

    $('.faq-ans').on('shown.bs.collapse', function () {
      $( '#toggle'+$(this).attr('rel') ).find('span').removeClass( "glyphicon-plus" ).addClass("glyphicon-remove");
    });

    $('.faq-ans').on('hidden.bs.collapse', function () {
      $( '.toggleArrow' ).find('span').removeClass( "glyphicon-remove" ).addClass("glyphicon-plus");
    });


    /**
     * Show Criteria Popup
     * 
     * @return
     */
    $('#borrowers .criteria .btn-learn, .criteria-popup').click( function(){
      
      console.log('Criteria');
      $('#creditCriteria').modal('show')  
      return false;
    });

    /**
     * rates term state selection
     * 
     * @return
     */
    $('#rateTerms .state-select select').change(function(){
      var stateCode = $(this).val();
      window.location = baseUrl + 'ratesTerms/' + stateCode;
    });

});