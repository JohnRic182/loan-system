/**
 * Custom Js 
 *
 * @author Global Fusion <ascend@globalfusion.net>
 * @since  version 0.1
 *
 * @notes
 * 		- Estimator has been removed for version 1.1.14.16
 */
$( document ).ready(function() {

	/**
	 * Maximum Reduct Debts Rewards
	 * @type {Number}
	 */
	var MAX_REDUCT_DEBTS_REWARDS            = 0.10;
	
	/**
	 * Maximum Increase Savings Rewards
	 * @type {Number}
	 */
	var MAX_INCREASE_SAVINGS_REWARDS        = 0.10;

	/**
	 * Maximum Limited Credit Spending Rewards
	 * @type {Number}
	 */
	var MAX_LIMITED_CREDIT_SPENDING_REWARDS = 0.10;

	/**
	 * Maximum Pays Debts on Time Rewards
	 * @type {Number}
	 */
	var MAX_PAYS_DEBTS_ON_TIME_REWARDS      = 1;

	/**
	 * Maximum Pledge Auto Title Rewards
	 * @type {Number}
	 */
	var MAX_PLEDGE_AUTO_TITLE_REWARDS       = 0.20;

	/**
	 * Annual Percentage Rate for Ascend Loan
	 */
	var APR_ASCEND                          = 0.26;

	/**
	 * Annual Percentage Rate for RateRewards
	 */
	var APR_RATEREWARDS                     = 0.0424516;
	
	/**
	 * Rate Rewards Maximum Annual Percentage Rate
	 */
	var RATEREWARDS_MAX_APR                 = 0.591493;   
	var RATEREWARDS_MULTIPLIER              = 0.0424516;	
	var ASCEND_MULTIPLIER                   = 0.0413636;

	var TotalMonthlyReward                  = 0.50,
		ReduceDebt                          = 0,
		IncreaseSavings                     = 0, 
		LimitCreditSpending                 = 0,
		PaysDebtOnTime                      = 0,
		PledgeAutoTitle                     = 0,
		AscendLoanPayment                   = 0,
		RateRewardsLoanPayment              = 0,
		AscendLoanPaymentReward             = 0,
		AscendLoanPaymentAfterReward        = 0, 
		RateRewardLoanPaymentReward         = 0; 
 
	 
	$('#estimator-trigger').addClass('closeTxt');

	/**
	 * No UI Slider 
	 * 
	 * @type {Array}
	 */
	$("#slider-tooltip").noUiSlider({
		start: [5000],
		step : 100,
		connect: "lower",
		range: {
			'min': 2600, 
			'max': 15000
		},
		format: wNumb({
			decimals: 0,
			thousand: ',',
			prefix: '$',
		})
	});

	var sliderVal; 

	/**
	 * Slider Slide Action
	 * 
	 * @param
	 * @return
	 */
	$("#slider-tooltip").on('slide', function(){ 

		sliderVal = $(this).val().replace('$','');
		sliderVal = sliderVal.replace(',', '');
 
 		$('#loanSliderInput').val(sliderVal);

 		ReduceDebt          = $('#ReduceDebtVal').val();
		IncreaseSavings     = $('#IncreaseSavingsVal').val();
		LimitCreditSpending = $('#LimitCreditSpendingVal').val();
		PaysDebtOnTime      = $('#PaysDebtOnTimeVal').val();
		PledgeAutoTitle     = $('#PledgeAutoTitleVal').val();
		TotalMonthlyReward  = $('#TotalMonthlyReward').val();

 		//Get Monthly Payment
		//RateRewardsLoanPayment = PMT( APR_RATEREWARDS / 12, 36, sliderVal);
		RateRewardsLoanPayment = sliderVal * RATEREWARDS_MULTIPLIER;
		RateRewardsLoanPayment = Math.abs(RateRewardsLoanPayment.toFixed(2));

		AscendLoanPayment = PMT( APR_ASCEND / 12, 36, sliderVal);
		//AscendLoanPayment = sliderVal * ASCEND_MULTIPLIER;
		AscendLoanPayment = Math.abs(AscendLoanPayment.toFixed(2));
	 
		$('#AscendPersonalLoanAmount').text('$' + Math.round(AscendLoanPayment));
		$('#RateRewardLoanAmount').text('$' + Math.round(RateRewardsLoanPayment));

		if( TotalMonthlyReward !="") {

			AscendLoanPaymentAfterReward  = AscendLoanPayment;
			AscendLoanPaymentAfterReward  = AscendLoanPaymentAfterReward.toFixed(2); 
			AscendLoanPaymentAfterReward  = '$' + Math.round(AscendLoanPaymentAfterReward)  + '<p>Fixed</p>';
 
			RateRewardLoanPaymentReward   = (RateRewardsLoanPayment * RATEREWARDS_MAX_APR) * parseFloat(TotalMonthlyReward).toFixed(2) ;
			RateRewardLoanPaymentReward   = RateRewardLoanPaymentReward.toFixed(2);

			RRfinalReward = RateRewardsLoanPayment - RateRewardLoanPaymentReward;

			$('#AscendPersonalLoanAmountAfterReward').html( AscendLoanPaymentAfterReward );
			$('#RateRewardLoanAmountAfterReward').text('$' + Math.round(RRfinalReward) );
		}
	});

	//slider Tooltip
	$("#slider-tooltip").Link('lower').to('-inline-');

	sliderVal = $("#slider-tooltip").val().replace('$','');
	sliderVal = sliderVal.replace(',', '');
 
	//Loan Slider Input
	$('#loanSliderInput').val( sliderVal );
  
	//Loan Type Submit
	$(".loan-type").on("click",function(){
		$('#loanTypeInput').val($(this).val());
		$('#loanTypeText').val($(this).attr('alt'));
		$('#LoanApplicationForm').trigger('submit');
	});

	/**
	 * Estimator Triggers
	 */
	$('#estimator-trigger').click(function(){
		 $( this ).find('span').toggleClass( "glyphicon-chevron-down" );
		 $(this).toggleClass('closeTxt');

	});
 

	////////////////////////////////////////////////////////////////////
	//                        ESTIMATOR                              //
	////////////////////////////////////////////////////////////////////
	
	var loanAmount      = $('#loanSliderInput').val();
	var DEFAULT_RR_RATE = 0.50;

	//Get Monthly Payment for RateReward Loan
	//RateRewardsLoanPayment = PMT( APR_RATEREWARDS / 12, 36, loanAmount);
	RateRewardsLoanPayment = loanAmount * RATEREWARDS_MULTIPLIER;
	RateRewardsLoanPayment = Math.abs(RateRewardsLoanPayment.toFixed(2));
 	 
 	//Rate Reward Loan Payment After Reward
	RateRewardLoanPaymentReward = (RateRewardsLoanPayment * RATEREWARDS_MAX_APR) * TotalMonthlyReward.toFixed(2) ;
	RateRewardLoanPaymentReward = Math.abs(RateRewardLoanPaymentReward.toFixed(2));

	RRfinalReward = RateRewardsLoanPayment - RateRewardLoanPaymentReward;
	 
	$('#TotalMonthlyReward').val(DEFAULT_RR_RATE);

	console.log(APR_ASCEND);
 	 
	AscendLoanPayment = PMT( APR_ASCEND / 12, 36, loanAmount);
	//AscendLoanPayment = loanAmount * ASCEND_MULTIPLIER;
	AscendLoanPayment = Math.round(Math.abs(AscendLoanPayment.toFixed(2)));

	//Ascend Loan Payment After Reward
	AscendLoanPaymentAfterReward = '$' + AscendLoanPayment  + '<p>Fixed</p>';
 
	$('#AscendPersonalLoanAmount').text('$' + AscendLoanPayment ); 
	$('#AscendPersonalLoanAmountAfterReward').html( AscendLoanPaymentAfterReward );
	$('#RateRewardLoanAmount').text('$' + Math.round(RateRewardsLoanPayment));
	$('#RateRewardLoanAmountAfterReward').text('$' + Math.round(RRfinalReward) );

	//Set RateRewards Default to 50%
	$('#ReduceDebtVal').val(MAX_REDUCT_DEBTS_REWARDS);
	$('#IncreaseSavingsVal').val(MAX_INCREASE_SAVINGS_REWARDS);
	$('#LimitCreditSpendingVal').val(MAX_LIMITED_CREDIT_SPENDING_REWARDS);
	$('#PaysDebtOnTimeVal').val(MAX_PAYS_DEBTS_ON_TIME_REWARDS);
	$('#PledgeAutoTitleVal').val(MAX_PLEDGE_AUTO_TITLE_REWARDS);
	

	//slider buttoms
   $('.slider-content input[type="radio"]').click(function(){
  
   		ReduceDebt          = $('#ReduceDebtVal').val();
		IncreaseSavings     = $('#IncreaseSavingsVal').val();
		LimitCreditSpending = $('#LimitCreditSpendingVal').val();
		PaysDebtOnTime      = $('#PaysDebtOnTimeVal').val();
		PledgeAutoTitle     = $('#PledgeAutoTitleVal').val();
 
   		var questionType    = $(this).attr('name'); 
   		var questionTypeVal = $(this).val();

   		if( questionType != 'autoTitle')
   			questionTypeVal = questionTypeVal / 100;
   		
   		
   		if( questionType == 'autoTitle'){
   			PledgeAutoTitle =  questionTypeVal * MAX_PLEDGE_AUTO_TITLE_REWARDS ;
   			$('#PledgeAutoTitleVal').val(PledgeAutoTitle);
   			$('#PledgeAutoTitleStep').find('span').removeClass('hide');
   		}

   		if( questionType == 'payDebtsOnTime') {
   			PaysDebtOnTime = questionTypeVal;
   			$('#PaysDebtOnTimeVal').val(PaysDebtOnTime);
   			$('#PaysDebtOnTimeStep').find('span').removeClass('hide');
   		}

   		if( questionType == 'limitCreditSaving') {
			LimitCreditSpending =  questionTypeVal * ( questionTypeVal / 10 );
			$('#LimitCreditSpendingVal').val(LimitCreditSpending);
			$('#limitCreditSavingStep').find('span').removeClass('hide');
   		}

   		if( questionType == 'increaseSavings') {
			IncreaseSavings =  questionTypeVal * ( questionTypeVal / 10 ); 
			$('#IncreaseSavingsVal').val(IncreaseSavings);
			$('#increaseSavingsStep').find('span').removeClass('hide');
   		}

   		if( questionType == 'reduceDebt') {
			ReduceDebt =  questionTypeVal * ( questionTypeVal / 10 );
			$('#ReduceDebtVal').val(ReduceDebt);
			$('#ReduceDebtStep').find('span').removeClass('hide');
   		}
 
   		TotalMonthlyReward = ( ( parseFloat(ReduceDebt) + parseFloat(IncreaseSavings) 
						+ parseFloat(LimitCreditSpending) ) * parseFloat(PaysDebtOnTime) ) 
						+ parseFloat(PledgeAutoTitle); 


		$('#TotalMonthlyReward').val(TotalMonthlyReward);

		var percentageTotal = 0; 

		if( TotalMonthlyReward != 0 ) {
		 	percentageTotal = TotalMonthlyReward * 100;	
		}
 
 		$('.reward-quote .estimate-percentage').text(  percentageTotal.toFixed() + '%');
		//Get Monthy Payment After Reward
		AscendLoanPaymentAfterReward  = AscendLoanPayment;
		AscendLoanPaymentAfterReward  = Math.abs(AscendLoanPaymentAfterReward.toFixed(2));

		AscendLoanPaymentAfterReward  = '$' + Math.round(AscendLoanPaymentAfterReward) + '<p>Fixed</p>';
		
		RateRewardLoanPaymentReward   = (RateRewardsLoanPayment * RATEREWARDS_MAX_APR) * TotalMonthlyReward.toFixed(2) ; 
		RateRewardLoanPaymentReward   = Math.abs(RateRewardLoanPaymentReward.toFixed(2));

		RRfinalReward = RateRewardsLoanPayment - RateRewardLoanPaymentReward;
  
		$('#AscendPersonalLoanAmountAfterReward').html( AscendLoanPaymentAfterReward );
		$('#RateRewardLoanAmountAfterReward').text('$' + Math.round(RRfinalReward) );
 
   });
	
	/**
	 * Carousel Slider
	 * 
	 * @param
	 * @return
	 */
	$('.carousel').on('slide.bs.carousel', function (e) {
	  var active = $(e.target).find('.carousel-inner > .item.active');
	  var from = active.index();
	  var next = $(e.relatedTarget);
	  var to = next.index();

	  $('#estimaterCollapse .steps')
	  .removeClass('active').removeClass('bubble-active').addClass('bubble')
	  .each(function(index){
	  		if( to == index )
	  			$(this).addClass('active').removeClass('bubble').addClass('bubble-active');
	  });
	})


	/**
	 * Estimator Collapse
	 * 
	 * @param
	 * @return
	 */
	$('#estimaterCollapse .steps').click(function(){
		var slidesNumber = [];
		slidesNumber['ReduceDebtStep'] 			= 0;
		slidesNumber['increaseSavingsStep'] 	= 1;
		slidesNumber['limitCreditSavingStep'] 	= 2;
		slidesNumber['PaysDebtOnTimeStep'] 		= 3;
		slidesNumber['PledgeAutoTitleStep'] 	= 4;

		$('#estimator-carousel').carousel(slidesNumber[$(this).attr('id')]);
	});

	/**
	 * RateRewards Tooltip
	 */
	$('.rwar').tooltip();
 	
	/**
	 * PMT
	 * @param {[type]} ir
	 * @param {[type]} np
	 * @param {[type]} pv
	 * @param {[type]} fv
	 * @param {[type]} type
	 */
	function PMT(ir, np, pv, fv, type) {
	    /*
	     * ir   - interest rate per month
	     * np   - number of periods (months)
	     * pv   - present value
	     * fv   - future value
	     * type - when the payments are due:
	     *        0: end of the period, e.g. end of month (default)
	     *        1: beginning of period
	     */
	    var pmt, pvif;

	    fv || (fv = 0);
	    type || (type = 0);

	    if (ir === 0)
	        return -(pv + fv)/np;

	    pvif = Math.pow(1 + ir, np);
	    pmt = - ir * pv * (pvif + fv) / (pvif - 1);

	    if (type === 1)
	        pmt /= (1 + ir);

	    return pmt;
	}
});