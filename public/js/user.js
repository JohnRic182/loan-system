/**
 * User/Applicant Module 
 *   This is where all javascript methods for User Module.
 *
 * Developer's Notes:
 *    - Added User Login method
 *    - Added CreateUser method
 *    - Added ValidateEmailAddress method\
 *    - Added UpdateLoanSessionData
 */
var loader  = $('#loader');

/**
 * User Module
 */
var User = function(){

	/**
	 * User Id
	 * @type {String}
	 */
	this.userId = '';
	
	/**
	 * Is Email Valid
	 * @type {Boolean}
	 */
	this.isEmailValid = ''; 

	this.invalidEmailMsg = 'Sorry, we can\'t verify your email address.';
}

/**
 * User Login 
 * 
 * @param  array param
 * @return
 */
User.prototype.login = function( param ) {

	console.log(param);

	$.ajax({
		url: baseUrl + 'login',
		type: 'POST',
		dataType: 'JSON',
		data: param,
		beforeSend: function(){
			loader.fadeIn().find('span').html('Logging In...');
		},
		success: function(response){

			console.log(response);

			loader.fadeOut();

			if( typeof response.result != 'undefined'){

				if( response.result == 'failed'){
					CommonObj.showNotification(response.message, 2 );
				} else{

					//Show Notification
					CommonObj.showNotification(response.message, 1 );
  
					var loanAppUrl = baseUrl;
 
					if( typeof response.loanAppUrl.Loan_App_Phase_URL_Txt != 'undefined'
						&& response.loanAppUrl.Loan_App_Phase_URL_Txt != "" ){
						loanAppUrl = baseUrl + response.loanAppUrl.Loan_App_Phase_URL_Txt;
					}else{
						loanAppUrl = baseUrl + response.loanAppUrl;
					}

					if(response.action == 'proceedToBorrowerPortal'){
						// $.ajax({
						// 	url: baseUrl + 'borrowerportal/forms/Login.aspx',
						// 	type: 'POST',
						// 	data: { Username : response.username, Password : response.password},
						// 	beforeSend: function(){
						// 		loader.fadeIn().find('span').html('Redirect to Borrower Portal');
						// 	},
						// 	success: function(response){
						// 		console.log(response);
						// 		loader.fadeOut();
						// 		//Redirect to Borrower Portal
						// 		setTimeout(function(){
						// 			window.location = baseUrl + 'borrowerportal/forms/LoanBalances.aspx';
						// 		}, 300);
						// 	}
						// });
						
						//Redirect to Portal page
						setTimeout(function(){
							window.location = 'portal';
						}, 300);

					}else if(response.action == 'showSecurityQuestion'){
						// show Security Question Popup
						$("#securityQuestionsModal").modal('show');
					}else{
						//Redirect Loan Application Step 1
						setTimeout(function(){
							window.location = loanAppUrl;
						}, 300);
					}
					
				}
			}
		}
	});

	return false;
}

/**
 * Security Question 
 * 
 * @param  array param
 * @return
 */
User.prototype.security = function( param ) {

	console.log(param);

	$.ajax({
		url: baseUrl + 'securityQuestion',
		type: 'POST',
		dataType: 'JSON',
		data: param,
		beforeSend: function(){
			loader.fadeIn().find('span').html('Saving Security questions...');
		},
		success: function(response){

			console.log(response);

			loader.fadeOut();

			if( typeof response.result != 'undefined'){

				if( response.result == 'failed'){
					CommonObj.showModalNotification(response.message, 2 );
				} else{
					
					//Redirect to Portal page
					setTimeout(function(){
						window.location = 'portal';
					}, 300);

					// $.ajax({
					// 	url: baseUrl + 'borrowerportal/forms/Login.aspx',
					// 	type: 'POST',
					// 	data: { Username : response.username, Password : response.password},
					// 	beforeSend: function(){
					// 		loader.fadeIn().find('span').html('Redirect to Borrower Portal');
					// 	},
					// 	success: function(response){
					// 		console.log(response);
					// 		loader.fadeOut();
					// 		//Redirect to Borrower Portal
					// 		setTimeout(function(){
					// 			window.location = baseUrl + 'borrowerportal/forms/LoanBalances.aspx';
					// 		}, 300);
					// 	}
					// });

				}
			}
			
		}
	});

	return false;
}

/**
 * Create User 
 * 
 * @param  {object} param
 * @return {}
 */
User.prototype.createUser = function(param) {
	
	var User  = this;

	$.ajax({
		url: baseUrl + 'postRegister',
		type: 'POST',
		dataType: 'JSON',
		data: param,
		beforeSend: function(){
			loader.fadeIn().find('span').html('Account Creation');
		},
		success: function(response){
			// console.log(response);
			if( typeof response.result != 'undefined'){
				if( response.result == 'failed'){
					CommonObj.showNotification(response.errorMessage, 2 );
					loader.fadeOut();
				} else{	
					window.location = baseUrl + 'checkcredit';
					// App.creditCheck();
				}
			}
		}
	});
}


/**
 * Validate Email Address using Mashape (Kickboxio)
 * @param  string email
 * @return
 */
User.prototype.validateEmailAddress = function( email , e) {
 
	var User  = this;

	$.ajax({
		url: baseUrl + 'validateEmail',
		type: 'POST',
		dataType: 'JSON',
		data: {email: email},
		beforeSend : function(){
			$(e).removeClass('validCheckInput')
						.addClass('inValidCheckInput');
		},
		success: function( response ){ 

			User.isEmailValid = response;

			console.log(response);

			if( typeof response != 'undefined' ) {
				if( response.result == 'invalid' ) {
					$(e).removeClass('validCheckInput')
						.addClass('inValidCheckInput');

					$('.validEmail').val('');

				}else{
					$(e).removeClass('inValidCheckInput')
						.addClass('validCheckInput');

					$('.validEmail').val(1);
				}
			}
		}
	}); 
}

/**
 * Update Loan Session Data
 * 
 * @param  array param
 * @return
 */
User.prototype.updateLoanSessionData = function(param, isAcctCreation ){

	$.ajax({
		url: baseUrl + 'updateLoanSessionData',
		type: 'POST',
		dataType: 'JSON',
		data: param,
		success: function(response) {

			console.log(isAcctCreation);

			if( isAcctCreation != false ){
				// console.log(response);
				var param = $('#UserAccountForm').serialize();
				User.createUser(param);
			}

		}
	}); 
}


/**
 * Added Typewatch
 * 
 * @param
 * @return
 */
var typewatch = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

/**
 * Initialize User Class
 * @type {User}
 */
var User = new User();
 
$(document).ready(function(){

	/**
	 * User Account Form
	 */
	$('#UserAccountForm').on('submit', function(){		
	
		var LoanParam = { 
			loanPurposeTxt: $('#loanPurposeArr option:selected').text(), 
			loanPurposeId: 	$('#loanPurposeArr').val()
		}

		console.log(LoanParam);

		if( LoanParam.loanPurposeId > 0 ) {
			// console.log('shit');
			 User.updateLoanSessionData( LoanParam );
		}else{
			CommonObj.showNotification('How will you use your loan is required', 2 );
		}	 

 		return false;
	});

	/**
	 * User Login Form 
	 * 
	 * @param
	 * @return
	 */
	$('#UserLoginForm').on('submit', function(){
		
		var param = $(this).serialize();
 
		User.login(param);

		return false;
	});

	/**
	 * Security Question Modal Form 
	 * 
	 * @param
	 * @return
	 */
	$('#SecurityQuestionForm').on('submit', function(){
		
		var param = $(this).serialize();
 
		User.security(param);

		return false;
	});	 


	/**
	 * Proceed to Credit Check
	 */
	$('.proceed').click(function(){


		var LoanParam = { 
			loanPurposeTxt: $('#loanPurposeArr option:selected').text(), 
			loanPurposeId: 	$('#loanPurposeArr').val()
		}
 
		if( LoanParam.loanPurposeId > 0 ) {
			 User.updateLoanSessionData( LoanParam );
		}else{
			CommonObj.showNotification('Loan Purpose is required', 2 );
		}	
	});


	/**
	 * Loan Purpose Default
	 */
	$('#loan-purpose').val('loanPurpose1');

	/**
	 * Loan Purpose CTA
	 *
	 * @deprecated 
	 * @param
	 * @return
	 */
	$(".loan-purpose .col-sm-4 span").on("click", function(){

		$(".loan-purpose .col-sm-4 span").each(function(){
			$(this).removeClass('active');
		});

		$(this).addClass('active');

		var param = { 
			loanPurposeTxt: $(this).text(), 
			loanPurposeId: $(this).attr('data-id')
		}

		User.updateLoanSessionData( param, false );

		$('#loan-purpose').val($(this).attr('data-id'));
	}); 
 

	/**
	 * Slider Edit Action
	 * 
	 * @return 
	 */
	//$('.slider-edit-action').click(function(event){
	$('.loanAmount').click(function(event){	
		event.preventDefault();
		event.stopImmediatePropagation();

		var askingLoanPrice = $('.asking-loan-price').text();

		$('#askingPriceInputAmount').val(askingLoanPrice);
		$('.slider-edit').fadeIn(); 

		//$(this).fadeOut();
	});

	/**
	 * Slider Edit - Cancel Button
	 */
	$('.slider-edit-cancel').click( function(event) {

		event.preventDefault();
		event.stopImmediatePropagation();

		$('.slider-edit-action').fadeIn();
		$('.slider-edit').fadeOut(); 
	});

	/**
	 * Slider Update Loan Price
	 * 
	 * @return
	 */
	$('.btn-slider-update-loan-price').click(function(){

		// console.log('button slider has been called');

		var newAskingLoanPrice = $('#askingPriceInputAmount').val().replace(',', '');
		var NumberObj = new Number(newAskingLoanPrice);
		
		if( newAskingLoanPrice  == "" || newAskingLoanPrice == 0 ) { 
			// $('#slider-edit-form .alert').fadeIn().html('Amount must not be null or empty.'); 
			$('.slider-edit .alert').fadeIn().html('Amount must not be null or empty.'); 
		} else if( newAskingLoanPrice  < 2600 ) { 
			$('.slider-edit .alert').fadeIn().html('Amount must be greater than 2600 .'); 
		} else{	

			User.updateLoanSessionData( { loanAmt: newAskingLoanPrice }, false );

			$('.asking-loan-price').html(NumberObj.format(2));
			$('.slider-edit-action').fadeIn();
			$('.slider-edit').fadeOut(); 
		}
	}); 
});