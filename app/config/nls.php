<?php

return array(
	'NLS_LOAN_PORTFOLIO_ID' => 5,
	'NLS_LOAN_TEMPLATE_ID'  => 1, 
	'NLS_LOAN_GROUP_ID'     => 1, 
	'NLS_DEFAULT_LOAN_ID'   => 1,
	'MIN_ANNUAL_INCOME'	    => 35000
);
