<?php

/**
 * Required Fields for 
 */
return array(
	'Application' => array(
		'Partner' => array(
			'PID' => array(
				'rule' => 'required|integer'
			),
			'CID' => array(
				'rule' => 'required|integer'
			), 
			'Username' => array(
				'rule' => 'required'
			), 
			'APIKey' => array(
				'rule' => 'required|string'
			), 
			'SubId' => array(
				'rule' => 'required'
			),
			'LeadId' => array(
				'rule' => 'required'
			)
		), 
		'Customer' => array(
			'LoanApp' => array(
				'LoanAmount' => array(
					'rule' => 'required|numeric|min:2600|max:15000'
				), 
				'ReasonForLoan' => array(
					'rule' => 'required|in:1,2,3,4,5,6,7,8'
				),
				'MonthlyIncome' => array(
					'rule' => 'required|numeric'
				)
			),
			'Personal' => array(
				'Ipaddress' => array(
					'rule' => 'required|ip'
				), 
				'SSN' => array(
					'rule' => 'required|regex:/^[0-9]{3}-[0-9]{2}-[0-9]{4}/'
				),
				'DOB' => array(
					'rule' => 'required|date'
				),
				'FirstName' => array(
					'rule' => 'required'
				),
				'LastName' => array(
					'rule' => 'required'
				), 
				'StreetAddress' => array(
					'rule' => 'required'
				),
				'City' => array(
					'rule' => 'required'
				),
				'State' => array(
					'rule' => 'required'
				) ,
				'Zip' => array(
					'rule' => 'required'
				),  
				'RentOrOwn' => array(
					'rule' => 'required|in:1,2,3,4'
				),
				'RentAmount' => array(
					'rule' => 'required_if:RentOrOwn,1|numeric'
				),
				'Email' => array(
					'rule' => 'required|email'
				),
				'Phone' => array(
					'rule' => 'required|regex:/^[0-9]{3}-[0-9]{3}-[0-9]{4}/'
				), 
				'EmploymentStatus' => array(
					'rule' => 'required|in:1,2'
				)
			)
		)
	)
);