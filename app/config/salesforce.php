<?php

/**
 * SalesForce Fields Mapping
 * 
 */

$list = array();

foreach(SalesForceMap::all() as $k => $config ){
 
	$list[$config->SF_Field_Name] = array(
		'SF_Field_Name'      => $config->SF_Field_Name, 
		'ODS_Table_Name'     => $config->ODS_Table_Name, 
		'ODS_Field_Name'     => $config->ODS_Field_Name, 
		'ODS_Key_Field_Name' => $config->ODS_Key_Field_Name, 
		'Is_Encrypted_Flag'  => $config->Is_Encrypted_Flag, 
		'Field_Type_Cd'      => $config->Field_Type_Cd, 
		'Field_Desc_Txt'     => $config->Field_Desc_Txt,
		'Is_Active_Flag'     => $config->Is_Active_Flag,
		'SF_Object_Name'     => $config->SF_Object_Name,
	);
}

return $list;