<?php

/**
 * Loan Application Sub Phase
 * 
 */

$list = array();

$format = function(&$list, $keys, $val) use(&$format) {
	$keys ? $format($list[array_shift($keys)], $keys, $val) : $list = $val;
};

foreach(LoanAppSubPhase::all() as $config ){

	//format code
	$code = str_replace(' ', '', $config->Loan_Sub_Phase_Desc ); 
	$code = str_replace('-', '', $code ); 

	$format($list, explode('.', $code ), array( 
		'Loan_App_Phase_Id'      => $config->Loan_App_Phase_Id, 
		'Loan_Sub_Phase_Id'      => $config->Loan_Sub_Phase_Id, 
		'Loan_Sub_Phase_Desc'    => $config->Loan_Sub_Phase_Desc, 
		'Loan_Sub_Phase_URL_Txt' => $config->Loan_Sub_Phase_URL_Txt	
		)
	);
}

return $list;