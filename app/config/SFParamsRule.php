<?php

/**
 * Salesforce Fields Validation Rules
 */
return array(
	'loanDecline' => array(
		'UserId' => 'required', 
		'LoanId' => 'required', 
		'LoanStatusDesc' => 'required'
	), 
	'loanApprove' => array(
		'UserId' => 'required', 
		'LoanId' => 'required',
		'LoanStatusDesc' => 'required',
		'AdverseActionReasonCd' => 'required'
	), 
	'reopen' => array(
		'UserId' => 'required',
		'LoanId' => 'required',
		'VerificationObjectId' => 'required', 
		'VerificationStatusDesc' => 'required'
	),
	'recalc' => array(
		'UserId' => 'required', 
		'LoanId' => 'required', 
		'CSRId'  => 'required',
		'VerifiedIncome' => 'required', 
		'AdditionalDebtPayments' => 'required'
	), 
	'bankStats' => array(
		'UserId' => 'required', 
		'LoanId' => 'required', 
		'CurrentBalance' => 'required',
		'AvailableBalance' => 'required', 
		'NSFCnt' => 'required', 
		'DaysSinceLastNSFCnt' => 'required'
	),
	'stipulation' => array(
		'UserId' => '',
		'LoanId' => '', 
		'StipulationTitle' => , 
		'StipulationId' => '',
		'DocumentName' => '', 
		'FilesRqredCnt' => ''
	),
	'assign' => array(
		'UserId' => '', 
		'LoanId' => '', 
		'CSRId'  => ''
	), 
	'verificationApprove' => array(
		'UserId' => '',
		'LoanId' => '', 
		'VerificationObjectId' => '',
		'VerificationODSId' => '', 
		'CSRId' => ''
	), 
	'verificationDecline' => array(
		'UserId' => '',
		'LoanId' => '', 
		'VerificationObjectId' => '',
		'VerificationODSId' => '', 
		'CSRId' => ''
	)
);