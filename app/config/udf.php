<?php

/**
 * NLS User Defined Fields 
 *
 * @todo - we need to transfer this to ODS table 
 */
return array(
	
	'LOANDETAIL1' => array(
		'UserDefined1'  => 'requestAmount', 
		'UserDefined2'  => 'loanPurpose', 
		'UserDefined3'  => 'employmentStatus', 
		'UserDefined4'  => 'annualIncome', 
		'UserDefined5'  => 'loanAmount', 
		'UserDefined6'  => 'totalNSFs', 
		'UserDefined7'  => 'DaysSinceNSF', 
		'UserDefined8'  => 'MoDepositToAverage', 
		'UserDefined9'  => 'avgBalPmtDate', 
		'UserDefined10' => 'currentBalance', 
		'UserDefined11' => 'numLowBalanceEvents', 
		'UserDefined12' => 'numLowBalanceDays',
		'UserDefined15' => 'loanType'
	),
	'LOANDETAIL2' => array(
		'UserDefined47' => 'numLowBalanceEvents', 
		'UserDefined48' => 'numLowBalanceDays',
		'UserDefined1'  => 'cdbM0',
		'UserDefined6'  => 'cdbPercent',
		'UserDefined7'  => 'sabM0',
		'UserDefined12' => 'sabPercent',
		'UserDefined13' => 'tccsM0',
		'UserDefined18' => 'tccsPercent',
		'UserDefined19' => 'autoTitleReward',
		'UserDefined26' => 'totalPercentDiscount',
	)
);