<?php

return array(


	'default' => 'ascend',
	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => array(

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'rollepc_admin',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'ascend' => array(
			'driver'   => 'sqlsrv',
			'host'     => '54.148.125.236',
			'database' => 'ODS',
			'username' => 'web_user',
			'password' => 'W3Bu5eR999',
			'charset'  => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'   => '',
		),

		'nls' => array(
			'driver'   => 'sqlsrv',
			'host'     => 'rs.cyberridge.com',
			'database' => 'Ascend_Test_Database',
			'username' => 'Acfrs5811',
			'charset'  => 'utf8',
			'password' => '24#nLs%06',
			'collation' => 'utf8_unicode_ci',
			'prefix'   => '',
		),

		'pgsql' => array(
			'driver'   => 'pgsql',
			'host'     => 'localhost',
			'database' => 'homestead',
			'username' => 'homestead',
			'password' => 'secret',
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		),

	),

);
