<?php

/**
 * Loan Application Phase
 * 
 */

$list = array();

$format = function(&$list, $keys, $val) use(&$format) {
	$keys ? $format($list[array_shift($keys)], $keys, $val) : $list = $val;
};

foreach(LoanAppPhase::all() as $config ){
	$format($list, explode('.', 'LOAN_APP_PHASE' . $config->Loan_App_Phase_Id), array( 
		'Loan_App_Phase_Desc' => $config->Loan_App_Phase_Desc, 
		'Loan_App_Phase_URL_Txt' => $config->Loan_App_Phase_URL_Txt	
		)
	);
}

return $list;