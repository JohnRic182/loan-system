<?php

/**
 * NLS Task Template 
 * 
 * @var array
 */
$list = array();

foreach(NLSTaskTemplate::all() as $k => $config ){
 
	$list[$config->task_template_name] = array(
		'TemplateName'       => $config->task_template_name, 
		'TemplateNo'     	 => $config->task_template_no
	);
}

return $list;