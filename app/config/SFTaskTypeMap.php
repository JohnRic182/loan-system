<?php

/**
 * Salesforce Task Type Mapping
 *
 * @package Salesforce
 * @version 1.0
 */

return array(
	1 => 'Contract',
	2 => 'Docs',
	3 => 'Employment',
	4 => 'Income',
	5 => 'Identity',
	6 => 'Income',
	7 => 'Identity',
	8 => 'Bank',
);