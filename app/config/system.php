<?php

/**
 * System Configuration
 */
$list = array();

$format = function(&$list, $keys, $val) use(&$format) {
	$keys ? $format($list[array_shift($keys)], $keys, $val) : $list = $val;
};

//get all system(command) config data
$configs = SystemConfig::getAllData(
				array(
					'Cfg_Name', 
					'Cfg_Val'
				)
			);

//format config name and values
foreach($configs as $config ){ 
	$format($list, explode('.', $config->Cfg_Name), $config->Cfg_Val );
}

return $list;