<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Salesforce Messages
	|--------------------------------------------------------------------------
	|
	| The following are the messages for Erros and Success notifications of the system
	|
	*/
 	
	'MISSING_PARAMS'             => 'missing parameters',
	'LOAN_DECLINED'              => 'Loan application has been declined!',
	'LOAN_APPROVED'              => 'Loan application has been approved!',
	'LOAN_ASSIGN'                => 'Loan application has been assigned!',
	'LOAN_REOPEN'                => 'Loan application has been reopened',
	'LOAN_AMT_UPDATE_SUCCESS'    => 'Loan amount has been updated successfully',
	'INCORRECT_LOAN_AMOUNT'      => 'Loan amount is either greater than Max Loan Amout or lower than Minimum',
	'LOAN_AMT_UNCHANGE'          => 'Loan amount already updated.',
	'APPLICATION_DOES_NOT_EXIST' => 'Loan application does not exist',
	'BANK_STATUS_UPDATE_SUCCESS' => 'Bank status has been updated',
	'VERIFICATION_APPROVED'      => 'Verification has been approved!',
	'VERIFICATION_DECLINED'      => 'Verification has been declined!', 
	'TASK_CREATED'               => 'Task is successfull created.', 
	'RECALC'                     => 'Recalculation of an application is triggered', 
	'NO_RECORD_MATCH'            => 'No Record match', 
	'INTERNAl_ERROR'             => 'Internal server error occur', 
	'DTI_DISQUALIFICATION'       => '',
	'DTI_LOAN_AMT_CHANGE'        => '', 
	'DTI_LOAN_AMT_NO_CHANGE'     => '',
	'LOAN_SP_FAIL'				 => '',
);