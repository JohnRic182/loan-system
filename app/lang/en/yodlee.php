<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Yodlee Messages
	|--------------------------------------------------------------------------
	|
	| The following are the messages for Erros and Success notifications of the system
	|
	*/

	'ERROR_ALL_FINE'                                           => "Your account has been successfully added",
	'ERROR_REFRESH_NEVER_DONE'                                 => "Please wait while we update your account",
	'ERROR_REFRESH_NEVER_DONE_AFTER_CREDENTIALS_UPDATE'        => "We are in the process of updating your account with revised credentials",
	'ERROR_STATUS_SITE_UNAVAILABLE'                            => "We could not update your account because the end site is experiencing technical difficulties.",
	'ERROR_STATUS_SITE_OUT_OF_BUSINESS'                        => "The site no longer provides online services to its customers.  Please delete this account",
	'ERROR_STATUS_SITE_APPLICATION_ERROR'                      => "We could not update your account because the site is experiencing technical difficulties.",
	'ERROR_STATUS_SITE_TERMINATED_SESSION'                     => "We could not update your account because the <SITE_NAME> is experiencing technical difficulties",
	'ERROR_STATUS_SITE_SESSION_ALREADY_ESTABLISHED'            => "We attempted to update your account, but another session was already established at the same time.  If you are currently logged on to this account directly, please log off and try after some time",
	'ERROR_STATUS_HTTP_DNS_ERROR_EXCEPTION'                    => "We could not update your account because the <SITE_NAME> site is experiencing technical difficulties. Please try later.",
	'ERROR_STATUS_ACCT_INFO_UNAVAILABLE'                       => "We were unable to detect an account. Please verify that you account information is available at this time and If the problem persists, please contact customer support at <SITE_NAME> for further assistance",
	'ERROR_STATUS_SITE_DOWN_FOR_MAINTENANCE'                   => "We were unable to update your account as the <SITE_NAME> site is temporarily down for maintenance. We apologize for the inconvenience.  This problem is typically resolved in a few hours. Please try later.",
	'ERROR_STATUS_CERTIFICATE_ERROR'                           => "We could not update your account because the <SITE_NAME> site is experiencing technical difficulties. Please try later.",
	'ERROR_STATUS_SITE_BLOCKING'                               => "We could not update your account for technical reasons. This type of error is usually resolved in a few days. We apologize for the inconvenience.",
	'ERROR_STATUS_SITE_CURRENTLY_NOT_SUPPORTED'                => "We currently does not support the security system used by this site. We apologize for any inconvenience. Check back periodically if this situation has changed. We suggest you to delete this site.",
	'ERROR_STATUS_PROPERTY_RECORD_NOT_FOUND'                   => "The <SITE_NAME> site is unable to find any property information for your address. Please verify if the property address you have provided is correct. We suggest you to delete this site. ",
	'ERROR_STATUS_HOME_VALUE_NOT_FOUND'                        => "The <SITE_NAME> site is unable to provide home value for your property. We suggest you to delete this site. ",
	'ERROR_STATUS_LOGIN_FAILED'                                => "We could not update your account because your username and/or password were reported to be incorrect.  Please re-verify your username and password.  ",
	'ERROR_STATUS_USER_ABORTED_REQUEST'                        => "Your account was not updated because you canceled the request.",
	'ERROR_STATUS_PASSWORD_EXPIRED'                            => "We could not update your account because the <SITE_NAME> site requires you to perform some additional action. Please visit the site or contact its customer support to resolve this issue. Once done, please update your account credentials in they are changed else try again.",
	'ERROR_STATUS_ACCOUNT_LOCKED'                              => "We could not update your account because it appears your <SITE_NAME>  account has been locked. This usually results from too many unsuccessful login attempts in a short period of time. Please visit the site or contact its customer support to resolve this issue.  Once done, please update your account credentials in they are changed.",
	'ERROR_STATUS_NO_ACCT_FOUND'                               => "We could not find your requested account at the <Account Name> site. You may have selected a similar site under a different category by accident in which you should select the correct site.",
	'ERROR_STATUS_DATA_MODEL_NO_SUPPORT'                       => "The type of account we found is not currently supported.  Please remove this site and add as a  manual account.",
	'ERROR_STATUS_SITE_MERGED_ERROR'                           => "The <SITE_NAME> site has merged with another. Please re-verify your credentials at the site and update the same.",
	'ERROR_STATUS_UNSUPPORTED_LANGUAGE_ERROR'                  => "The language setting for your <SITE_NAME> account is not English. Please visit the site and change the language setting to English.",
	'ERROR_STATUS_ACCOUNT_CANCELED'                            =>"We were unable to update your account information for <SITE_NAME> because it appears one or more of your related accounts have been closed.  Please deactivate or delete the relevant account and try again. ",
	'ERROR_STATUS_SPLASH_PAGE_EXCEPTION'                       =>"We could not update your account due to the <SITE_NAME> site requiring you to view a new promotion. Please log in to the site and click through to your account overview page to update the account.  We apologize for the inconvenience.",
	'ERROR_STATUS_TERMS_AND_CONDITIONS_EXCEPTION'              =>"We could not update your account due to the <SITE_NAME> site requiring you to accept a new Terms & Conditions. Please log in to the site and read and accept the T&C.",
	'ERROR_STATUS_UPDATE_INFORMATION_EXCEPTION'                =>"We could not update your account due to the <SITE_NAME> site requiring you to verify your personal information. Please log in to the site and update the fields required.",
	'ERROR_STATUS_SITE_NOT_SUPPORTED'                          =>"This site is no longer supported for data updates. Please deactivate or delete your account. We apologize for the inconvenience.",
	'ERROR_STATUS_REGISTRATION_PARTIAL_SUCCESS'                =>"Auto registration is not complete at <SITE_NAME> site. Please complete your registration at the end site. Once completed, please complete adding this account.",
	'ERROR_STATUS_REGISTRATION_FAILED_ERROR'                   =>"Your Auto-Registration could not be completed and requires further input from you.  Please re-verify your registration information to complete the process.",
	'ERROR_STATUS_REGISTRATION_INVALID_DATA'                   =>"Your Auto-Registration could not be completed and requires further input from you.  Please re-verify your registration information to complete the process.",
	'ERROR_REGISTRATION_ACCOUNT_ALREADY_REGISTERED'            =>"Your Auto-Registration could not be completed because the site reports that your account is already registered.  Please log in to the <SITE_NAME> to confirm and then complete the site addition process with the correct login information",
	'ERROR_NEW_LOGIN_INFO_REQUIRED_FOR_SITE'                   =>"We're sorry, to log in to this site, you need to provide additional information. Please update your account and try again.",
	'ERROR_NEW_MFA_INFO_REQUIRED_FOR_AGENTS'                   =>"Your account was not updated as the required additional authentication information was unavailable. Please try now.",
	'ERROR_MFA_INFO_NOT_PROVIDED_TO_YODLEE_BY_USER_FOR_AGENTS' =>"Your account was not updated as your authentication information like security question and answer was unavailable or incomplete. Please update your account settings.",
	'ERROR_MFA_INFO_MISMATCH_FOR_AGENTS'                       =>"We're sorry, <site> indicates that the additional authentication information you provided is incorrect. Please try updating your account again.",
	'ERROR_ENROLL_IN_MFA_AT_SITE'                              =>"Please enroll in the new security authentication system, <Account Name> has introduced. Ensure your account settings in <Cobrand> are updated with this information.",
	'ERROR_MFA_INFO_NOT_PROVIDED_IN_REAL_TIME_BY_USER_VIA_APP' =>"Your request has timed out as the required security information was unavailable or was not provided within the expected time. Please try again.",
	'ERROR_INVALID_MFA_INFO_IN_REAL_TIME_BY_USER_VIA_APP'      =>"We're sorry, the authentication information you  provided is incorrect. Please try again.",
	'ERROR_USER_PROVIDED_REAL_TIME_MFA_DATA_EXPIRED'           =>"We're sorry, the authentication information you provided has expired. Please try again.",
	'ERROR_INVALID_MFA_INFO_OR_CREDENTIALS'                    =>"We could not update your account because your username/password or additional security credentials are incorrect. Please try again.",
	'ERROR_NO_CONNECTION'                                      =>"We're sorry, your request timed out. Please try again.",
	'ERROR_INTERNAL_ERROR'                                     =>"We're sorry, there was a technical problem updating your account. This kind of error is usually resolved in a few days. Please try again later.",
	'ERROR_LOST_REQUEST'                                       =>"We're sorry, there was a technical problem updating your account. Please try again later.",
	'ERROR_DATA_EXPECTED'                                      =>"We're sorry, we couldn't find any accounts for you at the <SITE_NAME> site. Please log in at the  <SITE_NAME>  site and confirm that your account is set up, then try again.",
	'ERROR_REQUIRED_FIELD_UNAVAILABLE'                         =>"We're sorry, we couldn't update your account at <SITE_NAME> site because of a technical issue. This type of problem is usually resolved in a few days. Please try again later.",
	'ERROR_LOGIN_NOT_COMPLETED'                                =>"We're sorry, we couldn't update your account because of unexpected variations at the <SITE_NAME> site. This kind of problem is usually resolved in a few days. Please try again later.",
	'ERROR_BETA_SITE_WORK_IN_PROGRESS'                         =>"We're sorry, Yodlee has just started providing data updates for this site, and it may take a few days to be successful as we get started. Please try again later.",
	'ERROR_INSTANT_REQUEST_TIMEDOUT'                           =>"We are sorry, your request timed out due to technical reasons. Please try again.",
	'ERROR_TOKEN_ID_INVALID'                                   =>"We're sorry, we can't update your account because your token is no longer valid at the <SITE_NAME> site. Please update your information and try again, or contact <SITE_NAME>'s customer support.",
	'ERROR_GENERAL_EXCEPTION_WHILE_GATHERING_MFA_DATA'         =>"We'resorry, there was a technical problem updating your account. Please try again.",
	'ERROR_MFA_INFO_NOT_PROVIDED_IN_REAL_TIME_BY_GATHERER'     =>"We could not update your account for technical reasons. This type of error is usually resolved in a few days. We apologize for the inconvenience. Please try again later."

);