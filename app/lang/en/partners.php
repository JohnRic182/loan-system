<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Partners API Errors
	|--------------------------------------------------------------------------
	|
	| The following are the messages for Erros and Success notifications of the system
	|
	*/
 	
 	//Error Codes
 	'ERR_MISSING_CODE'    => array(
 		101 => 'Missing Parameters'
 	),
	'ERR_VALIDATION_CODE' => => array(
 		102 => 'Validation Field'
 	),
	'ERR_FRAUD_CHECK'    => array(
 		103 => 'Fraud Check Fail'
 	),
	'ERR_SP_EXCLUSION'    => array(
 		104 => 'Exclusion Fail'
 	),
	'ERR_SP_DECISIONING'  => array(
 		105 => 'Decisiong Fail'
 	),
	'ERR_TU_CREDIT_PULL'  => array(
 		106 => 'Credit Pull Fail'
 	),
	'ERR_CONTACT_CREATE'  => array(
 		107 => 'Creating Contact Fail'
 	),
	'ERR_LOAN_OBJECT'     => array(
 		108 => 'Creating Loan Fail'
 	)
);