<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Reason Code Messages
	|--------------------------------------------------------------------------
	|
	| The following are the messages for the reason codes
	|
	*/

	'A03'		=> 		'Unable to verify employment',
	'A05'		=> 		'Income insufficient for amount of credit requested',
	'A16'		=> 		'Existing loan not paid off',
	'A17'		=> 		'Time since recent declined application is too short',
	'X001'		=> 		'No credit file',
	'X002'		=> 		'Credit file mismatch',
	'X021'		=> 		'Bankruptcy filing reported',
	'X023'		=> 		'Delinquency on accounts',
	'X066'		=> 		'Number of derogatory public record',
	'X063'		=> 		'Number of accounts with recent delinquency',
	'X082'		=> 		'Proportion of balance to limit on bank revolving accounts is too high',
	'X009'		=> 		'Amount owed on bank revolving accounts is too high',
	'X019'		=> 		'Amount owed on revolving account is too high',
	'X008'		=> 		'Amount owed on accounts is too high',
	'X042'		=> 		'Length of time accounts have been established',
	'X089'		=> 		'Too few accounts with balances',
	'X109'		=> 		'Too many recently opened accounts with balances',
	'X100'		=> 		'Too many installment accounts',
	'XD01'		=> 		'Frequency of NSFs',
	'XD02'		=> 		'Recent NSF',
	'XD03'		=> 		'Negative or low balance',
	'XD04'		=> 		'Not enough funds',
	'XD05'		=> 		'Significant deposits decline'

);