<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| System Messages for Errors and Success
	|--------------------------------------------------------------------------
	|
	| The following are the messages for Erros and Success notifications of the system
	|
	*/

	//REGISTRATION
	'registrationSuccess'           => 'Registration success.', 
	'registrationFail'              => 'Registration failed.',
	'registrationFailEmailExist'    => 'Email Address already exists.',
	'registrationFailUserNameExist' => 'UserName already exist.',

	//LOGIN
	'userLoginFailed'               => 'The user name or password you entered is incorrect', 
	'userLoginSuccess'              => 'Successful login.',

	'requestMethodNotAllowed'       => 'Your Request is not allowed.',
	'requestParameterInvalid'       => 'Invalid Parameters.',

	//YODLEE
	'yodleeAccountCreationFailed'   => 'Yodlee Account creation Failed. Redirecting to Exclusion Process',

	//VALIDATION
	'ssnDuplicate'                  => 'There is another account associated with this Social Security Number',
	'invalidPassword'               => 'Password may only contain letter, numbers, upper -lower case and combination',
	'invalidAge'                    => 'Age should be 18 and above',
	'invalidState'                  => 'State not included',

	//CREDIT CHECK
	'noCreditPullData'              => 'We were unable to pull your credit file.  Please check your profile details and try again.',
  'applicationPeriodFailed'       => 'Our records show that you have already applied with this social security number. Our credit policies prohibit us from accepting multiple applications within six months. Please try back later.',

	//VERIFICATION MESSAGES
	300 => 'You have successfully completed this step.',
	301 => 'Verification step failed.',
	
	
	//GENERAL CODE
	//@todo change the above implementation to code
	201 => 'No data found!',
	202 => 'Lorem ipsum dolor sit amet.'
);