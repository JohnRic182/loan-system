 <meta charset="UTF-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1,max-scale=1">
 <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
 <link href='//fonts.googleapis.com/css?family=Open+Sans:700,400,600,300' rel='stylesheet' type='text/css'>
 <title>Ascend</title>
 {{HTML::style('css/bootstrap.min.css')}}
 {{HTML::style('css/style.css')}}
 <script> 
 	if ( window.location.protocol == "https:"){
 		var baseUrl = 'https://'+ location.hostname + '/'; 
 	}else{
 		var baseUrl = 'http://'+ location.hostname + '/'; 
 	} 
 </script>