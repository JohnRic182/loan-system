<header>
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
      <div class="container">
      <div class="navbar-header"> 
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
         </button> 
         <!-- <ul class="visible-xs nav navbar-nav pull-right small-devices" style="margin: 4px 10px;">
            <li><a href="/" class="btn btn-cta">CHECK YOUR RATE</a></li> 
          </ul>   --> 
          @if( isset($homepage) )
            <a class="navbar-brand navbar-brand-logo" href="/"></a>
          @else
            <a class="navbar-brand navbar-brand-logo navbar-brand-logo2" href="/"></a>
          @endif
      </div> 

      <div id="navbar" class="pull-right pagination-nav hidden-xs">
          <div class="pull-left">Loan Application</div>
          <ul class="pagination step"> 
            @for ($i = 1; $i < 7 ; $i++)
              @if(getActiveStepSession() == $i)
                <li><a href="#" class="active" >{{ $i }}</a></li> 
              @else
                <li><a href="#" >{{ $i }}</a></li> 
              @endif 
            @endfor             
          </ul>
        </div><!--/.navbar-collapse -->
      </div>
       
  </nav> 
</header>