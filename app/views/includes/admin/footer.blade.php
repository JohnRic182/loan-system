<div class="container inner-container footer-links">
	<div class="row-fluid">
		<div class="col-xs-6 col-sm-2">

			<ul class="nav">
				<li>Company</li>
				<li>{{ HTML::link('inThePress', 'In The Press') }}</li> 
				<li><a href="/#aboutus">About Us</a></li>
				<li>{{ HTML::link('faqs', 'FAQ') }}</li>  
			</ul>
			<div class="cleafix"></div>
		</div>
		<div class="col-xs-6 col-sm-2">
			<ul class="nav">
				<li>Product</li>
				<li>{{ HTML::link('howItWorks', 'RateRewards') }}</li>
				@if (!isLoggedIn())
				<li>{{ HTML::link('login', 'Log In') }}</li>
				@endif 
				<li>{{ HTML::link('#', 'Credit Criteria', array('class' => 'criteria-popup')) }}</li>
			</ul>
			<div class="cleafix"></div>
		</div>
 		 
		<div class="col-xs-6  col-sm-2">
			<ul class="nav">
				<li>Communicate</li>
				<li>{{ HTML::link('contact', 'Contact Us') }}</li>
				{{-- <li class="facebook"><a href="#"><i></i></a></li>
				<li class="twitter"><a href="#"><i></i></a></li> --}}
			</ul>
			<div class="cleafix"></div>
		</div> 

		<div class="col-xs-6 col-sm-3">
			<ul class="nav">
				<li>Legal</li>		
				<li>{{ HTML::link('ratesTerms', 'Rates & Terms') }}</li>
				<li>{{ HTML::link('privacyPolicy', 'Privacy Policy') }}</li>
				<li>{{ HTML::link('terms', 'Terms and Conditions') }}</li>
			</ul>
			<div class="cleafix"></div>
		</div>


		<div class="col-xs-12 col-sm-3 " id="footer-contact">
			<p class="footer-contact">Have a Question? - Contact Us.</p>
			<p class="footer-contact-number">800.497.5314</p>
			<p><a href="mailto:support@ascendloan.com">support@ascendloan.com</a></p>
			<div class="cleafix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	 
	 <hr>
	
	<div class="row-fluid"> 
		<div class="col-sm-12 disclaimer pull-right"> 
			<p class="copyright"> &copy; Copyright 2015 ASCEND CONSUMER FINANCE v1.1.4.20</p>
		</div> 
	</div>
</div>

{{-- POPUPS --}}
@include('blocks.application.credit-criteria.criteria')

 
{{HTML::script('js/jquery.min.js')}}
{{HTML::script('js/moment.js')}}
{{HTML::script('js/bootstrap.min.js')}}
{{HTML::script('js/bootsrap-datepicker.js')}}
