<header>
  <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container"> 
          <div class="navbar-header"> 
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
           </button> 
           <!-- <ul class="visible-xs nav navbar-nav pull-right small-devices" style="margin: 4px 10px;">
              <li><a href="/" class="btn btn-cta">CHECK YOUR RATE</a></li> 
            </ul>   --> 
            @if( isset($homepage) )
              <a class="navbar-brand navbar-brand-logo" href="{{ getHompageRelativeUrl() }}"></a>
            @else
              <a class="navbar-brand navbar-brand-logo navbar-brand-logo2" href="{{ getHompageRelativeUrl() }}"></a>
            @endif
          </div> 
          <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav main-nav">
                  <li><a href="{{ getHompageRelativeUrl() }}#howitworks">How It Works</a></li>
                  <li><a href="#" class="criteria-popup">Credit Criteria</a></li>
                  <li class="visible-xs"><a href="/ratesTerms">Rates &amp; Terms</a></li>
                  @if (!isLoggedIn())
                    <li class="visible-xs"><a href="{{ url('login') }}">Login </a></li> 
                  @else
                    <li class="visible-xs"><a href="{{ url('logout') }}">Logout </a></li> 
                  @endif
              </ul>
              <ul class="nav navbar-nav pull-right hidden-xs">
                <li><a href="#" class=""><i class="glyphicon glyphicon-earphone cta-link "></i> 800-497-5314</a></li>
                <!-- <li><a href="/" class="btn btn-cta check-rate">CHECK YOUR RATE</a></li> -->
                <li class="hidden-sm"><a href="/ratesTerms">Rates &amp; Terms </a></li>
                <li class="hidden-sm"><a href="#">|</a></li>
                @if (!isLoggedIn())
                  <li><a href="{{ url('login') }}">Login </a></li> 
                @else
                  <li><a href="{{ url('logout') }}">Logout </a></li> 
                @endif
              </ul>  
          </div> 
  </nav> 
</header>