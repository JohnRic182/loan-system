
<div class="loan-rate-sidebar"> 
<div class="sidebar-offer">
	<p>{{ HTML::image(asset('img/v2/logo-black.png'), '' ); }}</p>
	<hr>
	<ul>
	  	<li class="clearfix"><span class="icon icon-ok"></span> <span>Low APR and payment</span></li>
	  	<li class="clearfix"><span class="icon icon-ok"></span> <span>Earn an interest discount with RateRewards</span></li>
	  	<li class="clearfix"><span class="icon icon-ok"></span> <span>No origination fees</span></li>
	  	<li class="clearfix"><span class="icon icon-ok"></span> <span>Get funded in 1-3 business days</span></li>
	</ul>
	<hr>
</div>
<div class="sidebar-footer">
	<h2>Questions?</h2>
	<div class="row">
		<div class="col-xs-3 col-sm-2">
			<span class="icon icon-contact"></span>
		</div>
		<div class="col-xs-9 col-sm-10">
			<p>Call us at 800-497-5314 <br><a href="mailto:questions@ascendloan.com">questions@ascendloan.com</a></p>
		</div>
	</div>
</div>
</div>  