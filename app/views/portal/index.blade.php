
@section('content') 

    <div class="appl-title">
        <div class="container">            
        </div> 
    </div>
    <div id="borrowerPortal" class="container" {{ $styleTrigger }}> 
        <div class="content-wrapper">
            <div class="loan-details box-wrapper">
                <div class="row">
                    <div class="col-sm-3" {{ $styleNav }}>
                        <div class="portal-sidebar">
                            <ul>
                                <li><a href="/portal" class="active">Summary</a></li>
                                <li><a href="/portal/history">History</a></li>
								<li><a href="/portal/statement">Statement</a></li>
                                @if( $prodId == 2)
                                <li><a href="/portal/raterewards">RateRewards</a></li>
                                @endif
                                <li><a href="/portal/payment">Payment</a></li>
                                <li><a href="/portal/account">Account</a></li>
                            </ul>
                            <div class="sidebar-footer">
                                <h2>Questions?</h2>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-2">
                                        <span class="icon icon-contact"></span>
                                    </div>
                                    <div class="col-xs-9 col-sm-10">
                                        <p>Call us at 800-497-5314 <br><a href="mailto:questions@ascendloan.com">questions@ascendloan.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="portal-contents">
                            <div class="row data-header">
                                <div class="data-left col-sm-6"><span class="glyphicon glyphicon-list-alt"></span>  Summary </div>
                                <div class="data-right col-sm-6">Loan # {{ $loanNumber }}</div>
                            </div>
                            <div class="row data-row">
                                <div class="data-left col-sm-6">Initial Balance</div>
                                <div class="data-right col-sm-6">${{ $initialBalance }}</div>
                            </div>
                            <div class="row data-row">
                                <div class="data-left col-sm-6">APR</div>
                                <div class="data-right col-sm-6">{{ $APR }}%</div>
                            </div>
                     {{--        <div class="row data-row">
                                <div class="data-left col-sm-6">Required Monthly Payment</div>
                                <div class="data-right col-sm-6">${{ $monthlyPayment }}</div>
                            </div> --}}
                            <div class="row data-row no-dash">
                                <div class="data-left col-sm-6">Maturity Date</div>
                                <div class="data-right col-sm-6">{{ date('m/d/Y', strtotime($maturityDate)) }}</div>
                            </div>

                            @if( $prodId == 2 )
                            <hr>
                            <div class="row data-row">
                                <div class="data-left col-sm-6">Current Balance</div>
                                <div class="data-right col-sm-6">${{ $currentBalance }}</div>
                            </div>
                            <div class="row data-row">
                                <div class="data-left col-sm-6">Next Payment Date</div>
                                <div class="data-right col-sm-6">{{ date('m/d/Y', strtotime($nextPaymentDate)) }}</div>
                            </div>
                            <div class="row data-row data-row">
                                <div class="data-left col-sm-6">Base Payment</div>
                                <div class="data-right col-sm-6">${{ $basePayment }}</div>
                            </div>
                            <div class="row data-row">
                                <div class="data-left col-sm-6">{{ $monthReward}} Reward</div>
                                <div class="data-right col-sm-6">${{ $monthRewardAmt }}</div>
                            </div>
                            <div class="row data-row no-dash">
                                <div class="data-left col-sm-6">Payment Due</div>
                                <div class="data-right col-sm-6">${{ $paymentDue }}</div>
                            </div>

                            @endif
                        </div>    
                    </div>
                </div>
            </div>
        </div>        
    </div>        

@stop
