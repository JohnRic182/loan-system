@section('content') 

    <div class="appl-title">
        <div class="container">            
        </div> 
    </div>
    <div id="borrowerPortal" class="container" {{ $styleTrigger }}> 
        <div class="content-wrapper">
            <div class="loan-details box-wrapper">
                <div class="row">
                    <div class="col-sm-3" {{ $styleNav }}>
                        <div class="portal-sidebar">
                            <ul>
                                <li><a href="/portal">Summary</a></li>
                                <li><a href="/portal/history" class="active">History</a></li>
                                <li><a href="/portal/statement">Statement</a></li>
                                @if( $prodId == 2)
                                <li><a href="/portal/raterewards">RateRewards</a></li>
                                @endif
                                <li><a href="/portal/payment">Payment</a></li>
                                <li><a href="/portal/account">Account</a></li>
                            </ul>
                            <div class="sidebar-footer">
                                <h2>Questions?</h2>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-2">
                                        <span class="icon icon-contact"></span>
                                    </div>
                                    <div class="col-xs-9 col-sm-10">
                                        <p>Call us at 800-497-5314 <br><a href="mailto:questions@ascendloan.com">questions@ascendloan.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class=" col-sm-12 portal-contents history">
                            <div class="data-header">
                                <div class="col-sm-12 no-margin"><span class="glyphicon glyphicon-th-list"></span>  History </div>
                            </div>

                            <div class="row">
                                
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Description</th>
                                                <th class="border-left" >Payment</th>
                                                <th>Rate Rewards</th>
                                                <th class="border-left" >Fees</th>
                                                <th>Interest</th>
                                                <th>Principal</th>
                                                <th class="border-left">Ending Balance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            @if( count($loanHistory) > 0)
                                                @foreach($history as $date => $historyData)
                                                    {{-- <tr>
                                                        <td>{{ date('m/d/Y', strtotime($history->Txn_Dt)) }}</td>
                                                        <td>{{ $history->Txn_Desc}}</td>
                                                        <td>${{ round($history->Txn_Amt,2) }}</td>
                                                        <td>${{ round($history->Current_Loan_Bal_Amt, 2) }}</td>
                                                        <td>{{ $history->Txn_Cd; }}</td>
                                                        <!-- <div class="col-sm-2">${{ round($outstandingBalance, 2) }}</div> -->
                                                    </tr> --}}


                                                <tr>
                                                    <td>{{ date('m/d/Y', strtotime($date)) }}</td>
                                                    <td>{{ $historyData['Desc'] }}</td>
                                                    <td class="border-left">{{ $historyData['Payment'] }}</td>
                                                    <td>{{ $historyData['RR'] }}</td>
                                                    <td class="border-left" >{{ $historyData['Fees'] }}</td>
                                                    <td>{{ $historyData['Interest'] }}</td>
                                                    <td>{{ $historyData['Principal'] }}</td>
                                                    <td class="border-left" >{{ $historyData['Balance'] }}</td>
                                                </tr>

                                                @endforeach    
                                            @endif
                                        </tbody>
                                    </table>    
                                </div>

                            </div>


                           {{--  <div class="row data-history-title">
                                <div class="col-sm-3">Date</div>
                                <div class="col-sm-3">Description</div>
                                <div class="col-sm-3 data-right">Amount</div>
                                <div class="col-sm-3 data-center">Outstanding Balance</div>
                            </div>

                            @if( count($loanHistory) > 0)
                                @foreach($loanHistory as $history)
                                    <div class="data-history-row">
                                        <div class="col-sm-3">{{ date('m/d/Y', strtotime($history->Txn_Dt)) }}</div>
                                        <div class="col-sm-5">{{ $history->Txn_Desc}}</div>
                                        <div class="col-sm-2">${{ round($history->Txn_Amt,2) }}</div>
                                        <div class="col-sm-2">${{ round($history->Current_Loan_Bal_Amt, 2) }}</div>
                                        <!-- <div class="col-sm-2">${{ round($outstandingBalance, 2) }}</div> -->
                                    </div>
                                @endforeach    
                            @endif --}}


                           

                        </div>        
                    </div>
                </div>
            </div>
        </div>        
    </div>        

@stop
