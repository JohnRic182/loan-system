@section('loader')
    <div id="loader">
        <div class="loader">Loading...</div>
        <span style="color:#FFF">Loading Summary Page...</span>
    </div>
@stop


@section('content') 

    <div class="appl-title">
        <div class="container">            
        </div> 
    </div>
   <div id="borrowerPortal" class="container" {{ $styleTrigger }}> 
        <div class="content-wrapper">
            <div class="loan-details box-wrapper">
                <div class="row">
                    <div class="col-sm-3" {{ $styleNav }}>
                        <div class="portal-sidebar">
                            <ul>
                                <li><a href="/portal">Summary</a></li>
                                <li><a href="/portal/history">History</a></li>
								<li><a href="/portal/statement">Statement</a></li>
                                @if( $prodId == 2)
                                <li><a href="/portal/raterewards">RateRewards</a></li>
                                @endif
                                <li><a href="/portal/payment" class="active">Payment</a></li>
                                <li><a href="/portal/account">Account</a></li>
                            </ul>
                            <div class="sidebar-footer">
                                <h2>Questions?</h2>
                                <div class="row">
                                    <div class="col-xs-3 col-sm-2">
                                        <span class="icon icon-contact"></span>
                                    </div>
                                    <div class="col-xs-9 col-sm-10">
                                        <p>Call us at 800-497-5314 <br><a href="mailto:questions@ascendloan.com">questions@ascendloan.com</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="portal-contents">
                            <div class="data-header">
                                <div class="col-sm-12 no-margin"><span class="glyphicon glyphicon-share-alt"></span>  Make a Payment </div>
                            </div>
                            <p class="data-center"><br /><br />
                                This functionality is currently under development.<br />
                                Please call 800-497-5314 for assistance.
                            </p>
                        </div>    
                    </div>
                </div>
            </div>
        </div>        
    </div>        

@stop
