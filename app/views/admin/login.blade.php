<!DOCTYPE html>
<html>
<head>
<title>AscendLoan Admin</title>
{{HTML::style('css/bootstrap.min.css')}}
{{HTML::style('css/style.css')}}
<style type="text/css">
	html{ background-color: #FFF; }

	.wrapper {	
		margin-top: 80px;
		margin-bottom: 80px;
	}

	.form-signin {
		max-width: 500px;
		padding: 45px 35px;
		margin: 0 auto;
		background-color: #fff;
		border: none;
		border-radius: 0;
		box-shadow: none;
	}
	.form-signin img{
		margin-bottom: 25px;
	}

	.form-signin-heading,
	.checkbox {
		margin-bottom: 30px;
	}

	.checkbox {
		font-weight: normal;
	}

	.form-control {
		position: relative;
		font-size: 16px;
		height: auto;
		padding: 10px;
	}

	.form-signin label {
		text-align: left;
		font-size: 15px;
		padding-left: 0;
		text-transform: uppercase;
		color: #555;
	}

	.form-signin .row-fluid,
	.form-signin .row
	{
		padding-left: 15px;
	}


	input[type="text"] {
		margin-bottom: -1px;
	}

	input[type="password"] {
		margin-bottom: 20px;
	}
 
</style>
</head>
<body>

	<div class="container">
		<div class="wrapper">

			{{ Form::open(array('url' => '/admin/login', 'method' => 'POST', 'id' => 'reportFunding', 'class' => 'form-horizontal form-signin well text-center', 'role' => 'form')) }}
				{{HTML::image(asset('img/logoBig.png'));}}

				@if(Session::has('error')) 
				 <div class="alert alert-danger">
				 	{{ Session::get('error') }}
				 </div>
				@endif
	
				<div class="form-group">
					<div class="row-fluid">
						<label for="User Name" class="col-xs-4"> User Name</label>
						<input type="text" class="form-control" name="User_Name" placeholder="Please enter your username" required="" autofocus="" />
					</div>
				</div>
				<div class="form-group">
					<div class="row-fluid">
						<label for="User Name" class="col-xs-4"> Password</label>
						<input type="password" class="form-control col-xs-8" name="Password_Txt" placeholder="Please enter your password" required=""/>     
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-xs-12">
							<button class="btn btn-lg btn-block btn-cta" type="submit">Login</button>   
						</div>
					</div>
				</div>
			{{Form::close()}}
		</div>
	</div>

</body>
</html>

{{HTML::script('js/jquery.min.js')}}
{{HTML::script('js/bootstrap.min.js')}}