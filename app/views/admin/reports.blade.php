@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop

@section('header')
  <div class="container report-nav">
    <div class="row">
      <div class="col-sm12">

		<ul class="nav nav-pills">
		  <li role="presentation" class="active"><a href="#">Report</a></li>
		  <li role="presentation"><a href="#">Funding</a></li>
		</ul>

      </div>
    </div>
  </div>
@stop

@section('content') 

<fieldset id="exclusion-report-fieldset">

	<div class="content-wrapper container">

		<div class="row report-options" >
			
			<div class="col-sm-5">
				<label>Report Type:</label>
				<select name="reportType" class="form-control" id="reportType" required="required">
					
					@foreach ($reportType as $element)
						<option value="{{ str_replace(' ','_',$element) }}">{{ $element }}</option>
					@endforeach

				</select>
			</div>

			<div class="col-sm-3 ">

				<div class="form-group date-inputs">
					<div class='input-group date' id='datetimepickerStart'>
						<input type='text' class="form-control" placeholder="Start Date" value="{{date('Y-m-d')}}" />
						<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
					</div>
				</div>

			</div>

			<div class="col-sm-3">

				<div class="form-group date-inputs" >
					<div class='input-group date' id='datetimepickerEnd'>
						<input type='text' class="form-control" placeholder="End Date" value="{{date('Y-m-d')}}" />
						<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
					</div>
				</div>

			</div>
			<div class="col-sm-1 button-col"><button id="exclusionFilterBtn" class="btn btn-cta pull-left">Submit</button></div>
		</div>

		<div class="row ">
			<div class="table-row">
				<div class="col-sm-12 container results-tab" style="overflow: auto; max-height :500px;">

				<table class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th>Date</th>
							<th>AVG Amount Requested</th>
							<th>AVG Amount Approved</th>
							<th>AVG Amount Financed</th>
							<th>AVG FICO Approved</th>
							<th>AVG Amount Applied</th>
						</tr>
					</thead>
					<tbody class="table-hover">

						@for ($i = 0; $i < 50 ; $i++)

						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>

						@endfor	

					</tbody>
				</table>					
					
				</div>
			</div>
		</div>
	</div>

	<div class=" container-fluid">

		<div class="row action-section">
			<div class="col-sm-12">
				{{HTML::link('/admin/exportExclusionReport/'. date('Y-m-d') .'/'. date('Y-m-d') , 'Export to XLS', array('class' => 'btn btn-cta pull-right', 'id' => 'exclusionExportBtn'))}}
			</div>
		</div>

	</div>

</fieldset>

@stop

@section('scripts')

{{ HTML::script( 'js/bootstrap-datepicker.min.js');  }}
{{ HTML::script( 'js/admin.js');  }}

<script type="text/javascript">
	$(function () {
		$('#datetimepickerStart').datepicker({ format: 'yyyy-mm-dd' });
		$('#datetimepickerEnd').datepicker({ format: 'yyyy-mm-dd', });
	});
</script>
@stop