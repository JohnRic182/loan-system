@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop

@section('header')
  {{-- <div class="container report-nav">
    <div class="row">
      <div class="col-sm12">

		<ul class="nav nav-pills">
		  <li role="presentation" class="active"><a href="#">Report</a></li>
		  <li role="presentation"><a href="#">Funding</a></li>
		</ul>

      </div>
    </div>
  </div> --}}
@stop

@section('content') 

<fieldset id="partner-report-fieldset">

	<div class="content-wrapper container">

		<div class="row report-options" >
			
		</div>

		<div class="row ">
			<div class="table-row">
				<div class="col-sm-12 container results-tab" style="overflow: auto; max-height :500px;">

				<form name="partnerForm" id="partnerForm">
				<table class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th>Partner ID</th>
							<th>Partner Name</th>
							<th>Partner Type</th>
							<th>Partner Status</th>
						</tr>
					</thead>
					<tbody class="table-hover">



						@foreach($partners as $val)

						<tr>
							<td>{{ $val->Partner_Id }}</td>
							<td>{{ $val->Partner_Name }}</td>
							<td>{{ $val->Partner_Type_Id }}</td>
							<td>
								<input class = "partnerStat" rel="{{ $val->Partner_Id }}" {{ ($val->Actv_Flag) ? "checked" : "" ; }} type="checkbox" data-toggle="toggle" data-size = "mini" data-on="Active" data-off="Inactive">	
							</td>
						</tr>

						@endforeach

					</tbody>
				</table>					
				</form>
				</div>
			</div>
		</div>
	</div>

{{-- 	<div class=" container-fluid">

		<div class="row action-section">
			<div class="col-sm-12">
			<button class="btn btn-cta pull-right" id="savePartner">Save</button>

				{{-- {{HTML::link('/admin/savePartner/' , 'Save', array('class' => 'btn btn-cta pull-right', 'id' => 'exclusionExportBtn'))}} --}}
			</div>
		</div>

	</div>

</fieldset>

@stop

@section('scripts')

{{ HTML::script( 'js/admin.js');  }}

{{ HTML::script( 'js/bootstrap-toggle.min.js');  }}

<script type="text/javascript">
	$(function () {
		$('#datetimepickerStart').datepicker({ format: 'yyyy-mm-dd' });
		$('#datetimepickerEnd').datepicker({ format: 'yyyy-mm-dd', });
	});
</script>
@stop