@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Searching Borrower...</span>
	</div>
@stop


@section('content') 

<fieldset id="exclusion-report-fieldset">

	<div class="content-wrapper container">

		<div class="row reopen" >
			
			<div class="col-sm-5">
				<label>Search Loan:</label>
				<input type="text" class="form-control" placeholder="User ID or Last Name" id="searchReopenInput" name="searchBox" />
				
			</div>

			<div class="col-sm-7 button-col"><button id="searchReopenBtn" class="btn btn-cta pull-left">Submit</button></div>

		</div>

		<div class="row " id="reopenContents">

			<div class="col-sm-12 reopenContentsDiv">		
				<h3>Loan Detail</h3>
				<table class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th class="col-sm-1">User ID</th>
							<th class="col-sm-1">Loan ID</th>
							<th class="col-sm-2">First Name</th>
							<th class="col-sm-2">Last Name</th>
							<th class="col-sm-2">Application Date</th>
							<th class="col-sm-1">Current Balance</th>
							<th class="col-sm-2">Loan Status</th>
							<th class="col-sm-1"></th>
						</tr>
					</thead>
					<tbody class="table-hover">
						<tr>
							<td id="resultUserId"></td>
							<td id="resultLoanId"></td>
							<td id="resultFirstName"></td>
							<td id="resultLastName"></td>
							<td id="resultApplicationDate"></td>
							<td id="resultCurrentBalance"></td>
							<td id="resultLoanStatus"></td>
							<td id="reopenAction"><button class="reopenButton btn btn-cta">Reopen</button></td>
						</tr>
					</tbody>
				</table>
			
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="reopen-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">

	      <div class="modal-body">
	      	Are you sure you want to Reopen?
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" id="reopenStart">YES</button>
	        <button type="button" class="btn btn-cta" id="reopenClose">NO</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class=" container-fluid">

		

	</div>

</fieldset>

@stop

@section('scripts')

{{ HTML::script( 'js/bootstrap-datepicker.min.js');  }}
{{ HTML::script( 'js/admin.js');  }}

@stop