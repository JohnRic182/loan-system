@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Searching Borrower...</span>
	</div>
@stop


@section('content') 

<fieldset id="exclusion-report-fieldset">

	<div class="content-wrapper container">

		<div class="row report-options" >
			
			<div class="col-sm-5">
				<label>Search Borrower:</label>
				<input type="text" class="form-control" placeholder="First or Last Name"  id="search_borrower_input" name="searchBox" />
				
			</div>

			<div class="col-sm-3 ">

				<div class="form-group date-inputs borrowerSelectionPanel">
					<!-- <select name="borrowerListDD" class="form-control" id="borrowerSelection" required="required">					
					</select> -->
				</div>

			</div>

			<div class="col-sm-3 ">

				<div class="form-group date-inputs portalSelectionPanel">
					<label>Pages:</label>
					<select name="borrowerListDD" class="form-control" id="portalSelection" required="required">	
						<option value="summary">Summary</option>		
						<option value="history">History</option>		
						<option value="statement">Statement</option>		
						<option value="raterewards">RateRewards</option>			
						<option value="payment">Payment</option>			
						<option value="account">Account</option>				
					</select> 
				</div>

			</div>	

			<div class="col-sm-1 button-col"><button id="searchBorrowerBtn" class="btn btn-cta pull-left">Submit</button></div>

		</div>

		{{ HTML::script( 'js/jquery.min.js');  }}
		{{ HTML::script( 'js/bootstrap-datepicker.min.js');  }}
		{{ HTML::script( 'js/admin.js');  }}
		{{ HTML::script( 'js/bootstrap.min.js');  }} 

		<div class="row " id="borrowerRewardContents">
		</div>
	</div>

	<div class=" container-fluid">
	</div>

</fieldset>

@stop

{{-- @section('scripts')
{{ HTML::script( 'js/jquery.min.js');  }}
{{ HTML::script( 'js/bootstrap-datepicker.min.js');  }}
{{ HTML::script( 'js/admin.js');  }}
{{ HTML::script( 'js/bootstrap.min.js');  }} 
@stop --}}