@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop

@section('content') 

	<fieldset id="report-fieldset">
		<legend>Funding</legend>
	
	<div class="content-wrapper container-fluid">
		
		<div class="row">
			<div class="col-sm-12">
				<h3>Approved Loans</h3>
			</div>
		</div>
		
		@if(Session::has('nlsErrors'))
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>NLS Error</strong> update failed.
		</div>
		@endif

		@if(Session::has('successMsg'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ Session::get('successMsg') }}</strong>
		</div>
		@endif



		<div class="row ">
		<div class="table-row">
		{{ Form::open(array('url' => '/admin/updateFunding', 'method' => 'POST', 'id' => 'reportFunding','class' => 'form-horizontal' , 'role' => 'form')) }}
			<div class="col-sm-12">		

				<table class="table table-hover table-bordered table-condensed table-striped">
					<thead>
						<tr>
							<th class="col-sm-3">Name</th>
							<th class="col-sm-2">Loan Type</th>
							<th class="col-sm-2">Amount Requested</th>
							<th class="col-sm-2">Max Loan Amount</th>
							<th class="col-sm-2">Loan group</th>
							<th class="col-sm-1">Loan funded</th>
						
						</tr>
					</thead>
					<tbody class="table-hover">
						
					@foreach ($loans as $key =>  $val)

						<tr class="{{ (in_array($val->Loan_Id, $nlsErrors))? 'danger' : '' }}">
							<td>{{ ucfirst(strtolower($val->First_Name)). " ". ucfirst(strtolower($val->Last_Name)) }}</td>
							<td>{{ $val->Loan_Prod_Name }}</td>
							<td>{{ $val->Loan_Amt }}</td>
							<td>{{ $val->Max_Loan_Amt }}</td>
							<td>									
							{{ Form::select("loan_group[$val->Loan_Id]", $loan_group, 1 , array('placeholder' => 'select', 'class' => 'form-control ' ) ) }}
							</td>
							<td>
								<div class="checkbox">
									<label>
										<input name="funded[]" type="checkbox" value="{{ $val->Loan_Id }}">
									</label>
								</div>
							</td>
							
						</tr>

					@endforeach

					</tbody>
				</table>

			</div>
		
			</div>
		</div>
		
		<hr>

	</div>

	<div class=" container-fluid">

		<div class="row action-section">
			<div class="col-sm-12">
				<input type="submit" class="btn btn-cta pull-right" value="Submit" />
			</div>
		</div>
		
	</div>

	{{ Form::close() }}
	
	</fieldset>

@stop

@section('scripts')

{{ HTML::script( 'js/common.js');  }}
{{ HTML::script( 'js/admin.js');  }}

<script type="text/javascript">
	$(function () {
		$('#datetimepickerStart').datetimepicker();
		$('#datetimepickerEnd').datetimepicker();
	});
</script>
@stop