<table border="0" style="font-family:Arial, san-serif;" align="center" width="600">
    <tr>
        <td><a href="">{{ HTML::image( asset('img/logoBig.png')) }}</a></td>
    </tr> 
    <tr>
        <td style="text-align:left;">
            <p>Hi {{ $firstName }}, </p>
            <br>
            <p>Thank you for choosing Ascend Consumer Finance as your personal loan provider. You can choose from our Ascend Personal Loan that offers constant low monthly payments or our exciting Rate Rewards Loan that lets you lower you interest cost up to 50%.  Either way, you’re just a few clicks away from completing your application. </p>

            <p>Come back and we will show you the products, balances, and rates you qualify for.</p>

            <p>We are here to assist and answer any questions you may have, we are standing by now ready to help.</p>

            <p>We are open Monday thru Friday, 9am to 5pm PDT <br>
                Phone:  800-497-5314    <br>
                Email: <a href="mailto:support@ascendloan.com">support@ascendloan.com</a><br>
                Fax:   
            </p>

            <p>Thank you,</p>

            <p>Wayde Anderson <br>
            Ascend Consumer Finance
            </p>
        </td>
    </tr>
</table>  