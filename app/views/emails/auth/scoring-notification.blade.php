<!DOCTYPE html>
<html>
    <head>
        <title>Ascend Loan Scoring Notification</title>
        <meta charset="UTF-8">
        <style>
            body{
            }
            #main{
                width: 900px;
                margin: 0px auto;
                font-size: 16px;
                text-align: justify;
            }
            .header{
                font-weight: bold;
                text-align: center;
            }
            h3{
                margin: 10px 0px 10px 0px;
                padding: 0px 0px 0px 0px;
                text-align: center;
                color: gray;
            }
            table,td,tr{
                padding: 0px;
                margin: 0px;
            }
            .ta1{
                border: solid 1px gray;
                width: 447px;
                float: left;
                padding: 5px 0px;
                text-indent: 2px;
            }
            .ta2{
                border: solid 1px gray;
                padding: 5px 0px;
                float: left;
                width: 895px;
                margin: 10px 0px; 
                text-indent: 2px;
            }
            .tal3{
                width: 100%;
                text-align: center;
                float: left;
            }
            h2{
                margin: 0px;
                padding: 0px;
                font-size: 16px;
                text-align: center;
            }
            ul, p{ float: left;}
            .center { text-align: center; float: left; width: 100%; }
        </style>
    </head>
    <body>
        <div id="main">
            <div class="header">
            <br/> 
            Ascend Consumer Finance, Inc.<br />
    3701 Sacramento Street #442<br />
    San Francisco, CA 95121<br />
     {{$phone}}} – Fax: <u>{{ $fax }}</u> – www.<u>{{ $site }}</u><br />
            </div>
            <h3>NOTICE OF ACTION TAKEN AND STATEMENT REASONS</h3>
            <div style="width: 100%">
                <div class="ta1">Applicant’s Name:{{ $applicant }}</div>
                <div class="ta1">Applicant’s Address: {{ $address }}</div>
                <div class="ta1">Date:{{ $date }}</div>
                <div class="ta1">Description of Requested Credit:{{ $description }}</div>
                <div class="ta2">Description of Action Taken: Denial of Credit Request</div> 
            </div>
            <p>     
    Dear Applicant,<br />
    <br />
    Thank you for your recent application for <u>{{ $description }}</u>. We regret that we are unable to approve your request.
    <br /><br />
    Your application was processed by an Ascend custom risk system that assigns a numerical value to the various items of information in your credit report we consider in evaluating an application. These numerical values are based upon the results of analyses of repayment histories of large numbers of customers.

    <br />The information in your credit bureau report provided from the consumer reporting agency did not score a sufficient number of points for approval of the application. The reasons you did not score well compared with other applicants were:
  </p>
            <ul>
            @foreach ($scoringList as $scoring)
                <li>{{ $scoring }}</li>
            @endforeach
            </ul>
            <p>
    Our credit decision was based on information obtained in a report from the consumer reporting agency listed below. You have a right under the Fair Credit Reporting Act to know the information contained in your credit file at the consumer reporting agency. You also have a right to a free copy of your report the reporting agency, if you request it no later than 60 days after you receive this notice. In addition, if you find that any information contained in the report you receive is inaccurate or incomplete, you have the right to dispute the matter with the reporting agency:
    <br />Trans Union (www.transunion.com)
    <br />P.O. Box 1000
    <br />Chester, PA 19022
    <br />1-800-916-8800
  </p>
    <br /><br />
   <div class="tal3"><center>IF YOU HAVE ANY QUESTIONS REGARDING THIS NOTICE, YOU SHOULD CONTACT:<br />
        Ascend Consumer Finance, Inc., 3701 Sacramento Street #442, San Francisco, CA 95121</center></div>
    <br />
    <div class="center"><hr />
    <h2>NOTICE</h2><br /></div>
    <p>
    The federal Equal Credit Opportunity Act prohibits creditors from discriminating against credit applicants on the basis of race, color, religion, national origin, sex, marital status, age (provided that the applicant has the capacity to enter into a binding contract); because all or part of the applicant’s income derives from any public assistance program; or because the applicant has in good faith exercised any right under the Consumer Credit Protection Act.   The federal agency that administers compliance with this law concerning this creditor is the Federal Trade Commission, Equal Credit Opportunity, Washington, DC 20508.
            </p>
        </div><br /><br /><br />
    </body>

