<table border="0" style="font-family:Arial, san-serif;" align="center" width="600">
    <tr>
        <td><a href="">{{ HTML::image( asset('img/logoBig.png')) }}</a></td>
    </tr> 
    <tr>
        <td style="text-align:left;">
            <p>Hi {{ $firstName }}, </p>
            <br>
            <p>Congratulations you've been approved and are only a few short steps away from getting your loan with Ascend Consumer Finance.
            <br><br>
            Please click on the link below to confirm your email and return to complete your loan application. </p>

            <p><a id="verificationURL" href="{{ $verificationUrl }}">{{ $verificationUrl }}</a></p>

            <p>We are here to assist and answer any questions you may have, we are standing by now ready to help.</p>

            <p>We are open Monday thru Friday, 9am to 5pm PDT <br>
                Phone:  800-497-5314    <br>
                Email: <a href="mailto:support@ascendloan.com">support@ascendloan.com</a><br>
                Fax:   
            </p>

            <p>Thank you,</p>

            <p>Customer Support<br>
            Ascend Consumer Finance
            </p>
        </td>
    </tr>
</table>