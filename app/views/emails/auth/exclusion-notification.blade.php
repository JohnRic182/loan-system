<!DOCTYPE html>
<html>
    <head>
        <title>Ascend Loan Exclusion Notification</title>
        <meta charset="UTF-8">
        <style>
            body{
            }
            #main{
                width: 900px;
                margin: 0px auto;
                font-size: 16px;
                text-align: justify;
            }
            .header{
                font-weight: bold;
                text-align: center;
            }
            h3{
                margin: 10px 0px 10px 0px;
                padding: 0px 0px 0px 0px;
                text-align: center;
                color: gray;
            }
            table,td,tr{
                padding: 0px;
                margin: 0px;
            }
            .ta1{
                border: solid 1px gray;
                width: 447px;
                float: left;
                padding: 5px 0px;
                text-indent: 2px;
            }
            .ta2{
                border: solid 1px gray;
                padding: 5px 0px;
                float: left;
                width: 895px;
                margin: 10px 0px; 
                text-indent: 2px;
            }
            .tal3{
                width: 900px;
                text-align: center;
                float: left;
            }
            h2{
                margin: 0px;
                padding: 0px;
                font-size: 16px;
                text-align: center;
            }
            ul, p{ float: left;}
            .center { text-align: center; float: left; width: 100%; }
        </style>
    </head>
    <body>
        <div id="main">
            <div class="header">
            <br/> 
            Ascend Consumer Finance, Inc.<br />
    3701 Sacramento Street #442<br />
    San Francisco, CA 95121<br />
     {{$phone}}} – Fax: <u>{{ $fax }}</u> – www.<u>{{ $site }}</u><br />
            </div>
            <h3>NOTICE OF ACTION TAKEN AND STATEMENT REASONS</h3>
            <div class="ta1">Applicant’s Name:{{ $applicant }}</div>
            <div class="ta1">Applicant’s Address: {{ $address }}</div>
            <div class="ta1">Date:{{ $date }}</div>
            <div class="ta1">Description of Requested Credit:{{ $description }}</div>
            <div class="ta2">Description of Action Taken: Denial of Credit Request</div> 
            <p>     
    Dear Applicant,<br />
    <br />
    Thank you for your recent application. Your request for Ascend personal loan was carefully considered, and we regret that we are unable to approve your application at this time, for the following reason(s): 
    Exclusions used for declining (including application, bureau, DDA)
    <br /><br />
    We also obtained your credit score from this consumer reporting agency and used it in making our credit decision. Your credit score is a number that reflects the information in your consumer report. Your credit score can change, depending on how the information in your consumer report changes. 
    <br />Your credit score: <u>{{ $score }}</u>
    <br />Date: <u>{{ $date }}</u>
    <br />Scores range from a low of <u>{{ $low }}</u> to a high of <u>{{ $high }}</u>
    <br />Key factors that adversely affected your credit score:
  </p>
            <ul>
            @foreach ($exclusions as $exclusions)
                <li>{{ $exclusions }}</li>
            @endforeach
                <li>[Number of recent inquiries on consumer report, as a key factor]</li>
            </ul>
            <p>
    Our credit decision was based on information obtained in a report from the consumer reporting agency listed below. You have a right under the Fair Credit Reporting Act to know the information contained in your credit file at the consumer reporting agency. You also have a right to a free copy of your report the reporting agency, if you request it no later than 60 days after you receive this notice. In addition, if you find that any information contained in the report you receive is inaccurate or incomplete, you have the right to dispute the matter with the reporting agency:
    <br />Trans Union (www.transunion.com)
    <br />P.O. Box 1000
    <br />Chester, PA 19022
    <br />1-800-916-8800
  </p>
    <br /><br />
   <div class="tal3">IF YOU HAVE ANY QUESTIONS REGARDING THIS NOTICE, YOU SHOULD CONTACT:
        Ascend Consumer Finance, Inc., 3701 Sacramento Street #442, San Francisco, CA 95121</div>
    <br />
    <div class="center"><hr />
    <h2>NOTICE</h2><br /></div>
    <p>
    The federal Equal Credit Opportunity Act prohibits creditors from discriminating against credit applicants on the basis of race, color, religion, national origin, sex, marital status, age (provided that the applicant has the capacity to enter into a binding contract); because all or part of the applicant’s income derives from any public assistance program; or because the applicant has in good faith exercised any right under the Consumer Credit Protection Act.   The federal agency that administers compliance with this law concerning this creditor is the Federal Trade Commission, Equal Credit Opportunity, Washington, DC 20508.
            </p>
        </div><br /><br /><br />
    </body>
