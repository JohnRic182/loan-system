<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Password Reset Notification</h2>

		<div>
			To reset your password, please follow this link: <a href="<?php echo $url ?>">Reset now</a>.<br/>
		</div>
	</body>
</html>