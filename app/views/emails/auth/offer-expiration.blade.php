<table border="0" style="font-family:Arial, san-serif;" align="center" width="600">
    <tr>
        <td style="text-align:left;">
            <p>Dear {{ $firstName }} {{ $lastName }},</p>
            
            <p>Thank you for your interest in AscendLoan.  We’re sorry but your loan application has expired.  Loan offers expire 15 days after the initial offer.  </p>

            <br>
            <p>Best regards,</p> 
            <p>
                The AscendLoan Team<br />
                800-497-5314<br />
                support@ascendloan.com
            </p>
        </td>
    </tr>
    <tr>
        <td><a href=""><img src="https://ascendloan.com/img/rrLogo.png"></a></td>
    </tr> 
</table>