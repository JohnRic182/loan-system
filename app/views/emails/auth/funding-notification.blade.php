<table border="0" style="font-family:Arial, san-serif;" align="center" width="600">
    <tr>
        <td>
            <a href="">
            
                @if ( !in_array( $loanGroup, $ascendPersonalLoans ) )
                    {{ HTML::image( asset('img/rrLogo.png')) }}
                @else
                    {{ HTML::image( asset('img/logoBig.png')) }}
                @endif
                
            </a>
        </td>
    </tr> 
    <tr>
        <td style="text-align:left;">

        @if ( !in_array( $loanGroup, $ascendPersonalLoans ) )

            <h3>Welcome to Ascend Consumer Finance!</h3>

            <p>Your loan has been approved for funding and you should see the funds in your account in 1-2 business days.  </p>

            <p>Your RateReward loan has a base APR and payment that will be the same every month.  However, you can take actions every month that will reduce your interest cost up to 50%. We will automatically calculate your reward and reduce your monthly payment accordingly.</p>

            <p>There are three monthly rewards that can each reduce your interest cost up to 10%.  We automatically enroll you in the “Debt Reductions” and “Limit Credit Spending” rewards programs and will track this information automatically from your credit report.  You can enroll in the “Increase Savings” monthly reward by logging into to your borrower portal at <a href="http://www.ascendloan.com">www.ascendloan.com</a> and linking your savings account.  You can earn an additional 20% off your interest cost by enrolling in Auto Title rewards by calling us at 800-497-5314.</p>

            <p>You can read more about these actions here:  <a href="https://ascendloan.com/howItWorks">https://ascendloan.com/howItWorks</a></p>

            <p>If you enrolled in our easy electronic payment process, your payment will be automatically processed on your payment date.  If you elected to pay by check, you are responsible for mailing payments on time.  You may change your payment date by calling 800-497-5314 x2.</p>

            <h3>Rate us and tell others how we do  </h3>


            <p>We work hard to provide a great borrowing experience.  If you think we’re great, click through the link below to rate us and let others know know. </p>

             
            <p><a href="https://www.lendingtree.com/user/lender-55933257/review">https://www.lendingtree.com/user/lender-55933257/review</a></p>


            <p>Please contact us if you have any questions about your account or how to earn your RateRewards.</p>


            <p>
                Ascend Consumer Finance<br>
                <a href="mailto:support@ascendloan.com">support@ascendloan.com</a><br>
                800-497-5314
            </p>

        @else

            <h3>Welcome to Ascend Consumer Finance!</h3>

            <p>Your loan has been approved for funding and you should see the funds in your account in 1-2 business days.</p>
              
            <p>Your Ascend Personal Loan has a fixed APR and payment that will be the same every month.  If you enrolled in our easy electronic payment process, your payment will be automatically processed on your payment date.  If you elected to pay by check, you are responsible for mailing payments on time.  You may change your payment date by calling 800-497-5314.</p>



            <h3>Rate us and tell others how we do  </h3>

            <p>We work hard to provide a great borrowing experience.  If you think we’re great, click through the link below to rate us and let others know know. </p>

             
            <p><a href="https://www.lendingtree.com/user/lender-55933257/review">https://www.lendingtree.com/user/lender-55933257/review</a></p>
            <br>


            <p>Please contact us if you have any questions about your account,</p>
            <p>
                Ascend Consumer Finance<br>
                <a href="mailto:support@ascendloan.com">support@ascendloan.com</a><br>
                800-497-5314
            </p>


        @endif

        



        </td>
    </tr>
</table>  