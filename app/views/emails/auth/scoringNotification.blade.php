<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		
		Ascend Consumer Finance, Inc.
3701 Sacramento Street #442
San Francisco, CA 95121
 (415) xxx-xxxx – Fax: ___________ – www._________

NOTICE OF ACTION TAKEN AND STATEMENT REASONS

Applicant’s Name:
	Applicant’s Address:

Date:	Description of Requested Credit: 

Description of Action Taken:  

Dear Applicant,

Thank you for your recent application for ____. We regret that we are unable to approve your request.

Your application was processed by an Ascend custom risk system that assigns a numerical value to the various items of information in your credit report we consider in evaluating an application. These numerical values are based upon the results of analyses of repayment histories of large numbers of customers.

The information in your credit bureau report provided from the consumer reporting agency did not score a sufficient number of points for approval of the application. The reasons you did not score well compared with other applicants were:
• Reason statement #1 (based on AARC #1)
• Reason statement #2 (based on AARC #2)
• Reason statement #3 (based on AARC #3)
• Reason statement #4 (based on AARC #4)

Our credit decision was based on information obtained in a report from the consumer reporting agency listed below. You have a right under the Fair Credit Reporting Act to know the information contained in your credit file at the consumer reporting agency. You also have a right to a free copy of your report the reporting agency, if you request it no later than 60 days after you receive this notice. In addition, if you find that any information contained in the report you receive is inaccurate or incomplete, you have the right to dispute the matter with the reporting agency:
Trans Union (www.transunion.com)
P.O. Box 1000
Chester, PA 19022
1-800-916-8800
 
	
IF YOU HAVE ANY QUESTIONS REGARDING THIS NOTICE, YOU SHOULD CONTACT:
Ascend Consumer Finance, Inc., 3701 Sacramento Street #442, San Francisco, CA 95121
_____________________________________________________________________________________________

NOTICE
The federal Equal Credit Opportunity Act prohibits creditors from discriminating against credit applicants on the basis of race, color, religion, national origin, sex, marital status, age (provided that the applicant has the capacity to enter into a binding contract); because all or part of the applicant’s income derives from any public assistance program; or because the applicant has in good faith exercised any right under the Consumer Credit Protection Act.   The federal agency that administers compliance with this law concerning this creditor is the Federal Trade Commission, Equal Credit Opportunity, Washington, DC 20508.



	</body>
</html>
