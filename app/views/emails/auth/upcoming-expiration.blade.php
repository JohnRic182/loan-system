<table border="0" style="font-family:Arial, san-serif;" align="center" width="600">
    <tr>
        <td style="text-align:left;">
            <p>Dear {{ $firstName }} {{ $lastName }},</p>
            <p>
                Thank you for your interest in AscendLoan.  AscendLoan offers expire 15 days after the initial offer and you have only {{ $remainingDays }} left to finalize your loan.
            </p>
            <p></p>

            <table align="center" style="width: 280px">
                <tr>
                    <td style="border-right: 1px dashed gray">LOAN AMOUNT</td>
                    <td style="text-indent: 20px; ">TERM</td>
                </tr>
                <tr style="color: #54abd6">
                    <td style="border-right: 1px dashed gray"><strong>${{ $loanAmount }}<br /><br /></strong></td>
                    <td style="text-indent: 20px; "><strong>{{ $term }} Months</strong><br /><br /></td>
                </tr>
                <tr style="border-top: 1px dashed gray">
                    <td style="border-top: 1px dashed gray; border-right: 1px dashed gray">APR</td>
                    <td style="border-top: 1px dashed gray; text-indent: 20px; padding-top: 5px">MONTHLY<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PAYMENT</td>
                </tr>
                <tr style="color: #54abd6">
                    <td style="border-right: 1px dashed gray"><strong>{{ $APR }}%</strong><br /><br /></td>
                    <td style="text-indent: 20px; "><strong>${{ $MonthlyPayment }}</strong><br /><br /></td>
                </tr>
            </table>

            <p></p>

            <p>Come back now to finalize your loan and you can have your cash in as little as {{ $remainingBusinessDays }}.  <strong>Continue your offer <a href="https://www.ascendloan.com">here ></a></strong>
            </p>
            <br>
            <p>Best regards,</p> 
            <p>
                The AscendLoan Team<br />
                800-497-5314<br />
                support@ascendloan.com
            </p>
        </td>
    </tr>
    <tr>
        <td><a href=""><img src="https://ascendloan.com/img/rrLogo.png"></a></td>
    </tr> 
</table>