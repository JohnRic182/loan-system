<table border="0" style="font-family:Arial, san-serif;" align="center" width="600">
    <tr>
        <td style="text-align:left;">
            <p>Dear {{ $firstName }} {{ $lastName }},</p>
            <p>
                Thank you for your interest in AscendLoan.  AscendLoan applications expire 15 days after the application date and you have only {{ $remainingDays }} left to finalize your loan..
            </p>
            
            <p>Come back now to finish your application and you can have your cash in as little as {{ $remainingBusinessDays }}.  <strong>Continue your offer <a href="https://www.ascendloan.com">here ></a></strong>
            </p>
            <br>
            <p>Best regards,</p> 
            <p>
                The AscendLoan Team<br />
                800-497-5314<br />
                support@ascendloan.com
            </p>
        </td>
    </tr>
    <tr>
        <td><a href=""><img src="https://ascendloan.com/img/rrLogo.png"></a></td>
    </tr> 
</table>