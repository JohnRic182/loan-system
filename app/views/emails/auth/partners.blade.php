<table border="0" style="font-family:Arial, san-serif;" align="center" width="600">

    <tr>
        <td align="center">
            <a href="">{{ HTML::image( asset('img/rrLogo.png')) }}  </a>
            <hr style="border-color: #4AA1D0">
        </td>
    </tr> 
    <tr>
        <td style="text-align:left;">
            
            <p>Thank you for your interest in AscendLoan.  We’re proud to partner with LendingTree to bring you the most innovative loans on the market.  Get a fixed rate and payment with the Ascend Personal Loan.  Reduce your interest by up to 50% by adding our optional RateRewards program that rewards good financial behavior.</p>

            <p style="font-weight: bold;">Based on your LendingTree information, you may qualify for the following loan:</p>

            <table align="center" style="width: 600px" >
                <tr>
                    <td>
                        
                        <table align="center" style="width: 350px">
                            <tr>
                                <td style="border-right: 1px dashed gray">LOAN AMOUNT</td>
                                <td style="text-indent: 20px; ">TERM</td>
                            </tr>
                            <tr style="color: #54abd6">
                                <td style="border-right: 1px dashed gray"><strong>${{ $LOAN_AMOUNT }}<br /><br /></strong></td>
                                <td style="text-indent: 20px; "><strong>{{ $TERMS }} Months</strong><br /><br /></td>
                            </tr>
                            <tr style="border-top: 1px dashed gray">
                                <td style="border-top: 1px dashed gray; border-right: 1px dashed gray">APR</td>
                                <td style="border-top: 1px dashed gray; text-indent: 20px; ">MONTHLY PAYMENT</td>
                            </tr>
                            <tr style="color: #54abd6">
                                <td style="border-right: 1px dashed gray"><strong>{{ $APR }}</strong><br /><br /></td>
                                <td style="text-indent: 20px; "><strong>${{ $MONTHLY_PAYMENT }}</strong><br /><br /></td>
                            </tr>
                        </table>

                    </td>
                    <td>
                        
                        <table align="center" style="width: 250px; font-size : 14px;">
                            <tr>
                                <td >
                                    
                                    <ul style="list-style : none; padding-left : 0px;">
                                        <li style="margin-bottom: 5px;">
                                            <span style = "background: url( {{ asset('/img/v2/sprites.png') }} ) scroll -340px -33px transparent no-repeat; width: 25px; height: 20px; display: inline-block; float: left;"></span>
                                            <p style="margin-bottom: 15px; padding-left: 30px;">Get your quote in minutes</p>
                                        </li>
                                    
                                        <li style="margin-bottom: 15px;" >
                                            <span style = "background: url( {{ asset('/img/v2/sprites.png') }} ) scroll -340px -33px transparent no-repeat; width: 25px; height: 20px; display: inline-block; float: left;"></span>
                                            <p style="margin-bottom: 15px; padding-left: 30px;" >Get funded in 1-3 days</p>
                                        </li>
                                  
                                        <li style="margin-bottom: 15px;" >
                                            <span style = "background: url( {{ asset('/img/v2/sprites.png') }} ) scroll -340px -33px transparent no-repeat; width: 25px; height: 20px; display: inline-block; float: left;"></span>
                                            <p style="margin-bottom: 15px; padding-left: 30px;" >Use your money however you want</p>
                                        </li>
                                    
                                        <li style="margin-bottom: 15px;" >
                                            <span style = "background: url( {{ asset('/img/v2/sprites.png') }} ) scroll -340px -33px transparent no-repeat; width: 25px; height: 20px; display: inline-block; float: left;"></span>
                                            <p style="margin-bottom: 15px; padding-left: 30px;">Add RateRewards to lower your payment</p>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>


            
            <p align="center">
                <a href="{{ $OFFER_URL }}">{{ HTML::image( asset('/img/checkRateBtn.png'), '', array('width'=>'250px')) }}</a>    
            </p>
            

            
            <p>
                Click on the following link to continue where you left off at LendingTree.  We’ve saved your offer information and you’re only minutes away from getting approved for the best loan available.
                <br>
                Continue your offer {{ HTML::link( $OFFER_URL , 'here' ) }}
            </p>

            <hr style="border-color: #4AA1D0">
            <p>Best regards,</p> 
            <p>
                The AscendLoan Team<br />
                800-497-5314<br />
                support@ascendloan.com
            </p>
        </td>
    </tr>
</table>
