@section('content') 
{{ $contents }}
<!-- <div class="pages contact-us-page">
	<h1 class="text-center">Contact Us</h1> 
	<div class="content-wrapper contact-us">
		<div class="container inner-container">
			<div class="row">
					<div class="col-sm-6">
					<div class="box">
						<img src="{{ asset('img/contact-icon1.png')}}" alt="" class="pull-left">
						<div class="pull-left contact-desc">
							<p class="title">Hours</p>
							<p>
								Monday through Friday <br>
								9 am - 5 pm PST
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box">
						<img src="{{ asset('img/contact-icon4.png')}}" class="pull-left">
						<div class="pull-left contact-desc">
							<p class="title">Application/General</p>
							<p>
								Phone: 800-497-5314 <br>
								Fax: 630-578-2342 <br>
								<a href="mailto:support@ascendloan.com">support@ascendloan.com</a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="box">
						<img src="{{ asset('img/contact-icon2.png')}}"  class="pull-left">
						<div class="pull-left contact-desc">
							<p class="title">Billing/Account Service</p>
							<p> 
								Phone: 800-497-5314 <br>
								<a href="mailto:myloan@ascendloan.com">myloan@ascendloan.com</a>
							</p>
						</div>
					</div>
				</div>
				
				<div class="col-sm-6">
					<div class="box">
						<img src="{{ asset('img/contact-icon3.png')}}"  class="pull-left">
						<div class="pull-left contact-desc">
							<p class="title">Payment Address</p>
							<p>
								Ascend Consumer Finance <br>
								P.O Box 51751 <br>
								Los Angeles, CA 90051-6051
							</p>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>  -->
@stop
@section('scripts')
	{{HTML::script('js/common.js');}}
@stop