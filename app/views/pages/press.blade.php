@section('content') 
{{ $contents }}  
<!-- <div id="press-page" class="pages contact-us-page press-page">
	<h1 class="text-center">In the Press</h1> 
	<div class="content-wrapper contact-us">
		<div class="container inner-container">
			<div class="row">
				 <div class="col-sm-12">
				 	<div class="box container">
				 		<div class="row-fluid">
					 		<div class="box-header">
				 				<img src="{{ asset('img/press-icon.png')}}"  class="pull-left">
				 				<p class="pull-left">Media contacts  <br>
									<a href="mailto:media@ascendloan.com">media@ascendloan.com</a>
								</p>	
					 		</div>
					 	</div>
						<div class="clearfix"></div>
				 		<div class="row-fluid press-list">
							<h3>In The Press:</h3>
				 			<ul>
				 				<li><span>04/04/2015</span> {{ HTML::link('http://venturebeat.com/2015/04/14/ascend-raises-1-5m-to-launch-loans-where-you-can-bet-on-yourself/', 'Ascend raises $1.5M to launch loans where you can bet on yourself.VB NEWS', array('target' => '_blank')); }}</li>
								<li><span>04/04/2015</span> {{ HTML::link('http://vator.tv/news/2015-04-14-ascend-consumer-finance-launches-with-15m-seed', 'Ascend Consumer Finance launches with $1.5M seed. VATOR NEWS', array('target' => '_blank')); }}</li>
								<li><span>04/04/2015</span>{{ HTML::link('http://www.thestreet.com/video/13110637/ascend-ceo-discusses-real-time-loan-rate-adjustment-strategy.html', 'Ascend CEO Discusses Real-Time Loan Rate Adjustment Strategy. TheStreet.com', array('target' => '_blank')); }} </li>
								<li><span>06/03/2015</span>{{ HTML::link('http://www.insidephilanthropy.com/home/2015/6/3/how-a-cutting-edge-effort-to-boost-family-financial-stabilit.html', 'How a Cutting Edge Effort to Boost Family Financial Stability is Giving Out $3 Million. Inside Philanthropy', array('target' => '_blank')); }} .</li>
								<li><span>06/11/2015</span> {{ HTML::link('http://www.bloomberg.com/article/2015-06-11/apQBpNCmKMFI.html', 'Financial Solutions LabSM Announces Winners of $3 Million Cash Flow Management Competition. Bloomberg Business.', array('target' => '_blank')); }}</li>
								<li><span>06/11/2015</span> {{ HTML::link('http://www.jpmorganchase.com/corporate/Corporate-Responsibility/financial-solutions-lab.htm', '2015 Financial Solutions Lab Winners. JP Morgan Chase.', array('target' => '_blank')); }}</li>
								<li><span>06/15/2015</span> {{ HTML::link('http://www.forbes.com/sites/carolinecenizalevine/2015/06/15/three-innovative-technology-companies-are-changing-the-lives-of-the-everyday-worker/', 'Technology Innovations Improving The Lives Of The Everyday Worker. Forbes.', array('target' => '_blank')); }}</li>
								<li><span>06/16/2015</span> {{ HTML::link('http://www.pymnts.com/exclusive-series/2015/ascend-and-real-time-risk-descent/#.VYBe1flVhBe', 'Ascend and Real-Time Risk Descent. PYMNTS.com', array('target' => '_blank')); }}</li>
								<li><span>06/16/2015</span> {{ HTML::link('https://www.supermoney.com/reviews/personal-loans/ascend-financial', 'Ascend Consumer Finance review. SuperMoney.com.', array('target' => '_blank')); }}</li>
								<li><span>07/10/2015</span> {{ HTML::link('http://www.americanbanker.com/gallery/nine-apps-to-help-the-underbanked-take-control-of-their-finances-1075219-1.html', 'Nine Apps to Help the Underbanked Take Control of Their Finances. The American Banker', array('target' => '_blank')); }}</li>
				 			</ul> 
				 		</div>
				 		<div class="clearfix"></div>
				 		<div class="row-fluid press-list">
				 			<h3>Press Releases:</h3>
				 			<ul>
				 				<li><span>04/14/2015</span>{{ HTML::link(asset('press/Ascend Consumer Finance release v15 FINAL.pdf'), 'Ascend Announces Launch', array('target' => '_blank')); }}</li>
				 				<li><span>06/02/2015</span>{{ HTML::link('http://www.prnewswire.com/news-releases/lendingtree-adds-ascend-consumer-finance-to-its-personal-loan-network-300092496.html', 'Ascend Announces LendingTree Partnership', array('target' => '_blank')); }}</li> 
				 			</ul>
					 	</div>
				 	</div>
				 </div>
			</div> 
		</div>
	</div>
</div>  -->
@stop
@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
@stop