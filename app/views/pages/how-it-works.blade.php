@section('content') 

{{ $contents }}


{{-- <div id="hiwPage" class="pages" style="margin-top: -20px"> 
	<div class="hiw-banner-container">
		<div class="wide text-center hiw-banner"> 
			<div class="container"> 
				<div class="row">
					<div class="col-sm-12">
						<div class="hiw-banner-text"><p>RateRewards benefits<br /> better borrowers</p>						Lower your interest up to 50% by <br> showing financial responsibility.
						</div>
					</div>
				</div>
			</div>
		</div> 
	</div> 

	<div class="hiw-wrapper-how content-wrapper ">
		<div class="container inner-container">
			<div class="row hiw-contents1">
				<h1 class="text-center">HOW TO EARN REWARDS</h1>
				<p>
					Each month, you will earn rewards based on the actions you take.  Your monthly reward will automatically reduce your monthly payment.
				</p>	
				<div class="row mothly-action">
					<h3 class="col-xs-12">Monthly Actions</h3>
					<p class="col-xs-9">
						Each month, we monitor your bank and credit report to look at specific behaviors: reducing debt,increasing savings, and spending on debit instead of credit. Each of these activities can earn a month reward that starts at <strong>2.5%</strong> and grows to <strong>10%</strong> based on how many months in a row you achieve the goal.
					</p>
					<div class="col-xs-3 text-center">{{HTML::image('img/hiw-calendar.png','',array('class' => ''))}}</div>
				</div>
				
				<div class="row">
					<div class="col-xs-3 text-center">
						<div class="hiw-table-display">
							{{HTML::image('img/hiw-money.png','',array('class' => ''))}}
						</div>
					</div> 					
					<div class="col-xs-9">
						<h3>&nbsp;One-Time Action</h3>
						<p>
						Although it's optional, you can pledge your auto title as collateral against the loan. This will earn a <strong>20%</strong> discount in each month.
						</p>
					</div>
				</div>
			</div>	
		</div>
	</div>
	<div class="hiw-wrapper-breakdown content-wrapper ">
		<div class="container inner-container">
			<div class="row hiw-contents2">
				<h3 class="section-title">Rewards Schedule</h3>
				<p>We calculate a reward percentage every month based on what your actions in the current and previous months. Some rewards increase as you do them for consecutive months.</p>
				<table class="table table-bordered table-hiw table-responsive"> 
					<tr>
						<td class="head-gray vbottom title" rowspan="2">Monthly Triggers</th>
						<td colspan="4" class="tc-white head-gray title">Monthly Discount on Interest Cost</th>
					</tr> 
					<tr class="mdic-headers">
						<td class="head-lgray text-center">1 Month</td>
						<td class="head-lgray">2 Months</td>
						<td class="head-lgray">3 Months</td>
						<td class="head-lgray">4 Months</td>
					</tr>
					<tr>
						<td>Reduce debt by $50 per month</td>
						<td>2.5%</td>
						<td>5.0%</td>
						<td>7.5%</td>
						<td>10.0%</td>
					</tr>
					<tr>
						<td>Save $50 per month</td>
						<td>2.5%</td>
						<td>5.0%</td>
						<td>7.5%</td>
						<td>10.0%</td>
					</tr>
					<tr>
						<td>Credit card spending less than $50</td>
						<td>2.5%</td>
						<td>5.0%</td>
						<td>7.5%</td>
						<td>10.0%</td>
					</tr>
					<tr>
						<td>All debts on time</td>
						<td colspan="4">3 months in a row required</td>
					</tr>
					<tr class="head-gray">
						<td colspan="5" class="title">One-Time Trigger</td>
					</tr>
					<tr>
						<td>Auto title collateral</td>
						<td colspan="4">20% each month</td>
					</tr>
			</table>
			</div>	
		</div>
	</div>
	<div class="hiw-wrapper-example content-wrapper ">
		<div class="container inner-container">
			<div class="row hiw-contents2">
			    <h3 class="section-title">Calculating the Monthly Reward Amount</h3>
			    <p>Your reward percentage is calculated by adding up your reward earned for each action in the month. This reward percentage is multiplied by your finance charge to calculate the amount of your monthly award. Your monthly payment is automatically reduced by this reward amount.</p>
				<h3 class="section-title">An Example</h3>
				<p>Mary has a RateReward loan with a base monthly payment of $210. Her payment is comprised of $110 of interest and $100 of principal. Let's see how her reward would be calculated based on her recent banking and credit history.</p>
				<table class="table table-bordered table-hiw-sample"> 
					<tbody>
						<tr>
							<td>Paid all accounts on time 3 months in a row</td>
							<td>Quality for monthly rewards</td>
						</tr>
						<tr>
							<td>Reduced total debt by $50 for 3 months in a row</td>
							<td>7.5%</td>
						</tr>
						<tr>
							<td>Spent less than $50 on credit 4 months in a row</td>
							<td>10%</td>
						</tr>
						<tr>
							<td>Did not increase savings by $50 this month</td>
							<td>0%</td>
						</tr>
						<tr>
							<td>Has collateralized her loan with an auto title</td>
							<td>20%</td>
						</tr>
						<tr class="total-percentage-reward">
							<td class="head-lgrayd"><strong>Total Reward Percentage</strong></td>
							<td class="head-lgrayd"><strong>37.5%</strong></td>
						</tr>
					</tbody>
				</table>
			 
				<div class="hiw-features-box">
					<div class="row-fluid  boxi">
						<div class="col-xs-4 col-sm-5">
							<span>Monthly Reward Amount</span><br />
							<span class="reward-amt">$41</span><br />
							37.5% * $110 Interest Cost
						</div>
						<div class="col-xs-4 col-sm-2 text-center hiw-arrow-wrapper">
							<div class="hiw-arrow">
								{{HTML::image('img/hiw-arrow.png','',array('class' => ''))}}
							</div>
						</div>
						<div class="col-xs-4 col-sm-5">
							<span>New Monthly Payment</span><br>
							<span class="reward-amt">$169 </span><br>
							$210 Base Payment - $41 Reward
						</div>
						<div class="clearfix"></div>
					</div> 
				</div>

			</div>	
		</div>
	</div>


</div>  --}}


@stop

@section('scripts')
{{ HTML::script( 'js/common.js');  }}
@stop

