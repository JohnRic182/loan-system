@section('content') 

{{	$contents	}}
<!-- <div id="faqsPage" class="pages rates-terms-wrapper"> 
	 
	

<div class="appl-title rterms-title">
    <div class="container">
    	<h1 class="text-center">Frequently Asked Questions</h1> 
    </div> 
</div>


	<div class="faqs-wrapper content-wrapper ">
		<div class="container inner-container">
			<div class="row">
				
			</div>
			<div class="row">
				
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">		
				@foreach ($faq as $key => $element) 
					  <div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="headingOne">
					      <h4 class="panel-title">
					        <a class="faq-name" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapse{{$key}}">
					          {{ $element['faq_name'] }} 
					        </a> 
					        <a id="toggle{{$key}}" class="pull-right toggleArrow" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}" aria-expanded="true" aria-controls="collapseOne" >
								<span class="glyphicon glyphicon-plus"></span>
							</a>
					      </h4>
					    </div> 
					    <div id="collapse{{$key}}" rel="{{$key}}" class="faq-ans panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$key}}">
					      <div class="panel-body">
					      	{{ $element['faq_desc'] }}
					      </div>
					    </div> 
					  </div> 
				@endforeach
				</div>

			</div> 
		</div>
	</div>
</div> --> 
@stop

@section('scripts')
{{ HTML::script( 'js/common.js');  }}
@stop

