@section('content') 
<div id="faqsPage" class="pages rates-terms-wrapper contact-us-page"> 
	 
	<h1 class="text-center">Terms & Condition</h1> 

	<div class="faqs-wrapper content-wrapper ">
		<div class="container inner-container">
			<div class="row">
				
			</div>
			<div class="row">
				
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">	

				{{-- ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++= --}}
					<h3 class="text-center">ASCENDLOAN.COM TERMS OF USE</h3>

					<b>Welcome to Ascend Consumer Finance</b>

					<p>These terms and conditions govern your use of the World Wide Web site owned by Ascend Consumer Finance, Inc. (“Ascend”) located at www.AscendLoan.com (the “Site”).  By using this website, you accept these terms and conditions in full.   If you disagree with these terms and conditions or any part of these terms and conditions, you must not use this website. </p>

					<p>In addition to these Terms of Use, you may enter into other agreements with us or others that will govern your use of the Service or related services offered by us or others. If there is any contradiction between these Terms of Use and another agreement you enter into applicable to specific aspects of the Service, the other agreement shall take precedence in relation to the specific aspects of the Service to which it applies. </p>

					<b>Changes to these Terms of Use</b>
					<p>We may make changes to these Terms of Use from time to time. If we do this, we will post the changed Terms of Use on the Site and will indicate at the top of this page the date the Terms of Use were last revised. You understand and agree that your continued use of the Service or the Site after we have made any such changes constitutes your acceptance of the new Terms of Use.</p>

					<b>Privacy</b>
					<p>Please review the Site's Privacy Policy. By using the Site or the Service, you are consenting to the posted Privacy Policy.  You consent to have your personal data transferred to and processed by the Site. In the case of phone based communication, we may listen to and/or record phone calls between you and our representatives without notice to you as permitted by applicable law. For example, we listen to and record calls for quality monitoring purposes.  Additionally, by using the Site, you acknowledge and agree that Internet transmissions are never completely private or secure. You understand that any message or information you send to the Site may be read or intercepted by others, notwithstanding our efforts to protect such transmissions.</p>

					<b>Web-related Information Collection and Cookies</b>
					<p>We use cookies on our web site, including session ID cookies, non-persistent cookies and persistent cookies. A cookie is a text file sent by a web server to a web browser, and stored by the browser that enables the web server to identify and track the web browser and load the pages according to a user’s preferences for that particular site, including the personalization of content. Cookies are also used to gather statistical data, such as which pages are visited, what is downloaded, the ISP’s domain name and country of origin, and the addresses of sites visited before and after coming to our site, as well as your click activity on the Site.  This data is aggregated for analysis to ensure proper Site functionality, navigation, usability and marketing tracking.  At no time do any of our cookies capture any personal information. This information is encrypted and no personal data about you is stored on our servers.</p>

					<p>We may send a cookie that can be stored by your browser on your computer’s hard drive. We may use the information we obtain from the cookie in the administration of our web site, to improve its usability and for evaluating our marketing effectiveness as described above. We may also use that information to recognize your computer when you visit our web site (if you select the "remember me on this computer" option), and to personalize our web site for you. Most browsers allow you to refuse to accept cookies. (For example, in Internet Explorer you can refuse all cookie by clicking "Tools", "Internet Options", "Privacy", and selecting "Block all cookies" using the sliding selector). Blocking cookies, however, can also have a negative impact on the usability of many web sites.</p>

					<p>Cookies may have long-term expiration dates, or none, and thus can stay in your hard drive for months at a time. While you can remove them as instructed by the help content in your chosen browser, disabling cookies may prevent you from using our site. Additionally, Ascend currently does not process, acknowledge or otherwise respond to any web browser’s "do not track" signal or other similar mechanism that indicates a request to disable online tracking of individual users who visit our website.</p>

					<p>We also use web beacons or pixel tags, which are tiny graphics, to track marketing efforts.  We use third-party tracking services that use cookies or other tracking technology to track non-personal information about visitors to our site in the aggregate (such as web page views, other sites visited and referral page information to track the success of our marketing efforts to bring people to our site as well as overall site performance). We use respective web beacons to gather information that lets us tune and improve our users' browsing experience, as well as track user responsiveness to various advertising campaigns and user activities. Ascend utilizes information such as "click stream" and transactional data, and uses all information collected in the aggregate, for anonymous purposes; in other words, Ascend does not associate any collected data with any users as individuals. Additionally, no personal or sensitive information is transmitted to advertising partners. However, certain partners may collect incoming IP addresses in connection with your visits to our site (for example, to avoid double-counting the number of new users who joined as a result of an advertising campaign) and may also track the other websites visited by users of these IP addresses.</p>

					<b>Intellectual Property</b>
					<p>This Site, the content, and all intellectual property pertaining to or contained on the Site (including but not limited to copyrights, patents, database rights, graphics, designs, text, logos, trade dress, trademarks and service marks) are owned by Ascend or third parties and all right, title and interest therein shall remain the property of Ascend and/or such third parties (collectively, the "Content"). All Content is protected by trade dress, copyright, patent and trademark laws, and various other intellectual property rights and unfair competition laws.</p>

					<b>License to use website</b>
					<p>You may view, download, copy, and print pages from the website solely for the purpose of conducting business with Ascend.</p>

					<p>
						You agree that you will not: 

						<ul>
							<li>Republish material from this website (including republication on another website);</li>
							<li>Sell, rent or sub-license material from the website;</li>
							<li>Show any material from the website in public;</li>
							<li>Reproduce, duplicate, copy or otherwise exploit material on this website for a commercial purpose
							Reverse engineer or reverse compile any of the service technology, including but not limited to, any Java applets associated with the service.</li>
							<li>Edit, modify, or create derivative content from any material on the website</li>
						</ul>
					</p>


					<b>Acceptable use</b>
					<p>
						You must not use the Site in any way that:
						<ul>
							<li>Accesses data that is not intended for you</li>
							<li>Invades the privacy of or obtains personal information about any customer or user of the Site</li>
							<li>Causes, or may cause, damage to the website or impairment of the availability or accessibility of the Site.</li>
							<li>Is in any way unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity.</li>
						</ul>
					</p>

					<p>You must not use the Site to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software.</p>

					<p>You must not conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to the Site without express written consent.</p>


					<b>Collection of Third-Party Financial Information</b>
					<p>In using the Site and Services, you may be asked to provide read-only access to the online websites of one or more of your financial accounts.  By providing such access and login credentials, you agree to the following:

						<p>
							<b>Provide Accurate Information.</b> You, the end user, agree to provide true, accurate, current and complete information about yourself and your accounts maintained at other web sites and you agree to not misrepresent your identity or your account information. You agree to keep your account information up to date and accurate.
						</p>

						<p>
							<b>Proprietary Rights.</b> You are permitted to use content delivered to you through the service only on the service. You may not copy, reproduce, distribute, or create derivative works from this content. Further, you agree not to reverse engineer or reverse compile any of the service technology, including but not limited to, any Java applets associated with the service. 
						</p>

						<p>
							<b>Content You Provide And Is Derived by Use of the Service.</b> You are licensing to Ascend and its service providers (“Service Provider”) any information, data, passwords, materials or other content (collectively, “Content”) you provide through or to the service. Ascend and Service Provider may use, modify, display, distribute and create new material using such Content to provide the service to you. Ascend and Service Provider may also use, sell, license, reproduce, distribute and disclose aggregate, non-personally identifiable information that is derived through your use of the Service. By submitting Content, you automatically agree, or promise that the owner of such Content has expressly agreed that, without any particular time limit, and without the payment of any fees, Ascend and Service Provider may use the Content for the purposes set out above. As between Ascend and Service Provider, Ascend owns your confidential financial information. 
						</p>

						<p>
							<b>Third Party Accounts.</b> By using the service, you authorize Ascend and Service Provider to access third party sites designated by you, on your behalf, to retrieve information requested by you, and to register for accounts requested by you. For all purposes hereof, you hereby grant Ascend and Service Provider a limited power of attorney, and you hereby appoint Ascend and Service Provider as your true and lawful attorney-in-fact and agent, with full power of substitution and re-substitution, for you and in your name, place and stead, in any and all capacities, to access third party internet sites, servers or documents, retrieve information, and use your information, all as described above, with the full power and authority to do and perform each and every act and thing requisite and necessary to be done in connection with such activities, as fully to all intents and purposes as you might or could do in person. YOU ACKNOWLEDGE AND AGREE THAT WHEN ASCEND OR SERVICE PROVIDER ACCESSES AND RETRIEVES INFORMATION FROM THIRD PARTY SITES, ASCEND AND SERVICE PROVIDER ARE ACTING AS YOUR AGENT, AND NOT THE AGENT OR ON BEHALF OF THE THIRD PARTY. You agree that third party account providers shall be entitled to rely on the foregoing authorization, agency and power of attorney granted by you. You understand and agree that the service is not endorsed or sponsored by any third party account providers accessible through the service.
						</p>

						<p>
							<b>Indemnification.</b> You agree to protect and fully compensate Ascend and Service Provider and their affiliates from any and all third party claims, liability, damages, expenses and costs (including, but not limited to, reasonable attorneys’ fees) caused by or arising from your use of the service, your violation of these terms or your infringement, or infringement by any other user of your account, of any intellectual property or other right of anyone.  You agree that Service Provider is a third party beneficiary of the above provisions, with all rights to enforce such provisions as if Service Provider were a party to this Agreement.
						</p>

						If credit is extended to an applicant, Ascend may consider the bank account information provided as part of the application process and as eligible for processing payments against with the applicant’s consent.  

					</p>

					<b>Electronic Communication</b>
					<p>When you visit the Site or send emails to us, you are communicating with us electronically and you consent to receive communications from us electronically to the extent permissible by law. We will communicate with you by email or by posting notices on this Site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing to the extent permissible by law. You agree that we may send emails to you for the purpose of advising you of changes or additions to this Site, about any of our products or services, or for such other purposes as we deem appropriate and as permissible by law.  Nothing within this clause, however, requires Ascend to proactively inform you of any such changes.</p>

					<b>No warranties</b>
					<p>
						This website is provided “as is” without any representations or warranties, express or implied.  Ascend makes no representations or warranties in relation to this website or the information and materials provided on this website.  

						Without prejudice to the generality of the foregoing paragraph, Ascend does not warrant that:

						<ul>
							<li>This website will be constantly available, or available at all; or</li>
							<li>The information on this website is complete, true, accurate or non-misleading.</li>
						</ul>

						Nothing on this website constitutes, or is meant to constitute, advice of any kind.
					</p>

					<b>Limitations of liability</b>
					<p>Except where prohibited by law, neither Ascend, nor it’s directors, officers, employees, agents contractors, successors, or assigns of each shall be liable to you (whether under the law of contract, the law of torts or otherwise) in relation to the contents of, or use of, or otherwise in connection with, this website:

						<ul>
							<li>for any indirect, special or consequential loss; or</li>
							<li>for any business losses, loss of revenue, income, profits or anticipated savings, loss of contracts or business relationships, loss of reputation or goodwill, or loss or corruption of information or data.</li>
						</ul>									


					Ascend’s maximum liability for all claims arising out of or relating to this Site or its content, whether in contract, tort, or otherwise shall be limited to the amount you paid to access this site.  These limitations of liability apply even if Ascend has been expressly advised of the potential loss.</p>

					<b>Indemnity</b>
					<p>By using the Site, you hereby indemnify Ascend and undertake to keep Ascend indemnified against any losses, damages, costs, liabilities and expenses (including without limitation legal expenses and any amounts paid by Ascend to a third party in settlement of a claim or dispute on the advice of Ascend’s legal advisers) incurred or suffered by Ascend arising out of any breach by you of any provision of these terms and conditions, or arising out of any claim that you have breached any provision of these terms and conditions.</p>

					<b>Breaches of these terms and conditions</b>
					<p>Without prejudice to Ascend’s other rights under these terms and conditions, if you breach these terms and conditions in any way, Ascend may take such action as Ascend deems appropriate to deal with the breach, including suspending your access to the website, prohibiting you from accessing the website, blocking computers using your IP address from accessing the website, contacting your internet service provider to request that they block your access to the website and/or bringing court proceedings against you.</p>

					<b>Severability</b>
					<p>If a provision of these terms and conditions is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.  If any unlawful and/or unenforceable provision would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect. </p>

					<b>Law and jurisdiction</b>
					<p>These Terms of Use will be governed by and construed in accordance with California Law, and any disputes relating to these terms and conditions will be subject to the exclusive jurisdiction of the courts of San Francisco County, California.</p>

					<p><b>DISCLAIMER OF WARRANTIES.</b> YOU EXPRESSLY UNDERSTAND AND AGREE THAT: 
YOUR USE OF THE SERVICE AND ALL INFORMATION, PRODUCTS AND OTHER CONTENT (INCLUDING THAT OF THIRD PARTIES) INCLUDED IN OR ACCESSIBLE FROM THE SERVICE IS AT YOUR SOLE RISK. THE SERVICE IS PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. ASCEND AND SERVICE PROVIDER EXPRESSLY DISCLAIM ALL WARRANTIES OF ANY KIND AS TO THE SERVICE AND ALL INFORMATION, PRODUCTS AND OTHER CONTENT (INCLUDING THAT OF THIRD PARTIES) INCLUDED IN OR ACCESSIBLE FROM THE SERVICE, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. </p>

					<p>ASCEND AND SERVICE PROVIDER MAKE NO WARRANTY THAT (i) THE SERVICE WILL MEET YOUR REQUIREMENTS, (ii) THE SERVICE WILL BE UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE, (iii) THE RESULTS THAT MAY BE OBTAINED FROM THE USE OF THE SERVICE WILL BE ACCURATE OR RELIABLE, (iv) THE QUALITY OF ANY PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU THROUGH THE SERVICE WILL MEET YOUR EXPECTATIONS, OR (V) ANY ERRORS IN THE TECHNOLOGY WILL BE CORRECTED. 
ANY MATERIAL DOWNLOADED OR OTHERWISE OBTAINED THROUGH THE USE OF THE SERVICE IS DONE AT YOUR OWN DISCRETION AND RISK AND YOU ARE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA THAT RESULTS FROM THE DOWNLOAD OF ANY SUCH MATERIAL. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM ASCEND OR SERVICE PROVIDER THROUGH OR FROM THE SERVICE WILL CREATE ANY WARRANTY NOT EXPRESSLY STATED IN THESE TERMS</p>

					<p><b>LIMITATION OF LIABILITY.</b> YOU AGREE THAT NEITHER ASCEND OR SERVICE PROVIDER NOR ANY OF THEIR AFFILIATES, ACCOUNT PROVIDERS OR ANY OF THEIR AFFILIATES WILL BE LIABLE FOR ANY HARMS, WHICH LAWYERS AND COURTS OFTEN CALL DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES, EVEN IF ASCEND OR SERVICE PROVIDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, RESULTING FROM: (i) THE USE OR THE INABILITY TO USE THE SERVICE; (ii) THE COST OF GETTING SUBSTITUTE GOODS AND SERVICES, (iii) ANY PRODUCTS, DATA, INFORMATION OR SERVICES PURCHASED OR OBTAINED OR MESSAGES RECEIVED OR TRANSACTIONS ENTERED INTO, THROUGH OR FROM THE SERVICE; (iv) UNAUTHORIZED ACCESS TO OR ALTERATION OF YOUR TRANSMISSIONS OR DATA; (v) STATEMENTS OR CONDUCT OF ANYONE ON THE SERVICE; (vi) THE USE, INABILITY TO USE, UNAUTHORIZED USE, PERFORMANCE OR NON-PERFORMANCE OF ANY THIRD PARTY ACCOUNT PROVIDER SITE, EVEN IF THE PROVIDER HAS BEEN ADVISED PREVIOUSLY OF THE POSSIBILITY OF SUCH DAMAGES; OR (vii) ANY OTHER MATTER RELATING TO THE SERVICE. 
</p>

			<p>Last Date Updated: 2015-02-27</p>
				
				</div>

			</div> 
		</div>
	</div>
</div> 
@stop

@section('scripts')
{{ HTML::script( 'js/common.js');  }}
@stop

