<!DOCTYPE html>
<html>
<head>
	 <meta charset="UTF-8">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge">
	 <meta name="viewport" content="width=device-width, initial-scale=1,max-scale=1">
	 <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon">
	 <link href='//fonts.googleapis.com/css?family=Open+Sans:700,400,600,300' rel='stylesheet' type='text/css'>
	 <title>Ascend</title>
	 {{HTML::style('css/bootstrap.min.css')}}
	 {{HTML::style('css/style.css')}}
	 <script> 
	 	if ( window.location.protocol == "https:"){
	 		var baseUrl = 'https://'+ location.hostname + '/'; 
	 	}else{
	 		var baseUrl = 'http://'+ location.hostname + '/'; 
	 	} 
	 </script>
	 <style>
		html, body{
			background: #F5F1F0;
			height: 100%;
		}
		header{
			background: #FFF;
			padding-bottom: 30px;
		}
		.content-wrapper{
			background: #FFF;
		}
	 </style>
</head>
<body>
	
	<header>
	  <nav class="navbar navbar-default navbar-static-top" role="navigation">
	        <div class="container"> 
	          <div class="navbar-header"> 
	            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	           </button>  
	            @if( isset($homepage) )
	              <a class="navbar-brand navbar-brand-logo" href="{{ getHompageRelativeUrl() }}"></a>
	            @else
	              <a class="navbar-brand navbar-brand-logo navbar-brand-logo2" href="{{ getHompageRelativeUrl() }}"></a>
	            @endif
	          </div> 
	          <div id="navbar" class="navbar-collapse collapse">
	              <ul class="nav navbar-nav pull-right hidden-xs">
	                <li><a href="#" class=""><i class="glyphicon glyphicon-earphone cta-link "></i> 800-497-5314</a></li>
	              </ul>  
	          </div> 
	  </nav> 
	</header>

	<div class="pages contact-us-page" style="height:100%;">
		<h1 class="text-center">Maintenance Mode</h1> 
		<div class="content-wrapper contact-us" style="height:100%;">
			<div class="container inner-container">
				 <p>Special one is currently under maintenance. We should be back shortly. Thank you for your patience.</p>
			</div>
		</div>
	</div> 
</body>
</html>