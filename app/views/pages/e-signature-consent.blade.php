@section('content') 
<div class="pages container inner-container">
	 
	<h1>E-Sign Disclosure and Consent ("E-Consent")</h1>

	<div class="content-wrapper">
		 
		@if( $consent )
			{{ html_entity_decode($consent->Consent_Detail_Txt) }}
			<p>&nbsp;</p>
			<p>Last Date Updated: {{ $consent->Validity_Start_Dt }}</p>
			
		@endif
 
	</div>
</div> 
@stop