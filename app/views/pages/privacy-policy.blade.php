@section('content') 

<style type="text/css">
/*	table td{
		border: solid thin #000;
	}

		table tr{
		border: solid thin #000;
	}*/
</style>

<div id="policyPage" class="pages rates-terms-wrapper contact-us-page"> 


	

	 
	<h1 class="text-center">ASCEND CONSUMER FINANCE, INC.
CONSUMER PRIVACY POLICY NOTICE</h1> 

	<div class="faqs-wrapper content-wrapper ">
		<div class="container inner-container">
		 
			<div class="row">
				
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">	

				<p>Effective date is date of revision</p>
				<table class="table table-bordered">
					<tr>
						<td><b>FACTS</b></td>
						<td>
							WHAT DOES ASCEND CONSUMER FINANCE, INC., DO WITH YOUR PERSONAL INFORMATION?
						</td>
					</tr>
				</table>

				<table class="table table-bordered">
					<tr>
						<td>Why?</td>
						<td>Financial companies choose how they share your financial information.  Federal law gives consumers the right to limit some but not all sharing.  Federal law also requires us to tell you how we collect, share, and protect your personal information. Please read this notice carefully to understand what we do.</td>
					</tr>
				</table>

				<table class="table table-bordered">
					<tr>
						<td>What?</td>
						<td>
							The types of personal information we collect and share depend on the product or service you have with us.  This information can include:
							<ul>
								<li>Social security number and income</li>
<li>Account balances, account transactions and payment history</li>
<li>Credit history and credit scores</li>
							</ul>
			When you are no longer our customer, we continue to share your information as described in this notice.
						</td>
					</tr>
				</table>

				<table class="table table-bordered">
					<tr>
						<td>How?</td>
						<td>All financial companies need to share customers' personal information to run their every day business. In the section below, we list the reasons financial companies can share their customers' personal information; the reasons Ascend Consumer Finance, Inc., chooses to share; and whether you can limit this sharing.
</td>
					</tr>
				</table>


				{{-- ==================/ --}}

				<table class="table table-bordered">
					<tr>
						<th>Reasons we can share
your personal information</th>
						<th>Does Ascend Consumer Finance, Inc., share?</th>
						<th>Can you limit this sharing?</th>
					</tr>

					<tr>
						<td><b>For our every day business purposes – </b>
such as to process your transactions, maintain your account(s), respond to court orders and legal investigations, or report to credit bureaus</td>
						<td>Yes</td>
						<td>No</td>
					</tr>
					<tr>
						<td><b>For our marketing purposes – </b>
to offer our products and services to you
</td>
						<td>Yes</td>
						<td>No</td>
					</tr>

					<tr>
						<td><b>For joint marketing with other financial companies  </b></td>
						<td>Yes</td>
						<td>No</td>
					</tr>

					<tr>
						<td><b>For our affiliates' every day business purposes - </b>
Information about your creditworthness</td>
						<td>No</td>
						<td>We don't share</td>
					</tr>

					<tr>
						<td><b>For non-affiliates to market to you</b></td>
						<td>No</td>
						<td>We don't share</td>
					</tr>

				</table>

				{{-- ==================/ --}}

				<table class="table table-bordered">
					<tr>
						<td>To limit our sharing</td>
						<td>Call us at 800-497-5314 or visit us online at AscendLoan.com</td>
					</tr>

					<tr>
						<td>Questions</td>
						<td>Call us at 800-497-5314 or email us at support@ascendloan.com</td>
					</tr>
				</table>


				{{-- ==================/ --}}

				<table class="table table-bordered">
					<tr>
						<th colspan="2">What we do</th>
					</tr>
					<tr>
						<td>How does Ascend Consumer Finance, Inc., protect my personal information?</td>
						<td>
							<p>To protect your personal information from unauthorized access and use, we use security measures that comply with federal law.  These measures include computer safeguards and secured files and buildings.</p>
							<p>We also restrict access to your information to those who need it to provide service to you.</p>
						</td>
					</tr>
					<tr>
						<td>How does Ascend Consumer Finance, Inc., collect my personal information?</td>
						<td>We collect your personal information, for example, when you:
							<ul>
								<li>  Apply for a loan</li>
<li>  Give us your income information;</li>
<li>  Provide employment information</li>
<li>  Give us your contact information</li>
<li>  Pay us by check</li>
							</ul>
We also collect your personal information from others, such as credit bureaus or other companies.
</td>
					</tr>
					<tr>
						<td>Why can't I limit all sharing?</td>
						<td>
							Federal law gives you the right to limit only:
							<ul><li>  Sharing for affiliates' every day business 
    purposes – information about your credit 
    worthiness</li>
<li>  Affiliates from using your information to 
    market to you</li>
<li>  Sharing for non-affiliates to market to you</li></ul>
State laws and individual companies may give you additional rights to limit sharing.

						</td>
					</tr>
				</table>
				{{-- ==================/ --}}

				<table class="table table-bordered">
					<tr>
						<th colspan="2">Definitions</th>
					</tr>
					<tr>
						<td>Affiliates</td>
						<td>Companies related by common ownership or control.  They can be financial and nonfinancial companies. Ascend Consumer Finance, Inc., has no affiliates.
</td>
					</tr>
					<tr>
						<td>Non-affiliates</td>
						<td>Companies not related by common ownership or control.  They can be financial and nonfinacial companies. Ascend Consumer Finance, Inc., does not share information with non-affiliates so they can market to you</td>
					</tr>
					<tr>
						<td>Joint marketing</td>
						<td>A formal agreement between non-affiliated financial companies that together market financial products or services to you.  Ascend Consumer Finance, Inc., doesn't jointly market</td>
					</tr>
				</table>

				{{-- ==================/ --}}


				<table class="table table-bordered">
					<tr>
						<th>Other important information</th>
					</tr>
					<tr>
						<td>Policy Changes:  The effective date of this policy is the revision date.  Updates or changes to this policy will be posted on this website with a new revision date.  Your continued use of the website will constitute your acceptance to any changes in this policy.</td>
					</tr>
				</table>

				<p>Last Date Updated: 2015-02-27</p>

				</div>

			</div> 
		</div>
	</div>
</div> 
@stop

@section('scripts')
{{ HTML::script( 'js/common.js');  }}
@stop

