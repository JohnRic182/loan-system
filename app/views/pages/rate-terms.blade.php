@section('content') 

{{ $contents }}  

	@if(false)

	<div class="appl-title rterms-title">
	    <div class="container">
	    	<h1 class="text-center"> Rates &amp; Terms </h1>
	    </div> 
	</div>
	<div id="application" class="container">
		<div id="rateTerms" class="pages box-wrapper"> 
			<div class="row">
				<div class="banner-adjustment">
					<div class="col-sm-12">
						<p class="text-center rt-title">Select your state to see rate terms that apply to you.</p>
					</div>
					<div class="input-group ratesTermsState "> 
						
						<button class="input-group-addon active cal" rel="california" >California</button>
						<button class="input-group-addon other" title="At this time we only offer loans in California" rel="other" data-toggle="tooltip" data-placement="top" >Other</button> 	

						<div class="state-select">
							<select>
								<option value="">Select your state</option>
								<option value="CA">California</option>
								<option value="UT">Utah</option>
								<option value="OR">Oregon</option>
								<option value="IL">Illinios</option>
							</select>	
						</div>

						

					</div>  
					<div class="rates-table"> 

						<div id="california" class="row table">  
							
							


							<div class="col-xs-6 nopad-left nopad-right amtfigures border-top border-right"> 
								<div class="col-sm-11 border-dashed col-sm-offset-1 nopad-left">
									<span>Loan Amount</span>
									<p>$2,600 - $15,000</p>
								</div>
								<div class="col-sm-11 border-dashed col-sm-offset-1 nopad-left">
									<span>APR</span>
									<p>27-36%</p>
								</div>
								<div class="col-sm-11 col-sm-offset-1 nopad-left">
									<span>Term</span>
									<p>36 Months</p>
								</div> 
							</div>

							


							<div class="col-xs-6 desc border-top"> 
								<ul>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>Get a low rate and payment</p>
									</li>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>Reduce interest cost up to 50% with RateRewards</p>
									</li>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>No origination fees</p>
									</li>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>Funds deposited next day</p>
									</li>
								</ul> 
							</div> 
	 						<div class="clearfix"></div>
							<div class="row-fluid license text-center border-top">
								<div class="col-xs-2">
									<a class="pull-left" target="_blank" href="{{ asset('img/Ascend CFLL License March2015.pdf');}} "><img src="{{ asset('img/contract.png')}}" alt=""  style="border: 1px solid #76bf5b; max-width:70px; width:100% "></a>	
								</div>

								<div class="col-xs-10 cfll">
									<p>Loans Made pursuant to a Department of Business Oversight <br>California Finance Lenders Law License.</p>
									<a class="cta-link" target="_blank" href="{{ asset('img/Ascend CFLL License March2015.pdf');}} ">VIEW LICENSE (PDF)</a>
								</div>
								
							</div> 
						</div> 


						<div id="other" class="row table">
							
							<div class="col-xs-6 nopad-left nopad-right amtfigures border-top border-right"> 
								<div class="text-center other"><p>At this time we only offer loans in California</p></div>
							</div>

							<div class="col-xs-6 desc border-top"> 
								<ul>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>Get a low rate and payment</p>
									</li>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>Reduce interest cost up to 50% with RateRewards</p>
									</li>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>No origination fees</p>
									</li>
									<li>
										<span class="icon icon-ok" aria-hidden="true"></span>
										<p>Funds deposited next day</p>
									</li>
								</ul> 
							</div> 
	 						<div class="clearfix"></div>
	 						<div class="row-fluid license text-center border-top">
								<div class="col-xs-2">
									<a class="pull-left" target="_blank" href="{{ asset('img/Ascend CFLL License March2015.pdf');}} "><img src="{{ asset('img/contract.png')}}" alt=""  style="border: 1px solid #76bf5b; max-width:70px; width:100% "></a>	
								</div>

								<div class="col-xs-10 cfll">
									<p>Loans Made pursuant to a Department of Business Oversight <br>California Finance Lenders Law License.</p>
									<a class="cta-link" target="_blank" href="{{ asset('img/Ascend CFLL License March2015.pdf');}} ">VIEW LICENSE (PDF)</a>
								</div>
								
							</div>  
						</div>  
					</div>  
					<hr>
					<div class="rate-disclaimer"> 
						<div class="row middle-content">
							<div class="col-sm-12">
								<p><span class="glyphicon glyphicon-info-sign"></span> Rates and terms of our loan will depend on the unique financial situation of the applicant, including credit history, employment, and other factors.</p>
							</div> 
						</div> 
					</div>   
				</div>
			</div> 
		</div>
		<div class="row check-your-rate middle-content"> 
			<div class="col-sm-12 text-center"> 
				<a href="{{ URL::to('/') }}" class="btn btn-cta"><span class="icon icon-lock-v2"></span>Check your rate</a>

				{{ HTML::image(asset('img/v2/wontimpact.jpg'), '', array('class' => 'wontimpact') )}}
			</div>
		</div>
		<p></p>
	</div>


	@endif


@stop
@section('scripts')
	{{HTML::script('js/common.js');}}
	<script type="text/javascript">

	$(document).ready(function() {

		$('[data-toggle="tooltip"]').tooltip();

		$('.ratesTermsState button').click(function(){

			$('.ratesTermsState button').removeClass('active');
			$(this).addClass('active');

			switch($(this).attr('rel')){
				case 'california':
					$('#other').fadeOut();
					$('#california').fadeIn();
				break;

				default:
					$('#other').fadeIn();
					$('#california').fadeOut();
				break;
			}

		});

	});
	
</script>
@stop
