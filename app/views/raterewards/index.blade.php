 <div class="rr-main-content">
		<div class="col-right">

            <div class="page-title">{{$cdbMonthReward}} {{$cdbRewardYear}} Rewards Statement</div>

                <div class="reward-list">
					<table class="rr-options">
						<tr>
							<th width="75%">Monthly Reward</th>
							<th width="25%"></th>
						</tr>
						<tr class="rr-row">
							<td class="rn" id="otp-trigger">Qualifier: 3 months on-time payments</td>

							<td align="right">{{$qualifier}}</td>
						</tr>
						<tr class="rr-row">
							<td class="rn" id="cdb-trigger">Reduce debt</td>
							<td align="right">{{$cdbPercent}}%</td>
						</tr>
						<tr class="rr-row">
							<td class="rn" id="tccs-trigger">Limit credit card spending</td>
							<td align="right" class="cdbSpendingColor">{{$tccsPercent}}%</td>

						</tr>
						<tr class="rr-row" id="sab-trigger-row">
							<td class="rn" id="sab-trigger">
								Increase savings
								@if( $adminView  != 1  )
									@if( $increaseSavings  == 1  )
										( Checking Account | <a href="#" class="rr-sab-link">Change</a> )
									@endif
								@endif
							</td>
							<td align="right">
							 	@if( $increaseSavings  == NULL || $increaseSavings == 0 )
									Not Enrolled
								@else
									{{  $savingsSummary['rewardPrct'] . '%' }}
								@endif 
							</td>
						</tr>
						<tr>
							<td>Total monthly reward</td>

							<td align="right">{{$totalMonthlyReward}}%</td>

						</tr>
					</table>
					<table class="rr-options">
						<tr>
							<th colspan="2">Recurring Reward</th>
						</tr>
						<tr class="rr-row">
							<td class="rn" id="at-trigger">Auto collateral</td>

							<td style="text-align: right">
								@if($atPercent>0)
									0.20
								@else
									Not Enrolled
								@endif
							</td>

						</tr>
					</table>
					<table>
						<tr>
							<th>Recurring Reward</th>

							<th align="right" style="text-align: right">{{$atPercent}}%&nbsp;&nbsp;</th>

						</tr>
					</table>
					<table class="rr-results">
						<tr>
							<td >{{$cdbMonthReward}} {{$cdbRewardYear}} finance charges</td>

							<td align="right">${{$finance}}</td>
						</tr>
						<tr>
							<td>Reward amount</td>
							<td align="right">${{$rda}}</td>
						</tr>
					</table>
				</div>    
				<div class="reward-details">
					<div class="otp">
						<div class="p1">
							<span class="details-title">&nbsp;On-Time Payment</span><br />
							<span>&nbsp;3 months in a row to qualify for rewards</span>
						</div>
						<div class="p2">
							<table width="100%">
								<tr align="center">
									<td class="ot-p2-sub1"><b>Less than 3 months</b></td>
									<td class="ot-p2-sub1"><b>3+ Months</b></td>
								</tr>								
								<tr align="center">
									<td><img src="/img/rr-rewardSmall.png" /></td>
									<td><img src="/img/rr-rewardBig.png" /></td>
								</tr>
								<tr align="center">
									<td>No Rewards</td>
									<td>Rewards</td>
								</tr>
							</table>
						</div>
						<div class="p3">
							<h4>Summary</h4>
							According to TransUnion, your<br /> most recent deliquency was:<br /><br />

							<span class="summary-result {{ $qualifierMonthsClass }}">
								@if( $qualifierMonths  > $qualifierLimit &&  $qualifierMonths  > 1 && $qualifierMonths!=999  )
									{{$qualifierMonths}} Months Ago								
								@endif
								@if( $qualifierMonths  == 1 )
									{{$qualifierMonths}} Month Ago
								@endif	
								@if( $qualifierMonths  == 999 )
									No reported delinquencies
								@endif	
							</span>		
						</div>
					</div>
					<div class="cdb">
						<div class="p1 debt">
							<span class="details-title">&nbsp;Reduce Debt</span><br />
							<span>&nbsp;Discount grows as you reduce debt<br />&nbsp;by $50 in consecutive months</span>
						</div>
						<div class="p2">
							<center><img src="/img/rr-cdbBar.png" />
								<b>&nbsp;&nbsp;Month 1&nbsp;&nbsp;&nbsp;Month 2&nbsp;&nbsp;&nbsp;Month 3&nbsp;&nbsp;&nbsp;Month 4</b>
							</center>
						</div>
						<div class="p3 cdb-p3">
							<table class="summary-table">
								<tr>
									<th colspan="2">Summary</th>
								</tr>
								<tr>
									<td align="left">Months hitting target</td>

									<td align="right" style="padding-right: 50px">{{$cdbHit}}</td>
								</tr>
								<tr>
									<td align="left">Reward %</td>
									<td align="right" style="padding-right: 50px">{{$cdbPercent}}%</td>

								</tr>
							</table>
							<div class="summary-table-limit">
								<table class="summary-table">
									<tr>
										<th align="left">Month</th>
										<th align="left">Balance</th>
										<th align="right" class="rr-reward-summary ">Change</th>
									</tr>
									@if($cdbRewardMonths>0)
										@foreach($cdbMonthsArr as $cdbm)
										<tr>
											<td align="left">{{ $cdbm['month'] }}</td>
											<td align="left">{{ $cdbm['balance'] }}</td>
											<td align="right" class="rr-reward-summary positive {{ $cdbm['color'] }}">{{ $cdbm['reduction'] }}</td>
										</tr>
										@endforeach
									@endif
								</table>
							</div>
						</div>
					</div>
					<div class="tccs">
						<div class="p1 card">
							<span class="details-title">&nbsp;Limit Credit Card Spending</span><br />
							<span>&nbsp;Discount grows as you spend less than<br />&nbsp;by $50 on credit in consecutive months</span>
						</div>
						<div class="p2">
							<center><img src="/img/rr-cdbBar.png" />
								<b>&nbsp;&nbsp;Month 1&nbsp;&nbsp;&nbsp;Month 2&nbsp;&nbsp;&nbsp;Month 3&nbsp;&nbsp;&nbsp;Month 4</b>
							</center>
						</div>
						<div class="p3 cdb-p3">
							<table class="summary-table">
								<tr>
									<th colspan="2">Summary</th>
								</tr>
								<tr>
									<td align="left">Months hitting target</td>

									<td align="left"  class="rr-reward-summary">{{$tccsHit}}</td>
								</tr>
								<tr>
									<td align="left">Reward %</td>
									<td align="left" class="rr-reward-summary">{{$tccsPercent}}%</td>

								</tr>
							</table>
							<div class="summary-table-limit">
								<table class="summary-table">
									<tr>
										<th align="left">Month</th>
										<th align="left" class="rr-reward-summary">Credit Spending</th>
									</tr>
									@if($tccsRewardMonths>0)
										@foreach($tccsMonthsArr as $tccsm)
										<tr>
											<td align="left">{{ $tccsm['month'] }} </td>
											<td align="left" class="rr-reward-summary positive {{ $tccsm['color'] }}">{{ $tccsm['credit'] }}</td>
										</tr>
									 	@endforeach
									@endif
								</table>
							</div>
						</div>
					</div>
					<div class="sab">
						<div class="p1 savings">
							<span class="details-title">&nbsp;Increase Savings</span><br />

							<span>&nbsp;Discount grows as you save $50 in<br />&nbsp;in consecutive months</span>

						</div>
						<div class="p2">
							<center><img src="/img/rr-cdbBar.png" />
								<b>&nbsp;&nbsp;Month 1&nbsp;&nbsp;&nbsp;Month 2&nbsp;&nbsp;&nbsp;Month 3&nbsp;&nbsp;&nbsp;Month 4</b>
							</center>
						</div>
						<div class="p3 cdb-p3" id="bankSavings">

							@if( $increaseSavings  == 1  )
								<table class="summary-table">
									<tbody>
										<tr>
											<th colspan="2">Summary</th>
										</tr>
										<tr>
											<td align="left">Months hitting target</td>
											<td align="left">{{ $savingsSummary['targetCnt'] }}</td>
										</tr>
										<tr>
											<td align="left">Reward %</td>
											<td align="left">{{ $savingsSummary['rewardPrct']}}%</td>
										</tr>
									</tbody>
								</table>
								<table class="summary-table">
									<tr>
										<th align="left">Month</th>
										<th align="left">Balance</th>
										<th align="left" class="rr-reward-summary">Increase</th>
									</tr>
									@foreach($savingsSummary['balance'] as $key => $val )
										<tr>
											<td align="left">{{ $val['monthYr'] }}</td>
											<td align="left">{{ $val['balanceAmt'] }}</td>
											<!-- <td align="left"  class="positive rr-reward-summary {{$yodleeClass}}">{{ $val['increaseAmt'] }}</td> -->
											<td align="left"  class="positive rr-reward-summary {{$yodleeClass}}">{{ $val['increaseAmt'] }}</td>
										</tr>
									@endforeach
									
								</table>
							@else
								@if( $adminView  == 1  )
									<button id="createYodleeAcctRR" disabled type="button" class="enrollNow enroll" data-toggle="modal" data-target="#linkBankAcctModal">ENROLL NOW ></button>
								@else
									<button id="createYodleeAcctRR" type="button" class="enrollNow enroll" data-toggle="modal" data-target="#linkBankAcctModal">ENROLL NOW ></button>
								@endif	
							@endif
						</div>
					</div>
					<div class="at">
						<div class="p1 auto">
							<span class="details-title">&nbsp;Auto Collateral</span><br />
							<span>&nbsp;Pledging your title as collateral will<br />reduce interest cost by 20% each month.</span>
						</div>
						<div class="p2">
							<center><img src="/img/rr-autoBar.png" /></center>
						</div>
						<div class="p3">

							@if($atPercent > 0)
								<h4>Summary</h4>
								Your pledged your auto title on:<br />
								<span class="summary-result">{{$atDate}}</span>
							@else								
								<img src="/img/rr-autocontact.png" /><p class="autocontact">Call us at <span class="summary-result">800-497-5314</span> to reduce<br />
								your interest cost by 20% today.</p>
							@endif							

						</div>
					</div>
				</div>                
        </div>
	</div>
	
	{{-- RateRewards Popup --}}
	@include('raterewards.bank')