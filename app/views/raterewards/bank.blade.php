<div class="modal fade yodlee-form" id="linkBankAcctModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    		<h2>Link Bank Accounts and Get a Better Rate</h2>
    	</div>
    	<div class="modal-body">
    		<div class="row link-bank-account-search">
					<div id="bank-wrapper-search" class="form-wrapper row" style="margin-left: 25px;">
						<iframe src="{{ $fastLinkUrl }}" frameborder="0" width="90%" height="100%" scrolling="no" style="min-height:550px;"></iframe> 
						<p class="text-center" style="float: none"><b>Click close to continue to the next step</b></p>
					</div>
			</div>
    	</div> 
		<div class="modal-footer">
			<div class="action-section row">
				<div class="col-sm-12">
					<div class="pull-left">
						<a href="#" class="cta-link rr-cta-link" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
					</div>
					<div class="pull-right">
						<button id="linkBankAccountReward" disabled="disabled" class="btn btn-cta">Done</button>
						<input id="putMFARequestForSiteBtn" class="pull-right btn btn-cta hide" type="button" value="Link Bank Account"/>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
    </div>
  </div>
</div>
<input type="hidden" class="uid" value="{{ $uid }}" />
<input type="hidden" class="lid" value="{{ $lid }}" />
