<table class="summary-table">
	<tbody>
		<tr>
			<th colspan="2">Summary</th>
		</tr>
		<tr>
			<td align="left">Months hitting target</td>
			<td align="left">{{ $savingsSummary['targetCnt'] }}</td>
		</tr>
		<tr>
			<td align="left">Reward %</td>
			<td align="left">{{ $savingsSummary['rewardPrct']}}%</td>
		</tr>
	</tbody>
</table>
<table class="summary-table">
	<tr>
		<th align="left">Month</th>
		<th align="left">Balance</th>
		<th align="left">Increase</th>
	</tr>
	@foreach($savingsSummary['balance'] as $key => $val )
		<tr>
			<td align="left">{{ $val['monthYr'] }}</td>
			<td align="left">{{ $val['balanceAmt'] }}</td>
			<td align="left" class="positive">{{ $val['increaseAmt'] }}</td>
		</tr>
	@endforeach
</table>