<!-- Start of SkyGlue Code -->
<script type="text/javascript">
	var _sgq = _sgq || [];
	_sgq.push(['setSgAccount', 'zoligaep']);

	setTimeout(function() {
		var sg = document.createElement('script'); sg.type = 'text/javascript'; sg.async = true;
		sg.src = ("https:" == document.location.protocol ? "https://dc" : "http://cdn0") + ".skyglue.com/sgtracker.js";
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(sg, s);
	}, 1);

  var dimensionValue = '<?php echo getUserSessionID(); ?>';
  ga('set', 'dimension1', dimensionValue);
</script>
<!-- End of SkyGlue Code -->