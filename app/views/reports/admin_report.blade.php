@if (count($applicant_data))

<table class="table table-hover table-bordered table-condensed table-striped" >
	<thead>
		<tr>

			@foreach ($columnsNames as $val)
				<th>{{$val}}</th>
			@endforeach

		</tr>
	</thead>
	<tbody class="table-hover">

		@foreach ($applicant_data as $obj)
			<tr>
				@foreach ($obj as $key => $element)
					@if ($key != 'row_num')
						<td>{{ $element }}</td> 
					@endif										
				@endforeach								
			</tr>
		@endforeach

	</tbody>
</table>
@else
<hr>
<div class="text-center">
	<h3>No Results Found.</h3>
</div>
@endif
