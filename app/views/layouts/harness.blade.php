<!Doctype HTML>
<html>
<head>
	{{-- Head Section --}}
	@include('includes.harness.head')
</head>
<body>  
	{{-- Loader --}}
	@yield('loader')

	<div id="main" >
		@yield('content')
	</div>
	@yield('scripts')
</body>
</html>