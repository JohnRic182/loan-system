<!Doctype HTML>
<html>
<head>
	{{-- Head Section --}}
	@include('includes.admin.head')
	
	{{ getFontScript() }}

	@if ( Config::get('system.Ascend.DebugMode') == 'true')
		@include('tracking.gtm-staging')
		@include('experiments.ga-tracking-staging')	
	@else
		@include('tracking.gtm-prod')
		@include('experiments.ga-tracking-prod')
	@endif

</head>
<body>  
	{{-- Loader --}}
	@yield('loader')

	<header>
		@include('includes.admin.header')
	</header>

	{{-- Header Section --}}
	@include('includes.admin.report_nav')
		
	{{-- @yield('slogan') --}}

	{{-- Footer Comment --}}
	<div id="main" class="container">
		@yield('content')
	</div>

	{{-- Footer Section --}}
	<footer>
		@include('includes.admin.footer')
	</footer> 
	
	@yield('scripts')
</body>
</html>