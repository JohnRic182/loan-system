<!Doctype HTML>
<html>
<head>
	{{-- Head Section --}}
	@include('includes.portal.head')

	@if ($debugMode == 'true')
    	@include('experiments.home-tracking-staging')
		@include('tracking.gtm-staging')
    @else
    	@include('experiments.home-tracking-prod')
		@include('tracking.gtm-prod')
    @endif

	@yield('optionalCSS')
	
</head>
<body class="appl">
	{{-- Loader --}}
	@yield('loader')

	@yield('bg')
	
	@include('includes.portal.header')

	@yield('slogan')

	{{-- Footer Comment --}}
	<div id="main" class="container-fluid">
		@yield('content')
	</div>

	{{-- Footer Section --}}
	<footer>
		@include('includes.footer')
	</footer>

	@yield('scripts')
</body>
</html>