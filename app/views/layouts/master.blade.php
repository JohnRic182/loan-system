<!Doctype HTML>
<html>
<head>
	{{-- Head Section --}}
	@include('includes.head')

	@if(isset($debugMode) && $debugMode == 'true')
	@else
		@if (!strpos(Request::url(), 'mobile/v'))
			@include('experiments.home-experiment-prod')
		@endif
	@endif

	@yield('optionalCSS')
	
</head>

@if( isset($homepage) )
	<body class="home">  
@else
	<body class="">  
@endif

@if(isset($debugMode) && $debugMode == 'true')
	@include('tracking.gtm-staging')
@else
	@include('tracking.gtm-prod')
@endif

	{{-- Loader --}}
	@yield('loader')

	@yield('bg') 
	{{-- Header Section --}}
	@include('includes.header')
		
	@yield('slogan')

	{{-- Footer Comment --}}
	<div id="main" class="container-fluid">
		@yield('content')
	</div>

	{{-- Footer Section --}}
	<footer>
		@include('includes.footer')
	</footer>

	@yield('scripts')
</body>
</html>