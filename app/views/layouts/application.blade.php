<!Doctype HTML>
<html>
<head>
	{{-- Head Section --}}
	@include('includes.head')

	@yield('optionalCSS')
	
</head>
<body class="appl">
@if ( isset($debugMode) && $debugMode == 'true')
	@include('tracking.gtm-staging')
@else
	@include('tracking.gtm-prod')
@endif

	{{-- Loader --}}
	@yield('loader')

	@yield('bg')
	
	@if( isset($nav) && $nav == false )
		@include('includes.header-application')
	@else 
		@include('includes.header')
	@endif

	@yield('slogan')

	{{-- Footer Comment --}}
	<div id="main" class="container-fluid">
		@yield('content')
	</div>

	{{-- Footer Section --}}
	<footer>
		@include('includes.footer')
	</footer>

	@yield('scripts')
</body>
</html>