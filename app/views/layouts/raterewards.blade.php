<!Doctype HTML>
<html>
<head>
	{{-- Head Section --}}
	@include('includes.raterewards.head')

</head>
<body>  
	{{-- Loader --}}
	@yield('loader')

	<div id="rr-main">
		@yield('content')
	</div>
	
	{{HTML::script('js/jquery.min.js')}}
	{{HTML::script('js/moment.js')}}
	{{HTML::script('js/bootstrap.min.js')}}

	@yield('scripts')
</body>
</html>