@section('content') 
<div class="container inner-container"> 
	 <h1>Error</h1>
	 <div class="content-wrapper">
		{{ pre($exception ) }}
	 </div>
	 <div class="action-section">
		<div class="pull-left">
			{{-- {{ HTML::link('verification/steps', 'Back',array('class' => 'cta-link')) }} --}}
		</div>
		<div class="pull-right"> 
			{{-- <button class="btn btn-cta">Done</button> --}}
		</div>
		<div class="clearfix"></div>
	</div>
	{{ Form::close() }}
</div>

@stop