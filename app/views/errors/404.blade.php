@section('content') 
<div class="container inner-container"> 
	 <h1>Page not found!</h1>
	 <div class="content-wrapper">
		
		<p>Woopsie Daisy!</p>

		<p>Looks like something went completely wrong! But don't worry - it can happen to the best of us, 
		- and it just happened to you.</p>

	 </div>
	 <div class="action-section">
		<div class="pull-left">
			{{-- {{ HTML::link('verification/steps', 'Back',array('class' => 'cta-link')) }} --}}
		</div>
		<div class="pull-right"> 
			{{-- <button class="btn btn-cta">Done</button> --}}
		</div>
		<div class="clearfix"></div>
	</div>
	{{ Form::close() }}
</div>

@stop