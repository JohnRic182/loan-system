@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Creating Account...</span>
	</div>
@stop
@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1>
	    		<span class="appl-step">1</span>
				<span class="step-title">Create your account <i>(step 1 of 3)</i></span>
			</h1>
	    </div> 
	</div>

	<div id="application" class="container">

		 {{-- Need to convert this into blade templating --}}
		{{ Form::open(array('url' => '/postRegister', 'method' => 'POST', 'id' => 'UserAccountForm','class' => 'form-horizontal validate' , 'role' => 'form')) }}

		{{-- iovation bb 3rd and 1st party --}}
		{{ Form::hidden('ioBB', '', array('id' => 'ioBB' )) }} 
		{{ Form::hidden('fpBB', '', array('id' => 'fpBB' )) }}

		{{-- credit quality--}}
		<input type="hidden" id="creditQuality" name="creditQuality" value="{{ $loan['creditquality'] }}">

		<div class="content-wrapper">
			<div class="loan-details box-wrapper">
				<div class="row">
					<div class="col-sm-8">
            <div class="credit-header">
							<span class="icon icon-edit"></span>
              <h2 style="float: left; width: 80%; margin-top: 22px;">Review Loan Details</h2>
            </div>
						<table class="table">
							<thead>
								<tr>
									<th class="loanAmountLabel">Loan Amount</th>
									<th></th>
									<th>Loan Term</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td> 
										<input type="text" class="loanAmountFormat asking-loan-price required" alt="Loan Amount" value="$ <?php if(isset($loan['loanAmt'])) echo number_format($loan['loanAmt'], 0);?>" required="required" min="2600" max="15000" tabindex="1" />
										<div class="slider-edit"> 
										<form class="form-inline" class="validate" id="slider-edit-form">
										  <div class="alert alert-danger" role="alert"></div>
										  <div class="slider-input">
										    <label class="sr-only" for="askingPriceInputAmount">Amount (in dollars)</label>
										    <div class="input-group">
										      <div class="input-group-addon">$</div>
										      <input type="text" class="form-control required number number-only" id="askingPriceInputAmount" placeholder="Loan Amount" alt="Loan Amount" value="<?php if(isset($loan['loanAmt'])) echo number_format($loan['loanAmt'], 0);?>">
										      <div class="input-group-addon">.00</div>
										    </div>
										  </div>
										  <div class="clearfix"></div>
										  	<a href="#" class="slider-edit-cancel pull-left">Cancel</a>
											<input type="button" value="Done" class="btn btn-cta pull-right btn-slider-update-loan-price"> 
										</form> 
										</div>										
									</td>
									<td class="registerDivider">|</td>
									<td style="padding-top: 6px;"> 36 Months</td>
								</tr>
							</tbody>
						</table>  
						<hr>
					 	@if (!$logged) 
							<div class="user-account">
                				<div class="credit-header">
									<span class="icon icon-account"></span>
									<h2 style="float: left; ">Create your account </h2>
									<span class="required-not">Required</span>
								</div>
								{{--Transfer this to one blade block --}}
								<div id="notification"></div> 
								<input type="hidden" id="loan-purpose" value="">
								<div class="row register-mobile-form">

									<div class="col-md-6">

										<div class="form-group nopad-right">
											<div class="col-sm-12">
												{{ Form::label('id', 'Email Address'); }}
											</div>
											{{-- {{ _pre($loan) }} --}}
											<div class="col-sm-12">
												{{ Form::email('email', isset($loan['Email_Id']) ? $loan['Email_Id'] : '', array( 'class' => 'form-control required email', 'alt' =>'Email Address',  'alpha_num' => '' , 'placeholder' => 'Enter your email address', 'tabindex' => 2) ) }}
												<input type="hidden" class="required validEmail inValidEmailAddress includeValidate" value="1" name="validEmail" alt="Validity of Email Address">
											</div>
										</div> 

										<div class="form-group nopad-right">
											<div class="col-sm-12">
												{{ Form::label('id', 'Password'); }}
											</div>
											<div class="col-sm-12">
												{{ Form::password('password', array('id'=> 'password', 'class' => 'form-control required password passwordFormat minimum8 maximum20','alt' =>'Password',  'alpha_num' => '', 'placeholder' => 'Enter a password', 'tabindex' => 4, 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'At least 8 characters, including one capital letter and one number, but no special characters.') ) }}
											</div>
										</div>


									</div>

									<div class="col-md-6 mv-wrapper">
										
										<div class="register-mobile-how form-group  mv-bot">
											{{ Form::label('id', 'How Will You Use Your Loan  ', array('class' => 'col-sm-12  control-label')); }}
											<div class="col-sm-12">  
												{{ Form::select('loanPurposeArr', $loanPurposeArr , isset($loan['loanPurposeId']) ? ($loan['loanPurposeId']) : 0, array('class' => 'form-control required', 'id' => 'loanPurposeArr' ,'alt' => 'Loan Purpose', 'tabindex' => 3)); }}
											</div>
										</div>

										<div class="form-group mv-top"> 
											<div class="col-sm-12 ">
												{{ Form::label('id', 'Confirm Password'); }}
											</div>
											<div class="col-sm-12 ">
												{{ Form::password('confirmPassword', array( 'class' => 'form-control required matches|password', 'alt' =>'Confirm Password', 'alpha_num' => '', 'placeholder' => 'Re-enter a password', 'tabindex' => 5 ) ) }}
											</div>
										</div> 

									</div>

									
									{{ Form::hidden('User_Role_Id', $userRoleId ) }}
									{{ Form::hidden('uid', $uid ) }}
 
								</div>  
								<hr> 
								<div class="form-group row consent"> 
								
									<div class="col-sm-12">
										<input type="checkbox" name="smsConsent" class="required" alt="Agreement" tabindex="6">
										<div class="consentlabel">
											I consent to the {{ HTML::link('consent/eSignature', 'electronic communications agreement', array('target' => '_blank') ); }} 
										</div>										
									</div>

									<div class="col-sm-12">
										<input alt="Text/SMS Consent" style="float:left; margin-right: 4px; " name="agreementConsent" type="checkbox"  class="required" value="" tabindex="7">
										<div class="consentlabel">
											I agree to the {{ HTML::link('consent/creditPull', 'Credit Report Pull Agreement', array('target' => '_blank') ); }} , {{ HTML::link('terms', 'Terms of Use', array('target' => '_blank') ); }} , {{ HTML::link('privacyPolicy', 'Privacy Policy', array('target' => '_blank') ); }} and to receive calls and {{ HTML::link('consent/sms', 'SMS',  array('target' => '_blank') ); }} text messages.
										</div>
									<ul class="ca-residents">
										<li></span>{{ (isset($stateConfig['register.license_text'])) ? $stateConfig['register.license_text'] : ""; }}
										</li>
									</ul>
									</div>
								</div>

								<div class="clearfix"></div>
							</div> 
						@else
							<div class="user-account">
								<div id="notification"></div>
                <div class="credit-header">
									<span class="icon icon-account"></span>
                  <h2 style="float: left;">Update your application</h2>
									<span class="required-not">Required</span>
								</div>
								<div class="form-group col-sm-6">
									{{ Form::label('id', 'How Will You Use Your Loan  ', array('class' => 'col-sm-12 control-label')); }}
									<div class="col-sm-12">  
										{{ Form::select('loanPurposeArr', $loanPurposeArr , isset($loanPurposeId) ? $loanPurposeId : 0 , array('class' => 'form-control required', 'id' => 'loanPurposeArr' ,'alt' => 'Loan Purpose', 'tabindex' => 2)); }}
									</div>
								</div> 
								<div class="clearfix"></div>  
							</div>
						@endif

					</div>
					<div class="col-sm-4">
						@include('includes.sidebar')
					</div>
				</div>
				
			</div> 
		</div>

		<div class="row">
			<div class="action-section col-sm-8" >
				
				<div class="col-sm-4 .mv-bot">
					<a href="#" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
				</div>

				<div class="col-sm-8 mv-top">
					@if (!$logged)
						{{ Form::button('Agree and Continue <span class="glyphicon glyphicon-chevron-right"></span>', array('type' => 'submit', 'class' => 'btn btn-cta', 'id' => 'UserAccountFormSubmit', 'tabindex' => 8) ) }}
					@else
						{{ Form::button('Proceed to Credit Check <span class="glyphicon glyphicon-chevron-right"></span>', array('class' => 'btn btn-cta proceed', 'id' => 'submit', 'tabindex' => 3) ) }}
					@endif 
			</div>

			</div>
		</div>

		{{ Form::close() }}
	</div>

@stop

@section('scripts')
	{{ HTML::script( 'js/jquery.typing-0.2.0.min.js');  }}
	{{ HTML::script( 'js/jquery.maskedinput.min.js');  }}	
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/validate.js'); }}
	{{ HTML::script( 'js/user.js');  }}
	{{ HTML::script( 'js/register.js');  }}

	<!-- ================BEGIN iovation scripts ================ -->


	<script language="javascript">
	// io_bbout_element_id should refer to the hidden field in your form that contains the blackbox
		var io_bbout_element_id          = 'ioBB';

		// io_install_stm indicates whether the ActiveX object should be downloaded. The io_stm_cab_url
		// should reference your signed local copy of the ActiveX object
		var io_install_stm               = false;
		var io_exclude_stm               = 12;
		var io_install_flash             = false;
		var io_enable_rip                = true;

		// uncomment any of the below to signal an error when ActiveX or Flash is not present
		var io_flash_needs_update_handler = "";
	</script>

	@if($ioTestMode)
		<script language="javascript" src="https://ci-mpsnare.iovation.com/snare.js"></script>
	@else
		<script language="javascript" src="https://mpsnare.iesnare.com/snare.js"></script>
	@endif

	<script language="javascript">var fp_bbout_element_id = "fpBB";</script>
	<script language="javascript" src="{{ asset('/js/iojs/static_wdp.js'); }}"></script>
	<script language="javascript" src="{{ url('/iojs/4.1.1/dyn_wdp.js') }}"></script>

	<!-- ================END iovation scripts ================ -->

@stop