@section('content') 
	<div class="appl-title">
	    <div class="container">
			@if( $result = 'success')
				<h1>Thank you for verifying your email</h1> 
			@else
				<h1>Sorry, we can't verify your email</h1> 
			@endif 
	    </div> 
	</div>
	<div id="application" class="container"> 		
		<div class="approval-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-8">
					<br>
					<br>
					<p>Your email account has been <b>verified</b>.</p>
					<br> 
					<br>
					<br>
					<p class="text-center">
						<a href="{{ URL::to('verification/steps') }}" class="btn btn-cta" style="font-size: 22px;padding: 10px 51px;text-transform: uppercase;font-weight: bold;"> Proceed </a>
					</p>
				 
		 		    <br> 
		 		    <br>
					<div class="row">
						{{-- <div class="col-sm-6 text-center">
							 <a href="/">{{HTML::image(asset('img/logoBig.png'));}}</a>
						</div>

						<div class="col-sm-6">
							<div>
								<h2>Questions?</h2>
								<p>Call us at <br>
									800-497-5314 <br>
									<a href="#">questions@ascendloan.com</a>
								</p>
							</div>
						</div> --}}
					</div> 
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
		</div> 
		<div class="action-section row">
			<div class="col-sm-8">
				<div class="pull-left">
					&nbsp;
				</div>
				<div class="pull-right">
					 &nbsp;
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
 	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
@stop
