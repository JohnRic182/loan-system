{{HTML::style('css/bootstrap-datepicker.min.css')}}

@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop
@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1>
	    		<span class="appl-step">2</span>
				<span class="step-title check-title">Provide your credit Information <i>(step 2 of 3)</i></span>
			</h1>
	    </div> 
	</div>

	<div id="application" class="container"> 
		{{ Form::open(array('url' => '/postCreditCheck', 'method' => 'POST', 'id' => 'CreditCheckForm','class' => 'form-horizontal validate' , 'role' => 'form')) }}
		
		{{-- iovation blockbox --}}
		@if ( Session::has('fpBB') )
			{{ Form::hidden('ioBB', $ioBB, array('id' => 'ioBB' )) }} 
			{{ Form::hidden('fpBB', $fpBB, array('id' => 'fpBB' )) }}
		@endif

		<div class="content-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-8" id="cc-mobile-bg">
					<div class="credit-header">
						<span class="icon icon-search cc-search-icon"></span>
						<h2 style="float: left;">						
							Credit Check Details						
						</h2>
						<span class="required-not">Required</span>
					</div>
					{{--Transfer this to one blade block --}}
					<div id="notification"></div>  
					<div class="row"> 
						<div class="form-group col-sm-6">
							{{ Form::label('id', 'First Name  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12"> 
								{{ Form::hidden('uid', $uid, array('class' => 'form-control uid' ) ) }}
								{{ Form::text('firstName', $firstName , array('placeholder' => '', 'class' => 'form-control required alpha_num','alt' => 'First Name' ) ) }}
							</div> 
						</div>

						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Last Name  ' , array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12">
								{{ Form::text('lastName', $lastName, array('placeholder' => '', 'class' => 'form-control required' ,'alt' => 'Last Name')) }}
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Primary Phone Number  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12"> 
								{{ Form::text('phoneNumber', $homePhoneNumber, array('placeholder' => '###-###-####', 'class' => 'form-control required alpha_num PhoneNr' ,'alt' => 'Primary Phone Number', 'id' => 'primaryPhoneNum') ) }}
							</div> 
						</div>

						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Seconday PH. / Emergency Contact  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12"> 
								{{ Form::text('mobilePhoneNr', $mobilePhoneNumber, array('placeholder' => '###-###-####', 'class' => 'form-control required alpha_num PhoneNr' ,'alt' => 'Secondary Phone Number', 'id' => 'mobilePhoneNr') ) }}
							</div> 
						</div>

					   <div class="clearfix"></div>
						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Address  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12"> 
								{{ Form::text('address', $streetAddress1, array('placeholder' => 'Enter street', 'id' => 'address', 'class' => 'form-control required alpha_num' ,'alt' => 'Address', 'data-toggle' => 'tooltip', 'title' => 'Please use your current physical residence address. PO Boxes, mail reception services, and temporary addresses such as hospitals or hotels will not be accepted.') ) }}
							</div>
						</div>

						<div class="form-group col-sm-6">
							{{ Form::label('id', 'City  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12">
								{{ Form::text('city', $city, array('placeholder' => 'Enter city', 'id' => 'city', 'class' => 'form-control required alpha_num','alt' => 'City') ) }}
							</div>
						</div>
						
						 <div class="clearfix"></div>
						 
		 				<div class="form-group col-sm-6">
							{{ Form::label('id', 'State  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12">  
								{{ Form::select('state', $states , $state, array('class' => 'form-control required', 'id' => 'state' ,'alt' => 'State')); }}
							</div>
						</div>

						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Zip  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12">
								{{ Form::text('zip', $zip, array('placeholder' => 'Enter zip code', 'class' => 'form-control required alpha_num' , 'id' => 'zip', 'alt' => 'Zip code', 'maxlength' => '10') ) }}
							</div>
						</div>
						
						 <div class="clearfix"></div>						     		    

		     		   
						<div class="form-group ssn col-sm-6">
							{{ Form::label('id', 'Social Security Number  ', array('class' => 'col-xs-12 control-label')); }}

							<div class="clearfix"></div>

							<div class=" col-xs-12">
								{{ Form::text('ssn', $ssn, array('id' => 'ssn', 'placeholder' => '###-##-####', 'class' => 'form-control required alpha_num number-only', 'maxlength' => '12' ,'alt' => 'Social Security Number') ) }}
							</div> 

						</div>

						<div class="form-group col-sm-6">
							<div class="col-sm-12">
								<div class="cc-secure-icon pull-left"></div>
								<span class="sslText">We use 256-bit SSL encryption to protect and safeguard your data</span></p>
							</div>
						</div>

						<div class="clearfix"></div>

						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Date Of Birth  ', array('class' => 'col-xs-12 control-label')); }}
							<div class="row-fluid dob-div">
								<div class="col-xs-5 nopad-right">
									{{ Form::select('month', $months , (isset($m)) ? $m : "" , array('class' => 'form-control birthdate required padd-fix', 'id' => 'month' ,'alt' => 'Month' )); }}
								</div>
								<div class="col-xs-3 nopad-right">
									{{ Form::select('day', $days , (isset($d)) ?$d : "" , array('class' => 'form-control birthdate required padd-fix', 'id' => 'day' ,'alt' => 'Day')); }}
								</div>
								<div class="col-xs-4">
									{{ Form::select('year', $years , isset($y) ? $y  : ( date('Y') - 17 ), array('class' => 'form-control birthdate required padd-fix', 'id' => 'year' ,'alt' => 'Year')); }}
								</div>
							</div> 
							{{ Form::hidden('age', $age, array('class' => 'required valid-age', 'id' => "age" ) ) }} 
						</div>

					</div>  
					<hr> 
					<div class="credit-header">

						<span class="icon icon-info cc-icon-info"></span>
						<h2 style="float: left;">	
							<!-- <span class="icon icon-info"></span> -->
							Financial Information
							<!-- <span class="required-not">Required</span> -->
						</h2>
						<span class="required-not">Required</span>
					</div>

					{{-- credit quality--}}
					<input type="hidden" id="creditQuality" name="creditQuality" value="{{ $creditQuality }}">
					
					<div class="row">  
						<div class="form-group col-sm-6">  
							<div class="col-xs-12"> 
								<label for="" class="control-label">
			     		    		Gross Income
			     		    		<!-- <span data-toggle="popover" data-placement="right" title="Gross Income is your income each month or year BEFORE your taxes and deductions. Alimony, child support, or separate maintenance payments need not be revealed if you do not wish to have it considered as a basis for repaying the loan" class="rwar glyphicon glyphicon-question-sign"></span>  -->
			     		    		<div data-placement="right" title="Gross income is your income each month or year BEFORE your taxes and deductions.  This may include salary, investment income, social security or pension income" class="rwar cc-question-icon pull-right"></div>
			     		    	</label>
			     		    </div>
			     		    <div class="col-xs-6">	
								{{ Form::text('annualGrossIncome', $annualGrossIncome, array('placeholder' => '$', 'class' => 'form-control required number-only','alt' => 'Gross Income', 'id' => 'annualGrossIncome' ) ) }} 
							</div>
							<div class="col-xs-6">
								<!-- <label for="" class="control-label" style="color:#FFF;">Gross Income*</label> -->
								{{ Form::select('term', array('Annual', 'Monthly') , 'Annual', array('class' => 'form-control  padd-fix', 'id' => 'term' ,'alt' => 'term')); }}
							</div> 
						</div>
						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Current Employment Status  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12">
								{{ Form::select('employeeStatus', $empStatus, isset($employeeStatus) && $employeeStatus != '' ? $employeeStatus : 2 , array('placeholder' => 'select', 'class' => 'form-control required alpha_num','alt' => 'Current Employment Status' ) ) }}
							</div>
						</div>
		     		    <div class="clearfix"></div> 

						<div class="form-group col-sm-6">
							{{ Form::label('id', 'Housing Situation  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12">  
								{{ Form::select('housingSit', $housingSit , $rentOwn, array('class' => 'form-control required', 'id' => 'housingSit' ,'alt' => 'Housing Situation')); }} 
							</div>
						</div> 
						<?php $rentDisplay = ($rentOwn == 1 ) ? 'block' : 'none'; ?> 
						<div class="form-group col-sm-6" id="rentAmountDiv" style="display: {{ $rentDisplay }}">
							{{ Form::label('id', 'Monthly Rent  ', array('class' => 'col-sm-12 control-label')); }}
							<div class="col-sm-12"> 
								{{-- disable file if rentOwn value = 1 --}}
								<?php $disabledRent = ($rentOwn != 1)? "disabled" : "";  ?>							
								{{ Form::text('rentAmount', $rent_amount, array('placeholder' => '$', 'id' => 'rentAmount', 'class' => 'form-control rentAmount confirmRentAmount required number-only '.$disabledRent  ,'alt' => 'Monthly Rent', "$disabledRent" => '' ) ) }}
							</div>
						</div>
						{{-- Own With Mortgage --}}
						<div id="mortage" class="{{ ($rentOwn != 2)  ? 'hide' : '' }}">
					  		<div class="form-group col-sm-6">
								<div class="col-sm-12">
									{{ Form::label('id', 'Monthly Mortgage'); }}
								</div>
								<div class="col-sm-12"> 
									{{ Form::text('monthlyMortage', '', array('alt' => 'Monthly Mortgage' ,'class' => 'form-control required numberOnly greaterThanZero', 'id' => 'monthlyMortgageAmt', 'placeholder' => '$')) }} 
								</div>
							</div>      		    
			     		    <div class="clearfix"></div> 
			     		    <div class="form-group col-sm-6"></div>
						</div>

						<?php $housingNotiDisplay = ($rentOwn != 1 )? 'block' : 'none'; ?>
						<div class="form-group col-sm-6" style="display:{{ $housingNotiDisplay }}" id="housingSitNotification">
							<div class="col-sm-12 nopad-right">
								<div class="innerText">{{ $housingSitNotification }}</div>
							</div>											
						</div>
		     		    <div class="clearfix"></div>
						 
	     		   	 	<div id="contactInfoDiv" class="{{ ($rentOwn != 4) ? 'hide' : ''}}">
			     		    <div class="form-group col-sm-6 nopad-right">
								<div class="col-sm-12">
									{{ Form::label('id', 'Contact Name'); }}
								</div>
								<div class="col-sm-12">
									@if($rentOwn == 4)
									{{ Form::text('contactName', $contactName, array('alt' => 'Contact Name' ,'class' => 'form-control required', 'id' => 'contactName')) }}
									@else
									{{ Form::text('contactName', $contactName, array('alt' => 'Contact Name' ,'class' => 'form-control required', 'id' => 'contactName', 'disabled' => '')) }}
									@endif
								</div>
							</div>

							<div class="form-group col-sm-6 nopad-right"> 
								<div class="col-sm-12">
									{{ Form::label('id', 'Phone'); }}
								</div>
								<div class="col-sm-12">

									@if($rentOwn == 4)
									{{ Form::text('contactPhoneNr', $contactPhoneNr , array('placeholder' => '###-###-####', 'class' => 'form-control required alpha_num' ,'alt' => 'Phone Number', 'id' => 'contactPhoneNr') ) }}
									@else
									{{ Form::text('contactPhoneNr', $contactPhoneNr , array('placeholder' => '###-###-####', 'class' => 'form-control required alpha_num' ,'alt' => 'Phone Number', 'id' => 'contactPhoneNr', 'disabled' => 'disabled') ) }}
									@endif

								</div>
							</div> 					     		    
			     		    <div class="clearfix"></div> 
		     		    </div>
		     		    <p>&nbsp;</p>
			     		</div> 
				</div>
				<div class="col-sm-4 cc-questions-box">
					@include('includes.sidebar')
				</div>
			</div>
		</div>

		<div class="action-section row">
			<div class="col-sm-8">
				<div class="pull-left pull-left-mobile">
					<a href="#" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
				</div>
				<div class="pull-right">
					<button type="submit" class="btn btn-cta btn-mobile">CONTINUE <span class='glyphicon glyphicon-chevron-right'></span></button>
				</div>
				<div class="clearfix"></div>
			</div>
		</div> 

		{{ Form::close() }}
	</div>
	
	<!-- Modal -->
	<div class="modal fade" id="ra-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	{{-- 
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Modal title</h4>
		</div> 
	--}}
	      <div class="modal-body">
	      	Our real estate rental estimates indicate that there may be an error with the information you have provided.  Please confirm your rent amount is correct.  You may be asked to provide proof of rental amount.
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default btn-cta-default btn-back">BACK</button>
	        <button type="button" class="btn btn-cta btn-proceed">PROCEED</button>
	      </div>
	    </div>
	  </div>
	</div>
	
 	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script('js/bootstrap.min.js'); }}
	{{ HTML::script( 'js/jquery.maskedinput.min.js');  }}
	{{-- {{ HTML::script( 'js/user.js');  }} --}}
	{{ HTML::script( 'js/application.js');  }}

	<script type="text/javascript">

		$(document).ready(function(){
			$("#primaryPhoneNum").mask("999-999-9999");
			$("#mobilePhoneNr").mask("999-999-9999");
			$("#ssn").mask("999-99-9999");
			$("#contactPhoneNr").mask("999-999-9999");

			$("#annualGrossIncome").tooltip({
				'trigger':'manual', 
				'title': 'Alimony, child support, or separate maintenance payments need not be revealed if you do not wish to have it considered as part of your income.', 
				'placement' : 'right'
			}).focus(function(){
				var state = $('#state').val();
				if(state == 'CA')
					$(this).tooltip('show')
			}).blur(function(){ $(this).tooltip('hide'); });

			//Calculate Age
			var age = CommonObj.calculateAge(
				$('#month').val()
				, $('#day').val()
				, $('#year').val()
			);

			$('#age').val(age);
		});
	</script>

@stop