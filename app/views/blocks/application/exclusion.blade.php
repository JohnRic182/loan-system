<h1>Exclusion and Scoring Test Harness</h1>
        <style>
            input{ width: 70px; }
        </style>	
	<div class="row" style="background: white; min-height: 300px;">
		<div id="leftpanel" style="border-right: solid 1px black;min-height: 300px;" class="col-sm-4">
			<br /><br />
			<table width="100%">
			<tr><td>EMP_STAT</td><td><input type="text" value="{{$Employment_Status_Desc}}" class="EMP_STAT" /><br /></td></tr>
			<tr><td>AI</td><td><input type="text" value="{{$Annual_Gross_Income_Amt}}" class="AI" /><br /></td></tr>
			<tr><td>EXST_LN</td><td><input type="text" value="{{$EXST_LN}}" class="EXST_LN" /><br /></td></tr>
			<tr><td>LN_DECL</td><td><input type="text" value="{{$LN_DECL}}" class="LN_DECL" /><br /></td></tr>
			
            <tr><td>G094</td><td><input type="text" value="{{$G094}}" class="G094" /><br /></td></tr>
			<tr><td>G093</td><td><input type="text" value="{{$G093}}" class="G093" /><br /></td></tr>
			<tr><td>G083</td><td><input type="text" value="{{$G083}}" class="G083" /><br /></td></tr>
			<tr><td>G064</td><td><input type="text" value="{{$G064}}" class="G064" /><br /></td></tr>
			<tr><td>G071</td><td><input type="text" value="{{$G071}}" class="G071" /><br /></td></tr>
			<tr><td>G057</td><td><input type="text" value="{{$G057}}" class="G057" /><br /></td></tr>
			<tr><td>BC31</td><td><input type="text" value="{{$BC31}}" class="BC31" /><br /></td></tr>
			<tr><td>BR33</td><td><input type="text" value="{{$BR33}}" class="BR33" /><br /></td></tr>
			<tr><td>RE33</td><td><input type="text" value="{{$RE33}}" class="RE33" /><br /></td></tr>
			<tr><td>MT33</td><td><input type="text" value="{{$MT33}}" class="MT33" /><br /></td></tr>
			<tr><td>AT20</td><td><input type="text" value="{{$AT20}}" class="AT20" /><br /></td></tr>
			<tr><td>AT11</td><td><input type="text" value="{{$AT11}}" class="AT11" /><br /></td></tr>
			<tr><td>BC06</td><td><input type="text" value="{{$BC06}}" class="BC06" /><br /></td></tr>
			<tr><td>G098</td><td><input type="text" value="{{$G098}}" class="G098" /><br /></td></tr>
			
            <tr><td>TOTAL_NSF</td><td><input type="text" value="{{$Total_NSF_Cnt}}" class="TOTAL_NSF" /><br /></td></tr>
			<tr><td>Days_SINCE_NSF</td><td><input type="text" value="{{$Days_Since_Last_NSF_Cnt}}" class="Days_since_NSF" /><br /></td></tr>
			<tr><td>MO_DEPOSIT_TO_AVERAGE</td><td><input type="text" value="{{$Avg_Monthly_Deposit_Amt}}" class="MO_DEPOSIT_TO_AVERAGE" /><br /></td></tr>
			<tr><td>AVG_BAL_PMT_DATE</td><td><input type="text" value="{{$Avg_Bal_On_Pmt_Dt_Amt}}" class="AVG_BAL_PMT_DATE" /><br /></td></tr>
			<tr><td>CURR_BAL</td><td><input type="text" value="{{$Current_Bal_Amt}}" class="CURR_BAL" /><br /></td></tr>
			<tr><td>NUM_LOW_BAL_EVENTS</td><td><input type="text" value="{{$Low_Bal_Event_Cnt}}" class="NUM_LOW_BAL_EVENTS" /><br /></td></tr>
			<tr><td>NUM_LOW_BAL_DAYS</td><td><input type="text" value="{{$Low_Bal_Day_Cnt}}" class="NUM_LOW_BAL_DAYS" /><br /></td></tr>
			</table>
			<br />
			<center><button type="button" id="run">Run</button></center>
			<br />
			<br />
		</div>
		<div id="rightpanel" class="col-sm-7">
			<h1>Results</h1>
		</div>
	</div>

@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop
@section('content') 
	<h1>Exclusion and Scoring Test Harness</h1>
        <style>
            input{ width: 70px; }
        </style>	
	<div class="row" style="background: white; min-height: 300px;">
		<div id="leftpanel" style="border-right: solid 1px black;min-height: 300px;" class="col-sm-4">
			<br /><br />
			<table width="100%">
			<tr><td>EMP_STAT</td><td><input type="text" value="{{$Employment_Status_Desc}}" class="EMP_STAT" /><br /></td></tr>
			<tr><td>AI</td><td><input type="text" value="{{$Annual_Gross_Income_Amt}}" class="AI" /><br /></td></tr>
			<tr><td>EXST_LN</td><td><input type="text" value="{{$EXST_LN}}" class="EXST_LN" /><br /></td></tr>
			<tr><td>LN_DECL</td><td><input type="text" value="{{$LN_DECL}}" class="LN_DECL" /><br /></td></tr>
			
            <tr><td>G094</td><td><input type="text" value="{{$G094}}" class="G094" /><br /></td></tr>
			<tr><td>G093</td><td><input type="text" value="{{$G093}}" class="G093" /><br /></td></tr>
			<tr><td>G083</td><td><input type="text" value="{{$G083}}" class="G083" /><br /></td></tr>
			<tr><td>G064</td><td><input type="text" value="{{$G064}}" class="G064" /><br /></td></tr>
			<tr><td>G071</td><td><input type="text" value="{{$G071}}" class="G071" /><br /></td></tr>
			<tr><td>G057</td><td><input type="text" value="{{$G057}}" class="G057" /><br /></td></tr>
			<tr><td>BC31</td><td><input type="text" value="{{$BC31}}" class="BC31" /><br /></td></tr>
			<tr><td>BR33</td><td><input type="text" value="{{$BR33}}" class="BR33" /><br /></td></tr>
			<tr><td>RE33</td><td><input type="text" value="{{$RE33}}" class="RE33" /><br /></td></tr>
			<tr><td>MT33</td><td><input type="text" value="{{$MT33}}" class="MT33" /><br /></td></tr>
			<tr><td>AT20</td><td><input type="text" value="{{$AT20}}" class="AT20" /><br /></td></tr>
			<tr><td>AT11</td><td><input type="text" value="{{$AT11}}" class="AT11" /><br /></td></tr>
			<tr><td>BC06</td><td><input type="text" value="{{$BC06}}" class="BC06" /><br /></td></tr>
			<tr><td>G098</td><td><input type="text" value="{{$G098}}" class="G098" /><br /></td></tr>
			
            <tr><td>TOTAL_NSF</td><td><input type="text" value="{{$Total_NSF_Cnt}}" class="TOTAL_NSF" /><br /></td></tr>
			<tr><td>Days_SINCE_NSF</td><td><input type="text" value="{{$Days_Since_Last_NSF_Cnt}}" class="Days_since_NSF" /><br /></td></tr>
			<tr><td>MO_DEPOSIT_TO_AVERAGE</td><td><input type="text" value="{{$Avg_Monthly_Deposit_Amt}}" class="MO_DEPOSIT_TO_AVERAGE" /><br /></td></tr>
			<tr><td>AVG_BAL_PMT_DATE</td><td><input type="text" value="{{$Avg_Bal_On_Pmt_Dt_Amt}}" class="AVG_BAL_PMT_DATE" /><br /></td></tr>
			<tr><td>CURR_BAL</td><td><input type="text" value="{{$Current_Bal_Amt}}" class="CURR_BAL" /><br /></td></tr>
			<tr><td>NUM_LOW_BAL_EVENTS</td><td><input type="text" value="{{$Low_Bal_Event_Cnt}}" class="NUM_LOW_BAL_EVENTS" /><br /></td></tr>
			<tr><td>NUM_LOW_BAL_DAYS</td><td><input type="text" value="{{$Low_Bal_Day_Cnt}}" class="NUM_LOW_BAL_DAYS" /><br /></td></tr>
			</table>
			<br />
			<center><button type="button" id="run">Run</button></center>
			<br />
			<br />
		</div>
		<div id="rightpanel" class="col-sm-7">
			<h1>Results</h1>
		</div>
	</div>

@stop

@section('scripts')
	{{ HTML::script( 'js/test-harness.js');  }}
@stop