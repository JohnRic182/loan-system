@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Creating Account...</span>
	</div>
@stop
@section('content') 

	<div class="appl-title">
	    <div class="container">
	    	<h1>
				{{-- <span class="appl-step">1</span> --}}
				Review loan information and create your account.
			</h1>
	    </div> 
	</div>

	<div id="application" class="container">

		 {{-- Need to convert this into blade templating --}}
		{{ Form::open(array('url' => '/postRegister', 'method' => 'POST', 'id' => 'UserAccountForm','class' => 'form-horizontal validate' , 'role' => 'form')) }}				
			<div class="content-wrapper">
				<div class="loan-details box-wrapper">
					<div class="row">
						<div class="col-sm-8">
							<h2>
								<span class="icon icon-edit"></span>
								Review Loan Details
							</h2>
							<div id="notification"></div> 
							<table class="table">
								<thead>
									<tr>
										<th>Loan Amount</th>
										<th>Loan Term</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td> 
											$<span class="asking-loan-price"><?php if(isset($loan['loanAmt'])) echo number_format($loan['loanAmt'], 0 );?></span>
											
											@if (!$logged) 
											<a href="#" class="slider-edit-action">Edit</a>
											@endif

											<div class="slider-edit"> 
											<form class="form-inline" class="validate" id="slider-edit-form">
											  <div class="alert alert-danger" role="alert"></div>
											  <div class="slider-input">
											    <label class="sr-only" for="askingPriceInputAmount">Amount (in dollars)</label>
											    <div class="input-group">
											      <div class="input-group-addon">$</div>
											      <input type="text" class="form-control required number number-only" id="askingPriceInputAmount" placeholder="Loan Amount" alt="Loan Amount" value="<?php if(isset($loan['loanAmt'])) echo number_format($loan['loanAmt'], 0 ) ;?>">
											      <div class="input-group-addon">.00</div>
											    </div>
											  </div>
											  <div class="clearfix"></div>
											  	<a href="#" class="slider-edit-cancel pull-left">Cancel</a>
												<input type="button" value="Done" class="btn btn-cta pull-right btn-slider-update-loan-price">
											</form> 
											</div>
										</td>
										<td>36 Months</td>
									</tr>
								</tbody>
							</table> 
							<hr> 
							<div class="user-account"> 
								@if (!$logged) 
									<h2>
										<span class="icon icon-account"></span>
										Create Your Account 
										<span class="required-not">Required</span>
									</h2>
									{{--Transfer this to one blade block --}}
									<div id="notification"></div> 
									<input type="hidden" id="loan-purpose" value=""> 
									<div class="row">
										<div class="form-group col-sm-12 nopad-right">
											<div class="col-sm-12">
												{{ Form::label('id', 'Email Address'); }}
											</div>
											<div class="col-sm-12">
												{{ Form::email('email', (isset($email))? $email : '', array( 'class' => 'form-control required email', 'alt' =>'Email Address',  'alpha_num' => '' , 'placeholder' => 'Enter your email address') ) }}
												<input type="hidden" class="required validEmail inValidEmailAddress includeValidate" value="1" name="validEmail" alt="Validity of Email Address">
											</div>
										</div> 

										<div class="form-group col-sm-6 nopad-right">
											<div class="col-sm-12">
												{{ Form::label('id', 'Password'); }}
											</div>
											<div class="col-sm-12">
												{{ Form::password('password', array('id'=> 'password', 'class' => 'form-control required password passwordFormat minimum8 maximum20','alt' =>'Password',  'alpha_num' => '', 'placeholder' => 'Enter a password', 'data-toggle' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'At least 8 characters, including one capital letter and one number, but no special characters.') ) }}
											</div>
										</div>

										<div class="form-group col-sm-6 nopad-right"> 
											<div class="col-sm-12">
												{{ Form::label('id', 'Confirm Password'); }}
											</div>
											<div class="col-sm-12 nopad-right">
												{{ Form::password('confirmPassword', array( 'class' => 'form-control required matches|password', 'alt' =>'Confirm Password', 'alpha_num' => '', 'placeholder' => 'Re-enter a password' ) ) }}
											</div>
										</div>  								
										{{ Form::hidden('User_Role_Id', $userRoleId ) }}
										{{ Form::hidden('Update', $update ) }} 
										<div class="clearfix"></div>
									</div> 
									<hr> 	
								@endif  	
								<h2>
									<span class="icon icon-search"></span>
									Credit Check Details
									<span class="required-not">Required</span>
								</h2>
								{{--Transfer this to one blade block --}}
								<div id="notification"></div>  
								<div class="row"> 

									<div class="form-group col-sm-6">
										{{ Form::label('id', 'First Name  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12"> 
											{{ Form::hidden('uid', $uid, array('class' => 'form-control uid' ) ) }}
											{{ Form::text('firstName', $firstName , array('placeholder' => 'enter your first name', 'class' => 'form-control required alpha_num','alt' => 'First Name' ) ) }}
										</div> 
									</div>

									<div class="form-group col-sm-6">
										{{ Form::label('id', 'Last Name  ' , array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12 nopad-right">
											{{ Form::text('lastName', $lastName, array('placeholder' => 'enter your last name', 'class' => 'form-control required' ,'alt' => 'Last Name')) }}
										</div>
									</div>

									<div class="clearfix"></div>

									<div class="form-group col-sm-6">
										{{ Form::label('id', 'Primary Phone Number  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12"> 
											{{ Form::text('phoneNumber', $homePhoneNumber, array('placeholder' => '###-###-####', 'class' => 'form-control required alpha_num' ,'alt' => 'Primary Phone Number', 'id' => 'primaryPhoneNum') ) }}
										</div> 
									</div>

									<div class="form-group col-sm-6">
										{{ Form::label('id', 'Date Of Birth  ', array('class' => 'col-xs-12 control-label')); }}
										<div class="row-fluid">
											<div class="col-xs-5 nopad-right">
												{{ Form::select('month', $months , (isset($m)) ? $m : "" , array('class' => 'form-control birthdate required padd-fix', 'id' => 'month' ,'alt' => 'Month', 'style' => 'max-width: 100px;')); }}
											</div>
											<div class="col-xs-3 nopad-right">
												{{ Form::select('day', $days , (isset($d)) ?$d : "" , array('class' => 'form-control birthdate required padd-fix', 'id' => 'day' ,'alt' => 'Day')); }}
											</div>
											<div class="col-xs-4 nopad-right">
												{{ Form::select('year', $years , isset($y) ? $y  : "", array('class' => 'form-control birthdate required padd-fix', 'id' => 'year' ,'alt' => 'Year')); }}
											</div>
										</div>

										{{ Form::hidden('age', $age, array('class' => 'required valid-age', 'id' => "age" ) ) }}


									</div>

								   <div class="clearfix"></div>
					 
									
									<div class="form-group col-sm-6">
										{{ Form::label('id', 'Address  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12"> 
											{{ Form::text('address', $streetAddress1, array('placeholder' => 'enter street', 'class' => 'form-control required alpha_num' ,'alt' => 'Address') ) }}
										</div>
									</div>

									<div class="form-group col-sm-6">
										{{ Form::label('id', 'City  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12 nopad-right">
											{{ Form::text('city', $city, array('placeholder' => 'enter city', 'class' => 'form-control required alpha_num','alt' => 'City') ) }}
										</div>
									</div>
									
									 <div class="clearfix"></div>
									 
					 				<div class="form-group col-sm-6">
										{{ Form::label('id', 'State  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12"> 
											{{ Form::select('state', $states , 'key', array('class' => 'form-control required valid-state', 'id' => 'state' ,'alt' => 'State')); }}
										</div>
									</div>

									<div class="form-group col-sm-6">
										{{ Form::label('id', 'Zip  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12 nopad-right">
											{{ Form::text('zip', $zip, array('placeholder' => 'enter zip', 'class' => 'form-control required alpha_num' ,'alt' => 'Zip code', 'maxlength' => '10') ) }}
										</div>
									</div>
									
									 <div class="clearfix"></div>						     		    

					     		   
									<div class="form-group ssn col-sm-6">
										{{ Form::label('id', 'Social Security Number  ', array('class' => 'col-xs-12 control-label')); }}

										<div class="clearfix"></div>
										<div class=" col-xs-4">
											{{ Form::text('ssn1', $ssn1, array('placeholder' => '###', 'class' => 'form-control required alpha_num number-only', 'maxlength' => '3' ,'alt' => 'Social Security Number') ) }}
										</div>
										<div class=" col-xs-4">
											{{ Form::text('ssn2', $ssn2, array('placeholder' => '##', 'class' => 'form-control requried alpha_num number-only', 'maxlength' => '2','alt' => 'Social Security Number' ) ) }}
										</div>
										<div class="col-xs-4">
											{{ Form::text('ssn3', $ssn3, array('placeholder' => '####', 'class' => 'form-control requried alpha_num number-only pull-left', 'maxlength' => '4' ,'alt' => 'Social Security Number') ) }}
										</div> 
									</div>

									<div class="form-group col-sm-6">
										<p class="disclaimer-note" style="margin-top: 20px;">
										<span class="secure-icon glyphicon glyphicon-lock pull-left"></span>   We use 256-bit SSL encryption to protect and safeguard your data</p>
									</div>

									<div class="clearfix"></div>

									{{-- <hr>
									<p class="disclaimer-note"><span class="green-links">*</span>Annual Gross Income is your income each year BEFORE taxes and deductions. Alimony, child support, or separate maintenance payments need not be revealed if you do not wish to have them considered as a basis for repaying the loan.</p>
									<br> --}} 
								</div>  
								<hr> 
								<h2>
									<span class="icon icon-info"></span>
									Personal Information
									<span class="required-not">Required</span>
								</h2>
								<div class="row"> 
									<div class="form-group col-sm-6">
										{{ Form::label('id', 'How Will You Use Your Loan  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12">  
											{{ Form::select('loanPurposeArr', $loanPurposeArr , $loanPurposeId, array('class' => 'form-control required', 'id' => 'loanPurposeArr' ,'alt' => 'Loan Purpose')); }} 
										</div>
									</div> 
									<div class="clearfix"></div> 
									<div class="form-group col-sm-6">
										{{ Form::label('id', 'Housing Situation  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12">  
											{{ Form::select('housingSit', $housingSit , $rentOwn, array('class' => 'form-control required', 'id' => 'housingSit' ,'alt' => 'Housing Situation')); }} 
										</div>
									</div> 
									<?php $rentDisplay = ($rentOwn == 1 )? 'block' : 'none'; ?> 
									<div class="form-group col-sm-6" id="rentAmountDiv" style="display: {{ $rentDisplay }}">
										{{ Form::label('id', 'Monthly Rent  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12 nopad-right"> 
											{{-- disable file if rentOwn value = 1 --}}
											<?php $disabledRent = ($rentOwn != 1)? "disabled" : "";  ?>							
											{{ Form::text('rentAmount', $rent_amount, array('placeholder' => '$', 'id' => 'rentAmount', 'class' => 'form-control rentAmount confirmRentAmount required number-only '.$disabledRent  ,'alt' => 'Monthly Rent', "$disabledRent" => '' ) ) }}
										</div>
									</div>
									<?php $housingNotiDisplay = ($rentOwn != 1 )? 'block' : 'none'; ?>
									<div class="form-group col-sm-6" style="display:{{ $housingNotiDisplay }}" id="housingSitNotification">
										<div class="col-sm-12 nopad-right">
											<div class="innerText">{{ $housingSitNotification }}</div>
										</div>											
									</div>
					     		    <div class="clearfix"></div>

					     		    @if($rentOwn == 4)
					     		    	<div id="contactInfoDiv">
					     		    @else
					     		   	 	<div id="contactInfoDiv" class="hide">
					     		    @endif 
						     		    <div class="form-group col-sm-6 nopad-right">
											<div class="col-sm-12">
												{{ Form::label('id', 'Contact Name'); }}
											</div>
											<div class="col-sm-12">
												@if($rentOwn == 4)
												{{ Form::text('contactName', $contactName, array('alt' => 'Contact Name' ,'class' => 'form-control required', 'id' => 'contactName')) }}
												@else
												{{ Form::text('contactName', $contactName, array('alt' => 'Contact Name' ,'class' => 'form-control required', 'id' => 'contactName', 'disabled' => '')) }}
												@endif
											</div>
										</div>

										<div class="form-group col-sm-6 nopad-right"> 
											<div class="col-sm-12">
												{{ Form::label('id', 'Phone'); }}
											</div>
											<div class="col-sm-12 nopad-right">

												@if($rentOwn == 4)
												{{ Form::text('contactPhoneNr', $contactPhoneNr , array('placeholder' => '###-###-####', 'class' => 'form-control required alpha_num' ,'alt' => 'Phone Number', 'id' => 'contactPhoneNr') ) }}
												@else
												{{ Form::text('contactPhoneNr', $contactPhoneNr , array('placeholder' => '###-###-####', 'class' => 'form-control required alpha_num' ,'alt' => 'Phone Number', 'id' => 'contactPhoneNr', 'disabled' => 'disabled') ) }}
												@endif

											</div>
										</div> 					     		    
						     		    <div class="clearfix"></div> 
						     		</div>

					     		    <div class="form-group col-sm-6">
					     		    	<label for="" class="col-sm-12 control-label">
					     		    		Gross Income*
					     		    		<span data-toggle="tooltip" data-placement="right" title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost" class="rwar glyphicon glyphicon-exclamation-sign"></span>
					     		    	</label>
										<div class="col-xs-6">
											{{ Form::text('annualGrossIncome', $annualGrossIncome, array('placeholder' => '', 'class' => 'form-control required number-only','alt' => 'Gross Income' ) ) }} 
										</div>
										<div class="col-xs-6">
											{{ Form::select('term', array('Annual', 'Monthly') , 'Annual', array('class' => 'form-control  padd-fix', 'id' => 'term' ,'alt' => 'term')); }}
										</div>		
									</div>

									<div class="form-group col-sm-6">
										{{ Form::label('id', 'Current Employment Status  ', array('class' => 'col-sm-12 control-label')); }}
										<div class="col-sm-12 nopad-right">
											{{ Form::select('employeeStatus', $empStatus, isset($employeeStatus) && $employeeStatus != '' ? $employeeStatus : 2 , array('placeholder' => 'select', 'class' => 'form-control required alpha_num','alt' => 'Current Employment Status' ) ) }}
										</div>
									</div>
								
					     		    <div class="clearfix"></div>
								</div> 
								@if (!$logged)  
									<hr>
									<div class="form-group row consent"> 
										<div class="col-sm-12"><input type="checkbox" name="smsConsent" class="required" alt="Text/SMS Consent"> I consent to the electronic communications agreement that is outlined {{ HTML::link('consent/eSignature', 'here', array('target' => '_blank') ); }}</div>
										<div class="col-sm-12"><input alt="Agreement" name="agreementConsent" type="checkbox"  class="required" value=""> I agree to the following conditions specified below:
										<ul>
											<li></span>I consent to the {{ HTML::link('consent/creditPull', 'Credit Report Pull Agreement', array('target' => '_blank') ); }}</li>
											<li></span>I agree to Ascend's {{ HTML::link('terms', 'Terms of Use', array('target' => '_blank') ); }} and {{ HTML::link('privacyPolicy', 'Privacy Policy', array('target' => '_blank') ); }}</li>
											<li></span>I consent to receive call and SMS text messages from Ascend regarding my account as specified {{ HTML::link('consent/sms', 'here',  array('target' => '_blank') ); }}</li>
											<li></span>California residents: A married applicant may apply for a separate account CFLL No. 605 4860
											</li>
										</ul>
										</div>
									</div> 
								@endif 
							<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-sm-4">
 							@include('includes.sidebar')
 						</div>
					</div> 
				</div> 
			</div>

			<div class="action-section row">
				<div class="col-sm-8">
					<div class="pull-left">
						<a href="#" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
					</div>
					<div class="pull-right">
						{{-- @if (!$logged) --}}
							{{ Form::submit('Agree and Get My Rate', array('class' => 'btn btn-cta', 'id' => 'UserAccountFormSubmit') ) }}
						{{-- @else --}}
							{{-- {{ Form::button('Proceed to Credit Check', array('class' => 'btn btn-cta proceed', 'id' => 'submit') ) }} --}}
						{{-- @endif --}}
					</div>
				</div>
			</div> 
		{{ Form::close() }}
	</div>

	<!-- Modal -->
	<div class="modal fade" id="ra-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	{{-- 
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="myModalLabel">Modal title</h4>
		</div> 
	--}}
	      <div class="modal-body">
	      	Our real estate rental estimates indicate that there may be an error with the information you have provided.  Please confirm your rent amount is correct.  You may be asked to provide proof of rental amount.
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default btn-cta-default btn-back">BACK</button>
	        <button type="button" class="btn btn-cta btn-proceed">PROCEED</button>
	      </div>
	    </div>
	  </div>
	</div>

@stop

@section('scripts')
	{{ HTML::script( 'js/jquery.typing-0.2.0.min.js');  }}
	{{ HTML::script( 'js/jquery.maskedinput.min.js');  }}	
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/validate.js'); }}
	{{ HTML::script( 'js/user.js');  }}
	{{ HTML::script( 'js/register.js');  }}
	{{ HTML::script( 'js/application.js');  }}

	<script type="text/javascript">

		$(document).ready(function(){
			$("#primaryPhoneNum").mask("999-999-9999");
			$("#contactPhoneNr").mask("999-999-9999");
		
		});

	</script>
	
@stop