@include('includes.head')
<style>
	html{
		background: none;
	}
</style>

{{-- <div id="loader">
	<div class="loader">Loading...</div>
	<span style="color:#FFF">Redirecting to Exclusion and Scoring</span>
</div>
 --}}
<div id="notification">
	<br> 
	@if(isset($message))
		@if( isset($result) && $result == 'failed') 
			<div class="alert alert-danger">{{ $message }}</div> 
		@else
			<div class="alert alert-success">{{ $message }}</div> 
		@endif
	@endif
</div>  

@if( !isset($width) )
	{{  $width = '95%' }}
@endif

{{-- Fast Link Iframe --}}
@if( isset($link) )
	<script>

		console.log('link has been called');

		var windowTop = window.top; 
		var test = windowTop.document.getElementById("bank-wrapper-search"); 
		var parentDom = parent.document.getElementById('linkBankAccountReward');
		if(parentDom!=null){
			var test = parent.document.getElementById("bank-wrapper-search"); 
		}
		test.innerHTML = "";
		test.innerHTML = '<iframe src="{{ $link }}" frameborder="0" width="{{ $width }}" height="100%" style="min-height:600px;"></iframe> ';
 
	</script>
@endif

{{-- Redirection --}}
@if( isset($message) )
	@if( isset($result) && $result == 'success')
			<script>

				var windowTop = window.top; 
				var parentDom = parent.document.getElementById('linkBankAccountReward');

				if(parentDom != null){
					parentDom.removeAttribute('disabled');
					var att = document.createAttribute("style");
					att.value = "disabled: false";
					parentDom.setAttributeNode(att);
				}else{
					redirect(); 
				}	
			 
				function redirect(){
					// console.log(windowTop);
					var loader = windowTop.document.getElementById('loader');
					// var loader = document.getElementById('loader');
					loader.lastElementChild.innerHTML = '{{ $loadMessage }}';
					loader.style.display = 'block';
					top.window.location = "{{ url() . $nextLink }}";
				}

			</script>
	@endif
@endif
