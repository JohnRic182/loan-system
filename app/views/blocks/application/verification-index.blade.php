	@section('loader')
		<div id="loader">
			<div class="loader">Loading...</div>
			<span style="color:#FFF">Searching Banks...</span>
		</div>
	@stop
	
	@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1 class="appl-verification-h1"> <div class="appl-verification"></div><span>Congratulations, we've approved your loan request.</span></h1>
	    </div> 
	</div>
	<div id="application" class="container">
		 
	 	{{ Form::open(array('url' => '/verification/success', 'method' => 'POST', 'id' => 'verificationSteps','class' => 'form-horizontal' , 'role' => 'form')) }}
		<div class="approval-wrapper box-wrapper"> 
			<div class="row">
				<div class="col-sm-8 question-divider" style="position:relative;">
					
					<div style="height: 100%; border-left: thin solid #ccc; position:absolute; left:30px; " ></div>
					
					<div class="desc-wrapper">
						<h2 id="verification-mobile-label">
							Please click on the links below to sign your contract and provide information required to process your loan. 
							
						</h2>
					</div>
					<hr style="margin-top: 0px; border-top: 1px solid #81c868 ">  
					<ul class="verification-steps">  
						@if(isset($complete) && count($complete) > 0 )  
							@foreach($complete as $key => $step)  
								@if( $step['value'] == 'y' )
									<li class="submitted"><span class="glyphicon glyphicon-ok verification-status"></span>  <span class="updated">{{ $steps[$key-1]->Verification_Phase_Desc }}</span></li>
								@else
									<li><span class="glyphicon glyphicon-ok disabled verification-status"></span>  <a href="{{ generateVerfnStepsUrl($steps[$key-1]->Verification_Phase_Desc) }}">{{ $steps[$key-1]->Verification_Phase_Desc }}    <span class="glyphicon glyphicon-chevron-right" id="ver-arrow"></span></a></li>
								@endif
							@endforeach 
						@else
							@foreach($steps as $key => $step)  
								<li><span class="glyphicon glyphicon-ok disabled verification-status"></span>  <a href="{{ generateVerfnStepsUrl($step->Verification_Phase_Desc) }}">{{ $step->Verification_Phase_Desc }} ></a></li>
							@endforeach 
						@endif 
					</ul>
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div> 
		</div> 
		<div class="action-section row">
			<div class="col-sm-8">
				<div>
					<a href="#" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
 	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/yodlee.js');  }}
	{{ HTML::script( 'js/holder.js');  }}
@stop