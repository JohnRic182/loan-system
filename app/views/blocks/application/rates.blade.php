	@section('loader')
		<div id="loader">
			<div class="loader">Loading...</div>
			<span style="color:#FFF">Searching Banks...</span>
		</div>
	@stop
	
	@section('optionalCSS')
		{{HTML::style('css/jquery.nouislider.min.css')}}
 		{{HTML::style('css/jquery.nouislider.pips.min.css')}}
 	@stop

	@section('content') 


	<div class="loan-rate appl-title">
	    <div class="container">
	    	<h1>
				<span class="icon icon-loan-rate"></span>
				<br class="rwd-break">
				<!-- Congratulations, you qualify for an Ascend loan! <br> -->
				<span class="appl-title-desc" >Congratulations, you qualify for an Ascend loan!</span>
				
			</h1>
	    </div> 
	</div>
	<div id="application" class="loan-rate container">
		<div id="loan-rates" class="rates-wrapper box-wrapper">
			<div class="row">
				<div class="col-md-8 col-sm-12">	

					<h2 class="lr-custom-text2">
						<span class="icon icon-loan-purpose"></span>
							Finalize your loan amount
					</h2>
					
					<div id="notification">
						@if(isset($message))
							<div class="alert alert-danger">{{ $message }}</div>
						@endif
					</div>  

					<div class="raterewards">

						<div class="text-center slider-wrapper">
							<div id="slider-tooltip"></div>
							<span class="slider-nav pull-left">${{ number_format($minLoanAmount, 0) }}</span>
							<span id="slider-nav-label" class="slider-nav pull-right">${{ number_format($maxLoanAmount, 0) }}</span>
							<div class="clearfix"></div>
						</div>
						
						<input type="hidden" value="{{ $loanAmount }}" id="sliderAmt">

						<hr>

						<h2 class="lr-custom-text">
							<span class="icon icon-edit"></span>
								Customize your loan
						</h2>
						
						<p class="lr-custom">Add RateRewards to earn monthly discounts up to <strong>50% off your interest</strong> cost by demonstrating responsible financial behavior. {{ HTML::link('howItWorks', 'LEARN MORE') }} </p>
						
						 
					</div>
					

					<div class="loan-rate-result">  
						<div id="loader-rate-result">
							<div class="mini-loader">Loading...</div> 
						</div>
						
						<div class="col-sm-12 rate-mobile-compare">
							<p class="text-center rt-title">COMPARE AND CHOOSE ONES.</p>
						</div>
						<div class="input-group ratesTermsState" style="width: 100%"> 
							<button class="input-group-addon cal active" rel="other" data-toggle="tooltip" data-placement="top" >RATEREWARDS</button><button class="input-group-addon other" rel="standard" >STANDARD</button> 						 
						</div>  
						
						<section class="rates-choice">

							<div class="rates-choices row-fluid">
								
								<div class="rates-labels col-xs-4" id="rates-labels2">
									<table>
										<tr><td class="rl-header" style="border-bottom:none; ">CHOOSE ONE:</td></tr>
										<tr><td>Loan Amount</td></tr>
										<tr><td>Loan Term</td></tr>
										<tr><td>APR</td></tr>
										<tr><td>Monthly Payment</td></tr>
										<tr><td>Payments with RateRewards</td></tr>
									</table>
								</div>
								<div class="rates-rateRewards rate-products col-xs-4">
									<input type="hidden" id="rrloanAmountValue" value="{{ $rrLoanAmount }}" />
									<table>
										<tr><td id="rrLoanAmount">${{ number_format($rrLoanAmount, 0) }}</td></tr>
										<tr><td id="rrLoanTerms">36 Months</td></tr>
										<tr><td id="rrAPR">{{ $rrInterestRate }}%</td></tr>
										<tr><td id="rrMonthlyPayment">${{ number_format($rrMonthlyPayment, 2) }}</td></tr>
										<tr>
											<td id="rrRateRewardSavings" class="paymentRR">

												<div class="well">
													<span>As low as</span> <br>${{ number_format($rrEstMnthlyPymntAfrRwrd, 2) }}
												</div>
											
											</td>
										</tr>
									</table>
								</div>
								<div class="rates-standardLoan rate-products col-xs-4">
									<input type="hidden" id="loanAmountValue" value="{{ $stLoanAmount }}" />
									<table>
										<tr><td id="loanAmount">${{ number_format($stLoanAmount, 0) }}</td></tr>
										<tr><td id="loanTerms">36 Months</td></tr>
										<tr><td id="APR">{{ $stInterestRate }}%</td></tr>
										<tr><td id="monthlyPayment">${{ number_format($stMonthlyPayment, 2) }}</td></tr>
										<tr><td>N/A</td></tr>
									</table>
								</div>
								
							</div>

						</section>


					</div> 

					<input type="hidden" id="is-click-flag" value="0">

						<div class="disclaimer-note col-sm-12">
							<!-- <span class="cta-link">*</span>Payment with RateRewards reflects the average payment with a maximum monthly interest discount. Your actual reward level may vary based on your performance. See more details on the program {{ HTML::link('howItWorks', 'here', array('class' => 'cta-link')) }}.  -->
							<span class="cta-link">*</span>The RateRewards Estimator is provided as an educational tool to help you determine which product is best for you. Your actual reward level may vary based on your performance. See more details on the program {{ HTML::link('howItWorks', 'here', array('class' => 'cta-link')) }}.
						</div> 
				</div>
				<div class="col-md-4">
					@include('includes.sidebar')
				</div>
				
				<input type="hidden" id="qatUserId" value="{{ $qatUserId }}">

			</div>
		</div>

		<div class="action-section row rates-action">
			<div class="col-md-8 col-sm-12">
			<div class="pull-left" id="backLink">
			<a href="linkBankAccount" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
			</div>
			<div class="pull-right">
				@if( isset($loanType) && $loanType == 1 )
					@if( $idologyTestMode )
						<button class="btn btn-cta idologyShowModal">Continue With this Loan ></button>	
					@else
						<button class="btn btn-cta idology">Continue With this Loan ></button>
					@endif
				@else
					@if( $idologyTestMode)
						<button class="btn btn-cta idologyShowModal">Continue With this Loan ></button>	
					@else
						<button class="btn btn-cta idology">Continue With this Loan ></button>
					@endif
				@endif
			</div>
			<div class="clearfix"></div>
			</div>
		</div>

		<section class="loan-rates mobile-footer">
			<div class="row">
				<div class="disclaimer-note col-sm-12">
					<!-- <span class="cta-link">*</span>Payment with RateRewards reflects the average payment with a maximum monthly interest discount. Your actual reward level may vary based on your performance. See more details on the program {{ HTML::link('howItWorks', 'here', array('class' => 'cta-link')) }}.  -->
					<span class="cta-link">*</span>The RateRewards Estimator is provided as an educational tool to help you determine which product is best for you. Your actual reward level may vary based on your performance. See more details on the program {{ HTML::link('howItWorks', 'here', array('class' => 'cta-link')) }}.
				</div>

				<div class="col-sm-12 text-center" style="margin: 20px 0px;">
					<a href="linkBankAccount" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
				</div>			
			</div>

			<div class="row-fluid">
				<div class="col-sm-12 sidebar">
					@include('includes.sidebar')
				</div>
			</div>
		</section>

	</div>

	@if ( !$idologyTestMode )
		<input type="hidden" value="{{ $firstName }}" class="firstName" name="firstName" />
	  	<input type="hidden" value="{{ $lastName }}" class="lastName" name="lastName" />
	  	<input type="hidden" value="{{ $streetAddress1 }}" class="address" name="address" />
	  	<input type="hidden" value="{{ $city }}" class="city" name="city" />
	  	<input type="hidden" value="{{ $State }}" class="state" name="state" />
	  	<input type="hidden" value="{{ $zip }}" class="zip" name="zip" />
	  	<input type="hidden" value="{{ $ssn }}" class="ssn" name="ssn" /> 
	@else
	  	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Idology Fields</h4>
			      </div>
			      <div class="modal-body">
			      	<input type="hidden" value="{{$testMode}}" class="test" name="test" />
			        <table border="1">
						<tr><th>Variables</th><th>Values<br /></th></tr>
			          	<tr><td>FirstName</td><td><input type="text" value="Christopher" class="firstName" name="firstName" /><br /></td></tr>
			          	<tr><td>LastName</td><td><input type="text" value="Butler" class="lastName" name="lastName" /><br /></td></tr>
			          	<tr><td>Address</td><td><input type="text" value="2301 Polk St., Apt. 7" class="address" name="address" /><br /></td></tr>
			          	<tr><td>City</td><td><input type="text" value="San Francisco" class="city" name="city" /><br /></td></tr>
			          	<tr><td>State</td><td><input type="text" value="CA" class="state" name="state" /><br /></td></tr>
			          	<tr><td>Zip</td><td><input type="text" value="94109" class="zip" name="zip" /><br /></td></tr>
			          	<tr><td>SSN</td><td><input type="text" value="" class="ssn" name="ssn" /><br /></td></tr>
					</table>
			      </div>
			      <div class="modal-footer">
			        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary" id="idologyTest">Save changes</button>
			      </div>
		    </div>
		  </div>
		</div>
	@endif

	<div class="modal fade" id="modalQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <form id="idologySubmitAnswers">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 50px;"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel2" style="padding-top: 50px;">Identity Verification</h4>
	      </div>
	      <div class="modal-body">
	           <div class="questions">
	           </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary" >Save changes</button>
	      </div>
	      </form>
	    </div>
	  </div>
	</div>


 	{{--Transfer this to another block--}}
@stop

	
@section('scripts')	
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/yodlee.js');  }}
	{{ HTML::script( 'js/idology.js');  }}
	{{ HTML::script( 'js/jquery.nouislider.all.min.js') }}	
	{{ HTML::script( 'js/holder.js'); }}
	<script>
		jQuery(document).ready(function($) {
 
 			var updateSlider = $('#slider-tooltip');

 			var isMobile  = false;

 			// device detection
			if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
			    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
 			

 			if( isMobile == true ) {
 				$('#is-click-flag').val('1');	
 			}

 			updateSlider.noUiSlider({
				start: {{ $loanAmount }},
				step : 50,
				connect: "lower",
				range: {
					'min': {{ $minLoanAmount }}, 
					'max': {{ $maxLoanAmount }}
				},
				format: wNumb({
					decimals: 0,
					thousand: ',',
					prefix: '$',
				})
			});	

 			var Slider = function(){
 			}
     		
     		/**
     		 * Update Slider
     		 * 
     		 * @param  object response
     		 * @return
     		 */
 			Slider.prototype.updateSlider = function(response){
 				
 				console.log(response);

	 			var settings = {
	 				start: response.sliderLoanAmt,
					range: {
						'min': parseInt(response.minLoanAmount),
						'max': parseInt(response.maxLoanAmount)
					},
					connect: "lower",
					step: 50,
					format: wNumb({
						decimals: 0,
						thousand: ',',
						prefix: '$',
					})
				}; 


				$('#slider-nav-label').html('$'+ Common.addCommas(response.maxLoanAmount));

				$('#slider-tooltip').noUiSlider(settings, true);
			} 

			/**
			 * Update Loan
			 * 
			 * @param  float sliderAmt
			 * @param  boolean isRRAdded
			 * @return
			 */
			Slider.prototype.updateLoan = function( sliderAmt , isRRAdded ) {

				var sliderObj = this;
 
				$.ajax({
					url: baseUrl + 'loanRate',
					type: 'POST',
					dataType: 'JSON',
					data: { 
						rr: isRRAdded, 
						loanAmt: sliderAmt
					 },
					beforeSend: function(){
						$('#loader-rate-result ').fadeIn();
					},
					success: function( response ) {

						$('#loader-rate-result ').fadeOut();

						if( typeof response !== 'undefined'){
 	
							var paymentAfterReward =  'Add RateRewards to see';
							 
 							// 	if( isRRAdded == 'yes') {
							// 	paymentAfterReward =  'As low as $' + response.EstMnthlyPymntAfrRwrd;
							// }
							paymentAfterReward =  '<div class="well"> <span>As low as</span> <br>$'+ response.rrEstMnthlyPymntAfrRwrd +' </div>';

							


							$('#loanAmount').text( '$' + response.loanAmount );
							$('#loanAmountValue').val( response.loanAmount );
							$('#APR').text( response.interestRate + '%');
							$('#monthlyPayment').text( '$' + response.monthlyPayment );
							$('#rateRewardSavings').text( paymentAfterReward );

							$('#rrLoanAmount').text( '$' + response.rrLoanAmount );
							$('#rrloanAmountValue').val( response.loanAmount );
							$('#rrAPR').text( response.rrInterestRate + '%');
							$('#rrMonthlyPayment').text( '$' + response.rrMonthlyPayment );
							$('#rrRateRewardSavings').html( paymentAfterReward );


							sliderObj.updateSlider(response); 
						}
					}
				});
			}

			var Slider = new Slider();
 
			updateSlider.on('slide', function(){ 

				console.log('slider. ... moved');

				sliderVal = $(this).val().replace('$','');
				sliderVal = sliderVal.replace(',', ''); 

				$('#sliderAmt').val(sliderVal); 
			});

			//Update Slider Changes
			updateSlider.on('change', function(){ 
				console.log('slider. ... change');
				Slider.updateLoan($('#sliderAmt').val() , $('input[name=raterewards]:checked').val() );
			});

			// Tags after '-inline-' are inserted as HTML.
			// noUiSlider writes to the first element it finds.
			updateSlider.Link('lower').to('-inline-', function ( value ) {

				// The tooltip HTML is 'this', so additional
				// markup can be inserted here.
				$(this).html(
					'<span class="pull-left glyphicon glyphicon-chevron-left"></span>' +
					'<span class="pull-left">' + value + '</span>' +
					'<span class="pull-left glyphicon glyphicon-chevron-right"></span>' 
				);
			});


			sliderVal = updateSlider.val().replace('$','');
			sliderVal = sliderVal.replace(',', '')

			//Assign Slider Value
			$('#sliderAmtr').val(sliderVal); 

			/**
			 * RateRewerd Box
			 * 
			 * @param
			 * @return
			 */
			$('.rd-raterewards').click(function(event) {

				var sliderAmt = $('#sliderAmt').val();
				var isRRAdded = $(this).val();
			 
				Slider.updateLoan(sliderAmt , isRRAdded );
			});

			/**
			 * RateRewards product
			 * 
			 * 
			 * @return
			 */
			$('.rate-products').click(function(){

				console.log('raterewards products has been called');

				var h = $(this).attr('class');
				var h2 = h.split('-');
				var h3 = h2[1].split(' ');

				var sliderAmt = 0;
				var isRRAdded = 'no';
				//var sliderObj = new Slider();

				$('#is-click-flag').val('1');
			
				if( isMobile == false ){
 
					if(h3[0] == 'standardLoan' ){
							$('.rates-'+h3[0]).css('background-image','url("img/lr-active-standard.png")');
							$('.rates-rateRewards').css('background-image','url("img/lr-hover-rate-gray.png")'); 
							sliderAmt = $('#loanAmountValue').val();
					}else{
							$('.rates-'+h3[0]).css('background-image','url("/img/lr-active-rate.png")');
							$('.rates-standardLoan').css('background-image','url("img/lr-hover-standard-gray.png")');
							isRRAdded = 'yes';
							sliderAmt = $('#rrloanAmountValue').val();
					}

					$.ajax({
						url: baseUrl + 'insertLoanRate',
						type: 'POST',
						dataType: 'JSON',
						data: { 
							rr: isRRAdded, 
							loanAmt: sliderAmt
						 },
						beforeSend: function(){
							$('#loader-rate-result ').fadeIn();
						},
						success: function( response ) {

							$('#loader-rate-result ').fadeOut();

							if( typeof response !== 'undefined'){

								Slider.updateSlider(response); 

							}
						}
					});
				}
			});

			/**
			 * Standard Loan 
			 * 
			 * @return
			 */
			$('.other').click(function(){

				var sliderAmt = 0;
				var isRRAdded = 'no';
				//console.log(h3[0]);
				$('.rates-rateRewards').css('display','none');
				$('.rates-standardLoan').css('display','block');

				$('#is-click-flag').val('1');

				sliderAmt = $('#loanAmountValue').val();

				$.ajax({
					url: baseUrl + 'insertLoanRate',
					type: 'POST',
					dataType: 'JSON',
					data: { 
						rr: isRRAdded,
						loanAmt: sliderAmt
					 },
					beforeSend: function(){
						$('#loader-rate-result ').fadeIn();
					},
					success: function( response ) {

						$('#loader-rate-result ').fadeOut();

						if( typeof response !== 'undefined'){
							Slider.updateSlider(response); 
						}
					}
				});
			});

			/**
			 * Raterewards Loan
			 * 
			 * @return
			 */
			$('.cal').click(function(){

				var sliderAmt = 0;
				var isRRAdded = 'yes';

				//console.log(h3[0]);
				$('.rates-standardLoan').css('display','none');
				$('.rates-rateRewards').css('display','block');

				$('#is-click-flag').val('1');

				sliderAmt = $('#rrloanAmountValue').val();

				$.ajax({
					url: baseUrl + 'insertLoanRate',
					type: 'POST',
					dataType: 'JSON',
					data: { 
						rr: isRRAdded,
						loanAmt: sliderAmt
					 },
					beforeSend: function(){
						$('#loader-rate-result ').fadeIn();
					},
					success: function( response ) {

						$('#loader-rate-result ').fadeOut();

						if( typeof response !== 'undefined'){
							Slider.updateSlider(response); 
						}
					}
				});
			});


			$('[data-toggle="tooltip"]').tooltip();

			$('.ratesTermsState button').click(function(){

			$('.ratesTermsState button').removeClass('active');
			$(this).addClass('active');

			switch($(this).attr('rel')){
				case 'california':
					$('#other').fadeOut();
					$('#california').fadeIn();
				break;

				default:
					$('#other').fadeIn();
					$('#california').fadeOut();
				break;
			}

		});


		}); 
	</script>
@stop