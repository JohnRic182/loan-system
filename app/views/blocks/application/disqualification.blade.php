@section('content') 
	
	<div class="appl-title">
	    <div class="container">
	    	<h1>
				&nbsp;
			</h1>
	    </div> 
	</div>

	<div id="application" class="container">
		<div class="content-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-12 banner-adjustment">
					<div class="disqualification">
						@if( $debug == 'identity' || $debug == 'KBA')
							<p>Unfortunately we cannot positively verify your identity against the information you have provided and therefore cannot offer you a loan at this time.  If you think this has been in error, please contact our support team at {{ HTML::mailto('support@AscendLoan.com') }} or by calling 800-497-5314. </p>
						@elseif ($debug == 'income')
							<p>Applicants must have an annual income of $35,000 to qualify for an Ascend Personal Loan. We regret that we cannot offer you a loan at this time.</p>
						@else 
							<p>We regret that we cannot offer you a loan at this time.</p>
						@endif
						<div class="text-center note">
								<p>Thank you for your interest in</p>
								{{HTML::image(asset('img/logoBig.png'));}}
						</div>  
					</div>
				</div> 
			</div>
		</div>

		<div class="action-section row">
			<div class="col-sm-12">
				<div class="pull-right">
					<button onclick="window.location='/'" class="btn btn-cta" >Done</button>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>  
	</div>
@stop
@section('scripts')
	{{HTML::script('js/common.js');}}
@stop