<div class="modal fade" id="creditCriteria" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    	<div class="modal-header">
    		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    		<h2>Credit Criteria</h2>
    	</div>
    	<div class="modal-body">
    		<p>To qualify for an Ascend Personal Loan, you must:</p>

			<ul>
				<li>Be at least 18 years old</li>
				<li>Have a valid email account</li>
				<li>Have a verifiable name, date of birth, and social security number</li>
				<li>Have a bank account with a depository institution with a routing number</li>
			</ul>
			<p>You must also display the following credit characteristics:</p>
			<ul>
				<li>Have verifiable income over $35,000</li>
				<li>Have a minimum of 580 FICO credit score</li>
				<li>Have no more than 6 inquires in last 12 months</li>
				<li>Have no delinquencies in last 3 months</li>
				<li>Have no bankruptcy in last 12 months</li>
				<li>Meet our other underwriting scoring criteria that use a variety of credit report and other financial factors to assess and score your credit risk</li>
			</ul> 
    	</div> 
		<div class="modal-footer"> 
		</div>
    </div>
  </div>
</div>