@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1> 
				We need to verify your email address.
			</h1>
	    </div> 
	</div>

	<div id="application" class="container">
		<div class="approval-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-8">
					<p></p>
					<p>Please check your email for verification instructions. Click on the embedded link to verify your email and return to finish your application.</p>
		 		    <br>
		 		    <br>
		 		    <br>
					<div class="row">
						<div class="col-sm-6 text-center">
							 <a href="/">{{HTML::image(asset('img/logoBig.png'));}}</a>
						</div>

						<div class="col-sm-6">
							<div>
								<h2>Questions?</h2>
								<p>Call us at <br>
									800-497-5314 <br>
									<a href="#">questions@ascendloan.com</a>
								</p>
							</div>
						</div>
					</div> 
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
			
		</div> 
		<div class="action-section row">
			<div class="col-sm-8">
				<div class="pull-left">
					&nbsp;
				</div>	
				<div class="pull-right">
					 &nbsp;
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
 	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
@stop
