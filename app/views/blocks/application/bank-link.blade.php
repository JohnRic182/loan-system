	@section('loader')
		<div id="loader">
			<div class="loader">Loading...</div>
			<span style="color:#FFF">Searching Banks...</span>
		</div>
	@stop
	@section('content') 

	<div class="appl-title">
	    <div class="container">
	    	<h1>
				<span class="appl-step">3</span>
				<span class="step-title">Lower your APR <i>(step 3 of 3)</i></span>
			</h1>
	    </div> 
	</div>

	<div id="application" class="yodlee-form container">
		<div class="yodlee-bank-wrapper content-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-8">
					<h2 id="lb-s4">
						<span class="icon icon-link"></span>
						<span class="lb-s4-title">Link Bank Accounts and Get a Better Rate</span></h2>
						<div id="notification">
							@if(isset($message))
								<div class="alert alert-danger">{{ $message }}</div>
							@endif
						</div>  

					<div class="lb-s4-content" style="min-height:400px;">
						<p >We will lower your APR by 1% if you allow us to review your checking account history. We verify a steady source of income and look for responsible financial management. </p>

						<p>&nbsp;</p>
	 
						<div class="row link-actions">
							
							<div class="col-md-6">
								{{-- <a id="createYodleeAccount" href="createYodleeAccount" class="btn btn-cta text-normal text-center pull-right">Yes, Link My Bank Account <br> and Save {{ $savings}}</a> --}}

								<a id="createYodleeBankFastLink" href="createYodleeAccount" class="btn btn-cta text-normal text-center pull-right">Yes, Link My Bank Account <br> and Save {{ $savings}}</a>
							</div>

							<div class="link-divider"></div>
							
							<div class="m-link-divider">__</div>

							<div class="col-md-6 no-thanks">
								
								<a id="dontLinkBankAcct" class="btn btn-learn text-normal pull-left" href="javascript:void()">No Thanks, <br> Don't Link My Bank Account</a>
							</div> 
						</div> 
						<hr> 
						<div class="row">
							<div class="col-sm-12">
							<p class="security-icon" ><span class="icon icon-lock"></span></p>
							<p class="security-text"><strong>100% safe.</strong> Linking provides read-only access to your account and <u>we cannot execute transactions or move money from your account</u>.  We do not store your credentials and use high-level encryption and security to keep your data safe. {{ HTML::link('/faqs', 'Learn more.', array('class' => 'green-links')) }}</p>
							</div>
						</div>
						{{-- <div class="disclaimer-note col-sm-12" style="margin-top: 38px;">
							*Estimated savings based on requested loan amount and projected interest rate.
						</div> --}}
					</div>
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div> 
		</div>

		@if(!$LTExpress)		
			<div class="action-section row">
				<div class="col-sm-8">
					<div class="col-sm-4 pull-right-mobile">
						<a href="{{ url('checkcredit') }}" class="cta-link" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
					</div>
					<div class="col-sm-4">
					
					</div>
					<div class="col-sm-4"> 
						&nbsp;
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		@endif


	</div>
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/yodlee.js');  }}
@stop