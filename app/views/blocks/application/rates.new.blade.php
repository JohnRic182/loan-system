	@section('loader')
		<div id="loader">
			<div class="loader">Loading...</div>
			<span style="color:#FFF">Searching Banks...</span>
		</div>
	@stop
	
	@section('optionalCSS')
		{{HTML::style('css/jquery.nouislider.min.css')}}
 		{{HTML::style('css/jquery.nouislider.pips.min.css')}}
 	@stop

	@section('content') 
	
	<div class="appl-title">
	    <div class="container">
	    	<h1>
				<span class="icon icon-loan-rate"></span>
				Congratulations, you qualify for an Ascend loan! <br>
				<span class="appl-title-desc">Personalize your loan by adding our RateRewards Program</span>
			</h1>
	    </div> 
	</div>
	<div id="application" class="container">
		<div id="loan-rates" class="rates-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-8">	

					<h2>
						<span class="icon icon-loan-purpose"></span>
							Finalize your loan amount
					</h2>
					
					<div id="notification">
						@if(isset($message))
							<div class="alert alert-danger">{{ $message }}</div>
						@endif
					</div>  

					<div class="raterewards">

						<div class="text-center slider-wrapper">
							<div id="slider-tooltip"></div>
							<span class="slider-nav pull-left">${{ number_format($minLoanAmount, 0) }}</span>
							<span class="slider-nav pull-right">${{ number_format($maxLoanAmount, 0) }}</span>
							<div class="clearfix"></div>
						</div>
						
						<input type="hidden" value="{{ $loanAmount }}" id="sliderAmt">

						<hr>

						<h2>
							<span class="icon icon-edit"></span>
								Customize your loan
						</h2>
						
						<p>Add RateRewards to earn monthly discounts up to <strong>50% off your interest</strong> cost by demonstrating responsible financial behavior. {{ HTML::link('howItWorks', 'LEARN MORE') }} </p>
						<!-- <div class="form-group">
							<div class="col-sm-6">
								<input class="rd-raterewards" type="radio" name="raterewards" value="yes">
								<label>Yes, add RateRewards</label>
							</div>
							<div class="col-sm-6">
								<input class="rd-raterewards" type="radio" name="raterewards" value="no">
								<label>No, don't add RateRewards</label>
							</div>
						</div>
						<p>&nbsp;</p> -->
						 
					</div>
					

					<div class="loan-rate-result">  
						<div id="loader-rate-result">
							<div class="mini-loader">Loading...</div> 
						</div>
						<!-- <table class="table rates-tab">
							<tr>
								<td>Loan Amount</td>
								<td id="loanAmount">${{ number_format($loanAmount) }}</td>
							</tr>
							<tr>
								<td>Loan Term</td>
								<td id="loanTerms">36 Months</td>
							</tr>
							<tr>
								<td>APR</td>
									<td id="APR">{{ $interestRate }}%</td>
							</tr> 
							<tr>
								<td>Monthly Payment</td>
								<td id="monthlyPayment">${{ number_format($monthlyPayment, 2) }}</td>
							</tr>
							<tr>
								<td>Payments with RateRewards</td>
								<td id="rateRewardSavings">Add RateRewards to see</td>
							</tr> 
						</table>  -->
						<div class="rates-choices">
							<div class="rates-labels col-sm-4">
								<table>
									<tr><td class="rl-header">CHOOSE ONE:</td></tr>
									<tr><td>Loan Amount</td></tr>
									<tr><td>Loan Term</td></tr>
									<tr><td>APR</td></tr>
									<tr><td>Monthly Payment</td></tr>
									<tr><td>Payments with RateRewards</td></tr>
								</table>
							</div>
							<div class="rates-standardLoan col-sm-4">
								<table>
									<tr><td>${{ number_format($loanAmount) }}</td></tr>
									<tr><td>36 Months</td></tr>
									<tr><td>28%</td></tr>
									<tr><td>$206.82</td></tr>
									<tr><td>N/A</td></tr>
								</table>
							</div>
							<div class="rates-rateRewards col-sm-4">
								<table>
									<tr><td>${{ number_format($loanAmount) }}</td></tr>
									<tr><td>36 Months</td></tr>
									<tr><td>30%</td></tr>
									<tr><td>$212.36</td></tr>
									<tr><td class="paymentRR">As low as $175.57</td></tr>
								</table>
							</div>
						</div>
					</div>
					<hr style="margin-top: 0;">
					<div class="disclaimer-note">
							<span class="cta-link">*</span>Payment with RateRewards reflects the average payment with a maximum monthly interest discount. Your actual reward level may vary based on your performance. See more details on the program {{ HTML::link('howItWorks', 'here', array('class' => 'cta-link')) }}. 
						</div> 
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
		</div>

		<div class="action-section row">
			<div class="col-sm-8">
			<div class="pull-left">
			<a href="linkBankAccount" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
			</div>
			<div class="pull-right">
				@if( isset($loanType) && $loanType == 1 )
					@if( $idologyTestMode )
						<button class="btn btn-cta idologyShowModal">Continue With this Loan ></button>	
					@else
						<button class="btn btn-cta idology">Continue With this Loan ></button>
					@endif
				@else
					@if( $idologyTestMode)
						<button class="btn btn-cta idologyShowModal">Continue With this Loan ></button>	
					@else
						<button class="btn btn-cta idology">Continue With this Loan ></button>
					@endif
				@endif
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>

	@if ( !$idologyTestMode )
		<input type="hidden" value="{{ $firstName }}" class="firstName" name="firstName" />
	  	<input type="hidden" value="{{ $lastName }}" class="lastName" name="lastName" />
	  	<input type="hidden" value="{{ $streetAddress1 }}" class="address" name="address" />
	  	<input type="hidden" value="{{ $city }}" class="city" name="city" />
	  	<input type="hidden" value="{{ $State }}" class="state" name="state" />
	  	<input type="hidden" value="{{ $zip }}" class="zip" name="zip" />
	  	<input type="hidden" value="{{ $ssn }}" class="ssn" name="ssn" /> 
	@else
	  	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Idology Fields</h4>
			      </div>
			      <div class="modal-body">
			      	<input type="hidden" value="{{$testMode}}" class="test" name="test" />
			        <table border="1">
						<tr><th>Variables</th><th>Values<br /></th></tr>
			          	<tr><td>FirstName</td><td><input type="text" value="Christopher" class="firstName" name="firstName" /><br /></td></tr>
			          	<tr><td>LastName</td><td><input type="text" value="Butler" class="lastName" name="lastName" /><br /></td></tr>
			          	<tr><td>Address</td><td><input type="text" value="2301 Polk St., Apt. 7" class="address" name="address" /><br /></td></tr>
			          	<tr><td>City</td><td><input type="text" value="San Francisco" class="city" name="city" /><br /></td></tr>
			          	<tr><td>State</td><td><input type="text" value="CA" class="state" name="state" /><br /></td></tr>
			          	<tr><td>Zip</td><td><input type="text" value="94109" class="zip" name="zip" /><br /></td></tr>
			          	<tr><td>SSN</td><td><input type="text" value="" class="ssn" name="ssn" /><br /></td></tr>
					</table>
			      </div>
			      <div class="modal-footer">
			        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
			        <button type="submit" class="btn btn-primary" id="idologyTest">Save changes</button>
			      </div>
		    </div>
		  </div>
		</div>
	@endif

	<div class="modal fade" id="modalQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <form id="idologySubmitAnswers">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="padding-top: 50px;"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel2" style="padding-top: 50px;">Identity Verification</h4>
	      </div>
	      <div class="modal-body">
	           <div class="questions">
	           </div>
	        
	      </div>
	      <div class="modal-footer">
	        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="submit" class="btn btn-primary" >Save changes</button>
	      </div>
	      </form>
	    </div>
	  </div>
	</div>

 	{{--Transfer this to another block--}}
@stop

	
@section('scripts')	
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/yodlee.js');  }}
	{{ HTML::script( 'js/idology.js');  }}
	{{ HTML::script( 'js/jquery.nouislider.all.min.js') }}	
	{{ HTML::script( 'js/holder.js'); }}
	<script>
		jQuery(document).ready(function($) {
 
 			var updateSlider = $('#slider-tooltip');


 			updateSlider.noUiSlider({
				start: {{ $loanAmount }},
				step : 50,
				connect: "lower",
				range: {
					'min': {{ $minLoanAmount }}, 
					'max': {{ $maxLoanAmount }}
				},
				format: wNumb({
					decimals: 0,
					thousand: ',',
					prefix: '$',
				})
			});	

 			var Slider = function(){

 			}
     	
 			Slider.prototype.updateSlider = function(response){
 				
 				console.log(response);

	 			var settings = {
	 				start: response.sliderLoanAmt,
					range: {
						'min': parseInt(response.minLoanAmount),
						'max': parseInt(response.maxLoanAmount)
					},
					connect: "lower",
					step: 50,
					format: wNumb({
						decimals: 0,
						thousand: ',',
						prefix: '$',
					})
				}; 

				$('#slider-tooltip').noUiSlider(settings, true);
			} 

			Slider.prototype.updateLoan = function( sliderAmt , isRRAdded ) {

				var sliderObj = this;
 
				$.ajax({
					url: baseUrl + 'loanRate',
					type: 'POST',
					dataType: 'JSON',
					data: { 
						rr: isRRAdded, 
						loanAmt: sliderAmt
					 },
					beforeSend: function(){
						$('#loader-rate-result ').fadeIn();
					},
					success: function( response ) {

						$('#loader-rate-result ').fadeOut();

						if( typeof response !== 'undefined'){
 	
							var paymentAfterReward =  'Add RateRewards to see';
							 
 							if( isRRAdded == 'yes') {
								paymentAfterReward =  'As low as $' + response.EstMnthlyPymntAfrRwrd;
							}
 
							$('#loanAmount').text( '$' + response.loanAmount );
							$('#APR').text( response.interestRate + '%');
							$('#monthlyPayment').text( '$' + response.monthlyPayment );
							$('#rateRewardSavings').text( paymentAfterReward );

							sliderObj.updateSlider(response); 
						}
					}
				});

			}

			var Slider = new Slider();
 
			updateSlider.on('slide', function(){ 

				console.log('slider. ... moved');

				sliderVal = $(this).val().replace('$','');
				sliderVal = sliderVal.replace(',', ''); 

				$('#sliderAmt').val(sliderVal); 

			});



			updateSlider.on('change', function(){ 
				console.log('slider. ... change');
				Slider.updateLoan($('#sliderAmt').val() , $('input[name=raterewards]:checked').val() );
			});

			// Tags after '-inline-' are inserted as HTML.
			// noUiSlider writes to the first element it finds.
			updateSlider.Link('lower').to('-inline-', function ( value ) {

				// The tooltip HTML is 'this', so additional
				// markup can be inserted here.
				$(this).html(
					'<span class="pull-left glyphicon glyphicon-chevron-left"></span>' +
					'<span class="pull-left">' + value + '</span>' +
					'<span class="pull-left glyphicon glyphicon-chevron-right"></span>' 
				);
			});


			sliderVal = updateSlider.val().replace('$','');
			sliderVal = sliderVal.replace(',', '')

			//Assign Slider Value
			$('#sliderAmtr').val(sliderVal); 

			

			console.log(Slider);

			$('.rd-raterewards').click(function(event) {

				var sliderAmt = $('#sliderAmt').val();
				var isRRAdded = $(this).val();
			 
				Slider.updateLoan(sliderAmt , isRRAdded );
				
			});
		}); 
	</script>
@stop