@section('bg')
	<div class="bg-main"></div>
@stop
@section('content')

	<script type="text/javascript">
		var DEFAULT_SLIDER_VAL = {{ $defaultSliderAmt }};
    var MINIMUM_LOAN_AMT   = {{ $sliderMinLoanAmt }};
    var MAXIMUM_LOAN_AMT   = {{ $sliderMaxLoanAmt }};
	</script>
<style>
  a.navbar-brand-logo:after {content: url('/img/v3/logo.png'); }
  .bg-main {background: url('/img/v3/banner.jpg') no-repeat scroll 100% 0%; background-size: cover; height: 865px; }
  .cta-link {color: #fff; }
  .btn-cta-check-rate {padding: 10px 45px; }
  .partner-wrapper {margin: 40px auto 155px !important; max-width: 912px; }
  .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
    background: rgba(255,255,255,0.9);
    box-shadow: 0 0 25px rgba(0,0,0,0.1);
  }
  .partner-banner {padding:0; }
  .partner-form .slogan {margin-bottom: 4em; }
  .partner-form .slogan h1 {width: 100%; padding-left: 40px; padding-right: 40px; font-size: 30px; font-weight: bold; color: #2996cc; }
  .partner-form .slogan h1 small {font-size: 24px; font-weight: normal; color: inherit; }
  .partner-wrapper .wont-impact {margin-left: 20px; margin-top: -72px; background: url('/img/v3/impact.png') no-repeat; width: 292px; height: 52px; display: block; }
  .select-rate h1 {font-size: 18px; color: #333; text-transform: uppercase; margin-bottom: 1.4em; }
  .select-rate .slider-container {padding-left: 60px; }
  .select-rate .check-wrapper {text-align: center; padding-top: 1em; padding-right: 60px; }
  .features {background: transparent; padding: 35px 25px 25px; }
  .features p {font-size: 14px; }
  .features .feature-list {text-transform: none; font-size: 15px; }
  .media-list {background: #fff; border-bottom: 1px solid #eeefef; padding: 0; }
  .media-list ul {list-style: none; padding-left: 0; margin-bottom: 0; }
  .media-list ul li {display: inline; margin: 0 25px; }
  .noUi-origin {border: 1px solid #c0c0c0 !important; border-radius: 4px; }
  footer hr {border-top: 1px solid #d3dfe5; }

  @media (min-width: 1500px) {
    .bg-main {
      background: url('/img/v3/banner.jpg') no-repeat scroll 94% -130px;
      background-size: cover;
    }
  }

  @media (min-width: 1800px) {
    .bg-main {
      background: url('/img/v3/banner.jpg') no-repeat scroll 94% -240px;
      background-size: cover;
    }
  }

  @media (max-width: 600px) {
    .bg-main {
      background: url('/img/v3/banner.jpg') no-repeat scroll 94% 0%;
      background-size: cover;
      height: 1100px;
    }
    header .navbar {
      background: #212b30;
      padding: 10px 0 0px;
      border-bottom: 1px solid #1d2629;
      margin-bottom: 25px;
    }
    header .navbar-default .navbar-toggle {
      padding: 12px 0;
      width: 45px;
    }
    a.navbar-brand-logo:after {
      content: url('/img/v2/logo.png');
    }
    .navbar-brand {
      padding: 0px 10px;
    }
    header .navbar-default .navbar-toggle .icon-bar {
      width: 25px;
      height: 3px;
      border-radius: 0;
    }
    .partner-brand-container {
      margin-top: 25px;
    }
    .partner-wrapper.slider-form-wrapper {
      min-height: 750px;
      margin: 0 15px 45px !important;
      padding: 0;
    }
    .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
      background: rgba(255, 255, 255, 0.9);
      color: #fff;
      margin-top: 0;
      padding-top: 0;
      box-shadow: none;
    }
    .partner-wrapper .partner-form .slogan {
      margin-bottom: 1em;
    }
    .partner-wrapper .partner-form .slogan h1 {
      font-weight: 500;
      font-size: 24px !important;
      padding: 25px 15px 15px;
      margin-top: 0;
    }
    .partner-wrapper .partner-form .slogan h1 small {
      font-size: 20px;
      font-weight: 300;
      display: inline-block;
      margin-top: 8px;
      line-height: 1.4em;
    }
    .partner-wrapper .select-rate .slider-container {
      padding-left: 15px !important;
    }
    .partner-wrapper .select-rate h1 {
      margin-bottom: 15px;
      font-size: 14px;
      font-weight: bold;
      text-align: center;
    }
    .partner-wrapper .wont-impact-mobile {
      text-align: left;
      margin-top: -5px;
      padding-left: 0;
    }
    .select-rate .check-wrapper {
      padding: 15px;
      margin: 15px;
      border-radius: 4px;
    }
    .slider-wrapper {
      width: 75%;
      padding-top: 5px;
    }
    .partner-wrapper .select-rate .check-rate-wrapper {
      padding-top: 0;
    }
    .partner-wrapper .btn-cta-check-rate {
      width: 100%;
    }
    .media-list {
      line-height: 3em;
    }
    .media-list ul {margin-bottom: 15px; }
    .media-list ul li:first-child {
      width: 100%;
    }
    .media-list ul li {
      display: inline-block;
      margin: 0;
      width: 48%;
    }
    .media-list ul li img {max-width: 100%; padding: 0 10px; }

    .footer-links {
      margin-top: 30px;
    }
    .footer-links hr {
      border-top: 1px solid #ccc;
    }
    .footer-links #footer-contact {
      margin-top: 20px;
    }
    footer .disclaimer {
      padding-top: 0;
      text-align: left;
    }
    #aboutus, #borrowers {padding-top: 2em; padding-bottom: 2em; }
    #aboutus h1, #borrowers h1 {font-size: 28px; }
    #aboutus .inner-container, #howitworks .inner-container {padding-top: 20px; }
  }

  @media (max-width: 767px){
    .partner-wrapper .wont-impact-mobile {
      width: auto;
    }
    header .navbar-default .navbar-toggle {
      margin-right: 0;
    }
    #creditCriteria .modal-content {
      width: 100%;
    }
  }

  @media (max-width: 414px) {
  }

  @media (max-width: 360px) {

    .partner-wrapper .partner-form .slogan h1 {
      padding-left: 0;
      padding-right: 0;
    }
  }

  @media (max-width: 320px) {
    .partner-wrapper .partner-form .slogan h1 {
      font-size: 22px !important;
      line-height: 1.4em;
    }
    .partner-wrapper .partner-form .slogan h1 small {
      font-size: 17px;
      line-height: 1.2em;
    }
    .btn-cta-check-rate {padding: 10px; }
  }
</style>

    {{HTML::style('css/jquery.nouislider.min.css')}}
 	{{HTML::style('css/jquery.nouislider.pips.min.css')}}


	<div class="container inner-container banner partner-banner" style="margin-top: -15px;">
 		<div class="slider-form-wrapper partner-wrapper">

		{{ Form::open(array('url' => '/register', 'method' => 'POST', 'id' => 'LoanApplicationForm','class' => 'form-inline LoanApplicationForm-partner partner-form' , 'role' => 'form')) }}
 			<div class="partner slogan">
			    <h1>Get the funds you need at a low rate and payment.<br><small>Add RateRewards to lower your payment even more.</small></h1>
			</div>
      <div class="select-rate row">
        <div class="col-sm-7 slider-container">
          <h1>Select your loan amount:</h1>
          <div class="partner-slider-version">
            <div class="text-center slider-wrapper">
              <div id="slider-tooltip"></div>
            </div>
          </div>
        </div>

        <div class="col-sm-5 check-wrapper">

          <div class="check-rate-wrapper">
            <button class="btn btn-cta btn-cta-check-rate">
              <span class="icon icon-lock-v2"></span>Check your Rate
            </button>
          </div>
          <div class="wont-impact"></div>
          <div class="wont-impact-mobile" align="center">
            {{HTML::image( asset('/img/v3/impact-mobile.png') )}}
          </div>
        </div>
      </div>

      <div id="howitworks" class="features row">

        <div class="col-sm-3">
          <span class="features-icon features-icon1"></span>
          <p class="feature-list">1. Get your low-rate loan</p>
          <p>Start with a low monthly payment.</p>

        </div>
        <div class="col-sm-3">
          <span class="features-icon features-icon2"></span>
          <p class="feature-list">2. Add RateRewards</p>
          <p>Add our optional Rewards program.</p>
        </div>
        <div class="col-sm-3">
          <span class="features-icon features-icon3"></span>
          <p class="feature-list">3. Earn rewards</p>
          <p>Earn discounts up to 50% off your interest.</p>
        </div>
        <div class="col-sm-3">
          <span class="features-icon features-icon4"></span>
          <p class="feature-list">4. Lower your payment</p>
          <p>Lower your payment automatically.</p>
        </div>

      </div>

			<input type="hidden" name="TotalMonthlyReward" value="" id="TotalMonthlyReward">
			<input type="hidden" name="loanAmt" value="" id="loanSliderInput">
			<input type="hidden" name="loanPurposeId" value="0" id="loanPurposeInput">
			<input type="hidden" name="loanPurposeTxt" value="" id="loanPurposeTxt">
			<input type="hidden" name="loanProductId" value="1" id="loanTypeInput">
			<input type="hidden" name="loanProductTxt" value="Ascend Loan" id="loanTypeText">
			<input type="hidden" name="creditquality" value="" id="creditquality">
			<input type="hidden" name="isProductionQA" value="{{ $isProductionQA }}" id="isProductionQA">
			<input type="hidden" name="isProductionBypass" value="{{ $isProductionBypass }}" id="isProductionBypass">

			{{ Form::close() }}

      <div class="clearfix"></div>
		</div>
 	</div>
    <div id="media" class="media-list">
      <ul>
        <li>Featured in:</li>
        <li>{{ HTML::image(asset('img/media-bloomberg.png') ) }}</li>
        <li>{{ HTML::image(asset('img/media-thestreet.png') ) }}</li>
        <li>{{ HTML::image(asset('img/media-AB.png') ) }}</li>
        <li>{{ HTML::image(asset('img/media-pymnts.png') ) }}</li>
      </ul>
    </div>

	<div id="aboutus" class="about-us">
		<div class="container inner-container">
			<div class="row-fluid">
				<h1>Who is Ascend?</h1>
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit scores indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost. <br>
				It's that simple.
				</p>
			</div>
		</div>
	</div>


	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1>
				<hr class="centered-line">
			</div>


			{{-- carousel --}}

			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">

      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>

				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li>
								</ul>


							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>

				</div>


			  </div>

{{-- 			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
			    <span class="icon icon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
			    <span class="icon icon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a> --}}


			</div>


			{{-- carosel --}}

			<div class="clearfix"></div>

		</div>
	</div>

@stop

@section('scripts')
	{{HTML::script('js/common.js');}}
	{{HTML::script('js/bootstrap.min.js');}}
	{{HTML::script('js/jquery.nouislider.all.min.js')}}
	{{HTML::script('js/home.js')}}
@stop