@section('content')
	<div class="appl-title">
	    <div class="container">
	    	<h1> {{ $form['title'] }} </h1>
	    </div> 
	</div> 
	<div class="container" id="application">
		<div class="box-wrapper">
			<div class="row">
				<div class="col-sm-8">
					<br/> 
					@if($statusMsg)
						<div class="alert alert-danger" role="alert">
							{{ $statusMsg }}
						</div>
					@endif

					{{ Form::open(array('url' => $form['url'], 'method' => 'POST', 'class' => 'form-horizontal validate' , 'role' => 'form', 'id' => $form['id'])) }} 
					
					<div class="content-wrapper">
					 	
					 	<p>Please input your email address below.</p>
					 		
					 	<div id="notification"></div>
						
						<div class="form-group">
							{{ Form::label('id', 'Email', array(  'class' => 'col-sm-2 control-label',)); }}
							<div class="col-sm-10">
								{{ Form::email('Email', '', array( 'placeholder' => 'please enter your email address', 'class' => 'form-control required ' , 'alt' => 'Email Address' ) ) }}
							</div>
						</div>
					</div>	

					<div class="action-section">
						<div class="pull-right">
							<input type="submit" class="btn btn-cta" value="Send"> 
						</div>
						<div class="clearfix"></div>
					</div>
					{{ Form::close() }}
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
			
		</div>
	</div>
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/user.js');  }}
@stop