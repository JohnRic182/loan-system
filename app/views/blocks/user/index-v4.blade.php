@section('bg')
	<div class="bg-main"></div>
@stop
@section('content')

  <script type="text/javascript">
    var DEFAULT_SLIDER_VAL = {{ $defaultSliderAmt }};
    var MINIMUM_LOAN_AMT   = {{ $sliderMinLoanAmt }};
    var MAXIMUM_LOAN_AMT   = {{ $sliderMaxLoanAmt }};
  </script>
<style>
  a.navbar-brand-logo:after {content: url('/img/v3/logo.png'); }
  .bg-main {background: url('/img/v3/banner.jpg') no-repeat scroll 100% 0%; background-size: cover; height: 865px; }
  .cta-link {color: #fff; }
  .btn-cta-check-rate {padding: 10px 45px; }
  .partner-wrapper {margin: 40px auto 90px !important; }
  .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
    background: rgba(255,255,255,0.9);
    box-shadow: 0 0 25px rgba(0,0,0,0.1);
    margin-top: 20px;
    padding: 10px 10px 40px;
  }
  .partner-banner {padding:0; }
  .partner-form .slogan {margin-bottom: 3em; }
  .partner-form .slogan h1 {width: 100%; padding-left: 40px; padding-right: 40px; font-size: 30px; font-weight: bold; color: #2996cc; }
  .partner-form .slogan h1 small {font-size: 24px; font-weight: normal; color: inherit; }
  .partner-wrapper .wont-impact {margin-left: 30px; margin-top: -16px; background: url('/img/v3/impact2.png') no-repeat; width: 292px; height: 55px; display: block; }
  .select-rate h1.show-mobile {display: none; }
  .select-rate h1 {
    font-size: 14px;
    font-weight: bold;
    color: #333;
    text-transform: uppercase;
    margin: 0 -20px 10px -20px;
    line-height: 1.2em;
    padding: 15px 20px;
    border-bottom: 1px solid #ccc;
  }
  .select-rate h1 span {font-size: 36px; font-weight: 700; color: #2996cc; float: right; line-height: 0; }
  .select-rate .loan-form {
    border: 1px solid #ccc;
    margin-right: 40px;
    padding-left: 20px;
    padding-right: 20px;
    padding-bottom: 10px;
  }
  .select-rate .loan-form input.number-only,
  .select-rate .loan-form select {margin: 8px 0; font-size: 13px; border: 1px solid #ccc; }
  .select-rate .loan-form input.number-only {
	  width: 100%;
	  padding: 10px 15px 10px 28px;
	  background: transparent;

	  -webkit-appearance: textfield;
	  -moz-appearance: textfield;
	  -ms-appearance: textfield;
	  -o-appearance: textfield;
	  appearance: textfield;
  }
  .select-rate .loan-form input.number-only::-webkit-inner-spin-button,
  .select-rate .loan-form input.number-only::-webkit-outer-spin-button {
    -webkit-appearance: none;
    -moz-appearance: none;
	-ms-appearance: none;
	-o-appearance: none;
    appearance: none;
    margin: 0;
  }
  .select-rate .loan-form select {
    width: 100%;
    padding: 10px 15px;
    border-radius: 0;
    position: relative;
    -webkit-appearance: none;
    -moz-appearance: none;
	-ms-appearance: none;
    appearance: none;
  }
  .select-rate .loan-form select::-ms-expand {
	  display: none;
  }
  .select-rate .loan-form #loanAmt:before {
    content: "$";
    position: absolute;
    top: 0;
    right: 20px;
  }
  .select-rate .loan-form button {
    margin: 8px 0;
    width: 100%;
  }
  .select-rate .loan-form .prefix-dollar {position: absolute; top: 92px; left: 50px; }
  .select-rate .loan-form .dd-purpose {position: absolute; top: 147px; left: 321px; }
  .select-rate .loan-form .dd-credit-quality {position: absolute; top: 202px; left: 321px; }
  #products {margin-top: 0 !important; border-bottom: none !important; }
  #products .product-ascend,
  #products .product-rr,
  #products .plus-sign {padding: 0 80px 0 20px; margin-left:30px !important; background: none !important; }
  #products .product-ascend p,
  #products .product-rr p {font-size: 14px; color: #696969; }
  #products .product-ascend h3,
  #products .product-rr h3 {margin: 5px auto; }
  #products .product-rr h3 span {
    font-size: 11px;
    color: #81c868;
    border: 1px solid #81c868;
    padding: 0px 5px;
    border-radius: 50%;

    position: relative;
    top: -4px;
  }

  .overview-features {text-align: center; padding-top: 25px; padding-bottom: 25px; background: rgba(0,0,0,0.2); }
  .overview-features ul {margin-bottom: 0; list-style: none; padding: 0; }
  .overview-features ul li {color: #81c868; display: inline-block; position: relative; padding: 0 30px 0 38px; margin-left: 30px; border-right: 1px solid rgba(255,255,255,0.3); line-height: 1; }
  .overview-features ul li:before {content: ''; position: absolute; top: -8px; left: 0; width: 30px; height: 30px; background-repeat: no-repeat; }
  /*.overview-features ul li:first-child {padding-left: 0; }*/
  .overview-features ul li:last-child {padding-right: 0; border-right: 0; }
  .features {background: transparent; padding: 35px 25px 25px; }
  .features p {font-size: 14px; }
  .features .feature-list {text-transform: none; font-size: 15px; }
  .media-list {background: #fff; border-bottom: 1px solid #eeefef; padding: 0; }
  .media-list ul {list-style: none; padding-left: 0; margin-bottom: 0; }
  .media-list ul li {display: inline; margin: 0 25px; }
  .noUi-origin {border: 1px solid #c0c0c0 !important; border-radius: 4px; }
  footer hr {border-top: 1px solid #d3dfe5; }


  .feature-lowpay:before {background-image: url('/img/v3/icon-lowpay.png'); }
  .feature-quote:before {background-image: url('/img/v3/icon-quote.png'); }
  .feature-funding:before {background-image: url('/img/v3/icon-funding.png'); }
  .feature-raterewards:before {background-image: url('/img/v3/icon-rewards.png'); }


  @media (min-width: 1500px) {
    .bg-main {
      background: url('/img/v3/banner.jpg') no-repeat scroll 94% -70px;
      background-size: cover;
    }
  }

  @media (min-width: 1800px) {
    .bg-main {
      background: url('/img/v3/banner.jpg') no-repeat scroll 94% -240px;
      background-size: cover;
    }
  }

  @media (max-width: 600px) {
    .bg-main {
      background: url('/img/v3/banner.jpg') no-repeat scroll 94% 0%;
      background-size: cover;
      height: 1150px;
    }
    header .navbar {
      background: #212b30;
      padding: 10px 0 0px;
      border-bottom: 1px solid #1d2629;bye
      margin-bottom: 25px;
    }
    a.navbar-brand-logo:after {
      content: url('/img/v2/logo.png');
    }
    .navbar-brand {
      padding: 0px 10px;
    }
    header .navbar-default .navbar-toggle {
      padding: 12px 0;
      width: 45px;
    }
    header .navbar-default .navbar-toggle .icon-bar {
      width: 25px;
      height: 3px;
      border-radius: 0;
    }
    .partner-brand-container {
      margin-top: 25px;
    }
    .partner-wrapper.slider-form-wrapper {
      min-height: 750px;
      margin: 0 15px 45px !important;
      padding: 0;
    }
    .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
      background: rgba(255, 255, 255, 0.9);
      margin-top: 15px;
      padding: 10px 10px 30px;
      box-shadow: none;
    }
    .partner-wrapper .partner-form .slogan {
      margin-bottom: 1em;
    }
    .partner-wrapper .partner-form .slogan h1 {
      font-weight: bold;
      font-size: 20px !important;
      padding: 15px;
      margin-top: 0;
    }
    .partner-wrapper .partner-form .slogan h1 small {
      font-size: 20px;
      font-weight: 300;
      display: inline-block;
      margin-top: 8px;
      line-height: 1.4em;
    }
    .partner-wrapper .select-rate .slider-container {
      padding-left: 15px !important;
    }
    .select-rate h1.show-mobile {display: block; line-height: 0.5; }
    .select-rate h1:not(.show-mobile) {display: none; }
    .partner-wrapper .select-rate h1 {
      margin-bottom: 15px;
      font-size: 14px;
      font-weight: bold;
    }
    .partner-wrapper .select-rate h1 span {font-size: 24px; }
    .partner-wrapper .wont-impact-mobile {
      width: 98% !important;
      text-align: center;
      margin-top: -5px;
      padding-left: 0;
    }
    .select-rate .loan-form {
      margin: 0 -10px;
      border: 0 none;
      border-bottom: 1px solid #ccc;
      position: relative;
      top: -340px;
    }
    .select-rate .loan-form .prefix-dollar {position: absolute; top: 71px; left: 35px; }
    .select-rate .loan-form .dd-purpose {position: absolute; top: 126px; left: auto; right: 36px; }
    .select-rate .loan-form .dd-credit-quality {position: absolute; top: 182px; left: auto; right: 36px; }
    .select-rate .check-wrapper {
      border-radius: 4px;
    }
    .slider-wrapper {
      width: 75%;
      padding-top: 5px;
    }
    .partner-wrapper .select-rate .check-rate-wrapper {
      padding-top: 0;
    }
    .partner-wrapper .btn-cta-check-rate {
      width: 100%;
    }
    .partner-form #products {
      position: relative;
      top: 320px;
      z-index: 1;
    }
    #products .product-ascend,
    #products .product-rr,
    #products .plus-sign {
      background: transparent;
      padding: 0 25px;
      margin-left: 0 !important;
    }
    #products .plus-sign {
      line-height: 1em;
    }
    .media-list {
      line-height: 3em;
    }
    .media-list ul {margin-bottom: 15px; }
    .media-list ul li:first-child {
      width: 100%;
    }
    .media-list ul li {
      display: inline-block;
      margin: 0;
      width: 48%;
    }
    .media-list ul li img {max-width: 100%; padding: 0 10px; }
    #howitworks {padding: 35px 15px 25px; }
    #howitworks .overview {padding-bottom: 2em; }
    #howitworks h1, #aboutus h1, #borrowers h1 {font-size: 24px; }
    #howitworks p, #aboutus p, #borrowers p {font-size: 18px; }
    #howitworks .row-fluid .col-sm-3 {margin-bottom: 2em; }
    #howitworks .row-fluid .feature-list {font-size: 18px; }
    #howitworks .row-fluid p {font-size: 15px; }
    #howitworks .learn-more-wrapper {margin-top: 0; }

    .footer-links {
      margin-top: 30px;
    }
    .footer-links hr {
      border-top: 1px solid #ccc;
    }
    .footer-links #footer-contact {
      margin-top: 20px;
    }
    footer .disclaimer {
      padding-top: 0;
      text-align: left;
    }
    #aboutus, #borrowers {padding-top: 2em; padding-bottom: 2em; }
    #aboutus .inner-container, #howitworks .inner-container {padding-top: 20px; }
    .overview-features {padding-top: 15px; padding-bottom: 10px; }
    .overview-features ul li {
      width: 45%;
      padding: 0 0 0 38px;
      margin: 14px 0;
      text-align: left;
      border-right: 0 none;
    }
    .overview-features ul li.feature-funding:before,
    .overview-features ul li.feature-raterewards:before {top: 0px; }
  }

  @media (max-width: 767px){
    .partner-wrapper .wont-impact-mobile {
      width: auto;
    }
    header .navbar-default .navbar-toggle {
      margin-right: 0;
    }
    #creditCriteria .modal-content {
      width: 100%;
    }
  }

  @media (max-width: 414px) {
    .select-rate .loan-form {top: -350px; }
    #products .product-ascend h3, #products .product-rr h3 {font-size: 20px; }
  }

  @media (max-width: 375px) {
    .select-rate .loan-form {top: -370px; }
  }

  @media (max-width: 360px) {
    .partner-wrapper .partner-form .slogan h1 {padding-left: 0; padding-right: 0; }
  }

  @media (max-width: 320px) {
    .select-rate .loan-form {top: -400px; }
    #products .product-ascend h3, #products .product-rr h3 {font-size: 20px; }
    .partner-wrapper .select-rate h1 {font-weight: normal; }
    .partner-wrapper .select-rate h1 span {font-size: 20px; line-height: 0.4; }
    .partner-wrapper .partner-form .slogan h1 { font-size: 22px !important; line-height: 1.4em; }
    .partner-wrapper .partner-form .slogan h1 small {font-size: 17px; line-height: 1.2em; }
    .partner-wrapper .wont-impact-mobile {margin-left: -15px; }
    .btn-cta-check-rate {padding: 10px; }
    .overview-features ul li {font-size: 13px; }
  }
</style>

<!--[if lte IE 9]>
<style>
	.dd-purpose, .dd-credit-quality {display: none; }
</style>
<!-- <![endif]-->

{{HTML::style('css/jquery.nouislider.min.css')}}
{{HTML::style('css/jquery.nouislider.pips.min.css')}}


<div class="container inner-container banner partner-banner" style="margin-top: -15px;">
<div class="slider-form-wrapper partner-wrapper">

{{ Form::open(array('url' => '/register', 'method' => 'POST', 'id' => 'LoanApplicationForm','class' => 'form-inline LoanApplicationForm-partner partner-form' , 'role' => 'form')) }}
 <div class="partner slogan">
	<h1>Get a low rate and payment.<br><small>Add RateRewards to lower your payment even more.</small></h1>
</div>
<div class="row">
<!-- products -->
        <div id="products" class="products col-sm-6">
          <div class="product-ascend col-sm-12 text-center">
            <span class="icon icon-ascend"></span>
            <h3>Ascend Personal Loan</h3>
            <p>A standard loan with low monthly payment</p>
          </div>
          <div class="col-sm-12 text-center plus-sign">+</div>
          <div class="product-rr col-sm-12 text-center">
            <span class="icon icon-ratereward"></span>
            <h3>RateRewards <span data-toggle="tooltip" data-placement="top" title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost" class="rwar">?</span></h3>
            <p>Optional reward program to reduce interest costs by up to 50%.</p>
          </div>
        </div>

        <div class="select-rate col-sm-6">
          <div class="loan-form">
            <h1>Personal <br>loans up to <span>${{ number_format($sliderMaxLoanAmt, 0) }}</span></h1>
            <h1 class="show-mobile">Personal loans up to <span>${{ number_format($sliderMaxLoanAmt,0) }}</span></h1>
            <input type="number" id="loanAmt" name="loanAmt" placeholder="Enter loan amount (${{ number_format($sliderMinLoanAmt,0) }}-${{ number_format($sliderMaxLoanAmt,0) }})" required="required" min="{{ $sliderMinLoanAmt }}" max="{{ $sliderMaxLoanAmt }}" class="number-only" data-toggle="tooltip" data-placement="bottom" />
            <select id="loanPurposeId" name="loanPurposeId" required="required" data-toggle="tooltip" data-placement="bottom">
              <option value="">Select the purpose of your loan</option>
              <option value="1">Debt Consolidation</option>
              <option value="2">Emergency Expense</option>
              <option value="3">Vacation</option>
              <option value="4">Medical</option>
              <option value="5">Auto Expense</option>
              <option value="6">Everyday Expense</option>
              <option value="7">Home Improvement</option>
              <option value="8">Other</option>
            </select>
            <select id="creditquality" name="creditquality" required="required" data-toggle="tooltip" data-placement="bottom">
              <option value="">Select your credit quality</option>
              <option value="Excellent (720+)">Excellent (720+)</option>
              <option value="Good (680 - 719)">Good (680 - 719)</option>
              <option value="Fair (640 - 679)">Fair (640 - 679)</option>
              <option value="Poor (639 or less)">Poor (639 or less)</option>
            </select>

            <!-- added for home.js to work -->
            <div id="slider-tooltip" style="display: none;"></div>

            <!-- check rate button/wrapper -->
            <div class="check-wrapper">
              <div class="check-rate-wrapper">
                <button class="btn btn-cta btn-cta-check-rate">
                  <span class="icon icon-lock-v2"></span>Check your Rate
                </button>
              </div>
              <div class="wont-impact"></div>
              <div class="wont-impact-mobile" align="center">
                {{HTML::image( asset('/img/v3/impact-mobile2.png') )}}
              </div>
            </div>

            <!-- icon placeholders -->
            <span class="prefix-dollar">$</span>
            <span class="dd-purpose"><img src="/img/v3/dropdown-arrow.png" /></span>
            <span class="dd-credit-quality"><img src="/img/v3/dropdown-arrow.png" /></span>
          </div>
        </div>
      </div>

			<input type="hidden" name="TotalMonthlyReward" value="" id="TotalMonthlyReward">
			<!--<input type="hidden" name="loanAmt" value="" id="loanSliderInput">-->
			<!--<input type="hidden" name="loanPurposeId" value="1" id="loanPurposeInput">-->
			<input type="hidden" name="loanPurposeTxt" value="Debt Consolidation" id="loanPurposeTxt">
			<input type="hidden" name="loanProductId" value="1" id="loanTypeInput">
			<input type="hidden" name="loanProductTxt" value="Ascend Loan" id="loanTypeText">
			<input type="hidden" name="isProductionQA" value="{{ $isProductionQA }}" id="isProductionQA">
			<input type="hidden" name="isProductionBypass" value="{{ $isProductionBypass }}" id="isProductionBypass">

			{{ Form::close() }}

      <div class="clearfix"></div>
		</div>
 	</div>

  <!-- overview features -->
  <div class="overview-features">
    <ul>
      <li class="feature-lowpay">Low payment</li>
      <li class="feature-quote">Quote in minutes</li>
      <li class="feature-funding">Funding by tomorrow</li>
      <li class="feature-raterewards">Customize with RateRewards</li>
    </ul>
  </div>

  <div id="media" class="media-list">
    <ul>
      <li>Featured in:</li>
      <li>{{ HTML::image(asset('img/media-bloomberg.png') ) }}</li>
      <li>{{ HTML::image(asset('img/media-thestreet.png') ) }}</li>
      <li>{{ HTML::image(asset('img/media-AB.png') ) }}</li>
      <li>{{ HTML::image(asset('img/media-pymnts.png') ) }}</li>
    </ul>
  </div>

<div id="howitworks" class="features">
  <div class="container inner-container">
    <h1>All borrowers can get a great rate with an Ascend Personal Loan</h1>
    <hr class="centered-line">
    <p class="overview">Borrowers who are better than their credit score can enroll in our RateRewards Program that lets great borrowers reduce their interest costs up to 50%.</p>

    <div class="row-fluid">
      <div class="col-sm-3">
        <span class="features-icon features-icon1"></span>
        <p class="feature-list">1. GET YOUR ASCEND PERSONAL LOAN</p>
        <p>Get an Ascend Personal Loan with a low rate and payment that will never change.</p>

      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon2"></span>
        <p class="feature-list">2. ENROLL IN OPTIONAL RATEREWARDS PROGRAM</p>
        <p>Better borrowers can enroll in the optional RateRewards Program.</p>
      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon3"></span>
        <p class="feature-list">3. EARN REWARDS</p>
        <p>Earn rewards each month through responsible financial behaviors.</p>
      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon4"></span>
        <p class="feature-list">4. LOWER YOUR PAYMENT</p>
        <p>Your monthly interest costs and total payment will be reduced by the reward you earn.</p>
      </div>

      <div class="clearfix"></div>
    </div>

    <div class="row text-center learn-more-wrapper">
      {{ HTML::link('howItWorks', 'Learn More', array('class' => 'btn btn-learn') )}}
      {{-- <button class="btn btn-learn">
        Learn More
      </button> --}}
    </div>
  </div>
</div>

	<div id="aboutus" class="about-us">
		<div class="container inner-container">
			<div class="row-fluid">
				<h1>Who is Ascend?</h1>
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit scores indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost. <br>
				It's that simple.
				</p>
			</div>
		</div>
	</div>


	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1>
				<hr class="centered-line">
			</div>


			{{-- carousel --}}

			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">

      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>

				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li>
								</ul>


							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>

				</div>


			  </div>

			</div>


			{{-- carosel --}}

			<div class="clearfix"></div>

		</div>
	</div>

@stop

@section('scripts')
	{{HTML::script('js/common.js');}}
	{{HTML::script('js/bootstrap.min.js');}}
	{{HTML::script('js/jquery.nouislider.all.min.js')}}
	{{HTML::script('js/home.js')}}

<script type="text/javascript">
	//http://stackoverflow.com/questions/17479573/html5-required-attribute-on-non-supported-browsers
	if ($("<input />").prop("required") === undefined) {
		$(document).on("submit", function(event) {

			$('[data-toggle=tooltip]').each(function() {
				$(this).attr('title', '');
				$(this).attr('data-original-title', '');
			});

			var $loanAmt = $('#loanAmt');
			var minAmt = parseInt($loanAmt.attr('min'));
			var maxAmt = parseInt($loanAmt.attr('max'));

			//Validate for min amount
			if ($loanAmt.val().trim() != '' && $loanAmt.val() < minAmt) {
				$loanAmt.attr('data-original-title', 'Value must be greater than or equal to ' + minAmt);
				$loanAmt.tooltip('show');
				return false;
			}
			//Validate for max amount
			else if ($loanAmt.val().trim() != '' && $loanAmt.val() > maxAmt) {
				$loanAmt.attr('data-original-title', 'Value must be less than or equal to ' + maxAmt);
				$loanAmt.tooltip('show');
				return false;
			}
			else {
				$(this)
					.find("input, select, textarea")
					.filter("[required]")
					.filter(function() { return this.value == ''; })
					.each(function() {
						event.preventDefault();
						var title = ($(this).prop('name') == 'loanAmt') ? 'Please fill out this field.' : 'Please select an item in the list.';
						$(this).attr('data-original-title', title);
						$(this).tooltip('show');
						return false;
					});
			}
		});
	}
</script>
@stop