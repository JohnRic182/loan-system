@section('bg')
	<div class="bg-main"></div>
@stop
@section('content')

  <script type="text/javascript">
    var DEFAULT_SLIDER_VAL = {{ $defaultSliderAmt }};
    var MINIMUM_LOAN_AMT   = {{ $sliderMinLoanAmt }};
    var MAXIMUM_LOAN_AMT   = {{ $sliderMaxLoanAmt }};
  </script>
<style>
  a.navbar-brand-logo:after {content: url('/img/v6/logo.png'); }
  .bg-main {background: url('/img/v6/banner.jpg') no-repeat scroll 100% 0%; background-size: cover; height: 900px; }
  .navbar-default .navbar-nav > li > a {color: #374044 !important; }
  .cta-link {color: #374044; }
  .btn-cta-check-rate {padding: 10px 45px; width: 100%; }
  .partner-wrapper {margin: 40px auto 155px !important; max-width: 100%; }
  .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
    background: rgba(255,255,255,0.2);
    box-shadow: 0 0 25px rgba(180,180,180,0.1);
    margin-left: 360px;
    padding: 1px 10px 50px;
  }
  .partner-banner {padding:0; }
  .partner-form .slogan {margin-bottom: 3em; }
  .partner-form .slogan h1 {width: 100%; padding-left: 40px; padding-right: 40px; font-size: 40px !important; font-weight: bold; color: #374044; }
  .partner-form .slogan h1 small {font-size: 24px; font-weight: normal; color: inherit; }
  .partner-wrapper .wont-impact {margin-left: 6px; margin-top: -20px; background: url('/img/v6/impact.png') no-repeat; width: 283px; height: 50px; display: block; }

  .select-rate-header h1 {font-size: 18px; font-weight: bold; color: #374044; text-transform: uppercase; margin-bottom: 1.4em; text-align: center; }
  .select-rate-header h1.show-mobile {display: none; }
  .select-rate .slider-container {padding-left: 40px; padding-right: 15px; position: relative; }
  .select-rate .slider-container .slider-label {color: #999; font-size: 12px; position: absolute; top: 40px; }
  .select-rate .slider-container .slider-label.label-right {right: 57px; }
  .select-rate .check-wrapper {text-align: center; padding-top: 1em; padding-left: 0; padding-right: 40px; }

  .partner-form #products {margin-top: 0; border-bottom: 0 none; }
  .partner-form #products.show-mobile {display: none; }
  .partner-form #products .plus-sign {margin-left: 0; }
  .partner-form #products .product-info h3 {color: #2996cc; font-size: 20px !important; }
  .partner-form #products .product-info-rr span {
    font-size: 11px;
    color: #81c868;
    border: 1px solid #81c868;
    padding: 0px 5px;
    position: relative;
    top: -3px;
  }
  .partner-form #products .product-info-rr .tooltip {
    width: 200px !important;
  }
  .partner-form #products .product,
  .partner-form .product-ascend,
  .partner-form .product-rr {max-width: 305px; background: transparent; }

  .overview-features {text-align: center; padding-top: 25px; padding-bottom: 25px; background: rgba(0,0,0,0.4); }
  .overview-features ul {margin-bottom: 0; list-style: none; padding: 0; }
  .overview-features ul li {color: #fff; display: inline-block; position: relative; padding: 0 30px 0 38px; margin-left: 30px; border-right: 1px solid rgba(255,255,255,0.3); line-height: 1; }
  .overview-features ul li:before {content: ''; position: absolute; top: -8px; left: 0; width: 30px; height: 30px; background-repeat: no-repeat; }
  /*.overview-features ul li:first-child {padding-left: 0; }*/
  .overview-features ul li:last-child {padding-right: 0; border-right: 0; }

  .features {background: transparent; padding: 35px 25px 25px; }
  .features p {font-size: 14px; }
  .features .feature-list {text-transform: none; font-size: 15px; }
  .media-list {background: #fff; border-bottom: 1px solid #eeefef; padding: 0; }
  .media-list ul {list-style: none; padding-left: 0; margin-bottom: 0; }
  .media-list ul li {display: inline; margin: 0 25px; }
  .noUi-origin {border: 1px solid #c0c0c0 !important; border-radius: 4px; }
  footer hr {border-top: 1px solid #d3dfe5; }

  .feature-lowpay:before {background-image: url('/img/v5/icon-lowpay.png'); }
  .feature-quote:before {background-image: url('/img/v5/icon-quote.png'); }
  .feature-funding:before {background-image: url('/img/v5/icon-funding.png'); }
  .feature-raterewards:before {background-image: url('/img/v5/icon-rewards.png'); }

  @media (min-width: 1500px) {
    .bg-main {
      background: url('/img/v6/banner.jpg') no-repeat scroll 100% -100px;
      background-size: cover;
    }
  }

  @media (min-width: 1800px) {
    .bg-main {
      background: url('/img/v6/banner.jpg') no-repeat scroll 100% -180px;
      background-size: cover;
    }
  }

  @media (max-width: 1200px) {
    .bg-main {
      background: url('/img/v6/banner.jpg') no-repeat scroll 64% 0%;
      background-size: cover;
    }
    .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
      margin-left: 234px;
    }
  }

  @media (max-width: 991px) {
    .bg-main {
      background: url('/img/v6/banner.jpg') no-repeat scroll 0% 0%;
      background-size: cover;
    }
    .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
      background: rgba(255,255,255,0.8);
      margin-left: 0;
    }
    .select-rate .slider-container .slider-label.label-right {
      right: 35px;
    }
  }

  @media (max-width: 600px) {
    a.navbar-brand-logo:after {
      content: url('/img/v2/logo.png');
    }
    .navbar-brand {
      padding: 0px 10px;
    }
    .navbar-default .navbar-nav > li > a {
      color: #fff !important;
    }
    .bg-main {
      background: url('/img/v6/banner.jpg') no-repeat scroll 0% 0%;
      background-size: cover;
    }
    header .navbar {
      background: #212b30;
      padding: 10px 0 0px;
      border-bottom: 1px solid #1d2629;
      margin-bottom: 25px;
    }
    header .navbar-default .navbar-toggle {
      padding: 12px 0;
      width: 45px;
    }
    header .navbar-default .navbar-toggle .icon-bar {
      width: 25px;
      height: 3px;
      border-radius: 0;
    }
    .partner-brand-container {
      margin-top: 25px;
    }
    .partner-wrapper.slider-form-wrapper {
      margin: 0 15px 60px !important;
      padding: 0;
    }
    .partner-wrapper.slider-form-wrapper #LoanApplicationForm {
      background: rgba(255,255,255,0.9);
      color: #fff;
      margin-top: 0;
      margin-left: 0;
      padding: 1px 10px 30px;
      box-shadow: none;
    }
    .partner-wrapper .partner-form .slogan {
      margin-bottom: 1em;
    }
    .partner-wrapper .partner-form .slogan h1 {
      font-size: 28px !important;
      padding: 25px 15px 15px;
      margin-top: 0;
    }
    .partner-wrapper .partner-form .slogan h1 small {
      font-size: 18px;
      font-weight: 300;
      display: inline-block;
      margin-top: 8px;
      line-height: 1.4em;
    }
    .partner-wrapper .select-rate {
      position: relative;
      top: -220px;
    }
    .partner-wrapper .select-rate .slider-container {
      padding-left: 15px !important;
    }
    .partner-wrapper .select-rate h1 {
      margin-bottom: 15px;
      font-size: 14px;
      font-weight: bold;
      text-align: center;
    }
    .select-rate-header h1:not(.show-mobile) {display: none; }
    .select-rate-header h1.show-mobile {display: block; }
    .partner-wrapper .select-rate .slider-container .slider-label {
      display: none;
    }
    .partner-wrapper .select-rate-header {
      position: relative;
      top: -200px;
    }
    .partner-wrapper .partner-form .product-ascend,
    .partner-wrapper .partner-form .product-rr,
    .partner-wrapper .partner-form #products .product {
      background: transparent;
    }
    .partner-wrapper .partner-form #products {
      position: relative;
      top: 195px;
    }
    .partner-wrapper .partner-form #products.show-mobile {display: block; }
    .partner-wrapper .partner-form #products:not(.show-mobile) {display: none; }
    .partner-wrapper .partner-form #products .product > .col-sm-2 {
      margin-left: 0;
      margin-right: 5px;
      margin-bottom: 15px;
      float: left;
    }
    .icon-ascend {background: url('/img/v2/icons.png') scroll -7px -74px transparent no-repeat; }
    .partner-wrapper .partner-form #products .product .product-info * {
      text-align: left;
    }
    .partner-wrapper .partner-form #products .plus-sign {
      text-align: left;
      margin-left: 15px !important;
      margin-bottom: 18px !important;
      line-height: 0.2;
    }
    .partner-wrapper .wont-impact-mobile {
      text-align: left;
      margin-top: -5px;
      padding-left: 0;
    }
    .select-rate .check-wrapper {
      padding: 5px 15px;
      margin: 15px 5px;
      border-radius: 4px;
    }
    .slider-wrapper {
      width: 75%;
      padding-top: 5px;
    }
    .partner-wrapper .select-rate .check-rate-wrapper {
      padding-top: 0;
    }
    .partner-wrapper .btn-cta-check-rate {
      width: 100%;
    }
    .media-list {
      line-height: 3em;
    }
    .media-list ul {margin-bottom: 15px; }
    .media-list ul li:first-child {
      width: 100%;
    }
    .media-list ul li {
      display: inline-block;
      margin: 0;
      width: 48%;
    }
    .media-list ul li img {max-width: 100%; padding: 0 10px; }

    .footer-links {
      margin-top: 30px;
    }
    .footer-links hr {
      border-top: 1px solid #ccc;
    }
    .footer-links #footer-contact {
      margin-top: 20px;
    }
    footer .disclaimer {
      padding-top: 0;
      text-align: left;
    }
    #aboutus, #borrowers {padding-top: 2em; padding-bottom: 2em; }
    #aboutus h1, #borrowers h1 {font-size: 28px; }
    #aboutus .inner-container, #howitworks .inner-container {padding-top: 20px; }

    .overview-features {padding-top: 15px; padding-bottom: 10px; }
    .overview-features ul li {
      width: 45%;
      padding: 0 0 0 38px;
      margin: 14px 0;
      text-align: left;
      border-right: 0 none;
    }
    .overview-features ul li.feature-funding:before,
    .overview-features ul li.feature-raterewards:before {top: 0px; }
  }

  @media (max-width: 767px){
    .partner-wrapper .wont-impact-mobile {
      width: auto;
    }
    header .navbar-default .navbar-toggle {
      margin-right: 0;
    }
    #creditCriteria .modal-content {
      width: 100%;
    }
  }

  @media (max-width: 414px) {
  }

  @media (max-width: 360px) {

    .partner-wrapper .partner-form .slogan h1 {
      padding-left: 0;
      padding-right: 0;
    }
  }

  @media (max-width: 320px) {
    .partner-wrapper .select-rate-header h1 {font-size:14px; }
    .partner-wrapper .partner-form .slogan h1 {font-size: 24px !important; font-weight: bold; line-height: 1.2em; }
    .partner-wrapper .partner-form .slogan h1 small {font-size: 17px; line-height: 1.2em; }
    .partner-form #products .product-info h3 {font-size: 18px !important; }
    .btn-cta-check-rate {padding: 10px; }
    .overview-features ul li {font-size: 13px; }
  }
</style>

    {{HTML::style('css/jquery.nouislider.min.css')}}
 	{{HTML::style('css/jquery.nouislider.pips.min.css')}}


	<div class="container inner-container banner partner-banner" style="margin-top: -15px;">
 		<div class="slider-form-wrapper partner-wrapper">

		{{ Form::open(array('url' => '/register', 'method' => 'POST', 'id' => 'LoanApplicationForm','class' => 'form-inline LoanApplicationForm-partner partner-form' , 'role' => 'form')) }}
 			<div class="partner slogan">
			    <h1>Get a low rate and payment.<br><small>Add RateRewards to lower your payment even more.</small></h1>
			</div>

      <div id="products" class="row">
        <div class="product-ascend text-center">
          <div class="product row-fluid">
            <div class="col-sm-2 nopad-right nopad-left">
              <span class="icon icon-ascend"></span>
            </div>
            <div class="col-sm-9 product-info nopad-right nopad-left">
              <h3>Ascend Personal Loan</h3>
              <p> A standard loan with low monthly payment.</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="text-center plus-sign">+</div>
        <div class="product-rr text-center">
          <div class="product row-fluid">
            <div class="col-sm-2 nopad-right nopad-left">
              <span class="icon icon-ratereward"></span>
            </div>
            <div class="col-sm-9 product-info product-info-rr nopad-right nopad-left">
              <h3>RateRewards Program <span data-toggle="tooltip" data-placement="top" title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost" class="rwar">?</span></h3>
              <p>Reduce interest cost by up to 50% (optional).</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

      <div id="products" class="row show-mobile">
        <div class="product-ascend text-center">
          <div class="product row-fluid">
            <div class="col-sm-2 nopad-right nopad-left">
              <span class="icon icon-ascend"></span>
            </div>
            <div class="col-sm-9 product-info nopad-right nopad-left">
              <h3>Ascend Personal Loan</h3>
              <p>Constant low interest rate &amp; payment</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <div class="text-center plus-sign">+</div>
        <div class="product-rr text-center">
          <div class="product row-fluid">
            <div class="col-sm-2 nopad-right nopad-left">
              <span class="icon icon-ratereward"></span>
            </div>
            <div class="col-sm-9 product-info product-info-rr nopad-right nopad-left">
              <h3>Raterewards <span data-toggle="tooltip" data-placement="top" title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost" class="rwar">?</span></h3>
              <p>Reduce your interest cost up to 50%.</p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>

      <div class="select-rate-header">
        <h1>Select your loan amount:</h1>
        <h1 class="show-mobile">Desired loan amount:</h1>
      </div>

      <div class="select-rate row">
        <div class="col-sm-7 slider-container">
          <div class="partner-slider-version">
            <div class="text-center slider-wrapper">
              <div id="slider-tooltip"></div>
            </div>
          </div>
          <div class="slider-label label-left">${{ number_format($sliderMinLoanAmt, 0 )}}</div>
          <div class="slider-label label-right">${{ number_format($sliderMaxLoanAmt, 0 )}}</div>
        </div>

        <div class="col-sm-5 check-wrapper">

          <div class="check-rate-wrapper">
            <button class="btn btn-cta btn-cta-check-rate">
              <span class="icon icon-lock-v2"></span>Check your Rate
            </button>
          </div>
          <div class="wont-impact"></div>
          <div class="wont-impact-mobile" align="center">
            {{HTML::image( asset('/img/v3/impact-mobile.png') )}}
          </div>
        </div>
      </div>

			<input type="hidden" name="TotalMonthlyReward" value="" id="TotalMonthlyReward">
			<input type="hidden" name="loanAmt" value="" id="loanSliderInput">
			<input type="hidden" name="loanPurposeId" value="0" id="loanPurposeInput">
			<input type="hidden" name="loanPurposeTxt" value="" id="loanPurposeTxt">
			<input type="hidden" name="loanProductId" value="1" id="loanTypeInput">
			<input type="hidden" name="loanProductTxt" value="Ascend Loan" id="loanTypeText">
			<input type="hidden" name="creditquality" value="" id="creditquality">
			<input type="hidden" name="isProductionQA" value="{{ $isProductionQA }}" id="isProductionQA">
			<input type="hidden" name="isProductionBypass" value="{{ $isProductionBypass }}" id="isProductionBypass">

			{{ Form::close() }}

      <div class="clearfix"></div>
		</div>
 	</div>

<!-- overview features -->
<div class="overview-features">
  <ul>
    <li class="feature-lowpay">Low payment</li>
    <li class="feature-quote">Quote in minutes</li>
    <li class="feature-funding">Funding by tomorrow</li>
    <li class="feature-raterewards">Customize with RateRewards</li>
  </ul>
</div>

<div id="media" class="media-list">
  <ul>
    <li>Featured in:</li>
    <li>{{ HTML::image(asset('img/media-bloomberg.png') ) }}</li>
    <li>{{ HTML::image(asset('img/media-thestreet.png') ) }}</li>
    <li>{{ HTML::image(asset('img/media-AB.png') ) }}</li>
    <li>{{ HTML::image(asset('img/media-pymnts.png') ) }}</li>
  </ul>
</div>

<div id="howitworks" class="features">
  <div class="container inner-container">
    <h1>All borrowers can get a great rate with an Ascend Personal Loan</h1>
    <hr class="centered-line">
    <p class="overview">Borrowers who are better than their credit score can enroll in our RateRewards Program that lets great borrowers reduce their interest costs up to 50%.</p>

    <div class="row-fluid">
      <div class="col-sm-3">
        <span class="features-icon features-icon1"></span>
        <p class="feature-list">1. GET YOUR ASCEND PERSONAL LOAN</p>
        <p>Get an Ascend Personal Loan with a low rate and payment that will never change.</p>

      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon2"></span>
        <p class="feature-list">2. ENROLL IN OPTIONAL RATEREWARDS PROGRAM</p>
        <p>Better borrowers can enroll in the optional RateRewards Program.</p>
      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon3"></span>
        <p class="feature-list">3. EARN REWARDS</p>
        <p>Earn rewards each month through responsible financial behaviors.</p>
      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon4"></span>
        <p class="feature-list">4. LOWER YOUR PAYMENT</p>
        <p>Your monthly interest costs and total payment will be reduced by the reward you earn.</p>
      </div>

      <div class="clearfix"></div>
    </div>

    <div class="row text-center learn-more-wrapper">
      {{ HTML::link('howItWorks', 'Learn More', array('class' => 'btn btn-learn') )}}
      {{-- <button class="btn btn-learn">
        Learn More
      </button> --}}
    </div>
  </div>
</div>

	<div id="aboutus" class="about-us">
		<div class="container inner-container">
			<div class="row-fluid">
				<h1>Who is Ascend?</h1>
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit scores indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost. <br>
				It's that simple.
				</p>
			</div>
		</div>
	</div>


	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1>
				<hr class="centered-line">
			</div>


			{{-- carousel --}}

			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">

      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>

				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li>
								</ul>


							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>

				</div>


			  </div>

{{-- 			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
			    <span class="icon icon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
			    <span class="icon icon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a> --}}


			</div>


			{{-- carosel --}}

			<div class="clearfix"></div>

		</div>
	</div>

@stop

@section('scripts')
	{{HTML::script('js/common.js');}}
	{{HTML::script('js/bootstrap.min.js');}}
	{{HTML::script('js/jquery.nouislider.all.min.js')}}
	{{HTML::script('js/home.js')}}
@stop