@section('content') 
<div class="appl-title">
    <div class="container">
    	<h1> Reset Username </h1>
    </div> 
</div> 
<div id="application" class="credit-pull container"> 
	<div class="box-wrapper">
	 	<div class="row">
	 		<div class="col-sm-8">
	 			<p>&nbsp;</p>
	 			<div id="notification"></div> 
					{{ Form::open(array('url' => 'reset-username', 'id' => 'resetUsername', 'method' => 'POST', 'class' => 'form-horizontal validate' , 'role' => 'form')) }} 
					
					@if($errors->any())
						<div class="alert alert-danger hide" role="alert">
							{{$errors->first()}}
						</div>
					@endif

					<div class="form-group">
						{{ Form::label('id', 'New Username', array(  'class' => 'col-sm-3 control-label',)); }}
						<input name="key" type="hidden" value="<?php echo $key; ?>">
						<div class="col-sm-6">
							{{ Form::text('username', '', array( 'class' => 'form-control usernameCheck usernameFormat required', 'actionUrl' => 'checkDuplicate', 'alt' =>'User Name',  'alpha_num' => '', 'placeholder' => 'Create a username', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => 'At least 8 characters, including one capital letter, one number, and one special character.' ) ) }}
						</div>
						<div class="alert alert-danger error-password col-sm-4 hide" role="alert">
							Invalid Username.
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-9">
						 {{ Form::submit('Send', array('class' => 'btn btn-cta pull-right') ) }}
						</div>
					</div>
					{{ Form::close() }}
	 		</div>
	 		<div class="col-sm-4">
	 			@include('includes.sidebar')
	 		</div>
	 	</div> 
	</div>
</div>
@stop

@section('scripts')
	{{ HTML::script( 'js/jquery.typing-0.2.0.min.js');  }}
	{{ HTML::script( 'js/validate.js'); }}
	{{ HTML::script( 'js/register.js');  }}
@stop