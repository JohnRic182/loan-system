@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop

@section('content')  
	<div class="appl-title">
	    <div class="container">
	    	<h1> User Login </h1>
	    </div> 
	</div> 
	<div id="application" class="container">
	{{ Form::open(array('url' => '/login', 'method' => 'POST', 'class' => 'form-horizontal validate' , 'role' => 'form', 'id' => 'UserLoginForm')) }} 
		<div class="box-wrapper"> 
			<div class="row">
				<div class="col-sm-12 banner-adjustment">
					<p>&nbsp;</p>
			 		<div id="notification">
						@if(isset($message))
							<div class="alert alert-danger">{{ $message }}</div>
						@endif
					</div>   
					{{-- <div class="form-group">
						{{ Form::label('id', 'Username', array(  'class' => 'col-sm-2 control-label',)); }}
						<div class="col-sm-4">
							{{ Form::text('User_Name', '', array( 'placeholder' => 'Username', 'class' => 'form-control required', 'alt' => 'User Name' ) ) }}
						</div>
					</div>--}}

					<div class="form-group">
						{{ Form::label('id', 'Email Address', array(  'class' => 'col-sm-2 control-label',)); }}
						<div class="col-sm-4">
							{{ Form::email('Email', '', array( 'placeholder' => 'Email Address', 'class' => 'form-control required', 'alt' => 'Email Address' ) ) }}
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('id', 'Password' , array('class' => 'col-sm-2 control-label')); }}
						<div class="col-sm-4">
							{{ Form::password('Password', array('placeholder' => 'Password', 'class' => 'form-control required' , 'min' => '8','max' => '20' , 'alt' => 'Password' )) }}
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-4">
              			  {{ HTML::link('/forgotPassword', 'Forgot Password') }}<br>
						  Not yet registered? {{ HTML::link('/register', 'Sign Up') }}
						 <br>
						{{-- {{ HTML::link('/forgotUser', 'Forgot Username') }} --}}
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
						 	
						</div>
						<div class="col-sm-4">
						 {{ Form::submit('Login', array('class' => 'btn btn-cta') ) }}
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="action-section row">
			<div class="pull-left">
				&nbsp;
			</div>
			<div class="pull-right"> 
			</div>
			<div class="clearfix"></div>
		</div>
	{{ Form::close() }}
	</div> 

	<div class="modal fade" id="securityQuestionsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	   	  {{ Form::open(array('url' => '/', 'method' => 'POST', 'class' => 'form-horizontal validateModal' , 'role' => 'form', 'id' => 'SecurityQuestionForm')) }} 
	      <div class="modal-header">
	    		<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -8px;"><span aria-hidden="true">&times;</span></button>
	    	</div>
	      <div class="modal-body">	      		 
	          	<h3><span class="icon icon-account"></span>To secure your personal loan history,we require you to set up security questions</h3>
	          	<div id="notificationModal">
					@if(isset($message))
						<div class="alert alert-danger">{{ $message }}</div>
					@endif
				</div>   
	          	<div class="form-group col-sm-6 nopad-right">
					<div class="col-sm-12">
						{{ Form::label('id', 'Security Question 1', array('class' => 'securityLabel')) }} 
						{{ Form::select('securityQuestion1', $security, 1, array('class' => 'form-control required question')) }}

					</div>
					<div class="col-sm-12">
						{{ Form::label('id', '&nbsp;'); }}
						{{ Form::text('securityQuestionAnswer1', '', array( 'id' => 'securityQuestionAnswer1', 'class' => 'form-control required','alt' =>'Security Answer 1',  'alpha_num' => '', 'placeholder' => 'Enter your answer here' ) ) }}

						{{ Form::text('securityQuestionAnswer1', '', array( 'id' => 'securityQuestionAnswer1_', 'disabled' => 'disabled' , 'class' => 'answerFormat hide form-control required','alt' =>'Security Answer 1',  'alpha_num' => '', 'placeholder' => 'Enter a phone number' ) ) }}
					</div>
				</div>

				<div class="form-group col-sm-6 nomargin-left security">
					<div class="col-sm-12 nopad-right">
						{{ Form::label('id', 'Security Question 2', array('class' => 'securityLabel')); }}
						{{ Form::select('securityQuestion2', $security, 1, array('class' => 'form-control required question')) }}
						 
					</div>
					<div class="col-sm-12 nopad-right">
						{{ Form::label('id', '&nbsp;'); }}
						{{ Form::text('securityQuestionAnswer2', '', array( 'id' => 'securityQuestionAnswer2', 'class' => 'form-control required','alt' =>'Security Answer 2',  'alpha_num' => '', 'placeholder' => 'Enter your answer here' ) ) }}

						{{ Form::text('securityQuestionAnswer2', '', array( 'id' => 'securityQuestionAnswer2_', 'disabled' => 'disabled' , 'class' => 'answerFormat hide form-control required','alt' =>'Security Answer 2',  'alpha_num' => '', 'placeholder' => 'Enter a phone number' ) ) }}
					</div>
				</div> 
	      </div>
	      <div class="modal-footer" style="border-top: none;">
	        {{ Form::submit('Submit', array('class' => 'btn btn-cta') ) }}
	      </div>
	      {{ Form::close() }}
	    </div>
	  </div>
	</div>
@stop
@section('scripts') 
	{{ HTML::script( 'js/common.js');  }} 
	{{  HTML::script( 'js/validate.js'); }}
	{{ HTML::script( 'js/user.js');  }} 
@stop