@section('content') 
<div class="appl-title">
    <div class="container">
    	<h1> Reset Password </h1>
    </div> 
</div> 
<div id="application" class="credit-pull container"> 
	<div class="box-wrapper">
	 	<div class="row">
	 		<div class="col-sm-8">
	 			<p>&nbsp;</p>
	 			<div id="notification"></div> 
					{{ Form::open(array('url' => 'reset-password', 'id' => 'resetPassword', 'method' => 'POST', 'class' => 'form-horizontal validate' , 'role' => 'form')) }} 
					
					@if($errors->any())
						<div class="alert alert-danger hide" role="alert">
							{{$errors->first()}}
						</div>
					@endif

					<div class="form-group">
						{{ Form::label('id', 'New Password', array(  'class' => 'col-sm-2 control-label',)); }}
						<input name="key" type="hidden" value="<?php echo $key; ?>">
						<div class="col-sm-4">
							{{ Form::password('password', array( 'id' => 'password', 'placeholder' => 'Password', 'class' => 'form-control required password passwordFormat', 'alt' => 'Password','data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => 'At least 8 characters, including one capital letter, one number, and one special character.'  ) ) }}
						</div>
						<div class="alert alert-danger error-password col-sm-6 hide" role="alert">
							Invalid Password.
						</div>
					</div>

					<div class="form-group">	
						{{ Form::label('id', 'Confirm Password', array(  'class' => 'col-sm-2 control-label',)); }}
						<div class="col-sm-4">
							{{ Form::password('password_confirmation', array( 'id' => 'confirmPassword','placeholder' => 'Password Confirmation', 'class' => 'form-control required matches|password', 'alt' => 'Confirm Password','data-toggle' => 'tooltip', 'data-placement' => 'top', 'data-original-title' => 'At least 8 characters, including one capital letter, one number, and one special character.'  ) ) }}
						</div>
						<div class="alert alert-danger error-confirmPassword col-sm-6 hide" role="alert">
							Invalid Confirm Password. This should be the same value on the Password field.
						</div>
					</div>

					<div class="form-group row">
						<div class="col-sm-6">
						 {{ Form::submit('Send', array('class' => 'btn btn-cta pull-right') ) }}
						</div>
					</div>
					{{ Form::close() }}
	 		</div>
	 		<div class="col-sm-4">
	 			@include('includes.sidebar')
	 		</div>
	 	</div> 
	</div>
</div>

@stop

@section('scripts')
	{{ HTML::script('js/common.js');}}
	{{ HTML::script( 'js/jquery.typing-0.2.0.min.js');  }}
	{{ HTML::script( 'js/validate.js'); }}
@stop