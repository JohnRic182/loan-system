@section('content')
	<div class="appl-title">
	    <div class="container">
	    	<h1> User Login </h1>
	    </div> 
	</div> 
	<div class="container" id="application">
		<div class="loan-details-info box-wrapper">
		 	<div class="row">
		 		<div class="col-sm-8"> 
					<h3>Congratulations! </h3>
					<div class="form-group">
			
						<p>You have successfully change your {{ $notice }}. 
						
						@if ($notice == 'password')
							You can login <a href="<?php echo $root;?>/login">here</a>.
						@endif						

						<br /><br />
						<br />
						<br />
						<br />
						<br /></p>
						<center><p>Thank you for your interest in</p>
						{{HTML::image(asset('img/logoBig.png'));}}
						</center>
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
					</div>
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
		</div>
	</div>
@stop
@section('scripts')
	{{HTML::script('js/common.js');}}
@stop