@section('content')

{{ Session::get('key') }}

	<div class="appl-title">
	    <div class="container">
	    	<h1>{{ ucfirst($notice) }} Recovery</h1>
	    </div> 
	</div> 
	<div class="container" id="application">
		<div class="box-wrapper">
			<div class="row">
				<div class="col-sm-8">

					
					<div class="content-wrapper">
					 	<h3>Success!</h3>
						
						<div class="form-group">
							Please check your email for the reset {{ $notice }} link. Thank You!
						</div>

					</div>	


				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
			
		</div>
	</div>
@stop
@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
@stop