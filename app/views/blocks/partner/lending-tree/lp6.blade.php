{{-- Optional CSS --}}
@section('optionalCSS')
	 {{ HTML::style('css/partner/lp6.css' )}}
@stop

@section('bg')
	<div class="bg-main"></div>
@stop

{{-- Loader --}}
@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Searching Borrower...</span>
	</div>
@stop

{{-- Content --}}
@section('content')
	<!--[if gt IE 9]> -->
		<style>
		 #partner-offer select{
			-webkit-appearance: none;
			-moz-appearance: none;
			-ms-appearance: none;
			appearance: none;
			background-color: transparent;
			background-repeat: no-repeat;
			background-position: 97% 50%;
			background-image: url('/img/v3/dropdown-arrow.png');

			}
		</style>
	<![endif]-->
	<!--[if !IE]> -->
	<style>
	 #partner-offer select{
		-webkit-appearance: none;
		-moz-appearance: none;
		-ms-appearance: none;
		appearance: none;
		background-color: transparent;
		background-repeat: no-repeat;
		background-position: 97% 50%;
		background-image: url('/img/v3/dropdown-arrow.png');

		}
	</style>

	<![endif]-->
 
	{{ Form::open(array('id' => 'lendingTreeLPForm', 'method' => 'POST', 'class' => 'validate')) }} 
		{{-- iovation bb 3rd and 1st party --}}
		{{ Form::hidden('ioBB', '', array('id' => 'ioBB' )) }} 
		{{ Form::hidden('fpBB', '', array('id' => 'fpBB' )) }}

		<div class="container inner-container banner partner-banner" id="partner-generic-wrapper"> 
			<div class="row slider-form-wrapper partner-wrapper">
	 		 	<a class="navbar-brand navbar-brand-logo partner-main-logo pull-left" href="{{ getHompageRelativeUrl() }}"></a>
    			<div class="partner-brand-container">

	    			<div class="partner-statement-box">
	    				In partnership with		
	    			</div> 
	    			<div class="partner-logo-box partner-{{$partnerType}}">
	    				<img src="{{$partnerImage}}" class="navbar-brand navbar-brand-logo partner-logo" href="/">
	    			</div>
    			
    			</div>
			</div>
	 		<div class="row" id="partner-offer"> 		 

	 		 	<!-- This is for Larger Screens -->
	 		 	<p class="title offer-title text-center hidden-xs"><b>Congratulations!</b> You're just one step away from your loan offer.</p>
				
				<!-- This is for Smaller Screens -->
	 		 	<p class="title offer-title text-center visible-xs-* hidden-sm hidden-md hidden-lg"><b>Congratulations!</b> <br>You're just one step away from your loan offer.</p>


				<p class="title text-center">Add RateRewards to <b>lower your interest cost by up to 50%</b></p>

				<div class="col-sm-4 offer-table">
					<div class="product row-fluid">
	 					<div class="text-center col-md-12 col-xs-2">
	 						<span class="icon icon-ascend"></span>
	 					</div> 
	 					<div class="product-info col-md-12 col-xs-10">
	 						<h3>Ascend Personal Loan</h3>
	 						<p> A standard loan with low monthly payment.</p>
	 					</div> 
	 				</div>
		 			<div class="text-center plus-sign">+</div>
	 				<div class="product row-fluid">
	 					<div class="text-center col-md-12 col-xs-2">
	 						<span class="icon icon-ratereward"></span>
	 					</div>
	 					<div class="product-info product-info-rr col-md-12 col-xs-10">
	 						<h3>RateRewards <span data-toggle="tooltip" data-placement="top" title="" class="rwar" data-original-title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost">?</span></h3> 
	 						<p>Optional reward program to reduce interest cost by up to 50%.</p>
	 					</div> 
	 				</div>
	 				<div class="clearfix"></div>

	 				<hr class="offer-table-hr"/>
				</div>

				<input type="hidden" name="APR_Val" value="{{ $APR_Val }}">
				<input type="hidden" name="Monthly_Payment_Amt" value="{{ $Monthly_Payment_Amt }}">
				<input type="hidden" name="Loan_Amt" value="{{ $Loan_Amt }}">
				<input type="hidden" name="Terms" value="{{ $Terms }}"> 
				<input type="hidden" name="User_Id" value="{{ $User_Id }}">
				<input type="hidden" name="Loan_App_Nr" value="{{ $Loan_App_Nr }}">
				<input type="hidden" name="loanPurposeId" id="loanPurposeId" value="{{ $Loan_Purpose_Id }}">
				<input type="hidden" name="loanPurposeTxt" id="loanPurposeTxt" value="{{ $Loan_Purpose_Txt }}">
				{{-- <input type="hidden" name="IsPartner" value="1"> --}}
				<input type="hidden" name="PartnerId" value="{{ $Partner_Id }}">
				<input type="hidden" name="CampaignId" value="{{ $Campaign_Id }}">
				<input type="hidden" name="Funding_Partner_Id" value="{{ $Partner_Id }}">

				{{-- for iovation --}}
				<input type="hidden" name="streetAddress1" value="{{ $streetAddress1 }}">
				<input type="hidden" name="city" value="{{ $city }}">
				<input type="hidden" name="zip" value="{{ $zip }}">

				{{-- register --}}
				{{ Form::hidden('loanPurposeArr', $Loan_Purpose_Id ) }}
				{{ Form::hidden('User_Role_Id', $Loan_Purpose_Id ) }}			
				{{ Form::hidden('creditQuality', $creditquality ) }}
				{{ Form::hidden('IsPartner', '1' ) }}
				{{ Form::hidden('uid', '' ) }}

				<div class="col-sm-8 right-pane"> 
					<div class="user-form" > 
						<div class="row-fluid">
							<div class="col-sm-12">
								<div id="notification"></div>
							</div>
						</div>	 
						<div class="row-fluid">
							<div class="form-group">
								<div class="col-sm-12 col-md-6">
									{{ Form::label('id', 'Username', array('class' => 'control-label')); }}
									@if(App::environment() == 'local')
										<input type="text" name="email" class="form-control" value="{{ $Email_Id }}">
									@else
									<p class="text-left">{{ $Email_Id }}</p>
										<input type="hidden" name="email" value="{{ $Email_Id }}">
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-sm-12 col-md-6 ">
									{{ Form::label('id', 'Password', array('class' => 'control-label')); }}
									{{ Form::password('password', array( 'class' => 'form-control required password passwordFormat minimum8 maximum20', 'title' => "The Password field should contain a Minimum 8 characters and at least 1 Alphabet and 1 Number", 'tabindex' => 1, 'placeholder' => 'Enter a password', 'id' => 'password', 'alt' => 'Password' ) ) }}
								</div>
							</div>
						</div>	 
						<div class="row-fluid">
							<div class="form-group ">
								<div class="col-sm-12 col-md-6 ">
									{{ Form::label('id', 'Housing', array('class' => 'control-label')); }}
									{{ Form::select('housingSit', $housingSit , '', array('class' => 'form-control required', 'id' => 'housingSit' ,'tabindex' => 2, 'alt' => 'Housing Situation')); }} 
								</div>
							</div>
							<div class="form-group" id="rent-mortgage-wrapper">
								<div class="col-sm-12 col-md-6 ">
									{{ Form::label('id', 'Monthly Rent', array('class' => 'control-label monthlyRentMortagelbl')); }}

									{{ Form::number('rentAmount', '', array('class' => 'form-control rentAmount required' , 'id' => 'monthlyRentMortage' , 'placeholder' => "$ Monthly Rent", 'tabindex' => 3, 'alt' => 'Monthly Rent')); }} 
								</div>
							</div>
						</div> 				
						<div class="row-fluid">
							<div class="form-group ">
								<div class="col-sm-12 col-md-6 ">
									{{ Form::label('id', 'Annual Income', array('class' => 'control-label pull-left')); }}

									<div 
										data-placement="right" 
										title="Gross income is your income each year BEFORE your taxes and deductions. This my include salary, investment income, social security or pension income" 
										class="rwar ttr cc-question-icon"></div>

									<div 
										data-placement="left" 
										title="Gross income is your income each year BEFORE your taxes and deductions. This my include salary, investment income, social security or pension income" 
										class="rwar ttl cc-question-icon"></div>

									{{ Form::number('annualGrossIncome', '', array('class' => 'form-control annualGrossIncome required', 'placeholder' => '$ Enter your annual income', 'tabindex' => 4 , 'id' => 'annualGrossIncome', 'alt' => 'Annual Income' )); }} 
								</div>
							</div>					

							<div class="form-group">
								<div class="col-sm-12 col-md-6 ">

									{{ Form::label('id', 'When were you last delinquent on an account?', array('class' => 'control-label')); }}

									{{ Form::select('delinquencyPeriod', $delinquent , '', array('class' => 'form-control required', 'tabindex' => 5 )); }} 
									
								</div>
							</div>
						</div>	 
					</div> 	
					<div class="row">
						<div class="text-center validate-mortgage">
							WE VALIDATE MORTGAGE AND HOME OWNERSHIP AGAINST CREDIT BUREAU DATABASES
						</div>	
					</div>	 
				</div> 
 		 		<div class="form-group row consent"> 
								
					<div class="col-sm-10 col-sm-offset-2">
						<input type="checkbox" name="smsConsent" class="required" alt="Agreement" tabindex="6">
						<div class="consentlabel">
							I consent to the {{ HTML::link('consent/eSignature', 'Electronic Communications Agreement', array('target' => '_blank') ); }} 
						</div>										
					</div>

					<div class="col-sm-10 col-sm-offset-2">
						<input alt="Text/SMS Consent" style="float:left; margin-right: 4px; " name="agreementConsent" type="checkbox"  class="required" value="" tabindex="7">
						<div class="consentlabel">
							I agree to the {{ HTML::link('consent/creditPull', 'Credit Report Pull Agreement', array('target' => '_blank') ); }} , {{ HTML::link('terms', 'Terms of Use', array('target' => '_blank') ); }} , {{ HTML::link('privacyPolicy', 'Privacy Policy', array('target' => '_blank') ); }} and to receive calls and {{ HTML::link('consent/sms', 'SMS',  array('target' => '_blank') ); }} text messages
						</div>
						<ul class="ca-residents">
							<li></span>{{ (isset($stateConfig['register.license_text'])) ? $stateConfig['register.license_text'] : ""; }}
							</li>
						</ul>
					</div> 
				</div>  
	 		 </div> 
	 		 <div class="row-fluid">
	 		 	<div class="col-sm-12 text-center">
	 		 		<input type="submit" value="Get Started Now" class="btn btn-cta btn-cta-check-rate" />
	 		 			
	 		 		<div class="get-funded">
	 		 			{{ HTML::image(asset('img/v2/get-funded-13.png') )}}
	 		 		</div>
	 		 	</div>
	 		 </div> 
	 	</div>
 	{{ Form::close() }}
 	<p class="text-center partner-disclaimer">*We may require additional information that may change your offer details.</p> 
	<div class="featured-in-wrapper">
	 	<div class="container featured-in">
			<div class="row-fluid">
	 			<div class="col-sm-2 featured-icons">
	 				<p class="featured-info-title">Featured In:</p>
	 			</div>
	 			<div class="col-sm-2 col-xs-6 featured-icons">
	 				<a href="http://www.bloomberg.com/article/2015-06-11/apQBpNCmKMFI.html"><span class="featured-in-icon"></span></a>
	 			</div>
	 			<div class="col-sm-2 col-xs-6 featured-icons icon-street">
	 			    <a href="http://www.thestreet.com/video/13110637/ascend-ceo-discusses-real-time-loan-rate-adjustment-strategy.html"><span class="featured-in-icon"></span></a>
	 			</div>
	 			<div class="col-sm-3 col-xs-6 featured-icons icon-ab">
	 			    <a href="http://www.americanbanker.com/gallery/nine-apps-to-help-the-underbanked-take-control-of-their-finances-1075219-1.html"><span class="featured-in-icon"></span></a>
	 			</div>
	 			<div class="col-sm-2 col-xs-6 featured-icons icon-pymnts">
	 				<a href="http://www.insidephilanthropy.com/home/2015/6/3/how-a-cutting-edge-effort-to-boost-family-financial-stabilit.html"><span class="featured-in-icon"></span></a>
	 			</div>
			</div>
		</div>
	</div> 
	<div id="howitworks" class="features">
		
		<div class="container inner-container">
			
			<h1>All borrowers can get a great rate with an Ascend Personal Loan</h1>
			<hr class="centered-line">
			<p class="overview">Borrowers who are better than their credit score can enroll in our RateRewards <br>Program that lets great borrowers reduce their interest costs up to 50%.</p>

			<div class="row-fluid features-wrapper">
				<div class="col-sm-3">
					<span class="features-icon features-icon1"></span>
					<p class="feature-list">1. GET YOUR ASCEND PERSONAL LOAN</p>
					<p>Get an Ascend Personal Loan with a low rate and payment that will never change.</p>
					
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon2"></span>
					<p class="feature-list">2. ENROLL IN OPTIONAL RATEREWARDS PROGRAM</p>
					<p>Better borrowers can enroll in the optional RateRewards Program.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon3"></span>
					<p class="feature-list">3. EARN REWARDS</p>
					<p>Earn rewards each month through responsible financial behaviors.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon4"></span>
					<p class="feature-list">4. LOWER YOUR PAYMENT</p>
					<p>Your monthly interest costs and total payment will be reduced by the reward you earn.</p>
				</div>

				<div class="clearfix"></div>
			</div> 
			
			<div class="row text-center learn-more-wrapper">
				{{ HTML::link('howItWorks', 'Learn More', array('class' => 'btn btn-learn') )}}
				{{-- <button class="btn btn-learn">
					Learn More
				</button> --}}
			</div> 
		</div> 
	</div> 
	<div id="aboutus" class="about-us">
		<div class="container inner-container"> 
			<div class="row-fluid">
				<h1>Who is Ascend?</h1> 
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit score indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost.
				</p>

				<p>It's that simple.</p>


			</div> 
		</div>
	</div> 
	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1> 
				<hr class="centered-line">
			</div>
		 	

			{{-- carousel --}}
			
			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">
			      
      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>
			    
				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li> 
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li> 
								</ul>
						

							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>						

				</div>


			  </div>


			</div>


			{{-- carosel --}}
			<div class="clearfix"></div>						
		</div>
	</div>   
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js'); }}
	{{ HTML::script( 'js/bootstrap.min.js'); }}
	{{ HTML::script( 'js/jquery.nouislider.all.min.js'); }}
	{{ HTML::script( 'js/jquery.maskedinput.min.js');  }}
	{{ HTML::script( 'js/validate.js'); }}
	{{ HTML::script( 'js/user.js');  }}
	{{ HTML::script( 'js/application.js');  }}
	{{ HTML::script( 'js/partner.js');  }}
	<!-- ================BEGIN iovation scripts ================ --> 
	<script language="javascript">
	// io_bbout_element_id should refer to the hidden field in your form that contains the blackbox
		var io_bbout_element_id          = 'ioBB';

		// io_install_stm indicates whether the ActiveX object should be downloaded. The io_stm_cab_url
		// should reference your signed local copy of the ActiveX object
		var io_install_stm               = false;
		var io_exclude_stm               = 12;
		var io_install_flash             = false;
		var io_enable_rip                = true;

		// uncomment any of the below to signal an error when ActiveX or Flash is not present
		var io_flash_needs_update_handler = "";
	</script>

	@if($ioTestMode)
		<script language="javascript" src="https://ci-mpsnare.iovation.com/snare.js"></script>
	@else
		<script language="javascript" src="https://mpsnare.iesnare.com/snare.js"></script>
	@endif

	<script language="javascript">var fp_bbout_element_id = "fpBB";</script>
	<script language="javascript" src="{{ asset('/js/iojs/static_wdp.js'); }}"></script>
	<script language="javascript" src="{{ url('/iojs/4.1.1/dyn_wdp.js') }}"></script>

	<!-- ================END iovation scripts ================ --> 
@stop