{{-- 
--------------------------------------------------------------------------------------------------------------
-- Template: complex.blade.php                                                                              --
-- Category: Partner                                                                                        --
-- Purpose: Display the partners landing page for Lending Tree                                              --
--------------------------------------------------------------------------------------------------------------
--}}
@section('bg')
	<div class="bg-main"></div>
@stop
@section('content')

	<script type="text/javascript">
		var DEFAULT_SLIDER_VAL = {{ $default_slider_val }};
    var MINIMUM_LOAN_AMT   = {{ $sliderMinLoanAmt }};
    var MAXIMUM_LOAN_AMT   = {{ $sliderMaxLoanAmt }};
	</script>

  {{HTML::style('css/style.lendingRate.css')}}
  {{HTML::style('css/jquery.nouislider.min.css')}}
 	{{HTML::style('css/jquery.nouislider.pips.min.css')}}


	<div class="container inner-container banner partner-banner" style="margin-top: -15px;">
 		<div class="slider-form-wrapper partner-wrapper">

      <div class="row slider-form-wrapper partner-wrapper">
      <a class="navbar-brand navbar-brand-logo partner-main-logo pull-left" href="{{ getHompageRelativeUrl() }}"></a>
            
          <div class="partner-brand-container">

            <div class="partner-statement-box">
              In partnership with   
            </div>

            <div class="partner-logo-box partner-{{$partnerType}}">
              <img src="{{$partnerImage}}" class="navbar-brand navbar-brand-logo partner-logo" href="/">
            </div>
          
          </div>
    </div>

		{{ Form::open(array('url' => '/register', 'method' => 'POST', 'id' => 'LoanApplicationForm','class' => 'validate form-inline LoanApplicationForm-partner partner-form' , 'role' => 'form')) }}
 			<div class="partner slogan">
			    <h1 class="slogan-web">Get a low rate & payment on the funds you need.<br><small>Add RateRewards to reduce your interest expense up to 50%.</small></h1>
          <h1 class="slogan-mobile">Get a low rate & payment.<br><small>Add RateRewards to reduce your interest expense up to 50%.</small></h1>
        </div>
      <div class="row">
        <!-- products -->
        <div id="products" class="products col-sm-6">
          <div class="product-ascend col-sm-12 text-center">
            <span class="icon icon-ascend"></span>
            <h3>Ascend Personal Loan</h3>
            <p>A standard loan with low monthly payment</p>
          </div>
          <div class="col-sm-12 text-center plus-sign">+</div>
          <div class="product-rr col-sm-12 text-center">
            <span class="icon icon-ratereward"></span>
            <h3>Raterewards <span data-toggle="tooltip" data-placement="top" title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost" class="rwar">?</span></h3>
            <p>Optional reward program to reduce interest costs by up to 50%.</p>
          </div>
        </div>

        <div class="select-rate col-sm-6">
          <div class="loan-form">
            <h1>Personal <br>loans up to <span>$10,000</span></h1>
            <h1 class="show-mobile">Personal loans up to <span>$10,000</span></h1>
            <input type="number" id="loanAmt" name="loanAmt" class="lendingRt1" placeholder="Enter loan amount ($2,600-$10,000)" required min="2600" max="15000" />
            <select id="loanPurposeId" name="loanPurposeId" required="required">
              <option value="">Select the purpose of your loan</option>
              <option value="1">Debt Consolidation</option>
              <option value="2">Emergency Expense</option>
              <option value="3">Vacation</option>
              <option value="4">Medical</option>
              <option value="5">Auto Expense</option>
              <option value="6">Everyday Expense</option>
              <option value="7">Home Improvement</option>
              <option value="8">Other</option>
            </select>
            <select id="creditquality" name="creditquality" required="required">
              <option value="">Select your credit quality</option>
              <option value="Excellent (720+)">Excellent (720+)</option>
              <option value="Good (680 - 719)">Good (680 - 719)</option>
              <option value="Fair (640 - 679)">Fair (640 - 679)</option>
              <option value="Poor (639 or less)">Poor (639 or less)</option>
            </select>

            <!-- added for home.js to work -->
            <div id="slider-tooltip" style="display: none;"></div>

            <!-- check rate button/wrapper -->
            <div class="check-wrapper">
              <div class="check-rate-wrapper">
                <button class="btn btn-cta btn-cta-check-rate">
                  <span class="icon icon-lock-v2"></span>Check your Rate
                </button>
              </div>
              <div class="wont-impact"></div>
              <div class="wont-impact-mobile" align="center">
                {{HTML::image( asset('/img/v3/impact-mobile2.png') )}}
              </div>
            </div>

            <!-- icon placeholders -->
            <span class="prefix-dollar">$</span>
            <span class="dd-purpose"><img src="/img/v3/dropdown-arrow.png" /></span>
            <span class="dd-credit-quality"><img src="/img/v3/dropdown-arrow.png" /></span>
          </div>
        </div>
      </div>

			<input type="hidden" name="TotalMonthlyReward" value="" id="TotalMonthlyReward">
			<!--<input type="hidden" name="loanAmt" value="" id="loanSliderInput">-->
			<!--<input type="hidden" name="loanPurposeId" value="1" id="loanPurposeInput">-->
			<input type="hidden" name="loanPurposeTxt" value="Debt Consolidation" id="loanPurposeTxt">
			<input type="hidden" name="loanProductId" value="1" id="loanTypeInput">
			<input type="hidden" name="loanProductTxt" value="Ascend Loan" id="loanTypeText">
			<input type="hidden" name="isProductionQA" value="{{ $isProductionQA }}" id="isProductionQA">

			{{ Form::close() }}

      <div class="clearfix"></div>
		</div>
 	</div>

  <!-- overview features -->
  <div class="overview-features">
    <ul>
      <li class="feature-lowpay">Low payment</li>
      <li class="feature-quote">Quote in minutes</li>
      <li class="feature-funding">Funding by tomorrow</li>
      <li class="feature-raterewards">Customize with RateRewards</li>
    </ul>
  </div>

  <div id="media" class="media-list">
    <ul>
      <li>Featured in:</li>
      <li>{{ HTML::image(asset('img/media-bloomberg.png') ) }}</li>
      <li>{{ HTML::image(asset('img/media-thestreet.png') ) }}</li>
      <li>{{ HTML::image(asset('img/media-AB.png') ) }}</li>
      <li>{{ HTML::image(asset('img/media-pymnts.png') ) }}</li>
    </ul>
  </div>

<div id="howitworks" class="features">
  <div class="container inner-container">
    <h1>All borrowers can get a great rate with an Ascend Personal Loan</h1>
    <hr class="centered-line">
    <p class="overview">Borrowers who are better than their credit score can enroll in our RateRewards Program that lets great borrowers reduce their interest costs up to 50%.</p>

    <div class="row-fluid">
      <div class="col-sm-3">
        <span class="features-icon features-icon1"></span>
        <p class="feature-list">1. GET YOUR ASCEND PERSONAL LOAN</p>
        <p>Get an Ascend Personal Loan with a low rate and payment that will never change.</p>

      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon2"></span>
        <p class="feature-list">2. ENROLL IN OPTIONAL RATEREWARDS PROGRAM</p>
        <p>Better borrowers can enroll in the optional RateRewards Program.</p>
      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon3"></span>
        <p class="feature-list">3. EARN REWARDS</p>
        <p>Earn rewards each month through responsible financial behaviors.</p>
      </div>
      <div class="col-sm-3">
        <span class="features-icon features-icon4"></span>
        <p class="feature-list">4. LOWER YOUR PAYMENT</p>
        <p>Your monthly interest costs and total payment will be reduced by the reward you earn.</p>
      </div>

      <div class="clearfix"></div>
    </div>

    <div class="row text-center learn-more-wrapper">
      {{ HTML::link('howItWorks', 'Learn More', array('class' => 'btn btn-learn') )}}
      {{-- <button class="btn btn-learn">
        Learn More
      </button> --}}
    </div>
  </div>
</div>

	<div id="aboutus" class="about-us">
		<div class="container inner-container">
			<div class="row-fluid">
				<h1>Who is Ascend?</h1>
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit scores indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost. <br>
				It's that simple.
				</p>
			</div>
		</div>
	</div>


	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1>
				<hr class="centered-line">
			</div>


			{{-- carousel --}}

			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">

      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>

				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li>
								</ul>


							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>

				</div>


			  </div>

{{-- 			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
			    <span class="icon icon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
			    <span class="icon icon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a> --}}


			</div>


			{{-- carosel --}}

			<div class="clearfix"></div>

		</div>
	</div>

@stop

@section('scripts')
	{{HTML::script('js/common.js');}}
	{{HTML::script('js/bootstrap.min.js');}}
	{{HTML::script('js/jquery.nouislider.all.min.js')}}
  {{HTML::script('js/home.js')}}
@stop