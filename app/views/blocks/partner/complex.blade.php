{{-- 
--------------------------------------------------------------------------------------------------------------
-- Template: complex.blade.php    																			--
-- Category: Partner																						--
-- Purpose: Display the partners landing page for Complex Partners											--
--------------------------------------------------------------------------------------------------------------
--}}
@section('bg')
	<div class="bg-main"></div>
@stop
@section('content')
	{{ Form::open(array('url' => 'register', 'method' => 'POST')) }}
	<div class="container inner-container banner"> 
 		 <div class="row" id="partner-offer">
 		 	<p class="title">Congratulations. You may qualify for the following offer. Add RateRewards to lower your interest even more!</p>
			<p class="title offer-title">Your offer*</p>
			<div class="col-sm-6 offer-table">
				<table class="table ">
					<tr class="td-title">
						<td width="50%">Loan Amount</td>
						<td width="50%">Term</td> 
					</tr>
					<tr>
						<td>${{ $Loan_Amt }}</td>
						<td>{{ $Terms }} Months</td> 
					</tr>
				</table>

				<table class="table apr-table">
					<tr class="td-title">
						<td width="50%">APR</td>
						<td width="50%">Monthly Payment</td> 
					</tr>
					<tr>
						<td>{{ $APR_Val }}%</td>
						<td>${{ $Monthly_Payment_Amt }}</td> 
					</tr>
				</table>
			</div>
			<input type="hidden" name="APR_Val" value="{{ $APR_Val }}">
			<input type="hidden" name="Monthly_Payment_Amt" value="{{ $Monthly_Payment_Amt }}">
			<input type="hidden" name="Loan_Amt" value="{{ $Loan_Amt }}">
			<input type="hidden" name="Terms" value="{{ $Terms }}"> 
			<input type="hidden" name="User_Id" value="{{ $User_Id }}">
			<input type="hidden" name="Loan_App_Nr" value="{{ $Loan_App_Nr }}">
			<input type="hidden" name="loanPurposeId" value="{{ $Loan_Purpose_Id }}">
			<input type="hidden" name="IsPartner" value="1">
			<input type="hidden" name="PartnerId" value="{{ $Partner_Id }}">
			<input type="hidden" name="CampaignId" value="{{ $Campaign_Id }}">
			<input type="hidden" name="Funding_Partner_Id" value="{{ $Partner_Id }}">
			<div class="col-sm-6 text-center">
				<p><span class="icon icon-ratereward"></span></p>
				<h3>RateRewards</h3>
				<p>
					Our unique reward program that lets borrowers reduce interest costs up to 50% <br>
					<a href="{{ URL::to('howItWorks')}}" class="learn-more">Learn more</a>
				</p>
			</div>
			<p></p>
			<div class="col-sm-12 text-center">
				<button class="btn btn-cta btn-cta-check-rate">
						Get Started Now
					</button>
				<div class="get-funded">
				{{ HTML::image(asset('img/get-funded.png') )}}
			</div>
			</div> 
 		 </div>
 	</div>
 	{{ Form::close() }}
 	<p class="text-center partner-disclaimer">*We may require additional information that may cause us to change or withdraw your offer.</p>
 	<div class="featured-in-wrapper hidden-xs">
 	<div class="container featured-in">
		<div class="row-fluid">
 			<div class="col-sm-2 featured-icons">
 				<p class="featured-info-title">Featured In:</p>
 			</div>
 			<div class="col-sm-2 featured-icons">
 				<a href="http://www.bloomberg.com/article/2015-06-11/apQBpNCmKMFI.html"><span class="featured-in-icon"></span></a>
 			</div>
 			<div class="col-sm-2 featured-icons icon-street">
 			    <a href="http://www.thestreet.com/video/13110637/ascend-ceo-discusses-real-time-loan-rate-adjustment-strategy.html"><span class="featured-in-icon"></span></a>
 			</div>
 			<div class="col-sm-3 featured-icons icon-ab">
 			    <a href="http://www.americanbanker.com/gallery/nine-apps-to-help-the-underbanked-take-control-of-their-finances-1075219-1.html"><span class="featured-in-icon"></span></a>
 			</div>
 			<div class="col-sm-2 featured-icons icon-pymnts">
 				<a href="http://www.insidephilanthropy.com/home/2015/6/3/how-a-cutting-edge-effort-to-boost-family-financial-stabilit.html"><span class="featured-in-icon"></span></a>
 			</div>
			</div>
		</div>
	</div>
	<div id="howitworks" class="features">
		
		<div class="container inner-container">
			
			<h1>All borrowers can get a great rate with an Ascend Personal Loan</h1>
			<hr class="centered-line">
			<p class="overview">Borrowers who are better than their credit score can enroll in our RateRewards <br>Program that lets great borrowers reduce their interest costs up to 50%.</p>

			<div class="row-fluid">
				<div class="col-sm-3">
					<span class="features-icon features-icon1"></span>
					<p class="feature-list">1. GET YOUR ASCEND PERSONAL LOAN</p>
					<p>Get an Ascend Personal Loan with a low rate and payment that will never change.</p>
					
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon2"></span>
					<p class="feature-list">2. ENROLL IN OPTIONAL RATEREWARDS PROGRAM</p>
					<p>Better borrowers can enroll in the optional RateRewards Program.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon3"></span>
					<p class="feature-list">3. EARN REWARDS</p>
					<p>Earn rewards each month through responsible financial behaviors.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon4"></span>
					<p class="feature-list">4. LOWER YOUR PAYMENT</p>
					<p>Your monthly interest costs and total payment will be reduced by the reward you earn.</p>
				</div>

				<div class="clearfix"></div>
			</div> 
			
			<div class="row text-center learn-more-wrapper">
				{{ HTML::link('howItWorks', 'Learn More', array('class' => 'btn btn-learn') )}}
				{{-- <button class="btn btn-learn">
					Learn More
				</button> --}}
			</div> 
		</div> 
	</div>
 	
	<div id="aboutus" class="about-us">
		<div class="container inner-container"> 
			<div class="row-fluid">
				<h1>Who is Ascend?</h1> 
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit scores indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost. <br>
				It's that simple.
				</p>
			</div> 
		</div>
	</div>
 

	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1> 
				<hr class="centered-line">
			</div>
		 	

			{{-- carousel --}}
			
			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">
			      
      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>
			    
				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li> 
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li> 
								</ul>
						

							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>						

				</div>


			  </div>

{{-- 			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
			    <span class="icon icon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
			    <span class="icon icon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a> --}}


			</div>


			{{-- carosel --}}

			<div class="clearfix"></div>
			<div class="row text-center criteria learn-more-wrapper">
				<a href="#" class="btn btn-learn"> CREDIT CRITERIA </a>
			</div>
			
		</div>
	</div>  
 
@stop

@section('scripts')
	{{HTML::script('js/common.js');}}
	{{HTML::script('js/bootstrap.min.js');}}
	{{HTML::script('js/jquery.nouislider.all.min.js')}}	
@stop