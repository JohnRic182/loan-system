{{-- 
--------------------------------------------------------------------------------------------------------------
-- Template: generic.blade.php    																			--
-- Category: Partner																						--
-- Purpose: Display the partners landing page for Lending Tree												--
--------------------------------------------------------------------------------------------------------------
--}}
@section('bg')
	<div class="bg-main"></div>
@stop

@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Searching Borrower...</span>
	</div>
@stop

@section('content')

{{-- overide partnerwrapper --}}
<style type="text/css">

	.bg-main {
		background: url('/img/v3/banner.jpg') no-repeat scroll 100% 0%; background-size: cover; height: 1150px; 
	}
	
	#partner-offer{ 
		margin-bottom: -24px;
		padding: 30px 10px;
		background: rgba(255,255,255, 0.9);
	}

	#partner-offer,
	#partner-wrapper{ 
		max-width: 965px;
	}
	#partner-offer p.offer-title {
		margin-left: 0px; 
	}
	#partner-generic-wrapper {
		padding-bottom: 0;
	}

	#partner-generic-wrapper .partner-wrapper {
    	max-width: 965px;
	}
	
	#partner-offer p:last-child.title {
		  margin-bottom: 15px;
	}
	
	#partner-offer p.offer-title {
    	margin-left: 0px;
    	font-size: 28px;
	}

	#partner-offer div.tab-wrap {
    	border: solid thin #E5E5E5;
    	margin-bottom: none;
    	background-color: #fcfcfb;
    	/*border-right: none;*/
	}

	#partner-offer div.tab-wrap:first-child {
		border-bottom: none;
		padding-bottom: 16px;
		padding-left: 16px;
	    padding-right: 16px;
	    background-color: #fdfdfd 
	}
	
	#partner-offer .validate-mortgage {
		font-weight: 
	}
	#partner-offer div.tab-wrap.rrp p.desc{
		font-size: 13px;
	    font-weight: normal;
	    color: #696969;		
	}

	#partner-offer div.tab-wrap.rrp .title{
		font-size: 18px;
	}

	#partner-offer div.tab-wrap.rrp table td{
		border : none;	
	}

	#partner-offer table tr:last-child td {
	     font-size: 16px; 
	}

	#partner-offer table.apr-table tr:first-child td {
	    padding-top: 10px;
	}

	#partner-offer table tr:last-child td:last-child {
	    padding-bottom: 10px;
	}

	#partner-offer .offer-table{
		border-right: none;
	    padding-right: 0px;
	    margin-top: 8px;
	}

	#partner-offer .offer-table table td:last-child{
		padding-right: 10px;
	}

	.plus-sign {
	    color: #81C868;
	    font-size: 45px;
	    font-weight: 100;
	    width: 100%;
	    position: absolute;
	    top:-33px;
	}

	.form-group .control-label{
		font-weight: 600;
		font-size: 12px;		
	}

	.form-group div{
		margin-bottom: 10px;
	}

	.form-control{
	    border-radius: 0px;
	    -webkit-appearance: none;
	}

	.user-form{
		background-color: #fefefd;
		display: table;
		width: 100%;
		padding: 25px 0px;
		border: solid thin #81C868;
	}

	#partner-offer > div.right-pane{
	    margin-bottom: 20px;
	    padding-left: 0px
	}
	
	#partner-offer > div.right-pane > div:last-child{
	    font-weight: bold;
	    font-size: 13px;
	    color: #666666;
	}

	#partner-offer > div.form-group.row.consent{
		margin: 0px;
	}

	#partner-offer > div.form-group.row.consent > div{
		margin-bottom: 0px;
	}

	.consent ul.ca-residents{
		margin-top: 0px;
	}
	.consent div.consentlabel {
		  font-size: 12px;
	}
	
	.get-funded{
		margin-top: -15px;
	}

	.cc-question-icon{
		margin: 0px;
		margin-right: 5px;
		margin-left: 5px;
		margin-top: 3px;
		float: left;
	}


	.consentlabel{
		font-size: 12px;
	}

	.tooltip-inner {
    	min-width:250px;
	}

	.ttl{ display: none; }	

	#partner-offer input[type='number'] {
	    -moz-appearance:textfield;
	}

	select::-ms-expand {
	   display: none;
	}

	#partner-offer input[type=number]::-webkit-inner-spin-button, 
	#partner-offer input[type=number]::-webkit-outer-spin-button { 
	  	-webkit-appearance: none;
	    -moz-appearance: none;
	    -ms-appearance: none;
	  margin: 0; 
	}

	#partner-offer div.tab-wrap.rrp table td.rr-icon, 
	#partner-offer div.tab-wrap.rrp table td.rr-desc{
		padding-left: 0px;
	}

	#partner-offer div.tab-wrap.rrp table td.rr-desc .title{
		margin-bottom: 0px;
	}

	.features .overview {
	    padding-bottom: 20px;
	}

	.features-wrapper div{
		margin-bottom: 15px;
	}

	.btn-learn {
	    text-decoration: none;
	    text-transform: uppercase;
	}

	@media (max-width: 991px) { 
		.bg-main{ height: 1600px; }
	}

	@media (max-width: 768px) {

		.bg-main{ height: 1700px; }
		#partner-offer  .offer-table{
			border-bottom: none;
			padding-right: 15px;
		}

		#partner-offer .user-form{
			margin-top: -52px;
		}

		#partner-offer .control-label{ display: none; }

		#partner-offer .rpane{ padding-right: 15px; }

		#partner-offer .right-pane{ padding-right: 0px; }


		.cc-question-icon{
		    position: absolute;
		    right: 18px;
		    top: 7px;
		}

		.ttr{ display: none; }
		.ttl{ display: block; }

		.features .learn-more-wrapper {
		    margin-top: 0px;
		}

		.user-form{
			padding-top: 15px;
		}

		
	}

	@media (max-width: 520px){
		.partner-header .navbar{
			background-color: #212b30;
		}

		.navbar-default{
		    padding: 10px 0 10px 10px;
		}
	}


	@media (max-width: 414px){

		#partner-offer{     
	 		margin-right: 15px;
	 		margin-left: 15px;
	 	}

	 	.partner-main-logo500 {
		    display: block;
		    margin: 0px;
		    width: auto;
		}

		.partner-wrapper .partner-logo {
		    float: none;
		    width: 150px;
		}

		.partner-wrapper .partner-brand-container .partner-statement-box {
		    margin-left: 0px !important;
		    position: inherit;
		    font-size: 14px;
		    padding-left: 16px;
		    float: left;
		}

		.partner-lendingtree{
		    text-align: right;
		    padding-right: 10px;
		}

		.partner-banner {
			padding-right: 0px;
			padding-left: 0px;
		}

		.validate-mortgage{ padding: 0px 10px; }


	}


	@media (max-width: 375px){
		.featured-in{
			padding: 0px; 
		}

		.featured-in .row-fluid > div{
			padding: 0px; 
		}

		.featured-in .icon-ab .featured-in-icon{
			background-size: 750px;
			background-position: -289px 6px;
		}


		#partner-offer p.title{
			font-size: 14px;
		}

		#partner-offer p.offer-title{
			font-size: 17px;
		}

		#partner-offer .featured-in{
			padding: 0px; 
		}

		#partner-offer .featured-in .row-fluid > div{
			padding: 0px; 
		}

		#partner-offer .featured-in .icon-ab .featured-in-icon{
			background-size: 750px;
			background-position: -289px 6px;
		}

		#partner-offer .featured-in .featured-in-icon{
			width: 160px;
		}

		#partner-offer div.tab-wrap.rrp .title {
		    font-size: 15px;
		    margin-bottom: 0px;
		}

		.features h1 {
		    font-size: 26px;
		}

		.features .overview {
		    font-size: 20px;
		}

		#aboutus h1 {
		    font-size: 28px;
		}

		.featured-in .icon-pymnts .featured-in-icon {
		    background-position: -416px 6px;
		    background-size: 700px;
		}

		.featured-in .icon-street .featured-in-icon {
		    background-position: -190px 0;
		    background-size: 900px;
		}

	}

</style> 


<!--[if gt IE 9]>
	<style>
	 #partner-offer select{
		-webkit-appearance: none;
		-moz-appearance: none;
		-ms-appearance: none;
		appearance: none;
		background-color: transparent;
		background-repeat: no-repeat;
		background-position: 97% 50%;
		background-image: url('/img/v3/dropdown-arrow.png');

		}
	</style>
<![endif]-->
<!--[if !IE]> -->

	<style>
	 #partner-offer select{
		-webkit-appearance: none;
		-moz-appearance: none;
		-ms-appearance: none;
		appearance: none;
		background-color: transparent;
		background-repeat: no-repeat;
		background-position: 97% 50%;
		background-image: url('/img/v3/dropdown-arrow.png');

		}
	</style>

<![endif]-->


	{{ Form::open(array('id' => 'lendingTreeLPForm', 'method' => 'POST', 'class' => 'validate')) }}

		{{-- iovation bb 3rd and 1st party --}}
		{{ Form::hidden('ioBB', '', array('id' => 'ioBB' )) }} 
		{{ Form::hidden('fpBB', '', array('id' => 'fpBB' )) }}

	<div class="container inner-container banner partner-banner" id="partner-generic-wrapper"> 
		<div class="row slider-form-wrapper partner-wrapper">
 		 	<a class="navbar-brand navbar-brand-logo partner-main-logo pull-left" href="{{ getHompageRelativeUrl() }}"></a>
    				
    			<div class="partner-brand-container">

	    			<div class="partner-statement-box">
	    				In partnership with		
	    			</div>

	    			<div class="partner-logo-box partner-{{$partnerType}}">
	    				<img src="{{$partnerImage}}" class="navbar-brand navbar-brand-logo partner-logo" href="/">
	    			</div>
    			
    			</div>
		</div>
 		<div class="row" id="partner-offer"> 		 

 		 	<p class="title offer-title text-center"><b>Congratulations!</b> You may qualify for the following offer.</p>
			<p class="title text-center">Continue to see how you can <b>lower your interest cost by up to 50%</b></p>

			<div class="col-sm-4 offer-table">
				
				<div class="tab-wrap">
				<table class="table ">
					<tr class="td-title">
						<td width="50%">Loan Amount</td>
						<td width="50%">Term</td> 
					</tr>
					<tr>
						<td>${{ $Loan_Amt }}</td>
						<td>{{ $Terms }} Months</td> 
					</tr>
				</table>

				<table class="table apr-table">
					<tr class="td-title">
						<td width="50%">APR</td>
						<td width="50%">MO. Payment</td> 
					</tr>
					<tr>
						<td>{{ $APR_Val }}%</td>
						<td>${{ $Monthly_Payment_Amt }}</td> 
					</tr>
				</table>
				</div>

				<div class="tab-wrap rrp" style="position:relative">
				<div class="text-center plus-sign">+</div>
					<table class="table">
						<tr>
							<td class="rr-icon">							
								<span class="icon icon-ratereward"></span>
							</td>
							<td class="rr-desc">								
								<p class="title">RateRewards Program</p>
								<p class="desc">Lets good borrowers lower their interest cost by up to 50%</p>
							</td>

						</tr>
					</table>
				</div>
			</div>
			

			<input type="hidden" name="APR_Val" value="{{ $APR_Val }}">
			<input type="hidden" name="Monthly_Payment_Amt" value="{{ $Monthly_Payment_Amt }}">
			<input type="hidden" name="Loan_Amt" value="{{ $Loan_Amt }}">
			<input type="hidden" name="Terms" value="{{ $Terms }}"> 
			<input type="hidden" name="User_Id" value="{{ $User_Id }}">
			<input type="hidden" name="Loan_App_Nr" value="{{ $Loan_App_Nr }}">
			<input type="hidden" name="loanPurposeId" id="loanPurposeId" value="{{ $Loan_Purpose_Id }}">
			<input type="hidden" name="loanPurposeTxt" id="loanPurposeTxt" value="{{ $Loan_Purpose_Txt }}">
			{{-- <input type="hidden" name="IsPartner" value="1"> --}}
			<input type="hidden" name="PartnerId" value="{{ $Partner_Id }}">
			<input type="hidden" name="CampaignId" value="{{ $Campaign_Id }}">
			<input type="hidden" name="Funding_Partner_Id" value="{{ $Partner_Id }}">

			{{-- for iovation --}}
			<input type="hidden" name="streetAddress1" value="{{ $streetAddress1 }}">
			<input type="hidden" name="city" value="{{ $city }}">
			<input type="hidden" name="zip" value="{{ $zip }}">

			{{-- register --}}
			{{ Form::hidden('loanPurposeArr', $Loan_Purpose_Id ) }}
			{{ Form::hidden('User_Role_Id', $Loan_Purpose_Id ) }}			
			{{ Form::hidden('creditQuality', $creditquality ) }}
			{{ Form::hidden('IsPartner', '1' ) }}
			{{ Form::hidden('uid', '' ) }}

			<div class="col-sm-8 right-pane">

				<div class="user-form" >		
					
					<div class="row-fluid">
						<div class="col-sm-12">
							<div id="notification"></div>
						</div>
					</div>								

					<div class="row-fluid">
						<div class="form-group">
							<div class="col-sm-12 col-md-6">
								{{ Form::label('id', 'Username', array('class' => 'control-label')); }}
								@if(App::environment() == 'local')
									<input type="text" name="email" class="form-control" value="{{ $Email_Id }}">
								@else
								<p class="text-left">{{ $Email_Id }}</p>
									<input type="hidden" name="email" value="{{ $Email_Id }}">
								@endif
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12 col-md-6 ">
								{{ Form::label('id', 'Password', array('class' => 'control-label')); }}
								{{ Form::password('password', array( 'class' => 'form-control required password passwordFormat minimum8 maximum20', 'title' => "The Password field should contain a Minimum 8 characters and at least 1 Alphabet and 1 Number", 'tabindex' => 1, 'placeholder' => 'Enter a password', 'id' => 'password', 'alt' => 'Password' ) ) }}
							</div>
						</div>
					</div>					
			

					<div class="row-fluid">
						<div class="form-group ">
							<div class="col-sm-12 col-md-6 ">
								{{ Form::label('id', 'Housing', array('class' => 'control-label')); }}
								{{ Form::select('housingSit', $housingSit , '', array('class' => 'form-control required', 'id' => 'housingSit' ,'tabindex' => 2, 'alt' => 'Housing Situation')); }} 
							</div>
						</div>

						<div class="form-group" id="rent-mortgage-wrapper">
							<div class="col-sm-12 col-md-6 ">
								{{ Form::label('id', 'Monthly Rent', array('class' => 'control-label monthlyRentMortagelbl')); }}

								{{ Form::number('rentAmount', '', array('class' => 'form-control rentAmount required' , 'id' => 'monthlyRentMortage' , 'placeholder' => "$ Monthly Rent", 'tabindex' => 3, 'alt' => 'Monthly Rent')); }} 
							</div>
						</div>
					</div>



					{{-- <div class="row-fluid houseSit-other hide">
						<div class="form-group ">
							<div class="col-sm-12 col-md-6 ">
								{{ Form::label('id', 'Contact Name', array('class' => 'control-label')); }}								
								{{ Form::text('contactName', '', array('id' => 'contactName', 'class' => 'form-control', 'placeholder' => 'Contact Name' )) }}
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12 col-md-6 ">
								{{ Form::label('id', 'Phone Number', array('class' => 'control-label')); }}
								{{ Form::text('contactPhoneNr', '', array('class' => 'form-control', 'id' => 'contactPhoneNr' , 'placeholder' => "###-###-####", 'tabindex' => 3)); }} 
							</div>
						</div>
					</div> --}}

												
					<div class="row-fluid">
						<div class="form-group ">
							<div class="col-sm-12 col-md-6 ">
								{{ Form::label('id', 'Annual Income', array('class' => 'control-label pull-left')); }}

								<div 
									data-placement="right" 
									title="Gross income is your income each year BEFORE your taxes and deductions. This my include salary, investment income, social security or pension income" 
									class="rwar ttr cc-question-icon"></div>

								<div 
									data-placement="left" 
									title="Gross income is your income each year BEFORE your taxes and deductions. This my include salary, investment income, social security or pension income" 
									class="rwar ttl cc-question-icon"></div>

								{{ Form::number('annualGrossIncome', '', array('class' => 'form-control annualGrossIncome required', 'placeholder' => '$ Enter your annual income', 'tabindex' => 4 , 'id' => 'annualGrossIncome', 'alt' => 'Annual Income' )); }} 
							</div>
						</div>					

						<div class="form-group">
							<div class="col-sm-12 col-md-6 ">

								{{ Form::label('id', 'When were you last delinquent on an account?', array('class' => 'control-label')); }}

								{{ Form::select('delinquencyPeriod', $delinquent , '', array('class' => 'form-control required', 'tabindex' => 5 )); }} 
								
							</div>
						</div>
					</div>			


				</div>
				
						
				<div class="row">
					<div class="text-center validate-mortgage">
						WE VALIDATE MORTGAGE AND HOME OWNERSHIP AGAINST CREDIT BUREAU DATABASES
					</div>	
				</div>			

			</div>


 		 
				

 		 		{{-- ------------- --}}

 		 		<div class="form-group row consent"> 
								
					<div class="col-sm-8 col-sm-offset-2">
						<input type="checkbox" name="smsConsent" class="required" alt="Text/SMS Consent" tabindex="6">
						<div class="consentlabel">
							I consent to the {{ HTML::link('consent/eSignature', 'Electronic Communications Agreement', array('target' => '_blank') ); }} 
						</div>										
					</div>

					<div class="col-sm-8 col-sm-offset-2">
						<input alt="Agreement" style="float:left; margin-right: 4px; " name="agreementConsent" type="checkbox"  class="required" value="" tabindex="7">
						<div class="consentlabel">
							I agree to the {{ HTML::link('consent/creditPull', 'Credit Report Pull Agreement', array('target' => '_blank') ); }} , {{ HTML::link('terms', 'Terms of Use', array('target' => '_blank') ); }} , {{ HTML::link('privacyPolicy', 'Privacy Policy', array('target' => '_blank') ); }} and to receive calls and {{ HTML::link('consent/sms', 'SMS',  array('target' => '_blank') ); }} text messages
						</div>
						<ul class="ca-residents">
							<li></span>{{ (isset($stateConfig['register.license_text'])) ? $stateConfig['register.license_text'] : ""; }}
							</li>
						</ul>
					</div>

				</div>


 		 		{{-- ------------- --}}


			 
 		 </div>



 		 <div class="row-fluid">
 		 	<div class="col-sm-12 text-center">
 		 		<input type="submit" value="Get Started Now" class="btn btn-cta btn-cta-check-rate" />
 		 			
 		 		<div class="get-funded">
 		 			{{ HTML::image(asset('img/v2/get-funded-13.png') )}}
 		 		</div>
 		 	</div>
 		 </div> 
 	</div>
 	{{ Form::close() }}
 	<p class="text-center partner-disclaimer">*We may require additional information that may change your offer details.</p> 
	<div class="featured-in-wrapper">
	 	<div class="container featured-in">
			<div class="row-fluid">
	 			<div class="col-sm-2 featured-icons">
	 				<p class="featured-info-title">Featured In:</p>
	 			</div>
	 			<div class="col-sm-2 col-xs-6 featured-icons">
	 				<a href="http://www.bloomberg.com/article/2015-06-11/apQBpNCmKMFI.html"><span class="featured-in-icon"></span></a>
	 			</div>
	 			<div class="col-sm-2 col-xs-6 featured-icons icon-street">
	 			    <a href="http://www.thestreet.com/video/13110637/ascend-ceo-discusses-real-time-loan-rate-adjustment-strategy.html"><span class="featured-in-icon"></span></a>
	 			</div>
	 			<div class="col-sm-3 col-xs-6 featured-icons icon-ab">
	 			    <a href="http://www.americanbanker.com/gallery/nine-apps-to-help-the-underbanked-take-control-of-their-finances-1075219-1.html"><span class="featured-in-icon"></span></a>
	 			</div>
	 			<div class="col-sm-2 col-xs-6 featured-icons icon-pymnts">
	 				<a href="http://www.insidephilanthropy.com/home/2015/6/3/how-a-cutting-edge-effort-to-boost-family-financial-stabilit.html"><span class="featured-in-icon"></span></a>
	 			</div>
			</div>
		</div>
	</div> 
	<div id="howitworks" class="features">
		
		<div class="container inner-container">
			
			<h1>All borrowers can get a great rate with an Ascend Personal Loan</h1>
			<hr class="centered-line">
			<p class="overview">Borrowers who are better than their credit score can enroll in our RateRewards <br>Program that lets great borrowers reduce their interest costs up to 50%.</p>

			<div class="row-fluid features-wrapper">
				<div class="col-sm-3">
					<span class="features-icon features-icon1"></span>
					<p class="feature-list">1. GET YOUR ASCEND PERSONAL LOAN</p>
					<p>Get an Ascend Personal Loan with a low rate and payment that will never change.</p>
					
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon2"></span>
					<p class="feature-list">2. ENROLL IN OPTIONAL RATEREWARDS PROGRAM</p>
					<p>Better borrowers can enroll in the optional RateRewards Program.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon3"></span>
					<p class="feature-list">3. EARN REWARDS</p>
					<p>Earn rewards each month through responsible financial behaviors.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon4"></span>
					<p class="feature-list">4. LOWER YOUR PAYMENT</p>
					<p>Your monthly interest costs and total payment will be reduced by the reward you earn.</p>
				</div>

				<div class="clearfix"></div>
			</div> 
			
			<div class="row text-center learn-more-wrapper">
				{{ HTML::link('howItWorks', 'Learn More', array('class' => 'btn btn-learn') )}}
				{{-- <button class="btn btn-learn">
					Learn More
				</button> --}}
			</div> 
		</div> 
	</div> 
	<div id="aboutus" class="about-us">
		<div class="container inner-container"> 
			<div class="row-fluid">
				<h1>Who is Ascend?</h1> 
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit score indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost.
				</p>

				<p>It's that simple.</p>


			</div> 
		</div>
	</div> 
	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1> 
				<hr class="centered-line">
			</div>
		 	

			{{-- carousel --}}
			
			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">
			      
      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>
			    
				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li> 
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li> 
								</ul>
						

							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>						

				</div>


			  </div>


			</div>


			{{-- carosel --}}
			<div class="clearfix"></div>						
		</div>
	</div>   
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js'); }}
	{{ HTML::script( 'js/bootstrap.min.js'); }}
	{{ HTML::script( 'js/jquery.nouislider.all.min.js'); }}
	{{ HTML::script( 'js/jquery.maskedinput.min.js');  }}
	{{ HTML::script( 'js/validate.js'); }}
	{{ HTML::script( 'js/user.js');  }}
	{{ HTML::script( 'js/application.js');  }}
	{{ HTML::script( 'js/partner.js');  }}
	<!-- ================BEGIN iovation scripts ================ --> 
	<script language="javascript">
	// io_bbout_element_id should refer to the hidden field in your form that contains the blackbox
		var io_bbout_element_id          = 'ioBB';

		// io_install_stm indicates whether the ActiveX object should be downloaded. The io_stm_cab_url
		// should reference your signed local copy of the ActiveX object
		var io_install_stm               = false;
		var io_exclude_stm               = 12;
		var io_install_flash             = false;
		var io_enable_rip                = true;

		// uncomment any of the below to signal an error when ActiveX or Flash is not present
		var io_flash_needs_update_handler = "";
	</script>

	@if($ioTestMode)
		<script language="javascript" src="https://ci-mpsnare.iovation.com/snare.js"></script>
	@else
		<script language="javascript" src="https://mpsnare.iesnare.com/snare.js"></script>
	@endif

	<script language="javascript">var fp_bbout_element_id = "fpBB";</script>
	<script language="javascript" src="{{ asset('/js/iojs/static_wdp.js'); }}"></script>
	<script language="javascript" src="{{ url('/iojs/4.1.1/dyn_wdp.js') }}"></script>

	<!-- ================END iovation scripts ================ --> 
@stop