{{-- 
--------------------------------------------------------------------------------------------------------------
-- Template: complex.blade.php    																			--
-- Category: Partner																						--
-- Purpose: Display the partners landing page for simple partners											--
--------------------------------------------------------------------------------------------------------------
--}}
@section('bg')
	<div class="bg-main"></div>
@stop
@if($partnerFlag)
	
@else
	@section('slogan')
		@include('includes.slogan')
	@stop	
@endif
@section('content')

	<script type="text/javascript"> 
		var DEFAULT_SLIDER_VAL = {{ $default_slider_val }}; 
		var MINIMUM_LOAN_AMT   = {{ $sliderMinLoanAmt }};
    	var MAXIMUM_LOAN_AMT   = {{ $sliderMaxLoanAmt }};
	</script>

    {{HTML::style('css/jquery.nouislider.min.css')}}
 	{{HTML::style('css/jquery.nouislider.pips.min.css')}}

    @if($partnerFlag)
    	<div class="container inner-container banner partner-banner"> 
	 		
			
    		<div class="slider-form-wrapper partner-wrapper">

    			<a class="navbar-brand navbar-brand-logo partner-main-logo pull-left" href="{{ getHompageRelativeUrl() }}"></a>
    				
    			<div class="partner-brand-container">

	    			<div class="partner-statement-box">
	    				In partnership with    			
	    			</div>

	    			<div class="partner-logo-box partner-{{$partnerType}}">
	    				<img src="{{$partnerImage}}" class="navbar-brand navbar-brand-logo partner-logo" href="/">
	    			</div>
    			
    			</div>

    			
	    	{{ Form::open(array('url' => '/register', 'method' => 'POST', 'id' => 'LoanApplicationForm','class' => 'form-inline LoanApplicationForm-partner partner-form' , 'role' => 'form')) }} 
	 			<div class="partner slogan">
				    <h1>
				      Get a low rate and payment. Add RateRewards<br> to reduce your interest expense up to 50%.
				    </h1>
				</div>	
	 			<div id="products" class="row">
		 			<div class="product-ascend text-center">
		 				<div class="product row-fluid">
		 					<div class="col-sm-2 nopad-right nopad-left">
		 						<span class="icon icon-ascend"></span>
		 					</div>
		 					<div class="col-sm-10 product-info">
		 						<h3>Ascend Personal Loan</h3>
		 						<p> A standard personal loan with a constant low interest rate and payment.</p>
		 					</div>
		 					<div class="clearfix"></div>
		 				</div>
		 			</div>
		 			<div class="text-center plus-sign">+</div>
		 			<div class="product-rr text-center">
		 				<div class="product row-fluid">
		 					<div class="col-sm-2 nopad-right nopad-left">
		 						<span class="icon icon-ratereward"></span>
		 					</div>
		 					<div class="col-sm-10 product-info product-info-rr">
		 						<h3>Raterewards <span data-toggle="tooltip" data-placement="top" title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost" class="rwar">?</span></h3> 
		 						<p>An optional reward program that lets better borrowers reduce interest costs up to 50%.</p>
		 					</div>
		 					<div class="clearfix"></div>
		 				</div>
		 			</div>
		 		</div>
				<div class="row overview-features">
					<div class="col-sm-5 rate-details-panel">
						<div class="rate-details"><span class="glyphicon glyphicon-ok"></span>Get a low rate and payment</div>
						<div class="rate-details"><span class="glyphicon glyphicon-ok"></span>Receive your quote in minutes</div>
						<div class="rate-details"><span class="glyphicon glyphicon-ok"></span>Customize your best loan</div>
						<div class="rate-details"><span class="glyphicon glyphicon-ok"></span>Get funded by tomorrow</div>
					</div>

					<div class="divider1" align="center"></div>

					<div class="col-sm-2 rate-arrow">
						<!-- <span class="glyphicon glyphicon-chevron-right"></span> -->
						<div class="rate-arrow-line"></div>
					</div>
					<div class="col-sm-5 select-rate">
						<h1>Select Your Loan Amount:</h1>
						<div class="partner-slider-version">
							<div class="text-center slider-wrapper">
								<div id="slider-tooltip"></div>
							</div>
						</div>
						<div class="check-rate-wrapper">
							<button class="btn btn-cta btn-cta-check-rate">
								<span class="icon icon-lock-v2"></span>Check your Rate
							</button>
						</div>
						<div class="wont-impact"></div>
						<div class="wont-impact-mobile" align="center">
							{{HTML::image( asset('/img/impact-mobile.png') )}}
						</div>
					</div>
				</div>

	 
				<input type="hidden" name="TotalMonthlyReward" value="" id="TotalMonthlyReward">
				<input type="hidden" name="loanAmt" value="" id="loanSliderInput">
				<input type="hidden" name="loanPurposeId" value="1" id="loanPurposeInput">
				<input type="hidden" name="loanPurposeTxt" value="Debt Consolidation" id="loanPurposeTxt">
				<input type="hidden" name="loanProductId" value="1" id="loanTypeInput">
				<input type="hidden" name="loanProductTxt" value="Ascend Loan" id="loanTypeText">
				<input type="hidden" name="isProductionQA" value="{{ $isProductionQA }}" id="isProductionQA">

				{{ Form::close() }}
			</div>
	 	</div>
    @else
    	<div class="container inner-container banner"> 
	 		<div id="products" class="row">
	 			<div class="col-md-5 product-ascend text-center">
	 				<div class="product row-fluid">
	 					<div class="col-sm-2 nopad-right nopad-left">
	 						<span class="icon icon-ascend"></span>
	 					</div>
	 					<div class="col-sm-10 product-info">
	 						<h3>Ascend Personal Loan</h3>
	 						<p> A standard personal loan with a constant low interest rate and payment.</p>
	 					</div>
	 					<div class="clearfix"></div>
	 				</div>
	 			</div>
	 			<div class="col-md-2 text-center plus-sign">+</div>
	 			<div class="col-md-5 product-rr text-center">
	 				<div class="product row-fluid">
	 					<div class="col-sm-2 nopad-right nopad-left">
	 						<span class="icon icon-ratereward"></span>
	 					</div>
	 					<div class="col-sm-10 product-info product-info-rr">
	 						<h3>Raterewards <span data-toggle="tooltip" data-placement="top" title="RateRewards lets you prove you're a responsible borrower and deserve a lower rate.  Regardless of your credit score, you can demonstrate your financial responsibility by reducing your total debt balances, limiting your credit card spending, and increasing your savings.  In return, we'll lower your interest cost" class="rwar">?</span></h3> 
	 						<p>An optional reward program that lets better borrowers reduce interest costs up to 50%.</p>
	 					</div>
	 					<div class="clearfix"></div>
	 				</div>
	 			</div>
	 		</div>
		

			<div class="slider-form-wrapper">
				<h1>Select Your Loan Amount:</h1>
				<p class="visible-xs-* wont-impact-xs">Won't impact your score!</p>
	 		{{ Form::open(array('url' => '/apply', 'method' => 'POST', 'id' => 'LoanApplicationForm','class' => 'form-inline' , 'role' => 'form')) }} 

	 			<div class="row">
	 				<div class="col-sm-7">
						<div class="text-center slider-wrapper">
							<div id="slider-tooltip"></div>
						</div>
					</div>
					<div class="col-sm-5 check-rate-wrapper">
						<button class="btn btn-cta btn-cta-check-rate">
							<span class="icon icon-lock-v2"></span>Check your Rate
						</button>
					</div>

					<div class="wont-impact">
						{{ HTML::image(asset('img/v2/impact.png') ) }}
					</div>
				</div>
				<hr>
				<div class="row overview-features">
					<div class="col-sm-6">
						<div><span class="glyphicon glyphicon-ok"></span>Get a low rate and payment</div>
						<div><span class="glyphicon glyphicon-ok"></span>Receive your quote in minutes</div>
					</div>
					<div class="col-sm-6">
						<div><span class="glyphicon glyphicon-ok"></span>Customize your best loan</div>
						<div><span class="glyphicon glyphicon-ok"></span>Get funded by tomorrow</div>
					</div>
				</div>
	 
				<input type="hidden" name="TotalMonthlyReward" value="" id="TotalMonthlyReward">
				<input type="hidden" name="loanAmt" value="" id="loanSliderInput">
				<input type="hidden" name="loanPurposeId" value="1" id="loanPurposeInput">
				<input type="hidden" name="loanPurposeTxt" value="Debt Consolidation" id="loanPurposeTxt">
				<input type="hidden" name="loanProductId" value="1" id="loanTypeInput">
				<input type="hidden" name="loanProductTxt" value="Ascend Loan" id="loanTypeText">
				<input type="hidden" name="isProductionQA" value="{{ $isProductionQA }}" id="isProductionQA">

				{{ Form::close() }}
			</div>
	 	</div>
    @endif
	
 	  
	<div id="howitworks" class="features">
		<div class="container inner-container">
			<h1>All borrowers can get a great rate with an Ascend Personal Loan</h1>
			<hr class="centered-line">
			<p class="overview">Borrowers who are better than their credit score can enroll in our RateRewards <br>Program that lets great borrowers reduce their interest costs up to 50%.</p>

			<div class="row-fluid">
				<div class="col-sm-3">
					<span class="features-icon features-icon1"></span>
					<p class="feature-list">1. GET YOUR ASCEND PERSONAL LOAN</p>
					<p>Get an Ascend Personal Loan with a low rate and payment that will never change.</p>
					
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon2"></span>
					<p class="feature-list">2. ENROLL IN OPTIONAL RATEREWARDS PROGRAM</p>
					<p>Better borrowers can enroll in the optional RateRewards Program.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon3"></span>
					<p class="feature-list">3. EARN REWARDS</p>
					<p>Earn rewards each month through responsible financial behaviors.</p>
				</div>
				<div class="col-sm-3">
					<span class="features-icon features-icon4"></span>
					<p class="feature-list">4. LOWER YOUR PAYMENT</p>
					<p>Your monthly interest costs and total payment will be reduced by the reward you earn.</p>
				</div>

				<div class="clearfix"></div>
			</div> 
			
			<div class="row text-center learn-more-wrapper">
				{{ HTML::link('howItWorks', 'Learn More', array('class' => 'btn btn-learn') )}}
				{{-- <button class="btn btn-learn">
					Learn More
				</button> --}}
			</div> 
		</div> 
	</div>
 
	<div id="aboutus" class="about-us">
		<div class="container inner-container"> 
			<div class="row-fluid">
				<h1>Who is Ascend?</h1> 
				<hr class="centered-line">
				<p>We are an experienced team of lending, technology, and data professionals who are committed to developing fairer ways to price less than perfect borrowers.</p>
				<br>
				<p>We believe credit scores aren't very accurate at predicting how good of a borrower you will be in the future, and that you deserve a chance to prove you're better than your credit scores indicates.</p>
				<br>
				<p>If you demonstrate good financial habits that reduce our risk, we'll lower your interest cost. <br>
				It's that simple.
				</p>
			</div> 
		</div>
	</div>
 

	<div class="about-us borrowers" id="borrowers">
		<div class="container inner-container">
			<div class="row">
				<h1>You're in good company.</h1> 
				<hr class="centered-line">
			</div>
		 	

			{{-- carousel --}}
			
			<div id="carousel-reviews" data-interval="0" class="carousel slide" data-ride="carousel">

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner reviews" role="listbox">
			    <div class="reviews-inner item ">
			      
      		        <p>"When I heard about RateRewards, I was excited that I could lower my interest <br>costs by doing things that improve my overall financial health"</p>
			        <p> &mdash; Whanda, <span>San Jose</span></p>
			    </div>

			    <div class="item reviews-inner active">
			      <p>"I’m rebuilding my credit after the recession.  <br>I was glad to find a lender that gives me a chance to prove myself."</p>
			        <p> &mdash; Greg, <span>Los Angeles</span></p>
			    </div>

			    <div class="item reviews-inner">
			      <p>"I decided to go with an Ascend Personal Loan.  <br>I like the RateRewards idea, but I felt more comfortable locking in a fixed payment."</p>
			        <p> &mdash; Linda, <span>Sacramento</span></p>
			    </div>
			    
				<div class="reviews-nav" >


						<!-- Indicators thumbnails-->
						<div class="review-thumbnail" >

							<a class=" left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
								<span class="icon icon-chevron-left" aria-hidden="true"></span>
							</a>


								<ul class="carousel-indicators">
									<li data-target="#carousel-reviews" data-slide-to="0" >{{ HTML::image(asset('img/v2/customers/thumb-2.png') )}}</li>
									<li data-target="#carousel-reviews" data-slide-to="1" class="active">{{ HTML::image(asset('img/v2/customers/thumb-1.png') )}}</li> 
									<li data-target="#carousel-reviews" data-slide-to="2">{{ HTML::image(asset('img/v2/customers/thumb-3.png') )}}</li> 
								</ul>
						

							<a class=" right carousel-control" href="#carousel-reviews" role="button" data-slide="next" >
								<span class="icon icon-chevron-right" aria-hidden="true"></span>
							</a>

						</div>						

				</div>


			  </div>

{{-- 			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
			    <span class="icon icon-chevron-left" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
			    <span class="icon icon-chevron-right" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a> --}}


			</div>


			{{-- carosel --}}

			<div class="clearfix"></div>
			<div class="row text-center criteria learn-more-wrapper">
				<a href="#" class="btn btn-learn"> CREDIT CRITERIA </a>
			</div>
			
		</div>
	</div>  
 
@stop

@section('scripts')
	{{HTML::script('js/common.js');}}
	{{HTML::script('js/bootstrap.min.js');}}
	{{HTML::script('js/jquery.nouislider.all.min.js')}}	
	{{HTML::script('js/home.js')}}
@stop