@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop
@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1>
				Please upload documents that confirm your stated income.
			</h1>
	    </div> 
	</div>
	<div id="application" class="container">

		 {{ Form::open(array('url' => '/postIncomeVerification', 'method' => 'POST', 'id' => 'incomeVerification','class' => 'form-horizontal' , 'role' => 'form')) }}
		 <div class="content-wrapper verification box-wrapper">
		 	<div class="row">
		 		<div class="col-sm-8">
		 			<div id="income">

						<p>Please upload documents to support employment income and her income entered on your application.</p>
						<hr>

						@if (!$employmentStep)
							<p class="alert alert-warning"><span>You will need to provide Employment Information before completing this step</span></p>
						@endif
						

						<h3>EMPLOYMENT INCOME <span class="required-not">Required</span></h3>
		 				<hr>

						<div class="row-fluid"> 
							<div id="notification">
								@if(isset($message))
									<div class="alert alert-danger">{{ $message }}</div>
								@endif
							</div>  
			 
								<div class="form-group paystub-wrapper">

									<div class="col-sm-6 paystubs">

										<label for="Employer">
											<h4>Paid by Employer</h4>
											<span>Please upload your 2 most recent paystubs</span>
										</label>

										<div>
											<!-- The global progress bar -->
										    
										    <div class="progress">
										        <div class="progress-bar progress-bar-success"></div>
										    </div> 
										   
										</div>
										<div id="files" class="files clearfix"></div>		

											<button type="button" class="btn btn-cta btnSelectFileIncome"  rel="1">Select Paystub 1</button> 
											<button type="button" class="btn btn-cta btnSelectFileIncome"  rel="2">Select Paystub 2</button>					 
										
									</div>										

									<div class="horizontal" align="center">
										<span class="separatorText"> OR </span>
										<hr>
									</div>

									<div class="col-sm-6 tax-wrapper" style="position:relative">

										<span class="separatorText"> OR </span>

										<label for="Employer" >
										 <h4>Self Employed</h4>
										 <span>Please upload your most recent 1099 tax filing</span>
										</label>
										<div>
											<!-- The global progress bar -->
										    
										    <div class="progress">
										        <div class="progress-bar progress-bar-success"></div>
										    </div> 
										   	
										</div>
										<div id="files2" class="files"></div> 
										<button type="button" class="btn btn-cta btnSelectFileTax">Select 1099 Filling</button> 
										{{-- <button type="button" class="btn btn-cta btnSelectFileTax">Select Tax Filling 2</button>  --}} 
									</div>

								</div>	 
								<hr>
								
								{{-- 								
								<p style="text-align: center;border-top: 1px solid #ccc;"> 
									<span style="top: -11px;position: relative;background: #FFF;font-weight: bold;padding: 0 10px;">OR </span>
								</p> 
								--}}
						 
								
								<div class="form-group other-income-wrapper">
									
									<div class="col-sm-12">
										<label for="Employer" >
										 <h4>Other Income</h4>
										 	<span>
												If you included income sources in addition to your primary employment, please 
												provide documents that validate that income.
											</span>
										</label>																			
										    
										<div class="row">

											<div class="col-sm-6">
												<div class="progress">
													<div class="progress-bar progress-bar-success"></div>
												</div> 
											</div>

										</div>									   	
								
										
										<div id="files3" class="files"></div>
										
										<button type="button" class="btn btn-cta btnSelectOther">Select Documents</button> 
										{{-- <button type="button" class="btn btn-cta btnSelectFileTax">Select Tax Filling 2</button>  --}} 
									</div>
								</div>
								

								
								<input type="hidden" value="" name="CheckImageData" id="CheckImageData">
								<input type="hidden" value="" name="Employment_Type_Cd" id="Employment_Type_Cd"> 
							
						</div>
					</div>
		 		</div>	
		 		<div class="col-sm-4">
		 			@include('includes.sidebar')
		 		</div>
		 	</div>
			
		 </div>
		 <div class="action-section row">
		 	<div class="col-sm-8">

		 	<div class="row-fluid">
		 		<div class="pull-right col-sm-6 col-xs-12"> 
					@if ($employmentStep)
						<button class="btn btn-cta btnSubmitVerification btnIncomeVerification pull-right">Submit <span class="glyphicon glyphicon-chevron-right"></span></button>
					@endif
				</div>

		 		<div class=" col-sm-6 col-xs-12">
				<a href="../verification/steps" class="cta-link backLink" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a> 
				</div>
				
		 	</div>
				

{{-- 				<div class="clearfix"></div> --}}
			</div>
		</div>
		{{ Form::close() }}

		<span id="Paystub" class="btn btn-cta fileinput-button hide"> 
	        <span>Select Paystub 2</span>
	        <!-- The file input field used as target for the file upload widget -->
	        <input id="fileupload" type="file" name="files[]" multiple>
	    </span> 

	    <span id="TaxFilling" class="btn fileinput-button hide">
	        <span>Select Tax Filling</span>
	        <input id="fileuploadTax" type="file" name="files2[]" multiple>
	    </span>

	    <span id="OtherIncome" class="btn fileinput-button hide">
	        <span>Select Tax Filling</span>
	        <input id="fileuploadOther" type="file" name="files3[]" multiple>
	    </span>

	</div>
	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/verification.js');  }}

	{{ HTML::script( 'js/fileUpload/vendor/jquery.ui.widget.js');  }}

	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

	{{ HTML::script( 'js/fileUpload/jquery.iframe-transport.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-process.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-image.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-validate.js');  }} 
	
	<script>


		/*jslint unparam: true, regexp: true */
		/*global window, $ */
		$(function () {
		    'use strict';

		    var files = [];
		    var payStubBtn;

		    $('.btnSelectFileIncome').click(function(){
				payStubBtn = $(this).attr('rel');
			});

  
		    /**
			 * Extend Serialize Object
			 * @return {[type]}
			 */
			$.fn.serializeObject = function()
			{
			    var o = {};
			    var a = this.serializeArray();
			    $.each(a, function() {
			        if (o[this.name] !== undefined) {
			            if (!o[this.name].push) {
			                o[this.name] = [o[this.name]];
			            }
			            o[this.name].push(this.value || '');
			        } else {
			            o[this.name] = this.value || '';
			        }
			    });
			    return o;
			};


		    // Change this to the location of your server-side upload handler:
		    var url = baseUrl + 'verification/uploadIncomeVerification';
 			
	        var uploadButton = $('<button/>')
	            .addClass('btn btn-cta uploadActionBtn hide')
	            .prop('disabled', true)
	            .text('Processing...')
	            .on('click', function () {
	                var $this = $(this),
	                    data = $this.data(); 
	                    // console.log(data);
	                $this
	                    .off('click')
	                    .text('Abort')
	                    .on('click', function () {
	                        $this.remove();
	                        data.abort();
	                    });
	                data.submit().always(function () {
	                	$this.next().remove();
	                    $this.remove();
	                });

	                return false;
	            });

	        var cancelButtonEmp = $('<button/>')
	         		.addClass('btn btn-cta btnSelectFileIncome cancelBtn hide')	
	         		.prop('disabled', true )
	         		.text('X')
	         		.on('click', function(){

	         			var data = $(this).data();
 
	         				 $(this).parents('.image-files').remove();

	         			//remove disabled attributes on file tax select 
	         			if( $('.paystub-wrapper').find('.image-files').size() == 0 ) {
	         				$('.btnSelectFileTax').removeAttr('disabled');
	         				$(this).parents('.files').html('');

	         				data.fileInput.val(''); 
	         			} 

	         			//reset the progress bar
         				$('#progress .progress-bar').css(
				            'width','0%'
				        );


	         			return false;

	         		});

	        var deleteLinkEmp  = $('<span/>')
	        			.prop('class', 'glyphicon glyphicon-remove')
	        			.on('click', function(){
         				
	         				var data 	= $(this).data();
	         				var dltBtn 	= $(this).attr('rel');
        				 
	         				//empty files
		         			 $(this).parents('.image-files').remove();

	         				if( $('.paystub-wrapper').find('.image-files').size() == 0 ) {

	         					console.log('tset')

	         					//reset file input
	         					data.fileInput.val(''); 

	         					$('.btnSelectFileTax').removeAttr('disabled');
		         				$('#CheckImageData').val('');

		         				files = [];

	         				} 

	         				// reset the progress bar
	         				$('#progress .progress-bar').css(
					            'width','0%'
					        );

					        //show buttons once upload was cancelled
	         				$('.btnSelectOther, .btnSelectFileIncome[rel="'+ dltBtn +'"]').show();
  
	         		});


	         var cancelButton = $('<button/>')
	         		.addClass('btn btn-cta btnSelectFileIncome cancelBtn')	
	         		.prop('disabled', true )
	         		.text('X')
	         		.on('click', function(){

	         			var data = $(this).data();

	         				//reset file input
	         				data.fileInput.val(''); 

	         				//enabled  paystubs buttons
	         				$('.btnSelectFileIncome').removeAttr('disabled');
	         				//show buttons once upload was cancelled
	         				$('.btnSelectOther, .btnSelectFileIncome, .btnSelectFileTax').show();

	         				//empty files
	         				$(this).parents('.files').html('');

	         			//reset the progress bar
         				$('#progress .progress-bar').css(
				            'width','0%'
				        );
 
	         			return false;
	         		});


	         	var deleteLink  = $('<span/>')
	         			.prop('class', 'glyphicon glyphicon-remove').on('click', function(){

	         				var data = $(this).data();

	         				//reset file input
	         				data.fileInput.val(''); 

	         				//enabled  paystubs buttons
	         				@if(!$selfEmployed)
		         				$('.btnSelectFileIncome').removeAttr('disabled');
							@endif
	         				

	         				$('#CheckImageData').val('');

	         				files = [];

	         				//reset the progress bar
	         				$('#progress .progress-bar').css(
					            'width','0%'
					        );

	         				//empty files
	         				$(this).parents('.files').html('');

	         				//show buttons once upload was cancelled
	         				$('.btnSelectOther, .btnSelectFileTax').show();

	         				// .btnSelectFileIncome[rel="'+ payStubBtn +'"],
  
	         		});



	        $('#fileuploadTax').fileupload({
		        url: url,
		        dataType: 'json',
		        autoUpload: false,
		        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|doc?x|)$/i,
		        maxFileSize: 5100000, // 5 MB
		        // Enable image resizing, except for Android and Opera,
		        // which actually support image resizing, but fail to
		        // send Blob objects via XHR requests:
		        // disableImageResize: /Android(?!.*Chrome)|Opera/
		        //     .test(window.navigator.userAgent),
		        previewMaxWidth: 50,
		        previewMaxHeight: 50,
		        previewCrop: true
		    }).on('fileuploadadd', function (e, data) {

		    	console.log('fileupload has been triggered');
 
		    	$('#Employment_Type_Cd').val('SelfEmployed');
		    	$('#files2').html('');
 
		        data.context = $('<div class="image-files"/>').appendTo('#files2');

		        $.each(data.files, function (index, file) {

		        	console.log(index);

		        	var node = $('<div/>');

					var node2  = $('<div/>') 
		                    .append($('<p class="filenames" />').text( StringObj.generateShortname(file.name) ));

		            if ( !index ) 
		                var node3 =  $('<div/>').append(deleteLink.clone(true).data(data))
		                	
		            node.appendTo(data.context);
		            node2.appendTo(data.context);
		            node3.appendTo(data.context);

		        });

		        $('#files2').hide();		        

		    }).on('fileuploadprocessalways', function (e, data) {

		    	console.log('fileuploadprocessalways has been triggered'); 
		    	// console.log(data);

		        var index = data.index,
		            file  = data.files[index],
		            node  = $(data.context.children()[index]),
		            ext   = StringObj.getExtension(file.name);

		        if (file.preview)
		            node.prepend(file.preview);
		        else
		        	node.prepend('<div class="img-file-icon">'+ ext.toUpperCase() +'</div>');

		        if (file.error) {
					
					$(data.context)
						.prepend('<br>')
						.prepend($('<span class="text-danger"/>').text(file.error + '. (Max size 5MB)'));

					$('#files2').show();

		        }else{
		        	data.submit();		        	
			        $('.tax-wrapper .progress').show();
		        }
		        
		        if (index + 1 === data.files.length) {
		            data.context.find('button:first-child')
		                .text('Upload')
		                .prop('disabled', !!data.files.error);
		            data.context.find('button:last-child')
		                .text('X')
		                .removeProp('disabled');

		            $('.btnSelectFileTax').hide();

		        }
		    }).on('fileuploadprogressall', function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('.tax-wrapper .progress-bar').css(
		            'width',
		            progress + '%'
		        );
		    }).on('fileuploaddone', function (e, data) {
				
		    	console.log(data.result.files2);

		        $.each(data.result.files2, function (index, file) {


		            if (file.error) {
		                var error = $('<span class="text-danger"/>').text(file.error);
		                $(data.context.children()[index])
		                    .prepend('<br>')
		                    .prepend(error);
		            }

		            files.push(file.name)

		        });

				$('#files2').show();
				$('.tax-wrapper .progress').hide();
				$('.tax-wrapper .progress-bar').css('width','0%');
		        $('#CheckImageData').val(JSON.stringify(files));

		    }).on('fileuploadfail', function (e, data) {
		    	console.log(data);
		        $.each(data.files, function (index) {
		            var error = $('<span class="text-danger"/>').text('File upload failed.');
		            $(data.context.children()[index])
		                .prepend('<br>')
		                .prepend(error);
		        });
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');


		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        autoUpload: false,
		        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
		        maxFileSize: 5100000, // 5 MB greater
		        // Enable image resizing, except for Android and Opera,
		        // which actually support image resizing, but fail to
		        // send Blob objects via XHR requests:
		        // disableImageResize: /Android(?!.*Chrome)|Opera/
		        //     .test(window.navigator.userAgent),
		        previewMaxWidth: 50,
		        previewMaxHeight: 50,
		        previewCrop: true
		    }).on('fileuploadadd', function (e, data) {

		    	//console.log('fileupload has been triggered');
		    	//remove the first index
		    	// $('#files').html("");
		    	$('#Employment_Type_Cd').val('Employed');

		        data.context = $('<div class="image-files"/>').attr('rel', payStubBtn ).appendTo('#files');

		        $.each(data.files, function (index, file) {

		        	var node = $('<div/>');

					var node2  = $('<div />') 
		                    .append($('<p class="filenames" />').text( StringObj.generateShortname(file.name) ));		        

		            if ( !index ) {
		                var node3 =  $('<div />')
		                	.append(deleteLinkEmp.clone(true).attr( 'rel', payStubBtn ).data(data))
		                	// .append(cancelButtonEmp.clone(true).data(data));	                		
			                	 
		            }		            

		            node.appendTo(data.context); 
		            node2.appendTo(data.context);
		            node3.appendTo(data.context);
		            
		        });
		        

	        	$('#files .image-files[rel="'+ payStubBtn +'"]').hide();		        
		        $('.paystubs .btnSelectFileIncome[rel="'+ payStubBtn +'"]').hide();


		    }).on('fileuploadprocessalways', function (e, data) {

		    	console.log('fileuploadprocessalways has been triggered'); 
		    	console.log(data);

		    	if(payStubBtn == 1)
			       	$('.btnSelectFileIncome[rel="2"]').attr('disabled', 'disabled');
			    else
			    	$('.btnSelectFileIncome[rel="1"]').attr('disabled', 'disabled');

		        var index = data.index,
		            file  = data.files[index],
		            node  = $(data.context.children()[index]),
		            ext   = StringObj.getExtension(file.name);

		        if (file.preview) 
		            node.prepend(file.preview);
		        else
		        	node.prepend('<div class="img-file-icon">'+ ext.toUpperCase() +'</div>');

		        if (file.error) {

		            $(data.context)
		                .prepend('<br>')
		                .prepend($('<span class="text-danger"/>').text(file.error + '. (Max size 5MB)'));

		            $('#files .image-files[rel="'+ payStubBtn +'"]').show();
		            $('#CheckImageData').val('');
		            $('.btnSelectFileIncome').removeAttr('disabled');

		        }else{
		        	data.submit();
		        	$('.paystubs .progress').show();
		        }
		        
		        if (index + 1 === data.files.length) {
		            data.context.find('button:first-child')
		                .text('Upload')
		                .prop('disabled', !!data.files.error);
		            data.context.find('button:last-child')
		                .text('X')
		                .removeProp('disabled');

		        }		        

		    }).on('fileuploadprogressall', function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('.paystubs .progress-bar').css(
		            'width',
		            progress + '%'
		        );
		    }).on('fileuploaddone', function (e, data) {
		    	 
		        $.each(data.result.files, function (index, file) {
		            /*if (file.url) {
		                // var link = $('<a>')
		                //     .attr('target', '_blank')
		                //     .prop('href', file.url);
		                $(data.context.children()[index+2]).append(deleteLinkEmp.clone(true).attr( 'rel', payStubBtn ).data(data));
		                    // .wrap(link);
		            } else */

		            if (file.error) {
		                var error = $('<span class="text-danger"/>').text(file.error);
		                $(data.context.children()[index])
		                    .prepend('<br>')
		                    .prepend(error);
		            	
		            	$('.btnSelectFileIncome').removeAttr('disabled');
		            }

		         files.push(file.name)

		         $('#files .image-files[rel="'+ payStubBtn +'"]').show();

		        });

		        console.log(payStubBtn);
		        console.log(files);

		        $('.paystubs .progress').hide();
		        $('.paystubs .progress-bar').css( 'width', '0%' );
		        $('.btnSelectFileIncome').removeAttr('disabled');
		       	$('#CheckImageData').val(JSON.stringify(files));

		    }).on('fileuploadfail', function (e, data) { 
		    	console.log(data); 
		        $.each(data.files, function (index) {
		            var error = $('<span class="text-danger"/>').text('File upload failed.');
		            $(data.context.children()[index])
		                .prepend('<br>')
		                .prepend(error);
		        });
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');



			$('#fileuploadOther').fileupload({
		        url: url,
		        dataType: 'json',
		        autoUpload: false,
		        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|doc?x|)$/i,
		        maxFileSize: 5100000, // 5 MB
		        // Enable image resizing, except for Android and Opera,
		        // which actually support image resizing, but fail to
		        // send Blob objects via XHR requests:
		        // disableImageResize: /Android(?!.*Chrome)|Opera/
		        //     .test(window.navigator.userAgent),
		        previewMaxWidth: 50,
		        previewMaxHeight: 50,
		        previewCrop: true
		    }).on('fileuploadadd', function (e, data) {

		    	console.log('fileupload has been triggered');
 
		        data.context = $('<div class="image-files"/>').appendTo('#files3');		        

		        $.each(data.files, function (index, file) {

		        	console.log(index);
		        	var node = $('<div />');

		        	var node2  = $('<div/>')
		                    .append($('<p class="filenames" />').text( StringObj.generateShortname(file.name) ));

		            if ( !index ) {
		                var node3 =  $('<div/>')
		                	.append(deleteLinkEmp.clone(true).css('display', 'none').data(data));
		                	// .append(uploadButton.clone(true).data(data));
		            }

		            node.appendTo(data.context);
		            node2.appendTo(data.context);
		            node3.appendTo(data.context);
		        });


		    }).on('fileuploadprocessalways', function (e, data) {

		    	console.log('fileuploadprocessalways has been triggered'); 
		    	console.log(data);		    	

		    	$('.btnSelectOther').attr( 'disabled', 'disabled' );

		        var index = data.index,
		            file  = data.files[index],
		        	node  = $(data.context.children()[index]),
    				ext   = StringObj.getExtension(file.name);

		        if (file.preview)
		            node.prepend(file.preview);
		        else
		        	node.prepend('<div class="img-file-icon">'+ ext.toUpperCase() +'</div>');

		        if (file.error) {
		            $(data.context)
		                .prepend('<br>')
		                .prepend($('<span class="text-danger"/>').text(file.error + '. (Max size 5MB)'));
		        
		            $('#files3').show();
		            $('.btnSelectOther').removeAttr('disabled');
		            $('#files3 .glyphicon-remove').show();	 

		        }else{ 
			        data.submit();			        
			        $('.other-income-wrapper .progress').show();
		        }

		        if (index + 1 === data.files.length) {
		            data.context.find('button:first-child')
		                .text('Upload')
		                .prop('disabled', !!data.files.error);
		            data.context.find('button:last-child')
		                .text('X')
		                .removeProp('disabled');

		        

		        }
		    }).on('fileuploadprogressall', function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('.other-income-wrapper .progress-bar').css(
		            'width',
		            progress + '%'
		        );

		    }).on('fileuploaddone', function (e, data) {

				console.log('fileuploaddone triggered');				

		        $.each(data.result.files3, function (index, file) {
		        	console.log(file);
		            /*if (file.url) { 
		                $(data.context.children()[index+2]).append(deleteLink.clone(true).data(data) );
		                    // .wrap(link);
		            } else */

		            if (file.error) {
		                var error = $('<span class="text-danger"/>').text(file.error);
		                $(data.context)
		                    .prepend('<br>')
		                    .prepend(error);
		            }

		            files.push(file.name)

		        });

		        $('#files3').show();
		        $('.other-income-wrapper .progress-bar').css('width', '0%');
		        $('.other-income-wrapper .progress').hide();
		        $('.btnSelectOther').removeAttr( 'disabled' );
				$('#files3 .glyphicon-remove').show();	        
		        $('#CheckImageData').val(JSON.stringify(files));

		    }).on('fileuploadfail', function (e, data) {
		    	console.log(data);
		        $.each(data.files, function (index) {
		            var error = $('<span class="text-danger"/>').text('File upload failed.');
		            $(data.context)
		                .prepend('<br>')
		                .prepend(error);

		            $('.btnSelectOther').removeAttr('disabled');
		        });
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');

			// income verification		
			@if($selfEmployed)
				$('.btnSelectFileIncome').unbind('click');
				$('.btnSelectFileIncome').attr('disabled', 'disabled');
			@endif

		});
		</script>
@stop
