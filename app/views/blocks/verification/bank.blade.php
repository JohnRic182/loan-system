@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop
@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1>
				Please confirm your bank account
			</h1>
	    </div> 
	</div>
	<div id="application" class="container">
		 {{ Form::open(array('url' => '/postBankVerification', 'method' => 'POST', 'id' => 'bankVerification','class' => 'form-horizontal' , 'role' => 'form')) }}
		 <div class="content-wrapper verification box-wrapper">
		 	<div class="row">
		 		<div class="col-sm-8">
		 			<div id="income" class="bankVerificationPanel" style="margin-top:25px">

						<p style="width: 100%; font-weight: bold">Please choose how you would like to confirm your bank account information.</p>
						<hr>
						<div class="row-fluid"> 
							<div id="notification">
								@if(isset($message))
									<div class="alert alert-danger">{{ $message }}</div>
								@endif
							</div>   
							<div class="form-group paystub-wrapper bankOptions" style="text-align: center; border-bottom: 1px solid #eee;">

								<div class="col-sm-6 paystubs">
									
									<button type="button" class="btn btn-cta btnLinkBankAccount" rel="1">LINK BANK ACCOUNT<BR/>(RECOMMENDED)</button> 
									
									<label for="Employer">
										<span>Provide read-only access to your account for faster, secure verification.</span>
									</label>


								</div>										

								<div class="horizontal" align="center">
									<span class="separatorText"> OR </span>
									<hr>
								</div>

								<div class="col-sm-6 tax-wrapper" style="position:relative">

								<span class="separatorText">OR</span>

									<button type="button" class="btn btnUploadStatements btn-cta-inactive">UPLOAD LAST 3<BR/>STATEMENTS</button> 
									
									<label for="Employer">
										<span>Manually upload your last 3 checking account statements.</span>
									</label>	
									{{-- <button type="button" class="btn btn-cta btnSelectFileTax">Select Tax Filling 2</button>  --}} 
								</div> 
							</div>
							<input type="hidden" value="" name="CheckImageData" id="CheckImageData">
							<input type="hidden" value="" name="Employment_Type_Cd" id="Employment_Type_Cd"> 
							<div id="bank_fastLink" style="display: none; float: left;">
								<h3>LINK BANK ACCOUNT</h3>
								
								<p>Enter your login credentials to allow us to link to your bank account and download the most recent 90 days of transactions.</p>
								<div id="bank-wrapper-search" class="form-wrapper row">
									<iframe src="{{ $fastLinkUrl }}" frameborder="0" width="75%" height="100%" scrolling="no" style="min-height:550px;"></iframe> 
									<p class="text-center" style="float: none"><b>Click close to continue to the next step</b></p>
								</div>
							</div>
							<div id="bank_statements" style="display: none">
								<h3>BANK STATEMENT UPLOAD</h3>
								<p style="width: 100%">Upload your last 3 statements for your primary checking account</p><br/>
								<div class="statement1" style="margin-top 20px;"> 
									<div class="progress" style="width: 100%">
										<div class="progress-bar progress-bar-success"></div>
									</div>  
								</div>	
								<div class="row">
									<div id="files1" class="files col-sm-6"></div>
								</div>
								<button type="button" class="btn btn-cta btnSelectFileIncome" rel="1">UPLOAD STATEMENT 1</button> <br/>
								<button type="button" class="btn btn-cta btnSelectFileIncome" rel="2">UPLOAD STATEMENT 2</button> <br/>
								<button type="button" class="btn btn-cta btnSelectFileIncome" rel="3">UPLOAD STATEMENT 3</button> 
							</div>
						</div>
					</div>
		 		</div>	
		 		<div class="col-sm-4">
		 			@include('includes.sidebar')
		 		</div>
		 	</div>
			
		 </div>
		 <div class="action-section row">
		 	<div class="col-sm-8">

				<div class="row-fluid">
					
					<div class="pull-right col-sm-6 col-xs-12"> 
						<button class="btn btn-cta btnBankVerification" style="display: none; margin-left: 170px">Submit <span class="glyphicon glyphicon-chevron-right"></span></button>
					</div>

					<div class=" col-sm-6 col-xs-12">
						<a href="../verification/steps" class="cta-link backLink" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a> 
					</div>

					
				</div>

				{{-- <div class="clearfix"></div> --}}
			</div>
		</div>
		{{ Form::close() }}

		<span id="Statement1" class="btn btn-cta fileinput-button hide"> 
	        <span>Select Paystub 2</span>
	        <input id="fileupload" type="file" name="files[]" multiple>
	    </span> 

	</div>
	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/verification.js');  }}

	{{ HTML::script( 'js/fileUpload/vendor/jquery.ui.widget.js');  }}

	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

	{{ HTML::script( 'js/fileUpload/jquery.iframe-transport.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-process.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-image.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-validate.js');  }} 
	
	<script>

		/*jslint unparam: true, regexp: true */
		/*global window, $ */
		$(function () {
		    'use strict';

		    var files = [];
		    var payStubBtn;
		    var statementUploadCount = 0;

		    $('.btnSelectFileIncome').click(function(){
				payStubBtn = $(this).attr('rel');
			});

		    /**
			 * Extend Serialize Object
			 * @return {[type]}
			 */
			$.fn.serializeObject = function()
			{
			    var o = {};
			    var a = this.serializeArray();
			    $.each(a, function() {
			        if (o[this.name] !== undefined) {
			            if (!o[this.name].push) {
			                o[this.name] = [o[this.name]];
			            }
			            o[this.name].push(this.value || '');
			        } else {
			            o[this.name] = this.value || '';
			        }
			    });
			    return o;
			};


		    // Change this to the location of your server-side upload handler:
		    var url = baseUrl + 'verification/uploadBankVerification';
 			
	        var uploadButton = $('<button/>')
	            .addClass('btn btn-cta uploadActionBtn hide')
	            .prop('disabled', true)
	            .text('Processing...')
	            .on('click', function () {
	                var $this = $(this),
	                    data = $this.data(); 
	                    // console.log(data);
	                $this
	                    .off('click')
	                    .text('Abort')
	                    .on('click', function () {
	                        $this.remove();
	                        data.abort();
	                    });
	                data.submit().always(function () {
	                	$this.next().remove();
	                    $this.remove();
	                });

	                return false;
	            });

         	var deleteLink  = $('<span/>')
         			.prop('class', 'glyphicon glyphicon-remove').on('click', function(){

 				var data 	= $(this).data();
 				var dltBtn 	= $(this).attr('rel');
			 
 				//empty files
     			 $(this).parents('.image-files').remove();

 				if( $('#files1').find('.image-files').size() == 0 ) {

 					console.log('tset')
 					//reset file input
 					data.fileInput.val('');	         					
     				$('#CheckImageData').val('');
     				files = [];
 				} 

 				// reset the progress bar
 				$('#progress .progress-bar').css(
		            'width','0%'
		        );

		        //show buttons once upload was cancelled
 				$('.btnSelectFileIncome[rel="'+ dltBtn +'"]').show();
 				statementUploadCount--;
 				if(statementUploadCount < 3)
	            	$('.btnBankVerification').css('display','none');

         	});

	        // Statement 1			
		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        autoUpload: false,
		        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png|pdf|doc?x|)$/i,
		        maxFileSize: 5100000, // 5 MB
		        // Enable image resizing, except for Android and Opera,
		        // which actually support image resizing, but fail to
		        // send Blob objects via XHR requests:
		        // disableImageResize: /Android(?!.*Chrome)|Opera/
		        //     .test(window.navigator.userAgent),
		        previewMaxWidth: 50,
		        previewMaxHeight: 50,
		        previewCrop: true
		    }).on('fileuploadadd', function (e, data) {

		    	console.log('fileupload has been triggered');
 
		    	
		    	// $('#files1').html('');
 
		        //data.context = $('<div class="image-files"/>').appendTo('#files1');
		        data.context = $('<div class="image-files"/>').attr('rel', payStubBtn ).appendTo('#files1');

		        $.each(data.files, function (index, file) {

		        	console.log(index);

		        	//var node = $('<div class="col-sm-3"/>');
		        	var node = $('<div/>');

					//var node2  = $('<div class="col-sm-6 nopad-left"/>') 
					var node2  = $('<div />') 
		                    .append($('<p class="filenames" />').text( StringObj.generateShortname(file.name) ));

		            if ( !index ) {
		                //var node3 =  $('<div class="col-sm-1"/>')
		                var node3 =  $('<div />')
			                //.append(deleteLink.clone(true).data(data))
			                .append(deleteLink.clone(true).attr( 'rel', payStubBtn ).data(data))
		                	// .append(cancelButton.clone(true).data(data));	                	
		                	
		            }
		            node.appendTo(data.context);
		            node2.appendTo(data.context);
		            node3.appendTo(data.context);

		        });

		        $('#files1 .image-files[rel="'+ payStubBtn +'"]').hide();		        
		        $('.paystubs .btnSelectFileIncome[rel="'+ payStubBtn +'"]').hide();	        

		    }).on('fileuploadprocessalways', function (e, data) {

		    	console.log('fileuploadprocessalways has been triggered'); 
		    	console.log(data);

		    	if(payStubBtn == 1){
		    		$('.btnSelectFileIncome[rel="2"]').attr('disabled', 'disabled');
		    		$('.btnSelectFileIncome[rel="3"]').attr('disabled', 'disabled');
		    	}else if(payStubBtn == 2){
		    		$('.btnSelectFileIncome[rel="1"]').attr('disabled', 'disabled');
		    		$('.btnSelectFileIncome[rel="3"]').attr('disabled', 'disabled');
		    	}else{
		    		$('.btnSelectFileIncome[rel="1"]').attr('disabled', 'disabled');
		    		$('.btnSelectFileIncome[rel="2"]').attr('disabled', 'disabled');
		    	}
		    	

		        var index = data.index,
		            file = data.files[index],
		            node = $(data.context.children()[index]),
		            ext   = StringObj.getExtension(file.name);

		        if (file.preview)
		            node.prepend(file.preview);
		        else
		        	node.prepend('<div class="img-file-icon">'+ ext.toUpperCase() +'</div>');

		        if (file.error) {
					
					$(data.context)
						.prepend('<br>')
						.prepend($('<span class="text-danger"/>').text(file.error + '. (Max size 5MB)'));

					$('#files1 .image-files[rel="'+ payStubBtn +'"]').show();
		            $('#CheckImageData').val('');
		            $('.btnSelectFileIncome').removeAttr('disabled');

		        }else{
		        	data.submit();		        	
			        $('.statement1 .progress').show();
		        }
		        
		        if (index + 1 === data.files.length) {
		            data.context.find('button:first-child')
		                .text('Upload')
		                .prop('disabled', !!data.files.error);
		            data.context.find('button:last-child')
		                .text('X')
		                .removeProp('disabled');

		        }
		        $('.btnSelectFileIncome[rel="'+ payStubBtn +'"]').hide();
		    }).on('fileuploadprogressall', function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('.statement1 .progress-bar').css(
		            'width',
		            progress + '%'
		        );
		    }).on('fileuploaddone', function (e, data) {
				
		    	console.log(data.result.files1);

		        $.each(data.result.files, function (index, file) {

		            /*if (file.url) { 
		                $(data.context.children()[index+2]).append(deleteLink.clone(true).data(data) );
		                    // .wrap(link);
		            } else */

		            if (file.error) {
		                var error = $('<span class="text-danger"/>').text(file.error);
		                $(data.context.children()[index])
		                    .prepend('<br>')
		                    .prepend(error);
		            }

		            files.push(file.name)
		            $('#files1 .image-files[rel="'+ payStubBtn +'"]').show();

		            statementUploadCount++;
		            if(statementUploadCount >= 3)
		            	$('.btnBankVerification').css('display','block');
		        });

				$('#files1').show();
				$('.statement1 .progress').hide();
				$('.statement1 .progress-bar').css('width','0%');
		        $('#CheckImageData').val(JSON.stringify(files));

		        if(payStubBtn == 1){
		    		$('.btnSelectFileIncome[rel="2"]').removeAttr('disabled');
		    		$('.btnSelectFileIncome[rel="3"]').removeAttr('disabled');
		    	}else if(payStubBtn == 2){
		    		$('.btnSelectFileIncome[rel="1"]').removeAttr('disabled');
		    		$('.btnSelectFileIncome[rel="3"]').removeAttr('disabled');
		    	}else{
		    		$('.btnSelectFileIncome[rel="1"]').removeAttr('disabled');
		    		$('.btnSelectFileIncome[rel="2"]').removeAttr('disabled');
		    	}



		    }).on('fileuploadfail', function (e, data) {
		    	console.log(data);
		        $.each(data.files, function (index) {
		            var error = $('<span class="text-danger"/>').text('File upload failed.');
		            $(data.context.children()[index])
		                .prepend('<br>')
		                .prepend(error);
		        });
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');			

		});
		</script>
@stop
