@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop
@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1>
				{{-- <span class="appl-step">6</span> --}}
				Congratulations, we've approved your loan request.
			</h1>
	    </div> 
	</div>

<div class="container" id="application">  
	 <div class="content-wrapper verification step6 box-wrapper">
	 	<div class="row">
	 		<div class="col-sm-8">
	 			@if( isset($result) && $result == 'success' )
	 				<br>
					<p>Thank you for your application.  Our loan officers will verify the information you have provided and may contact you with questions.</p>

					<hr>

					<p class="payment-process">Your payment <br>should be processed by:</p>
					<p class="text-center date-result">{{ $processedDate }}</p>
					 
				@else
					<h4>We regret that we have no loans that match your credit profile.</h4>
					<br>
					<br>
					<div class="text-center">
						<p>Thank you for your interest in</p> 
						<a href="{{ url('/') }}">{{HTML::image(asset('img/logoBig.png'))}}</a>
					</div>
				@endif
	 		</div>
	 		<div class="col-sm-4">
	 			@include('includes.sidebar')
	 		</div>
	 	</div> 
	 </div>
	 <div class="action-section row">
		<div class="col-sm-8">
			<div class="pull-left">
			&nbsp;
				{{-- {{ HTML::link('verification/steps', 'Back',array('class' => 'cta-link')) }} --}}
			</div>
			<div class="pull-right"> 
			&nbsp;
				{{-- <button class="btn btn-cta">Done</button> --}}
			</div>
			<div class="clearfix"></div>
		</div>
	</div> 
</div>

@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/verification.js');  }}
@stop