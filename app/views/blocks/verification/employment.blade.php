@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop
@section('content') 

	<div class="appl-title">
	    <div class="container">
	    	<h1>
				{{-- <span class="appl-step">2</span> --}}				
				Please provide a few details about yourself.
				{{-- <span class="required-not">Required</span> --}}
			</h1>
	    </div> 
	</div>

<div class="container" id="application"> 
	{{ Form::open(array('url' => '/verification/postEmploymentVerification', 'method' => 'POST', 'id' => 'EmploymenVerificationForm','class' => 'form-horizontal validate' , 'role' => 'form')) }}
		<div class="content-wrapper verification box-wrapper">
			<div class="row">
				<div class="col-sm-8">
					<div id="employmentVerification"> 
						<h2>
							<span class="icon icon-info"></span>
							Provide Employment Information
							<span class="required-not">Required</span>
						</h2>
						 
						<div class="row-fluid">
							<div id="notification">
								@if(isset($message))
									<div class="alert alert-danger">{{ $message }}</div>
								@endif
							</div>
							<p class="text-uppercase">Are you self employed?</p>
							<a href="#" class="btn btn-cta btn-cta-inactive btn-employment" data-value="yes">Yes</a>
							<a class="separator"></a>
							<a href="#" class="btn btn-cta btn-employment" data-value="no">No</a>

							<input type="hidden" name="SelfEmployed" value="no" id="SelfEmployed"/>

			 				<hr>
			 				<div class="form-group-wrapper employed">
								<p>Thank you.  You will be asked to provide your tax returns as proof of income.</p>
			 				</div>
							<div class="form-group-wrapper unemployed">
								<div class="form-group">
									<div class="col-sm-6">
										<label for="Employer">Company Name</label>
										{{ Form::text('EmployerName', '', array('class' => 'form-control required', 'placeholder' => 'Enter Company Name', 'alt' => 'Company Name', 'data-message' => "Company Name")); }}
									</div>
									<div class="col-sm-6">
										<label for="WorkPhoneNr">&nbsp</label>
										{{ Form::text('WorkPhoneNr', '', array('class' => 'form-control required number-only', 'id' => 'WorkPhoneNr', 'placeholder' => 'Enter Company Phone Number', 'alt' => 'Company Phone Number', 'data-message' => "Enter Company Phone Number")); }}
									</div>
								</div>	
								<div class="form-group">
									<div class="col-sm-6">
										<label for="Employer">Company Address</label>
										{{ Form::text('StreetAddress1', '', array('class' => 'form-control required', 'placeholder' => 'Enter Street Address 1', 'alt' => 'Street Address 1')); }}
									</div>
									<div class="col-sm-6">
										<label for="Employer">&nbsp;</label>
										{{ Form::text('StreetAddress2', '', array('class' => 'form-control', 'placeholder' => 'Enter Street Address 2', 'alt' => 'Street Address 2')); }}
									</div>
								</div>
								<div class="form-group"> 
									<div class="col-sm-6">
										<label for="City">City</label>
										{{ Form::text('City', '', array('class' => 'form-control required', 'placeholder' => 'Enter City', 'alt' => 'City', 'max-lenght' => '5' )); }}
									</div>

									<div class="col-sm-3">
										<label for="State">Zip Code</label>
										{{ Form::select('State', $states, 'key', array('class' => 'form-control required','alt' => 'State'));}}
									</div>
									<div class="col-sm-3">
										<label for="Zip">&nbsp</label>
										{{ Form::text('Zip', '', array('class' => 'form-control required number-only numberOnly', 'placeholder' => 'Zip', 'alt' => 'Zip')); }}
									</div>
								</div> 
								<div class="form-group">
									
									<div class="col-sm-6">
										<label for="ContactPersonName">Supervisor Contact Information</label>
										{{ Form::text('ContactPersonName', '', array('class' => 'form-control required', 'placeholder' => 'Enter Name', 'alt' => 'Supervisor Contact Name')); }}
									</div>
									<div class="col-sm-6">
										<label for="SupervisorPhoneNr">&nbsp;</label>
										{{ Form::text('SupervisorPhoneNr', '', array('class' => 'form-control required number-only', 'id' => 'SupervisorPhoneNr', 'placeholder' => 'Enter Supervisor Contact Number', 'alt' => 'Enter Supervisor Contact Number')); }}
									</div>
								</div> 
								<div class="form-group">
									
									<div class="col-sm-6">
										<label for="PhoneNr">Employment Title</label>
										{{ Form::text('JobTitleName', '', array('class' => 'form-control', 'id' => 'JobTitleName', 'placeholder' => 'Enter Employment Title', 'alt' => 'Employment Title')); }}
									</div>
									<div class="col-sm-6">
										<label for="Employer">Time at current job</label> 
										<select name="Tenureship" class="form-control required" id="Tenureship" alt="Tenureship">
											<option value="">Select Job Tenure</option>
											<option value="1">Less than 6 months</option>
											<option value="2">6-12 months</option>
											<option value="3">1-2 years</option>
											<option value="4">>2 years</option>
										</select> 
									</div>
								</div>
								<div class="form-group">
									
									<div class="col-sm-6">
										<label for="Employer">Income Type</label>
										<select name="IncomeType" class="form-control required" id="IncomeType" alt="Income Type">
											<option value="">Select income Type</option>
											<option value="Salary">Salary</option>
											<option value="Hourly">Hourly</option>
											<option value="Hourly">Commission only</option>
											<option value="Hourly">Salary plus commission</option>
										</select>
									</div> 
									<div class="col-sm-6">
										<label for="Employer">Pay Frequency</label>
										<select name="PayFrequency" class="form-control required" id="PayFrequency" alt="Pay Frequency">
											<option value="">Select Pay Frequency</option>
											<option value="twice_monthly">Twice monthly</option>
											<option value="every_other_week">Every other week</option>
											<option value="monthly">Monthly</option>
											<option value="other">Other</option>
										</select> 
									</div>  
								</div>
								<div class="form-group">
									<div class="col-sm-6">
										<label for="PhoneNr">Your Work Phone Number</label>
										{{ Form::text('PhoneNr', '', array('class' => 'form-control required number-only', 'id' => 'PhoneNr', 'placeholder' => 'Enter Phone Number', 'alt' => 'Work Phone Number')); }}
									</div>
								</div>
							</div> 
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
		</div>
		<div class="action-section row">

			<div class="col-sm-8">
				<div class="row-fluid">
					<div class="pull-right col-sm-6 col-xs-12">
						<button class="btn btn-cta btnSubmitVerification pull-right">Submit2 <span class="glyphicon glyphicon-chevron-right"></span></button>
					</div>
					<div class=" col-sm-6 col-xs-12">
						<a href="../verification/steps" class="cta-link backLink" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a> 
					</div>
				</div>
				{{-- <div class="clearfix"></div> --}}
			</div>
		</div>
	{{ Form::close() }}
</div>
	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/verification.js');  }}
	{{ HTML::script( 'js/jquery.maskedinput.min.js');  }}

	<script type="text/javascript">

		$(document).ready(function(){
			$("#WorkPhoneNr").mask("999-999-9999");
			$("#SupervisorPhoneNr").mask("999-999-9999");
			$("#PhoneNr").mask("999-999-9999");



		});

	</script>

@stop
