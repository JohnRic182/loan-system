@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop
@section('content') 
{{HTML::style('css/jquery.fileupload.css')}}
{{HTML::style('css/jquery.fileupload-ui.css')}}

	<div class="appl-title">
	    <div class="container">
	    	<h1>
				Tell Us Where You'd Like Loan Proceeds Deposited
			</h1>
	    </div> 
	</div>
	<div class="container" id="application">
		{{ Form::open(array('url' => '/verification/postFundingVerification', 'method' => 'POST', 'id' => 'FundingVerification','class' => 'form-horizontal' , 'role' => 'form', 'files' => true )) }}
		 <div class="content-wrapper verification box-wrapper">
		 	<div class="row">
		 		<div class="col-sm-8">
					<div id="funding">
						<div class="row-fluid"> 
							<br>
							<div id="notification">
								@if(isset($message))
									<div class="alert alert-danger">{{ $message }}</div>
								@endif
							</div>  

							<div class="form-group row"> 
								<div class="col-md-offset-1 col-sm-5">	
									{{ Form::label('AcctFirstName', 'Name on Account*') }} 
									{{ Form::text('AcctFirstName', '', array('class' => 'form-control required', 'placeholder' => 'Enter First Name', 'alt' => 'First Name')); }}
								</div>
								<div class="col-sm-5">
									{{ Form::label('', '',  array('class' => 'empty-label' )) }} 
									{{ Form::text('AcctLastName', '', array('class' => 'form-control required', 'placeholder' => 'Enter Last Name', 'alt' => 'Last Name')); }}
								</div>
							</div>

							<div class="row">							
									<div class="col-md-5 col-md-offset-1">
										<div class="form-group"> 
											<div class="col-sm-12">
												{{ Form::label('BankRoutingNr', 'Bank Routing Number*') }} 
												{{ Form::text('BankRoutingNr', '', array('class' => 'form-control required number-only accountNumber', 'placeholder' => 'Enter Bank Routing Number', 'alt' => 'Bank Routing Number', 'maxlength' => 9 )); }}
											</div>
										</div>	 

										<div class="form-group"> 
											<div class="col-sm-12">
												{{ Form::label('AcctNr', 'Account Number*') }} 
												{{ Form::text('AcctNr', '', array('class' => 'form-control required number-only', 'placeholder' => 'Enter Bank Account Number', 'alt' => 'Bank Account Number', 'maxlength' => 17 )); }}
											</div>
										</div>
									</div>
									<div class="col-md-6 sample-check">										

										{{-- <img src="http://placehold.it/330x130"> --}}
										{{ HTML::image( asset('img/check.png'),'' , array('class' => 'checkimg') )}}
										<!-- The container for the uploaded files -->
								    	<div id="files" class="files" style="margin-left:20px"></div> 
									</div>
							</div>



							<div class="form-group">

								<div class="col-md-offset-6 col-sm-6">
									<div class="progress">
										<div class="progress-bar progress-bar-success"></div>
									</div> 

									<button class="btn btn-cta btnSelectFile">Select Voided Check</button>
								</div>


							</div>

							<input type="hidden" name="CheckImageData" id="CheckImageData" />  
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
		 </div>
		<div class="action-section row">
		 	<div class="col-sm-8">

		 		<div class="row-fluid">
			 		<div class="pull-right col-sm-6 col-xs-12"> 
						<button type="button" class="btn btn-cta btnSubmitVerification pull-right" id="btnUpload">Submit <span class="glyphicon glyphicon-chevron-right"></button>
					</div>
		 		</div>
		 		<div class=" col-sm-6 col-xs-12">
					<a href="../verification/steps" class="cta-link backLink" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a> 
				</div>				
				{{-- <div class="clearfix"></div> --}}
			</div>
		</div>
		{{ Form::close() }}
		
		<span class="btn btn-cta fileinput-button hide"> 
	        <span>Select Image of Voided Check</span>
	        <!-- The file input field used as target for the file upload widget -->
	        <input id="fileupload" type="file" name="files[]" multiple>
	    </span>  
	</div>
	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/verification.js');  }}

	{{ HTML::script( 'js/fileUpload/vendor/jquery.ui.widget.js');  }}

	<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
	<!-- The Canvas to Blob plugin is included for image resizing functionality -->
	<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

	{{ HTML::script( 'js/fileUpload/jquery.iframe-transport.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-process.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-image.js');  }}
	{{ HTML::script( 'js/fileUpload/jquery.fileupload-validate.js');  }} 
	
	<script>
		/*jslint unparam: true, regexp: true */
		/*global window, $ */
		$(function () {
		    'use strict';


		    /**
			 * Extend Serialize Object
			 * @return {[type]}
			 */
			$.fn.serializeObject = function()
			{
			    var o = {};
			    var a = this.serializeArray();
			    $.each(a, function() {
			        if (o[this.name] !== undefined) {
			            if (!o[this.name].push) {
			                o[this.name] = [o[this.name]];
			            }
			            o[this.name].push(this.value || '');
			        } else {
			            o[this.name] = this.value || '';
			        }
			    });
			    return o;
			};


		    // Change this to the location of your server-side upload handler:
		    var url = baseUrl + 'verification/uploadFundingCheck';
  
         	//TODO: add option to delete the image in the server
         	var deleteLink  = $('<span/>')
         			.prop('class', 'glyphicon glyphicon-remove').on('click', function(){
         				
         				var data = $(this).data();

         				//reset file input
         				data.fileInput.val(''); 

         				$('#CheckImageData').val('');

         				//reset the progress bar
         				$('#progress .progress-bar').css(
				            'width','0%'
				        );

         				//empty files
         				$(this).parents('.files').html('');

         				//show buttons once upload was cancelled
         				$('.btnSelectFile').show();

         		});

		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        autoUpload: false,
		        // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
		        maxFileSize: 5100000, // 5 MB
		        // Enable image resizing, except for Android and Opera,
		        // which actually support image resizing, but fail to
		        // send Blob objects via XHR requests:
		        // disableImageResize: /Android(?!.*Chrome)|Opera/
		        //     .test(window.navigator.userAgent),
		        previewMaxWidth: 50,
		        previewMaxHeight: 50,
		        previewCrop: true

		    }).on('fileuploadadd', function (e, data) {

		    	console.log('fileupload has been triggered');

		    	//remove the first index
		    	$('#files').html("");

		        data.context = $('<div class="image-files"/>').appendTo('#files');

		        $.each(data.files, function (index, file) {
		            
		            var node = $('<div/>');
		            
		            var node2 = $('<div/>') 
		                    .append($('<p class="vr-filenames" />').text( StringObj.generateShortname(file.name) ));

		            if (!index)
		                var node3 = $('<div/>').append(deleteLink.clone(true).data(data)); 
 
		            node.appendTo(data.context);
		            node2.appendTo(data.context);
		            node3.appendTo(data.context);
		        });

		        $('#files').hide();

		    }).on('fileuploadprocessalways', function (e, data) {
		    	// console.log('fileuploadprocessalways has been triggered');

		        var index 	= data.index,
		            file 	= data.files[index],
		            node 	= $(data.context.children()[index]),
		            ext   	= StringObj.getExtension(file.name);
		        
		        if (file.preview) 
		            node.prepend(file.preview);
		        else
		        	node.prepend('<div class="img-file-icon">'+ ext.toUpperCase() +'</div>');

		        if (file.error) {
		            $(data.context)
		                .prepend('<br>')
		                .prepend($('<span class="text-danger"/>').text(file.error + '. (Max size 5MB)'));

		            $('#files').show();

		        } else {
		        	data.submit();		        	
			        $('.progress').show();
		        }

		        $('.btnSelectFile').hide();

		    }).on('fileuploadprogressall', function (e, data) {
		        var progress = parseInt(data.loaded / data.total * 100, 10);
		        $('.progress-bar').css(
		            'width',
		            progress + '%'
		        );
		    }).on('fileuploaddone', function (e, data) {

		    	// console.log(data.files[0].name);

		    	$('#CheckImageData').val(data.files[0].name);
  
		        $.each(data.result.files, function (index, file) {

		            if (file.error) {
		                var error = $('<span class="text-danger"/>').text(file.error);
		                $(data.context.children()[index])
		                    .prepend('<br>')
		                    .prepend(error);
		            }

		            // files.push(file.name)

		        });

		        $('#files').show();
		        $('.progress').hide();
				$('.progress-bar').css('width','0%');
		        // $('#CheckImageData').val(JSON.stringify(files));
 
		    }).on('fileuploadfail', function (e, data) { 
		        $.each(data.files, function (index) {
		            var error = $('<span class="text-danger"/>').text('File upload failed.');
		            $(data.context.children()[index])
		                .prepend('<br>')
		                .prepend(error);
		        });
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
		</script>
@stop