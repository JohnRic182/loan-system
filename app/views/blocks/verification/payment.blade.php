@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop
@section('content') 
	<div class="appl-title">
	    <div class="container">
	    	<h1>
				Choose your payment method
			</h1>
	    </div> 
	</div>
	<div class="container" id="application"> 

		{{ Form::open(array('url' => '/verification/postPaymentVerification', 'method' => 'POST', 'id' => 'PaymentVerification','class' => 'form-horizontal validate' , 'role' => 'form')) }}

		
		<input type="hidden" name="monthlyPayment" value="{{ $monthlyPayment }}"/>
		<input type="hidden" name="bankRoutingNr" value="{{ $bankRoutingNr }}"/>
		<input type="hidden" name="bankAccntNr" value="{{ $bankAccntNr }}"/>
		<input type="hidden" name="companyName" value="{{ $companyName }}"/>

		<div class="content-wrapper verification box-wrapper">
		   <div class="row">
		   	  <div class="col-sm-8">
			      <div class="paymentVerification row-fluid"> 
			      	 <br>
			         @if( $isFundingStepComplete  == FALSE )
			         	<p class="alert alert-warning"><span>You will need to verify your bank account information before completing this step</span></p>
			         @endif
			       	
			         <div class="col-md-12">
			            <div id="notification">
			               @if(isset($message))
			               <div class="alert alert-danger">{{ $message }}</div>
			               @endif
			            </div>
			            <div class="form-group row active">
			               <div class="col-sm-4">
			                  <input type="radio" name="paymentMethodId" checked="checked" value="1"> <strong>Electronic Fund Transfer</strong>
			                  <br>(recommended)
			               </div>
			               <div class="col-sm-7 col-sm-offset-1">
			                  We will automatically process payment of the amount owed on your due date from the accont you have specified to receive loan proceeds. By selecting this option, you consent to the voluntary Electronic Payment Agreement found <a href="">here</a>.
			               </div>
			            </div>
			            <div class="form-group row">
			               <div class="col-sm-4">
			                  <input type="radio" name="paymentMethodId" value="2"> <strong>Manual Check</strong>
			               </div>
			               <div class="col-sm-7 col-sm-offset-1">
			                  Manually mail in your check for the amounts due each month.
			               </div>
			            </div>
			         </div>
			      </div>
		      </div>
		      <div class="col-sm-4">
		      	@include('includes.sidebar')
		      </div>
		   </div>
		</div>	 
		<div class="action-section row">
			<div class="col-sm-8">
				<div class="row-fluid">
					<div class="pull-right col-sm-6 col-xs-12"> 
						@if( $isFundingStepComplete == TRUE )
							<button class="btn btn-cta btnSubmitVerification btnPaymentVerification pull-right">Submit <span class="glyphicon glyphicon-chevron-right"></span></button>

						@else
							{{-- &nbsp; --}}
						@endif
					</div>

					<div class=" col-sm-6 col-xs-12">
						<a href="../verification/steps" class="cta-link backLink" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a> 
					</div>
				</div>
				{{-- <div class="clearfix"></div> --}}
			</div>
		</div> 
		{{ Form::close() }}
	</div>
@stop
@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/verification.js');  }} 
@stop