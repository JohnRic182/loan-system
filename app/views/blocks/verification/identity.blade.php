@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF"></span>
	</div>
@stop
@section('content')
	<div class="appl-title">
	    <div class="container">
	    	<h1>
				Please confirm your identity
			</h1>
	    </div> 
	</div>

	<div class="container" id="application">  
		<!-- {{ Form::open(array('url' => '/verification/identity', 'method' => 'POST', 'id' => 'IdentityVerification','class' => 'form-horizontal' , 'role' => 'form')) }} -->
			 <div class="content-wrapper verification box-wrapper">
			 	<div class="row">
			 		<div class="col-sm-8">
						<div id="employment">
							<br> 
							<p style="font-weight: bold">Please answer the following questions about yourself to confirm your identity.</p>
							<hr> 
							<div class="row-fluid">
								 	
								<div class="form-group" style="text-align: center">
									<p>Leaving this page once you have started the questions will close your application.</p>
									<a href="#" class="btn btn-cta btn-employment idologyFinal" data-value="yes">Begin</a>
									<a href="../verification/steps" class="btn btn-cta btn-employment" data-value="no">Close</a>
								</div>	  
															</div>
						</div>
						<div id="questions" style="display: none">
							<br> 
							<p style="font-weight: bold">Please answer the following questions about yourself to confirm your identity.</p>
							<p style="font-size: 13px;">Leaving this page once you have started the questions will close your application.</p>
								<h3>IDENTITY VERIFICATION QUESTIONS</h3>
							<hr> 
							<p>Answer the questions below to verify your identity</p>
							<form id="idologySubmitAnswers">
								<div id="notification"></div>
								<div id="questions_contents">
								</div>
								<div class="form-group" style="text-align: center">
		        					<button type="submit" class="btn btn-cta" >Submit</button>
		        				</div>
							</form>
						</div>
					</div>
					<div class="col-sm-4">
						@include('includes.sidebar')
					</div>
				</div>
			 </div>
		
			<div class="action-section row">
				<div class="col-sm-8">
					<div class=" col-sm-6 col-xs-12">
						<a href="../verification/steps" class="cta-link backLink" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a> 
					</div>
					
					<div class="clearfix"></div>
				</div>
			</div>
		<!-- {{ Form::close() }} -->
	</div>
	

	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/validate.js');  }}
	{{ HTML::script( 'js/verification.js');  }}	
	{{ HTML::script( 'js/idology.js');  }}
@stop
