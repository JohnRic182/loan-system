@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Creating Account...</span>
	</div>
@stop
@section('content') 
<div class="appl-title">
    <div class="container">
    	<h1>Sign Loan Contract</h1>
    </div> 
</div>
<div class="container" id="application"> 
	 <div class="content-wrapper verification box-wrapper">
	 	<div class="row">
		 	<div class="col-sm-12 banner-adjustment">
			 	@if($isComplete == true)
					Loan Contract has been already signed 
				@else 
					<div id="contract" style="overflow-x:scroll;">  
					</div>
				@endif
			</div>
		</div>
	 </div>
	 <div class="action-section row">
	 	<div class="col-sm-8">
			<div class="pull-left">
				<a href="../verification/steps" class="cta-link" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a> 
			</div>
			<div class="pull-right"> 
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
	{{--Transfer this to another block--}}
@stop

@section('scripts')
	{{ HTML::script( 'js/common.js');  }} 
	{{ HTML::script( 'js/verification.js');  }} 
	@if($isComplete != true )
		<script type="text/javascript" src="//s3.amazonaws.com/cdn.hellofax.com/js/embedded.js"></script>
		@foreach( $signatureIds as $k => $v )
			<script>

				var loader  = $('#loader');

				var clientId  = "<?php echo $clientId;?>";
				var signInURL = "<?php echo $v->sign_url ;?>"; 
				var debugMode = "<?php echo $debugMode; ?>";
				var skipDomainVerification =  "<?php echo $skipDomainVerification; ?>";

				console.log(skipDomainVerification);
				
				HelloSign.init(clientId);

				HelloSign.open({
				    url: signInURL,
				    // redirectUrl: baseUrl + 'verification/steps',
				    debug:debugMode,
				    width:'100%',
				    height:640,
				    skipDomainVerification: skipDomainVerification, 
				    container:document.getElementById('contract'),
				    allowCancel: true,
				    messageListener: function(eventData) { 

				    	console.log(eventData);

						loader.fadeIn().find('span').html('Updating Contracts...');

				    	if ( eventData.event == 'signature_request_signed' ) {

				    		//delayed calling the update Loan Document callback 
				    		setTimeout( function(){ 
					    		$.ajax({
					    			url :  baseUrl + 'verification/updateLoanDocument', 
					    			type: 'GET',
					    			success: function( response ) {

					    				console.log(response);

					    	 			// Go to a signature request sent confirmation page	
					    	 			document.location = baseUrl + 'verification/steps'; 
					    	 			loader.fadeOut();
					    	 			
					    			}, error: function (xhr, ajaxOptions, thrownError) {
								        console.log(xhr.status);
								        console.log(thrownError);
							      }
					    		})
				    		}, 15000 );  
					    } else {
					    	loader.fadeOut();
					    }
					}
				}); 

			</script> 
		@endforeach
	@endif
@stop