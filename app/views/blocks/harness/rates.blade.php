@section('content')
<div class="container inner-container">
	<div class="content content-wrapper"> 
		<h1 style="color:red">Loan Product Decisioning Test Harness</h1>
		<p>This is a test harness for Loan Product Decisioning. Use the Form in the left.</p>
		<div class="row">
			<div class="col-sm-6">
				<h2>Parameters:</h2>
				<form action="" id="testLoanRate" class="validate">
					<div class="form-group">
						<label for="">ARS Score</label>
						<input type="text" name="ARSScore" value="{{ $actual['ARS'] }}" class="required number-only form-control" placeholder="Please enter a number between 100 to 500">
					</div>
					<div class="form-group">
						<label for="">Loan Type</label>
						<select name="loanType" id="" class="form-control">
							<option value="1" {{ ($actual['LoanTypeId'] == 1 ) ? 'selected' : "" }}>Ascend Loan</option>
							<option value="2" {{ ($actual['LoanTypeId'] == 2 ) ? 'selected' : "" }}>Rate Rewards Loan</option>
						</select>
					</div>
					<div class="form-group">
						<label for="">Annual Gross Income</label>
						<input type="text" name="AnnualGrossIncome"  class="required number-only form-control" placeholder="0.00" value="{{ $actual['AnnualGrossIncome'] }}">
					</div>

					<div class="form-group">
						<label for="">Monthly Expense</label>
						<input type="text" name="MonthlyExpenses"  class="required number-only form-control" placeholder="0.00" value="{{ $actual['TotalMonthlyExp'] }}">
					</div>
			 
					<div class="form-group">
						<button class="btn btn-cta" id="calculate">RUN TEST</button>
					</div> 
				</form>
			</div>
			<div class="col-sm-6">
				<h2>Result:</h2>
				<table class="table table-bordered" id="result">
					<thead>
						<tr class="success">
							<th>Key</th>
							<th>Value</th>
						</tr>
					</thead>
					<tbody>
						@foreach($result as $key => $val ) 
							<tr>
								<td><strong>{{ $key }}</strong></td>
								<td>{{ $val }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div> 
	</div>
</div>
@stop 
@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/validate.js');  }} 
	<script>
		$(document).ready(function(){

			$('#testLoanRate').submit( function(){

				var param = $(this).serialize();

				$.ajax({
					url: baseUrl + 'test_postLoanRate',
					type: 'POST',
					dataType:'JSON',
					data: param,
					success: function(data){
						
						var str = '';

	 					for(var key in data){
	 						if(data.hasOwnProperty(key)){
	 							str += '<tr>';
	 							str += '<td><b>' + key + '</b></td>'; 
	 							str += '<td>'+ data[key] +'</td>'; 
	 							str += '</tr>';
	 						}
	 					}
 
						$('#result tbody').html(str);


						console.log(data);
					}
				});

				return false;
			});

		})
	</script>
@stop