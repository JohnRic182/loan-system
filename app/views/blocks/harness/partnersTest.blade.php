@section('content')
<div class="container">

	<div class="row">
		<div class="col-md-6">
			<label>Target Endpoint URI</label><br>
			<input type="text" name="uri" id="uri" class="form-control" value="http://rwards.loc/api/v1/partner" required="required" pattern="" title="">
		</div>
	</div>	
	<div class="row">
		<div class="col-md-6">
			<label>Request JSON</label><br> 
			<textarea id="request" cols="120" rows="15">{ 
  "Application":{
    "Partner": {
      "PID": 6,
      "CID": 2,
      "Username": "Dot818",
      "APIKey": "2Gm1lK3U6tmikh97328vxxs1s4yKw157",
      "SubId": 111,
      "ReferringURL": "",
      "LeadId": 1,
      "ResponseEndPoint": "http://rwards.loc/api/v1/testEndpoint"
    },
    "Customer": {
      "LoanApp": {
        "LoanAmount": 15000,
        "ReasonForLoan": 1,
        "MonthlyIncome": 16666,
        "SelfReportedCredit": "poor639less",
        "BureauProvidedCredit": 0
      },
      "Personal": {
        "Ipaddress": "120.0.0.1",
        "SSN": "666-58-7433",
        "DOB": "10/28/1976",
        "FirstName": "HILDA",
        "LastName": "CHALGUJIAN",
        "MiddleName":"A",
        "StreetAddress": "317 DEANNE LN",
        "City": "DALY CITY",
        "State": "CA",
        "Zip": "94014", 
        "RentOrOwn": 1,
        "RentAmount":1000,
        "Email": "qatascend+081@gmail.com",
        "Phone": "154-444-4444",
        "EmploymentStatus": 2
      }
    }
  }
}</textarea>
		</div>
	</div>	

	<div class="row">
		<div class="col-md-6">
			<button id="requestBtn" class="btn btn-cta">SEND</button>
		</div>
	</div>


	<hr>
	<div class="row">
		<div class="col-md-6">
			<label for="response">Response</label><br>
			<textarea id="response" cols="120" rows="15"></textarea>
		</div>
	</div>

</div>


</body>
</html>

{{HTML::script( 'js/jquery.min.js');  }}
{{HTML::script('js/bootstrap.min.js')}}

<script type="text/javascript">
	
	jQuery(document).ready(function(){
		$("#requestBtn").click(function(){
			$('button').prop('disabled', true);
			$('#response').val('');
			// console.log( $('#request').val());
			// console.log($('#uri').val());
			// 
			var jsonString = $.parseJSON($('#request').val());
 
			$.ajax({
			  type: "POST",
			  postData: 'json',
			  url: $('#uri').val(),
			  data: jsonString,
			  success: function(response){
			  	console.log(response);
			  	// var rs  = JSON.stringify(response);
			  	$('#response').val(response);
			  	$('button').prop('disabled', false);
			  },
			  error: function(){
			  	$('button').prop('disabled', false);
			  }
			});
		});
	});

</script>
@stop