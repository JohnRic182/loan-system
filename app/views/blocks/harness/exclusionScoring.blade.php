
@section('content') 
<div class="container inner-container">
	<h1>Exclusion and Scoring Test Harness</h1>
	<input type="hidden" name="uid" class="uid" value="{{$uid}}" />
     <input type="hidden" name="type" class="type" value="{{$type}}" />
     @if ($pullFlag)
          <div id="leftpanel" style="background: white; height: 300px" >
               <br /><br /><br /><br />
                <center>No Credit Pull Data for this account. Please proceed to Credit Check page.</center>
               <br /><br />
          </div>
     @else
     	<div id="leftpanel" >
     		<br /><br />
     		<table border="1">
     		<tr><th>Variables</th><th>Values<br /></th></tr>
     		<tr><td colspan="2" align="center">Exclusions</td></tr>
     		
                     <tr><td>EMP_STAT</td><td><input type="text" readOnly="true" value="{{$Employment_Status_Desc}}" class="EMP_STAT" name="EMP_STAT" /><br /></td></tr>
                         <tr><td>AI</td><td><input type="text" readOnly="true" value="{{$Annual_Gross_Income_Amt}}" class="AI" name="AI" /><br /></td></tr>
                         <tr><td>EXST_LN</td><td><input type="text" readOnly="true" value="{{$EXST_LN}}" class="EXST_LN" name="EXST_LN" /><br /></td></tr>
                         <tr><td>LN_DECL</td><td><input type="text" readOnly="true" value="{{$LN_DECL}}" class="LN_DECL" name="LN_DECL" /><br /></td></tr>

                         <tr><td>G094</td><td><input type="text" readOnly="true" value="{{$G094}}" class="G094" name="G094" /><br /></td></tr>
                         <tr><td>G093</td><td><input type="text" readOnly="true" value="{{$G093}}" class="G093" name="G093" /><br /></td></tr>
                         <tr><td>G083</td><td><input type="text" readOnly="true" value="{{$G083}}" class="G083" name="G083" /><br /></td></tr>
                         <tr><td>G064</td><td><input type="text" readOnly="true" value="{{$G064}}" class="G064" name="G064" /><br /></td></tr>
                         <tr><td>G071</td><td><input type="text" readOnly="true" value="{{$G071}}" class="G071" name="G071" /><br /></td></tr>
                         <tr><td>G057</td><td><input type="text" readOnly="true" value="{{$G057}}" class="G057" name="G057" /><br /></td></tr>
                         <tr><td>RE33</td><td><input type="text" readOnly="true" value="{{$RE33}}" class="RE33" name="RE33" /><br /></td></tr>
                         <tr><td>MT33</td><td><input type="text" readOnly="true" value="{{$MT33}}" class="MT33" name="MT33" /><br /></td></tr>
                         <!-- <tr><td>BC06</td><td><input type="text" readOnly="true" value="{{$BC06}}" class="BC06" name="BC06" /><br /></td></tr>
                         <tr><td>G098</td><td><input type="text" readOnly="true" value="{{$G098}}" class="G098" name="G098" /><br /></td></tr> -->

                    @if ($bankFlag)     
                         <tr><td>TOTAL_NSF</td><td><input type="text" readOnly="true" value="{{$Total_NSF_Cnt}}" class="Total_NSF_Cnt" name="Total_NSF_Cnt" /><br /></td></tr>
                         <tr><td>Days_SINCE_NSF</td><td><input readOnly="true" type="text" value="{{$Days_Since_Last_NSF_Cnt}}" class="Days_Since_Last_NSF_Cnt" name="Days_Since_Last_NSF_Cnt" /><br /></td></tr>
                         <tr><td>MO_DEPOSIT_TO_AVERAGE</td><td><input readOnly="true" type="text" value="{{$Avg_Monthly_Deposit_Amt}}" class="Avg_Monthly_Deposit_Amt" name="Avg_Monthly_Deposit_Amt" /><br /></td></tr>
                         <tr><td>Monthly_Loan_Payment</td><td><input readOnly="true" type="text" value="{{$Monthly_Loan_Pay}}" class="Monthly_Loan_Pay" name="Monthly_Loan_Pay" /><br /></td></tr>
                         <tr><td>AVG_BAL_PMT_DATE</td><td><input readOnly="true" type="text" value="{{$Avg_Bal_On_Pmt_Dt_Amt}}" class="Avg_Bal_On_Pmt_Dt_Amt" name="Avg_Bal_On_Pmt_Dt_Amt" /><br /></td></tr>
                         <tr><td>CURR_BAL</td><td><input type="text" readOnly="true" value="{{$Current_Bal_Amt}}" class="Current_Bal_Amt" name="Current_Bal_Amt" /><br /></td></tr>
                         <tr><td>NUM_LOW_BAL_EVENTS</td><td><input type="text" readOnly="true" value="{{$Low_Bal_Event_Cnt}}" class="Low_Bal_Event_Cnt" name="Low_Bal_Event_Cnt" /><br /></td></tr>
                         <tr><td>NUM_LOW_BAL_DAYS</td><td><input type="text" readOnly="true" value="{{$Low_Bal_Day_Cnt}}" class="Low_Bal_Day_Cnt" name="Low_Bal_Day_Cnt" /><br /></td></tr>
                    @endif     
                         <tr><td colspan="2" align="center">Scoring</td></tr>
                         <tr><td>AT01</td><td><input type="text" readOnly="true" value="{{$AT01}}" class="AT01" name="AT01" /><br /></td></tr>
                         <tr><td>AT20</td><td><input type="text" readOnly="true" value="{{$AT20}}" class="AT20" name="AT20" /><br /></td></tr>
                         <tr><td>AT12</td><td><input type="text" readOnly="true" value="{{$AT12}}" class="AT12" name="AT12" /><br /></td></tr>


                         <tr><td>AT34</td><td><input type="text" readOnly="true" value="{{$AT34}}" class="AT34" name="AT34" /><br /></td></tr>
                         <tr><td>BC09</td><td><input type="text" readOnly="true" value="{{$BC09}}" class="BC09" name="BC09" /><br /></td></tr>
                         <tr><td>BC21</td><td><input type="text" readOnly="true" value="{{$BC21}}" class="BC21" name="BC21" /><br /></td></tr>
                         <tr><td>BI01</td><td><input type="text" readOnly="true" value="{{$BI01}}" class="BI01" name="BI01" /><br /></td></tr>
                         <tr><td>G046</td><td><input type="text" readOnly="true" value="{{$G046}}" class="G046" name="G046" /><br /></td></tr>
                         <tr><td>G049</td><td><input type="text" readOnly="true" value="{{$G049}}" class="G049" name="G049" /><br /></td></tr>
                         <tr><td>G082</td><td><input type="text" readOnly="true" value="{{$G082}}" class="G082" name="G082" /><br /></td></tr>
                         <tr><td>G102</td><td><input type="text" readOnly="true" value="{{$G102}}" class="G102" name="G102" /><br /></td></tr>
                         <tr><td>IN12</td><td><input type="text" readOnly="true" value="{{$IN12}}" class="IN12" name="IN12" /><br /></td></tr>
                         <tr><td>IN34</td><td><input type="text" readOnly="true" value="{{$IN34}}" class="IN34" name="IN34" /><br /></td></tr>
                         <tr><td>MT02</td><td><input type="text" readOnly="true" value="{{$MT02}}" class="MT02" name="MT02" /><br /></td></tr>
                         <tr><td>MT20</td><td><input type="text" readOnly="true" value="{{$MT20}}" class="MT20" name="MT20" /><br /></td></tr>
                         <tr><td>MT36</td><td><input type="text" readOnly="true" value="{{$MT36}}" class="MT36" name="MT36" /><br /></td></tr>
                         <tr><td>OF01</td><td><input type="text" readOnly="true" value="{{$OF01}}" class="OF01" name="OF01" /><br /></td></tr>
                         <tr><td>OF20</td><td><input type="text" readOnly="true" value="{{$OF20}}" class="OF20" name="OF20" /><br /></td></tr>
                         <tr><td>OF28</td><td><input type="text" readOnly="true" value="{{$OF28}}" class="OF28" name="OF28" /><br /></td></tr>
                         <tr><td>OF36</td><td><input type="text" readOnly="true" value="{{$OF36}}" class="OF36" name="OF36" /><br /></td></tr>
                         <tr><td>PB21</td><td><input type="text" readOnly="true" value="{{$PB21}}" class="PB21" name="PB21" /><br /></td></tr>
                         <tr><td>RE20</td><td><input type="text" readOnly="true" value="{{$RE20}}" class="RE20" name="RE20" /><br /></td></tr>
                         <tr><td>S019</td><td><input type="text" readOnly="true" value="{{$S019}}" class="S019" name="S019" /><br /></td></tr>
                         <tr><td>S043</td><td><input type="text" readOnly="true" value="{{$S043}}" class="S043" name="S043" /><br /></td></tr>
                         <tr><td>S114</td><td><input type="text" readOnly="true" value="{{$S114}}" class="S114" name="S114" /><br /></td></tr>
                         
                         <tr><td>AT28</td><td><input type="text" readOnly="true" value="{{$AT28}}" class="AT28" name="AT28" /><br /></td></tr>
                         <tr><td>G001</td><td><input type="text" readOnly="true" value="{{$G001}}" class="G001" name="G001" /><br /></td></tr>
                         <tr><td>G002</td><td><input type="text" readOnly="true" value="{{$G002}}" class="G002" name="G002" /><br /></td></tr>
                         <tr><td>G091</td><td><input type="text" readOnly="true" value="{{$G091}}" class="G091" name="G091" /><br /></td></tr>
                         <tr><td>G096</td><td><input type="text" readOnly="true" value="{{$G096}}" class="G096" name="G096" /><br /></td></tr>
                         <tr><td>S004</td><td><input type="text" readOnly="true" value="{{$S004}}" class="S004" name="S004" /><br /></td></tr>
                         <tr><td>S059</td><td><input type="text" readOnly="true" value="{{$S059}}" class="S059" name="S059" /><br /></td></tr>
                         <tr><td>S064</td><td><input type="text" readOnly="true" value="{{$S064}}" class="S064" name="S064" /><br /></td></tr>
     		</table>
     		<br />
     		<center><button type="button" id="run">Run</button></center>
     		<br />
     		<br />
     	</div>
     	<div id="rightpanel" class="col-sm-7">
     		<h1>Results</h1>
     	</div>
      @endif    
</div>
@stop


@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop

@section('scripts')
	{{HTML::script('js/jquery.min.js')}}
	{{HTML::script('js/moment.js')}}
	{{ HTML::script( 'js/test-harness.js');  }}
@stop