<!DOCTYPE html>
<html>
<head>
	<title>Ratereward Loan Test</title>
	{{HTML::style('css/bootstrap.min.css')}}
	<style type="text/css">

		.rrtest-container{
			background: gray;
			min-height: 800px;
		}
		h1{	background: wheat}
		.results{
			background: white;
			height: 400px;
			width: 100%;
			border-bottom: 10px solid gray;
		}
		.loanNumberSelect{
			margin-left: 7px;
		}
	</style>
</head>
<body>

<div class="container rrtest-container">	
	<h1>Ratereward Loan Test</h1>
	<div class="row">

		&nbsp;<strong>User ID:</strong><input type="text" value="" name="uid" id="uid" /> &nbsp;&nbsp;<strong>Loan Number:</strong><input type="text" value="" name="lid" id="lid" />&nbsp;&nbsp;<button type="button" class="loanNumberSelect">Run</button>
		&nbsp;&nbsp;Result: &nbsp;<span id="result"></span>
		<form class="form-inline rr_calculation">
		<table class="table">
	        <thead> 
	            <tr>
	               	<th>Triggers</th>
					<th>Old Data</t>
					<th>New Data</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td>CDB(RE33S)</td>
	                <td><input type="text" id="cdb0" name="cdb0" ></td>
	                <td><input type="text" id="cdb1" name="cdb1" ></td>
	            </tr>
	            <tr>
	                <td>SAB(CURRENT BALANCE)</td>
	                <td><input type="text" id="sab0" name="sab0" ></td>
	                <td><input type="text" id="sab1" name="sab1" ></td>
	            </tr>
	            <tr>
	                <td>TCCS(TCCS)</td>
	                <td><input type="hidden" id="tccs0" name="tccs0" ></td>
	                <td><input type="text" id="tccs1" name="tccs1" ></td>
	            </tr>
	            <tr>
	                <td>On Time Payment(AT36S )</td>
	                <td colspan="2"><input type="text" id="otp1" name="otp1" ></td>
	                
	            </tr>
	            <tr>
	                <td>Auto Title</td>
	                <td colspan="2"><label>Yes</label>&nbsp;<input type="radio" checked="checked" value="1" name="at" >&nbsp;<label>No</label>&nbsp;<input value="0" name="at" type="radio" ></td>
	            </tr>
	            <tr>
	                <td>Monthly Interest Rate:</td>
	                <td colspan="2"><input type="text" id="mir" name="mir" value="" ></td>
	            </tr>
	            <tr>
	                <td colspan="3" align="center"><input type="submit" value="Start" ></td>
	            </tr>
	        </tbody>
    	</table>
    	</form>
    	<div class="col-md-12">
			<label>Results:</label>
			<div class="results">
			</div>
		</div>
		

	</div>	

</div>


</body>
</html>

{{HTML::script( 'js/jquery.min.js');  }}
{{HTML::script('js/bootstrap.min.js')}}

<script type="text/javascript">
	
	$(document).ready(function(){
		console.log('ggg');

		$(".loanNumberSelect").click(function(){
			console.log('loanNumberSelect');
			event.preventDefault();	
			var uid = $("#uid").val();
			var lid = $("#lid").val();
			//console.log($(this).serialize());
			$.ajax({
			  type: "GET",
			  url: '/raterewardLoan/rewardsTUPull/'+lid + '/'+uid,	
			  success: function(response){
			  	console.log('loanNumberSelect response');
			  	console.log(response);
			  	$('#cdb0').val(response.cdb0);
			  	$('#cdb1').val(response.cdb1);
			  	$('#sab0').val(response.sab0);
			  	$('#sab1').val(response.sab1);
			  	$('#tccs0').val(response.tccs0);
			  	$('#tccs1').val(response.tccs1);
			  	$('#otp0').val(response.otp0);
			  	$('#otp1').val(response.otp1);
			  	$('#mir').val(response.mir);
			  	var json = response.result; $('#result').html(json[0]);
			  }
			});

		});

		$(".rr_calculation").submit(function(){
			console.log('sss');
			event.preventDefault();	
			var uid = $("#uid").val();
			var lid = $("#lid").val();
			//console.log($(this).serialize());
			$.ajax({
			  type: "POST",
			  url: '/raterewardLoan/testTUprocess/'+lid + '/'+uid,				  
			  data: $(this).serialize(),
			  dataType: 'json',
			  success: function(response){
			  	console.log('run');
			  	var h ='RDR = ((CDB + SAB + TCCS) * (OP)) + AT<br />RDR = (('+response.cdb+' + '+response.sab+' + '+response.tccs+') * ('+response.op+')) + '+response.at+'<br />RDR = '+response.rdr+'<br /><br />		RDA = RDR * Monthly Interest <br />RDA = '+response.rdr+' * '+ response.interest+'	<br/>RDA = '+response.rda;
			  	$('.results').html(h);

			  }
			});

		});


	});

</script>