@section('content') 
<div class="container inner-container">
	<h1>Reward Test Harness</h1>
	<div class="content content-wrapper">
		{{ $body_contents }}
		
	</div>
</div>


@stop


@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop

@section('scripts')
	{{HTML::script( 'js/test-harness.js');  }}
@stop