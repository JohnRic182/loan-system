@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Searching Banks...</span>
	</div>
@stop

@section('content') 

	<div class="appl-title">
	    <div class="container">
	    	<h1>
				{{-- <span class="appl-step">3</span> --}}
				Lower your APR by 1%.
			</h1>
	    </div> 
	</div>
	<div id="application" class="yodlee-form container"> 
		<div class="yodlee-bank-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-8">
					<h2>Link Bank Accounts and Get a Better Rate</h2>
					<p >We will lower your APR by 1% if you allow us to review your checking account history. We verify a steady source of income and look for responsible financial management. </p>		
					<hr> 
					<div id="notification">
						@if(isset($message))
							<div class="alert alert-danger">{{ $message }}</div>
						@endif
					</div>  
				 
					<div data-url="#" class="back-to-form hide cta-link"><span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Search / Select Bank Accounts</div>
					
					<div id="bank-wrapper-search" class="form-wrapper row">
						<div class="top-banks text-center"> 
							<p>Select from these popular banks</p>
							{{-- TOP BANKS --}}
							@foreach ($banks as $key => $bank)
								@if( $key % 4 == 0 )
									<div class="row-fluid">
									 
								@endif 
									<div data-bank-name="{{ $bank->Site_Name }}" class="banks col-sm-3 col-xs-6" id="bank-{{ $bank->Site_Id }}">{{ HTML::image(asset($bank->Image_Url_Txt), $bank->Site_Name ); }}</div>

								@if( $key % 4 == 0)
									</div>		
								@endif
							@endforeach 
						</div>
						<p class="text-center clearfix">or Search for your bank</p> 
						{{-- BANK SEARCH --}}
						{{ Form::open(array('url' => '/verifyBankAccount', 'method' => 'POST', 'id' => 'vefiryBankAccountForm','class' => 'form-inline text-center clearfix' , 'role' => 'form')) }}
				 
							<div class="form-group text-center"> 
								{{ Form::text('bankName', '',  array('id'=>'bankName', 'placeholder' => 'Bank Name', 'class' => 'form-control','autocomplete' => "off", "required" => "required" ) ) }} 
							</div>	
							<div class="form-group"> 
								<div class="text-center">
								{{ Form::submit('Search', array('class' => 'btn btn-cta btn-yodlee-search')) }}
								</div>
							</div>
						{{ Form::close() }}
			 		</div>

			 		<div id="sitesSearchResult" class="form-wrapper"> 
						<div class="sites">
						 	
						</div>
			 		</div> 
		
		
					{{-- BANK CREDENTIALS --}}
					<div id="site-login-form" class="form-wrapper">
						{{ Form::open(array('url' => '/addSiteAccount', 'method' => 'POST', 'id' => 'addSiteAccount1','class' => 'form-horizontal' , 'role' => 'form')) }}
							
							<div class="form-group">
							<div class="col-sm-2"></div>
							<div class="col-sm-10">
								<p>Link Bank Account</p>
								<h4 class="sitename"></h4>
								{{-- <img src="holder.js/150x80"> --}}
							</div>
							</div>
							<div class="fields">
								
							</div>
							<div class="form-group">
								<div class="col-sm-6">
									<input id="addSiteAccountBtn" class="pull-right btn btn-cta hide" type="submit" value="Link Bank Account"/>
								</div>
							</div>
				 		

						{{ Form::close() }}
					</div>

					{{-- MFA REQUEST --}}
					<div id="MFA-form" class="form-wrapper">
						<div class="form-group">
						<div class="col-sm-2"></div>
						<div class="col-sm-10">
							<p>Link Bank Account</p>
							<h4 class="sitename"></h4>
							{{-- <img src="holder.js/150x80"> --}}
						</div>
						</div>
						{{ Form::open(array('url' => '/putMFARequestForSite', 'method' => 'POST', 'id' => 'MFARequest','class' => 'form-horizontal' , 'role' => 'form')) }}
						{{ Form::close() }}
					</div> 
					<br>
				</div>
				<div class="col-sm-4">
					@include('includes.sidebar')
				</div>
			</div>
		</div> 
		<div class="action-section row">
			<div class="col-sm-8">
				<div class="pull-left">
					<a href="#" class="cta-link backBtn" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
				</div>
				<div class="pull-right">
					<button id="linkBankAccount" disabled="disabled" class="btn btn-cta-inactive">Link Bank Account</button>
					<input id="putMFARequestForSiteBtn" class="pull-right btn btn-cta hide" type="button" value="Link Bank Account"/>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	{{--Transfer this to another block--}}
@stop

@section('scripts')
{{ HTML::script( 'js/common.js');  }}
{{ HTML::script( 'js/yodlee.js');  }}
@stop