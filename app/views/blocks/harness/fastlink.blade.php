@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Searching Banks...</span>
	</div>
@stop

@section('content') 

	<div class="appl-title">
	    <div class="container">
	    	<h1>
				<span class="appl-step">3</span>
				Lower your APR by 1%.
			</h1>
	    </div> 
	</div>
	<div id="application" class="yodlee-form container"> 
		<div class="yodlee-bank-wrapper box-wrapper">
			<div class="row">
				<div class="col-sm-12" style="background-color: #FFF;">
					<h2>
						<span class="icon icon-link"></span>
						Link Bank Accounts and Get a Better Rate
					</h2>
					<p >We will lower your APR by 1% if you allow us to review your checking account history. We verify a steady source of income and look for responsible financial management. </p>		
					<hr> 
					<div id="notification">
						@if(isset($message) )
							<div class="alert alert-danger">{{ $message }}</div>
						@endif
						@if(isset($error) )
							<div class="alert alert-danger">{{ $error }}</div>
						@endif
					</div>  
				 
					<div data-url="#" class="back-to-form hide cta-link"><span class="glyphicon glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Search / Select Bank Accounts</div>
					
					<div id="bank-wrapper-search" class="form-wrapper row">
						
						@if( !isset($error) )

							<script type="text/javascript">
							window.onload = function(){

								var rsession = "{{ $fastLinkUrl['rsession'] }}"; 
								var token = "{{ $fastLinkUrl['token']  }}"; 
								var node_url = "{{ $fastLinkUrl['node_url'] }}"; 
								var finapp_id = "{{ $fastLinkUrl['finapp_id']}}"; 
								var extra_params =  "{{ $fastLinkUrl['extra_params'] }}";

						 		var iframe = document.createElement('iframe');
								iframe.frameBorder=0;
								iframe.width="100%";
								iframe.height="800px";
								iframe.id="randomid";
								// iframe.setAttribute("src", link);
								document.getElementById("adb").appendChild(iframe);

								var doc;

								if (iframe.contentDocument) // FF Chrome
								  doc = iframe.contentDocument;
								else if ( iframe.contentWindow ) // IE
								  doc = iframe.contentWindow.document;

								doc.open();
							    doc.write(
							    '<div style="display: none;">'
							    + '<form action="' + node_url + '" method="POST" id="rsessionPost" >'
							    + '<input type="text" name="rsession" placeholder="rsession" value="' + rsession +'"/>'
							    + '<input type="text" name="app" placeholder="FinappId" value="' + finapp_id + '"/>'
							    + '<input type="text" name="redirectReq" placeholder="true/false" value="true"/>'
							    + '<input type="text" name="token" placeholder="token" value="' + token + '"/>'
							    + '<input type="text" name="extraParams" placeholer="Extra Params" value="' + extra_params + '"/>'
							    + '<input type="submit"/>'
							    + '</form>'
							    + '</div>');
							    doc.close();
							    doc.getElementById('rsessionPost').submit();
							}
						</script>
						<div id="adb"></div>

{{-- 
							<iframe src="{{ $fastLinkUrl }}" frameborder="0" width="100%" height="100%" style="min-height:550px;"></iframe>  --}}
						@else						
							<button id="dontLinkBankAcct" class="btn btn-cta text-center pull-right">Proceed to Exclusion and Scoring</button>
						@endif 

			 		</div> 
			 		<p class="text-center"><b>Click close to continue to the next step</b></p>
					<br>
				</div> 
			</div>
		</div> 
		<div class="action-section row">
			<div class="col-sm-8">
				<div class="pull-left">
					<a href="{{ url('linkBankAccount') }}" class="cta-link" > <span class="glyphicon glyphicon-chevron-left"></span> BACK</a>
				</div>
				<div class="pull-right">
					
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	{{--Transfer this to another block--}}
@stop

@section('scripts')
{{ HTML::script( 'js/common.js');  }}
{{ HTML::script( 'js/yodlee.js');  }}
@stop