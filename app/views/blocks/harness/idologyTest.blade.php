@section('content') 
<div class="container inner-container">
    <h1>Idology Test Harness</h1>
<div class="content content-wrapper">
	<div id="notification">		
	</div>
	Results: <div id="results">
	</div>
  <br />
  <div id="errCodes">
  </div>

  <form>
      
    <table class="table">
          <tr><th>Variables</th><th>Values<br /></th></tr>
          <tr><td>FirstName</td><td><input type="text" value="Christopher" class="firstName" name="firstName" /><br /></td></tr>
          <tr><td>LastName</td><td><input type="text" value="Butler" class="lastName" name="lastName" /><br /></td></tr>
          <tr><td>Address</td><td><input type="text" value="2301 Polk St., Apt. 7" class="address" name="address" /><br /></td></tr>
          <tr><td>City</td><td><input type="text" value="San Francisco" class="city" name="city" /><br /></td></tr>
          <tr><td>State</td><td><input type="text" value="CA" class="state" name="state" /><br /></td></tr>
          <tr><td>Zip</td><td><input type="text" value="94109" class="zip" name="zip" /><br /></td></tr>
          <tr><td>SSN</td><td><input type="text" value="306923795" class="ssn" name="ssn" /><br /></td></tr>
    </table>
    <button type="submit" class="btn btn-primary" id="idologyTest">Save changes</button>

  </form>
</div>


<div class="modal fade" id="modalQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="idologySubmitAnswers">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel2">Fraud Questions</h4>
      </div>
      <div class="modal-body">
           <h3>Fraud Questions</h3>
           <div class="questions">
           </div>
        
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" >Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>

</div>
@stop


@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop

@section('scripts')
	{{HTML::script( 'js/test-harness.js');  }}
@stop