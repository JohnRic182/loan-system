@section('content')
<div class="container inner-container">
<div class="content content-wrapper">
	<h1>Loan Rate Test Decisioning</h1>

	<div id="notification">
		
	</div>
	<form action="" id="testLoanRate" class="validate">
		<div class="form-group">
			<label for="">ARS Score</label>
			<input type="text" name="ARSScore" value="100" class="required number-only form-control" placeholder="Please enter a number between 100 to 500">
		</div>
		<div class="form-group">
			<label for="">Loan Type</label>
			<select name="loanType" id="" class="form-control">
				<option value="1">Ascend Loan</option>
				<option value="2">Rate Rewards Loan</option>
			</select>
		</div>
		<div class="form-group">
			<label for="">Annual Gross Income</label>
			<input type="text" name="AnnualGrossIncome"  class="required number-only form-control" placeholder="0.00" value="50000">
		</div>

		<div class="form-group">
			<label for="">Monthly Expense</label>
			<input type="text" name="MonthlyExpenses"  class="required number-only form-control" placeholder="0.00" value="1500">
		</div>

		<div class="form-group">
			<label for="">No of Months to Pay</label>
			<input type="text" name="terms" class="required number-only form-control" placeholder="36" value="36">
		</div>

		<div class="form-group">
			<button class="btn btn-cta" id="calculate">Calculate</button>
		</div>

		<div id="result">
			 <h1>Result</h1>
			 <table width="100%" class="table table-bordered">
				<thead>
					<tr>
						<td>Name</td>
						<td>Value</td>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			 </table>
		</div>

		<div class="formulas">
		 	
		</div>
	</form>
</div>
</div>
@stop
@section('scripts')
	{{ HTML::script( 'js/common.js');  }}
	{{ HTML::script( 'js/validate.js');  }} 
	<script>
		$(document).ready(function(){

			$('#testLoanRate').submit( function(){

				var param = $(this).serialize();

				$.ajax({
					url: baseUrl + 'test_postLoanRate',
					type: 'POST',
					dataType:'JSON',
					data: param,
					success: function(data){
						
						var str = '';

	 					for(var key in data){
	 						if(data.hasOwnProperty(key)){
	 							str += '<tr>';
	 							str += '<td><b>' + key + '</b></td>'; 
	 							str += '<td>'+ data[key] +'</td>'; 
	 							str += '</tr>';
	 						}
	 					}
 
						$('#result tbody').html(str);


						console.log(data);
					}
				});

				return false;
			});

		})
	</script>
@stop