
@section('content') 
	<h1>&nbsp;&nbsp;Exclusion and Scoring Override</h1>
	<input type="hidden" name="uid" class="uid" value="{{$uid}}" />
     <input type="hidden" name="type" class="type" value="{{$type}}" />

     	<div id="leftpanel" >
     		<br /><br />
     		<table border="1">
     		<tr><th>Variables</th><th>Values<br /></th></tr>
     		<tr><td colspan="2" align="center">Exclusions</td></tr>
     		
                     <tr><td>EMP_STAT</td><td><input type="text" value="Employed" class="EMP_STAT" name="EMP_STAT" /><br /></td></tr>
                         <tr><td>AI</td><td><input type="text" value="63500" class="AI" name="AI" /><br /></td></tr>
                         <tr><td>EXST_LN</td><td><input type="text" value="0" class="EXST_LN" name="EXST_LN" /><br /></td></tr>
                         <tr><td>LN_DECL</td><td><input type="text" value="0" class="LN_DECL" name="LN_DECL" /><br /></td></tr>

                         <tr><td>G094</td><td><input type="text" value="0" class="G094" name="G094" /><br /></td></tr>
                         <tr><td>G093</td><td><input type="text" value="0" class="G093" name="G093" /><br /></td></tr>
                         <tr><td>G083</td><td><input type="text" value="0" class="G083" name="G083" /><br /></td></tr>
                         <tr><td>G064</td><td><input type="text" value="0" class="G064" name="G064" /><br /></td></tr>
                         <tr><td>G071</td><td><input type="text" value="0" class="G071" name="G071" /><br /></td></tr>
                         <tr><td>G057</td><td><input type="text" value="0" class="G057" name="G057" /><br /></td></tr>
                         <tr><td>RE33</td><td><input type="text" value="3754" class="RE33" name="RE33" /><br /></td></tr>
                         <tr><td>MT33</td><td><input type="text" value="0" class="MT33" name="MT33" /><br /></td></tr>
                         <tr><td>BC06</td><td><input type="text" value="0" class="BC06" name="BC06" /><br /></td></tr>
                         <tr><td>G098</td><td><input type="text" value="0" class="G098" name="G098" /><br /></td></tr>

                    @if ($bankFlag)     
                         <tr><td>TOTAL_NSF</td><td><input type="text" value="" class="Total_NSF_Cnt" name="Total_NSF_Cnt" /><br /></td></tr>
                         <tr><td>Days_SINCE_NSF</td><td><input type="text" value="" class="Days_Since_Last_NSF_Cnt" name="Days_Since_Last_NSF_Cnt" /><br /></td></tr>
                         <tr><td>MO_DEPOSIT_TO_AVERAGE</td><td><input type="text" value="" class="Avg_Monthly_Deposit_Amt" name="Avg_Monthly_Deposit_Amt" /><br /></td></tr>
                         <tr><td>Monthly_Loan_Payment</td><td><input type="text" value="" class="Monthly_Loan_Pay" name="Monthly_Loan_Pay" /><br /></td></tr>
                         <tr><td>AVG_BAL_PMT_DATE</td><td><input type="text" value="" class="Avg_Bal_On_Pmt_Dt_Amt" name="Avg_Bal_On_Pmt_Dt_Amt" /><br /></td></tr>
                         <tr><td>CURR_BAL</td><td><input type="text" value="" class="Current_Bal_Amt" name="Current_Bal_Amt" /><br /></td></tr>
                         <tr><td>NUM_LOW_BAL_EVENTS</td><td><input type="text" value="" class="Low_Bal_Event_Cnt" name="Low_Bal_Event_Cnt" /><br /></td></tr>
                         <tr><td>NUM_LOW_BAL_DAYS</td><td><input type="text" value="" class="Low_Bal_Day_Cnt" name="Low_Bal_Day_Cnt" /><br /></td></tr>
                    @endif     
                         <tr><td colspan="2" align="center">Scoring</td></tr>
                         <tr><td>AT01</td><td><input type="text" value="1" class="AT01" name="AT01" /><br /></td></tr>
                         <tr><td>AT28</td><td><input type="text" value="1" class="AT28" name="AT28" /><br /></td></tr>
                         <tr><td>G001</td><td><input type="text" value="1" class="G001" name="G001" /><br /></td></tr>
                         <tr><td>G002</td><td><input type="text" value="1" class="G002" name="G002" /><br /></td></tr>
                         <tr><td>G091</td><td><input type="text" value="1" class="G091" name="G091" /><br /></td></tr>
                         <tr><td>G096</td><td><input type="text" value="1" class="G096" name="G096" /><br /></td></tr>
                         <tr><td>S004</td><td><input type="text" value="1" class="S004" name="S004" /><br /></td></tr>
                         <tr><td>S059</td><td><input type="text" value="1" class="S059" name="S059" /><br /></td></tr>
                         <tr><td>S064</td><td><input type="text" value="1" class="S064" name="S064" /><br /></td></tr>
     		</table>
     		<br />
     		<center><button type="button" id="run">Run</button></center>
     		<br />
     		<br />
     	</div>
     	<div id="rightpanel" class="col-sm-7">
     		<h1>Results</h1>
     	</div>
@stop


@section('loader')
	<div id="loader">
		<div class="loader">Loading...</div>
		<span style="color:#FFF">Credit Checking...</span>
	</div>
@stop

@section('scripts')
	{{HTML::script('js/jquery.min.js')}}
	{{HTML::script('js/moment.js')}}
	{{ HTML::script( 'js/test-harness.js');  }}
@stop