@section('content')
<div class="container">
	<style>
		label{
			font-weight: bold;
		}
	</style>
	<div class="row">
		<div class="col-md-6">
			<label>Target API Method URL</label><br>
			<input type="text" name="uri" id="uri" class="form-control" value="http://rwards.loc/salesforce/api/v1/loan/approve" required="required" pattern="" title="">
		</div>
	</div>	
	<div class="row">
		<div class="col-md-6">
			<label>JSON Request</label><br> 
			<textarea id="request" cols="120" rows="15">{
 "User_Id":14611, 
 "Loan_Id":12941,
 "Loan_Status_Desc":"LOAN_APPROVED"
}</textarea>
		</div>
	</div>	

	<div class="row">
		<div class="col-md-6">
			<button id="requestBtn" class="btn btn-cta">SEND</button>
		</div>
	</div>


	<hr>
	<div class="row">
		<div class="col-md-6">
			<label for="response">Response</label><br> 
			<textarea id="response" cols="120" rows="15"></textarea>
		</div>
	</div>

</div>


</body>
</html>

{{HTML::script( 'js/jquery.min.js');  }}
{{HTML::script('js/bootstrap.min.js')}}

<script type="text/javascript">
	
	jQuery(document).ready(function(){
		$("#requestBtn").click(function(){

			$('button').prop('disabled', true);
			$('#response').val('');

			var jsonString = $.parseJSON($('#request').val());
 
			$.ajax({
			  type: "POST",
			  postData: 'json',
			  url: $('#uri').val(),
			  data: jsonString,
			  success: function(response){
			  	console.log(response);
			  	var rs  = JSON.stringify(response, null, 2); 
			  	$('#response').val(rs);
			  	$('button').prop('disabled', false);
			  },
			  error: function(){
			  	$('button').prop('disabled', false);
			  }
			});
		});
	});

</script>
@stop