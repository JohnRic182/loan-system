<!DOCTYPE html>
<html>
<head>
	<title>Lending Tree Test</title>
	{{HTML::style('css/bootstrap.min.css')}}
	<style type="text/css">

		textarea{
			width: 100%;
		}

	</style>
</head>
<body>

<div class="container">

	<div class="row">
		<div class="col-md-12">
			<label>Target Endpoint URI</label><br>
			<input type="text" name="uri" id="uri" class="form-control" value="" required="required" pattern="" title="">
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">
			<label>Request XML</label><br>
			<textarea id="request" cols="120" rows="20"></textarea>
		</div>
	</div>	

	<div class="row">
		<div class="col-md-12">
			<button id="requestBtn">Send XML Request</button>
		</div>
	</div>


	<hr>
	<div class="row">
		<div class="col-md-12">
			<label for="response">Reponse XML</label><br>
			<textarea id="response" cols="120" rows="20"></textarea>
		</div>
	</div>

</div>


</body>
</html>

{{HTML::script( 'js/jquery.min.js');  }}
{{HTML::script('js/bootstrap.min.js')}}

<script type="text/javascript">
	
	jQuery(document).ready(function(){

		$("#requestBtn").click(function(){

			$.ajax({
			  type: "POST",
			  url: '/api/v1/ltHarness',
			  data: { req : $('#request').val(), uri : $('#uri').val() },
			  success: function(response){
			  	$('#response').val(response);
			  }
			});

		});


	});

</script>