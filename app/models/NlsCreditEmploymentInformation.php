<?php

class NlsCreditEmploymentInformation extends Eloquent{

	protected $table = 'NLS_CreditEmploymentInformation';

	protected $primaryKey = 'CreditEmploymentInformationID';

	public $timestamps = false;

}