<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class DelinquencyPeriod extends Eloquent{
	
	protected $table      = 'Delinquency_Period';
	
	protected $primaryKey = 'Delinquency_Period_Id';
	
	public $timestamps    = false;
}