<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 */
class ApplicantEmploymentInfo extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| Applicant Employment Info Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	protected $table = 'Applicant_Employment_Info';

	protected $primaryKey = 'User_Id';

	public $timestamps  = false;

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
  

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Insert New Applicant Employment Info
	 * 
	 * @param  string $userId
	 * @param  array  $data
	 * @return
	 */
	public static function newEmploymentInfo( $userId, $data = array() )
	{

		$AppEmpInfo = new ApplicantEmploymentInfo();
		//prepare data to be stored
		$AppEmpInfo->User_Id                  = $userId;
		$AppEmpInfo->Employer_Name            = $data['EmployerName'];
		$AppEmpInfo->Work_Phone_Nr            = $data['WorkPhoneNr'];
		$AppEmpInfo->Street_Addr_1_Txt        = $data['StreetAddress1'];
		$AppEmpInfo->Street_Addr_2_Txt		  = (!empty($data['StreetAddress2'])) ? $data['StreetAddress2'] : '';
		$AppEmpInfo->City_Name		 		  = $data['City'];
		$AppEmpInfo->State_Cd		 		  = $data['State'];
		$AppEmpInfo->Zip_Cd		 			  = $data['Zip'];
		$AppEmpInfo->Contact_Person_Name      = $data['ContactPersonName'];
		$AppEmpInfo->Supervisor_Phone_Nr      = $data['SupervisorPhoneNr'];
		$AppEmpInfo->Job_Title_Name           = (!empty($data['JobTitleName'])) ? $data['JobTitleName'] : '';
		$AppEmpInfo->Phone_Nr  				  = $data['PhoneNr'];
		$AppEmpInfo->Months_of_Employment_Cnt = $data['Tenureship'];
		$AppEmpInfo->Pay_Type_Desc            = $data['IncomeType'];
		$AppEmpInfo->Pay_Frequency_Desc       = $data['PayFrequency']; 
		$AppEmpInfo->Employment_Start_Dt      = date('Y-m-d'); 
		$AppEmpInfo->Employment_End_Dt        = date('Y-m-d'); 
		$AppEmpInfo->Created_By_User_Id       = $userId; 
		$AppEmpInfo->Create_Dt                = date('Y-m-d');  
		$AppEmpInfo->save(); 
		
	}
}