<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class ConsentVersion extends Eloquent{

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'Consent_Version';

	/**
	 * Primary key
	 * 
	 * @var string
	 */
	protected $primaryKey = 'Consent_Version_Nr';

 	/**
 	 * Get Consent Data By Page ID
 	 * 
 	 * @param  integer $consentTypeId
 	 * @return array
 	 */
	public function getDataByConsentId( $consentTypeId = 1 )
	{

		return DB::table($this->table)
            ->join('Consent_Version_Detail', 'Consent_Version.Consent_Version_Nr', '=', 'Consent_Version_Detail.Consent_Version_Nr') 
            ->select('Consent_Version.*', 'Consent_Version_Detail.*')
            ->where('Consent_Version_Detail.Consent_Type_Id', $consentTypeId )
            ->where('Consent_Version.Validity_End_Dt', '1900-01-01' )
            ->first(); 
	}

	/**
 	 * Get Consent Data
 	 * 
 	 * @param  integer $consentTypeId
 	 * @return array
 	 */
	public function getData()
	{

		return DB::table($this->table)
            ->join('Consent_Version_Detail', 'Consent_Version.Consent_Version_Nr', '=', 'Consent_Version_Detail.Consent_Version_Nr') 
            ->select('Consent_Version.*', 'Consent_Version_Detail.*')
            ->where('Consent_Version.Validity_End_Dt', '1900-01-01' )
            ->get(); 
	}
}