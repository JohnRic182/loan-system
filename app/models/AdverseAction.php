<?php

class AdverseAction extends Eloquent{
	/*
	|--------------------------------------------------------------------------
	| AffordableLoanFactor Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'Applicant_Adverse_Action_Codes';

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';

	/**
	 * Timestamps
	 * @var boolean
	 */
    public $timestamps = false;

}