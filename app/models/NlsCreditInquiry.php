<?php

class NlsCreditInquiry extends Eloquent{

	protected $table = 'NLS_CreditInquiry';

	protected $primaryKey = 'CreditInquiryID';

	public $timestamps = false;

}