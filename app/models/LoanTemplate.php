<?php

class LoanTemplate extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'Loan_Template';
	
	/**
	 * Primary Field
	 * @var null
	 */
	protected $primaryKey = 'Loan_Template_Id';
		
	/**
	 * Save Timestamps for Create and Update
	 * @var boolean
	 */
	public $timestamps    = false;	
	

	public static function getLoanTemplateName( $loanTemplateId )
	{
		$loanTemplate = LoanTemplate::where('Loan_Template_Id', '=', $loanTemplateId)->first();	
		if( $loanTemplate )
			return $loanTemplate->Loan_Template_Name;
	}
  
}