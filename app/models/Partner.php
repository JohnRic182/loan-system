<?php

/**
 * Developer's Note
 *   - 
 *   - 
 */
class Partner extends Eloquent
{
	/**
	 * Table Name
	 * @var string
	 */
	protected $table  = 'Partner';
	
	/**
	 * Primary key
	 * @var string
	 */
	protected $primaryKey = 'Partner_Id';

	const CREATED_AT = 'Create_Dt';
	const UPDATED_AT = 'Update_Dt';


	/**
	 * Validate Partner 
	 * 
	 * @param  string  $partnerName
	 * @param  string  $key
	 * @return boolean
	 */
	public function isPartnerExist( $partnerName, $key, $pid)
	{
		if( !empty($partnerName) && !empty($key)){
			return $this->where(array(
					'Partner_Name'   => $partnerName,
					'Access_Key_Val' => $key, 
					'Partner_Id'     => $pid
					))->count();
		}
	}

	/**
	 * Get Data 
	 *
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{  
		if( $isRow == true )
			return Partner::where($where)->select($fields)->first();

		return Partner::where($where)->select($fields)->get();
	}	

}