<?php

/**
 * Developer's Note
 *   - 
 *   - 
 */
class PartnerType extends Eloquent
{
	/**
	 * Table Name
	 * @var string
	 */
	protected $table  = 'Partner_Type';
	
	/**
	 * Primary key
	 * @var string
	 */
	protected $primaryKey = 'Partner_Type_Id';

	const CREATED_AT = 'Create_Dt';
	const UPDATED_AT = 'Update_Dt';
 
}