<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class LoanProdRateMap extends Eloquent{


	/*
	|--------------------------------------------------------------------------
	| LoanProdRateMap Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name 
	 * 
	 * @var string
	 */
	protected $table      = 'Loan_Prod_Rate_Map';

	/**
	 * Primary key field 
	 * 
	 * @var null
	 */
	protected $primaryKey = null;
	
	/**
	 * Time Stamps
	 * @var boolean
	 */
	public $timestamps    = false;	

	/**
	 * Get Loan Amt Range
	 *
	 * 
	 * @param  string $stateCd
	 * @return
	 */
	public static function getLoanAmtRange( $stateCd = 'CA') 
	{
		return LoanProdRateMap::where(
			array(
				'State_Cd' => $stateCd
			)
		)
		->select(
			'State_Cd', 
			'Absolute_Max_Loan_Amt', 
			'Absolute_Min_Loan_Amt', 
			'Loan_Prod_Id'
		)
		->orderBy('Absolute_Max_Loan_Amt', 'DESC')
		->first();
	}

	/**
	 * Get Loan Product Rate Mapping 
	 * 
	 * @param  integer $loanProductId
	 * @param  float   $scoreRange
	 * @param  string  $isLinkBankAcct
	 * @param  string  $stateCd
	 * @return
	 */
	public static function getLoanProdRateMap( $loanProductId, $scoreRange, $isLinkBankAcct, $stateCd )
	{ 
		return LoanProdRateMap::where('Loan_Prod_Id','=', $loanProductId )
			   ->where('High_Risk_Score_Val', '>=', $scoreRange )
			   ->where('Low_Risk_Score_Val', '<=', $scoreRange )
			   ->where('Bank_Acct_Link_Reqd_Flag', '=', $isLinkBankAcct )
			   ->where('State_Cd', '=', $stateCd )
			   ->select(
				 	'Loan_Prod_Id', 
				 	'Low_Risk_Score_Val', 
				 	'High_Risk_Score_Val',
				 	'APR_Val', 
				 	'Absolute_Max_Loan_Amt', 
				 	'Absolute_Min_Loan_Amt',
				 	'Bank_Acct_Link_Reqd_Flag'
			  ) 
			  ->first(); 
	}
}