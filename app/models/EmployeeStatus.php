<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class EmployeeStatus extends Eloquent{

	protected $table      = 'Employment_Status'; 
	
	protected $primaryKey = 'Employment_Status_Id';
	
	public $timestamps    = false;
}