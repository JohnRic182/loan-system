<?php

class CreditProfile extends Eloquent{

	protected $table = 'CreditProfile';

	protected $connection = 'nls';
	
	protected $primaryKey = 'CreditProfileID';

    public $timestamps = false;
    /*

	public function NlsCreditTradeLine() 
    {
        return $this->hasMany('NlsCreditTradeLine');
    }

    public function NlsCreditInquiry() 
    {
        return $this->hasMany('NlsCreditInquiry');
    }
    */

}