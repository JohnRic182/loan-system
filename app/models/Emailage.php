<?php

class Emailage
{

	protected $_sid;
	protected $_authToken;
	protected $_emailToLookup;
	protected $_oauthVersion;
	protected $_requestFormat;
	protected $_signatureMethod;
	protected $_urlParametersString = '';
	protected $_urlParameters = array();
	protected $_oauthData = '';
	protected $_signature;
	protected $_method;
	protected $_url;
	protected $_requestUrl = '';
	protected $_response;
	protected $_responseData;

	function __construct($email)
	{
		$this->_requestFormat 		= 'json';
		$this->_oauthVersion 		= '1.0';
		$this->_emailToLookup 		= $email;
		$this->_url 				= Config::get('system.EmailAge.URL');		
		$this->_sid 				= Config::get('system.EmailAge.SID');		
		$this->_authToken 			= Config::get('system.EmailAge.authToken');
		$this->_signatureMethod 	= 'HMAC-SHA1';
		$this->_method 				= 'POST';
		// $this->_url = 'https://sandbox.emailage.com/EmailAgeValidator/';
		// $this->_sid = 'E389F2D899D3467D8D5EB45A0250D5A5';
		// $this->_authToken = '3454D151E36B4861B54BE8164AACC4FE';
	}

	function performRequest()
	{
		$this->_buildRequest();
		$this->_sendRequest();
		$this->_parseResponse();
		//response data are now stored as associative array at : $this->_responseData
		//echo($this->_responseData);
	}

	function getResponseData()
	{
		return $this->_responseData;
	}

	protected function _buildRequest()
	{
		$this->_buildOauthData();
		$this->_buildSignature();
		$this->_requestUrl = $this->_url . '?' . $this->_urlParametersString . '&oauth_signature=' . urlencode($this->_signature);
		
	}

	protected function _sendRequest()
	{
	
	$url = $this->_requestUrl;	
	$data = array($this->_emailToLookup);
	// Build Http query using params
	$query = (  http_build_query ($data));
	
	$paramsJoined = array();

	foreach($data as $data => $value) {
	   $paramsJoined[] = "$value";
	}

	$query = implode('&', $paramsJoined);
	// use key 'http' even if you send the request to https://...
	$options = array(
		'http' => array(
			'header' => "Content-type: application/x-www-form-urlencoded\r\n". "User-Agent: Mozilla 4.0\r\n".
                            "Content-Length: ".strlen($query)."\r\n",
                            "Connection: close\r\n\r\n",
			'method'  => 'POST',
			'content' => $query,
		),
	);
	$context  = stream_context_create($options);
	//print_r($context); 
	$result = file_get_contents($url, false, $context);
		
	$result = substr($result, 3); // remove garbage string

	$result = json_decode($result, true);

	$this->_responseData = $result;
	//return $result;
	//return $result;
	//var_dump($result);
	//pre($a); die;


	//print_r($result); 
	//var_dump($result); 

	}

	protected function _parseResponse()
	{
		if ($this->_response) {
			$partialDecode = urldecode($this->_response);
			$partialDecode = preg_replace('/[^(\x20-\x7F)]*/', '', $partialDecode); //removing utf-8 special characters that mess up json_decode when present
			$this->_responseData = json_decode($partialDecode, true);
		}
	}

	private function _buildOauthData()
	{
		$this->_buildUrlParametersString();
		$this->_oauthData = urlencode($this->_method);
		$this->_oauthData .= '&' . urlencode($this->_url);
		$this->_oauthData .= '&' . urlencode($this->_urlParametersString);
	}

	private function _buildSignature()
	{
		$hash = hash_hmac('sha1', $this->_oauthData, $this->_authToken . '&', true);
		$base64Hash = base64_encode($hash);

		$this->_signature = $base64Hash;
	}

	private function _buildUrlParametersString()
	{
		$string = '';

		$this->_urlParameters['format'] = $this->_requestFormat;
		$this->_urlParameters['oauth_version'] = $this->_oauthVersion;
		$this->_urlParameters['oauth_consumer_key'] = $this->_sid;
		$this->_urlParameters['oauth_timestamp'] = time();
		$this->_urlParameters['oauth_nonce'] = $this->_generateRandomString();
		$this->_urlParameters['oauth_signature_method'] = $this->_signatureMethod;

		foreach ($this->_urlParameters as $key => $value) {
			$string .= urlencode($key) . '=' . urlencode($value) . '&';
		}

		if (strrpos($string, '&') == (strlen($string) - 1)) {
			$string = rtrim($string, '&');
		}

		$this->_urlParametersString = $string;
	}

	private function _generateRandomString($length = 10)
	{
		return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
	}

}

?>



