<?php

/**
 * Developer's Note
 *
 *  - update todo list
 */

class NLSTaskStatusCode extends Eloquent{
	
  	/**
  	 * NLS Task Table
  	 */ 
	protected $connection = 'nls'; 
	
	protected $primaryKey = 'status_code_id';
	
	protected $table      = 'task_status_codes';


}