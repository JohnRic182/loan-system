<?php

class LoanApplicationDetail extends Eloquent{

    /**
     * Table Name
     * @var string
     */
	protected $table      = 'Loan_Application_Detail';
	   
    /**
     * Primary Key
     * @var string
     */
	protected $primaryKey = 'Loan_App_Nr';
	
    /**
     * Timestamps
     * @var boolean
     */
	public $timestamps    = false;
 
    /**
     * Get Loan Application Detail Data
     * 
     * @param  array   $fields
     * @param  array   $where
     * @param  boolean $isRow
     * @return [type]
     */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{ 
		if( $isRow == true )
			return LoanApplicationDetail::where($where)->select($fields)->first();

		return LoanApplicationDetail::where($where)->select($fields)->get();
	}

    /**
     * Update Previous Loan App and Sub App Phase End Date
     * 
     * @param  integer $userId
     * @param  integer $loanAppNr
     * @param  integer $prevLoanAppNr
     */
    public function updatePrevLoanAppPhaseEndDt( $userId, $loanAppNr, $prevLoanAppNr )
    {
        LoanApplicationDetail::where(
            array(
                'Applicant_User_Id' => $userId, 
                'Loan_App_Nr'       => $loanAppNr, 
                'Loan_App_Phase_Nr' => $prevLoanAppNr
            )
        )->update(
            array(
                'Phase_End_Dttm'        => date('Y-m-d'),
                'Sub_Phase_End_Dttm'    => date('Y-m-d')
            )
        );

    }

	/**
     * Set Loan Application Phase 
     *
     * @param integer $userId
     * @param integer $loanAppNr
     */
    public static function getLoanAppPhase($userId, $loanAppNr)
    {
        
        $data = array(); 

        if( !empty($userId) && !empty($loanAppNr ) ) {
 
            //check if the loan application is active
            $loanApp    = LoanApplication::isLoanApplicationActive($userId, $loanAppNr);
            $fields     = array('Loan_App_Phase_Nr', 'Phase_Status_Desc');
            $whereParam = array('Applicant_User_Id' => $userId, 'Loan_App_Nr' => $loanAppNr);
            $data 		= LoanApplicationDetail::getData($fields, $whereParam, true );
            
        }

        return $data;
    }


    /**
     * Is Loan App Exist
     *
     * @param integer $userId
     * @param integer $loanAppNr
     * @param integer $loanAppPhaseNr
     */
    public static function isLoanAppPhaseExist( $userId, $loanAppNr , $loanAppPhaseNr )
    {
        return LoanApplicationDetail::where(
                array(
                    'Applicant_User_Id' => $userId, 
                    'Loan_App_Nr'       => $loanAppNr, 
                    'Loan_App_Phase_Nr' => $loanAppPhaseNr
                )
            )->get()->count();
    }

  
    /**
     * Set Loan Application Phase
     *
     * @param integer $userId
     * @param integer $loanAppNr
     * @param array   $loanAppPhase
     */
    public function setLoanAppPhase( $userId, $loanAppNr, $param = array() )
    {
        $data = array(); 

        if( !empty($userId) && !empty($loanAppNr ) ) {

            $whereParam  =  array(
                                'Applicant_User_Id' => $userId, 
                                'Loan_App_Nr'       => $loanAppNr, 
                                'Loan_App_Phase_Nr' => $param['loanAppPhaseNr']
                            );

            //Remove Loan Sub Phase Number In Condition 
            //if Sub Phase Number is equal to zero
            if( $param['subPhaseNr'] != 0 )
                $whereParam['Loan_Sub_Phase_Nr'] = $param['subPhaseNr'];

            //check if the Loan Application Phase Exist
            $loanAppCnt = LoanApplicationDetail::where( $whereParam ) 
                            ->get()
                            ->count();
            
            if( $loanAppCnt > 0 ) {

               return LoanApplicationDetail::where(
                    array(
                        'Applicant_User_Id' => $userId, 
                        'Loan_App_Nr'       => $loanAppNr, 
                        'Loan_App_Phase_Nr' => $param['loanAppPhaseNr']
                    )
                )->update(
                    array(
                        'Loan_App_Phase_Nr'     => $param['loanAppPhaseNr'],
                        'Phase_Status_Desc'     => $param['loanAppPhaseDesc'],
                        'Phase_Start_Dttm'      => date('Y-m-d H:m:s'),
                        'Phase_End_Dttm'        => date('Y-m-d H:m:s'),
                        'Loan_Sub_Phase_Nr'     => $param['subPhaseNr'],
                        'Sub_Phase_Status_Desc' => $param['subPhaseStatusDesc'],
                        'Sub_Phase_Start_Dttm'  => date('Y-m-d H:m:s'),
                        'Sub_Phase_End_Dttm'    => date('Y-m-d H:m:s'),
                    )
                );

            } else {

                $loanAppPhase = new LoanApplicationDetail();
                $loanAppPhase->Applicant_User_Id     = $userId;
                $loanAppPhase->Loan_App_Nr           = $loanAppNr;
                $loanAppPhase->Loan_App_Phase_Nr     = $param['loanAppPhaseNr'];
                $loanAppPhase->Phase_Status_Desc     = $param['loanAppPhaseDesc'];
                $loanAppPhase->Phase_Start_Dttm      = date('Y-m-d H:m:s');
                $loanAppPhase->Phase_End_Dttm        = date('Y-m-d H:m:s');
                $loanAppPhase->Loan_Sub_Phase_Nr     = $param['subPhaseNr'];
                $loanAppPhase->Sub_Phase_Status_Desc = $param['subPhaseStatusDesc'];
                $loanAppPhase->Sub_Phase_Start_Dttm  = date('Y-m-d H:m:s');
                $loanAppPhase->Sub_Phase_End_Dttm    = date('Y-m-d H:m:s');
                $loanAppPhase->Created_By_User_Id    = $userId;
                $loanAppPhase->Create_Dt             = date('Y-m-d');
                $loanAppPhase->save();

                return true;
            } 
        }

        $prevLoanAppNr = $param['loanAppPhaseNr'] - 1; 

        //Update Previous Loan App Phase Everytime we do Insert / Update
        $this->updatePrevLoanAppPhaseEndDt( $userId, $loanAppNr, $prevLoanAppNr );

    }

    /**
     * Update Or Create Loan Application Detail
     * 
     * @param  array  $data
     * @param  integer $loanAppNr
     * @return
     */
    protected static function updateOrCreateLoanAppDetail( $data = array(), $loanAppNr )
    {
        if( count($data) > 0  && !empty( $loanAppNr) ) {

            //Update Loan Appication Phase
            $loanAppPhase = Config::get('loan.LOAN_APP_PHASE2'); 
 
            $loanAppDetailCnt = self::where(array(
                    'Loan_App_Nr'       => $loanAppNr, 
                    'Applicant_User_Id' => $data['userId'],
                    'Loan_App_Phase_Nr' => 2,
                    'Loan_Sub_Phase_Nr' => $data['Loan_Sub_Phase_Nr']
                ))
                ->get()
                ->count();

            // _pre($loanAppDetailCnt);

            if( $loanAppDetailCnt > 0 ) {

                self::where( 
                        array(
                            'Loan_App_Nr'       => $loanAppNr,
                            'Applicant_User_Id' => $data['userId'],
                            'Loan_App_Phase_Nr' => 2, 
                            'Loan_Sub_Phase_Nr' => $data['Loan_Sub_Phase_Nr']
                        )
                    )
                    ->update( 
                        array(
                            'Applicant_User_Id'     => $data['userId'], 
                            'Loan_App_Phase_Nr'     => 2,
                            'Phase_Status_Desc'     => $loanAppPhase['Loan_App_Phase_Desc'],
                            'Loan_Sub_Phase_Nr'     => $data['Loan_Sub_Phase_Nr'],
                            'Sub_Phase_Status_Desc' => $data['Loan_Sub_Phase_Desc'],
                            'Sub_Phase_Start_Dttm'  => date('Y-m-d H:m:s'), 
                            'Sub_Phase_End_Dttm'    => date('Y-m-d H:m:s'),
                            'Phase_Start_Dttm'      => date('Y-m-d H:m:s'),
                            'Phase_End_Dttm'        => date('Y-m-d H:m:s'),
                            'Created_By_User_Id'    => $data['userId'],
                            'Create_Dt'             => date('Y-m-d')
                        )
                    );
                    
            } else {

                $loanAppDetail = new LoanApplicationDetail(); 
                $loanAppDetail->Applicant_User_Id     = $data['userId'];
                $loanAppDetail->Loan_App_Nr           = $loanAppNr;
                $loanAppDetail->Loan_App_Phase_Nr     = 2;
                $loanAppDetail->Phase_Status_Desc     = $loanAppPhase['Loan_App_Phase_Desc'];
                $loanAppDetail->Loan_Sub_Phase_Nr     = $data['Loan_Sub_Phase_Nr'];
                $loanAppDetail->Sub_Phase_Status_Desc = $data['Loan_Sub_Phase_Desc'];
                $loanAppDetail->Sub_Phase_Start_Dttm  = date('Y-m-d H:m:s');
                $loanAppDetail->Phase_Start_Dttm      = date('Y-m-d H:m:s');
                $loanAppDetail->Phase_End_Dttm        = date('Y-m-d H:m:s');
                $loanAppDetail->Sub_Phase_End_Dttm    = date('Y-m-d H:m:s');
                $loanAppDetail->Created_By_User_Id    = $data['userId'];
                $loanAppDetail->Create_Dt             = date('Y-m-d');
                $loanAppDetail->save();
            }
        }
    }
}