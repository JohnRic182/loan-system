<?php

class ApplicantAddress extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| AffordableLoanFactor Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'Applicant_Address';
	
	/**
	 * Primary Key 
	 * 
	 * @var string
	 */
	protected $primaryKey = 'User_Id';
	
	/**
	 * Timestamps
	 * @var boolean
	 */
	public $timestamps    = false;

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Street_Addr_1_Txt', 'Street_Addr_2_Txt');


	/**
	 * Class Constructor
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		Gfauth::decryptData();
 
	}



	/**
	 * Get Data 
	 *
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{  
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		if( $isRow == true )
			return ApplicantAddress::where($where)->select($encryptedFieldArr)->first();

		return ApplicantAddress::where($where)->select($encryptedFieldArr)->get();
	}


	/**
	 * Save ApplicantAddress data
	 *
	 * @return string
	 */
	public function saveApplicantAddress($fields = array(), $updateFlag = 0)
	{
		if( $updateFlag ){

			$ApplicantCnt = ApplicantAddress::where('User_Id', '=', $fields['userId'])
            					->get()
            					->count();

			if( $ApplicantCnt > 0 ) {
				$result = ApplicantAddress::where('User_Id', '=', $fields['userId'])
								            ->update(
								            array(
							            		'Street_Addr_1_Txt'  => Gfauth::encryptFields($fields['streetAddress1']),
												'Street_Addr_2_Txt'	 => Gfauth::encryptFields($fields['streetAddress1']),
												'City_Name' 		 => $fields['city'],
												'State_Cd' 			 => $fields['state'],
												'Zip_Cd'			 => $fields['zip'],
												'Zip_4_Cd'			 => $fields['zip4'],
												'Validity_Start_Dt'  => DB::raw("CAST(GETDATE() AS DATE)"),
												'Created_By_User_Id' => $fields['userId'],
												'Create_Dt'			 => DB::raw("CAST(GETDATE() AS DATE)")	
								            ));
           	} else {
			   $result = $this->newApplicantAddress($fields);
        	}
		}else{
			$result = $this->newApplicantAddress($fields);
		}

		return $result;
	}

	/**
	 * Save New Applicant Address
	 * 
	 * @param  array  $fields
	 * @return
	 */
	public function newApplicantAddress( $fields = array() )
	{
		if( count($fields) > 0 ) {

			$result = ApplicantAddress::insert(
				    array(
				    	'User_Id'          	 => $fields['userId'],
						'Street_Addr_1_Txt'  => Gfauth::encryptFields($fields['streetAddress1']),
						'Street_Addr_2_Txt'	 => Gfauth::encryptFields($fields['streetAddress1']),
						'City_Name' 		 => $fields['city'],
						'State_Cd' 			 => $fields['state'],
						'Zip_Cd'			 => $fields['zip'],
						'Zip_4_Cd'			 => $fields['zip4'],
						'Validity_Start_Dt'  => DB::raw("CAST(GETDATE() AS DATE)"),
						'Created_By_User_Id' => $fields['userId'],
						'Create_Dt'			 => DB::raw("CAST(GETDATE() AS DATE)")
				    	)
				);

			return $result;
		}
	}
}