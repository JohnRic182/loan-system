<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Library
 */
class ApplicantIdentityInfo extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| Applicant Funding Verification Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	protected $table = 'Applicant_Identity_Verification';

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Id_Image_URL_Txt');


	public function __construct()
	{
		parent::__construct();
		//run data decryption
		Gfauth::decryptData();
	}

	/**
	 * Get Data 
	 *
	 * @todo   remove the old getData and rename this one
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		if( $isRow == true )
			return ApplicantIdentityInfo::where($where)->select($encryptedFieldArr)->first();

		return ApplicantIdentityInfo::where($where)->select($encryptedFieldArr)->get();
	}


}