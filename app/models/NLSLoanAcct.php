<?php

/**
 * Developer's Note
 *
 *  - update todo list
 */

class NLSLoanAcct extends Eloquent{
	
  	/**
  	 * NLS connection
  	 */ 
	protected $connection = 'nls'; 
	
	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'cifno';
	
	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'loanacct';

	/**
	 * Check whether the Loan Application Exist
	 * 
	 * @param  integer  $loanAppNr
	 * @return boolean
	 */
	public static function isLoanAcctExist( $loanAppNr )
	{
		if( !empty($loanAppNr) ) {
			return NLSLoanAcct::where('loan_number', '=', $loanAppNr)
							->get()
							->count();
		}
	}
}