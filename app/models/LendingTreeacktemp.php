<?php

/**
 * Developer's Note
 *
 *   - temp table for lending tree testing
 *   -
 */
class LendingTreeacktemp extends Eloquent{

	protected $table  = 'LendingTree_Ack_Temp';

	const CREATED_AT = 'Create_Dt';

	const UPDATED_AT = 'Update_Dt';
}