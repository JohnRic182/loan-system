<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class LoanPurpose extends Eloquent{

	/**
	 *  Primary Table
	 *  
	 * @var string
	 */
	protected $table      = 'Loan_Purpose';
		
	/**
	 * Primary key
	 * @var string
	 */
	protected $primaryKey = 'Loan_Purpose_Id';

	public static function getLoanPurposeName($purposeId)
	{
		$purposeName  = "";
		$result = self::find($purposeId);
		
		if( isset($result->Loan_Purpose_Desc) )
			$purposeName = $result->Loan_Purpose_Desc;
		
		return $purposeName;
	}

}