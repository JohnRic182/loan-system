<?php

/**
 * Loan Application Sub Phase Model
 *
 * @version  1.0
 * @author   Global Fusion <ascend@global-fusion.net>
 *
 * Develope's Note
 *
 *  - Added Loan Payment Calculation
 *  - Added Loan Payment Date Calculation
 */
class LoanAppSubPhase extends Eloquent{

	/**
	 * Table name
	 * @var string
	 */
	protected $table      = 'Loan_Sub_Phase';
	
	/**
	 * Primary Id
	 * @var null
	 */
	protected $primaryKey = NULL;
	
	/**
	 * Timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
	 * Get Loan App Sub Phase 
	 * 
	 * @param  integer $loanAppPhaseNr
	 * @param  integer $loanSubPhaseNr
	 * @return array
	 */
	public static function getLoanAppSubPhaseById( $loanAppPhaseNr = 0, $loanSubPhaseNr = 0 )
	{
		return LoanAppSubPhase::where(
				 	array(
				 		'Loan_App_Phase_Id' => $loanAppPhaseNr, 
				 		'Loan_Sub_Phase_Id' => $loanSubPhaseNr
				 	)
				)->get();
	}
	
}