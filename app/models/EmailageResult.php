<?php



class EmailageResult extends Eloquent {

 
	/** 
	 * The database table used by the model.
	 *
	 * @var string
	 */ 
	protected $table = 'Email_Age';

	
	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';


	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

	public $timestamps = false;
 

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Email_Id', 'E_Name', 'Birth_Dt', );

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();  

		//call decryption of data
		Gfauth::decryptData();
	}
 
	/**
	 * Get User Data
	 * 
	 * @param  array  $fields
	 * @return array
	 */
  	public static function getData( $fields = array(), $where = array(), $isRow = false )
  	{	
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		//get only the first row 
		if($isRow == true)
			return User::where($where)->select($encryptedFieldArr)->first();	

		return User::where($where)->select($encryptedFieldArr)->get();
  	}

	/**
	 * Save User data
	 *
	 * @param  array $fields
	 * @return string
	 */
	public function saveUser( $fields = array())
	{
		
		if( count($fields) ) {
			
			try {
				$result = DB::table('User_Acct')->insert(
				    array(
						'User_Name'             => $fields['username'],
						'User_Id'               => DB::raw("DEFAULT"),
						'Password_Txt'          => Gfauth::encryptFields($fields['password']),
						'Remember_Token_Txt'    => $fields['_token'],
						'Email_Id'              => Gfauth::encryptFields($fields['email']),
						'Alt_Email_Id'          => Gfauth::encryptFields($fields['email']),
						'IP_Addr_Id'		    => $fields['IP_Addr_Id'],
						'User_Role_Id'          => $fields['User_Role_Id'] ,
						'User_Ref_Nr'           => "",
						'Credit_Pull_Auth_Flag' => 'Y',
						'Created_By_User_Id'    => 1 ,
						'Create_Dt'             => DB::raw("CAST(GETDATE() AS DATE)")
				    )
				);
				
				$user = User::where('User_Name','=',$fields['username'])->first();

			    return $user;

			}catch(Exception $e){
				return $e->getMessage();
			}
		}	
	}

}