<?php

class ExclusionVersionVariable extends Eloquent{

	/**
     * table name 
     * 
     * @var string
     * @access protected
     */
	protected $table = 'Exclusion_Version_Variable';

	/**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = null;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

}