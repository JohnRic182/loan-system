<?php

/**
 * Developer's Note
 *
 *   
 */
class PasswordResetInfo extends Eloquent{

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table      = 'Password_Reset_Info';
	
	/**
	 * Table Primary Key
	 * 
	 * @var string
	 */
	protected $primaryKey = 'User_Id';
		
	/**
	 * Constructor
	 */

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

	public function __construct(){

		parent::__construct();  
		//call decryption of data
		Gfauth::decryptData();
	}

	public static function getData( $fields = array(), $where = array() ){

		$data = PasswordResetInfo::where($where)->select($fields)->first();

		return $data;
	
	}

	/**
	 * STATUS: EXPIRED, IN_PROGRESS, COMPLETE
	 */
	public function updateStatus( $verificationCode, $status ){

		DB::table($this->table)->where('Reset_Verification_Cd', '=', $verificationCode)
			->update(
				array(
					'Password_Reset_Status_Desc' => $status,
					'Update_Dt' => date('Y-m-d')
					)
			);

	}


}