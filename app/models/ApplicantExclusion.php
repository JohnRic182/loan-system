<?php

class ApplicantExclusion extends Eloquent{
	/*
	|--------------------------------------------------------------------------
	| Applicant_Exclusion Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'Applicant_Exclusion';

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'Applicant_User_Id';

	/**
	 * Timestamps
	 * @var boolean
	 */
    public $timestamps = false;

}