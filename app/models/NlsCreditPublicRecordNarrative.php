<?php

class NlsCreditPublicRecordNarrative extends Eloquent{

	protected $table = 'NLS_CreditPublicRecordNarrative';

	protected $primaryKey = 'CreditPublicRecordNarrativeID';

	public $timestamps = false;

}