<?php

/**
 * Verification Assignment
 *
 * @version 1.0
 * @author  Janzell Jurill
 */

class VerificationAssignment extends Eloquent{

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'Verification_Assignment';

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';

	/**
	 * Timestamps
	 * @var boolean
	 */
    public $timestamps = false;

    /**
     * Assign new/update CSR for Verification
     * 
     * @param  array   $data
     * @return
     */
    public static function assign( $data = array() )
    {

    	$result = array(); 

    	if( count($data) > 0 ) {

    		$isExist = self::where(array(
    			'User_Id' => $data['User_Id'], 
    			'Loan_App_Nr' => $data['Loan_Id']
    		))->count();

    		if( $isExist > 0 ) {
    			//update assignee
    			$result = self::where(
	    			array(
		    			'User_Id' => $data['User_Id'], 
		    			'Loan_App_Nr' => $data['Loan_Id']
			    	))->update( 
    					array(
							'CSR_Id' => $data['CSR_Id'],
							'CSR_User_Name' => $data['CSR_User_Name'], 
							'Updated_By_User_Id' => $data['User_Id'],
							'Update_Dt' => date('Y-m-d H:m:s'),
    					) 
    				);
    		} else {
    			//create new assignee
				$data['Create_Dt'] = date('Y-m-d H:m:s');
				$data['Created_By_User_Id'] = $data['User_Id'];

    			$result = self::insert( 
    				array(
    					'User_Id' => $data['User_Id'], 
    					'Loan_App_Nr' => $data['Loan_Id'],
    					'CSR_Id' => $data['CSR_Id'], 
    					'CSR_User_Name' => $data['CSR_User_Name'],
    					'Create_Dt' => date('Y-m-d H:m:s'),
    					'Created_By_User_Id' => $data['User_Id']
    				) 
    			);
    		}
    	}

    	return $result;
    }
}