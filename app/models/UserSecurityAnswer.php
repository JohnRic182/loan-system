<?php

/**
 * Developer's Notes:
 *
 */

class UserSecurityAnswer extends Eloquent{ 

	/** 
	 * The database table used by the model.
	 *
	 * @var string
	 */ 
	protected $table = 'User_Security_Answer';


	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Answer_Txt');

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		//call decryption of data
		Gfauth::decryptData();
	}

	/**
	 * Get User Data
	 * 
	 * @param  array  $fields
	 * @return array
	 */
  	public static function getData( $fields = array(), $where = array())
  	{	
		return SecurityQuestion::where($where)->select($fields)->get();
  	}

}