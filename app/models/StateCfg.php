<?php

class StateCfg extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| AffordableLoanFactor Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'State_Cfg';

	public static function getStateConfig($statCode = ''){

		if(empty($statCode))
			$statCode = 'CA';			

		$data = StateCfg::select(array('Loan_Cfg_Name', 'Loan_Cfg_Val'))->where(array('State_Cd' => $statCode))->get();

		return $data;

	}

}