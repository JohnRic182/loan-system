<?php

/**
 * Developer's Note
 *
 *  - update todo list
 */

class NLSTaskTemplate extends Eloquent{
	
  	/**
  	 * NLS Task Table
  	 */ 
	protected $connection = 'nls'; 
	
	protected $primaryKey = 'task_template_no';
	
	protected $table      = 'task_template';
 

}