<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserProviderInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

/**
 * Developer's Notes:
 *
 *   - transferred logout method to helper
 *   - added username in saveuser method
 *   - removed the encryption and decryption of data (see- libraries/gfauth.php )
 *   - made the getData static method
 */

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
 
	/** 
	 * The database table used by the model.
	 *
	 * @var string
	 */ 
	protected $table = 'User_Acct';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('Password_Txt');
 

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';


	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

	public $timestamps = false;
 

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Password_Txt', 'Email_Id', 'Alt_Email_Id', );

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();  

		//call decryption of data
		Gfauth::decryptData();
	}
 
	/**
	 * Get User Data
	 * 
	 * @param  array  $fields
	 * @return array
	 */
  	public static function getData( $fields = array(), $where = array(), $isRow = false )
  	{	
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		//get only the first row 
		if($isRow == true)
			return User::where($where)->select($encryptedFieldArr)->first();	

		return User::where($where)->select($encryptedFieldArr)->get();
  	}

	/**
	 * Get User Data by email
	 * 
	 * @param  array  $fields
	 * @return array
	 */
  	public static function getDataByEmail( $fields, $Email_Id  )
  	{

		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		return User::select($encryptedFieldArr)
					->whereRaw('CONVERT(VARCHAR(100), DECRYPTBYKEY(Email_Id)) = ?', array("$Email_Id"))
					->first();

  	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->attributes['User_Name'];
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
	    return 'Remember_Token_Txt';
	}
	
	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->attributes['Password_Txt'];
	}

	/**
	 *  Check if username and password exist
	 *
	 * @return True if User exist
	 */
	public function checkUser($param = array(), $fields = array(), $isEmail = TRUE )
	{

		$encryptedFieldArr = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);
 	
 		if( $isEmail == TRUE ) {
			return User::whereRaw('CONVERT(VARCHAR(100), DECRYPTBYKEY(Email_Id)) = ? AND CONVERT(VARCHAR(100), DECRYPTBYKEY(Password_Txt)) = ?', $param )
					->select($encryptedFieldArr)
					->first();
 		}else {
 			return User::whereRaw('User_Name = ? AND CONVERT(VARCHAR(100), DECRYPTBYKEY(Password_Txt)) = ? AND User_Role_Id = ?', $param )
					->select($encryptedFieldArr)
					->count();
 		}
		
	}

	
	/**
	 * Update Password
	 *
	 * @param integer $uid
	 * @param string  $password
	 */
	public function updatePassword( $uid = 0, $password = '')
  	{	
  		$sql = '';
  		//$password  = User::formatEncryptedFields($field);
  		$sql = 'Update User_Acct 
				set Password_Txt = '.Gfauth::encryptFields($password).'
				where User_Id = '.$uid;
		return DB::update($sql);
	}

	/**
	 * Update username
	 *
	 * @param integer $uid
	 * @param string  $username
	 */
	public function updateUsername( $uid = 0, $username = '')
  	{	
  		$sql = '';
  		$sql = "Update User_Acct set User_Name = '$username' where User_Id = ".$uid;
		return DB::update($sql);
	}

	/**
	 * Check if Email Address is unique
	 *
	 * @param  string $email
	 * @return integer
	 */
	public function isUniqueEmail($email = '')
	{
		return User::whereRaw('CONVERT(VARCHAR(100), DECRYPTBYKEY(Email_Id)) = ?', array("$email"))
						->count();
	}

	/**
	 * Check if Email Address is unique
	 *
	 * @return string
	 */
	public function isRecordExist( $param = array() )
	{
		return User::Where($param)->count();
	}

	/**
	 * Update User Account
	 * 
	 * @param  integer $userId
	 * @param  array  $fields
	 * @return
	 */
	public function updateUser( $userId, $fields = array() )
	{
		if( !empty($userId) && count($fields) > 0 ){

			return DB::table('User_Acct')->where(
					array(
						'User_Id' => $userId
					)
				)->update(
				    array(
						'User_Name'             => $fields['username'],
						'Password_Txt'          => Gfauth::encryptFields($fields['password']),
						'Email_Id'              => Gfauth::encryptFields($fields['email']),
						'IP_Addr_Id'		    => $fields['IP_Addr_Id'],
						'Update_Dt'				=> date('Y-m-d'),
						'Updated_By_User_Id'    => $userId
				    )
				);
		}
		
	}

	/**
	 * Save User data
	 *
	 * @param  array $fields
	 * @return string
	 */
	public function saveUser( $fields = array())
	{
		
		if( count($fields) ) {
			
			try {
				$result = DB::table('User_Acct')->insert(
				    array(
						'User_Name'             => $fields['username'],
						'User_Id'               => DB::raw("DEFAULT"),
						'Password_Txt'          => Gfauth::encryptFields($fields['password']),
						'Remember_Token_Txt'    => $fields['_token'],
						'Email_Id'              => Gfauth::encryptFields($fields['email']),
						'Alt_Email_Id'          => Gfauth::encryptFields($fields['email']),
						'IP_Addr_Id'		    => $fields['IP_Addr_Id'],
						'User_Role_Id'          => $fields['User_Role_Id'] ,
						'User_Ref_Nr'           => "",
						'Credit_Pull_Auth_Flag' => 'Y',
						'Created_By_User_Id'    => 1 ,
						'Create_Dt'             => DB::raw("CAST(GETDATE() AS DATE)")
				    )
				);
				
				$user = User::where('User_Name','=',$fields['username'])->first();

			    return $user;

			}catch(Exception $e){
				return $e->getMessage();
			}
		}	
	}

	/**
	 * Update User Reference No from NLS
	 * 
	 * @return
	 */
	protected static function updateUserRefNo()
	{

		//Get all users where User_Ref_Nr is empty
		$users = User::whereIn(
				'User_Ref_Nr', array("","en", NULL)
				)->select('User_Id')
		->get();

		$user = array();

		if (count( $users ) > 0 ) {
			foreach ($users as $key => $value) {
				$user[] = $value->User_Id;
			} 
		}

		//Get correspding cifno from NLS 
		$nls = DB::connection('nls')->table('cif')->whereIn(
			'cifnumber', 
			$user	
		)
		->select('cifno', 'cifnumber')
		->get();

		//Update NLS table 
		if (count( $nls ) > 0 ) {

			foreach($nls as $key => $val ) {

				User::where(
					'User_Id', '=', $val->cifnumber
				)->update(
					array(
						'User_Ref_Nr' => $val->cifno
					)
				);
			}
		}
	}
}