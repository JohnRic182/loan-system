<?php

class TURateReward extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'TU_Rate_Reward_Attribute';

	/**
	 * Primary key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';

	/**
	 * Timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
     * Is Credit Profile Exist
     *  
     * @param  integer  $userId
     * @param  integer  $loanAppNr
     * @return boolean
     */
    public static function isTURateRewardExist( $loanAppNr, $userId )
    {
    	return TURateReward::where('User_Id', '=', $userId )
    			 	->where('Loan_App_Nr', '=', $loanAppNr )
    				->get()  
    				->count();
    }
}