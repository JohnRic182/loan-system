<?php

/**
 * Developer's Note
 *   - 
 *   - 
 */
class IovationResponse extends Eloquent
{
	/**
	 * Table Name
	 * @var string
	 */
	protected $table  = 'Iovation_Response';
	
	/**
	 * Primary key
	 * @var string
	 */
	// protected $primaryKey = '';

	const CREATED_AT = 'Create_Dt';
	const UPDATED_AT = 'Update_Dt';


	/**
	 * Get Data 
	 *
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{  
		if( $isRow == true )
			return Partner::where($where)->select($fields)->first();

		return Partner::where($where)->select($fields)->get();
	}


	public static function saveIovationResponse($data = array()){

		$ioObject = new IovationResponse();

		$ioObject->Result_Cd 		= (isset($data['result'])) ? $data['result'] : DB::raw("DEFAULT");
		$ioObject->Reason_Desc_Txt 	= (isset($data['reason'])) ? $data['reason'] : DB::raw("DEFAULT");
		$ioObject->End_Blackbox_Txt = (isset($data['endblackbox'])) ? $data['endblackbox'] : DB::raw("DEFAULT");
		$ioObject->Tracking_Nr 		= (isset($data['trackingnumber'])) ? $data['trackingnumber'] : DB::raw("DEFAULT");
		
		$ioObject->Device_Alias_Cd 					= (isset($data['device.alias'])) ? $data['device.alias'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Conf_Language_Cd 	= (isset($data['device.browser.configuredlang'])) ? $data['device.browser.configuredlang'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Language_Cd 		= (isset($data['device.browser.lang'])) ? $data['device.browser.lang'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Type_Cd 			= (isset($data['device.browser.type'])) ? $data['device.browser.type'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Version_Nr 		= (isset($data['device.browser.version'])) ? $data['device.browser.version'] : DB::raw("DEFAULT");
		$ioObject->Device_Cookie_Enabled_Flag 	= (isset($data['device.cookie.enabled'])) ? $data['device.cookie.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_Firstseen_Ts 			= (isset($data['device.firstseen'])) ? $data['device.firstseen'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Enabled_Flag 	= (isset($data['device.flash.enabled'])) ? $data['device.flash.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Installed_Flag 	= (isset($data['device.flash.installed'])) ? $data['device.flash.installed'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Storage_Enabled_Flag = (isset($data['device.flash.storage.enabled'])) ? $data['device.flash.storage.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Version_Nr 	= (isset($data['device.flash.version'])) ? $data['device.flash.version'] : DB::raw("DEFAULT");
		$ioObject->Device_Js_Enabled_Flag 	= (isset($data['device.js.enabled'])) ? $data['device.js.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_New_Flag 		= (isset($data['device.new'])) ? $data['device.new'] : DB::raw("DEFAULT");
		$ioObject->Device_Os_Version_Nr = (isset($data['device.os'])) ? $data['device.os'] : DB::raw("DEFAULT");
		$ioObject->Device_Screen_Txt 	= (isset($data['device.screen'])) ? $data['device.screen'] : DB::raw("DEFAULT");
		$ioObject->Device_Type_Cd 		= (isset($data['device.type'])) ? $data['device.type'] : DB::raw("DEFAULT");
		$ioObject->Device_Tz 			= (isset($data['device.tz'])) ? $data['device.tz'] : DB::raw("DEFAULT");

		$ioObject->IP_Addr_Id 					= (isset($data['ipaddress'])) ? $data['ipaddress'] : DB::raw("DEFAULT");
		$ioObject->Real_IP_Addr_Id 				= (isset($data['realipaddress'])) ? $data['realipaddress'] : DB::raw("DEFAULT");
		$ioObject->Real_IP_Addr_Id_Source_Cd 	= (isset($data['realipaddress.source'])) ? $data['realipaddress.source'] : DB::raw("DEFAULT");
		$ioObject->Ruleset_Rulesmatched_Val 	= (isset($data['ruleset.rulesmatched'])) ? $data['ruleset.rulesmatched'] : DB::raw("DEFAULT");
		$ioObject->Ruleset_Score_Val 			= (isset($data['ruleset.score'])) ? $data['ruleset.score'] : DB::raw("DEFAULT");

		$ioObject->Evidence_Types_Txt 	= '';
		$ioObject->Created_By_User_Id 	= $data['userId'];
		$ioObject->Create_Dt 			= date('Y-m-d');
		$ioObject->Updated_By_User_Id 	= $data['userId'];
		$ioObject->Update_Dt 			= date('Y-m-d');

		$ioObject->save();

	}


}