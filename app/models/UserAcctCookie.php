<?php
/**
 * Developer's Note
 *
 *   - 
 */
class UserAcctCookie extends Eloquent{

	protected $fillable = array('Cookie_Id', 'Partner_Id', 'Campaign_Id', 'Created_By_User_Id');

	protected $table = 'User_Acct_Cookie';

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
	
	protected $primaryKey = 'Cookie_Id';

	/**
	 * Update Expiration Date for Cookie
	 * 
	 * @param   string $Cookie_Id
	 * @return
	 */
	public function updateExpiration( $Cookie_Id )
	{

		$UserAcctCookie = DB::select('select * from '. $this->table .' where Cookie_Id = ? and Cookie_Expired_Flag = ? and User_Registered_Flag = ?', array($Cookie_Id, 0, 0));

		if( $UserAcctCookie ){

			$numDays = countDays( $UserAcctCookie[0]->Cookie_Create_Dttm , date('Y-m-d'));
			$flag = ($numDays > 60)? 1 : 0 ;

			DB::table($this->table)
		    ->where(['Cookie_Id' => $Cookie_Id, 'Cookie_Expired_Flag' => 0, 'User_Registered_Flag' => 0 ])
		    ->update(array('Cookie_Expired_Flag' => $flag ));

	    }
	}

	/**
	 * Update Registered Cookie
	 * 
	 * @param  string $Cookie_Id
	 * @param  string $flag
	 * @return
	 */
	public function updateRegistered($Cookie_Id, $flag)
	{

		$result = false;

		$UserAcctCookie = DB::select('select * from '. $this->table .' where Cookie_Id = ? and Cookie_Expired_Flag = ? and User_Registered_Flag = ?', array($Cookie_Id, 0, 0));

		if( $UserAcctCookie ){

			$result = DB::table($this->table)
		    ->where(['Cookie_Id' => $Cookie_Id, 'Cookie_Expired_Flag' => 0, 'User_Registered_Flag' => 0 ])
		    ->update(array('User_Registered_Flag' => $flag ));

	    }

		return $result;

	}


	/**
	 * Update Created Date and Time
	 * 
	 * @param  string $Cookie_Id
	 * @return
	 */
	public function updateDate( $Cookie_Id )
	{
		DB::table($this->table)
	    ->where(['Cookie_Id' => $Cookie_Id, 'Cookie_Expired_Flag' => 0, 'User_Registered_Flag' => 0 ])
	    ->update(array('Cookie_Create_Dttm' => date('Y-m-d')));
	}

}