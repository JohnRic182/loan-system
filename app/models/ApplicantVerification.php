<?php

/**
 * Applicant Verification Object
 * 
 */
class ApplicantVerification extends Eloquent{

	protected $table      = 'Applicant_Verification';
	
	protected $primaryKey = 'User_Id';

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

	public function __construct()
	{

	}

	/**
	 * Update Applicant Verification
	 * 
	 * @param  array  $where
	 * @param  array  $data
	 * @return
	 */
	public static function updateData( $where = array(), $data = array())
	{
		$result  = NULL;
		$isExist = ApplicantVerification::where( $where )->count();

		if( $isExist > 1 ) {

			$data['Update_Dt'] = date('Y-m-d');
			$data['Updated_By_User_Id'] = $data['User_Id'];

			$result = ApplicantVerification::where( $where )
					->update( $data );	
		}

		return $result;
	}

	/**
	 * Save Applicant Verification 
	 * 
	 * @param  array  $data
	 * @return
	 */
	public static function createData( $data = array() )
	{

		// echo 'create data';

		// _pre($data);

		$isExist = self::where(
			array(
				'User_Id' => $data['User_Id'],
				'Loan_App_Nr' => $data['Loan_App_Nr'],
				'ODS_Verification_Id' => $data['ODS_Verification_Id']
			)
		)->count();

		// pre($isExist);

		if( $isExist > 0 ) {

			foreach ($data as $key => $value) {
				if( empty($value) ) {
					$data[$key] = DB::raw("DEFAULT");
				}
			}

			$data['Update_Dt'] = date('Y-m-d');
			$data['Updated_By_User_Id'] = $data['User_Id'];

			return self::where(
				array(
					'User_Id' => $data['User_Id'],
					'Loan_App_Nr' => $data['Loan_App_Nr'],
					'ODS_Verification_Id' => $data['ODS_Verification_Id']
				)
			)->update( $data );

		} else{

			$data['Create_Dt'] = date('Y-m-d');
			$data['Created_By_User_Id'] = $data['User_Id'];

			return self::insert( $data );
		}
	}
}