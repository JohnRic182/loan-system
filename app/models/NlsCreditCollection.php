<?php

class NlsCreditCollection extends Eloquent{

	protected $table = 'NLS_CreditCollection';

	protected $primaryKey = 'CreditCollectionID';

	public $timestamps = false;

}