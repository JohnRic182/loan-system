<?php

class NlsCreditBankruptcy extends Eloquent{

	protected $table = 'NLS_CreditBankruptcy';

	protected $primaryKey = 'CreditBankruptcyID';

	public $timestamps = false;
}