<?php
/**
 * 
 *
 * @version  0.1
 */

class VwSpvLoan extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'vw_Rpt_SPV_Loan_Report'; 

 	protected $primaryKey = 'Loan_Number';

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		// Gfauth::decryptData();
		// DB::statement('SET ANSI_NULLS ON');
		// DB::statement('SET ANSI_WARNINGS ON');
	}


	public function getData($startDate, $endDate){

		$data = DB::table($this->table)
		->whereBetween('Date_Of_Loan', array( $startDate, $endDate ))->orderBy('Date_Of_Loan')->get();
		
		return $data;
	}	

	 
}