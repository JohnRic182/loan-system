<?php

class Idology {
    
    /**
     * Service URL
     * @var string
     */
    protected $serviceUrl; 

    /**
     * Idology Answer Url
     * @var string
     */
    protected $answerUrl; 

    /**
     * Idology Username
     * @var string
     */
    protected $username; 

    /**
     * Idology Password
     * @var string
     */
    protected $password; 

    /**
     * Idology IP Address
     * @var string
     */
    protected $ip; 

    /**
     * Idology Username for Ascend Init Profile
     * @var string
     */
    protected $usernameInit;
 
    protected $layout  =  null;

    public $fraudCheck1 = 0;
    public $fraudCheck2 = 0;
    public $test = 0;

    /**
     * Constructor
     */
    public function __construct()
    {

        $this->serviceUrl = Config::get('system.Idology.URL');
        $this->username   = Config::get('system.Idology.APIUsername');
        $this->password   = Config::get('system.Idology.APIPassword'); 
        $this->answerUrl  = Config::get('system.Idology.AnswerURL');
        // Idology Init 
        $this->usernameInit   = Config::get('system.Idology.APIUsernameInit');
        $this->passwordInit   = Config::get('system.Idology.APIPasswordInit'); 
        //get the client real ip address       
        $this->ip = getRealClientIpAddress();
        
        
    }


    /**
     * Send Request
     * 
     * @param  array  $fields
     * @param  string $profile
     * @return
     */
    public function sendRequest($fields = array(), $profile = 'ascend' )
    {
        $answersArr   = array();
        $questionsArr = array();

        $id           = '';        
        $userId       = getUserSessionID();
        $ip           = $this->ip;
        
        if(count($fields) > 0){
            $fields = array( 
                'username'  => $this->username,
                'password'  => $this->password,

                'firstName' => $fields['firstName'],
                'lastName'  => $fields['lastName'],
                'address'   => $fields['address'],
                'city'      => $fields['city'],
                'state'     => $fields['state'],
                'zip'       => $fields['zip'],
                'ssn'       => str_replace("-", "", $fields['ssn'])
            );  

        }else{
            $whereParam = array('User_Id' => $userId);  
            $applicant = new Applicant();
            $applicant_fields = array(
                'First_Name',
                'Middle_Name',
                'Last_Name',
                'Birth_Dt',
                'Social_Security_Nr'
            );
            
            $applicantData = $applicant->getData($applicant_fields,$whereParam, true );

            $whereParam = array('User_Id' => $userId);  
            $applicant = new Applicant();
            $applicant_fields = array(
                'First_Name',
                'Middle_Name',
                'Last_Name',
                'Birth_Dt',
                'Social_Security_Nr'
            );
            
            $applicantData = $applicant->getData($applicant_fields,$whereParam, true );

            $applicantAddress = new ApplicantAddress();
            $applicantAddress_fields = array(
                'Street_Addr_1_Txt',
                'City_Name',
                'State_Cd',
                'Zip_Cd'
            );
            $addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);
            
            $fields['lastName']          = $applicantData->Last_Name;
            $fields['firstName']         = $applicantData->First_Name;
            $fields['ssn']               = str_replace("-", "", $applicantData->Social_Security_Nr);
            $fields['address']           = $addressData->Street_Addr_1_Txt;
            $fields['city']              = $addressData->City_Name;
            $fields['state']             = $addressData->State_Cd;
            $fields['zip']               = $addressData->Zip_Cd;
        }

        if($profile == 'ascendInit'){
            $fields['username'] = $this->usernameInit;
            $fields['password'] = $this->passwordInit;
        }else{
            $fields['username'] = $this->username;
            $fields['password'] = $this->password;
        }

        // SET IDOLOGY SERVICE URL
        $url = $this->serviceUrl;

        // echo $url;
        $query = http_build_query($fields);

        // pre($query);
        
        $options = array(
            'http' => array(
                'method' => "POST",
                'header' => "Host: web.idologylive.com\r\n".
                "Content-type: application/x-www-form-urlencoded\r\n". "User-Agent: Mozilla 4.0\r\n".
                "Content-length: ".strlen($query)."\r\n".
                "Connection: close\r\n\r\n",
                'content' => $query, )
        );
        $data['test'] = 0;
        
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);

        // _pre($result);

        $idologyObject = new SimpleXMLElement($result);
        //pre($idologyObject);
        //die;
        $msg = array();

        if(property_exists($idologyObject, 'id-number')){
            $idNumber    = $idologyObject->{'id-number'};
        }else{
            $idologyObject->{'id-number'} = 00000;
            $idNumber    = $idologyObject->{'id-number'};
        }

        $summary     = $idologyObject->{'summary-result'};
        
        // check for errors
        if(!empty($idologyObject->error)){
            $errors = $idologyObject->error;
            
            $cerror = '';
            if(count($errors) > 0){
                foreach($errors as $error){
                    $cerror .= $error;
                }
                $msg = array(
                    'result'   => 'Failed',
                    'message'  => $cerror,
                    'ipAddress' => $ip,
                    'idNumber'  => $idNumber
                );
            }else{
                $msg = array(
                    'result'   => 'Failed',
                    'ipAddress' => $ip,
                    'message'  => $errors,
                    'idNumber'  => $idNumber
                );
            }  
            $summary->message = 'FAIL';
        }else{
            
            $flag = 0;
            
            $summary     = $idologyObject->{'summary-result'};
            $results     = $idologyObject->{'results'};
            $qualifiers  = $idologyObject->{'qualifiers'};
            $idnotescore = $idologyObject->{'idnotescore'};
            $questions   = $idologyObject->{'questions'};
            switch($summary->message){
                case 'PASS':
                    if($profile == 'ascendInit'){
                        $flag = 1;
                        $msg = array(
                            'result' => 'success',
                            'idNumber'  => $idNumber ,
                            'ipAddress' => $ip,
                            'message'   => 'Pass'
                        );
                    }else{
                        if(count($questions) > 0){
                            foreach($questions as $q){
                                foreach($q->question as $w){
                                    array_push($questionsArr, array(
                                            $w->prompt,
                                            $w->type,
                                            $w->answer
                                        ));
                                }
                            }
                            $flag = 0;
                            $msg = array(
                                    'result'    => 'questionTriggered',
                                    'questions' => $questionsArr,
                                    'idNumber'  => $idNumber,
                                    'ipAddress' => $ip,
                                    'message'   => 'questionTriggered',
                                    'flag'      => $summary->message
                            );
                        }else{
                            $flag = 1;
                            $msg = array(
                                'result' => 'success',
                                'idNumber'  => $idNumber ,
                                'ipAddress' => $ip,
                                'message'   => 'Pass'
                            );
                        }
                    }
                    break;
                case 'FAIL':
                case 'PARTIAL':
                    if($this->test){
                        $flag = 0;
                    }
                    if(count($questions) > 0){
                        if($profile != 'ascendInit'){
                            foreach($questions as $q){
                                foreach($q->question as $w){
                                    array_push($questionsArr, array(
                                            $w->prompt,
                                            $w->type,
                                            $w->answer
                                        ));
                                }
                            }
                            $flag = 0;
                            $msg = array(
                                    'result'    => 'questionTriggered',
                                    'questions' => $questionsArr,
                                    'idNumber'  => $idNumber,
                                    'ipAddress' => $ip,
                                    'message'   => 'questionTriggered',
                                    'flag'      => $summary->message
                            );
                        }else{
                            $msg = array(
                                'result' => 'Failed',
                                'idNumber'  => $idNumber ,
                                'ipAddress' => $ip,
                                'message'   => 'Fail No Error Message'
                            );
                        }
                    }else{
                        $flag = 1;
                        $msg = array(
                            'result' => 'Failed',
                            'idNumber'  => $idNumber ,
                            'ipAddress' => $ip,
                            'message'   => 'Fail No Error Message'
                        );
                    }
                    break;
                case 'NOT FOUND':
                    if($this->test){
                        $msg = array(
                            'result' => 'Not Found',
                            'message' => 'Not Found',
                            'idNumber'  => $idNumber ,
                            'ipAddress' => $ip,
                        );
                    }
                    $flag = 0;
                    break;
            }
        }
        //pre($msg);
        // die;
        return $msg;
    }

    /**
     * Send Answers 
     * 
     * @param  array $data
     * @return
     */
    public function sendAnswers($data)
    {
        $flag = 0;
        $fields = array(
            'username'          => $this->username,
            'password'          => $this->password,
            'idNumber'          => $data['idNumber']
        );  
        
        if(array_key_exists('question1Type', $data)){
            $fields['question1Type'] = $data['question1Type'];
            $fields['question1Answer'] = $data['question1Answer'];
        }
        if(array_key_exists('question2Type', $data)){
            $fields['question2Type'] = $data['question2Type'];
            $fields['question2Answer'] = $data['question2Answer'];
        }
        if(array_key_exists('question3Type', $data)){
            $fields['question3Type'] = $data['question3Type'];
            $fields['question3Answer'] = $data['question3Answer'];
        }
        if(array_key_exists('question4Type', $data)){
            $fields['question4Type'] = $data['question4Type'];
            $fields['question4Answer'] = $data['question4Answer'];
        }
        if(array_key_exists('question5Type', $data)){
            $fields['question5Type'] = $data['question5Type'];
            $fields['question5Answer'] = $data['question5Answer'];
        }        
        
        //pre($fields);
        // SET IDOLOGY SERVICE URL
        //$url = 'https://web.idologylive.com/api/idliveq-answers.svc';
        $url = $this->answerUrl;

        $query = http_build_query($fields);
        //print '<br />query<br />';
        //pre($query);
        $options = array(
            'http' => array(
                'method' => "POST",
                'header' => "Host: web.idologylive.com\r\n".
                "Content-type: application/x-www-form-urlencoded\r\n". "User-Agent: Mozilla 4.0\r\n".
                "Content-length: ".strlen($query)."\r\n".
                "Connection: close\r\n\r\n",
                'content' => $query, )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $idologyObject = new SimpleXMLElement($result);
        
        //pre($idologyObject); 
        //die;
        $error = $idologyObject->{'error'};
        //print $error; die;
        $iqSummaryResult = $idologyObject->{'iq-summary-result'};
        if(strtolower($iqSummaryResult) == 'fail'){
            $idliveqResult  = $idologyObject->{'idliveq-result'};
             $msg = array(
                'result' => 'Fail',
                'idNumber'  => $data['idNumber'],
                'ipAddress' => $this->ip,
                'message'   => $idliveqResult->message,
                'flag'      => 0
            );
        }else{            
             $msg = array(
                'result' => 'Pass',
                'idNumber'  => $data['idNumber'],
                'ipAddress' => $this->ip,
                'message'   => 'Pass',
                'flag'      => 1
            );
        }
        
        return $msg;
    }

    /**
     * Idology Call
     * 
     * @param  array   $idologyFields
     * @param  string  $type
     * @param  array   $phaseFields
     * @return
     */
    public function idologyCall( $idologyFields = array(), $type = '', $phaseFields = array()  )
    {
        $uid  = getUserSessionID();
        $lid  = getLoanAppNr();
        $flag = 'done';

        //Initialize Idology Result
        $idologyResult = new IdologyResult();

        // Checking if AscendInit Profile is already called
        $idologyCheck = IdologyResult::where('User_Id', '=', $uid)
                ->where('Call_Type_Cd', '=', 'ascendInit')
                ->first();  

        //Check idology check
        if( count($idologyCheck) == 0 ){

            $result = $this->sendRequest( $idologyFields, $type); 
        
            if( $result['result'] == 'success' ){
                $idologyResult->Idology_Result_Desc  = 'Pass';              
                $flag = 'success';

            }else{ 
                $idologyResult->Idology_Result_Desc  = 'Fail';
                $flag = 'fail';
            }

            $idologyResult->User_Id              = $uid;
            $idologyResult->Idology_Id_Nr        = $result['idNumber'];
            $idologyResult->Idology_Access_Dt    = date('Y-m-d');
            $idologyResult->Idology_KBA_Flag     = 0;
            $idologyResult->Idology_Reason_Txt   = $result['message'];
            $idologyResult->Call_Type_Cd         = 'ascendInit';
            $idologyResult->IP_Addr_Id           = $result['ipAddress'];
            $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
            $idologyResult->Created_By_User_Id   = $uid;
            $idologyResult->Create_Dt            = date('Y-m-d');
            $idologyResult->save();

        }

        return $flag;        
    }

}