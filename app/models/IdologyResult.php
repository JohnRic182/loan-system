<?php

class IdologyResult extends Eloquent{

	protected $table = 'Idology_Result';

	protected $primaryKey = 'Idology_Id_Nr';

    public $timestamps = false;

}