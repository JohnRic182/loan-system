<?php

/**
 * BankAccountTxnDetails Model 
 *
 * @version  0.1
 */

/**
 * Note: 
 */

class BankAccountTxnDetails extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'Bank_Acct_Txn_Detail';

	/**
	 * Primary ID
	 * @var string
	 */
	protected $primaryKey = '';

	//@todo - we need to transfer this to config table
 	protected $encryptedPassword = '5k_Us3r_4cct_P455w0rd';

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected $encryptedFields = array('Acct_Holder_Name', 'Acct_Nr');

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();  
	}

	 
	/**
	 * Decrypt Data
	 * 
	 * @return
	 */
	public function decryptData()
	{
		return DB::statement("OPEN SYMMETRIC KEY sk_User_Acct
							DECRYPTION BY ASYMMETRIC KEY ak_User_Acct
							WITH PASSWORD = '". $this->encryptedPassword ."';");
	}

	/**
	 * Format Encrypted Fields
	 * 
	 * @param  array  $fields
	 * @return [type]
	 */
	public function formatEncryptedFields( $fields =  array() )
	{
		if( count($fields) > 0 ) {

			foreach($fields as $key => $field ){
	  			if( in_array($field, $this->encryptedFields ) )
	  				$fields[$key] = "CONVERT(VARCHAR(100), DECRYPTBYKEY($field)) AS $field";
	  		}
  			return implode(',', $fields );
  		}
  		
	}

	/**
	 * Format Encrypt Fields
	 * 
	 * @param  array  $fields
	 * @return [type]
	 */
	public function encryptFields( $field = '' )
	{
		return DB::raw("ENCRYPTBYKEY(KEY_GUID('sk_User_Acct'), '".$field."')");
	}

	 
	 
}