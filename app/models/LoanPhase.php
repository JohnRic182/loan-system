<?php

class LoanPhase extends Eloquent{

	protected $table = 'Loan_App_Phase';
	
	protected $primaryKey = 'Loan_App_Phase_Id';

    public $timestamps = false;

}