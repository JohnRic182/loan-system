<?php

class YodleeAccessLog extends Eloquent{

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'Yodlee_Access_Log';

	/**
	 * Primary Key
	 * @var null
	 */
	protected $primaryKey = '';

	/**
	 * Timestamps
	 * 
	 * @var boolean
	 */
    public $timestamps = FALSE;

    /**
     * Insert Log
     * 
     * @param  array  $data
     * @return
     */
    public static function insertLog( $data = array() )
    {
        DB::table('Yodlee_Access_Log')->insert(
            array(
                'User_Id'               => $data['userId'], 
                'Loan_App_Nr'           => $data['loanAppNr'],
                'Site_Id'               => $data['siteId'], 
                'Yodlee_Access_Dt'      => date('Y-m-d'), 
                'Successful_Linking_Flag'     => $data['successfulLinkingFlag'], 
                'Successful_90_Day_Pull_Flag' => $data['successful90DayPullFlag']

            )
        );
    }

    /**
     * Update Log
     * 
     * @param  array  $data
     * @return
     */
    public static function updateLog( $data = array() )
    {
        DB::table('Yodlee_Access_Log')
            ->where(
                array(
                    'User_Id'          => $data['userId'], 
                    'Loan_App_Nr'      => $data['loanAppNr'],
                    'Site_Id'          => $data['siteId'], 
                    'Yodlee_Access_Dt' => date('Y-m-d')
                )
            )
            ->update(
            array( 
                'Successful_Linking_Flag'     => $data['successfulLinkingFlag'], 
                'Successful_90_Day_Pull_Flag' => $data['successful90DayPullFlag'] 
            )
        );
    }


    /**
     * Delete Log
     * 
     * @param  array  $data
     * @return
     */
    public function deleteLog( $data = array() )
    {

    }
}