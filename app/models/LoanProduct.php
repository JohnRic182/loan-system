<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class LoanProduct extends Eloquent{


	/*
	|--------------------------------------------------------------------------
	| LoanProduct Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name 
	 * 
	 * @var string
	 */
	protected $table      = 'Loan_Product';

	/**
	 * Primary key field 
	 * 
	 * @var null
	 */
	protected $primaryKey = 'Loan_Prod_Id';
	
	/**
	 * Time Stamps
	 * @var boolean
	 */
	public $timestamps    = false;	
	

}