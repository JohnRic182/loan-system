<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class LoanDocument extends Eloquent{

	protected $table = 'Loan_Document';
 
	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';


	protected $timestamp = false;


	public function __construct()
	{
		parent::__construct();
	} 

	/**
	 * Get Contract Count
	 * @param  [type] $userId
	 * @param  [type] $loanAppNr
	 * @return [type]
	 */
	public static function getContract($userId, $loanAppNr, $isCount = false) 
	{

		$where  = array(
			'User_Id'     => $userId, 
			'Loan_App_Nr' => $loanAppNr
		);

		if($isCount)
			return LoanDocument::where($where)->count(); 		

	  	return LoanDocument::where($where)->first(); 	
	}

	/**
	 * Save Hello Design Contract 
	 * 
	 * @param  array  $param
	 * @return 
	 */
	public static function saveLoanDocument( $param = array() )
	{

		if( count($param) > 0 ) {
 	
			$isCnt = LoanDocument::where(
					array(
						'User_Id'     => $param['userId'],
						'Loan_App_Nr' => $param['loanAppNr']
					)
				)->count();

			if( empty($param['signingUrl']) )
				$param['signingUrl'] = DB::raw('DEFAULT');

			if( $isCnt > 0 ) {

				LoanDocument::where(
					array(
						'User_Id'     => $param['userId'],
						'Loan_App_Nr' => $param['loanAppNr']
					)
				)->update(
					array(
						'User_Id'            => $param['userId'], 
						'Loan_App_Nr'        => $param['loanAppNr'], 
						'State_Cd'           => $param['stateCd'],
						'Signature_Id'       => $param['signatures'],
						'Signature_Req_Id'   => $param['signatureRequestId'],
						'Doc_Url_Txt'        => $param['filesUrl'],	
						'Sign_In_Url_Txt'    => $param['signingUrl'],
						'Sign_In_Dt'         => $param['signingDt'],
						'Doc_Title_Txt'      => $param['title'], 
						'Doc_Template_Id'    => $param['templateId'], 
						'Loan_Term_Desc'     => $param['loanTerm'], 
						'Payment_Amt'        => $param['paymentAmt'], 
						'Payment_Due_Dt'     => $param['paymentDueDt'], 
						'Created_By_User_Id' => $param['userId'],
						'Create_Dt'          => date('Y-m-d'),
						'Update_Dt'          => date('Y-m-d'), 
						'Updated_By_User_Id' => $param['userId']
					)
				); 
			} else{

				$LoanDocument = new LoanDocument();
				$LoanDocument->User_Id            = $param['userId'];
				$LoanDocument->Loan_App_Nr        = $param['loanAppNr'];
				$LoanDocument->State_Cd           = $param['stateCd'];
				$LoanDocument->Signature_Id       = $param['signatures'];
				$LoanDocument->Signature_Req_Id   = $param['signatureRequestId'];
				$LoanDocument->Doc_Url_Txt        = $param['filesUrl'];
				$LoanDocument->Sign_In_Url_Txt    = $param['signingUrl'];
				$LoanDocument->Sign_In_Dt 		  = $param['signingDt'];
				$LoanDocument->Doc_Title_Txt      = $param['title'];
				$LoanDocument->Doc_Template_Id    = $param['templateId'];
				$LoanDocument->Loan_Term_Desc     = $param['loanTerm'];
				$LoanDocument->Payment_Amt        = $param['paymentAmt'];
				$LoanDocument->Payment_Due_Dt     = $param['paymentDueDt'];
				$LoanDocument->Created_By_User_Id = $param['userId'];
				$LoanDocument->Create_Dt          = date('Y-m-d');
				$LoanDocument->save(); 
			}
		}
	}
}

 