<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor 
 */
class SalesForceMap extends Eloquent{

 	
	protected $table = 'SalesForce_Fields_Mapping';
	 
	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
	
	/**
	 * Get Sales Force Fields 
	 * 
	 * @param  string $fieldTypeCd
	 * @param  string $SFObjectName
	 * @return
	 */
	public static function getSFFieldsBySFObjectName( $SFObjectName = 'All' ) 
	{

		if( $SFObjectName == 'All' ) {
			$fields = SalesForceMap::all();
		} else{
			$fields =  SalesForceMap::where('SF_Object_Name', 'LIKE', '%' . $SFObjectName . '%')
									->orWhere('SF_Object_Name', 'All')
									->orderBy('Order_Id', 'DESC')
									->get();
		}
		
		$list = array();

		if( count($fields) > 0 ) {

			foreach($fields as $k => $config ){
		 
				$list[$config->SF_Field_Name] = array(
					'SF_Field_Name'      => $config->SF_Field_Name, 
					'ODS_Table_Name'     => $config->ODS_Table_Name, 
					'ODS_Field_Name'     => $config->ODS_Field_Name, 
					'ODS_Key_Field_Name' => $config->ODS_Key_Field_Name, 
					'Is_Encrypted_Flag'  => $config->Is_Encrypted_Flag, 
					'Field_Type_Cd'      => $config->Field_Type_Cd, 
					'Field_Desc_Txt'     => $config->Field_Desc_Txt,
					'Is_Active_Flag'     => $config->Is_Active_Flag,
					'SF_Object_Name'     => $config->SF_Object_Name,
				);
			}
		}
		
		return $list;
	}
}