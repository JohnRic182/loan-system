<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class Applicant extends Eloquent{


	/*
	|--------------------------------------------------------------------------
	| Applicant Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	protected $table      = 'Applicant';
	
	protected $primaryKey = 'User_Id';

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

 
	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('First_Name', 'Middle_Name','Last_Name', 'Birth_Dt', 'Social_Security_Nr', 'Yodlee_User_Name', 'Yodlee_Password_Txt');


	/**
	 * Class Constructor
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		Gfauth::decryptData();
	}


	/**
	 * Get Data 
	 *
	 * @todo   remove the old getData and rename this one
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		if( $isRow == true )
			return Applicant::where($where)->select($encryptedFieldArr)->first();

		return Applicant::where($where)->select($encryptedFieldArr)->get();
	}


	/**
	 * Check if SSN is unique
	 *
	 *
	 * @return string
	 */
	public function isUniqueSSN($ssn = '')
	{
		$result = Applicant::whereRaw('CONVERT(VARCHAR(100), DECRYPTBYKEY(Social_Security_Nr)) = ?', array("$ssn"))->first();
		if(is_object($result)){
			// existing
			$flag = 0;
		}else if(empty($result)){
			// not exist, unique
			$flag = 1;
		}
		return $flag;
	}


  /**
   * Gets the application by SSN.
   *
   * @param string $ssn
   * @return object
   */
  public function getApplicationBySSN($ssn = '') {
    $result = Applicant::whereRaw('CONVERT(VARCHAR(100), DECRYPTBYKEY(Social_Security_Nr)) = ?', array("$ssn"))
      ->orderBy('Create_Dt', 'desc')
      ->orderBy('Update_Dt', 'desc')
      ->first();
    if (is_object($result)) {
      return $result;
    }
    else {
      return false;
    }
  }


	/**
	 * Save User data
	 *
	 * @return string
	 */
	public function saveApplicant($fields = array(), $updateFlag = 0)
	{
		if( $updateFlag ){
			
			$result = DB::table('Applicant')
            ->where('User_Id', $fields['userId'])
            ->update(array(
            		'First_Name'		 => Gfauth::encryptFields($fields['firstName']),
					'Middle_Name'		 => Gfauth::encryptFields($fields['middleName']),
					'Last_Name' 		 => Gfauth::encryptFields($fields['lastName']),
					'Age_Cnt' 			 => $fields['age'],
					'Birth_Dt'			 => Gfauth::encryptFields($fields['birthDate']),
					'Social_Security_Nr' => Gfauth::encryptFields($fields['ssn']),
					'Yodlee_User_Name'	 => Gfauth::encryptFields($fields['yodleeUserName']),
					'Yodlee_Password_Txt'=> Gfauth::encryptFields($fields['yodleePassword']),
					'Created_By_User_Id' => 1 ,
					'Create_Dt'			 => DB::raw("CAST(GETDATE() AS DATE)"),
					'Updated_By_User_Id' => NULL ,
					'Update_Dt'			 => DB::raw("CAST(GETDATE() AS DATE)"))
            	);
		}else{
			$result = DB::table('Applicant')->insert(
			    array(
			    	'User_Id'          	 => $fields['userId'],
					'First_Name'		 => Gfauth::encryptFields($fields['firstName']),
					'Middle_Name'		 => Gfauth::encryptFields($fields['middleName']),
					'Last_Name' 		 => Gfauth::encryptFields($fields['lastName']),
					'Age_Cnt' 			 => $fields['age'],
					'Birth_Dt'			 => Gfauth::encryptFields($fields['birthDate']),
					'Social_Security_Nr' => Gfauth::encryptFields($fields['ssn']),
					'Yodlee_User_Name'	 => Gfauth::encryptFields($fields['yodleeUserName']),
					'Yodlee_Password_Txt'=> Gfauth::encryptFields($fields['yodleePassword']),
					'Created_By_User_Id' => 1 ,
					'Create_Dt'			 => DB::raw("CAST(GETDATE() AS DATE)"),
					'Updated_By_User_Id' => NULL ,
					'Update_Dt'			 => NULL
			    	)
			);
		}
		return $result;
	}
}