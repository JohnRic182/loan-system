<?php

/**
 * BankAccountInfo Model 
 *
 * @version  0.1
 */

/**
 * Note: 
 *
 * @todo  - transfer Decryption to Gfauth Library
 */

class BankAccountInfo extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'Bank_Acct_Info';

	/**
	 * Primary ID
	 * 
	 * @var string
	 */
	protected $primaryKey = '';
	
	public $timestamps    = false;
	
	public $incrementing  = false;


 	protected $encryptedPassword = '5k_Us3r_4cct_P455w0rd';

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected $encryptedFields = array('Acct_Holder_Name', 'Acct_Nr');

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		$this->decryptData();
	}

	 
	/**
	 * Decrypt Data
	 * 
	 * @return
	 */
	public function decryptData()
	{
		return DB::statement("OPEN SYMMETRIC KEY sk_User_Acct
							DECRYPTION BY ASYMMETRIC KEY ak_User_Acct
							WITH PASSWORD = '". $this->encryptedPassword ."';");
	}

	/**
	 * Format Encrypted Fields
	 * 
	 * @param  array  $fields
	 * @return [type]
	 */
	public function formatEncryptedFields( $fields =  array() )
	{
		if( count($fields) > 0 ) {

			foreach($fields as $key => $field ){
	  			if( in_array($field, $this->encryptedFields ) )
	  				$fields[$key] = "CONVERT(VARCHAR(100), DECRYPTBYKEY($field)) AS $field";
	  		}
  			return implode(',', $fields );
  		}
  		
	}

	/**
	 * Format Encrypt Fields
	 * 
	 * @param  array  $fields
	 * @return [type]
	 */
	public function encryptField( $field = '' )
	{
		return DB::raw("ENCRYPTBYKEY(KEY_GUID('sk_User_Acct'), '".$field."')");
	}

	/**
	 * Insert Batch data
	 * 
	 * @param  array  $data
	 * @return
	 */
	public static function insertBatch( $batchData = array() )
	{
		if ( count($batchData) > 0 ) {

			foreach ($batchData as $key => $data) {
				//Check if the data exist using User_Id, Loan_App_Nr, Yodlee_Access_Dt, Site_Id and Account_Type
				$isExist = BankAccountInfo::where(
							array(
								'User_Id'        => $data['User_Id'],  
								'Site_Id'        => $data['Site_Id'], 
								'Acct_Type_Desc' => $data['Acct_Type_Desc']
							)
						)->count();
			
				if( $isExist == 1 ){
					BankAccountInfo::where(
							array(
								'User_Id'        => $data['User_Id'],  
								'Site_Id'        => $data['Site_Id'], 
								'Acct_Type_Desc' => $data['Acct_Type_Desc']
							)
						)->update( $data );
				}else{
					BankAccountInfo::insert( $data );
				}			
			}		
		}
	}
	 
}