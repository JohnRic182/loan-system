<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 */
class LTCampaignCfg extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| LendingTree Campaign Config Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	protected $table = 'LT_Campaign_Cfg';

	protected $primaryKey = '';
	protected $configurations = array();

	public $timestamps  = false;

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
  

	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * Returns the row object of the configuration name.
	 *
	 * @param $configName
	 * @return mixed
	 */
	public function findConfigByName($configName) {
		$config = LTCampaignCfg::where('LT_Campaign_Cfg_Name', '=', $configName)->get();
		return $config;
	}


	public function getValue($configName) {
		$value = LTCampaignCfg::where('LT_Campaign_Cfg_Name', '=', $configName)->select('LT_Campaign_Cfg_Val')->first();
		return $value->LT_Campaign_Cfg_Val;
	}

	/**
	 * Returns the CID with it's corresponding Filter Routing IDs.
	 *
	 * @return array
	 */
	public function getCIDFilters() {
		$CID_Filter = array();

		$cids = $this->findConfigByName('CID');
		foreach ($cids as $cid) {

			//Get FilterRoutingId
			$filterRoutingIds = $this->findConfigByName('FilterRoutingId.' . $cid->LT_Campaign_Cfg_Val);

			foreach ($filterRoutingIds as $fr) {
				$CID_Filter[$cid->LT_Campaign_Cfg_Val][] = $fr->LT_Campaign_Cfg_Val;
			}
		}

		return $CID_Filter;
	}

}