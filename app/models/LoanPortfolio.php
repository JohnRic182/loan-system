<?php

class LoanPortfolio extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'Loan_Portfolio';
	
	/**
	 * Primary key
	 * @var string
	 */
	protected $primaryKey = 'Loan_Portfolio_Id';
	
	/**
	 * TimeStamps
	 * @var boolean
	 */
	public $timestamps    = false;	


	public static function getLoanPortfolioName( $loanPortfolioId )
	{
		$loanPortfolio = LoanPortfolio::where('Loan_Portfolio_Id', '=', $loanPortfolioId)->first();	
		
		if( $loanPortfolio )
			return $loanPortfolio->Loan_Portfolio_Name;
	}

}