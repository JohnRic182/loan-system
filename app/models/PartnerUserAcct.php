<?php

class PartnerUserAcct extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| AffordableLoanFactor Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'Partner_User_Acct';
	
	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('First_Name', 'Last_Name', 'Phone_Nr', 'Email_Id', 'Birth_Dt', 'Social_Security_Nr', 'Street_Addr_1_Txt', 'Street_Addr_2_Txt' );


	/**
	 * Class Constructor
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		Gfauth::decryptData();
 
	}

	/**
	 * Get Data 
	 *
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{  
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		if( $isRow == true )
			return PartnerUserAcct::where($where)->select($encryptedFieldArr)->first();

		return PartnerUserAcct::where($where)->select($encryptedFieldArr)->get();
	}	

	/**
	 * Save Partner Applicant Data
	 *
	 * @param  array $fields
	 * @return
	 */
	public function saveApplicantData($fields = array())
	{
		if( count($fields) > 0 ) {

			$PartnerUserAcct = PartnerUserAcct::firstOrNew([ 'Partner_User_Id' => $fields['trackingNr'] ]);

			if( $PartnerUserAcct->exists != 1) {

				$PartnerUserAcct->Partner_Id                = ( isset( $fields['PID'] ) )? $fields['PID'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Partner_User_Id           = ( isset( $fields['trackingNr'] ) )? $fields['trackingNr'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->First_Name                = Gfauth::encryptFields(isset($fields['firstName']) ? $fields['firstName'] : '' );
				$PartnerUserAcct->Last_Name                 = Gfauth::encryptFields(isset($fields['lastName']) ? $fields['lastName'] : '' );
				$PartnerUserAcct->Birth_Dt                  = Gfauth::encryptFields(isset($fields['dob']) ? $fields['dob'] : '' );
				$PartnerUserAcct->Social_Security_Nr        = Gfauth::encryptFields(isset($fields['ssn']) ? $fields['ssn'] : '' );
				$PartnerUserAcct->Street_Addr_1_Txt         = Gfauth::encryptFields(isset($fields['address1']) ? $fields['address1'] : '' );
				$PartnerUserAcct->Street_Addr_2_Txt         = Gfauth::encryptFields(isset($fields['address2']) ? $fields['address2'] : '' );
				$PartnerUserAcct->City_Name                 = ( isset( $fields['city'] ) )? $fields['city'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->State_Cd                  = ( isset( $fields['state'] ) )? $fields['state'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Zip_Cd                    = ( isset( $fields['zip'] ) )? $fields['zip'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Zip_4_Cd                  = ( isset( $fields['zip4'] ) )? $fields['zip4'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Phone_Nr                  = Gfauth::encryptFields(isset($fields['address2']) ? $fields['phoneNr'] : '' );
				$PartnerUserAcct->Email_Id                  = Gfauth::encryptFields(isset($fields['address2']) ? $fields['email'] : '' );
				$PartnerUserAcct->Housing_Sit_Id            = ( isset( $fields['housingSit'] ) )? $fields['housingSit'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Fico08_Val                = ( isset( $fields['score'] ) )? $fields['score'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Employment_Status_Id      = ( isset( $fields['empStatus'] ) )? $fields['empStatus'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Annual_Gross_Income_Amt   = ( isset( $fields['income'] ) )? $fields['income'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Loan_Purpose_Id           = ( isset( $fields['loanPurpose'] ) )? $fields['loanPurpose'] : DB::raw("DEFAULT") ;					
				$PartnerUserAcct->Monthly_Rent_Amt          = ( isset( $fields['montRent'] ) )? $fields['montRent'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Offered_APR_Val           = ( isset( $fields['APR'] ) )? $fields['APR'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Offered_Loan_Amt          = ( isset( $fields['loanAmt'] ) )? $fields['loanAmt'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Term_In_Months_Cnt        = ( isset( $fields['term'] ) )? $fields['term'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Campaign_Id               = ( isset( $fields['CID'] ) )? $fields['CID'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Self_Rpt_Credit_Score_Val = ( isset( $fields['selfReportedCredit'] ) )? $fields['selfReportedCredit'] : DB::raw("DEFAULT") ;
				$PartnerUserAcct->Ascend_User_Id            = isset($fields['userId']) ? $fields['userId'] : 0;
				$PartnerUserAcct->LT_Express_Funnel_Flag	= ( isset($fields['LTExpressFunnelFlag']) )? $fields['LTExpressFunnelFlag'] : 0 ;
				$PartnerUserAcct->Created_By_User_Id        = 0;
				$PartnerUserAcct->save();

			}
		}
	}

	/**
	 * update data via userID
	 * @param  string $userId UserID from exsisting record
	 * @param  array  $fields fields and value to be updated
	 * @return [type]         [description]
	 */
	public static function updateData($userId = '', $fields = array() )
	{

		$result = PartnerUserAcct::where('Ascend_User_Id', '=', $userId)
					->update($fields);

		return $result;

	}
}