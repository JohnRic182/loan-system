<?php

class RewardAutoCollateral extends Eloquent{

	protected $table      = 'Reward_Auto_Collateral';
	
	protected $primaryKey = 'Borrower_User_Id';
	
	public $timestamps    = false;


}