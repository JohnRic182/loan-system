<?php

class ApplicantFinancialInfo extends Eloquent{

	protected $table      = 'Applicant_Financial_Info';
	
	protected $primaryKey = 'User_Id';
	
	public $timestamps    = false;

	/**
	 * 6 means no delinquent accounts
	 * 
	 */
	const DEFAULT_DELINQUENT_VAL = 0;
	
	/**
	 * Get User Data
	 * 
	 * @param  array  $fields
	 * @return array
	 */
  	public static function getData( $fields = array(), $where = array(), $isRow = false )
  	{	
  		if($isRow)
  			return ApplicantFinancialInfo::where($where)->select($fields)->first();

		return ApplicantFinancialInfo::where($where)->select($fields)->get();
  	}

  	/**
	 * Update Or Create Applicant Financial Info
	 * 
	 * @param  array   $fields
	 * @param  integer $updateFlag
	 *
	 * @todo  
	 * 		 - Need to transfer this to Model
	 * @return
	 */
	protected static function updateOrCreateAppFinancialInfo($fields = array(), $updateFlag )
	{
 		if( $updateFlag ){

			$financial = self::find( $fields['userId'] );
			 
			if( !empty($financial) ) {

				$rentAmount = 0 ;

				if( $fields['rentAmount'] != '' ) 
					$rentAmount = $fields['rentAmount'];

				$updateFields = array(
						'Employment_Status_Desc'       => $fields['employeeStatusDescription'], 
						'Employment_Status_Id'         => $fields['employeeStatus'], 
						'User_Id'                      => $fields['userId'], 
						'Annual_Gross_Income_Amt'      => $fields['annualGrossIncome'],
						'Housing_Sit_Id'               => $fields['housingSit'],
						'Monthly_Mortgage_Payment_Amt' => $fields['monthlyMortage'],
						'Monthly_Rent_Amt'             => $rentAmount,
						'Zillow_Rent_Est_Amt'          => (isset($fields['zestimate']) && !empty($fields['zestimate']) && $fields['housingSit'] == 1) ? $fields['zestimate'] : null,
						'Zillow_API_Response_Txt'	   => (isset($fields['zestimateXML'])) ? $fields['zestimateXML'] : null,
						'Contact_Name'                 => $fields['contactName'],
						'Contact_Phone_Nr'             => $fields['contactPhoneNr'],
						'Self_Rpt_Credit_Desc' 		   => (isset($fields['creditQuality']) ? $fields['creditQuality'] : null),
						'Bureau_Credit_Score_Val'	   => (isset($fields['bureauProvidedCredit']) && !empty($fields['bureauProvidedCredit'])) ? $fields['bureauProvidedCredit'] : null,
						'Created_By_User_Id'           => $fields['userId'], 
						'Create_Dt'                    => date('Y-m-d')
					);

				// if delinquent period is present from inputs
				if(isset( $fields['delinquencyPeriod'] ))
					$updateFields['Last_Bank_Acct_Delinquency_Id'] = $fields['delinquencyPeriod'];
 
				self::where('User_Id', '=', $fields['userId'])->update( $updateFields ); 
			 
			}else{ 
				self::createApplicantFinancialInfo($fields);  
			}

		} else {
			self::createApplicantFinancialInfo($fields);
		}
	}

	/**
	 * Create Applicant Financial Info
	 * 
	 * @param  array  $fields
	 * @return
	 */
	protected static function createApplicantFinancialInfo( $fields = array() )
	{	
		if( count($fields) > 0 ) {

			$financial = new ApplicantFinancialInfo();		
			$financial->Employment_Status_Desc  = $fields['employeeStatusDescription']; 
			$financial->Employment_Status_Id    = $fields['employeeStatus'];  
			$financial->User_Id                 = $fields['userId'];
			$financial->Annual_Gross_Income_Amt = $fields['annualGrossIncome'];
			$financial->Housing_Sit_Id          = $fields['housingSit'];
			$financial->Self_Rpt_Credit_Desc    = (isset($fields['creditQuality']) ? $fields['creditQuality'] : null);
			$financial->Bureau_Credit_Score_Val = ((isset($fields['bureauProvidedCredit']) && !empty($fields['bureauProvidedCredit'])) ? $fields['bureauProvidedCredit'] : null);
			
			if( $fields['contactPhoneNr'] != '' )
				$financial->Contact_Phone_Nr = $fields['contactPhoneNr'];

			if( $fields['contactName'] != '' )
				$financial->Contact_Name = $fields['contactName'];

			if( $fields['monthlyMortage'] != '' )
				$financial->Monthly_Mortgage_Payment_Amt = $fields['monthlyMortage'];
		 
			if( $fields['rentAmount'] != '' ) 
				$financial->Monthly_Rent_Amt 	= $fields['rentAmount'];

			if(isset($fields['delinquencyPeriod']))
				$financial->Last_Bank_Acct_Delinquency_Id = $fields['delinquencyPeriod'];

			if(isset($fields['zestimate']))
				$financial->Zillow_Rent_Est_Amt     = (!empty($fields['zestimate']) && $fields['housingSit'] == 1) ? $fields['zestimate'] : null;

			if(isset($fields['zestimateXML']))
				$financial->Zillow_API_Response_Txt = $fields['zestimateXML'];

			$financial->Created_By_User_Id      = $fields['userId'];
			$financial->Create_Dt               = date('Y-m-d');
			$financial->save(); 
		}
		
	}
  	
}