<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class HousingSituation extends Eloquent{
	
	protected $table      = 'Housing_Situation';
	
	protected $primaryKey = 'Housing_Sit_Id';
	
	public $timestamps    = false;
}