<?php

class Task extends Eloquent{
	/*
	|--------------------------------------------------------------------------
	| SalesforceTask
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'Task';

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';

	/**
	 * Timestamps
	 * @var boolean
	 */
    public $timestamps = false;

    public function __construct()
    {

    }

    /**
     * Insert New Task Records
     * 
     * @param  array  $data
     * @return
     */
    public static function newTask( $data = array() )
    {
    	if( count($data) > 0 ) {
    		try{

	    		$isExist = self::where(
	    			array(
						'User_Id'     => $data['User_Id'], 
						'Loan_App_Nr' => $data['Loan_App_Nr'],
						'SF_Task_Id'  => $data['SF_Task_Id'],
	    			)
	    		)->count();

	    		foreach ($data as $key => $value) {
	    			if( empty($value) ) {
	    				$data[$key] = DB::raw("DEFAULT");
	    			}
	    		}

	    		// _pre($data);

	    		if( $isExist > 0 ) {
	    			return self::where(
	    				array(
							'User_Id'     => $data['User_Id'],  
							'Loan_App_Nr' => $data['Loan_App_Nr'], 
							'SF_Task_Id'  => $data['SF_Task_Id']
		    			)
	    			)->update( $data );
	    		} else{
	    			return self::insert($data);
	    		}
	    	}catch(Exception $e){
	    		writeLogEvent(
	    			'ODS Task Creation', 
	    			array(
	    				'Message' => $e->getMessage()
	    			)
	    		);
	    	}
    	}
    }
}