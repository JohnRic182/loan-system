<?php

class ExclusionVariable extends Eloquent{

	/**
     * table name 
     * 
     * @var string
     * @access protected
     */
	protected $table = 'Exclusion_Variable_Coefficient';

	/**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = null;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

}