<?php

class JoomlaCmsContent extends Eloquent{ 

	protected $connection = 'joomla';

	protected $table = 'ascend_content';

	protected $primaryKey = 'id';

	public $timestamps = false;

	
	/**
	 * Get Data 
	 *
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{
		if( $isRow == true )
			return JoomlaCmsContent::where($where)->select($fields)->first();

		return JoomlaCmsContent::where($where)->select($fields)->get();
	}


}