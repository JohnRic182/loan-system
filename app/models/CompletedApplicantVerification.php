<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class CompletedApplicantVerification extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| Completed Application Verification Phase Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/
	
	protected $table = 'Completed_Applicant_Verification';

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

	
	/**
	 * Get Complete Applicant Verification 
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return array
	 */
	public static function getCompletedAppVer($userId, $loanAppNr)
	{
		 
		return CompletedApplicantVerification::whereRaw("User_Id = $userId AND Loan_App_Nr = $loanAppNr")
											    ->first();
	}

	/**
	 * Set Completed Applicant Verification
	 * 
	 * @param integer $userId
	 * @param integer $loanAppNr
	 * @param integer $appVerId
	 */
	public static function setCompletedApplicantVerification($userId, $loanAppNr, $appVerId, $appVerIdValue = 'y')
	{
		$compAppVerData = CompletedApplicantVerification::where(
		 					array(
		 						 'User_Id' 	   => $userId
		 						,'Loan_App_Nr' => $loanAppNr
		 					)
		 				)->first();

 	
		 
		if(	$compAppVerData == null ) {
		 	
		 	//get the default phase 
		 	$result = LoanVerificationPhase::all();

		 	$completedAppVer = array();

		 	foreach ($result as $key => $row) {

		 		$completedAppVer[$row->Verification_Phase_Id]['id'] = $row->Verification_Phase_Id;
		 		$completedAppVer[$row->Verification_Phase_Id]['value'] = 'n'; 

		 		//set Verification step to Y if 
		 		if( $appVerId == $row->Verification_Phase_Id )
		 			$completedAppVer[$row->Verification_Phase_Id]['value'] = 'y';
		 	}

		 	//encode back to json before updating
			$completedAppVerJSON = serialize($completedAppVer);
 
			$compAppVerDataNew = new CompletedApplicantVerification(); 			
			$compAppVerDataNew->User_Id                       = $userId; 
			$compAppVerDataNew->Loan_App_Nr                   = $loanAppNr; 
			$compAppVerDataNew->Created_By_User_Id            = $userId;  
			$compAppVerDataNew->Create_Dt                     = date('Y-m-d');  
			$compAppVerDataNew->Verification_Phase_Status_Txt = $completedAppVerJSON; 	
			$compAppVerDataNew->save();
 
		} else {

			$completedAppVer = unserialize($compAppVerData->Verification_Phase_Status_Txt);

			//@override the current data and set the value to YES
			$completedAppVer[$appVerId]['value'] = $appVerIdValue;

			//encode back to json before updating
			$completedAppVerJSON = serialize($completedAppVer);
  
			$compAppVerData->where(array(
				 'User_Id' 	   => $userId
				,'Loan_App_Nr' => $loanAppNr
			))->update(array( 
				'Update_Dt' =>date('Y-m-d'),
				'Updated_By_User_Id' => $userId,
				'Verification_Phase_Status_Txt' => $completedAppVerJSON
			));
		} 
	}
}