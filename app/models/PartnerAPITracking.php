<?php

class PartnerAPITracking extends Eloquent
{
	/*
	|--------------------------------------------------------------------------
	| PartnerAPITracking Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * @var string
	 */
	protected $table 	= 'Partner_API_Tracking';
	
	/**
	 * Override Created Date
	 */
	const CREATED_AT 	= 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT 	= 'Update_Dt';

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('API_Req_Res_Txt'); //Gfauth::encryptFields($fields['API_XML_Txt']);

	protected $Created_By_User_Id;

	/**
	 * Class Constructor
	 * 
	 */
	public function __construct(){

		parent::__construct();
		Gfauth::decryptData();

		$this->Created_By_User_Id = 0;


	}


	/**
	 * save API response ore request
	 * @param  array  $data data from the response or request
	 * @return int    query reponse from db
	 */
	public function saveApiInfo($data = array()){

		$ret = 0;
		//encrypt xml data
		$data['API_Req_Res_Txt']    = Gfauth::encryptFields($data['API_Req_Res_Txt']);
		$data['Created_By_User_Id'] = $this->Created_By_User_Id;
		$data['Create_Dt']			= date('Y-m-d H:i:s');

		// _pre($data);
		if(!$this->checkExist($data))
			$ret = DB::table($this->table)->insert($data);

		return $ret;
	}


	/**
	 * get data with encyrpted fields
	 * @param  array   $param  condition for where clause
	 * @param  array   $fields fileds needed
	 * @param  boolean $isRow  if mulitple row or single row
	 * @return array          data
	 */
	public function getData($param = array(), $fields = array(), $isRow = false){

		$encryptedFieldArr  = Gfauth::encryptLargeFieldArray(self::$encryptedFields, $fields );
		
		$ret = PartnerAPITracking::where( $param )->select( $encryptedFieldArr );

		if($isRow)
			$data = $ret->first();
		else
			$data = $ret->get();

		return $data;
	}


	/**
	 * check if data exist from partner id tracking number and api description
	 * @param  array $data array from request
	 * @return boolean
	 */
	public function checkExist($data){

		$response = PartnerAPITracking::where(['Partner_Id' => $data['Partner_Id'] ])
						->where([ 'Tracking_Nr' => $data['Tracking_Nr'] ])
						->where([ 'API_Txn_Type_Desc' => $data['API_Txn_Type_Desc'] ])
						->get()->count();

		return ($response > 0)? true : false;

	}


}