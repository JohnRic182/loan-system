<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class LoanLocation extends Eloquent{

	/**
	 *  Primary Table
	 *  
	 * @var string
	 */
	protected $table      = 'Loan_Location';

}