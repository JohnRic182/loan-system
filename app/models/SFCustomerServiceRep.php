<?php

/**
 * Salesforce Customer Service Rep
 *
 * @version 1.0
 * @author  Janzell Jurill
 */

class SFCustomerServiceRep extends Eloquent{

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'SF_Customer_Service_Rep';

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';

	/**
	 * Timestamps
	 * @var boolean
	 */
    public $timestamps = false;

    /**
     * Get Default CSR Assigne 
     * 
     * @return
     */
    public static function getDefaultAssignee()
    {
    	return self::where(
    		array('Default_CSR_Flag' => 1 )
    	)->first();
    }

    /**
     * Get CSR by Type 
     * 
     * @param  string $type
     * @param  boolean $row
     * @return array
     */
    public static function getCSRByType( $type = 'CSR', $row = true ) 
    {
    	$SFCustomerServiceRep =  SFCustomerServiceRep::where(
    		array(
    			'CSR_Type_Desc' => $type
    		)
    	);

    	if( $row == true )
    		return $SFCustomerServiceRep->first();

    	return $SFCustomerServiceRep->get();
    }

}