<?php

class ApplicantContactInfo extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| AffordableLoanFactor Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'Applicant_Contact_Info';

	/**
	 * Primary key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

 
	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Home_Phone_Nr', 'Mobile_Phone_Nr');


	/**
	 * Class Constructor
	 * 
	 */
	public function __construct()
	{
		parent::__construct();

		Gfauth::decryptData();
	}


	/**
	 * Get Data 
	 *
	 * @todo   remove the old getData and rename this one
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

		if( $isRow == true )
			return ApplicantContactInfo::where($where)->select($encryptedFieldArr)->first();

		return ApplicantContactInfo::where($where)->select($encryptedFieldArr)->get();
	}


	/**
	 * Save ApplicantContactInfo data
	 *
	 * @return string
	 */
	public function saveApplicantContact($fields = array(), $updateFlag = 0)
	{
		if($updateFlag){

			$applicantContactCnt = ApplicantContactInfo::where('User_Id', '=', $fields['userId'])
            						 ->get()
            						 ->count();

            if( $applicantContactCnt > 0 ) {

            	if( !isset( $fields['mobilePhoneNr'] ) )
					$fields['mobilePhoneNr'] = DB::raw('DEFAULT');
 
				$result = ApplicantContactInfo::where('User_Id', $fields['userId'])
	            								->update(
	            								array(
							            			'Home_Phone_Nr'		 => Gfauth::encryptFields($fields['homePhoneNumber']),
							            			'Mobile_Phone_Nr'	 => Gfauth::encryptFields($fields['mobilePhoneNr']),
													'Validity_Start_Dt'  => DB::raw("CAST(GETDATE() AS DATE)"),
													'Created_By_User_Id' => $fields['userId'],
													'Create_Dt'			 => DB::raw("CAST(GETDATE() AS DATE)")
	            								));
	        } else {
	        	$result = $this->newApplicantContact($fields);	
	        }

		}else{
			$result = $this->newApplicantContact($fields);
		}
		return $result;
	}

	/**
	 * New Applicant Contact Info
	 * 
	 * @param  array  $fields
	 * @return
	 */
	public function newApplicantContact( $fields = array() )
	{
		if( count($fields) > 0 ) {

			if( !isset( $fields['mobilePhoneNr'] ) )
				$fields['mobilePhoneNr'] = DB::raw('DEFAULT');
 
			$result = ApplicantContactInfo::insert(
										    array(
										    	'User_Id'          	 => $fields['userId'],
												'Home_Phone_Nr'		 => Gfauth::encryptFields($fields['homePhoneNumber']),
												'Mobile_Phone_Nr'	 => Gfauth::encryptFields($fields['mobilePhoneNr']),
												'Validity_Start_Dt'  => DB::raw("CAST(GETDATE() AS DATE)"),
												'Created_By_User_Id' => $fields['userId'],
												'Create_Dt'			 => DB::raw("CAST(GETDATE() AS DATE)")
										    	)
										);

			return $result;
		}
	}

}