<?php

class NlsCreditProfileSSN extends Eloquent{

	protected $table = 'NLS_CreditProfileSSN';

	protected $primaryKey = 'CreditProfileSSNID';

	public $timestamps = false;	

}