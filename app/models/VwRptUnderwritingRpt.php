<?php
/**
 * exclusion report table
 *
 * @version  0.1
 */

class VwRptUnderwritingRpt extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'vw_Rpt_Underwriting_Rpt';
 

 	protected $primaryKey = 'User_Id';


	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		Gfauth::decryptData();

	}


	public function getExclusiontData($startDate, $endDate){
		
		$data = DB::table($this->table)
		->whereBetween('Create_Dt', array( $startDate, $endDate ))->orderBy('Create_Dt')->get();
		
		return $data;
	}	

	 
}