<?php

class NlsCreditLegalItem extends Eloquent{

	protected $table = 'NLS_CreditLegalItem';

	protected $primaryKey = 'CreditLegalItemID';

	public $timestamps = false;
}