<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class LoanVerificationPhase extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| Loan Verification Phase Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/
	
	protected $table      = 'Loan_Verification_Phase';
	
	protected $primaryKey = 'Verification_Phase_Id';

}