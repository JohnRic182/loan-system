<?php

class ExclusionVersion extends Eloquent{

	/**
     * table name 
     * 
     * @var string
     * @access protected
     */
	protected $table = 'Exclusion_Version';

	/**
     * primaryKey 
     * 
     * @var integer
     * @access protected
     */
    protected $primaryKey = null;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    /**
     * Get Credit Exclusions
     * 
     * @param  integer $userId
     * @return
     */
    public static function getCreditExclusion($userId)
    {
        $exclusions = array();

        $latestVersion = DB::select("SELECT TOP 1 Exclusion_Ver_Nr 
                                     FROM ODS.dbo.Exclusion_Algorithm 
                                     WHERE Validity_End_Dt IS NULL 
                                     ORDER BY Exclusion_Ver_Nr DESC, 
                                     Create_Dt DESC" 
                                );

        if( count( $latestVersion ) > 0 ) { 
            
            $exclusionCodes = DB::select("EXEC ODS.dbo.usp_Exclusion_AA_Cd_Desc ?,?,?,?", 
                                array( $latestVersion[0]->Exclusion_Ver_Nr, 'Prtnr', 'CA', $userId ) 
                              );
        
            //@todo - we need to use batch query for exclusion statement
            if( count($exclusionCodes) > 0){        
                foreach($exclusionCodes as $exclusionCode){
                    $adverseReason = AdverseReason::where('Reason_Cd', '=', $exclusionCode->Adverse_Action_Cd)->first();
                    array_push($exclusions, $adverseReason->Reason_Statement_Txt);
                }
            }
        }


        return $exclusions;
    }

    /**
     * Get Credit Exclusions
     * 
     * @param  integer $userId
     * @return
     */
    public static function getUsersForReprocess()
    {
        $exclusions = array();
        $exclusions = DB::select("SELECT * from APPLICANT_REPROCESS");
        // $exclusions = DB::select("SELECT DISTINCT ae.Applicant_User_Id
        //      , ld.Funding_Partner_Id
        //      , ae.Loan_App_Nr
        //      , ld.Loan_Status_Desc
        //      , lad.Loan_App_Phase_Nr
        //      , CASE
        //       WHEN lad.Loan_App_Phase_Nr <= 5 THEN 'initial'
        //       WHEN lad.Loan_App_Phase_Nr >= 6 THEN 'final'
        //       ELSE 'no exclusion'
        //        END AS Exclusion_Type_Desc
        //      , ae.Create_Dt
        //     FROM ODS.dbo.Applicant_Exclusion ae
        //     INNER JOIN
        //     (
        //      SELECT DISTINCT Loan_App_Nr
        //       , Applicant_User_Id
        //       , MAX(Loan_App_Phase_Nr) OVER(PARTITION BY Applicant_User_Id, Loan_App_Nr) AS Loan_App_Phase_Nr
        //      FROM ODS.dbo.Loan_Application_Detail
        //     ) lad ON ae.Applicant_User_Id = lad.Applicant_User_Id
        //      AND ae.Loan_App_Nr = lad.Loan_App_Nr
        //     INNER JOIN
        //     (
        //      SELECT DISTINCT Loan_App_Nr
        //       , Borrower_User_Id
        //       , Funding_Partner_Id
        //       , Loan_Status_Desc
        //      FROM ODS.dbo.Loan_Detail
        //     ) ld ON ae.Applicant_User_Id = ld.Borrower_User_Id
        //      AND ae.Loan_App_Nr = ld.Loan_App_Nr
        //     WHERE ae.Exclusion_Ver_Nr = '3.6';
        // " );    

        return $exclusions;
    }


}