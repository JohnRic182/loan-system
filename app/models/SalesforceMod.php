<?php

/**
 * Salesforce Database Operations
 *
 * @version 1.0
 * @author  Janzell V. Jurilla
 */
class SalesforceMod{


	public function __construct()
	{
	}

	/**
	 * Import Account Object
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  string $type
	 * @param  string $accountObjId
	 * @return array
	 */
	public function importAccountObj( $userId, $loanAppNr, $type = 'create', $accountObjId = "")
	{
		$account =  array(); 

		$fields = SalesForceMap::getSFFieldsBySFObjectName('Account');
		$option = array( 'keyVal' => $userId );
		//Remapped Fields between Salesforce and ODS
		$orderedFields = $this->mapDefinedFields($fields, $option );
  
		//Convert Stdclass Object to Array Objects
		$obj = $this->remapStdClassObj($orderedFields);
  
		if( count($obj) > 0 ) {
			//Assign User Id and Loan Id for Account Object
			$obj['Account']->User_ID__c = $obj['All']->User_ID__c;
			$obj['Account']->Loan_ID__c = $obj['All']->Loan_ID__c; 

			$personBirthdate = date_create($obj['Account']->PersonBirthdate);
			$obj['Account']->PersonBirthdate =  date_format( $personBirthdate, 'Y-m-d');


			if( $type == 'update') {

				//Assign Account Object Id
				$obj['Account']->Id = $accountObjId;

				//Create Account Object
				$account = Salesforce::update(
					array($obj['Account']), 'Account'
				);
			}else{
				//Create Account Object
				$account = Salesforce::create(
					array($obj['Account']), 'Account'
				);	
			} 
		}

		return $account;
	}

	/**
	 * Prepare Loan Object
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return array
	 */
	public function prepareLoanObj( $userId, $loanAppNr )
	{
		//Get All Fields
		$fields = SalesForceMap::getSFFieldsBySFObjectName('Loan_Application__c');

		$option = array( 'keyVal' => $userId );

		//Remapped Fields between Salesforce and ODS
		$orderedFields = $this->mapDefinedFields($fields, $option );
 
		//Convert Stdclass Object to Array Objects
		$obj = $this->remapStdClassObj($orderedFields);
 		
 		return $obj;
 	}
 	
	/**
	 * Create / Update Salesforce Loan Object
	 *
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  integer $accountId
	 * @param  string  $loanAppSFId
	 * @return array
	 */
	public function importSFLoanObj( $userId, $loanAppNr, $accountId = "", $loanAppSFId = "" )
	{
		
		$obj = $this->prepareLoanObj( $userId, $loanAppNr );
		 
		if( count($obj) > 0 && !empty($accountId) ) {

			try {
 	

				$fullName = '';

				if( isset($obj['Loan_Application__c']->First_Name) 
					&& isset($obj['Loan_Application__c']->Last_Name ) ) {
					$fullName =  $obj['Loan_Application__c']->First_Name . ' ' .  $obj['Loan_Application__c']->Last_Name;
					
					//Special condition for name
					$obj['Loan_Application__c']->Name = $fullName;

					unset($obj['Loan_Application__c']->First_Name);
					unset($obj['Loan_Application__c']->Last_Name);
				}

				//Filter empty fields
				foreach ($obj['Loan_Application__c'] as $key => $value) {			
					if( empty($value) ) {
						unset($obj['Loan_Application__c']->$key);			
					}
				}

				if( !empty( $loanAppSFId ) ) {
					//Update Loan Object
					$obj['Loan_Application__c']->Id = $loanAppSFId;

					//Create Loan Object
					$loan = Salesforce::update(
						array($obj['Loan_Application__c']), 'Loan_Application__c'
					);
				}else {

					//Assign User_Id and Loan_Id
					$obj['Loan_Application__c']->User_ID__c = $obj['All']->User_ID__c;
					$obj['Loan_Application__c']->Loan_ID__c = $obj['All']->Loan_ID__c;

					$obj['Loan_Application__c']->ACCOUNT__c =  $accountId;

					//Create Loan Object
					$loan = Salesforce::create(
						array($obj['Loan_Application__c']), 'Loan_Application__c'
					);
				}

				return $loan;

	        } catch (Exception $e) {
	            return $e->getMessage();
	        }	
		} else{
			return 'Fields object is empty.';
		}
	}

	/**
	 * Update Account Object
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  string $accountObjId
	 * @return array
	 */
	public function updateAccount( $userId, $loanAppNr, $accountObjId )
	{
		return $this->importAccountObj($userId, $loanAppNr, 'update', $accountObjId );
	}

	/**
	 * Create Account Object
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return object
	 */
	public function createAccount( $userId, $loanAppNr )
	{	
		return $this->importAccountObj($userId, $loanAppNr, 'create' );
	}

	/**
	 * Create Contact Object in the Salesforce
	 *
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return array
	 */
	public function createContact( $userId, $loanAppNr )
	{	
		$result = array(); 

		$fields = SalesForceMap::getSFFieldsBySFObjectName('Contact');

		$option = array( 'keyVal' => $userId );

		//Remapped Fields between Salesforce and ODS
		$orderedFields = $this->mapDefinedFields($fields, $option );
 
		$obj = $this->remapStdClassObj($orderedFields);

		if( count($obj) > 0 ) {
			
			//Format Birthdate
			$birthdate = date_create($obj['Contact']->Birthdate);
			$obj['Contact']->Birthdate =  date_format( $birthdate, 'Y-m-d');  

			//Create Contact Object
			$result =  Salesforce::create(
				array($obj['Contact']), 'Contact'
			);
		}

		return $result;
	}

	/**
	 * Create Verification Object
	 *
	 * @param  integer $accountId
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  string  $type
	 * @param  array   $option
	 * @return
	 */
	public function createVerification( $accountId, $userId, $loanAppNr, $type, $param = array() )
	{	
		$result = array();

		$fields = SalesForceMap::getSFFieldsBySFObjectName($type);
  
		if( count($fields) > 0 ) {

			$option = array( 'keyVal' => $userId );

			$orderedFields = $this->mapDefinedFields($fields, $option );
			
			// pre($orderedFields);

			$obj =  $this->remapStdClassObj($orderedFields);
 
			if( isset($obj[$type]->Loan__c) )
				$obj[$type]->Loan__c =  $accountId; 

			if( isset($obj[$type]->LoanApp__c ) ) 
				$obj[$type]->LoanApp__c =  $accountId;

 			if( isset($obj[$type]->Loan_App__c ) ) 
				$obj[$type]->Loan_App__c =  $accountId;

			//Check for other options parameters
			if( count($param) > 0 ) {
				foreach ($param as $i => $g) {
					$obj[$type]->$i = $g;
				}
			}
 			
 			// _pre($obj);

			//Filters
			$obj = $this->filterFilesURL($obj, $type );

			//Condition for Employment Verification Only 
			if( isset( $obj['Employment_Verification__c'] ) &&
				empty( $obj['Employment_Verification__c']->Name) ){
				$obj['Employment_Verification__c']->Name = 'Self Employed';
			} 
 
			//Filters for Income Verification 
			if( isset( $obj['Income_Verification__c']->Employment_Type__c ) ){
				if( $obj['Income_Verification__c']->Employment_Type__c == 'SelfEmployed' ) {
					unset($obj['Income_Verification__c']->Employee_Paystub_1__c);
					unset($obj['Income_Verification__c']->Employee_Paystub_2__c);
				}else{
					unset($obj['Income_Verification__c']->Self_Employed_1099__c);
				}
			} 

			// pre($obj);

			if ( isset($obj[$type]->LoanApp__c) && $obj[$type]->LoanApp__c != "" ) {
				// Create Account Object
				$result = Salesforce::create(
					array($obj[$type]), $type
				);
			}else{
				return array(
					'User_Id' => $userId, 
					'Message' => "No match"
				);
			}
 
		}

		return $result;
	}

	/**
	 * Filter Verification Documents
	 * 
	 * @param  array  $obj
	 * @param  string $type
	 * @return array
	 */
	public function filterFilesURL( $obj = array(), $type = '' )
	{

		$fields = array(
			'Employee_Paystub_1__c', 
			'Employee_Paystub_2__c', 
			'Self_Employed_1099__c',
			'Statement_1__c', 
			'Statement_2__c', 
			'Statement_3__c', 
			'ID_Image__c',
			'Check_image_URL__c'
		);

		foreach ($fields as $key => $value) {
			if( isset($obj[$type]->$value ) ) {
				
				$image = json_decode($obj[$type]->$value);

				$obj[$type]->$value = isset($image[0]) ? urldecode($image[0]) : "";

				if( $value == 'Statement_1__c' ) 
					$obj[$type]->$value = isset($image[0]) ? urldecode($image[0]) : "";

				if( $value == 'Statement_2__c' ) 
					$obj[$type]->$value = isset($image[1]) ? urldecode($image[1]) : "";

				if( $value == 'Statement_3__c' ) 
					$obj[$type]->$value = isset($image[2]) ? urldecode($image[2]) : "";

				if( $value == 'Employee_Paystub_2__c' ) 
					$obj[$type]->$value = isset($image[1]) ? urldecode($image[1]) : "";
			}
		}

		return $obj;
	}

	/**
	 * Check Whether the Record Exist
	 *
	 * @param  
	 * @return boolean
	 */
	public function isRecordExist( $objectName, $accountId )
	{
		$query = "SELECT Id FROM $objectName WHERE LoanApp__c = '". $accountId ."'";
		$salesforce = Salesforce::query($query);
		return $salesforce->size;
	}

	/**
	 * Get Verification Data by Account Id
	 * 
	 * @param  string $verObjectName
	 * @param  string $accountId
	 * @return
	 */
	public function getVerificationDataByAccountId( $verObjectName, $accountId )
	{
		$query = "SELECT Id FROM $verObjectName WHERE LoanApp__c = '". $accountId ."'";
		$salesforce = Salesforce::query($query);

		return $salesforce;
	}

	/**
	 * Remapped of Std Class Object to Array Objects
	 * 
	 * @param  array  $orderedFields
	 * @return array
	 */
	public function remapStdClassObj( $orderedFields = array() )
	{
		$obj = array();	
	 	
	 	//Remap result into std class objects
	 	if( is_array($orderedFields) ) {

			foreach ($orderedFields as $key => $value) {

				$obj[$key] = new stdClass; 

				foreach ($value as $d => $g) {
 					//Convert Object to Array
 					if( is_object($g) )
 						$g = (array) $g;

 					//Assign values and key to new stdclass object
 					if( is_array($g) ) {
 						foreach ($g as $m => $v) {
 							$obj[$key]->$m = $v;
						}
 					} 
				} 
			}
		}
 
		return $obj;
	}

	/**
	 * Map Salesforce Defined Fields 
	 * 
	 * @param  array  $fields
	 * @param  array  $option
	 * @return array
	 */
	public function mapDefinedFields( $fields = array(), $option = array() )
	{	
 
		$orderedFields = array();

		if( count($fields) > 0 ) {

			//Call Decryption
			Gfauth::decryptData();

			foreach ($fields as $key => $field) {  
 
				//Generate associative array for SF field names
				if( !in_array($field['SF_Field_Name'], $orderedFields) ){ 
					
					//Shows only active fields
					if( $field['Is_Active_Flag'] == 1 ) {
						
						$SFFieldName = $field['SF_Field_Name'];
						$ODSFieldName = $field['ODS_Field_Name'];
						$ODSFieldNameArr = explode(',', $ODSFieldName );
 
						//Format Encrypted Field
						if( $field['Is_Encrypted_Flag'] == 1 ) {
							$decryptField = Gfauth::decryptLongField( 
												$ODSFieldName, 
												$SFFieldName 
											);

							$orderedFields[$SFFieldName]['ODS_Field_Name'] = $decryptField;	
						}else{ 
							$orderedFields[$SFFieldName]['ODS_Field_Name'] = DB::raw("$ODSFieldName AS $SFFieldName");
						} 

						//Set Key Field Id
						$orderedFields[$SFFieldName]['keyField'] = $field['ODS_Key_Field_Name'];
						$orderedFields[$SFFieldName]['ODS_Table_Name'] = $field['ODS_Table_Name'];
						$orderedFields[$SFFieldName]['SF_Object_Name'] = $field['SF_Object_Name']; 
					}
				} 
			
			}
		} 
 		  
		$keyFieldTemp   = array( 'Payment_Method_Id' => 'Payment_Method_Type' ); 
		$keyFieldValues = array(); 

		$result = array();

		$salesforceObj = array();

		if( count($orderedFields) > 0 ) {

			foreach ($orderedFields as $key => $value) {

				$tableName = $value['ODS_Table_Name'];
				$multipleFields = explode(',', $value['SF_Object_Name']); 
				$sfObjectName = $value['SF_Object_Name'];

				if( count($multipleFields) > 1 ) {

					foreach ($multipleFields as $mField) {
						$salesforceObj[$mField][$tableName]['fields'][]     = $value['ODS_Field_Name'];
						$salesforceObj[$mField][$tableName]['sfKey'][]      = $key;
						$salesforceObj[$mField][$tableName]['key']          = $value['keyField'];
						$salesforceObj[$mField][$tableName]['sfObjectName'] = $value['SF_Object_Name'];
					} 

				} else {

					$salesforceObj[$sfObjectName][$tableName]['fields'][]     = $value['ODS_Field_Name'];
					$salesforceObj[$sfObjectName][$tableName]['sfKey'][]      = $key;
					$salesforceObj[$sfObjectName][$tableName]['key']          = $value['keyField'];
					$salesforceObj[$sfObjectName][$tableName]['sfObjectName'] = $value['SF_Object_Name'];
				} 
			}

			$keyValue     = $option['keyVal'];
 
			foreach ($salesforceObj as $k => $tables) {
				foreach ($tables as $i => $table) { 
					$result[$k][$i] = DB::table($i)->select($table['fields'])
										->where(array($table['key'] => $keyValue))
										->first();  
 
				}
				 
			}
		}
  
		return $result;
	}

	/**
	 * Create Verification Object
	 * 
	 * @param  integer $userId
	 * @param  string $verificationType
	 * @return array
	 */
	public function createVerObject( $userId , $verificationType = 'Identity_Verification__c')
	{
		$verification = array();

		$query = "SELECT Id, User_ID__c FROM Loan_Application__c WHERE User_ID__c = $userId";
		$q = Salesforce::query($query);
		
		if( count($q->records) > 0 ) {
			
			foreach ($q->records as $key => $value) {
				 
				$loanAppNr = '';
				$accountId = $value->Id;

				$isRecordExist = $this->isRecordExist($verificationType, $accountId);

				// _pre($isRecordExist);

				if( $isRecordExist == 0 ){

					$verification[$userId] = $this->createVerification(
						$accountId, 
						$userId, 
						$loanAppNr, 
						$verificationType 
					);
				}else{
					$verification[$userId] = $this->getVerificationDataByAccountId( $verificationType, $accountId );
					_pre($verification);
				}
			}
		} else {
			writeLogEvent( $verificationType, 
				array(
					'Loan_App_Nr' => $loanAppNr, 
					'User_Id' => $userId, 
					'No Loan Application found'
				),
				'warning'
			);
		}

		return $verification;
	}

	/**
	 * Get Account Ids
	 * 
	 * @param  array  $userIds
	 * @return
	 */
	public function getAccountIds( $userIds = array() ) 
	{

		$strUserIds = implode(',', $userIds );
		$query = "SELECT Id FROM Account WHERE User_Id__c IN ( $strUserIds ) ";
		$salesforce = Salesforce::query($query);
 
		return $salesforce->records;
	}

	/**
	 * Get Account Id By User Id
	 * 
	 * @param  integer $userId
	 * @return string
	 */
	public function getAccountId( $userId )
	{

		$id = ""; 
		$query = "SELECT Id FROM Account WHERE User_Id__c = $userId ";
		$salesforce = Salesforce::query($query);

		if( count( $salesforce->records ) > 0 ){
			$id = $salesforce->records[0]->Id;
		}
		
		return $id;
	}

	/**
	 * Get SF Loan Application Id 
	 * 
	 * @param  integer $userId
	 * @return
	 */
	public function getSFLoanApplicationId( $userId )
	{
		$id = "";

		$query = "SELECT Id, User_ID__c FROM Loan_Application__c WHERE User_ID__c = $userId";
		$q = Salesforce::query($query);

 		if( count( $q->records ) > 0 ){
			$id = $q->records[0]->Id;
		}
		
		return $id;
	}

	/**
	 * Add new Task 
	 *
	 * @return
	 */
	public function newTask( $data  = array() , $loanAppSFId = '')
	{

		try{

			$task = new stdClass();

			$task->Subject              = $data['Subject_Txt']; 
			$task->Status               = $data['Task_Status_Desc']; 
			$task->Description          = $data['Comments_Txt'];
			$task->WhatId               = $loanAppSFId;
			$task->Stipulate_Title__c   = $data['Subject_Txt']; 
			$task->Verification_Step__c = $data['Task_Type_Desc'];

			if( isset($data['Task_Doc_Upload_URL_Txt']) )
				$task->Document_Name__c = $data['Task_Doc_Upload_URL_Txt'];

			if( isset($data['Reqd_File_Cnt']) )
				$task->Reqd_File_Cnt__c = $data['Reqd_File_Cnt']; 

			if( isset($data['NLS_Task_Id']) )
				$task->NLS_Task_Id__c   = $data['NLS_Task_Id'];

  
			return Salesforce::create(
				array($task), 'Task'
			);

		}catch( Exception $e ) {

			writeLogEvent('SF Task Creation', 
				array('message' => $e->getMessage() ) 
			);

		}
	}
}