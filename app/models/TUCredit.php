<?php

class TUCredit extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'TU_Credit_Attribute';

	/**
	 * Primary key
	 * @var string
	 */
	protected $primaryKey = 'User_Id';

	/**
	 * Timestamps
	 * @var boolean
	 */
	public $timestamps = false;

	/**
     * Is Credit Profile Exist
     *  
     * @param  integer  $userId
     * @param  integer  $loanAppNr
     * @return boolean
     */
    public static function isTUCreditExist( $loanAppNr, $userId )
    {
    	return TUCredit::where('User_Id', '=', $userId )
    			 	->where('Loan_App_Nr', '=', $loanAppNr )
    				->get()  
    				->count();
    }
}