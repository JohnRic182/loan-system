<?php

class FinancialOrganization extends Eloquent {

	protected $table = 'dbo.Financial_Organization';
 
	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
 

	/**
	 * Get User Data
	 * 
	 * @param  array  $fields
	 * @return array
	 */
  	public function getData(  $where = array(), $fields = array(), $limit = 0 )
  	{	

  		$results = array(); 

  		try{
 
			$finOrg =  FinancialOrganization::Where($where)->select($fields);
	 
			if( $limit != 0)
				$finOrg->take($limit);

			return $finOrg->get(); 
	 

		}catch(Exception $e){
			print_r($e->getMessage());
		}
		 
  	} 

  	/**
  	 * Get Top Banks
  	 * 
  	 * @param  integer $limit
  	 * @return array
  	 */
  	public static function getTopBanks( $fields = array(), $limit = 10 ) 
  	{
  		$results = array(); 

  		try{
  
			$topBanks  =  DB::select('SELECT TOP 10 Site_Id
					, Site_Name
					, Content_Svc_Id
					, Popularity_Idx_Val
					, Rank_Nr
					, Image_Url_Txt
				FROM
				(
					SELECT ROW_NUMBER() OVER(PARTITION BY Site_Id, Rank_Nr ORDER BY Popularity_Idx_Val DESC) AS Row_Nr
						, Site_Id
						, Site_Name
						, Content_Svc_Id
						, Popularity_Idx_Val
						, Rank_Nr
						, Image_Url_Txt
					FROM ODS.dbo.Financial_Organization
					WHERE Rank_Nr IS NOT NULL
				) fin_org
				WHERE Row_Nr = 1
				ORDER BY Rank_Nr');
 
 			return $topBanks;
 
   		 
  		}catch(Exception $e){
  			return $e->getMessage();
  		}
  	}

  	/**
  	 * Get FinancialOrganization Organization 
  	 * 
  	 * @param  string  $organizationName
  	 * @param  integer $limit
  	 * @param  string  $fields
  	 * @return object
  	 */
  	public static function getFinancialOrganization( $organizationName = "", $limit = 0, $fields = array() )
  	{
  		$results = array();
 
  		try{
 	
  			//@todo We need to update this db select
			$banksSearch  =  DB::select("SELECT Site_Id
				, Site_Name
				, Content_Svc_Id
				, Popularity_Idx_Val
				, Rank_Nr
				, Image_Url_Txt
			FROM
			(
				SELECT ROW_NUMBER() OVER(PARTITION BY Site_Id ORDER BY Popularity_Idx_Val DESC) AS Row_Nr
					, Site_Id
					, Site_Name
					, Content_Svc_Id
					, Popularity_Idx_Val
					, Rank_Nr
					, Image_Url_Txt
				FROM ODS.dbo.Financial_Organization
				WHERE Site_Name LIKE '%$organizationName%'
			) fin_org
			WHERE Row_Nr = 1
			ORDER BY Popularity_Idx_Val DESC");	

			return $banksSearch;

		}catch(Exception $e){
			return $e->getMessage();
		}
  	}
 
}