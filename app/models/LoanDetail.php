<?php

class LoanDetail extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'Loan_Detail';

	/**
	 * Timestamps
	 * @var boolean
	 */
	public $timestamps = false;	

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'Loan_Id';

	/**
	 * Class Constructor
	 * 
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Get Loan Detail Data
	 * 
	 * @param  array   $fields
	 * @param  array   $where
	 * @param  boolean $isRow
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{ 
		if( $isRow == true )
			return LoanDetail::where($where)->select($fields)->first();

		return LoanDetail::where($where)->select($fields)->get();
	}

	/**
	 * Get Loan Detail when Applicant Logged In
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  array  $fields
	 * @return array
	 */
	public static function getLoanDetail( $userId , $loanAppNr, $fields = array() )
	{
		$loanDetail =  LoanDetail::where('Borrower_User_Id', '=', $userId)
								->where('Loan_Id', '=', $loanAppNr);
		if( count($fields) > 0 )
			$loanDetail->select($fields);
		else
			$loanDetail->select('Loan_Prod_Id', 'Loan_Amt', 'Loan_Status_Desc');

		return $loanDetail->first();	
	}
	

	/**
	 * Is Bank Acct Linked
	 * 
	 * @param  integer  $userId
	 * @param  integer  $loanAppNr
	 * @return array
	 */
	public static function isBankAcctLinked( $userId, $loanAppNr )
	{
		$bankAcctLinked = LoanDetail::where( 
				array(
					'Borrower_User_Id' => $userId, 
					'Loan_Id'          => $loanAppNr
				)
			)->select('Bank_Acct_Linked_Flag')
			 ->first();

		$isBankAcctLinked = 'N';
	 
		if( isset( $bankAcctLinked->Bank_Acct_Linked_Flag) && $bankAcctLinked->Bank_Acct_Linked_Flag == 'Y' ) {
			$isBankAcctLinked = $bankAcctLinked->Bank_Acct_Linked_Flag;
		}

		return $isBankAcctLinked;
	}
  
	/**
	 * Is Bank Acct Linked
	 * 
	 * @param  integer  $userId
	 * @param  integer  $loanAppNr
	 * @return array
	 */
	public static function updateBankAcctLinked( $userId, $loanAppNr , $flag = 'N' )
	{
		return LoanDetail::where( 
				array(
					'Borrower_User_Id' => $userId, 
					'Loan_Id'          => $loanAppNr
				)
			)->update(
				array(
					'Bank_Acct_Linked_Flag' => $flag
				)
			);
	}
 	
 	/**
 	 * Get Approved Loans
 	 * 
 	 * @param  integer $userId
 	 * @param  integer $loanAppNr
 	 * @return
 	 */
 	public static function getApprovedLoans( $userId, $loanAppNr )
 	{
		return LoanDetail::where('Borrower_User_Id', '=', $userId)
						  ->where('Loan_Id','=',$loanAppNr)
						  ->where('Loan_Status_Desc','=','LOAN_APPROVED')
						  ->first();
 	}

	/** 
	 * Update Loan Referrence Number
	 * 
	 * @return
	 */
	protected static function updateNLSRef()
	{

		//Get all loans where Loan_Ref_Nr is empty
		$loanDetail = LoanDetail::where(
				'Loan_Ref_Nr', NULL
			)->select('Loan_Id')
		->get();

		$loans = array();

		if (count( $loanDetail ) > 0 ) {
			foreach ($loanDetail as $key => $value) {
				$loans[] = $value->Loan_Id;
			}
		}

		//Get corresponding Acctrefno ( Loan_Ref_Nr ) from NLS 
		$nls = DB::connection('nls')->table('loanacct')->whereIn(
			'loan_number', 
			$loans	
		)
		->select('acctrefno', 'cifno', 'loan_number')
		->get();

		//Update Loan Detail
		if (count( $nls ) > 0 ) {
			foreach($nls as $key => $val ) {

				LoanDetail::where(
					'Loan_Id', '=', $val->loan_number
				)->update(
					array(
						'Loan_Ref_Nr' => $val->acctrefno
					)
				);
			}
		}
	}

	/**
	 * Update Or Create Loan Detail
	 * 
	 * @param  array  $fields
	 * @param  integer $loanAppNr
	 * @param  integer $updateFlag
	 * @return
	 */
	protected static function updateOrCreateLoanDetail( $fields = array() , $loanAppNr , $updateFlag )
	{
		//Update Loan Detail Table
		if( $updateFlag && !empty( $loanAppNr ) ){

			//Check if Loan Detail Record Exist
			$loanDetailCnt =  self::where('Borrower_User_Id', '=', $fields['userId'] )
						->where('Loan_Id', '=', $loanAppNr )
						->get()
						->count();
			if( $loanDetailCnt > 0 ){

				self::where('Borrower_User_Id', '=', $fields['userId'] )
						->where('Loan_Id', '=', $loanAppNr )
						->update(array(
							'Loan_Amt' 	   => (int)$fields['loanAmount'],
							'Loan_Prod_Id' => $fields['loanProductId']
						));
		 	} else {
				self::createLoanDetail($loanAppNr, $fields);
			}

		} else { 
			self::createLoanDetail($loanAppNr, $fields);
		}
	}

	/**
	 * Create Loan Detail Data
	 * 
	 * @param  integer $loanAppNr
	 * @param  array  $fields
	 * @return 
	 */
	public static function createLoanDetail( $loanAppNr, $fields = array() )
	{
		if( !empty($loanAppNr) && count($fields) > 0 ) {
			
			$loanDetail = new LoanDetail();

			//if partner session has data
			if( isset($fields['Funding_Partner_Id'] ))
				$loanDetail->Funding_Partner_Id = $fields['Funding_Partner_Id'];

			if( isset($fields['Campaign_Id'] ) )
				$loanDetail->Campaign_Id = $fields['Campaign_Id'];

			 
			$loanDetail->Borrower_User_Id   = $fields['userId'];
			$loanDetail->Loan_Id            = $loanAppNr;
			$loanDetail->Loan_Portfolio_Id  = $fields['nlsLoanPortfolio'];
			$loanDetail->Loan_Grp_Id        = $fields['nlsLoanGroup'];
			$loanDetail->Loan_Template_Id   = $fields['nlsLoanTemplate'];
			$loanDetail->Loan_Prod_Id       = $fields['loanProductId'];
			$loanDetail->Loan_App_Nr        = $loanAppNr;
			$loanDetail->Loan_Amt           = (int)$fields['loanAmount'];
			$loanDetail->Loan_Status_Desc   = 'APPLICATION_STEP1';
			$loanDetail->Created_By_User_Id = $fields['userId'];
			$loanDetail->Create_Dt          = date('Y-m-d');

			$loanDetail->save();
		}
	}

	/**
	 * Is User Have Loan App 
	 * 
	 * @param  integer  $userId
	 * @return boolean
	 */
	public static function isUserHaveLoanApp( $userId =  NULL )
	{
		return LoanDetail::where(
			array(
				'Borrower_User_Id' => $userId
			)
		)->count();
	}

	/**
	 * Is Imported in Salesforce
	 * 
	 * @param  integer  $userId
	 * @param  integer  $loanAppNr
	 * @return boolean
	 */
	public static function isImportedInSalesforce( $userId, $loanAppNr )
	{
		return LoanDetail::where(
			array(
				'Borrower_User_Id' => $userId, 
				'Loan_Id' => $loanAppNr
			)
		)
		->select('Salesforce_Data_Import_Flag')
		->first();
	}

	/**
	 * Update SF Import Flag
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return
	 */
	public static function updateSFImportFlag( $userId, $loanAppNr )
	{
		//Update Loan Detail Salesforce Flag
		return LoanDetail::where(
					array(
						'Borrower_User_Id' => $userId,
						'Loan_Id' => $loanAppNr
					)
				)
				->update(
					array(
						'Salesforce_Data_Import_Flag' => 'Y'
					)
				);
	}

	/**
	 * Get Loan Status Desc
	 * 
	 * @param  array  $data
	 * @return
	 */
	protected static function getStatus( $data = array() )
	{
		$result = "";

		$loanDetail = LoanDetail::where(
					array(
						'Borrower_User_Id' => $data['User_Id'],
						'Loan_Id' => $data['Loan_Id']
					)
				)->select('Loan_Status_Desc')
				->first();

		if( count($loanDetail) > 0 ) 
			$result = $loanDetail->Loan_Status_Desc;

		return $result;
	}

	/**
	 * Get Loan Reference Number
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return
	 */
	public static function getLoanRefNr( $userId, $loanAppNr )
	{

		echo $userId;
		echo $loanAppNr;

		//Get Loan Reference Number
		return LoanDetail::select('Loan_Ref_Nr')->where(
			array(
				'Borrower_User_Id' => $userId,
				'Loan_Id' => $loanAppNr
			)
		)->first();
	}

	/**
	 * Application  Count
	 * 
	 * @param  integer $userId
	 * @param  integer $loanId
	 * @return
	 */
	public static function applicationCnt( $userId, $loanId )
	{
		return LoanDetail::where(
		array(
			'Borrower_User_Id' => $userId, 
			'Loan_Id' => $loanId
		))->count();
	}
}