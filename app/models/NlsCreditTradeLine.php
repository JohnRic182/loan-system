<?php

class NlsCreditTradeLine extends Eloquent{

	protected $table = 'NLS_CreditTradeLine';

	protected $primaryKey = 'CreditTradeLineID';

	public $timestamps = false;

}