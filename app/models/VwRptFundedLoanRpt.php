<?php
/**
 * Funded Loan Report Table
 *
 * @version  0.1
 */

class VwRptFundedLoanRpt extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'vw_Rpt_Funded_Loan_Rpt'; 

 	protected $primaryKey = 'User_Id';

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		Gfauth::decryptData();

	}


	public function getData($startDate, $endDate){
		
		$data = DB::table($this->table)
		->whereBetween('Application_Dt', array( $startDate, $endDate ))->orderBy('Application_Dt')->get();
		
		return $data;
	}	

	 
}