<?php

/**
 * Developer's Notes:
 *
 */

class SecurityQuestion extends Eloquent{ 

	/** 
	 * The database table used by the model.
	 *
	 * @var string
	 */ 
	protected $table = 'Security_Question';

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Get User Data
	 * 
	 * @param  array  $fields
	 * @return array
	 */
  	public static function getData( $fields = array(), $where = array())
  	{	
		return SecurityQuestion::where($where)->select($fields)->get();
  	}
  	
}