<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class ApplicantPaymentMethodVerification extends Eloquent{

	protected $table        = 'Applicant_Payment_Method_Verification';
	
	protected $primaryKey   = ''; 
	
	public    $timestamps   = FALSE;
	
	protected $guarded      = array();
	
	public    $incrementing = false;

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
 
	protected $fillable = array('User_Id', 'Loan_App_Nr', 'Create_Dt', 'Created_By_User_Id');

}