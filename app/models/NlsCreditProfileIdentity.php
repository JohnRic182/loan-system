<?php

class NlsCreditProfileIdentity extends Eloquent{

	protected $table = 'NLS_CreditProfileIdentity';

	protected $primaryKey = 'CreditProfileIdentityID';

	public $timestamps = false;
}