<?php

/**
 * Developer's Note
 *
 *   
 */
class PreApplicationInfo extends Eloquent{

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table      = 'Pre_Application_Info';
	
	/**
	 * Table Primary Key
	 * 
	 * @var string
	 */
	protected $primaryKey = 'Applicant_User_Id';
		

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
	

	protected $timestamp = false;
}