<?php

/**
 * Loan Payment
 */
class LoanPayment extends Eloquent{

	protected $table      = 'Loan_Payment';
	
	protected $primaryKey = null;
	
	public $timestamps    = false;	

}