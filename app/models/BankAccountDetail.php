<?php

/**
 * Bank Account Detail - History
 *
 * @version  0.1
 */

/**
 * Developer's Note:
 * - Initial updates 2015-01-27
 *
 * @todo  transfer the encryption and use
 * the library for encryption and decryption
 */

class BankAccountDetail extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'Bank_Acct_History';

	/**
	 * Primary ID
	 * @var string
	 */
	protected $primaryKey = '';
	
	/**
	 * Timestamps
	 * 
	 * @var boolean
	 */
	public $timestamps    = false;
	

	/**
	 * Increments
	 * 
	 * @var boolean
	 */
	public $incrementing  = false;
 
		//@todo - we need to transfer this to config table
 	protected $encryptedPassword = '5k_Us3r_4cct_P455w0rd';

 	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected $encryptedFields = array('Acct_Holder_Name', 'Acct_Nr');

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();  
		$this->decryptData();
	}

	 
	/**
	 * Decrypt Data
	 * 
	 * @return
	 */
	public function decryptData()
	{
		return DB::statement("OPEN SYMMETRIC KEY sk_User_Acct
							DECRYPTION BY ASYMMETRIC KEY ak_User_Acct
							WITH PASSWORD = '". $this->encryptedPassword ."';");
	}

	/**
	 * Format Encrypted Fields
	 * 
	 * @param  array  $fields
	 * @return [type]
	 */
	public function formatEncryptedFields( $fields =  array() )
	{
		if( count($fields) > 0 ) {

			foreach($fields as $key => $field ){
	  			if( in_array($field, $this->encryptedFields ) )
	  				$fields[$key] = "CONVERT(VARCHAR(100), DECRYPTBYKEY($field)) AS $field";
	  		}
  			return implode(',', $fields );
  		}
  		
	}

	/**
	 * Format Encrypt Fields
	 * 
	 * @param  array  $fields
	 * @return [type]
	 */
	public function encryptFields( $field = '' )
	{
		return DB::raw("ENCRYPTBYKEY(KEY_GUID('sk_User_Acct'), '".$field."')");
	}

	/**
	 * Get Bank Account
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return array
	 */
	public static function getBankAccount( $userId, $loanAppNr )
	{
		return BankAccountDetail::where('User_Id','=',$userId)
								->where('Loan_App_Nr','=',$loanAppNr)
								->orderBy('Yodlee_Access_Dt', 'DESC')
								->first();
	}

	/**
	 * Check Bank Account
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return array
	 */
	public static function checkBankAccount( $userId, $loanAppNr, $date )
	{
		return BankAccountDetail::where('User_Id','=',$userId)
								->where('Loan_App_Nr','=',$loanAppNr)
								->where('Create_Dt', '>', $date)
								->orderBy('Yodlee_Access_Dt', 'DESC')
								->first();
	}


	/**
	 * Insert Batch data
	 * 
	 * @param  array  $data
	 * @return
	 */
	public static function insertBatch( $batchData = array() )
	{
		if ( count($batchData) > 0 ) {

			foreach ($batchData as $key => $data) {

				//Check if the data exist using User_Id, 
				//Loan_App_Nr, Yodlee_Access_Dt,
				// Site_Id and Account_Type and Acct_Nr
				$isExist = BankAccountDetail::where(
							array(
								'User_Id'        	=> $data['User_Id'], 
								'Loan_App_Nr'    	=> $data['Loan_App_Nr'],
								'Site_Id'        	=> $data['Site_Id'], 
								'Acct_Type_Desc' 	=> $data['Acct_Type_Desc'], 
								'Yodlee_Access_Dt'	=> $data['Yodlee_Access_Dt']
							)
						)->whereRaw(
							'CONVERT(VARCHAR(100), DECRYPTBYKEY(Acct_Nr)) = ?', array( $data['Acct_Nr_Raw'] )
						)
						->count();

				//Remove the Acct_Nr_Raw in the Array
				unset($data['Acct_Nr_Raw']);
 
				if( $isExist == 1 ){
					BankAccountDetail::where(
							array(
								'User_Id'          => $data['User_Id'], 
								'Loan_App_Nr'      => $data['Loan_App_Nr'],
								'Site_Id'          => $data['Site_Id'], 
								'Acct_Type_Desc'   => $data['Acct_Type_Desc'],
								'Yodlee_Access_Dt' => $data['Yodlee_Access_Dt']
							)
						)->update( $data );
				}else{
					BankAccountDetail::insert( $data );
				}			
			}		
		}
	}
  
	/**
	 * Get Bank Account History
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  string $acctType
	 * @return
	 */
	public static function getBankAccountHistory( $userId = NULL, $loanAppNr = NULL, $acctType = 'saving')
	{
		return BankAccountDetail::where(
			array(
				'Bank_Acct_History.User_Id'     =>  $userId, 
				'Bank_Acct_History.Loan_App_Nr' =>  $loanAppNr, 
				'Bank_Acct_Info.Acct_Type_Desc' =>  $acctType
			)
	   )
	   ->join('Bank_Acct_Info', function($join)
        {
            $join->on('Bank_Acct_History.User_Id', '=', 'Bank_Acct_Info.User_Id');
            $join->on('Bank_Acct_History.Acct_Type_Desc', '=', 'Bank_Acct_Info.Acct_Type_Desc');
        })
	   ->select('Bank_Acct_History.Current_Bal_Amt', 
	   			'Bank_Acct_History.Yodlee_Access_Dt', 
	   			'Bank_Acct_Info.Acct_Type_Desc',
	   			'Bank_Acct_Info.Site_Id'
	   	)
	   ->orderBy('Bank_Acct_History.Yodlee_Access_Dt', 'DESC')
	   ->get();
	}

	/**
	 * Get Bank Account Records
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  string $acctType
	 * @return
	 */
	public static function getBankAccountRecords( $userId = NULL, $loanAppNr = NULL, $acctType = 'saving')
	{
		$bankSelect = DB::raw("SELECT Yodlee_Access_Dt, Current_Bal_Amt, Row_Nr, Month_Year
		FROM
		(
		 SELECT ROW_NUMBER() OVER(PARTITION BY Yodlee_Access_Dt ORDER BY Current_Bal_Amt DESC) AS Row_Nr
		  , CAST(DATENAME(MONTH,Yodlee_Access_Dt)AS VARCHAR(20)) +' '+CAST(YEAR(Yodlee_Access_Dt)AS CHAR(4)) AS Month_Year
		  , Current_Bal_Amt
		  , Yodlee_Access_Dt
		 FROM ODS.dbo.Bank_Acct_History
		 WHERE User_Id = ".$userId."
		 AND Loan_App_Nr = ".$loanAppNr."
		 AND Acct_Type_Desc = 'checking'
		)bah
		WHERE Row_Nr = 1
		order by Yodlee_Access_Dt
			");

		return DB::select($bankSelect);

	}
}