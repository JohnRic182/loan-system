<?php

class LoanGroup extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| LoanGroup Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'Loan_Group';
	
	/**
	 * Primary Key
	 * @var null
	 */
	protected $primaryKey = 'Loan_Grp_Id';
	
	/**
	 * Time stamps
	 * @var boolean
	 */
	public $timestamps    = false;	
	
	/**
	 * Get Loan Group Name
	 * 
	 * @param  integer $loanGroupId
	 * @return 	
	 */
	public static function getLoanGroupName( $loanGroupId )
	{

		$loanGroup = LoanGroup::where('Loan_Grp_Id', '=', $loanGroupId )->first();	

		if( $loanGroup )
			return $loanGroup->Loan_Grp_Name;
	}
}