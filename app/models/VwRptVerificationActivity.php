<?php
/**
 * exclusion report table
 *
 * @version  0.1
 */

class VwRptVerificationActivity extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'vw_Rpt_Verification_Activity';
 

 	protected $primaryKey = 'Applicant_User_Id';


	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		Gfauth::decryptData();

	}


	public function getData($startDate, $endDate){
		
		$data = DB::table($this->table)
		->whereBetween('App_Start_Dt', array( $startDate, $endDate ))->orderBy('App_Start_Dt')->get();
		
		return $data;
	}	

	 
}