<?php

class NlsCreditPublicRecord extends Eloquent{

	protected $table = 'NLS_CreditPublicRecord';

	protected $primaryKey = 'CreditPublicRecordID';

	public $timestamps = false;

}