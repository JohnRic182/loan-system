<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class DocumentTemplate extends Eloquent{

	protected $table = 'Document_Template';

	public function getTemplateId( $param = array() )
	{
		return DocumentTemplate::where($param)
							    ->select('Doc_Template_Id')
							    ->first();
	}
}