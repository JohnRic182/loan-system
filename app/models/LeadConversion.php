<?php

class LeadConversion extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| LeadConversion Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'Lead_Conversion';

 
	/**
	 * Timestamps
	 * @var boolean
	 */
    public $timestamps = false;

    protected $fillable = array('Lead_Id', 'Campaign_Id', 'Loan_Id');
 	
 	/**
	 * Store Lead Conversion
	 *
	 * @param  array $param
	 * 
	 * @return
	 */
	public static function saveLeadConversion( $param = array() )
	{
		if( count($param) > 0 )
		{
			$isExist = self::isConversionExist($param);

			if( $isExist == 1 ) { 
				$param['Update_Dt'] = date('Y-m-d');
				$param['Updated_By_User_Id'] = $param['Created_By_User_Id'];
			 
				return self::where(
					array(
						'Lead_Id'              => $param['Lead_Id'], 
						'Campaign_Id'          => $param['Campaign_Id'], 
						'Loan_Id'              => $param['Loan_Id']
					) )->update( $param ); 
			}else{

				$LeadConversion                       = new LeadConversion();
				$LeadConversion->Lead_Id              = $param['Lead_Id'];
				$LeadConversion->Campaign_Id          = $param['Campaign_Id'];
				$LeadConversion->Loan_Id              = $param['Loan_Id']; 
				$LeadConversion->Loan_Funded_Dt       = $param['Loan_Funded_Dt']; 
				$LeadConversion->Funding_Partner_Id   = $param['Funding_Partner_Id'] ;
				$LeadConversion->Marketing_Partner_Id = $param['Marketing_Partner_Id']; 
				$LeadConversion->Created_By_User_Id   = $param['Created_By_User_Id'];
				$LeadConversion->Create_Dt            = $param['Create_Dt'];
				$LeadConversion->save(); 
			}
		}
	}

	/**
	 * Check whether the conversion Exist
	 * 
	 * @param  array   $param
	 * @return boolean
	 */
	public static function isConversionExist( $param  = array() )
	{
		if( count($param) > 0 ) {
			return LeadConversion::where(
				array(
					'Lead_Id'              => $param['Lead_Id'], 
					'Campaign_Id'          => $param['Campaign_Id'], 
					'Loan_Id'              => $param['Loan_Id'], 
					// 'Funding_Partner_Id'   => $param['Funding_Partner_Id'], 
					// 'Marketing_Partner_Id' => $param['Marketing_Partner_Id']
				)
			)->count();
		}
	}
}