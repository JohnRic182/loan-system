<?php

/**
 * Developer's Note
 *
 *  - update todo list
 */

class NLSTask extends Eloquent{
	
  	/**
  	 * Database Connection 
  	 */ 
	protected $connection = 'nls'; 
	
	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'task_refno';
	
	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'task';

	/**
	 * NLS Tasks Names
	 */
	const TASK_NAME_EMPLOYMENT          = 'CONFIRM_EMPLOYMENT';
	const TASK_NAME_INCOME              = 'CONFIRM_INCOME';
	const TASK_NAME_FUNDING             = 'CONFIRM_PMT_ACCT';
	const TASK_NAME_BANK            	= 'CONFIRM_BANK_ACCOUNT ';
	const TASK_NAME_CONTRACT            = 'REVIEW_CONTRACT';
	const TASK_NAME_DOCUMENTS           = 'CONFIRM_ID';
	const TASK_NAME_IDENTITY        	= 'ID_VERIFIED'; 
	const TASK_NAME_PAYMENT             = 'REVIEW_PAYMENT_METHOD';
	const TASK_NAME_FINAL_APPROVAL      = 'FINAL_APPROVAL'; 

	/**
	 * NLS Tasks Code
	 */
	const NLS_STATUS_CODE_APPROVED      = '100002';
	const NLS_STATUS_CODE_NOT_STARTED   = '100001';
	const NLS_STATUS_CODE_OPEN          = 1;
	const NLS_STATUS_CODE_DENIED        = '100004';
	
	/**
	 * NLS Status Final Approval
	 */
	const NLS_TASK_STATUS_APPROVED      = 'APPROVED';
	const NLS_TASK_STATUS_DENIED        = 'DENIED';
 
	/**
	 * NLS Loan Template
	 */
	const NLS_LOAN_TEMPLATE_FINAL_APPROVAL = 6;


	/**
	 * NLS Status Code
	 * 
	 * @return
	 */
	public function statusCode()
	{
		return $this->hasOne('NLSTaskStatusCode');
	}

	/**
	 * Is NLS Task Exist
	 * 
	 * @param  integer  $NLSRefNr
	 * @param  integer  $taskTemplateNr
	 * @return boolean
	 */
	public function isTaskExist( $NLSRefNr, $taskTemplateNr )
	{
		return $this->where('task_template_no', '=', $taskTemplateNr ) 
					 ->where('NLS_refno', '=', $NLSRefNr)
					 ->get()
					 ->count();
	}

	/**
	 * Get Task by Template No and Reference No
	 * 
	 * @param  array  $cifNrs
	 * @param  integer  $taskTemplateNr
	 * @return boolean
	 */
	public function getTasksByRefNoExcludeTemplateNo( $cifNrs , $taskTemplateNr )
	{
		return $this->whereIn('NLS_refno', $cifNrs ) 
				 ->where('task_template_no', '!=' , $taskTemplateNr )
				 ->join(
				 		'task_status_codes', 'task_status_codes.status_code_id'
				 		,'='
				 		,'task.status_code_id'
				 )
				 ->orderBy('NLS_refno', 'DESC')
				 ->select(
				 	 'task.NLS_refno'
				 	,'task.task_refno'
				 	,'task.status_code_id'
				 	,'task.subject'
				 	,'task.creator_uid'
				 	,'task.owner_uid'
				 	,'task.creation_date'
				 	,'task_status_codes.status_code'
				 	,'task_status_codes.status_code_description'
				 	,'task_template_no'
				 )
				 ->get();		
	}


	/**
	 * Get Task by Task Template No
	 * 
	 * @param  integer  $taskTemplateNr
	 * @return boolean
	 */
	public function getTasksByTaskTemplate( $taskTemplateNr )
	{
		return $this->where('task_template_no', '=',  $taskTemplateNr ) 
				 ->join(
				 		 'task_status_codes'
				 		,'task_status_codes.status_code_id'
				 		,'='
				 		,'task.status_code_id'
				 )
				 ->orderBy('NLS_refno', 'DESC')
				 ->select(
				 	 'task.NLS_refno'
				 	,'task.task_refno'
				 	,'task.status_code_id'
				 	,'task.subject'
				 	,'task.creator_uid'
				 	,'task.owner_uid'
				 	,'task.creation_date'
				 	,'task_status_codes.status_code'
				 	,'task_status_codes.status_code_description'
				 )
				 ->get();	
	}

	/**
	 * Get Task by Reference No and Status Code
	 * 
	 * @param  integer  $NLSRefNr
	 * @param  integer  $statusCd
	 * @return boolean
	 */
	public function getTasksByRefNoAndStatusCode( $NLSRefNr, $statusCd )
	{
		return $this->where(array( 
					'task.NLS_refno' => $NLSRefNr,
					'task.status_code_id' => $statusCd
		 	))->join(
			 		'task_template', 'task_template.task_template_no'
			 		,'='
			 		,'task.task_template_no'
			 )
			 ->orderBy('task.task_template_no', 'asc')
			 ->select(
			 	 'task.NLS_refno'
			 	,'task.task_refno'
			 	,'task.status_code_id'
			 	,'task.subject'
			 	,'task.creator_uid'
			 	,'task.owner_uid'
			 	,'task.creation_date'
			 	,'task_template.task_template_name'
			 )
			 ->get();
	}

	/**
	 * Get Task by Reference No 
	 * 
	 * @param  integer  $NLSRefNr
	 * @return boolean
	 */
	public function getTasksByRefNo( $NLSRefNr )
	{
		return $this->where(array( 
					'task.NLS_refno' => $NLSRefNr
		 	))->join(
			 		'task_template', 'task_template.task_template_no'
			 		,'='
			 		,'task.task_template_no'
			 )
			 ->orderBy('task.task_template_no', 'asc')
			 ->select(
			 	 'task.NLS_refno'
			 	,'task.task_refno'
			 	,'task.status_code_id'
			 	,'task.subject'
			 	,'task.creator_uid'
			 	,'task.owner_uid'
			 	,'task.creation_date'
			 	,'task_template.task_template_name'
			 )
			 ->get();
	}

	/**
	 * Get Task Reference No
	 * 
	 * @param  integer $NLSRefNr
	 * @param  integer $templateNr
	 * @return
	 */
	public function getTasksByRefNoTemplateNo( $NLSRefNr , $templateNr )
	{
		return $this->where(
			array( 
					'task.NLS_refno' => $NLSRefNr,
					'task.task_template_no' => $templateNr
		 	))
			->orderBy('task.task_template_no', 'asc')
			->select( 'task.task_refno')
			->first();
	}

	/**
	 * Create Final Approval Task 
	 *
	 * @param  integer $NLSRefNr
	 * @param  integer $loanAppNr
	 * @param  integer $userId
	 * @param  string  $status
	 * @param  string  $notes
	 * @return
	 */
	public function createFinalApprovalTask( $NLSRefNr, $loanAppNr, $userId, $status = 'NOT_STARTED', $notes = '' )
	{
		
		$isTaskExist = $this->isTaskExist( 
			$NLSRefNr, 
			self::NLS_LOAN_TEMPLATE_FINAL_APPROVAL 
		);
 
 		if( $isTaskExist == 0 && !empty($loanAppNr) ) { 

			$applicantInfo = Applicant::find( $userId );
			 
			$firstName = '';
			$lastName  = '';

			if( isset($applicantInfo['firstName']) ){
				$firstName = $applicantInfo['firstName']; 
			}

			if( isset($applicantInfo['lastName']) ){
				$lastName = $applicantInfo['lastName']; 
			}

			$applName =  $firstName  . ' ' . $lastName;
			
			$task = array(
				'UpdateFlag'       => 0,   
				'NLSType'          => 'Loan', 
				'NLSRefNumber'     => $loanAppNr,
				'CreatorUID'       => 0, 
				'OwnerUID'         => 0,
				'OwnerUserName'    => 'Wayde',  
				'StartDate'        => date('m/d/Y'), 
				'DueDate'          => date('m/d/Y'),
				'Subject'          => self::TASK_NAME_FINAL_APPROVAL . ' ' . $applName, 
				'Notes'            => $notes,
				'TaskTemplateName' => self::TASK_NAME_FINAL_APPROVAL,
				'PriorityCodeName' => 'NORMAL',
				'StatusCodeName'   => $status
			);


			try{

				$NLS = new NLS();
				$NLS->addTask($task); 

			}catch(Exception $e) {
				writeLogEvent('Final Approval Task', 
					array(
						'Loan_App_Nr' => $loanAppNr
					), 
					'warning'
				);
			}
		}
	}

	/**
	 * Close All Tasks Associated to a Loan 
	 * 
	 * @param  integer $NLSRefNr
	 * @param  string  $loanAppNr
	 * @param  string $newStatusCd
	 * @return
	 */
	public function updateAllNLSTasks( $NLSRefNr, $loanAppNr, $userId, $newStatusCd = 'LOAN_APP_CLOSED')
	{

	    $applicant  = Applicant::getData(
	    	array( 'First_Name', 'Last_Name' ), 
	    	array( 'User_Id' => $userId  ),
	      	true
      	);

	    $applName = ''; 

	    if( count( $applicant ) > 0 ){
      		$applName =  $applicant['First_Name']  . ' ' . $applicant['Last_Name'];
	    }

	    //Get all tasks associated to a loan application
      	$tasks = $this->getTasksByRefNo( $NLSRefNr );

      	if( count($tasks) > 0 ){ 
      		
      		try{
		        foreach ( $tasks as $key => $data ) { 
		          
		          $task = array(
		            'UpdateFlag'       => 1,  
		            'TaskRefno'        => $data['task_refno'],
		            'TaskTemplateName' => $data['task_template_name'],
		            'PriorityCodeName' => 'NORMAL',
		            'StatusCodeName'   => $newStatusCd,
		            'NLSRefNumber'     => $NLSRefNr,
		            'Subject'          => $data['task_template_name'] . ' ' . $applName,
		            'CompletionDate'   => date('m/d/Y')
		          );

		          $NLS = new NLS();
		          $NLS->updateTask( $task ); 
		        }

		        return true;

	    	}catch(Exception $e){

	    		writeLogEvent('Update All NLS Task', 
		    		array(
		    			'Message' => $e->getMessage()
		    		), 
		    		'warning'
	    		);
	    	}
      	}
	}
}