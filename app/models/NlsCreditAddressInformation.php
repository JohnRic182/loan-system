<?php

class NlsCreditAddressInformation extends Eloquent{

	protected $table = 'NLS_CreditAddressInformation';

	protected $primaryKey = 'CreditAddressInformationID';

	public $timestamps = false;

}