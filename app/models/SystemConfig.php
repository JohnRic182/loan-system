<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class SystemConfig extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| System Configuration Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	/**
	 * System App Config
	 * 
	 * @var string
	 */
	protected $table = 'System_App_Cfg';

	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Cfg_Name', 'Cfg_Desc', 'Cfg_Grp_Name', 'Cfg_Val' );


	public function __construct()
	{
		parent::__construct();  
	}
	
	/**
	 * Get all data 
	 * 
	 * @param  array  $fields
	 * @return array
	 */
	public static function getAllData( $fields = array() )
	{
		Gfauth::decryptData(); 

		return SystemConfig::select(
			DB::raw(
			" CONVERT(NVARCHAR(MAX), DECRYPTBYKEY(Cfg_Name)) AS Cfg_Name
    		, CONVERT(VARCHAR(MAX), DECRYPTBYKEY(Cfg_Val)) AS Cfg_Val"
			)
		)->get();
	}
}