<?php

/**
 * Developer's Note
 *
 * @todo  
 *     - update method name
 *     - remove test methods
 */

class NLS {
    
    /**
     * Service Name
     * 
     * @var object
     */
    protected $serviceName;    

    /**
     * Service URL
     * 
     * @var string
     */
    protected $url;             

    /**
     * Service Login Name
     * 
     * @var string
     */
    protected $serviceLogin;    

    /**
     * Service Password
     * 
     * @var string
     */
    protected $servicePassword; 

    /**
     * Database Type
     * 
     * @var string
     */
    protected $databaseType;

    /**
     * NLS Service Name
     * @var string
     */
    protected $serverName;      

    /**
     * Database Name 
     * 
     * @var string
     */
    protected $databaseName;

    /**
     * Web User Name
     * @var string
     */
    protected $webUsername;     

    /**
     * Web Password
     * @var string
     */
    protected $webPassword;     

    /**
     * NLS Connection
     * @var string
     */
    public $connection = '';
    
    /**
     * Loan Number
     * @var integer
     */
    public $LoanNumber = 0;  
        
    /**
     * Result Flag
     * 
     * @var integer
     */
    public $flag = 0; 


    /**
     * NLS Loan Status Codes
     * 
     * @var array
     */
    public $NLSStatusCodes =  array(
                                 'APPLICATION_STEP1'
                                ,'APPLICATION_STEP2'
                                ,'APPLICATION_STEP3'
                                ,'APPLICATION_STEP4'
                                ,'APPLICATION_REJECTED'
                                ,'VERIFICATION' 
                                ,'LOAN_APPROVED'
                                ,'LOAN_FUNDED'
                            ); 

    const NLS_KEY  = '1610CE2B670DA8C8CAA496C13511FDB962401E4BF13ADB7833F65EFBF46D2561';

    /**
     * Errors
     * 
     * @var string
     */
    public $error      = ''; 

    /**
     * NLS Response
     * 
     * @var string
     */
    public $response   = '';

    public function __construct()
    {
        $this->serviceName     = Config::get('system.NLS.ServiceName');
        $this->url             = Config::get('system.NLS.URL');
        $this->serviceLogin    = Config::get('system.NLS.ServiceLogin');
        $this->servicePassword = Config::get('system.NLS.ServicePassword');
        $this->databaseType    = Config::get('system.NLS.DBType');
        $this->serverName      = Config::get('system.NLS.ServerName');
        $this->databaseName    = Config::get('system.NLS.DatabaseName');
        $this->webUsername     = Config::get('system.NLS.WebAuthenticationUsername');      /* Used for Web Authentatication API */
        $this->webPassword     = Config::get('system.NLS.WebAuthenticationPassword');   

        if( empty( $this->connection ) ) {
            $this->connection = $this->nlsConnection();
        }
    }

    /**
     * Set NLS Connection 
     * 
     * @param  object $con
     * @return
     */
    public function nlsSetConnection( $con )
    {
        $this->connection = $con;
    }

    /**
     * Set NLS Connection - Soap Service
     * 
     * @return
     */
    public function nlsConnection()
    {
        //only set the Soap Wrapper Serives if 
        //it is empty
        if( count( SoapWrapper::services() ) == 0 ){
        
            return SoapWrapper::add(function ($service) {
                $service
                    ->name($this->serviceName)
                    ->wsdl($this->url)
                    ->trace(true)                                                   // Optional: (parameter: true/false)
                    ->options(['login' => $this->serviceLogin, 'password' => $this->servicePassword]);   // Optional: Set some extra options
            });
        }
    }

    /**
     * NLS Authentication
     * 
     * @return
     */
    public function nlsAuthenticate()
    {   

        $this->flag = 0;
        $service    = $this->connection;

        $param = [
            'db_type'       => $this->databaseType,
            'server_name'   => $this->serverName,
            'database_name' => $this->databaseName,
            'username'      => $this->webUsername,
            'password'      => $this->webPassword,
            'ipaddress'     => ''
        ];

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = $service->call('NLSAuthenticate', [$param])->NLSAuthenticateResult;
        });

        return $this->flag;
    }

    /**
     * NLS Verify Contact
     * 
     * @param array $fields
     */
    public function NLSverifyContact( $fields = array() )
    {
        $service = $this->connection;
        $param   = array(
                'ImportXML' => array(
                    'db_type'       => $this->databaseType,
                    'ServerName'    => $this->serverName,
                    'DatabaseName'  => $this->databaseName,
                    'FirstName'     => $fields['firstName'],
                    'LastName'      => $fields['lastName'],
                    'SSNumber'      => $fields['ssn'],
                    'Address'       => $fields['streetAddress1'],
                    'City'          => $fields['city'],
                    'State'         => $fields['State'],
                    'Zip'           => $fields['zip'],
                    'Phone'         => $fields['homePhoneNumber'],
                    'LoanNumber'    => $fields['loanId'],
                    'EmailAddress'  => $fields['email']
                )
        );
        
        $error = 0;

        try {
            SoapWrapper::service($this->serviceName, function ($service) use ($param) {
                $this->flag = $service->call('NLSVerifyContact', $param)->NLSVerifyContactResult;
            });
        } catch (SoapFault $fault) {
          $error = 1;
        }

        if( $this->flag ){
            print 'Succesfully Created Contact Object'; 
            return $this->flag;
        }else{
            print 'Error in Creating Contact Object';
            exit;
        }
    }
    
    /**
     * Create NLS Contact Object
     * 
     * @param array $fields
     * @todo  
     *     - set the message into language files 
     *     - update some of the variable naming
     */
    public function NLSCreateContact( $fields = array() )
    {
        $flag         = 'false';
        $importString = '';
        $message      = 'Succesfully Created Contact';
        $service      = $this->connection;
        $this->error  = '';

        $importString .=' <?xml version=”1.0” encoding=”UTF-8”?>
                <NLS>
                    <CIF 
                        CIFNumber="'.$fields['contactId'].'" 
                        Entity="Individual"
                        EmailAddress1="'.$fields['email'].'"
                        >
                        <CIFWEBCREDENTIALS 
                            UserName="'.$fields['username'].'"
                            UserPassword="'.$fields['password'].'"
                            WebAccessEnabled="1"
                            Hint1="'.$fields['hint1'].'"
                            Hint1Answer="'.$fields['hint1Answer'].'"
                            Hint2="'.$fields['hint2'].'"
                            Hint2Answer="'.$fields['hint2Answer'].'"
                             >
                        </CIFWEBCREDENTIALS>    
                    </CIF>
                </NLS>';

        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {

            $service->call('ImportXML', $param)->ImportXMLResult;

            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);
            $j = $k[0];
            
            if( $j=='false' ){

                $err  = explode('&lt;Error&gt;',$service->getLastResponse());
                $er1 = explode('&lt;/Error&gt;',$err[1]);
                $m   = explode('CIFWEBCREDENTIALS: ', $er1[0]);
  
                $this->error = $m[1]; // Error Message 
            }else{
                $this->flag = 1;
            }
        });

        //Log Errors if there's any exist
        if( $this->error != "" ) {
            Log::warning(
                'NLS Contact Object', 
                array(
                    'User_Id'       => $fields['contactId'], 
                    'Email_Address' => $fields['email'], 
                    'Error_Message' => $this->error
                )
            );
        }

        return array(
                    'result'  => $this->flag,
                    'message' => $this->error
                );
        
    }


    /**
     * Update <CIFWEBCREDENTIALS>
     * @param array $fields [description]
     * @param ind $contactID NLS contact ID
     * 
     *  required fields: UserName, UserPassword     *    
     *  optional fields: WebAccessEnabled, Hint1, Hint1Answer, Hint2, Hint2Answer
     */
    public function NSLUpdateWebCredentials( $contactID, $fields = array() ){

        $this->flag   = 0;
        $service      = $this->connection;
        $importString = "";
        //creat xml import string
        $importString .=' <?xml version=”1.0” encoding=”UTF-8”?>
        <NLS>
            <CIF 
                CIFNumber="'.$contactID.'" 
                Entity="Individual"
                UpdateFlag="1"
                >
                <CIFWEBCREDENTIALS ';

                foreach ($fields as $key => $value)
                    $importString .= $key.'="'.$value.'" ';

        $importString .='>
                </CIFWEBCREDENTIALS>    
            </CIF>
        </NLS>';

        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
        });

    }


    /**
     * NLS Update Contact
     * 
     * @param array $fields
     */
    public function NLSUpdateContact( $fields = array() )
    {
        
        $this->flag   = 0;
        $service      = $this->connection;

        $secondaryNum = '';
        
        if(isset($fields['mobilePhoneNr'])){
            $secondaryNum = '<CIFPHONENUMBER 
                            PhoneDescription="Work Phone" 
                            Operation = "UPDATE" 
                            PhoneNumber="'.$fields['mobilePhoneNr'].'"
                            MobileNumberFlag="1" />';
        }


        $importString =' <?xml version=”1.0” encoding=”UTF-8”?>
                <NLS>
                    <CIF 
                        UpdateFlag="1"
                        CIFNumber="'.$fields['contactId'].'" 
                        Entity="Individual"
                        FirstName1="'.$fields['firstName'].'"
                        LastName1="'.$fields['lastName'].'"
                        ShortName = "'.$fields['firstName'].' '.$fields['lastName'].'"
                        City="'.$fields['city'].'"
                        StreetAddress1="'.$fields['streetAddress1'].'"
                        State="'.$fields['State'].'"
                        DateOfBirth="'.$fields['birthDate'].'"
                        CIFPortfolioName="BORROWER" 
                        TaxIDNumber="'.$fields['ssn'].'"
                        ZipCode="'.$fields['zip'].'"
                        >
                        <CIFPHONENUMBER 
                            PhoneDescription="Home Phone"
                            Operation = "UPDATE" 
                            PhoneNumber="'.$fields['homePhoneNumber'].'"
                            PrimaryFlag="1" />

                        '. $secondaryNum .'
                        
                    </CIF>
                </NLS>';

        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
        });
    }

    /**
     * Create Loan Object into NLS 
     * 
     * @param array $fields
     * @todo 
     *     - update variables
     */
    public function NLSCreateLoan( $fields = array() )
    {
        
        $this->flag     = 0;
        $this->response = '';

        $service        = $this->connection;
         
        $importString = '';
        $importString = '<? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <LOAN 
                        LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                        LoanPortfolioName="'.$fields['nlsLoanPortfolio'].'"
                        LoanGroupName="'.$fields['nlsLoanGroup'].'"
                        CIFNumber="'.$fields['contactId'].'" 
                        LoanNumber="'.$fields['loanId'].'"
                        AccountName="'.$fields['lastName'].' '.$fields['firstName'].'"
                        OriginationDate="'.date('m/d/Y').'"
                        CurrencyID="USD"
                        TermType="Months"
                        Term="'.$fields['term'].'"
                        >
                        <LOANSTATUSES
                         Operation="ADD"
                         LoanStatusCode="'.$fields['loanStatusCode'].'"
                         EffectiveDate="'.date('m/d/Y').'"
                         >
                        </LOANSTATUSES>
                        <LOANINTERESTRATERECORD InterestType="0" InterestRate="28.00000">
                        </LOANINTERESTRATERECORD>
                        <LOANDETAIL1 
                            UserDefined1="'.$fields['loanAmount'].'"
                            UserDefined2="'.$fields['loanPurpose'].'"
                            UserDefined3="'.$fields['employeeStatus'].'"
                            UserDefined4="'.$fields['annualGrossIncome'].'"
                            >
                        </LOANDETAIL1>
                    </LOAN>
                </NLS>';
        
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            
            $service->call('ImportXML', $param)->ImportXMLResult;
            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);
            $this->flag = $k[0];
            $this->response = $service->getLastResponse();
        });
        
        if( $this->flag == 'true' ){
            return 1;
        }else{

            // _pre($this->response);

            $logFile = Config::get('log.application');
            Log::useDailyFiles(storage_path().'/logs/' . $logFile );

            //Log failed Response
            Log::warning(
                'NLS Loan Object', 
                array(
                    'User_Id'       => $fields['contactId'], 
                    'Loan_Number'   => $fields['loanId'], 
                    'Error_Message' => $this->response
                )
            );
           
            return 0;
        }
    }


    /**
     * Pull Credit Data from NLS
     * 
     * @param integer $contactId
     */
    public function IsNLSCreditPullExist( $contactId )
    {
        $this->flag = false;

        $service = $this->connection;
        $importString = '';
        $importString = '<?xml version="1.0" encoding="UTF-8"?>
                <NLS> 
                    <CREDITPROFILE>
                        <PULLCREDIT CIFNumber="'.$contactId.'" CreditBureauName="TransUnion" PullCreditIfExistingIsXDaysOld="5" /> 
                    </CREDITPROFILE>
                </NLS>';

        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );
        
        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = 0;
            $service->call('ImportXML', $param)->ImportXMLResult;
            
            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);

              // $this->flag = $service->getLastResponse();
            
            if( isset($k[0]) && $k[0] == 'true' )
                $this->flag = $k[0];
            else
                $this->flag = $service->getLastResponse();
            
        });
        // _pre($this->flag);
        return $this->flag;
    }


    /**
     * Pull Credit Data from NLS
     * 
     * @param   array $fields
     * @deprecated v1.0.4.21
     */
    public function NLSCreditPull( $fields = array() )
    {
        if($this->flag){
            $this->flag = 0;
        }  
        $service = $this->connection;
        $importString = '';
        $importString = '<? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <LOAN 
                       UpdateFlag="1"
                        LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                        CIFNumber="'.$fields['contactId'].'" 
                        LoanNumber="'.$fields['loanId'].'"
                        AccountName="'.$fields['lastName'].' '.$fields['firstName'].'"
                        OriginationDate="'.date('m/d/Y').'"
                        CurrencyID="USD"
                        >
                        <CREDITBUREAU
                            CreditBureauHeader="'.$fields['creditBureauHeader'].'">
                        </CREDITBUREAU>
                    </LOAN>
                    <CREDITPROFILE>
                        <PULLCREDIT CIFNumber="'.$fields['contactId'].'" CreditBureauName="TransUnion" PullCreditIfExistingIsXDaysOld="5" /> 
                    </CREDITPROFILE>
                </NLS>';

        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );
        
        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = 0;
            $service->call('ImportXML', $param)->ImportXMLResult;
            // print '<pre>';
            // var_dump($service->getLastrequest());
            // var_dump($service->getLastResponse());
            // exit;
            
            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);
           
            $this->flag = $k[0];  // ImportXMLResult
            
        });
        
        if($this->flag == 'true'){
           
            return 1;
        }else{
           
            return 0;
        }
    }

    /**
     * Update NLS Loan Object
     * 
     * @param  array $fields
     * @todo
     *     - change method  name
     * @return
     */
    public function nlsUpdateLoan( $fields = array() )
    {
       
        $this->flag = 0;
 
        $importStatusString  = '';

        //Check for the Loan Status Code
        if( isset($fields['loanStatusCode'] ) ){
            //Set Delete and Add Operations for Loan Statuses
            foreach( $this->NLSStatusCodes as $status ){

                if( $status == strtoupper( $fields['loanStatusCode'] ) ) {
                   
                    $importStatusString .= '<LOANSTATUSES
                                                Operation="ADD"
                                                LoanStatusCode = "'.$fields['loanStatusCode'].'"
                                                EffectiveDate="'.date('m/d/Y').'">
                                            </LOANSTATUSES>';
                } else {
                   
                    $importStatusString .= '<LOANSTATUSES
                                                Operation="DELETE"
                                                LoanStatusCode = "'.$status.'"
                                                EffectiveDate="'.date('m/d/Y').'">
                                            </LOANSTATUSES>';
                }
            }
        }
        
        //Set Import String
        $importString = '<?xml version="1.0" encoding="UTF-8"?>
                        <NLS>
                            <LOAN 
                                UpdateFlag="1"
                                LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                                CIFNumber="'. $fields['contactId'] . '" 
                                LoanNumber="' . $fields['loanId'] . '"
                                AccountName="' . $fields['lastName'] . ' ' . $fields['firstName'] . '"
                                OriginationDate="'.date('m/d/Y').'"
                                CurrencyID ="USD"';

                                if( isset( $fields['LoanPortfolioName'] ) ) {
                                    $importString .= 'LoanPortfolioName="'. $fields['LoanPortfolioName'] .'"';
                                }
                                
                                if( isset( $fields['paymentAmount'] ) ) {
                                    $importString .= 'PaymentAmount="'. $fields['paymentAmount'] .'"';
                                }

                                $importString .= '>'; 

                                if( isset( $fields['interestRate'] ) ) {
                                    $importString .= '<LOANINTERESTRATERECORD
                                                        InterestRate = "' . $fields['interestRate'] . '"
                                                        InterestType="0">
                                                    </LOANINTERESTRATERECORD>';
                                }

                                $userDefinedFields =  Config::get('udf');

                                //Check for all LOANDETAIL1 User Defined Fields
                                if( isset($fields['LOANDETAIL1']) ) {
                                
                                    $importString .= '<LOANDETAIL1'; 

                                    foreach( $userDefinedFields['LOANDETAIL1'] as $key => $value ) {
                                        if( isset($fields[$value]) )
                                            $importString .= ' ' . $key . '="' . $fields[$value] . '"';
                                    }

                                    $importString .= '></LOANDETAIL1>';
                                }

                                //Check for all LOANDETAIL2
                                if( isset($fields['LOANDETAIL2']) ) {
                                
                                    $importString .= '<LOANDETAIL2'; 

                                    foreach( $userDefinedFields['LOANDETAIL2'] as $key => $value ) {
                                        if( isset($fields[$value]) )
                                            $importString .= ' ' . $key . '="' . $fields[$value] . '"';
                                    }

                                    $importString .= '></LOANDETAIL2>';
                                }            

                                //Check for Status Code
                                if( isset( $fields['loanStatusCode']) )
                                    $importString .= $importStatusString;

                                $importString .= '</LOAN>
                                        </NLS>';

       
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
        });

        return $this->flag;
    }

    /**
     * Update NLS Loan Object Reprocess
     * 
     * @param  array $fields
     * @todo
     *     - change method  name
     * @return
     */
    public function nlsUpdateLoanReprocess( $fields = array() )
    {
       
        $this->flag = 0;
 
        $importStatusString  = '';

        //Check for the Loan Status Code
        if( isset($fields['loanStatusCode'] ) ){
            //Set Delete and Add Operations for Loan Statuses
            foreach( $this->NLSStatusCodes as $status ){

                if( $status == strtoupper( $fields['loanStatusCode'] ) ) {
                   
                    $importStatusString .= '<LOANSTATUSES
                                                Operation="ADD"
                                                LoanStatusCode = "'.$fields['loanStatusCode'].'"
                                                EffectiveDate="11/09/2015">
                                            </LOANSTATUSES>';
                } else {
                   
                    $importStatusString .= '<LOANSTATUSES
                                                Operation="DELETE"
                                                LoanStatusCode = "'.$status.'"
                                                EffectiveDate="11/09/2015">
                                            </LOANSTATUSES>';
                }
            }
        }
        
        //Set Import String
        $importString = '<?xml version="1.0" encoding="UTF-8"?>
                        <NLS>
                            <LOAN 
                                UpdateFlag="1"
                                LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                                CIFNumber="'. $fields['contactId'] . '" 
                                LoanNumber="' . $fields['loanId'] . '"
                                AccountName="' . $fields['lastName'] . ' ' . $fields['firstName'] . '"
                                OriginationDate="11/09/2015"
                                CurrencyID ="USD"';

                                if( isset( $fields['LoanPortfolioName'] ) ) {
                                    $importString .= 'LoanPortfolioName="'. $fields['LoanPortfolioName'] .'"';
                                }
                                
                                if( isset( $fields['paymentAmount'] ) ) {
                                    $importString .= 'PaymentAmount="'. $fields['paymentAmount'] .'"';
                                }

                                $importString .= '>'; 

                                if( isset( $fields['interestRate'] ) ) {
                                    $importString .= '<LOANINTERESTRATERECORD
                                                        InterestRate = "' . $fields['interestRate'] . '"
                                                        InterestType="0">
                                                    </LOANINTERESTRATERECORD>';
                                }

                                $userDefinedFields =  Config::get('udf');

                                //Check for all LOANDETAIL1 User Defined Fields
                                if( isset($fields['LOANDETAIL1']) ) {
                                
                                    $importString .= '<LOANDETAIL1'; 

                                    foreach( $userDefinedFields['LOANDETAIL1'] as $key => $value ) {
                                        if( isset($fields[$value]) )
                                            $importString .= ' ' . $key . '="' . $fields[$value] . '"';
                                    }

                                    $importString .= '></LOANDETAIL1>';
                                }

                                //Check for all LOANDETAIL2
                                if( isset($fields['LOANDETAIL2']) ) {
                                
                                    $importString .= '<LOANDETAIL2'; 

                                    foreach( $userDefinedFields['LOANDETAIL2'] as $key => $value ) {
                                        if( isset($fields[$value]) )
                                            $importString .= ' ' . $key . '="' . $fields[$value] . '"';
                                    }

                                    $importString .= '></LOANDETAIL2>';
                                }            

                                //Check for Status Code
                                if( isset( $fields['loanStatusCode']) )
                                    $importString .= $importStatusString;

                                $importString .= '</LOAN>
                                        </NLS>';

       
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );

        // pre($param);

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;

            // writeLogEvent(
            //                 'NLS Update Loan ', 
            //                 array(
            //                     'Result' => $service->getLastResponse()
            //                 )
            //             );

            // pre();
        });

        return $this->flag;
    }


/**
     * Update NLS Loan Object
     * 
     * @param  array $fields
     * @todo
     *     - change method  name
     * @return
     */
    public function nlsUpdateLoan667( $fields = array() )
    {
       
        $this->flag = 0;
 
        $importStatusString  = '';

        //Check for the Loan Status Code
        if( isset($fields['loanStatusCode'] ) ){
            //Set Delete and Add Operations for Loan Statuses
            foreach( $this->NLSStatusCodes as $status ){

                if( $status == strtoupper( $fields['loanStatusCode'] ) ) {
                   
                    $importStatusString .= '<LOANSTATUSES
                                                Operation="ADD"
                                                LoanStatusCode = "'.$fields['loanStatusCode'].'"
                                                EffectiveDate="'.date('m/d/Y').'">
                                            </LOANSTATUSES>';
                } else {
                   
                    $importStatusString .= '<LOANSTATUSES
                                                Operation="DELETE"
                                                LoanStatusCode = "'.$status.'"
                                                EffectiveDate="'.date('m/d/Y').'">
                                            </LOANSTATUSES>';
                }
            }
        }
        
        //Set Import String
        $importString = '<?xml version="1.0" encoding="UTF-8"?>
                        <NLS>
                            <LOAN 
                                UpdateFlag="1"
                                AcctRefno="3104" 
                                LoanTemplateNo="3"
                                CIFNumber="'. $fields['contactId'] . '" 
                                LoanNumber="' . $fields['loanId'] . '"
                                AccountName="' . $fields['lastName'] . ' ' . $fields['firstName'] . '"
                                OriginationDate="' . date('m/d/Y') . '"
                                CurrencyID ="USD"                               
                                LoanGroupName="ASCEND-DEFAULT"
                                Term="33"';

                                if( isset( $fields['LoanPortfolioName'] ) ) {
                                    $importString .= 'LoanPortfolioName="'. $fields['LoanPortfolioName'] .'"';
                                }
                                
                                if( isset( $fields['paymentAmount'] ) ) {
                                    $importString .= 'PaymentAmount="'. $fields['paymentAmount'] .'"';
                                }

                                $importString .= '>'; 

                                if( isset( $fields['interestRate'] ) ) {
                                    $importString .= '<LOANINTERESTRATERECORD
                                                        InterestRate = "' . $fields['interestRate'] . '"
                                                        InterestType="0">
                                                    </LOANINTERESTRATERECORD>';
                                }

                                $userDefinedFields =  Config::get('udf');

                                //Check for all LOANDETAIL1 User Defined Fields
                                if( isset($fields['LOANDETAIL1']) ) {
                                
                                    $importString .= '<LOANDETAIL1'; 

                                    foreach( $userDefinedFields['LOANDETAIL1'] as $key => $value ) {
                                        if( isset($fields[$value]) )
                                            $importString .= ' ' . $key . '="' . $fields[$value] . '"';
                                    }

                                    $importString .= '></LOANDETAIL1>';
                                }

                                //Check for all LOANDETAIL2
                                if( isset($fields['LOANDETAIL2']) ) {
                                
                                    $importString .= '<LOANDETAIL2'; 

                                    foreach( $userDefinedFields['LOANDETAIL2'] as $key => $value ) {
                                        if( isset($fields[$value]) )
                                            $importString .= ' ' . $key . '="' . $fields[$value] . '"';
                                    }

                                    $importString .= '></LOANDETAIL2>';
                                }            

                                //Check for Status Code
                                if( isset( $fields['loanStatusCode']) )
                                    $importString .= $importStatusString;

                                $importString .= '</LOAN>
                                        </NLS>';

       
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );

        // pre($param);

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;

            // writeLogEvent(
            //                 'NLS Update Loan ', 
            //                 array(
            //                     'Result' => $service->getLastResponse()
            //                 )
            //             );

            // pre();
        });

        return $this->flag;
    }


    /**
     * Update NLS Loan Status
     * 
     * @param  array  $fields
     * @todo  
     *     - change method name
     * @return
     */
    public function nlsUpdateLoanStatus( $fields = array() )
    {
        $this->flag = 0;
 
        $importStatusString  = '';

        //Set Delete and Add Operations for Loan Statuses
        foreach( $this->NLSStatusCodes as $status ){

            if( $status == strtoupper( $fields['loanStatusCode'] ) ) {
               
                $importStatusString .= '<LOANSTATUSES
                                            Operation="ADD"
                                            LoanStatusCode = "'.$fields['loanStatusCode'].'"
                                            EffectiveDate="'.date('m/d/Y').'">
                                        </LOANSTATUSES>';
            } else {
               
                $importStatusString .= '<LOANSTATUSES
                                            Operation="DELETE"
                                            LoanStatusCode = "'.$status.'"
                                            EffectiveDate="'.date('m/d/Y').'">
                                        </LOANSTATUSES>';
            }
        }

        //risk Rating Code
        $riskRatingCode = ( isset($fields['riskSegment']) )? 'RiskRatingCode="'. $fields['riskSegment'] .'"' : 'NONE';

        $importString = '<? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <LOAN 
                        UpdateFlag="1"
                        LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                        LoanGroupName="'.$fields['nlsLoanGroup'].'"
                        LoanPortfolioName="'.$fields['nlsLoanPortfolio'].'"                       
                        AccountName="'.$fields['lastName'].' '.$fields['firstName'].'"
                        CIFNumber="'.$fields['contactId'].'" 
                        LoanNumber="'.$fields['loanId'].'"
                        CurrencyID="USD"
                        OriginationDate="'.date('m/d/Y').'" 
                        '. $riskRatingCode .'
                        >';
                        
        $importString .= $importStatusString;
        $importString .='
                    </LOAN> 
                </NLS>';

        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );
     
        SoapWrapper::service($this->serviceName, function ($service) use ($param) {           
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
            $this->flag = $service->getLastresponse();
        });

        return $this->flag;
    }
    
    /**
     * Add Comment to NLS
     *
     * @param  array  $fields
     * @return
     */
    public function addComment( $fields = array() )
    {
            
        $this->flag = 'test';

        $files = '';

        if( isset($fields['Files']) ) {
            foreach ($fields['Files'] as $key => $value) {
               $files .= '<DOCBINARY Filename="'.$value['Filename'].'" DocImage="'.$value['DocImage'].'"/>'; 
            }
        }
        
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => '
                <? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <LOAN 
                        UpdateFlag="1"
                        LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                        LoanPortfolioName="'.$fields['nlsLoanPortfolio'].'"
                        LoanGroupName="'.$fields['nlsLoanGroup'].'"
                        CIFNumber="'.$fields['contactId'].'" 
                        LoanNumber="'.$fields['loanId'].'"
                        CurrencyID="USD"
                        >
                        <LOANCOMMENTS
                            Date="'.date('m/d/Y').'"
                            Comment="'.$fields['Comment'].'"
                            Category="'.$fields['Category'].'">
                            '. $files.'
                        </LOANCOMMENTS>
                    </LOAN> 
                </NLS>'
            )
        );
     
        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $service->call('ImportXML', $param)->ImportXMLResult;
            $this->flag = $service->getLastresponse();            
        });

       return $this->flag;
    }

    /**
    * Add Task to NLS 
    * 
    * @param  array  $fields
    * @return
    */
    public function addTask( $fields = array(), $comments = array() )
    {
        
        $importString   = '';
        $commentString  = '';

        if(isset($comments['commentStr']) && $comments['commentStr'] != '' ){

            $commentString = '<TASKCOMMENTS 
                                Date = "'. date('m/d/Y') .'" 
                                Comment = "'. $comments['commentStr'] .'" 
                                Category = ""
                            >'.$comments['files'].'</TASKCOMMENTS>';
        }

        foreach ( $fields as $key => $value ) 
            $importString .= $key . '="' . $value . '" '; 
    
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => '
                <? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <TASK '. $importString .'>
                        '. $commentString .'
                    </TASK> 
                </NLS>'
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            //$this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
            $service->call('ImportXML', $param)->ImportXMLResult;
            
            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);
            $this->flag = $k[0];  // ImportXMLResult
            // pre($service->getLastResponse());
            // writeLogEvent('CreatingNLSTask', array(
            //     'response' => $service->getLastResponse()
            // ), 'warning');
        });

        if( $this->flag == 'true' )
            return true;

        return false;
    }

    /**
    * Update Task to NLS 
    * 
    * @param  array  $fields
    * @return
    */
    public function updateTask( $fields = array(), $comments = array() )
    {
        
        $importString   = '';
        $commentString  = '';

        if(isset($comments['commentStr']) && $comments['commentStr'] != '' ){

            $commentString = '<TASKCOMMENTS 
                                Date = "'. date('m/d/Y') .'" 
                                Comment = "'. $comments['commentStr'] .'" 
                                Category = ""
                            >'.$comments['files'].'</TASKCOMMENTS>';
        }

        foreach ( $fields as $key => $value ) 
            $importString .= $key . '="' . $value . '" '; 
    
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => '
                <? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <TASK '. $importString .'>
                        '. $commentString .'
                    </TASK> 
                </NLS>'
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            //$this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
            $service->call('ImportXML', $param)->ImportXMLResult;
            
            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);
            $this->flag = $k[0];  // ImportXMLResult
            // pre($service->getLastResponse());
        });

        if( $this->flag == 'true' )
            return true;

        return false;
    }
 
    /**
     * Add Address Book Record to NLS
     * 
     * @param  array  $fields
     * @return
     */
    public function updateAddressBook( $fields = array() )
    {
        $this->flag = false;

        if( count( $fields ) > 0 ) {
            
            $param = array(
                'ImportXML' => array(
                    'ServerName'    => $this->serverName,
                    'DatabaseName'  => $this->databaseName,
                    'ImportString'  => '
                    <?xml version="1.0" encoding="UTF-8"?>
                    <NLS>
                        <CIF 
                            UpdateFlag="1"
                            CIFNumber="'.$fields['UserId'].'" 
                            Entity="Individual">
                            <CIFADDRESSBOOK 
                                AddressDescription="'. $fields['RelationshipCode']. '"
                                Entity="'. $fields['Entity']. '"
                                RelationshipCode="'. $fields['RelationshipCode']. '"
                                CompanyName="'. $fields['CompanyName']. '"
                                LastName1="'. $fields['LastName1']. '"
                                FirstName1="'. $fields['FirstName1']. '"
                                Title1="'. $fields['Title1']. '"
                                EmailAddress1="'. $fields['EmailAddress1']. '"
                                City="'. $fields['City']. '"
                                ZipCode="'. $fields['ZipCode']. '"
                                UserDefinedField1="'. $fields['UserDefinedField1']. '" 
                                UserDefinedField2="'. $fields['UserDefinedField2']. '" 
                                UserDefinedField5="'. $fields['UserDefinedField5']. '"
                                UserDefinedField7="'. $fields['UserDefinedField7']. '"
                                UserDefinedField8="'. $fields['UserDefinedField8']. '"
                                UserDefinedField9="'. $fields['UserDefinedField9']. '"
                                UserDefinedField10="'. $fields['UserDefinedField10']. '"
                                UserDefinedField11="'. $fields['UserDefinedField11']. '"
                                UserDefinedField12="'. $fields['UserDefinedField12']. '"
                                UserDefinedField13="'. $fields['UserDefinedField13']. '"
                            />
                        </CIF>
                    </NLS>'
                )
            );
 
            SoapWrapper::service( $this->serviceName, function ($service) use ($param) {
               $this->flag =  $service->call('ImportXML', $param)->ImportXMLResult;
               // pre($service->getLastResponse());
            });
        }

        return $this->flag;
    }

    /**
     * Update NLS Loan Status
     * 
     * @param  array  $fields
     * @todo  
     *     - change method name
     * @return
     */
    public function createTransaction( $fields = array() )
    {
        $this->flag = 0;
 
        $importStatusString  = '';

        $importString = '<? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <TRANSACTIONS>

                        <TRANSACTIONCODE TransactionCode="'. $fields['TransactionCode']. '"
                                LoanNumber="'. $fields['LoanNumber']. '"
                                EffectiveDate="'.date('m/d/Y').'"
                                Amount="'. round($fields['Amount'],2). '"

                                />
                        ';
        $importString .= $importStatusString;
        $importString .='
                    </TRANSACTIONS> 
                </NLS>';

        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => $importString
            )
        );
     
        SoapWrapper::service($this->serviceName, function ($service) use ($param) {           
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
            //pre($service->getLastResponse());
        });

        return $this->flag;
    }

    
    /**
     * Add ACH to NLS
     *   
     * @param  array  $fields
     * @return
     */
    public function addACH( $fields = array() )
    {
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => '
                <? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <LOAN 
                        UpdateFlag="1"
                        LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                        LoanPortfolioName="'.$fields['nlsLoanPortfolio'].'"
                        LoanGroupName="'.$fields['nlsLoanGroup'].'"
                        CIFNumber="'.$fields['contactId'].'" 
                        LoanNumber="'.$fields['loanId'].'"
                        >
                        <ACH
                            ACHCompanyName="'.$fields['companyName'].'"
                            ABANumber="'.$fields['bankRoutingNumber'].'"
                            AccountNumber = "'.$fields['accountNumber'].'"
                            Amount = "'.$fields['amount'].'"
                            AmountType="0"
                            BillingStartDate="'.$fields['billingStartDate'].'"
                            BillingType="2"
                            BillingPeriod="MO"
                            Status="0"
                            Description="MONTHLY ACH"
                            >
                        </ACH>
                    </LOAN> 
                </NLS>'
            )
        );
 
        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $service->call('ImportXML', $param)->ImportXMLResult;
            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);
            $this->flag = $k[0]; 
        });
 
        if($this->flag=='true'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Update ACH to NLS
     *   
     * @param  array  $fields
     * @return
     */
    public function updateACH( $fields = array() )
    {
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => '
                <? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <LOAN 
                        UpdateFlag="1"
                        LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                        LoanPortfolioName="'.$fields['nlsLoanPortfolio'].'"
                        LoanGroupName="'.$fields['nlsLoanGroup'].'"
                        CIFNumber="'.$fields['contactId'].'" 
                        LoanNumber="'.$fields['loanId'].'"
                        >
                        <ACH
                            ACHCompanyName="'.$fields['companyName'].'"
                            ABANumber="'.$fields['bankRoutingNumber'].'"
                            AccountNumber = "'.$fields['accountNumber'].'"
                            Amount = "'.$fields['amount'].'"    
                            >
                        </ACH>
                    </LOAN> 
                </NLS>'
            )
        );
    
        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $service->call('ImportXML', $param)->ImportXMLResult;
            $h = explode('<ImportXMLResult>',$service->getLastResponse());
            $k = explode('</ImportXMLResult>',$h[1]);
            $this->flag = $k[0];  // ImportXMLResult
            //pre($service->getLastResponse());
        });

        if($this->flag=='true'){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Add CIF Vendor to NLS Contact
     *  - Please see NLS IMPORT XML <CIFVENDOR> SUB
     * 
     * @param array $fields
     */
    public function addCIFVendor( $fields = array() )
    {
 
        $this->flag  = false;

        if( count( $fields ) > 0 ) { 

            $param = array(
                'ImportXML' => array(
                    'ServerName'    => $this->serverName,
                    'DatabaseName'  => $this->databaseName,
                    'ImportString'  => '<?xml version="1.0" encoding="UTF-8"?>
                    <NLS>
                        <CIF
                            UpdateFlag ="1" 
                            CIFNumber="'.$fields['CIFNumber'].'" 
                            Entity="Individual">
                        <CIFVENDOR 
                            Payee="'. $fields['Payee'] .'" 
                            AccountNumber="'.$fields['AccountNumber'].'"
                            ACHPayableABANumber="'. $fields['ACHPayableABANumber'] .'" 
                            ACHPayableAccountNumber="'. $fields['ACHPayableAccountNumber'] .'" 
                            ACHReceivableABANumber="'. $fields['ACHReceivableABANumber'] .'" 
                            ACHReceivableAccountNumber="'. $fields['ACHReceivableAccountNumber'] .'">
                        </CIFVENDOR>
                        </CIF>
                    </NLS>'
                )
            );  
 
            SoapWrapper::service( $this->serviceName, function ($service) use ($param) { 
                $service->call('ImportXML', $param)->ImportXMLResult;
                $this->flag = $service->getLastresponse();
            });
             
        } 

        return $this->flag;
    }

    /**
     * Add Yodlee Information to NLS Loan 
     * 
     * @param  array $fields
     * @return
     */
    public function nlsUpdateYodlee( $fields = array() )
    {
        
        $this->flag = 0;
       
        $param = array(
            'ImportXML' => array(
                'ServerName'    => $this->serverName,
                'DatabaseName'  => $this->databaseName,
                'ImportString'  => '
                <? xml version="1.0" encoding="UTF-8"?>
                <NLS>
                    <LOAN 
                        UpdateFlag="1"
                        LoanTemplateName="'.$fields['nlsLoanTemplate'].'" 
                        CIFNumber="'.$fields['contactId'].'" 
                        LoanNumber="'.$fields['loanId'].'"
                        CurrencyID="USD"
                        >
                        <LOANDETAIL1 
                            UserDefined6="'.$fields['totalNSFs'].'" 
                            UserDefined7="'.$fields['DaysSinceNSF'].'"
                            UserDefined8="'.$fields['MoDepositToAverage'].'"
                            UserDefined9="'.$fields['avgBalPmtDate'].'"
                            UserDefined10="'.$fields['currentBalance'].'"
                            UserDefined11="'.$fields['numLowBalanceEvents'].'"
                            UserDefined12="'.$fields['numLowBalanceDays'].'"
                            >
                        </LOANDETAIL1>
                    </LOAN> 
                </NLS>'
            )
        );

        SoapWrapper::service($this->serviceName, function ($service) use ($param) {
            $this->flag = $service->call('ImportXML', $param)->ImportXMLResult;
        });

        return $this->flag;
    }

    /**
     * Parse NLS TU Attributes Data
     * 
     * @param  integer $userId
     * @param  string  $type
     * @return
     */
    public function parseXML( $userId = 0, $loanAppNr, $type = 'initial' )
    {

        $tuAttributesArr = array();
        $FICO      = 0;
        $Vantage   = 0;
        $monthlyPayment = 0;

        try{

            $creditProfileResults = NlsCreditProfile::getCreditProfileByUserId($userId);

            //Decrypt first Data before Inserting it into ODS DB.
            $strKey = self::NLS_KEY;
            
            $bureauData = $creditProfileResults->bureau;
            
            $h = explode( '[000001]', $bureauData);
            $g = explode( '==]', $h[1] );
            $f = substr($g[0],1);
            
            $strData  = $g[1];
            $strIv = $f."==";
            
            $blockSize = 128;
                   
            //Truncate to 32 key
            $key = substr($strKey, 0, 32); 

            //Truncate to 16
            $strIv = substr($strIv, 0 , 16);

            $blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
            $pad       = $blockSize - (strlen($strData) % $blockSize);
            $strData   = $strData . str_repeat(chr($pad), $pad);

            //Mcrypt Decryption
            $crypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($strData), MCRYPT_MODE_CBC, $strIv);
     
            $parseBureauObject = simplexml_load_string(cleanXML($crypttext));
            
            // TU Attributes
            $tuAttributes = $parseBureauObject->product->subject->subjectRecord->addOnProduct;

            foreach($tuAttributes as $tu){
                // TU attributes            
                if($tu->code == '00N05'){
                    foreach($tu->scoreModel->characteristic as $row){
                        if(property_exists($row,'value')){
                          array_push($tuAttributesArr, (int)$row->value);
                        }else{
                          array_push($tuAttributesArr, -1);
                        }                   }
                // FICO 
                }else if($tu->code == '00Q88'){
                    $FICO = $tu->scoreModel->score->results;
                // Vantage  
                }else if($tu->code == '00V60'){
                    $Vantage = $tu->scoreModel->score->results;
                }
            } 
            // get credit monthly payment
            $monthlyPayment = $parseBureauObject->product->subject->subjectRecord->custom->credit->creditSummary->totalAmount->monthlyPayment;
                        
            if( $type != 'initial' ){
                $this->saveBureauData($userId, $loanAppNr, 'final',$tuAttributesArr, $FICO, $Vantage, $monthlyPayment);
            } else {
                $this->saveBureauData($userId, $loanAppNr, 'initial',$tuAttributesArr, $FICO, $Vantage, $monthlyPayment);
            }

        }catch(Exception $e){
            //We need to check the Initial & Final Call
        }
    }

    /**
     * Save NLS Data into ODS
     * 
     * @param  array  $fields
     * @todo  
     *      - change method name
     * @return
     */
    public function saveNLS( $userId, $loanAppNr )
    {
 
        $loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
                                ->where('Loan_Id','=', $loanAppNr)
                                ->select(
                                    'Loan_Portfolio_Id', 
                                    'Loan_Grp_Id', 
                                    'Loan_Template_Id'
                                )
                                ->first();

     
        $fields = array(
            'contactId'        => $userId, 
            'loanId'           => $loanAppNr ,
            'nlsLoanPortfolio' => $loanDetail->Loan_Portfolio_Id ,
            'nlsLoanGroup'     => $loanDetail->Loan_Grp_Id ,
            'nlsLoanTemplate'  => $loanDetail->Loan_Template_Id 
        );
 
        $CreditProfileID    = 0; 
        $CreditLegalItemID  = 0;
        $CreditBankruptcyID = 0;
        $CreditCollectionID = 0;

        $creditProfileResults = array();

        // Retrieve contact ref num and loan ref num using CID and LID
        $loanAcctQuery   = "SELECT TOP 1 acctrefno, cifno, loan_number FROM loanacct WHERE loan_number = '" . $fields['loanId'] . "'"; 
        $loanAcctResults = DB::connection('nls')->select( $loanAcctQuery );

        if( count($loanAcctResults) > 0 ) {
            //Update User Reference Number for User_Acct table
            DB::table('User_Acct')
                ->where('User_Id',$fields['contactId'])
                ->update(array('User_Ref_Nr' => $loanAcctResults[0]->cifno)); 

            //Update Loan Reference Number for Loan_Application and Loan_Detail
            DB::table('Loan_Application')
                ->where('Loan_App_Nr',$fields['loanId'])
                ->update(array('Loan_Ref_Nr' => $loanAcctResults[0]->acctrefno)); 

            DB::table('Loan_Detail')
                ->where('Loan_Id',$fields['loanId'])
                ->update(array('Loan_Ref_Nr' => $loanAcctResults[0]->acctrefno));

                
            $cifQuery  = "SELECT TOP 1 cifno FROM cif WHERE cifnumber = '" . $fields['contactId'] . "'"; 
            $cifResult = DB::connection('nls')->select( $cifQuery );

            //Do a loop for a certain retry just
            //to make sure that we're getting the Credit profile data
            //from TransUnion
            $ctr = 1;

            if( isset($cifResult[0]->cifno) ) {
                while ( $ctr <= (int)Config::get('system.CreditPull.RetryCount') ) {

                    $creditProfileQuery   = "SELECT TOP 1 * FROM CreditProfile WHERE cifno = ". (int) $cifResult[0]->cifno  . ' ORDER BY CreditProfileID DESC' ;
                    $creditProfileResults = DB::connection('nls')->select( $creditProfileQuery );
     
                    if( sizeOf( $creditProfileResults ) > 0 )
                        break;

                    $ctr++;
                }
            }
        }
      
        if( count($creditProfileResults) > 0 ) {
 
        //Save Credit Profile Info        
            foreach( $creditProfileResults as $creditProfile ) {
      
                $CreditProfileID                   = $creditProfile->CreditProfileID;
                
                $lh                                = new NlsCreditProfile();
                $lh->CreditProfileID               = $creditProfile->CreditProfileID;
                $lh->User_Id                       = $fields['contactId'];
                $lh->Cifno                         = $creditProfile->cifno;
                $lh->PulledByUID                   = $creditProfile->PulledByUID;
                $lh->CreditBureauID                = $creditProfile->CreditBureauID;
                $lh->CreditBureauName              = $creditProfile->CreditBureauName;
                $lh->BureauXMLProfile              = $creditProfile->BureauXMLProfile;
                $lh->ReportDate                    = $creditProfile->ReportDate;
                $lh->ReferenceNumber               = $creditProfile->ReferenceNumber;
                $lh->UserDescription               = $creditProfile->UserDescription;
                $lh->ConsumerNameType              = $creditProfile->ConsumerNameType;
                $lh->Surname                       = $creditProfile->Surname;
                $lh->SecondSurname                 = $creditProfile->SecondSurname;
                $lh->FirstName                     = $creditProfile->FirstName;
                $lh->MiddleName                    = $creditProfile->MiddleName;
                $lh->NameSuffix                    = $creditProfile->NameSuffix;
                $lh->SpouseFirstName               = $creditProfile->SpouseFirstName;
                $lh->SpouseSSN                     = $creditProfile->SpouseSSN;
                $lh->DriversLicenseState           = $creditProfile->DriversLicenseState;
                $lh->DateOfBirth                   = $creditProfile->DateOfBirth;
                $lh->YearOfBirth                   = $creditProfile->YearOfBirth;
                $lh->InputSSN                      = $creditProfile->InputSSN;
                $lh->HasNegativeEvaluation         = $creditProfile->HasNegativeEvaluation;
                $lh->HasMatchingSSN                = $creditProfile->HasMatchingSSN;
                $lh->ConsumerSSN                   = $creditProfile->ConsumerSSN;
                $lh->SSNMatchString                = $creditProfile->SSNMatchString;
                $lh->PhoneType                     = $creditProfile->PhoneType;
                $lh->PhoneNumber                   = $creditProfile->PhoneNumber;
                $lh->PhoneSource                   = $creditProfile->PhoneSource;
                $lh->DriversLicenseState           = $creditProfile->DriversLicenseState;
                $lh->DriversLicenseNumber          = $creditProfile->DriversLicenseNumber;
                $lh->DisputedAccountsExcluded      = $creditProfile->DisputedAccountsExcluded;
                $lh->InstallmentBalance            = $creditProfile->InstallmentBalance;
                $lh->RealEstateBalance             = $creditProfile->RealEstateBalance;
                $lh->RevolvingBalance              = $creditProfile->RevolvingBalance;
                $lh->PastDueAmount                 = $creditProfile->PastDueAmount;
                $lh->MonthlyPayment                = $creditProfile->MonthlyPayment;
                $lh->AllTLIncludedInMonthlyPayment = $creditProfile->AllTLIncludedInMonthlyPayment;
                $lh->RealEstatePayment             = $creditProfile->RealEstatePayment;
                $lh->AllTLIncludedInREPayment      = $creditProfile->AllTLIncludedInREPayment;
                $lh->RevolvingAvailablePercent     = $creditProfile->RevolvingAvailablePercent;
                $lh->RevolvingAllTLIncluded        = $creditProfile->RevolvingAllTLIncluded;
                $lh->TotalInquiries                = $creditProfile->TotalInquiries;
                $lh->InquiriesDuringLast6Months    = $creditProfile->InquiriesDuringLast6Months;
                $lh->TotalTradeLines               = $creditProfile->TotalTradeLines;
                $lh->PaidAccountsCount             = $creditProfile->PaidAccountsCount;
                $lh->SatisfactoryAccounts          = $creditProfile->SatisfactoryAccounts;
                $lh->NowDelinquentDerog            = $creditProfile->NowDelinquentDerog;
                $lh->WasDelinquentDerog            = $creditProfile->WasDelinquentDerog;
                $lh->OldestTradeOpenDate           = $creditProfile->OldestTradeOpenDate;
                $lh->DelinquenciesOver30Days       = $creditProfile->DelinquenciesOver30Days;
                $lh->DelinquenciesOver60Days       = $creditProfile->DelinquenciesOver60Days;
                $lh->DelinquenciesOver90Days       = $creditProfile->DelinquenciesOver90Days;
                $lh->DerogCount                    = $creditProfile->DerogCount;
                $lh->PublicRecordsCount            = $creditProfile->PublicRecordsCount;
                $lh->Created_By_User_Id            = $fields['contactId'];
                $lh->Create_Dt                     = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();      
            }

            
            // Get NLS CreditTradeLine data
            $creditTradeLineResults = DB::connection('nls')->select('SELECT TOP 1 * FROM CreditTradeLine where CreditProfileID='.$CreditProfileID.'');
            foreach($creditTradeLineResults as $creditTradeLine){
                // Insert data to ODS
                $lh = new NlsCreditTradeLine();
                $lh->CreditTradeLineID  = $creditTradeLine->CreditTradeLineID;
                $lh->CreditProfileID    = $creditTradeLine->CreditProfileID;
                $lh->Evaluation         = $creditTradeLine->Evaluation;
                $lh->OpenDate           = $creditTradeLine->OpenDate;
                $lh->ClosedDate         = $creditTradeLine->ClosedDate;
                $lh->StatusDate         = $creditTradeLine->StatusDate;
                $lh->MaxDelinquencyDate = $creditTradeLine->MaxDelinquencyDate;
                $lh->AccountType        = $creditTradeLine->AccountType;
                $lh->PaymentFrequency   = $creditTradeLine->PaymentFrequency;
                $lh->TermsDuration      = $creditTradeLine->TermsDuration;
                $lh->ECOA               = $creditTradeLine->ECOA;
                $lh->BalanceDate        = $creditTradeLine->BalanceDate;
                $lh->BalanceAmount      = $creditTradeLine->BalanceAmount;
                $lh->Status             = $creditTradeLine->Status;
                $lh->ExcludeFlag        = $creditTradeLine->ExcludeFlag;
                $lh->ManuallyAddedFlag  = $creditTradeLine->ManuallyAddedFlag;
                $lh->Created_By_User_Id = $fields['contactId'];
                $lh->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();    
            }

            // Get NLS CreditInquiry data
            $creditInquiryResults = DB::connection('nls')->select('SELECT TOP 1 * FROM CreditInquiry where CreditProfileID='.$CreditProfileID.'');
            foreach($creditInquiryResults as $creditInquiry){
                // Insert data to ODS
                $lh = new NlsCreditInquiry();
                $lh->CreditInquiryID       = $creditInquiry->CreditInquiryID;
                $lh->CreditProfileID       = $creditInquiry->CreditProfileID;
                $lh->InquiryDate           = $creditInquiry->InquiryDate;
                $lh->Amount                = $creditInquiry->Amount;
                $lh->TypeCode              = $creditInquiry->TypeCode;
                $lh->Terms                 = $creditInquiry->Terms;
                $lh->AccountNumber         = $creditInquiry->AccountNumber;
                $lh->SubCode               = $creditInquiry->SubCode;
                $lh->KOB                   = $creditInquiry->KOB;
                $lh->SubscriberDisplayName = $creditInquiry->SubscriberDisplayName;
                $lh->Created_By_User_Id    = $fields['contactId'];
                $lh->Create_Dt             = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();    
            }

            // Get NLS CreditLegalItem data
            $creditLegalItemResults = DB::connection('nls')->select('SELECT TOP 1 * FROM CreditLegalItem where CreditProfileID='.$CreditProfileID.'');
            foreach($creditLegalItemResults as $creditLegalItem){
                $CreditLegalItemID = $creditLegalItem->CreditLegalItemID;
                // Insert data to ODS
                $lh = new NlsCreditLegalItem();
                $lh->CreditLegalItemID  = $creditLegalItem->CreditLegalItemID;
                $lh->CreditProfileID    = $creditLegalItem->CreditProfileID;
                $lh->Code               = $creditLegalItem->Code;
                $lh->Description        = $creditLegalItem->Description;
                $lh->EvaluationCode     = $creditLegalItem->EvaluationCode;
                $lh->DateFiled          = $creditLegalItem->DateFiled;
                $lh->CustomerNumber     = $creditLegalItem->CustomerNumber;
                $lh->CustomerName       = $creditLegalItem->CustomerName;
                $lh->IndustryCode       = $creditLegalItem->IndustryCode;
                $lh->Amount             = $creditLegalItem->Amount;
                $lh->Created_By_User_Id = $fields['contactId'];
                $lh->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditPublicRecord data
            $creditPublicRecordResults = DB::connection('nls')->select('SELECT TOP 1 * FROM creditPublicRecord where CreditProfileID='.$CreditProfileID.'');
            foreach($creditPublicRecordResults as $creditPublicRecord){
                // Insert data to ODS
                $lh = new NlsCreditPublicRecord();
                $lh->CreditPublicRecordID      = $creditPublicRecord->CreditPublicRecordID;
                $lh->CreditProfileID           = $creditPublicRecord->CreditProfileID;
                $lh->StatusCode                = $creditPublicRecord->StatusCode;
                $lh->StatusDate                = $creditPublicRecord->StatusDate;
                $lh->FilingDate                = $creditPublicRecord->FilingDate;
                $lh->EvaluationIsNegative      = $creditPublicRecord->EvaluationIsNegative;
                $lh->Amount                    = $creditPublicRecord->Amount;
                $lh->ReferenceNumber           = $creditPublicRecord->ReferenceNumber;
                $lh->PlaintiffName             = $creditPublicRecord->PlaintiffName;
                $lh->IsDisputed                = $creditPublicRecord->IsDisputed;
                $lh->ECOA                      = $creditPublicRecord->ECOA;
                $lh->BankruptcyTypeVoluntary   = $creditPublicRecord->BankruptcyTypeVoluntary;
                $lh->BankruptcyTypeVoluntary   = $creditPublicRecord->BankruptcyTypeVoluntary;
                $lh->BankruptcyLiabilityAmount = $creditPublicRecord->BankruptcyLiabilityAmount;
                $lh->BankruptcyLiabilityAmount = $creditPublicRecord->BankruptcyLiabilityAmount;
                $lh->BankruptcyLiabilityAmount = $creditPublicRecord->BankruptcyLiabilityAmount;
                $lh->BookPageSequence          = $creditPublicRecord->BookPageSequence;
                $lh->Created_By_User_Id        = $fields['contactId'];
                $lh->Create_Dt                 = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();    
            }

            // Get NLS CreditProfileIdentity data
            
            $creditProfileIdentityResults = DB::connection('nls')->select('SELECT TOP 1 * FROM CreditProfileIdentity where CreditProfileID='.$CreditProfileID.'');
            foreach($creditProfileIdentityResults as $creditProfileIdentity){
                // Insert data to ODS
                $lh = new NlsCreditProfileIdentity();
                $lh->CreditProfileIdentityID = $creditProfileIdentity->CreditProfileIdentityID;
                $lh->CreditProfileID         = $creditProfileIdentity->CreditProfileID;
                $lh->NameType                = $creditProfileIdentity->NameType;
                $lh->LastName                = $creditProfileIdentity->LastName;
                $lh->FirstName               = $creditProfileIdentity->FirstName;
                $lh->MiddleName              = $creditProfileIdentity->MiddleName;
                $lh->GenName                 = $creditProfileIdentity->GenName;
                $lh->YOB                     = $creditProfileIdentity->YOB;
                $lh->DOB                     = $creditProfileIdentity->DOB;
                $lh->SpouseFirstName         = $creditProfileIdentity->SpouseFirstName;
                $lh->SpouseSSN               = $creditProfileIdentity->SpouseSSN;
                $lh->PhoneType               = $creditProfileIdentity->PhoneType;
                $lh->PhoneNumber             = $creditProfileIdentity->PhoneNumber;
                $lh->PhoneSource             = $creditProfileIdentity->PhoneSource;
                $lh->DriversLicenseState     = $creditProfileIdentity->DriversLicenseState;
                $lh->DriversLicenseNumber    = $creditProfileIdentity->DriversLicenseNumber;
                $lh->Created_By_User_Id      = $fields['contactId'];
                $lh->Create_Dt               = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditAddressInformation data
            $creditAddressInformationResults = DB::connection('nls')->select('SELECT TOP 1 * FROM CreditAddressInformation where CreditProfileID='.$CreditProfileID.'');
            foreach($creditAddressInformationResults as $creditAddressInformation){
                // Insert data to ODS
                $lh = new NlsCreditAddressInformation();
                $lh->CreditAddressInformationID = $creditAddressInformation->CreditAddressInformationID;
                $lh->CreditProfileID            = $creditAddressInformation->CreditProfileID;
                $lh->FirstReportedDate          = $creditAddressInformation->FirstReportedDate;
                $lh->LastUpdatedDate            = $creditAddressInformation->LastUpdatedDate;
                $lh->Origination                = $creditAddressInformation->Origination;
                $lh->TimesReported              = $creditAddressInformation->TimesReported;
                $lh->LastReportingSubcode       = $creditAddressInformation->LastReportingSubcode;
                $lh->DwellingType               = $creditAddressInformation->DwellingType;
                $lh->AddressLine1               = $creditAddressInformation->AddressLine1;
                $lh->AddressLine2               = $creditAddressInformation->AddressLine2;
                $lh->City                       = $creditAddressInformation->City;
                $lh->State                      = $creditAddressInformation->State;
                $lh->Zipcode                    = $creditAddressInformation->Zipcode;
                $lh->CensusGeoCode              = $creditAddressInformation->CensusGeoCode;
                $lh->CountyCode                 = $creditAddressInformation->CountyCode;
                $lh->AddressStatus              = $creditAddressInformation->AddressStatus;
                $lh->StreetNameMatch            = $creditAddressInformation->StreetNameMatch;
                $lh->CityNameMatch              = $creditAddressInformation->CityNameMatch;
                $lh->StateCodeMatch             = $creditAddressInformation->StateCodeMatch;
                $lh->ZIPCodeMatch               = $creditAddressInformation->ZIPCodeMatch;
                $lh->UnitNumberMatch            = $creditAddressInformation->UnitNumberMatch;
                $lh->Created_By_User_Id         = $fields['contactId'];
                $lh->Create_Dt                  = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditEmploymentInformation data
            $creditEmploymentInformationResults = DB::connection('nls')->select('SELECT TOP 1  * FROM CreditEmploymentInformation where CreditProfileID='.$CreditProfileID.'');
            foreach($creditEmploymentInformationResults as $creditEmploymentInformation){
                // Insert data to ODS
                $lh = new NlsCreditEmploymentInformation();
                $lh->CreditEmploymentInformationID = $creditEmploymentInformation->CreditEmploymentInformationID;
                $lh->CreditProfileID               = $creditEmploymentInformation->CreditProfileID;
                $lh->FirstReportedDate             = $creditEmploymentInformation->FirstReportedDate;
                $lh->LastUpdatedDate               = $creditEmploymentInformation->LastUpdatedDate;
                $lh->IsOriginalInquiry             = $creditEmploymentInformation->IsOriginalInquiry;
                $lh->EmployerName                  = $creditEmploymentInformation->EmployerName;
                $lh->AddressLine1                  = $creditEmploymentInformation->AddressLine1;
                $lh->AddressLine2                  = $creditEmploymentInformation->AddressLine2;
                $lh->AddressLine3                  = $creditEmploymentInformation->AddressLine3;
                $lh->Zipcode                       = $creditEmploymentInformation->Zipcode;
                $lh->Created_By_User_Id            = $fields['contactId'];
                $lh->Create_Dt                     = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditProfileSSN data
            $creditProfileSSNResults = DB::connection('nls')->select('SELECT TOP 1  * FROM CreditProfileSSN where CreditProfileID='.$CreditProfileID.'');
            foreach($creditProfileSSNResults as $creditProfileSSN){
                // Insert data to ODS
                $lh = new NlsCreditProfileSSN();
                $lh->CreditProfileSSNID       = $creditProfileSSN->CreditProfileSSNID;
                $lh->CreditProfileID          = $creditProfileSSN->CreditProfileID;
                $lh->SameAsInputSSN           = $creditProfileSSN->SameAsInputSSN;
                $lh->SSN                      = $creditProfileSSN->SSN;
                $lh->WrongDigitsIndicator     = $creditProfileSSN->WrongDigitsIndicator;
                $lh->SSNDateOfIssue           = $creditProfileSSN->SSNDateOfIssue;
                $lh->SSNStateOfIssue          = $creditProfileSSN->SSNStateOfIssue;
                $lh->SSNDateOfDeath           = $creditProfileSSN->SSNDateOfDeath;
                $lh->SSNStateOfDeath          = $creditProfileSSN->SSNStateOfDeath;
                $lh->SSNDateOfIssueBeginRange = $creditProfileSSN->SSNDateOfIssueBeginRange;
                $lh->SSNDateOfIssueBeginRange = $creditProfileSSN->SSNDateOfIssueBeginRange;
                $lh->Created_By_User_Id       = $fields['contactId'];
                $lh->Create_Dt                = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditProfileProperties data
            $creditProfilePropertiesResults = DB::connection('nls')->select('SELECT TOP 1  * FROM CreditProfileProperties where CreditProfileID='.$CreditProfileID.'');
            foreach($creditProfilePropertiesResults as $creditProfileProperties){
                // Insert data to ODS
                $lh = new NlsCreditProfileProperties();
                $lh->CreditProfilePropertiesID = $creditProfileProperties->CreditProfilePropertiesID;
                $lh->CreditProfileID           = $creditProfileProperties->CreditProfileID;
                $lh->GroupID                   = $creditProfileProperties->GroupID;
                $lh->PropertyType              = $creditProfileProperties->PropertyType;
                $lh->PropertyName              = $creditProfileProperties->PropertyName;
                $lh->PropertyValue             = $creditProfileProperties->PropertyValue;
                $lh->Created_By_User_Id        = $fields['contactId'];
                $lh->Create_Dt                 = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditBankruptcy data
            $creditBankruptcyResults = DB::connection('nls')->select('SELECT TOP 1  * FROM CreditBankruptcy where CreditProfileID='.$CreditProfileID.'');
            foreach($creditBankruptcyResults as $creditBankruptcy){
                $CreditBankruptcyID = $creditBankruptcy->CreditBankruptcyID;
                // Insert data to ODS
                $lh = new NlsCreditBankruptcy();
                $lh->CreditBankruptcyID     = $creditBankruptcy->CreditBankruptcyID;
                $lh->CreditProfileID        = $creditBankruptcy->CreditProfileID;
                $lh->DateFiled              = $creditBankruptcy->DateFiled;
                $lh->CustomerNumber         = $creditBankruptcy->CustomerNumber;
                $lh->CustomerName           = $creditBankruptcy->CustomerName;
                $lh->IndustryCode           = $creditBankruptcy->IndustryCode;
                $lh->IndustryDescription    = $creditBankruptcy->IndustryDescription;
                $lh->CaseNumber             = $creditBankruptcy->CaseNumber;
                $lh->TypeCode               = $creditBankruptcy->TypeCode;
                $lh->TypeDescription        = $creditBankruptcy->TypeDescription;
                $lh->FilerCode              = $creditBankruptcy->FilerCode;
                $lh->FilerDescription       = $creditBankruptcy->FilerDescription;
                $lh->DispositionCode        = $creditBankruptcy->DispositionCode;
                $lh->DispositionDescription = $creditBankruptcy->DispositionDescription;
                $lh->DispositionDate        = $creditBankruptcy->DispositionDate;
                $lh->LiabilityAmount        = $creditBankruptcy->LiabilityAmount;
                $lh->LiabilityCurrency      = $creditBankruptcy->LiabilityCurrency;
                $lh->AssetAmount            = $creditBankruptcy->AssetAmount;
                $lh->AssetCurrency          = $creditBankruptcy->AssetCurrency;
                $lh->ExemptAmount           = $creditBankruptcy->ExemptAmount;
                $lh->ExemptCurrency         = $creditBankruptcy->ExemptCurrency;
                $lh->VerificationDate       = $creditBankruptcy->VerificationDate;
                $lh->PriorIntentCode        = $creditBankruptcy->PriorIntentCode;
                $lh->PriorIntentDescription = $creditBankruptcy->PriorIntentDescription;
                $lh->DateReported           = $creditBankruptcy->DateReported;
                $lh->Created_By_User_Id     = $fields['contactId'];
                $lh->Create_Dt              = DB::raw("CAST(GETDATE() AS DATE)"); 
                //$lh->XIF001CreditBankruptcy     = $creditBankruptcy->XIF001CreditBankruptcy;
                $lh->save();  
            }

            // Get NLS CreditRiskModel data
            $creditRiskModelResults = DB::connection('nls')->select('SELECT TOP 1  * FROM CreditRiskModel where CreditProfileID='.$CreditProfileID.'');
            foreach($creditRiskModelResults as $creditRiskModel){
                // Insert data to ODS
                $lh = new NlsCreditRiskModel();
                $lh->CreditRiskModelID       = $creditRiskModel->CreditRiskModelID;
                $lh->CreditProfileID         = $creditRiskModel->CreditProfileID;
                $lh->ScoreType               = $creditRiskModel->ScoreType;
                $lh->Score                   = $creditRiskModel->Score;
                $lh->ScoreFactor1            = $creditRiskModel->ScoreFactor1;
                $lh->ScoreFactor2            = $creditRiskModel->ScoreFactor2;
                $lh->ScoreFactor3            = $creditRiskModel->ScoreFactor3;
                $lh->ScoreFactor4            = $creditRiskModel->ScoreFactor4;
                $lh->ScoreReasonCode         = $creditRiskModel->ScoreReasonCode;
                $lh->ScoreReasonDescription  = $creditRiskModel->ScoreReasonDescription;
                $lh->RejectReasonCode        = $creditRiskModel->RejectReasonCode;
                $lh->RejectReasonDescription = $creditRiskModel->RejectReasonDescription;
                $lh->Code                    = $creditRiskModel->Code;
                $lh->Status                  = $creditRiskModel->Status;
                $lh->Created_By_User_Id      = $fields['contactId'];
                $lh->Create_Dt               = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditCollection data
            $creditCollectionResults = DB::connection('nls')->select('SELECT TOP 1  * FROM CreditCollection where CreditProfileID='.$CreditProfileID.'');
            foreach($creditCollectionResults as $creditCollection){
                $CreditCollectionID = $creditCollection->CreditCollectionID;
                // Insert data to ODS
                $lh = new NlsCreditCollection();
                $lh->CreditCollectionID           = $creditCollection->CreditCollectionID;
                $lh->CreditProfileID              = $creditCollection->CreditProfileID;
                $lh->DateReported                 = $creditCollection->DateReported;
                $lh->AssignedDate                 = $creditCollection->AssignedDate;
                $lh->CustomerNumber               = $creditCollection->CustomerNumber;
                $lh->CustomerName                 = $creditCollection->CustomerName;
                $lh->IndustryCode                 = $creditCollection->IndustryCode;
                $lh->IndustryDescription          = $creditCollection->IndustryDescription;
                $lh->ClientNameNumber             = $creditCollection->ClientNameNumber;
                $lh->AccountNumber                = $creditCollection->AccountNumber;
                $lh->OriginalAmount               = $creditCollection->OriginalAmount;
                $lh->OriginalAmountCurrency       = $creditCollection->OriginalAmountCurrency;
                $lh->BaseAmount                   = $creditCollection->BaseAmount;
                $lh->BaseAmountCurrency           = $creditCollection->BaseAmountCurrency;
                $lh->DateOfLastPayment            = $creditCollection->DateOfLastPayment;
                $lh->StatusDate                   = $creditCollection->StatusDate;
                $lh->StatusCode                   = $creditCollection->StatusCode;
                $lh->StatusDescription            = $creditCollection->StatusDescription;
                $lh->DateOfFirstDelinquency       = $creditCollection->DateOfFirstDelinquency;
                $lh->AccountDesignatorCode        = $creditCollection->AccountDesignatorCode;
                $lh->AccountDesignatorDescription = $creditCollection->AccountDesignatorDescription;
                $lh->CreditorClassificationCode   = $creditCollection->CreditorClassificationCode;
                $lh->CreditorClassificationDesc   = $creditCollection->CreditorClassificationDesc;
                $lh->UpdateIndicatorCode          = $creditCollection->UpdateIndicatorCode;
                $lh->UpdateIndicatorDescription   = $creditCollection->UpdateIndicatorDescription;
                $lh->Address                      = $creditCollection->Address;
                $lh->City                         = $creditCollection->City;
                $lh->State                        = $creditCollection->State;
                $lh->ZIPCode                      = $creditCollection->ZIPCode;
                $lh->Phone                        = $creditCollection->Phone;
                $lh->Created_By_User_Id           = $fields['contactId'];
                $lh->Create_Dt                    = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            // Get NLS CreditPublicRecordNarrative data
            $creditPublicRecordNarrativeResults = DB::connection('nls')->select('SELECT TOP 1  * FROM CreditPublicRecordNarrative where CreditLegalItemID='.$CreditLegalItemID.' and CreditBankruptcyID='.$CreditBankruptcyID.' and CreditCollectionID='.$CreditCollectionID.'');
            
            foreach($creditPublicRecordNarrativeResults as $creditPublicRecordNarrative){
                // Insert data to ODS
                $lh = new NlsCreditPublicRecordNarrative();
                $lh->PublicRecordNarrativeID = $creditPublicRecordNarrative->PublicRecordNarrativeID;
                $lh->CreditLegalItemID       = $creditPublicRecordNarrative->CreditLegalItemID;
                $lh->CreditBankruptcyID      = $creditPublicRecordNarrative->CreditBankruptcyID;
                $lh->CreditCollectionID      = $creditPublicRecordNarrative->CreditCollectionID;
                $lh->Code                    = $creditPublicRecordNarrative->Code;
                $lh->Description             = $creditPublicRecordNarrative->Description;
                $lh->Created_By_User_Id      = $fields['contactId'];
                $lh->Create_Dt               = DB::raw("CAST(GETDATE() AS DATE)"); 
                $lh->save();  
            }

            $principalPaymentDate = '';
            
            try{
                // // Get NLS Loan Account Payment data
                $loanAccountPaymentResults = DB::connection('nls')->select("SELECT TOP 1  * FROM loanacct_payment where acctrefno=".$loanAcctResults[0]->acctrefno );
                
                foreach($loanAccountPaymentResults as $loanAccountPaymentResult){
                    
                    $principalPaymentDate = strtotime($loanAccountPaymentResult->first_principal_payment_date);
                    
                    //Insert Loan Payment
                    $lh = new LoanPayment();
                    $lh->Borrower_User_Id   = $fields['contactId'];
                    $lh->Loan_Id            = $fields['loanId'];
                    $lh->Loan_Portfolio_Id  = $fields['nlsLoanPortfolio'];
                    $lh->Loan_Grp_Id        = $fields['nlsLoanGroup'];
                    $lh->Loan_Template_Id   = $fields['nlsLoanTemplate'];
                    $lh->Payment_Amt        = $loanAccountPaymentResult->next_payment_total_amount;
                    $lh->Payment_Due_Dt     = date('Y-m-d',$principalPaymentDate);
                    $lh->Created_By_User_Id = $fields['contactId'];
                    $lh->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 
                    $lh->save();      
                } 
            }catch(Exception $e){
                //Do nothing
            }
        }

        return true; 
    }

    /**
     * Save Bureau Data into ODS
     * 
     * @param  integer $uid
     * @param  integer $lid
     * @param  string  $pullType
     * @param  array   $data
     * @param  integer $FICO
     * @param  integer $vantage
     * @param  integer $monthlyPayment     
     *
     * @todo  
     *     - we might need to have descriptive key for bureau data array
     * @return
     */
    public function saveBureauData( $uid = 0, $lid = 0,  $pullType = 'initial', $data = array() , $FICO = 0, $vantage = 0, $monthlyPayment = 0 )
    {
        $tuCredits = TUCredit::where('User_Id', '=', $uid)
                                            ->where('Loan_App_Nr','=', $lid)
                                            ->where('Credit_Pull_Type_Desc','=', $pullType)
                                            ->get();
        if(sizeOf($tuCredits) == 0){
            $tu = new TUCredit();
            $tu->User_Id = $uid;
            $tu->Loan_App_Nr = $lid;
            $tu->Credit_Pull_Type_Desc = $pullType;
            $tu->Fico08_Val = (int)ltrim ($FICO, '+');
            $tu->Vantage_Val = (int)ltrim ($vantage, '+');
            $tu->Credit_Monthly_Payment_Amt = $monthlyPayment;
            $tu->Trans_Union_Access_Dttm = date('Y-m-d H:i:s');

            $tu->AT01 = $data[0];
            $tu->AT03 = $data[1];
            $tu->AT05 = $data[2];
            $tu->AT06 = $data[3];
            $tu->AT07 = $data[4];
            $tu->AT08 = $data[5];
            $tu->AT09 = $data[6];
            $tu->AT10 = $data[7];
            $tu->AT11 = $data[8];
            $tu->AT12 = $data[9];
            $tu->AT13 = $data[10];
            $tu->AT14 = $data[11];
            $tu->AT20 = $data[12];
            $tu->AT21 = $data[13];
            $tu->AT23 = $data[14];
            $tu->AT24 = $data[15];
            $tu->AT25 = $data[16];
            $tu->AT26 = $data[17];
            $tu->AT27 = $data[18];
            $tu->AT28 = $data[19];
            $tu->AT29 = $data[20];
            $tu->AT33 = $data[21];
            $tu->AT34 = $data[22];
            $tu->AT35 = $data[23];
            $tu->AT36 = $data[24];
            $tu->AT99 = $data[25];
            $tu->BR03 = $data[26];
            $tu->BR20 = $data[27];
            $tu->BR28 = $data[28];
            $tu->BR33 = $data[29];
            $tu->FR03 = $data[30];
            $tu->FR33 = $data[31];
            $tu->FR35 = $data[32];
            $tu->RE03 = $data[33];
            $tu->RE10 = $data[34];
            $tu->RE11 = $data[35];
            $tu->RE12 = $data[36];
            $tu->RE13 = $data[37];
            $tu->RE14 = $data[38];
            $tu->RE20 = $data[39];
            $tu->RE28 = $data[40];
            $tu->RE32 = $data[41];
            $tu->RE33 = $data[42];
            $tu->RE34 = $data[43];
            $tu->RE35 = $data[44];
            $tu->BI01 = $data[45];
            $tu->BI03 = $data[46];
            $tu->BI05 = $data[47];
            $tu->BI06 = $data[48];
            $tu->BI07 = $data[49];
            $tu->BI08 = $data[50];
            $tu->BI09 = $data[51];
            $tu->BI20 = $data[52];
            $tu->BI28 = $data[53];
            $tu->FI01 = $data[54];
            $tu->FI03 = $data[55];
            $tu->FI05 = $data[56];
            $tu->FI06 = $data[57];
            $tu->FI07 = $data[58];
            $tu->FI08 = $data[59];
            $tu->FI09 = $data[60];
            $tu->IN03 = $data[61];
            $tu->IN05 = $data[62];
            $tu->IN06 = $data[63];
            $tu->IN07 = $data[64];
            $tu->IN08 = $data[65];
            $tu->IN09 = $data[66];
            $tu->IN10 = $data[67];
            $tu->IN11 = $data[68];
            $tu->IN12 = $data[69];
            $tu->IN13 = $data[70];
            $tu->IN14 = $data[71];
            $tu->IN21 = $data[72];
            $tu->IN28 = $data[73];
            $tu->IN33 = $data[74];
            $tu->IN34 = $data[75];
            $tu->MT01 = $data[76];
            $tu->MT02 = $data[77];
            $tu->MT03 = $data[78];
            $tu->MT04 = $data[79];
            $tu->MT20 = $data[80];
            $tu->MT21 = $data[81];
            $tu->MT22 = $data[82];
            $tu->MT28 = $data[83];
            $tu->MT29 = $data[84];
            $tu->MT32 = $data[85];
            $tu->MT33 = $data[86];
            $tu->MT34 = $data[87];
            $tu->MT35 = $data[88];
            $tu->MT36 = $data[89];
            $tu->MT41 = $data[90];
            $tu->MT42 = $data[91];
            $tu->MT43 = $data[92];
            $tu->MT44 = $data[93];
            $tu->MT45 = $data[94];
            $tu->MT46 = $data[95];
            $tu->MT47 = $data[96];
            $tu->MT48 = $data[97];
            $tu->MT49 = $data[98];
            $tu->MT50 = $data[99];
            $tu->MT51 = $data[100];
            $tu->MT52 = $data[101];
            $tu->MT53 = $data[102];
            $tu->MT54 = $data[103];
            $tu->MT55 = $data[104];
            $tu->MT56 = $data[105];
            $tu->MT57 = $data[106];
            $tu->PF02 = $data[107];
            $tu->PF03 = $data[108];
            $tu->PF05 = $data[109];
            $tu->PF06 = $data[110];
            $tu->PF07 = $data[111];
            $tu->PF08 = $data[112];
            $tu->PF09 = $data[113];
            $tu->PF33 = $data[114];
            $tu->PF34 = $data[115];
            $tu->OF01 = $data[116];
            $tu->OF03 = $data[117];
            $tu->OF20 = $data[118];
            $tu->OF28 = $data[119];
            $tu->OF29 = $data[120];
            $tu->OF33 = $data[121];
            $tu->OF36 = $data[122];
            $tu->ON01 = $data[123];
            $tu->ON03 = $data[124];
            $tu->ON20 = $data[125];
            $tu->ON33 = $data[126];
            $tu->ON34 = $data[127];
            $tu->BC01 = $data[128];
            $tu->BC02 = $data[129];
            $tu->BC03 = $data[130];
            $tu->BC05 = $data[131];
            $tu->BC06 = $data[132];
            $tu->BC07 = $data[133];
            $tu->BC08 = $data[134];
            $tu->BC09 = $data[135];
            $tu->BC10 = $data[136];
            $tu->BC11 = $data[137];
            $tu->BC12 = $data[138];
            $tu->BC13 = $data[139];
            $tu->BC14 = $data[140];
            $tu->BC21 = $data[141];
            $tu->BC29 = $data[142];
            $tu->BC30 = $data[143];
            $tu->BC31 = $data[144];
            $tu->BC34 = $data[145];
            $tu->BC35 = $data[146];
            $tu->BC36 = $data[147];
            $tu->BC98 = $data[148];
            $tu->PB05 = $data[149];
            $tu->PB03 = $data[150];
            $tu->PB06 = $data[151];
            $tu->PB07 = $data[152];
            $tu->PB08 = $data[153];
            $tu->PB09 = $data[154];
            $tu->PB10 = $data[155];
            $tu->PB11 = $data[156];
            $tu->PB12 = $data[157];
            $tu->PB13 = $data[158];
            $tu->PB14 = $data[159];
            $tu->PB20 = $data[160];
            $tu->PB21 = $data[161];
            $tu->PB33 = $data[162];
            $tu->PB35 = $data[163];
            $tu->RT01 = $data[164];
            $tu->RT03 = $data[165];
            $tu->RT05 = $data[166];
            $tu->RT06 = $data[167];
            $tu->RT07 = $data[168];
            $tu->RT08 = $data[169];
            $tu->RT09 = $data[170];
            $tu->RT10 = $data[171];
            $tu->RT11 = $data[172];
            $tu->RT12 = $data[173];
            $tu->RT13 = $data[174];
            $tu->RT14 = $data[175];
            $tu->RT20 = $data[176];
            $tu->RT21 = $data[177];
            $tu->RT28 = $data[178];
            $tu->RT29 = $data[179];
            $tu->RT33 = $data[180];
            $tu->RT34 = $data[181];
            $tu->RT35 = $data[182];
            $tu->RT36 = $data[183];
            $tu->UR03 = $data[184];
            $tu->UR05 = $data[185];
            $tu->UR06 = $data[186];
            $tu->UR07 = $data[187];
            $tu->UR08 = $data[188];
            $tu->UR09 = $data[189];
            $tu->UR10 = $data[190];
            $tu->UR11 = $data[191];
            $tu->UR12 = $data[192];
            $tu->UR13 = $data[193];
            $tu->UR14 = $data[194];
            $tu->UR20 = $data[195];
            $tu->UR21 = $data[196];
            $tu->UR28 = $data[197];
            $tu->UR33 = $data[198];
            $tu->UR35 = $data[199];
            $tu->DS02 = $data[200];
            $tu->DS03 = $data[201];
            $tu->DS04 = $data[202];
            $tu->DS05 = $data[203];
            $tu->DS06 = $data[204];
            $tu->DS07 = $data[205];
            $tu->DS08 = $data[206];
            $tu->DS09 = $data[207];
            $tu->DS10 = $data[208];
            $tu->DS11 = $data[209];
            $tu->DS12 = $data[210];
            $tu->DS13 = $data[211];
            $tu->DS14 = $data[212];
            $tu->DS21 = $data[213];
            $tu->DS33 = $data[214];
            $tu->DS35 = $data[215];
            $tu->G001 = $data[216];
            $tu->G002 = $data[217];
            $tu->G003 = $data[218];
            $tu->G004 = $data[219];
            $tu->G005 = $data[220];
            $tu->G006 = $data[221];
            $tu->G007 = $data[222];
            $tu->G008 = $data[223];
            $tu->G009 = $data[224];
            $tu->G016 = $data[225];
            $tu->G017 = $data[226];
            $tu->G018 = $data[227];
            $tu->G019 = $data[228];
            $tu->G020 = $data[229];
            $tu->G021 = $data[230];
            $tu->G022 = $data[231];
            $tu->G023 = $data[232];
            $tu->G024 = $data[233];
            $tu->G025 = $data[234];
            $tu->G026 = $data[235];
            $tu->G027 = $data[236];
            $tu->G028 = $data[237];
            $tu->G029 = $data[238];
            $tu->G030 = $data[239];
            $tu->G041 = $data[240];
            $tu->G042 = $data[241];
            $tu->G043 = $data[242];
            $tu->G044 = $data[243];
            $tu->G045 = $data[244];
            $tu->G046 = $data[245];
            $tu->G047 = $data[246];
            $tu->G048 = $data[247];
            $tu->G049 = $data[248];
            $tu->G050 = $data[249];
            $tu->G051 = $data[250];
            $tu->G057 = $data[251];
            $tu->G058 = $data[252];
            $tu->G059 = $data[253];
            $tu->G060 = $data[254];
            $tu->G061 = $data[255];
            $tu->G062 = $data[256];
            $tu->G063 = $data[257];
            $tu->G064 = $data[258];
            $tu->G065 = $data[259];
            $tu->G066 = $data[260];
            $tu->G067 = $data[261];
            $tu->G068 = $data[262];
            $tu->G069 = $data[263];
            $tu->G070 = $data[264];
            $tu->G071 = $data[265];
            $tu->G082 = $data[266];
            $tu->G083 = $data[267];
            $tu->G084 = $data[268];
            $tu->G085 = $data[269];
            $tu->G086 = $data[270];
            $tu->G087 = $data[271];
            $tu->G088 = $data[272];
            $tu->G089 = $data[273];
            $tu->G091 = $data[274];
            $tu->G093 = $data[275];
            $tu->G094 = $data[276];
            $tu->G095 = $data[277];
            $tu->G096 = $data[278];
            $tu->G098 = $data[279];
            $tu->G102 = $data[280];
            $tu->G103 = $data[281];
            $tu->G104 = $data[282];
            $tu->S002 = $data[283];
            $tu->S004 = $data[284];
            $tu->S008 = $data[285];
            $tu->S009 = $data[286];
            $tu->S010 = $data[287];
            $tu->S011 = $data[288];
            $tu->S012 = $data[289];
            $tu->S014 = $data[290];
            $tu->S015 = $data[291];
            $tu->S018 = $data[292];
            $tu->S019 = $data[293];
            $tu->S020 = $data[294];
            $tu->S027 = $data[295];
            $tu->S040 = $data[296];
            $tu->S043 = $data[297];
            $tu->S046 = $data[298];
            $tu->S054 = $data[299];
            $tu->S055 = $data[300];
            $tu->S059 = $data[301];
            $tu->S060 = $data[302];
            $tu->S061 = $data[303];
            $tu->S062 = $data[304];
            $tu->S063 = $data[305];
            $tu->S064 = $data[306];
            $tu->S065 = $data[307];
            $tu->S066 = $data[308];
            $tu->S078 = $data[309];
            $tu->S079 = $data[310];
            $tu->S114 = $data[311];
            $tu->S115 = $data[312];
            $tu->S0Y2 = $data[313];

            $tu->Created_By_User_Id = $uid;
            $tu->Create_Dt = date('Y-m-d');
            $tu->save();
            //print 'Done saveBureauData<br />';
        }else{

        }
    }


}