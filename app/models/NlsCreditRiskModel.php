<?php

class NlsCreditRiskModel extends Eloquent{

	protected $table = 'NLS_CreditRiskModel';

	protected $primaryKey = 'CreditRiskModelID';

	public $timestamps = false;

}