<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 *   - Added GfAuth Helper
 */
class ApplicantRiskInfo extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table      = 'Applicant_Risk_Info';
	
	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'Loan_App_Nr';
	
	/**
	 * Timestamps
	 * @var boolean
	 */
	public $timestamps    = FALSE; 
	
	/**
	 * Fillable Fields
	 * @var array
	 */
	protected $fillable   = array( 
									 'User_Id'
									,'Loan_App_Nr'
									,'Risk_Score_Ver_Nr'
									,'Risk_Segment_Nr'
									,'Risk_Score_Dt'
									,'Risk_Score_Val'
									,'Created_By_User_Id'
									,'Create_Dt'
							);


	/**
	 * Get Risk Score Value 
	 * 
	 * @param  integer $loanAppNr
	 * @return array
	 */
	public static function getRiskScore( $loanAppNr )
	{
		return ApplicantRiskInfo::where('Loan_App_Nr', '=', $loanAppNr)
								->select('Risk_Score_Val')
								->first();
	} 

	/**
	 * Store Failed Score Information
	 * 
	 * @param  integer $loanAppNr
	 * @param  integer $userId
	 * @param  integer $score
	 * @return 
	 */
	public static function storeFailedARSScore($loanAppNr, $userId, $score )
	{
		ApplicantRiskInfo::updateOrCreate( 
				array('Loan_App_Nr' => $loanAppNr ), 
				array(
					'User_Id'            => $userId, 
					'Loan_App_Nr'        => $loanAppNr,
					'Risk_Score_Ver_Nr'  => '1.0',
					'Risk_Segment_Nr'    => 0,
					'Risk_Score_Dt'      => date('Y-m-d'),
					'Risk_Score_Val'     => $score,
					'Created_By_User_Id' => $userId,
					'Create_Dt'          => date('Y-m-d'),
				)
		);
	}

	/**
    * Compute Applicant Credit Score
    * 
    * @param  string  $type
    * @param  string  $riskScoreNumber
    * @param  string  $partnerID
    * @param  string  $stateCd
    * @param  integer $test
    * @param  array   $data
    * @return 
    */
	public static function computeCreditScore( $uid, $lid, $type = 'initial', $riskScoreNumber = '1', $partnerID = 'xx', $stateCd = 'xx', $test = 0, $data = array() )
	{	

		$segment        = 3;
		$ARS_Score      = 0;
		$adverseActions = array();

		if( $test ){
			$uid    = 0; 
			$tuData = $data;
        }else{
            $tuData = TUCredit::find($uid);
    	}

    	//Adverse Reason Code
    	if( count($tuData) > 0 ){
	    	if( $tuData['AT20'] <= 12 || $tuData['AT12'] <= 1 ){
	        	$segment = 1;
	        }else if($tuData['MT02'] >= 1){
	        	$segment = 2;
	        }
    	}
        
        if( $test )
        	print '<br />Segment: '.$segment.'<br />';


        //ARS Scoring Store Procedure
		$ARS_Score = DB::select("EXEC usp_ARS_Score_Val ?,?,?,?,?", array('1.0', 'Prtnr', 'CA', $segment, $uid ) );

		//Update or Create Application Risk Info Table\
		//@todo - need to update the Risk Score Version Number using ODS table
		if( count( $ARS_Score ) > 0 ){

			$riskInfo  = ApplicantRiskInfo::updateOrCreate( 
				array('Loan_App_Nr' => $lid ), 
				array(
					'User_Id'            => $uid, 
					'Loan_App_Nr'        => $lid,
					'Risk_Score_Ver_Nr'  => '1.0',
					'Risk_Segment_Nr'    => $segment,
					'Risk_Score_Dt'      => date('Y-m-d'),
					'Risk_Score_Val'     => $ARS_Score[0]->Final_ARS_Score_Val,
					'Created_By_User_Id' => $uid,
					'Create_Dt'          => date('Y-m-d'),
				)
			 );
		}

		//Adverse Action Code Store Procedure
		$adverseResults = DB::select("EXEC usp_Adverse_Action_Cd ?,?,?", array('1.0', $segment, $uid ) );

		if( count($adverseResults) > 0 ) {

			foreach( $adverseResults as $adverseResult ){
				$adverseDetail =  new AdverseAction();
				$adverseDetail->User_Id            = $uid;
				$adverseDetail->Loan_App_Nr        = $lid;
				$adverseDetail->Risk_Score_Ver_Nr  = '1.0';
				$adverseDetail->Risk_Segment_Nr    = $segment;
				$adverseDetail->Risk_Score_Dt      = date('Y-m-d');
				$adverseDetail->Risk_Score_Val     = $ARS_Score[0]->Final_ARS_Score_Val;
				$adverseDetail->Adverse_Action_Cd  = $adverseResult->Adverse_Action_Cd;
				$adverseDetail->Created_By_User_Id = $uid;
				$adverseDetail->Create_Dt          = date('Y-m-d');
				$adverseDetail->save();	
			}

			$scoreResults = array(
				'ARS_SCORE'      => $ARS_Score,
				'adverseActions' => $adverseResults
			);

			return $scoreResults;
		}
	}

	/**
	 * Compute Applicant Credit Score
	 * 
	 * @param  string  $type
	 * @param  integer $test
	 * @param  array   $testData
	 * @return integer
	 */
	protected static function getCreditScore( $uid, $lid, $type = 'initial', $isTest = 0, $testData = array())
	{
		//Inital Scoring
		if( $type == 'initial' ){
			//Test mode
			if( $isTest == 1 ){

				$scoreResults   = self::computeCreditScore($uid, $lid, 'initial','0','xx', 'xx', 1, $testData);
				$adverseActions = $scoreResults['adverseActions'];

				print 'Adverse Reason Codes:<br />';

				if( count($adverseActions) > 0 ){

					eval('print "<br />-".self::X'.$adverseActions[0]->Adverse_Action_Cd.';');
					eval('print "<br />-".self::X'.$adverseActions[1]->Adverse_Action_Cd.';');
					eval('print "<br />-".self::X'.$adverseActions[2]->Adverse_Action_Cd.';');
					eval('print "<br />-".self::X'.$adverseActions[3]->Adverse_Action_Cd.';');
				}

				print '<br /><br />ARS_SCORE: '.$scoreResults['ARS_SCORE'][0]->Final_ARS_Score_Val;

				$score = $scoreResults['ARS_SCORE'][0]->Final_ARS_Score_Val;

			}else{
				$scoreResults   = self::computeCreditScore($uid, $lid, 'initial');					
				$adverseActions = $scoreResults['adverseActions'];
				$score          = $scoreResults['ARS_SCORE'][0]->Final_ARS_Score_Val;
			}
		//Final Scoring
		}else{
			//Test mode
			if( $isTest == 1 ){
				
				$scoreResults   = self::computeCreditScore($uid, $lid, 'final','0','xx', 'xx', 1, $testData);
				$adverseActions = $scoreResults['adverseActions'];

				print 'Adverse Reason Codes:<br />';

				if( count($adverseActions) > 0 ){

					eval('print "<br />-".self::X'.$adverseActions[0]->Adverse_Action_Cd.';');
					eval('print "<br />-".self::X'.$adverseActions[1]->Adverse_Action_Cd.';');
					eval('print "<br />-".self::X'.$adverseActions[2]->Adverse_Action_Cd.';');
					eval('print "<br />-".self::X'.$adverseActions[3]->Adverse_Action_Cd.';');
				}

				print '<br /><br />ARS_SCORE: '.$scoreResults['ARS_SCORE'][0]->Final_ARS_Score_Val;

				$score = $scoreResults['ARS_SCORE'][0]->Final_ARS_Score_Val;

			}else{
				$scoreResults   = self::computeCreditScore($uid, $lid, 'final');
				$adverseActions = $scoreResults['adverseActions'];
				$score          = $scoreResults['ARS_SCORE'][0]->Final_ARS_Score_Val;
			}
		}

		return array(
			'adverseActions' => $adverseActions, 
			'score'			 => $score
		);
	} 
}