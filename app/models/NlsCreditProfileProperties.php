<?php

class NlsCreditProfileProperties extends Eloquent{

	protected $table = 'NLS_CreditProfileProperties';

	protected $primaryKey = 'CreditProfilePropertiesID';

	public $timestamps = false;
}