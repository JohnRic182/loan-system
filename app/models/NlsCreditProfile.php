<?php

class NlsCreditProfile extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'NLS_CreditProfile';

	/**
	 * Primary Key
	 * @var string
	 */
	protected $primaryKey = 'CreditProfileID';

	/**
	 * Timestamps
	 * @var boolean
	 */
    public $timestamps = false;

    /**
     * Is Credit Profile Exist
     *  
     * @param  integer  $userId
     * @return boolean
     */
    public static function isCreditProfileExist( $userId )
    {
    	return NlsCreditProfile::where('User_Id', '=', $userId )
    				->get()  
    				->count();
    }

    /**
     * Get Credit Profile By User Id
     * 
     * @param  integer $userId
     * @return array
     */
    public static function getCreditProfileByUserId( $userId = 0 )
    {
    	return NlsCreditProfile::select( DB::raw('cast(BureauXMLProfile as TEXT) as bureau') )
							->where( 'User_Id', '=', $userId )
							->first();	
    }
}