<?php
/**
 * Developer's Note
 *
 *   - 
 */
class UserPartnerSrc extends Eloquent{


	protected $table = 'User_Partner_Src';

	protected $fillable = array('Partner_Id', 'Campaign_Id', 'User_Id' );

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';

	/**
	 * [trackPartner description]
	 * @param  array $data should contain = Partner_Id, Campaign_Id, User_Id, Created_By_User_Id, Create_Dt
	 * @return [type]     
	 */
	public function trackPartner($data){

		$where = array(
			'Partner_Id' 	=> $data['Partner_Id'],
			'Campaign_Id' 	=> $data['Campaign_Id'],
			'User_Id' 		=> $data['User_Id']
		);

		$result = UserPartnerSrc::where($where)->get();

		if(count($result) > 0)
			$response = DB::table($this->table)->update($data)->where($where);
		else
			$response = DB::table($this->table)->insert($data);

		return $response;
	}

}