<?php
/**
 * Marketing funnel table
 *
 * @version  0.1
 */

class VwRptMktgFunnel extends Eloquent{

	/**
	 * Table Name
	 * @var string
	 */
	protected $table = 'vw_Rpt_Mktg_Funnel';
 

 	protected $primaryKey = 'User_Id';


	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();
		Gfauth::decryptData();

	}

	public function getMktgFunnelData($startDate, $endDate){
		
		$data = DB::table($this->table)
		->whereBetween('User_Acct_Create_Dt', array( $startDate, $endDate ))->orderBy('User_Acct_Create_Dt')->get();
			
		return $data;
	}	

	 
}