<?php

/**
 * Loan Application Model
 *
 *
 * @version  1.0
 * @author   Global Fusion <ascend@global-fusion.net>
 *
 * Develope's Note
 *
 *  - Added Loan Payment Calculation
 *  - Added Loan Payment Date Calculation
 */
class LoanApplication extends Eloquent{

    /**
     * Loan Application table 
     * 
     * @var string
     */
	protected $table = 'Loan_Application';

    /**
     * Table Primary Key
     * 
     * @var string
     */
	protected $primaryKey = 'Loan_App_Nr';

    /**
     * Timestamps
     * 
     * @var boolean
     */
    public $timestamps = false;
        

    public function ___construct()
    {
        parent::___construct();
    }

    /**
     * Check Wether the Loan Application is Active
     * 
     * @param  integer  $userId
     * @param  integer  $loanAppNr
     * @return boolean
     */
    public static function isLoanApplicationActive( $userId, $loanAppNr )
    {
        return self::where(array('Applicant_User_Id' => $userId, 'Loan_App_Nr' => $loanAppNr))
                         ->select('Status_Desc')
                         ->first();
    }


    /**
     * Check Wether the Loan Application is Active
     * 
     * @param  integer  $userId
     * @param  integer  $loanAppNr
     * @return boolean
     */
    public static function hasExistingLoan( $userId  )
    {
       
        return self::where('Applicant_User_Id', '=', $userId )
                            ->select('Loan_App_Nr')
                            ->orderBy('Create_Dt', 'DESC')
                            ->first();
    }
    
    /**
     * Get Latest Application
     * 
     * @param  integer $userId
     * @return
     */
    public static function getLatestApplication( $userId )
    {
        return self::where('Applicant_User_Id', '=', $userId )
                        ->orderBy('Loan_App_Nr', 'ASC')
                        ->select('Loan_App_Nr', 'Loan_Purpose_Id', 'Reco_Loan_Prod_Id','Applicant_User_Id','Status_Desc')
                        ->first();
    }


    /**
     * Get Active Loan Application Phase
     * 
     * @param  integer $userId
     * @param  integer $loanAppNr
     * @return
     */
    public static function getActiveLoanAppPhase( $userId, $loanAppNr )
    {
        return self::where(
                    array(
                        'Applicant_User_Id' => $userId, 
                        'Loan_App_Nr'       => $loanAppNr
                    )
                )->select('Actv_Loan_App_Phase_Nr')
                 ->first();
    }

     /**
     * Set Active Loan Application Phase
     * 
     * @param integer $userId
     * @param integer $loanAppNr
     * @param integer $loanAppPhaseNr
     */
    public static function setActiveLoanAppPhase( $userId, $loanAppNr, $loanAppPhaseNr)
    {
        self::where(
            array(
                'Applicant_User_Id' => $userId, 
                'Loan_App_Nr'       => $loanAppNr, 
            )
         )
        ->update( array( 'Actv_Loan_App_Phase_Nr' => $loanAppPhaseNr ) );
                        
    }

    /**
     * Insert New Loan Application 
     * 
     * @param  array  $fields
     * @return
     */
    protected static function newLoanApplication( $fields = array() )
    {

        if( count( $fields ) > 0 ) {
            $LoanApplication = new LoanApplication();
            $LoanApplication->Applicant_User_Id    = $fields['userId'];         
           
            $LoanApplication->Campaign_Id          = "0";
            $LoanApplication->Funding_Partner_Id   = "0";

            if( Session::has('partner') ){
                $LoanApplication->Campaign_Id          = Session::get('partner.CID');
                $LoanApplication->Funding_Partner_Id   = Session::get('partner.PID');
            }

            if( isset( $fields['campaignID'] ) )
                $LoanApplication->Campaign_Id          = $fields['campaignID'];

            if( isset( $fields['partnerID'] ) )
                $LoanApplication->Funding_Partner_Id   = $fields['partnerID'];
            

            $LoanApplication->Marketing_Partner_Id = "0";
            $LoanApplication->Banner_Id            = "0";
            $LoanApplication->Loan_Ref_Nr          = "0";
            $LoanApplication->Loan_Purpose_Id      = $fields['loanPurposeId'];
            $LoanApplication->Status_Desc          = "0";
            $LoanApplication->App_Start_Dt         = date('Y-m-d');
            $LoanApplication->App_End_Dt           = NULL;
            $LoanApplication->Created_By_User_Id   = $fields['userId'];
            $LoanApplication->Create_Dt            = date('Y-m-d');
            $LoanApplication->save(); 
        } 
    }


    /**
     * Update Or Create Loan Application
     * 
     * @param  array   $fields
     * @param  integer $updateFlag
     * @return
     */
    protected static function updateOrCreateLoanApp( $loanAppNr = "", $fields = array(), $updateFlag )
    {
 
        if( $updateFlag && !empty( $loanAppNr ) ){

            $loanApplCnt = self::where('Applicant_User_Id', '=', $fields['userId'])
                                        ->where('Loan_App_Nr', '=', $loanAppNr )
                                        ->get()
                                        ->count();

            if( count($loanApplCnt) > 0 ) {

                self::where('Applicant_User_Id','=', $fields['userId'] )
                            ->where('Loan_App_Nr', '=', $loanAppNr )
                            ->update(array( 
                                'Loan_Purpose_Id' => $fields['loanPurposeId']
                            ));

            } else {
                self::newLoanApplication($fields);
                self::setNewLoanAppNr($fields['userId']);
            }
        }else{ 
            self::newLoanApplication($fields); 
            self::setNewLoanAppNr($fields['userId']);
        }
    }

    /**
     * Set New Loan Application Number After Creating Loan Application 
     * 
     * @param integer $userId
     */
    public static function setNewLoanAppNr( $userId )
    {
        $loanObj = self::where('Applicant_User_Id', '=', $userId )
                                    ->orderBy('Loan_App_Nr', 'DESC')
                                    ->select('Loan_App_Nr')
                                    ->first();
        if( !empty( $loanObj ) )    
            setLoanAppNr( $loanObj->Loan_App_Nr ); 
    }

 
}