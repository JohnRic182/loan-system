<?php

/**
 * Developer's Note
 *
 *   - Added comments and class constructor
 */
class ApplicantIncomeVerification extends Eloquent{

	/*
	|--------------------------------------------------------------------------
	| Applicant Employment Info Model
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route
	|
	*/

	protected $table = 'Applicant_Income_Verification';

	/**
	 * Override Created Date
	 */
	const CREATED_AT = 'Create_Dt';

	/**
	 * Override Updated Date
	 */
	const UPDATED_AT = 'Update_Dt';
  

	/**
 	 * Encrypted fields 
 	 * 
 	 * @var array
 	 */
	protected static $encryptedFields = array('Income_Doc_Image_URL_Txt');


	public function __construct()
	{
		parent::__construct();
		//run data decryption
		Gfauth::decryptData();
	}

	/**
	 * Get Data 
	 *
	 * @todo   remove the old getData and rename this one
	 * @param  array  $fields
	 * @param  array  $where
	 * @return
	 */
	public static function getData($fields = array(), $where = array(), $isRow = false ) 
	{
		$encryptedFieldArr  = Gfauth::encryptFieldArray(self::$encryptedFields, $fields);

	 
		if( $isRow == true )
			return ApplicantIncomeVerification::where($where)->select($encryptedFieldArr)->first();

		return ApplicantIncomeVerification::where($where)->select($encryptedFieldArr)->get();
	}

	/**
	 * Create New Applicant Income Verification 
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  json    $imageUrlTxt
	 * @param  array   $data
	 * @return
	 */
	public static function newIncomeVerification( $data = array() )
	{

		$AppIncomeInfo = new ApplicantIncomeVerification(); 
		$AppIncomeInfo->User_Id                  = $data['userId'];
		$AppIncomeInfo->Loan_App_Nr              = $data['loanAppNr'];
		$AppIncomeInfo->Employment_Type_Cd       = $data['Employment_Type_Cd'];
		$AppIncomeInfo->Income_Doc_Image_URL_Txt = Gfauth::encryptFields($data['imageUrlTxt']); 
		$AppIncomeInfo->Created_By_User_Id       = $data['userId']; 
		$AppIncomeInfo->Create_Dt                = date('Y-m-d');  
		$AppIncomeInfo->save(); 
	}

}