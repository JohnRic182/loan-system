<?php

/**
 * NLS Loan Account Payment
 *
 * @deprecated
 */

class NLSLoanAcctPayment extends Eloquent{

	protected $table      = 'NLS_LoanAcct_Payment';
	
	protected $primaryKey = null;
	
	public $timestamps    = false;	

}