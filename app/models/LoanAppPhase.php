<?php

/**
 * Loan Application Phase Model
 *
 *
 * @version  1.0
 * @author   Global Fusion <ascend@global-fusion.net>
 *
 * Develope's Note
 *
 *  - Added Loan Payment Calculation
 *  - Added Loan Payment Date Calculation
 */
class LoanAppPhase extends Eloquent{

	protected $table      = 'Loan_App_Phase';
	
	protected $primaryKey = 'Loan_App_Phase_Id';
	
	public    $timestamps = false;
	
	public static function getLoanAppPhaseById( $loanAppPhaseId )
	{
		return LoanAppPhase::find($loanAppPhaseId);
	}	

}