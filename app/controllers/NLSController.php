<?php

/**
 * Developers Notes
 * 
 * Notes:
 *   - Added CheckLoanStatus - checking of Loan Application status for BOTH 
 */

class NLSController extends BaseController {
	
	/*
	|--------------------------------------------------------------------------
	| NLS Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'NLSController@index');
	|
	*/
		
	/**
	 * NLS Tasks Names
	 */
	const TASK_NAME_EMPLOYMENT          = 'CONFIRM_EMPLOYMENT';
	const TASK_NAME_INCOME              = 'CONFIRM_INCOME';
	const TASK_NAME_FUNDING             = 'CONFIRM_PMT_ACCT';
	const TASK_NAME_BANK            	= 'CONFIRM_BANK_ACCOUNT ';
	const TASK_NAME_CONTRACT            = 'REVIEW_CONTRACT';
	const TASK_NAME_DOCUMENTS           = 'CONFIRM_ID';
	const TASK_NAME_IDENTITY        	= 'ID_VERIFIED'; 
	const TASK_NAME_PAYMENT             = 'REVIEW_PAYMENT_METHOD';
	const TASK_NAME_FINAL_APPROVAL      = 'FINAL_APPROVAL'; 
		
	/**
	 * Comment Category
	 */
	const COMMENT_CATEGORY_CONTRACT     = 'EXECUTED_CONTRACT'; 
	const COMMENT_CATEGORY_IDENTITY     = 'PROOF_OF_IDENTITY'; 
	const COMMENT_CATEGORY_INCOME       = 'PROOF_OF_INCOME';  
	const COMMENT_CATEGORY_FUNDING      = 'CONFIRM_BANK_ACCOUNT';
	
	/**
	 * NLS Application Status Codes
	 */
	const STATUS_CODE_STEP1             = 'APPLICATION_STEP1'; 	    
	const STATUS_CODE_STEP2             = 'APPLICATION_STEP2';
	const STATUS_CODE_STEP3             = 'APPLICATION_STEP3';
	const STATUS_CODE_STEP4             = 'APPLICATION_STEP4';
	const STATUS_CODE_REJECTED          = 'APPLICATION_REJECTED';
	const STATUS_CODE_VERIFICATION      = 'VERIFICATION';
	const STATUS_CODE_FOR_APPROVAL      = 'FOR_APPROVAL';
	const STATUS_CODE_LOAN_APPROVED     = 'LOAN_APPROVED';
	const STATUS_CODE_LOAN_FUNDED       = 'LOAN_FUNDED';
	const STATUS_CODE_APPROVAL_REQUIRED = 'APPROVAL_REQUIRED';
	const STATUS_CODE_INCOMPLETE        = 'INCOMPLETE';
		
	/**
	 * NLS Tasks Code
	 */
	const NLS_STATUS_CODE_APPROVED      = '100002';
	const NLS_STATUS_CODE_NOT_STARTED   = '100001';
	const NLS_STATUS_CODE_OPEN          = 1;
	const NLS_STATUS_CODE_DENIED        = '100004';
	
	/**
	 * NLS Status Final Approval
	 */
	const NLS_TASK_STATUS_APPROVED      = 'APPROVED';
	const NLS_TASK_STATUS_DENIED        = 'DENIED';
 
	/**
	 * NLS Loan Template
	 */
	const NLS_LOAN_TEMPLATE_FINAL_APPROVAL = 6;


	/**
	 * NLS Loan Template
	 */
	const VERIFICATION_TOTAL_TASK = 8;


	/**
	 * NLS Loan Template
	 */
	const LOAN_TERM = 36;

	/**
	 * Applicant Email 
	 * 
	 * @var string
	 */
	private $email = '';

	/**
	 * Class constructor
	 */
	public function __construct() {}

	/**
	 * Check and Create a FINAL_APPROVAL task
	 * if All Verification tasks have been Approved.
	 * 
	 * @return mixed
	 */
	public function processVerificationTasks()
	{	

		try{ 

			$NLSTask = new NLSTask();
  
			//Select all Loan Applicaton that has status of verification
			$loanApplication  =  LoanDetail::where('Loan_Status_Desc', '=', self::STATUS_CODE_VERIFICATION )  
											// ->where('Loan_Ref_Nr', '!=', '')
											->join('Loan_Group', 'Loan_Group.Loan_Grp_Id', '=', 'Loan_Detail.Loan_Grp_Id')
											->join('Loan_Template', 'Loan_Template.Loan_Template_Id', '=', 'Loan_Detail.Loan_Template_Id')
											->join('Loan_Portfolio', 'Loan_Portfolio.Loan_Portfolio_Id', '=', 'Loan_Detail.Loan_Portfolio_Id')
											->orderBy('Loan_Id', 'DESC')
											->select(array(
												 'Borrower_User_Id'
												,'Loan_Id'
												,'Loan_Template_Name'
												,'Loan_Portfolio_Name'
												,'Loan_Grp_Name'
												,'Loan_Ref_Nr'
											))
											->get();
				
			// _pre($loanApplication);

			$loanIds       = array();
			$userIds       = array();
			$loanTemplate  = array(); 
			$loanPortfolio = array(); 
			$loanGroup     = array(); 

			echo 'Number of Loan Application: ' .  count($loanApplication) . '<br>';

			//Get Loan Ids, User Ids, Template, Portfolio and Group from 
			//Result. Else Exit the method
			if( count( $loanApplication ) > 0 ) {
				foreach ($loanApplication as $key => $loan) {
					$loanIds[]                     = trim( $loan->Loan_Id );
					$userIds[$loan->Loan_Id]       = $loan->Borrower_User_Id;
					$loanPortfolio[$loan->Loan_Id] = $loan->Loan_Portfolio_Name;
					$loanTemplate[$loan->Loan_Id]  = $loan->Loan_Template_Name;
					$loanGroup[$loan->Loan_Id]     = $loan->Loan_Grp_Name;
				}
			} else {
				exit();
			}


			// _pre($loanIds);
			
			//Get Matching Acct Reference Number of Loan Application from 
			//NLS.loanacct table
			$NLSLoanAcct = new NLSLoanAcct(); 
			$cifNoResult = $NLSLoanAcct->whereIn('loan_number', $loanIds )
								 	   ->select('acctrefno', 'loan_number')
								 	   ->orderBy('loan_number', 'DESC')
								       ->get();

			echo 'Number of Corresponding CIF Number: ' .  count($cifNoResult) . '<br>';

			$cifNos   = array(); 
			$loansArr = array(); 

			// _pre($cifNoResult);

			if( count( $cifNoResult )  > 0 ) {
				foreach ($cifNoResult as $key => $value) {
					$cifNos[$value->acctrefno] = $value->acctrefno;
					$loansArr[$value->acctrefno] = $value->loan_number;
				}
			}
 			

			//Get Loan Application Task from task table
			//It should check each 6 tasks created in verification steps
			//a. contract, b.payment, c.funding, d.employment, e.identity, f.income
			$tasks = array();

			// pre($cifNos);
		
			if( count($cifNoResult) > 0 ){
				$tasks = $NLSTask->getTasksByRefNoExcludeTemplateNo( 
					$cifNos , 
					self::NLS_LOAN_TEMPLATE_FINAL_APPROVAL 
				);
			}
			
			echo 'Number of Task Created: ' .  count($tasks) . '<br>';
			// _pre($tasks);
			$tasksGroupByNLSNr =  array();

			//Format the result array by using the NLS Number 
			//as Key for easy checking of FINAL APPROVAL Loans
			if( count( $tasks ) > 0 ) {
				foreach( $tasks as $key => $task ) {

					$taskTemplate = getNLSTaskTemplateName( $task->task_template_no );
					
					$tasksGroupByNLSNr[$loansArr[$task->NLS_refno]][] = array(
						'statusCodeId'   => $task->status_code_id, 
						'subject'        => $task->subject,
						'statusCodeDesc' => $task->status_code,
						'NLSRefno'		 => $task->NLS_refno,
						'TaskRefno'		 => $task->task_refno,
						'TaskTempName'   => $taskTemplate->task_template_name
					); 
				}
			}
			$finalApprovalLoans = array(); 

			echo 'Tasks Group based on Loan Application Number';
 	

 			// pre($tasksGroupByNLSNr); 

			//Determine whether the task has been approved or denied.
			//If one of the tasks has beend DENIED, Loan Application 
			//will be REJECTED. Else, if all tasks have been APPROVED,
			//the application will be mark as APPROVAL_REQUIRED and will 
			//create a FINAL_APPROVAL task
			if( count($tasksGroupByNLSNr) > 0 ) {

				foreach ($tasksGroupByNLSNr as $key => $loan ) { 

					// _pre($loan);

					//Process those task with complete verification task items
					if( count($loan) >= self::VERIFICATION_TOTAL_TASK ) {

						//Set the default status code for Approval Required
						$statusCode = self::STATUS_CODE_APPROVAL_REQUIRED; 

						$status  = array(); 

						//Collect all tasks status
						foreach( $loan as $k => $task ) { 
							$status[] = $task['statusCodeId']; 
						}

						//Trapped all DENIED task, reject the application 
						if( in_array(self::NLS_STATUS_CODE_DENIED, $status ) ){
							$statusCode = self::STATUS_CODE_REJECTED;
						} else {
							//Check if the tasks still incomplete
							if(in_array(self::NLS_STATUS_CODE_NOT_STARTED, $status ) ) {
								$statusCode = self::STATUS_CODE_INCOMPLETE;
							}
						}

						$nlsRefNo  = $loan[0]['NLSRefno']; 

						//Check if Applicant Exist
						if( isset($userIds[$key]) ) {
							$finalApprovalLoans[$key] = array( 
								'statusCode'    => $statusCode ,
								'userId'        => $userIds[$key], 
								'loanGroup'     => $loanGroup[$key],
								'loanPortfolio' => $loanPortfolio[$key], 
								'loanTemplate'  => $loanTemplate[$key],
								'NLSRefno'		=> $nlsRefNo
							);
						}
					}
				}
			}

			 //_pre($finalApprovalLoans);
			 //die;
			if( count( $finalApprovalLoans ) > 0 ) {
				foreach ( $finalApprovalLoans as $key => $status ) {
					//if loan status is complete, create another task
					if( $status['statusCode'] != self::STATUS_CODE_INCOMPLETE ) {

						//Update Loan Status Code if the status code is APPLICATION_REJECTED 
						if( $status['statusCode'] == self::STATUS_CODE_REJECTED ) {
							echo 'Updating Not started tasks:' . $key . '<br>';
							
							// update Not Started tasks to Loan App Closed
							updateNotStartedTasks($key , $status, self::NLS_STATUS_CODE_NOT_STARTED );

							echo 'Updating Loan Status:' . $key . '<br>';
							// update Loan status to APPLICATION_REJECTED
							$this->updateLoanStatus($key , $status );
							 
						}

						//Create Final Approval Task for Completed Verification Tasks
						if( $status['statusCode'] == self::STATUS_CODE_APPROVAL_REQUIRED ) {

							//echo 'creating Final Approval Task:' . $key . '<br>';
							$NLSTask->createFinalApprovalTask( $status['NLSRefno'], $key, $status['userId'] );

							//echo 'Updating Loan Portfolio into ASCEND_V1'
							$param = array(
								'Loan_Id'             => $key ,
								'Loan_Template_Name'  => $status['loanTemplate'],
								'Loan_Portfolio_Name' => 'ASCEND_V1',
								'User_Id'             => $status['userId']
							);

							$this->updateNLSLoanPortfolio( $param );
						}
					}
				}
			}
		
		}catch(Exception $e){
			dd($e->getMessage());
		}
	}

	/**
	 * Update NLS Loan Portfolio
	 * 
	 * @param  array  $param
	 * @return
	 */
	protected function updateNLSLoanPortfolio( $param = array() )
	{

		if( count($param) > 0 ) {

			$NLS = new NLS();

			//Get the Applicant Information
			$fields = array(
				'First_Name', 
				'Last_Name'
			);

			$isRow = true;

			$whereParam = array( 'User_Id' => $param['User_Id'] );
			$applicant  = Applicant::getData($fields, $whereParam, $isRow );

			if( count($applicant) > 0 ){
				
				//Update NLS Loan Portfolio
				$fields = array(
					'loanId' 			=> $param['Loan_Id'],
					'nlsLoanTemplate'	=> $param['Loan_Template_Name'],
					'LoanPortfolioName'	=> $param['Loan_Portfolio_Name'],
					'lastName'			=> $applicant->First_Name,
					'firstName'			=> $applicant->Last_Name,
					'contactId'			=> $param['User_Id']
				);

				$NLS->nlsUpdateLoan($fields);

				//@Todo, put this on global variable
				//Update ODS Loan Portfolio
				LoanDetail::where(
					'Loan_Id' , $param['Loan_Id']
				)->update(
					array(
						'Loan_Portfolio_Id' => 4  //ASCEND_V1
					)
				);
			}
		}
	}

	/**
	 * Get All Final Approval Loans
	 * Check if the Final Approval Loan Tasks have been confirmed.
	 * 	 
	 * @return
	 */
	protected function processFinalApprovalLoans()
	{	

		//get the final approval template no 
		//Since, there are difference between
		//template no of NLS production and Test DB
		$NLSTaskTemplate = new NLSTaskTemplate(); 

		$taskTemplate  = $NLSTaskTemplate->where('task_template_name', self::TASK_NAME_FINAL_APPROVAL )
										 ->select('task_template_no')
										 ->first();

		$NLSTask = new NLSTask();
		$tasks = $NLSTask->getTasksByTaskTemplate( $taskTemplate->task_template_no );
		$acctRefNos  = array(); 
		$cifNoResult = array();

		//get corresponding Loan_App_Nr from NLS.loanacct table
		if( count($tasks) > 0 ) {

			foreach ($tasks as $key => $value)
				$acctRefNos[] = $value->NLS_refno;

			$NLSLoanAcct = new NLSLoanAcct();

			$cifNoResult = $NLSLoanAcct->whereIn('acctrefno', $acctRefNos )
							 ->select('acctrefno', 'loan_number')
							 ->orderBy('loan_number', 'DESC')
							 ->get();
		}
 		 
		$loanNrs = array();
		$userIds = array();

		// _pre($cifNoResult); 

		if( count($cifNoResult) > 0 ) {

			//Group Loan Number by Acct Reference Number
			foreach ($cifNoResult as $key => $value){
				$loanNrs[$value->acctrefno] = $value->loan_number; 
			}

			//Get Loan Detail Information
			$loanDetail = LoanDetail::whereIn('Loan_Id', $loanNrs )
									 ->whereIn('Loan_Status_Desc' , array( 
									 		 self::STATUS_CODE_VERIFICATION
									 		,self::STATUS_CODE_LOAN_APPROVED
									 		// ,self::STATUS_CODE_REJECTED
									 	)
									 )
								 	 ->select(
								 	 		 'Loan_Detail.Loan_Grp_Id'
								 	 		,'Loan_Id'
								 	 		,'Borrower_User_Id'
								 	 		,'Loan_Detail.Loan_Template_Id'
								 	 		,'Loan_Detail.Loan_Portfolio_Id'
								 	 		,'Loan_Template.Loan_Template_Name'
								 	 		,'Loan_Portfolio.Loan_Portfolio_Name'
								 	 		,'Loan_Group.Loan_Grp_Name')
								 	 ->join('Loan_Group', 'Loan_Group.Loan_Grp_Id', '=', 'Loan_Detail.Loan_Grp_Id')
								 	 ->join('Loan_Template', 'Loan_Template.Loan_Template_Id', '=', 'Loan_Detail.Loan_Template_Id')
								 	 ->join('Loan_Portfolio', 'Loan_Portfolio.Loan_Portfolio_Id', '=', 'Loan_Detail.Loan_Portfolio_Id')
									 ->get(); 
		   
		}
 
		$loanDetailArr = array(); 

		// _pre($loanDetail);

		//Match Loan Detail and User 
	 	if( count( $loanDetail ) > 0 ) {

	 		foreach( $loanDetail as $key => $value ) {

	 			//Group Loan Portfolio, Template, Group Name and User Id by Loan Id
	 			$loanDetailArr[$value->Loan_Id] = array(
						'Loan_Portfolio_Name' => $value->Loan_Portfolio_Name, 
						'Loan_Template_Name'  => $value->Loan_Template_Name, 
						'Loan_Grp_Name'       => $value->Loan_Grp_Name,
						'Borrower_User_Id'    => $value->Borrower_User_Id
	 			);
	 			//User Data
	 			$userIds[$value->Loan_Id] 	  = $value->Borrower_User_Id;
	 		}
	 	}
  		

	 	//Check if Final Approval Task is APPROVED OR DENIED. 
		if( count( $tasks ) > 0  && count( $loanDetailArr ) > 0 ) {

			foreach ($tasks as $key => $task) { 
				 
				if( isset( $loanNrs[$task->NLS_refno] ) ) {

					$loanNumber =  $loanNrs[$task->NLS_refno];

					//Check if Loan Application exist on ODS
					if( isset( $loanDetailArr[$loanNrs[$task->NLS_refno]] ) ) {
						
						echo 'Updating Loan_Id:' . $loanNumber . '<br>'; 
						$status =  array(
							'userId'        => $userIds[$loanNumber],
							'loanTemplate'  => $loanDetailArr[$loanNumber]['Loan_Template_Name'],
							'loanGroup'     => $loanDetailArr[$loanNumber]['Loan_Grp_Name'],
							'loanPortfolio' => $loanDetailArr[$loanNumber]['Loan_Portfolio_Name']
						);

						if( $task->status_code == self::NLS_TASK_STATUS_APPROVED ) {
							echo 'calling Loan Approval status LOAN APPROVED' . '<br>';
							$status['statusCode'] = self::STATUS_CODE_LOAN_APPROVED; 
							pre( $this->updateLoanStatus( $loanNumber, $status ) );
						}elseif( $task->status_code == self::NLS_TASK_STATUS_DENIED ){
							// echo 'calling Loan Approval status LOAN REJECTED'. '<br>';
							// $status['statusCode'] = self::STATUS_CODE_REJECTED; 
							// pre($this->updateLoanStatus( $loanNumber, $status ));
						}

					} //end isset( $loanDetailArr[$loanNrs[$task->NLS_refno]]
				} //end isset( $loanNrs[$task->NLS_refno] )
			} //end count($tasks) > 0  && count($loanDetailArr) > 0
		} 
	}

	/**
	 * Update Loan Status Description after Verification Tasks have been Completed
	 * 
	 * @param  integer $loanAppNr
	 * @param  string $status
	 * @return
	 */
	protected function updateLoanStatus( $loanAppNr = NULL , $status = '' )
	{	
  
       	//Update Loan Application Status in the NLS DB
		if( !empty( $loanAppNr ) ) {

			$fields = array('First_Name', 'Last_Name'); 
		 
			//Get Applicant Information
			$applicant   = getApplicant($status['userId'], $fields, true );

			$fields = array(
				'nlsLoanTemplate'  	=> $status['loanTemplate'],
				'nlsLoanGroup'     	=> $status['loanGroup'],
				'nlsLoanPortfolio' 	=> $status['loanPortfolio'],
				'contactId' 		=> $status['userId'],
				'loanId' 			=> $loanAppNr,
				'firstName' 		=> $applicant['First_Name'],
				'lastName' 			=> $applicant['Last_Name'],
				'loanStatusCode' 	=> $status['statusCode']
			);			

			try{
				
				$NLS = new NLS();
				$NLS->nlsUpdateLoanStatus($fields); 

				//Update Loan Application Status in the ODS DB
				if( !empty( $loanAppNr )  ){
					LoanDetail::where('Loan_Id', $loanAppNr )
		            			->update( array('Loan_Status_Desc' => $status['statusCode']  ) );
		        }

					

			}catch(Exception $e) {
				return $e->getMessage();
			}
		} 
	}

	/**
	 * Reject Loan Application if it More than Expiry Days limit
	 * 
	 * @param  integer $expiryDaysLimit
	 * @return [type]
	 */
	protected function rejectExpiredLoanApplication( $expiryDaysLimit = 15 )
	{
		try{
			//Generate the Due Date 
			$dueDate = date ( 'Y-m-d', strtotime ( "-$expiryDaysLimit day" . date('Y-m-d') ) );
			//print $dueDate;
			
			// Send Upcoming Expiration notification
			$this->autoCloseUpcoming( $expiryDaysLimit - 5 );
			$this->autoCloseUpcoming( $expiryDaysLimit - 4 );
			$this->autoCloseUpcoming( $expiryDaysLimit - 1 );
			
			//Get Loan Detail Information
			$loan = LoanDetail::whereNotIn('Loan_Status_Desc' , array(
											self::STATUS_CODE_REJECTED, 
											self::STATUS_CODE_LOAN_APPROVED, 
											self::STATUS_CODE_LOAN_FUNDED
										)
									)
							 ->where('Loan_Detail.Create_Dt', '<=', $dueDate )
						 	 ->select(
						 	 		'Loan_Id'
						 	 		,'Loan_Status_Desc'
						 	 		,'Borrower_User_Id'
						 	 		,'Loan_Detail.Loan_Template_Id'
						 	 		,'Loan_Detail.Loan_Portfolio_Id'
						 	 		,'Loan_Template.Loan_Template_Name'
						 	 		,'Loan_Portfolio.Loan_Portfolio_Name'
						 	 		,'Loan_Group.Loan_Grp_Name'
						 	 		,'Loan_Detail.Create_Dt')
						 	 ->join('Loan_Group', 'Loan_Group.Loan_Grp_Id', '=', 'Loan_Detail.Loan_Grp_Id')
						 	 ->join('Loan_Template', 'Loan_Template.Loan_Template_Id', '=', 'Loan_Detail.Loan_Template_Id')
						 	 ->join('Loan_Portfolio', 'Loan_Portfolio.Loan_Portfolio_Id', '=', 'Loan_Detail.Loan_Portfolio_Id')
							 ->take(1)->get();

			print '<br/>Closed Loans:<br/>';

			//Reject Loan Application if it is more than days limit
			if( count($loan) > 0 ){

				foreach ($loan as $key => $value) {
					pre($value['attributes']);	
					$status['loanTemplate']  = $value->Loan_Template_Name;
					$status['loanGroup']     = $value->Loan_Grp_Name;
					$status['loanPortfolio'] = $value->Loan_Portfolio_Name;
					$status['userId']        = $value->Borrower_User_Id;
					$status['statusCode']    = self::STATUS_CODE_REJECTED; 

					//Update Loan Status in ODS and NLS
					$this->updateLoanStatus( $value->Loan_Id, $status );

					//Get User Account Information 
					$param = array('User_Id' => $value->Borrower_User_Id);
				    $applFields = array('First_Name','Last_Name');
				    $applData = Applicant::getData($applFields, $param, true);	

				    //Get Email address via userID 
					$userFields = array('Email_Id', 'Alt_Email_Id');
					$whereParam = array('User_Id' => $value->Borrower_User_Id );	
					$isRow = true;
					$userData   = User::getData( $userFields, $whereParam, $isRow );
					$this->email = $userData->Email_Id;

					$data = array(
								'firstName' 	=> $applData->First_Name,
								'lastName'  	=> $applData->Last_Name
							);

					// send mail
					Mail::send('emails.auth.offer-expiration', $data, function($message){

					    $message->to($this->email, 'Customer')
					    		->subject('Your AscendLoan offer has expired')
					    		->bcc('chris@ascendloan.com')
					    		->bcc('sharanya@ascendloan.com')
					    		->bcc('fcorugda@vicomm.com')
								->bcc('lseno@vicomm.com');
					});

				}
			}  
		}catch(Exception $e) {
				//$e->getMessage();
				writeLogEvent('rejectExpiredLoanApplication', 
				array( 
					'Message'     => $e->getMessage()
				),
				'warning'
			); 
		}
	}

	/**
	 * Autoclose and send message to applicant for loans that are closed out after 15 days
	 * 
	 * @return
	 */
	public function autoCloseUpcoming( $expiryDaysLimit = 13 )
	{
		try{
			//Generate the Due Date  
			$dueDate = date ( 'Y-m-d', strtotime ( "-$expiryDaysLimit day" . date('Y-m-d') ) );
			//dd($dueDate);
			// Due Date minus 2 days
			//Get Loan Detail Information
			$loan = LoanDetail::whereNotIn('Loan_Status_Desc' , array(
								self::STATUS_CODE_REJECTED, 
								self::STATUS_CODE_LOAN_APPROVED, 
								self::STATUS_CODE_LOAN_FUNDED
							)
						)
						 ->where('Create_Dt', '=', $dueDate ) // dueDate should be 13
					 	 ->select(
					 	 		'Loan_Id'
					 	 		,'Loan_Status_Desc'
					 	 		,'Borrower_User_Id'
					 	 		,'Loan_Amt'
					 	 		,'APR_Val'
					 	 		,'Monthly_Payment_Amt'
					 	 		,'Create_Dt')
					 	 ->take(1)->get();

			print "<br/>Upcoming Loans in " . (15 - $expiryDaysLimit) . " days:<br/>";

			// Send Upcoming Expiration Notice
			if( count($loan) > 0 ){

				foreach ($loan as $key => $value) {
					pre($value['attributes']);
					$status['userId']        = $value->Borrower_User_Id;
					
					//Get User Account Information 
					$param = array('User_Id' => $value->Borrower_User_Id);
				    $applFields = array('First_Name','Last_Name');
				    $applData = Applicant::getData($applFields, $param, true);	

				     //Get Email address via userID 
					$userFields = array('Email_Id', 'Alt_Email_Id');
					$whereParam = array('User_Id' => $value->Borrower_User_Id );	
					$isRow = true;
					$userData   = User::getData( $userFields, $whereParam, $isRow );
					$this->email = $userData->Email_Id;

					$remainingDays = ((15 - $expiryDaysLimit) > 1) ? (15 - $expiryDaysLimit) . ' days' : '1 day';
					$remainingBusinessDays = ((15 - $expiryDaysLimit) > 1) ? (15 - $expiryDaysLimit) . ' business days' : '1 business day';
					$data = array(
								'firstName' 	=> $applData->First_Name,
								'lastName'  	=> $applData->Last_Name,
								'loanAmount' 	=> $value->Loan_Amt,
								'APR' 			=> $value->APR_Val*100,
								'term' 			=> self::LOAN_TERM,
								'MonthlyPayment'=> $value->Monthly_Payment_Amt,
								'remainingDays'	=> $remainingDays,
								'remainingBusinessDays' => $remainingBusinessDays
							);

					$emailView = 'emails.auth.upcoming-expiration';

					// If status is Step1 or Step2
					if($value->Loan_Status_Desc == self::STATUS_CODE_STEP1 || $value->Loan_Status_Desc == self::STATUS_CODE_STEP2)
						$emailView = 'emails.auth.upcoming-expiration2';

					// send mail
					Mail::send($emailView, $data, function($message) use ($remainingDays) {

					    $message->to($this->email, 'Customer')
					    		->subject('Your AscendLoan application will expire in ' . $remainingDays)
					    		->bcc('chris@ascendloan.com')
					    		->bcc('sharanya@ascendloan.com')
					    		->bcc('fcorugda@vicomm.com')
								->bcc('lseno@vicomm.com');

					});
				}
			}  	
		}catch(Exception $e) {
				//$e->getMessage();
				writeLogEvent('autoClose Upcoming Loans', 
				array( 
					'Message'     => $e->getMessage()
				),
				'warning'
			); 
		}
	}

	/**
	 * Autoclose and send message to applicant for loans that are closed out after 15 days
	 * 
	 * @return
	 */
	public function autoCloseLoansTest( $expiryDaysLimit = 15 )
	{
		try{
			//Generate the Due Date 
			$dueDateBefore = date ( 'Y-m-d', strtotime ( "-$expiryDaysLimit day" . date('Y-m-d') ) );

			//Generate the Due Date 
			$dueDate = date ( 'Y-m-d', strtotime ( "-$expiryDaysLimit day" . date('Y-m-d') ) );

			// dd($dueDate);

			// Due Date minus 2 days
			//Get Loan Detail Information
			$loan = LoanDetail::whereNotIn('Loan_Status_Desc' , array(
											self::STATUS_CODE_REJECTED, 
											self::STATUS_CODE_LOAN_APPROVED, 
											self::STATUS_CODE_LOAN_FUNDED
										)
									)
									 ->where('Create_Dt', '=', $dueDateBefore ) // dueDate should be 13
								 	 ->select(
								 	 		'Loan_Id'
								 	 		,'Loan_Status_Desc'
								 	 		,'Borrower_User_Id'
								 	 		,'Loan_Amt'
								 	 		,'APR_Val'
								 	 		,'Monthly_Payment_Amt'
								 	 		)
								 	 ->get();

			// Send Upcoming Expiration Notice
			if( count($loan) > 0 ){

				foreach ($loan as $key => $value) {

					$status['userId']        = $value->Borrower_User_Id;
					
					//Get User Account Information 
					$param = array('User_Id' => $value->Borrower_User_Id);
				    $applFields = array('First_Name','Last_Name');
				    $applData = Applicant::getData($applFields, $param, true);	

				     //Get Email address via userID 
					$userFields = array('Email_Id', 'Alt_Email_Id');
					$whereParam = array('User_Id' => $value->Borrower_User_Id );	
					$isRow = true;
					$userData   = User::getData( $userFields, $whereParam, $isRow );
					$this->email = $userData->Email_Id;

					$data = array(
								'firstName' 	=> $applData->First_Name,
								'lastName'  	=> $applData->Last_Name,
								'loanAmount' 	=> $value->Loan_Amt,
								'APR' 			=> $value->APR_Val,
								'term' 			=> self::LOAN_TERM,
								'MonthlyPayment'=> $value->Monthly_Payment_Amt
								
							);
					// send mail 

					Mail::send('emails.auth.upcoming-expiration', $data, function($message){

					    $message->to($this->email, 'Customer')
					    		->subject('Your AscendLoan will expire in 2 days')
					    		->cc('jricana@vicomm.com');
					    		//->cc('fcorugda@vicomm.com');
					});

				}
			}  	


			//Generate the Due Date 
			$dueDate = date ( 'Y-m-d', strtotime ( "-$expiryDaysLimit day" . date('Y-m-d') ) );

			// dd($dueDate);

			//Get Loan Detail Information
			$loansDue = LoanDetail::whereNotIn('Loan_Status_Desc' , array(
											self::STATUS_CODE_REJECTED, 
											self::STATUS_CODE_LOAN_APPROVED, 
											self::STATUS_CODE_LOAN_FUNDED
										)
									)
									 ->where('Loan_Detail.Create_Dt', '=', $dueDate ) 
								 	 ->select(
								 	 		'Loan_Id'
								 	 		,'Loan_Status_Desc'
								 	 		,'Borrower_User_Id'
								 	 		,'Loan_Detail.Loan_Template_Id'
								 	 		,'Loan_Detail.Loan_Portfolio_Id'
								 	 		,'Loan_Template.Loan_Template_Name'
								 	 		,'Loan_Portfolio.Loan_Portfolio_Name'
								 	 		,'Loan_Amt'
								 	 		,'APR_Val'
								 	 		,'Monthly_Payment_Amt'
								 	 		,'Loan_Group.Loan_Grp_Name')
								 	 ->join('Loan_Group', 'Loan_Group.Loan_Grp_Id', '=', 'Loan_Detail.Loan_Grp_Id')
								 	 ->join('Loan_Template', 'Loan_Template.Loan_Template_Id', '=', 'Loan_Detail.Loan_Template_Id')
								 	 ->join('Loan_Portfolio', 'Loan_Portfolio.Loan_Portfolio_Id', '=', 'Loan_Detail.Loan_Portfolio_Id')
									 ->get();

			// Send Upcoming Expiration Notice
			if( count($loansDue) > 0 ){

				foreach ($loansDue as $key => $value) {

					$status['loanTemplate']  = $value->Loan_Template_Name;
					$status['loanGroup']     = $value->Loan_Grp_Name;
					$status['loanPortfolio'] = $value->Loan_Portfolio_Name;
					$status['userId']        = $value->Borrower_User_Id;
					$status['statusCode']    = self::STATUS_CODE_REJECTED; 

					//Update Loan Status in ODS and NLS
					$this->updateLoanStatus( $value->Loan_Id, $status );

					//Get User Account Information 
					$param = array('User_Id' => $value->Borrower_User_Id);
				    $applFields = array('First_Name','Last_Name');
				    $applData = Applicant::getData($applFields, $param, true);	

				    //Get Email address via userID 
					$userFields = array('Email_Id', 'Alt_Email_Id');
					$whereParam = array('User_Id' => $value->Borrower_User_Id );	
					$isRow = true;
					$userData   = User::getData( $userFields, $whereParam, $isRow );
					$this->email = $userData->Email_Id;

					$data = array(
								'firstName' 	=> $applData->First_Name,
								'lastName'  	=> $applData->Last_Name,
								'loanAmount' 	=> $value->Loan_Amt,
								'APR' 			=> $value->APR_Val*100,
								'term' 			=> self::LOAN_TERM,
								'MonthlyPayment'=> $value->Monthly_Payment_Amt
							);

					// send mail 

					Mail::send('emails.auth.offer-expiration', $data, function($message){

					    $message->to($this->email, 'Customer')
					    		->subject('Your AscendLoan offer has expired')
					    		->cc('jricana@vicomm.com');
					    		//->cc('fcorugda@vicomm.com');
					});

				}
			} 					 

		}catch(Exception $e) {
				pre($e->getMessage());
				writeLogEvent('autoCloseLoans', 
				array( 
					'Message'     => $e->getMessage()
				),
				'warning'
			); 
		}

	}

	/**
	 * Update User Reference Number
	 * 
	 * @return
	 */
	protected function updateUserRefNo()
	{
		User::updateUserRefNo();
	}

	/**
	 * Update NLS Refs Number
	 * 
	 * @return
	 */
	protected function updateNLSRef()
	{
		LoanDetail::updateNLSRef();
	}
}