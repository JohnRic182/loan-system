<?php

/**
 * Developers Notes
 *
 * @todo 
 * 	  - use lang->en->messages for any success and error notifications
 */

class VerificationController extends BaseController {

	/**
	 * The layout that should be used for responses
	 * 
	 * @var string 
	 */
	protected $layout = 'layouts.application';
 
	/**
	 * Applicant User Id 
	 * 
	 * @var integer
	 */
	protected $userId; 

	/**
	 * Loan Application Number
	 * 
	 * @var integer
	 */
	protected $loanAppNr;
 	
	/**
	 * Default Document Template Id
	 * @var string
	 */
	protected $documentTmplteId;

	/**
	 * Default Loan Application Verfication Phase
	 * 
	 * @var array
	 */
	protected $defaultLoanAppVer = array();

	/**
	 * Loan Detail 
	 * 
	 * @var array
	 */
	protected $loanDetail = array();
		
	/**
	 * Loan Template
	 * 
	 * @var array
	 */
	protected $loanTemplate = array();
		
	/**
	 * Loan Group 
	 * 
	 * @var array
	 */
	protected $loanGroup = array();

	/**
	 * Applicant Information
	 * 
	 * @var array
	 */
	protected $applicant = array(); 

	/**
	 * Applicant Email Address
	 * @var
	 */
	protected $email;

	/**
	 * Email Subject
	 * @var
	 */
	protected $subject;

	/**
	 * Debug Mode Flag
	 * 
	 * @var string
	 */
	protected $debugMode;

	/**
	 * s3 URL
	 * 
	 * @var string
	 */
	protected $s3URL;

	/**
	 * Assignee
	 * 
	 * @var
	 */
	protected $assignee; 
		
	/**
	 * Loan Portfolio
	 * 
	 * @var array
	 */
	protected $loanPortfolio = array();

	/**
	 * SF Verification Id
	 * 
	 * @var string
	 */
	protected $sfVerificationId; 

	/**
	 * NLS Task Id
	 * 
	 * @var
	 */
	protected $NLSTaskId; 

	/**
	 * ODS Verification Id
	 * 
	 * @var
	 */
	protected $ODSVerificationId;

	protected $documentObj = array();

	protected $taskTemplateNr;

 
	const TASK_NAME_EMPLOYMENT      = 'CONFIRM_EMPLOYMENT';
	const TASK_NAME_INCOME          = 'CONFIRM_INCOME';
	const TASK_NAME_FUNDING         = 'CONFIRM_PMT_ACCT';
	const TASK_NAME_CONTRACT        = 'REVIEW_CONTRACT';
	const TASK_NAME_DOCUMENTS       = 'CONFIRM_ID'; 
	const TASK_NAME_IDENTITY        = 'ID_VERIFIED'; 
	const TASK_NAME_PAYMENT         = 'REVIEW_PAYMENT_METHOD';
	const TASK_NAME_FINAL_APPROVAL  = 'FINAL_APPROVAL'; 
	const TASK_NAME_BANK            = 'CONFIRM_BANK_ACCOUNT ';
 
	const COMMENT_CATEGORY_CONTRACT = 'EXECUTED_CONTRACT'; 
	const COMMENT_CATEGORY_IDENTITY = 'PROOF_OF_IDENTITY'; 
	const COMMENT_CATEGORY_INCOME   = 'PROOF_OF_INCOME';  
	const COMMENT_CATEGORY_FUNDING  = 'CONFIRM_BANK_ACCOUNT';
	const COMMENT_CATEGORY_BANK     = 'CONFIRM_BANK_INFO';
	
	const ACH_RR_COMPANY_NAME       = 'ASCEND CONSFINR1'; 
	const ACH_AL_COMPANY_NAME       = 'ASCEND CONSFIN01'; 

	/**
	 * Applicant Document Signer Code
	 */
	const APPLNT_SIGNER_CODE    = 'Applicant';

	/**
	 * Constructor
	 * 
	 */
	public function __construct()
	{
 
		$this->userId 	 = getUserSessionID();
		$this->loanAppNr = getLoanAppNr();

		//Get the applicant address  information
		$applicantAddress = getApplicantAddress($this->userId, $fields = array('Street_Addr_1_Txt') );
		//Get the applicant information
		$this->applicant  = getApplicantData();		
		$this->applicant['address'] = $applicantAddress['Street_Addr_1_Txt'];

		$this->setDefaultLoanAppVer();
 
		//Set loan details
		$this->setLoanDetail($this->userId, $this->loanAppNr);

		//Set applicant, loan template, loan portfolio and loan group 
		if( count( $this->loanDetail ) > 0 ){

			$this->setLoanTemplate($this->loanDetail->Loan_Template_Id);
			$this->setLoanGroup($this->loanDetail->Loan_Grp_Id);
			$this->setLoanPortfolio($this->loanDetail->Loan_Portfolio_Id);
		}

		//Set default Assignee
		$this->assignee = SFCustomerServiceRep::getDefaultAssignee();

		//Set s3 URL
		$this->s3URL =  Config::get('system.S3.URL');

		//Set Debug Mode Flag
		$this->debugMode = Config::get('system.Ascend.DebugMode');
 
		$this->setSFAccountIdSession( $this->userId );

		$this->setSFLoanApplicationId( $this->userId );

    	View::share('debugMode', $this->debugMode);		
 
	}

	/**
	 * Set SF Account Id in Session 
	 * 
	 * @param integer $userId
	 */
	public function setSFAccountIdSession( $userId )
	{
		$SalesforceMod = new SalesforceMod();
		$id = $SalesforceMod->getAccountId($userId);

		if( empty(Session::get('sfAccountId')) ) {
			Session::put('sfAccountId', $id );
		}
	}

	/**
	 * Set SF Loan Application Id 
	 * 
	 * @param integer $userId
	 */
	public function setSFLoanApplicationId( $userId )
	{
		$SalesforceMod = new SalesforceMod();
		$id = $SalesforceMod->getSFLoanApplicationId($userId);

		if( empty(Session::get('loanAppSFId')) ) {
			Session::put('loanAppSFId', $id );
		}
	}

	/**
	 * Set Loan Detail
	 *
	 *
	 * @todo  we need to set the data into session before checking it in database
	 *        everytime we call the Verification Controller
	 *        
	 * @param integer $userId
	 * @param integer $loanAppNr
	 */
	public function setLoanDetail($userId, $loanAppNr)
	{
		$this->loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
								->where('Loan_Id','=',$loanAppNr)
								->first();
	}

	/**
	 * Set SF Verification Id
	 * 
	 * @param object $sfObject
	 */
	public function setSFVerificationId( $sfObject )
	{	
		// _pre($sfObject);

		if( count($sfObject) > 0 ) {
			foreach ($sfObject as $key => $value) {
				if( isset($value->records[0]->Id) ) {
					// echo 'isset called';
					$this->sfVerificationId =  $value->records[0]->Id;
				}
			}	
		}
	}

	/**
	 * Set Loan Template
	 *
	 * @todo  we need to set the data into session before checking it in database
	 *        everytime we call the Verification Controller
	 * 
	 * @param integer $Loan_Template_Id
	 */
	public function setLoanTemplate( $loanTemplateId )
	{
		$this->loanTemplate = LoanTemplate::find( $loanTemplateId ); 
	}

	/**
	 * Set Loan Group 
	 *
	 * @todo  we need to set the data into session before checking it in database
	 *        everytime we call the Verification Controller
	 * 
	 * @param integer $loanGroupId
	 */
	public function setLoanGroup( $loanGroupId )
	{
		$this->loanGroup = LoanGroup::find( $loanGroupId );
	}

	/**
	 * Set Loan Portfolio
	 *
	 * @todo  we need to set the data into session before checking it in database
	 *        everytime we call the Verification Controller
	 * @param integer $loanPortfolioId
	 */
	public function setLoanPortfolio( $loanPortfolioId )
	{ 
		$this->loanPortfolio = LoanPortfolio::find( $loanPortfolioId );
	}

	/**
	 * Set Default Loan Application Verification
	 */
	public function setDefaultLoanAppVer()
	{  
		$this->defaultLoanAppVer = LoanVerificationPhase::all();  
	}

	/**
	 * Set Document Template Id for HelloSign
	 * 
	 * @param string $docTemplateId
	 */
	public function setDocTemplateId($docTemplateId = "")
	{
		$this->documentTmplteId = $docTemplateId;
	}

	/**
	 * Get Applicant Risk Info
	 * 
	 * @return 
	 */
	protected function getApplicantRiskInfo()
	{
		if( !empty($this->userId) && !empty($this->loanAppNr ) ) {
			return ApplicantRiskInfo::where('User_Id', '=' , $this->userId )
								 ->where('Loan_App_Nr', '=', $this->loanAppNr )
								 ->first();
		}
	}

	/**
	 * Show Email Verification
	 * 
	 * @return
	 */
	protected function showEmailVerificationStep()
	{	

		//Get Applicant Session Data
		$applicant = getApplicantData();

		//check if the applicant is verified
		if( isset( $applicant['verified'] ) && $applicant['verified'] == '0' ) {

			//Prepare email verifcation
			$data = array(
				'firstName'       => $applicant['firstName'], 
				'verificationUrl' => route('verifyApplicant', array( 'token' => $applicant['token'] ) )
			);

			//Send Email Verification
			$this->sendEmailVerification( $applicant['email'], $data );
		}

		$this->layout->content = View::make('blocks.application.email-verification');
	}
 	
 	/**
 	 * Send Email Verification 
 	 *      - send email verification to verify the applicant stau
 	 * @param  string $email
 	 * @param  array  $data
 	 * @return
 	 */
	protected function sendEmailVerification( $email, $data = array() )
	{
		try{
			
			$this->email 	= $email;
			$this->subject  = 'Ascend Consumer Finance Email Address Verification Request';

			Mail::send('emails.auth.email-verification', $data, function($message)
			{ 
			    $message->to( $this->email, 'Customer');

			    if( $this->debugMode == 'true' ){
			    	$message->cc('chris@ascendloan.com')
			    			->cc('fcorugda@vicomm.com');
			    }

			   	$message->subject($this->subject);
			});

			$loanAppPhaseNr = 5;
			$subPhaseCode   = 'VerificationEmailSent';

			//Set Loan Application Phase to  Final Offer - Verification Email Sent
			setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );


		} catch(Exception $e) {

			writeLogEvent('Email Verification Sending', 
					array( 
						'User_Id'     => $this->userId,
						'Loan_App_Nr' => $this->loanAppNr,
						'Message'     => $e->getMessage()
					),
					'warning'
				); 
		}
	} 

	/**
	 * Salesforce Assignee 
	 * 
	 *    - Assign the application in one of the saleforce
	 *      representative
	 *      
	 * @param  array  $data
	 * @return
	 */
	protected function assign( $data = array() )
	{
		if( count($this->assignee) > 0 && count($data) > 0 ) {
			return VerificationAssignment::assign(
				array(
					'CSR_Id'        => $this->assignee->CSR_Id,
					'CSR_User_Name' => $this->assignee->CSR_User_Name,
					'User_Id'       => $data['User_Id'], 
					'Loan_Id'       => $data['Loan_App_Nr']
				)
			);
		}
	}
	 
	/**
	 * Show Approval Page - Step 5 Loan Application
	 *
	 * @param  string $result
	 * @return
	 */
	protected function showVerificationSteps( $result = "success")
	{	
		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat('verification');

		if( !empty($redirect) )
			return Redirect::to($redirect);

		//Do the Email Verification if EmailVerify Config is set to 1
		if( Config::get('system.Ascend.EmailVerify') == '1' ) {
			//check if the applicant has been verified
			//Get Applicant Session Data
			$applicant = getApplicantData();

			//if the applicant is not verified then, show the email validation instead
			if( isset( $applicant['verified'] ) && $applicant['verified'] == '0' ) {
				return Redirect::to('verifyAccount');
			}
		}
		
		// check if Bank is done then step is complete, else show fastLink step
		$bankAccountDetail = BankAccountDetail::where('User_Id','=',$this->userId)->where('Loan_App_Nr','=',$this->loanAppNr)->first();
		$bankAccountInfo = BankAccountInfo::where('User_Id','=',$this->userId)->first();
		
		if( count($bankAccountDetail) > 0 && count($bankAccountInfo) > 0 ){
			$loanAppVerId = 8; // bank account verification
			$this->updateCompletedAppVer($loanAppVerId);
		}

		//set the application step	
		setActiveStepSession('5');

		$data['isApproved'] = TRUE;	
 
		if( $result == 'fail' ){
			return Redirect::to('disqualification/identity');
		}
		
		//Retrieve Completed Verification Steps
		$data['complete']   = array(); 

		$steps = $this->getCompletedAppVer($this->userId, $this->loanAppNr);

		if( isset($steps->Verification_Phase_Status_Txt) ){
			$data['complete'] = unserialize($steps->Verification_Phase_Status_Txt);
		}
		
 		//check if all validation steps are complete
 		if( count( $data['complete'] ) > 0 ) {
			foreach ($data['complete'] as $key => $step) {
				if( $step['value'] == 'n' ){
					$data['isApproved'] = FALSE;
					break;
				}
			}
		} else {
			$data['isApproved'] = FALSE;
		}

		$loanAppPhaseNr = 7; 
 		
		//Check if there's an Existing Loan App Phase
		$loanAppPhaseCnt  = LoanApplicationDetail::isLoanAppPhaseExist(
			$this->userId , 
			$this->loanAppNr, 
			$loanAppPhaseNr 
		);
		
		if( $loanAppPhaseCnt == 0 ){
			setLoanAppPhase( $this->userId, $this->loanAppNr, $loanAppPhaseNr );
		}


		//Set the Assignee on the Application
		$this->assign(
			array(
				'User_Id' => $this->userId, 
				'Loan_App_Nr' => $this->loanAppNr
			)
		);

		//Update NLS Loan Status to VERIFICATION
		$loanStatusCode = 'VERIFICATION'; 
		$this->updateNLSLoanStatus($loanStatusCode);

		//Update Loan Status in ODS
		updateLoanStatus($this->loanAppNr, $loanStatusCode);

		//Create / Update SF Loan Object
		importSFLoanObj( $this->userId, $this->loanAppNr );

		//Set Loan Status Description
		Session::put('loan.statusDesc', $loanStatusCode );

		//get the default loan application version 
		$data['steps'] = $this->defaultLoanAppVer;

		if( $data['isApproved'] )
			return Redirect::to('verification/success')->with('secure', true);
		else
			$this->layout->content = View::make('blocks.application.verification-index', $data );	
	}

	/**
	 * Get Loan Document Template Id
	 * 
	 * @param  string $loanProdId
	 * @param  string $stateCd
	 * @return
	 */
	protected function getLoanDocumentTemplateId( $loanProdId = "", $stateCd = "" )
	{	
		$DocTemplate = new DocumentTemplate();

		$template = $DocTemplate->getTemplateId( array(
							'Loan_Prod_Id' => $loanProdId, 
							'State_Cd'	   => $stateCd
						));
 
		if( isset($template->Doc_Template_Id))
			return $template->Doc_Template_Id;
	}

	/**
	 * Create NLS CIF Vendor
	 *
	 * @param  array $param
	 * @return
	 */
	protected function createNLSCIFVendor( $param = array() )
	{
		$vendor =  array( 
						'CIFNumber'                  => $param['UserId'],
						'Payee'                      => $param['PayeeName'],
						'AccountNumber'              => $param['AcctNr'], 
						'ACHPayableABANumber'        => $param['BankRoutingNr'], 
						'ACHPayableAccountNumber'    => $param['AcctNr'],
						'ACHReceivableABANumber'     => $param['BankRoutingNr'], 
						'ACHReceivableAccountNumber' => $param['AcctNr']
					);

		try{
			$NLS = new NLS();
			return $NLS->addCIFVendor($vendor); 
		}catch(Exception $e) {
			return $e->getMessage();
		}
	}

	/**
	 * Insert NLS Contact 
	 * 
	 * @param  array $param
	 * @return
	 */
	protected function createNLSAddressBook( $param = array() )
	{
		if( count($param) > 0 && !empty($this->userId) ) {

			$contact = array( 
				'UserId'             => $param['UserId'],
				'AddressDescription' => 'Company Address',
				'Entity'             => 'COMPANY',
				'RelationshipCode'   => 'EMPLOYER', 
				'CompanyName'        => $param['CompanyName'], 
				'LastName1'          => $param['LastName'], 
				'FirstName1'         => $param['FirstName'], 
				'Title1'             => $param['Position'], 
				'StreetAddress1'     => $param['StreetAddress1'], 
				'StreetAddress2'     => $param['StreetAddress2'], 
				'EmailAddress1'      => $param['EmailAddress1'], 
				'City'               => $param['City'], 
				'ZipCode'            => $param['ZipCode'], 
				'UserDefinedField1'  => $param['EmploymentStatus'], 
				'UserDefinedField2'  => $param['Tenureship'], 		
				'UserDefinedField5'  => $param['Position'],
				'UserDefinedField7'  => $param['EmployerName'],
				'UserDefinedField8'  => $param['StreetAddress1'],
				'UserDefinedField9'  => $param['StreetAddress2'],
				'UserDefinedField10' => $param['City'],
				'UserDefinedField11' => $param['State'],
				'UserDefinedField12' => $param['ZipCode'],
				'UserDefinedField13' => $param['PhoneNr'],
			);	 

			try{
				$NLS = new NLS();
				return $NLS->updateAddressBook( $contact );
			}catch(Exception $e) {
				return $e->getMessage();
			}
		}
	}


	/**
	 * Create NLS ACH 
	 * 
	 * @param  string $taskName
	 * @return
	 */
	protected function createNLSACH( $bankAccount = array() )
	{
		if( count($bankAccount) > 0 && !empty($this->userId) && !empty($this->loanAppNr) ) {

			$companyName = self::ACH_AL_COMPANY_NAME;

			if( $this->loanDetail->Loan_Prod_Id == 2 )
				$companyName = self::ACH_RR_COMPANY_NAME;

			$ach =  array(
						'loanId'            => $this->loanAppNr, 
						'contactId'         => $this->userId,
						'nlsLoanTemplate'   => $this->loanTemplate->Loan_Template_Name,
						'nlsLoanGroup'      => $this->loanGroup->Loan_Grp_Name,
						'nlsLoanPortfolio'  => $this->loanPortfolio->Loan_Portfolio_Name,
						'companyName'       => $companyName,
						'bankRoutingNumber' => $bankAccount['BankRoutingNr'],
						'accountNumber'     => $bankAccount['AcctNr'],
						'amount'            => $bankAccount['Amount'], 
						'billingStartDate'  => $bankAccount['BillingStartDate'], 
					);

			try{
				$NLS = new NLS();
				return $NLS->addACH($ach); 
			}catch(Exception $e) {
				return $e->getMessage();
			}
		}
	}

	/**
	 * Create Task on Salesforce, ODS, and NLS
	 *
	 * @updates 
	 * 		   - removing the task template code
	 * 		   - added task status as required for ID Verification step 
	 * 		   
	 * @param  string  $taskName
	 * @param  string  $comments
	 * @param  string  $taskStatus
	 * @return
	 */
	protected function createTask( $taskName = "", $comments = array(), $notes = "", $taskStatus = '' )
	{
		$result = array(); 

		if( !empty($taskName) && !empty($this->userId) && !empty($this->loanAppNr) ) {

			try{
				
				$NLS = new NLS();
				$SalesforceMod = new SalesforceMod();

				//get Applicant Name 
				$applicantInfo = Session::get('applicant'); 

				$firstName = '';
				$lastName  = '';

				if( isset($applicantInfo['firstName']) ) {
					$firstName = $applicantInfo['firstName']; 
				}

				if( isset($applicantInfo['lastName']) ){
					$lastName = $applicantInfo['lastName']; 
				}

				$applName =  $firstName  . ' ' . $lastName; 
				$subject  = $taskName . ' ' . $applName;

				$task = array(
					'UpdateFlag'       => 0, 
					'TaskTemplateName' => $taskName,
					'PriorityCodeName' => 'NORMAL',
					'NLSType'          => 'Loan', 
					'NLSRefNumber'     => $this->loanAppNr,
					'CreatorUID'       => 0, 
					'OwnerUID'         => 0,
					'OwnerUserName'    => 'Wayde',  
					'StartDate'        => date('m/d/Y'), 
					'DueDate'          => date('m/d/Y'),
					'Subject'          => $subject, 
					'Notes'            => $notes,
				);

				//Set the completion date
				if($taskStatus == 'APPROVED' || $taskStatus == 'DENIED'){
					$task['CompletionDate'] = date('m/d/Y');
				}

				//Set the status code name
				if(isset($taskStatus) && $taskStatus != ''){
					$task['StatusCodeName'] = $taskStatus;			
				}
			 
			 	//Add NLS Task
				$result['nls'] = $NLS->addTask($task, $comments);

				$verificationType = \Config::get("SFTaskTypeMap.$this->ODSVerificationId");
				
				$param = array(
					'User_Id'            => $this->userId, 
					'Loan_App_Nr'        => $this->loanAppNr, 
					'Task_Title_Txt'     => $taskName,
					'Task_Name'          => $subject, 
					'ODS_Verification_Id'=> $this->ODSVerificationId, 
					'SF_Verification_Id' => $this->sfVerificationId,
					'Task_Status_Desc'   => 'OPEN',
					'SF_Task_Id'		 => '',
					'Doc_Name'           => $taskName,
					'Task_Type_Desc'     => $verificationType,
					'Reqd_File_Cnt'      => $this->documentObj['Reqd_File_Cnt'], 
					'Assigned_CSR_Id'    => $this->assignee->CSR_Id, 
					'Comments_Txt'       => $comments['commentStr'],
					'Subject_Txt'        => $subject, 
					'Notes_Txt'          => $notes,
					'Create_Dt'          => date('Y-m-d'), 
					'Created_By_User_Id' => $this->userId,
					'Task_Doc_Upload_URL_Txt' => $this->documentObj['Task_Doc_Upload_URL_Txt']
				);
  	
  				//Get Task Reference Number
				$NLSTask = new NLSTask();
				$taskRefNo = $NLSTask->getTasksByRefNoTemplateNo(
					$this->loanDetail->Loan_Ref_Nr, 
					$this->taskTemplateNr
				);
 
				// Loan Contract
				// Funding Account Information
				// Employment Verification
				// Income Verification
				// Identity Documents
				// Payment Choice
				// Identity Verification
				// Bank Verification
				// _pre($taskRefNo);

				$param['Task_Id'] 		= $taskRefNo->task_refno;
				$param['NLS_Task_Id'] 	= $taskRefNo->task_refno;

				//Set NLSTaskId
				$this->NLSTaskId = $taskRefNo->task_refno;
 				
 				// pre($param);

 				$loanAppSFId = Session::get('loanAppSFId');

				//Create Salesforce Task 
				$result['sf'] = $SalesforceMod->newTask($param, $loanAppSFId);

				// _pre($result['sf']);

				if( isset($result['sf'][0]->id) ) {
					$param['SF_Task_Id'] = $result['sf'][0]->id;
				}

				//Create ODS Task
				$result['ods'] = Task::newTask($param);

				// pre($result); 

			}catch(Exception $e) {
				writeLogEvent(
					'createTask', 
					array(
						'Message' => $e->getMessage()
					),
					'warning'
				);
			}
		}

		return $result;
	}

	/**
	 * Create NLS Comment
	 *
	 * @param  string $commentStr
	 * @param  mixed $docPath
	 * @param  string $category
	 * @return array
	 */
	protected function createNLSComment( $commentStr = "", $docPath, $category = "", $filename = '')
	{

		if( !empty($commentStr) && !empty($this->userId) && !empty($this->loanAppNr) ) {

			$hexImage = array();

			//check if the docPath is an array
			if( is_array( $docPath ) && count($docPath) > 0 ) {

				$cleanCategory = preg_replace('/ /', '-', $category); 
				
				foreach ($docPath as $key => $image ) {
					$extension   = pathinfo($image, PATHINFO_EXTENSION);
					$hexImage[]  = array(
						'DocImage' => $this->convertImageToHex( $image ), 
						'Filename' => $cleanCategory . $key . '.' . $extension
					);
				}

			} else{

				if( $filename == '' ) 
					$cleanCategory = preg_replace('/ /', '-', $category); 	
				else 
					$cleanCategory = $filename;


				$extension     = pathinfo($docPath, PATHINFO_EXTENSION); 
				
				$hexImage[0] = array(
					'DocImage' => $this->convertImageToHex( $docPath ), 
					'Filename' => $cleanCategory . '.' . $extension
				);
			}

			$comment =  array(
						'loanId'           => $this->loanAppNr, 
						'contactId'        => $this->userId,
						'nlsLoanTemplate'  => $this->loanTemplate->Loan_Template_Name,
						'nlsLoanGroup'     => $this->loanGroup->Loan_Grp_Name,
						'nlsLoanPortfolio' => $this->loanPortfolio->Loan_Portfolio_Name,
						'Comment'		   => $commentStr,
						'Category'		   => $category,
						'Files'		  	   => $hexImage
					);
			
			try{
				$NLS = new NLS();
				return $NLS->addComment($comment); 
			}catch(Exception $e) {
				return $e->getMessage();
			}
		}
	}

	/**
	 * Update NLS Loan Status Code
	 *
	 * @param  string $loanStatusCode
	 * @return array
	 */
	protected function updateNLSLoanStatus( $loanStatusCode = "")
	{

		if( !empty($this->userId) && !empty($this->loanAppNr) ) {

			$fields   = array('First_Name', 'Middle_Name', 'Last_Name', 'Social_Security_Nr'); 
		 
			//Get Applicant Information
			$applicant   = getApplicant($this->userId, $fields, true );

			$fields = array(
				'nlsLoanTemplate'  	=> $this->loanTemplate->Loan_Template_Name,
				'nlsLoanGroup'     	=> $this->loanGroup->Loan_Group_Name,
				'nlsLoanPortfolio' 	=> $this->loanPortfolio->Loan_Portfolio_Name,
				'contactId' 		=> $this->userId,
				'loanId' 			=> $this->loanAppNr,
				'firstName' 		=> $applicant['First_Name'],
				'lastName' 			=> $applicant['Last_Name'],
				'loanStatusCode' 	=> $loanStatusCode
			);

			try{
				$NLS = new NLS();
				return $NLS->nlsUpdateLoanStatus($fields); 
			}catch(Exception $e) {
				return $e->getMessage();
			}
		}
	}
  	
  	/**
  	 * Convert Image File to Hex
  	 * 
  	 * @param  string $imagePath
  	 * @return
  	 */
  	protected function convertImageToHex( $imagePath = '') 
  	{

  		$hexImage = '';
 
  		if( file_exists($imagePath) ) {

  			$rh = fopen($imagePath, 'r'); 
  			$pb = fread($rh, filesize($imagePath)); 

  			$pc = bin2hex($pb);

  			$hexImage = '0x'.$pc;
  		}

  		return $hexImage; 
  	}

	/**
	 * Show Contract
	 * 
	 * @return
	 */
	protected function showLoanDocument()
	{

		$loanAppVerId = 1; //App verifcation ID

		$stateCode = 'CA';

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');



		if( empty( $this->userId ) || empty( $this->loanAppNr ) ) {
			return Redirect::to('/');
		}

		//Get Contact Request according to User Id And Loan Application Number
		$contract = LoanDocument::getContract(
						$this->userId, 
						$this->loanAppNr 
					);
		 
		//Get Loan Detail Information
		$loanDetail  = $this->loanDetail;

		//Get Loan Product Information
		$loanProduct = LoanProduct::find($loanDetail->Loan_Prod_Id);
  
		$loanData = Session::get('loan');

		//Get State Code
		$whereParam = array('User_Id' => $this->userId );	
		$applicantAddressFields = array(
			'State_Cd'			
		);
		$addressData   = ApplicantAddress::getData($applicantAddressFields, $whereParam, TRUE );
 		if(count($addressData) > 0){
 			$stateCode = $addressData->State_Cd;
 		}
		//set the state code of the applicant
		$templateId =  $this->getLoanDocumentTemplateId( $loanDetail->Loan_Prod_Id, $stateCode );
		//$templateId =  $this->getLoanDocumentTemplateId( 2, 'OR' );

		//set document template id 
		if( !empty($templateId)){
			$this->setDocTemplateId($templateId);
		}
 
		$fields   = array('First_Name', 'Middle_Name', 'Last_Name'); 
		 
		//Get Applicant Information
		$applicant   = getApplicant($this->userId, $fields, true );

		//Get User Account Information
		$userAccount = User::getData(array('Email_Id'), array('User_Id' => $this->userId), true );

		//Get Applicant Address Information
		$applAddrs 	 = getApplicantAddress($this->userId, array('Street_Addr_1_Txt', 'Zip_Cd', 'City_Name'));

		//Get Loan Payment Information
		$loanPayment = LoanPayment::where('Borrower_User_Id', $this->userId)->first();
   
		//format applicant full name
		$applName = $applicant['First_Name'] . " " . $applicant['Last_Name'] ;

		//debug mode 
		$data['debugMode'] = Config::get('system.HelloSign.Debug');
		
		//Initialized Hello Sign Package
		$HelloSign = App::make('hellosign');

		$interestRate           = 0;
		$NLSCurrentInterestBal  = 0; 
		$NLSCurrentPrincipalBal = 0;
		$NLSTotalPayments       = 0;
		$minimumPayment			= 0;
		$loanTerm               = 36; 
		$principalPaymentDt     = 0;
		$maturityDate			= date('Y-m-d');
		$monthlyPayment         = 0;
		$paymentDueDt           = 0;

		//@override NLS Loan data
		if( !empty( $loanPayment ) ) { 
			$minimumPayment		  = $loanPayment->Min_Payment_Amt;
			$principalPaymentDt   = $loanPayment->First_Principal_Payment_Dt; 
			$paymentDueDt         = $loanPayment->Payment_Due_Dt; 
		}	

		//@override default values by Loan Detail Amount
		if( !empty( $loanDetail ) ) {
			$NLSCurrentInterestBal  = !empty( $loanDetail->Current_Interest_Bal_Amt ) ? $loanDetail->Current_Interest_Bal_Amt  : 0 ; 
			$NLSCurrentPrincipalBal = !empty( $loanDetail->Current_Principal_Bal_Amt ) ? $loanDetail->Current_Principal_Bal_Amt : 0 ; 
			$interestRate           = !empty( $loanDetail->APR_Val ) ? $loanDetail->APR_Val : 0; 
			$NLSTotalPayments       = !empty( $loanDetail->Loan_Payment_Total_Amt ) ? $loanDetail->Loan_Payment_Total_Amt : 0 ;
			$maturityDate           = !empty( $loanDetail->Open_Maturity_Dttm ) ? $loanDetail->Open_Maturity_Dttm : date('Y-m-d');  
			$monthlyPayment         = !empty( $loanDetail->Monthly_Payment_Amt ) ? $loanDetail->Monthly_Payment_Amt : 0;  
		}
	
		$option = array(
			'subject' 		=> $loanProduct->Loan_Prod_Name . ' Borrower\'s Agreement', 
			'message' 		=> '',
			'templateId'	=> $this->documentTmplteId,
			'testMode'		=> $data['debugMode']
		);

		$SignerNameAndTitle = Config::get('system.HelloSign.ContactName') . '-' . Config::get('system.HelloSign.ContactPosistion');

		$finalDueDate = strtotime('+37 months'); 
		$finalDueYear = date("Y",$finalDueDate); 
		$ppYear = date('Y', strtotime($paymentDueDt));

		//get Final Payment Value
		$totalPayments = $NLSCurrentInterestBal + $NLSCurrentPrincipalBal;
		
		//Hello Sign Custom Fiels Values
		$customFieldsValues = array(
			'Name'                   => $applName,
			'Address'                => ucfirst( $applAddrs['Street_Addr_1_Txt'] . ', ' . $applAddrs['City_Name'] ),
			'ZipCode'                => $applAddrs['Zip_Cd'],
			'LoanPrice'              => number_format($loanDetail->Loan_Amt, 2),
			'LoanNumber'             => $loanDetail->Loan_Id,
			'InterestRate'           => $interestRate * 100,
			'InterestRateMonthly'	 => number_format( ( $interestRate / 12 ) * 100, 2 ),
			'NLSCurrentInterestBal'  => number_format($NLSCurrentInterestBal, 2),
			'NLSCurrentPrincipalBal' => number_format($NLSCurrentPrincipalBal, 2),
			'NLSTotalPayments'       => number_format($NLSTotalPayments, 2),
			'LoanTerm'               => $loanTerm, 
			'MinimumPayment'         => number_format($monthlyPayment , 2 ),
			'PrincipalPaymentDt'     => date('F d', strtotime($paymentDueDt)), 
			'PrincipalPaymentYear'   => $ppYear[2].$ppYear[3],
			'LoanAmount'             => number_format($loanDetail->Loan_Amt, 2),
			'MaturityDate'           => date('F d, Y', strtotime($maturityDate ) ),
			'AcceptedBy'             => Config::get('system.HelloSign.ContactCompany'),
			'DateAccepted'           => date('Y-m-d'),
			'NameAndTitle'           => $SignerNameAndTitle,
			'FinalPayment'			 => abs(number_format(($monthlyPayment * 36) - $totalPayments, 2)),
			'FinalPaymentDueDate'    => date("F d",$finalDueDate),
			'FinalPaymentDueYear'    => $finalDueYear[2].$finalDueYear[3]
		);
		
		//Get Custom Fiedls From HelloSign
		$customFields = $HelloSign->getCustomFields($this->documentTmplteId); 

		//Mapped HelloSign Fields and Value
		if( count($customFields) > 0 ) {
			foreach( $customFields as $k => $field ) {
				if( isset( $customFieldsValues[$field->name] ) ) {
					$option['customFields'][$field->name] = $customFieldsValues[$field->name];
				}
			}
		}


		//Set Document Signer
		$HelloSign->setSigner(
			array(
				array(
					'code'      => self::APPLNT_SIGNER_CODE, 
					'email'     => $userAccount->Email_Id, 
					'firstName' => $applName
				)
			)
		);

		//Get Embedded Form
		$data = $HelloSign->getEmbeddedForm($option);

		//Set Signature Request Data
		$signatureRequestData =  $data['signatureRequest'];

		$signatures = array(); 

		foreach ( $signatureRequestData->signatures as $key => $signature) {
			$signatures[] = array(
				'signatureId' => $signature->signature_id, 
				'statusCode'  => $signature->status_code
			);
		}

		$signaturesJson = json_encode($signatures);
	 
  		//store the signature id's in session just in case
  		//the applicant reload the page
		if( count($data) > 0 )
			Session::put('helloSign', $data);


		$signingUrl = DB::raw('DEFAULT');

		if( !empty($signatureRequestData->signing_url) )
			$signingUrl = $signatureRequestData->signing_url;

		//Prepare ODS data
		$param = array(
			'userId'             => $this->userId,
			'loanAppNr'          => $this->loanAppNr,
			'stateCd'			 => 'CA',
			'signatureRequestId' => $signatureRequestData->signature_request_id,
			'filesUrl'           => $signatureRequestData->files_url,
			'signingUrl'         => $signatureRequestData->signing_url,
			'signingDt'          => date('Y-m-d'), 
			'signatures'         => $signaturesJson,
			'title'              => $signatureRequestData->title,
			'templateId'         => $option['templateId'],
			'loanTerm'           => '36 Months',
			
		);

		//Prepare Payment Data
		if( !empty( $loanPayment ) ){
			$param['paymentAmt']   = $monthlyPayment;
			$param['paymentDueDt'] = $loanPayment->Payment_Due_Dt;
		} else{
			$param['paymentAmt']   = "0";
			$param['paymentDueDt'] = date('Y-m-d');
		}

		LoanDocument::saveLoanDocument($param);

		$data['isComplete']  = false;
		$data['documentId']  = $this->documentTmplteId;

		//set the loan document into session  
		Session::put('Signature_Req_Id', $signatureRequestData->signature_request_id);


		$data['skipDomainVerification'] = Config::get('system.HelloSign.DomainVerification');

		//Set Applicant Name
 		Session::put('ApplicantName' , $applName ); 
  
		$this->layout->content = View::make('blocks.verification.contract', $data); 
	}

	/**
	 * Update Contract Document 
	 * 
	 *  - Get Contact Document from HelloSign Server
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce
	 * 
	 * @return
	 */
	public function updateLoanDocument()
	{
		 
		if ( !empty( Session::get('Signature_Req_Id') ) ) {

			try{

				$appNameAndDt = Session::get('ApplicantName') . '-'. date('Y-m-d') ; 
				$pdfName = $appNameAndDt . '.pdf'; 

				//Get the contract document and transfer it the system
				$contractDirName = dirname($_SERVER['SCRIPT_FILENAME']).'/files/contract/' ;
				$contractDocPath =  $contractDirName . $this->loanAppNr . '/';

				if( !File::exists($contractDocPath) )
					File::makeDirectory($contractDocPath , 0777, true );

				$s3path = 'contract/'. $this->loanAppNr . '/';

				if( $this->debugMode == 'true' )
					$s3path = 'staging/contract/'. $this->loanAppNr . '/';

				//Get Contract Document from Hello Sign
				$HelloSign = App::make('hellosign');
				$HelloSign->getFiles(
						Session::get('Signature_Req_Id'),  
						$contractDocPath . $pdfName
				);

				$imageUrlTxt = json_encode( array( $s3path . $pdfName ) );

				//Set Document Obj
				$this->documentObj = array(
					'Task_Doc_Upload_URL_Txt' => $imageUrlTxt, 
					'Reqd_File_Cnt' => 1
				);

 
				//@Todo = Set Loan App Verification Id in constant
				$loanAppVerId = 1; 
				$this->updateCompletedAppVer($loanAppVerId);

				//Update Contract Date
				$this->updateContractDt( $this->loanAppNr );

				//Create NLS Task
				$taskComment = array(

					'commentStr' => 'Review Contract
									 No fields needed from application.',
					'files'		 => $this->prepareCommentFiles(
										$contractDocPath.$pdfName, 
										self::COMMENT_CATEGORY_CONTRACT, 
										$appNameAndDt 
									)
				);
 			
 				//Set NLS Task Template Number
				$this->taskTemplateNr = 2;

				//Set ODSVerification ID
				$this->ODSVerificationId = $loanAppVerId;

				//Create Salesforce Verification Object
				$sfObject = $this->createSFVerificationObject('Contract_Verification__c');

				$this->setSFVerificationId($sfObject);

				//set a timeout to get the salesforce verification Id
				sleep(2);

				$task = $this->createTask( self::TASK_NAME_CONTRACT, $taskComment , '' );

				//Create an Appliction data 
				$this->createApplVerification( $loanAppVerId );
				
			 	$loanAppPhaseNr = 7;
				$subPhaseCode   = 'LoanContractSubmitted';

				//Set Loan Application Phase to  Final Offer - Email Verified
				setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

				//Upload Contract To S3
				$s3 =  $this->uploadToS3( $s3path . $pdfName, $contractDocPath . $pdfName);

				//Update Files URL in Loan Document 
				$this->updateContractDocURL( $this->loanAppNr , $this->s3URL . 'contract/'. $this->loanAppNr . '/' . $pdfName  );
				
				return Response::json(
					array(
						's3'   => $s3,
						'task' => $task
					)); 
			
			}catch(Exception $e) {

				$message = $e->getMessage(); 
				return Response::json(
					array(
						'message' => $message
					));
			}
		} 
	}

	/**
	 * Update Contract DOC URL 
	 * 
	 * @param  integer $loanAppNr
	 * @param  string $s3Path
	 * @return
	 */
	public function updateContractDocURL( $loanAppNr, $s3Path )
	{
		//Update Files URL in Loan Document 
		LoanDocument::where( array(
				'Loan_App_Nr' => $loanAppNr
			)
		)->update(
			array(
				'Doc_Url_Txt' => $s3Path 
			)
		);
	}

	/**
	 * Update Loan Contract Date
	 * 
	 * @param  integer $loanAppNr
	 * @return
	 */
	public function updateContractDt( $loanAppNr )
	{
		//Update Contract Date
		LoanDetail::where('Loan_Id', $loanAppNr )
					->update(
						array(
							'Contract_Dt' => date('Y-m-d')
						)
					);
	}
 
	/**
	 * Show Employment Verfication
	 * 
	 * @return
	 */
	protected function showEmploymentVerification()
	{
		
		$loanAppVerId = 3;

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');

		$data['states'] = getStatesArray();
		$this->layout->content = View::make('blocks.verification.employment', $data );	
	}

	/**
	 * Employment Verification 
	 *
	 *  - Store Information into Employment Info Table
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce
	 * 
	 * @return json
	 */
	protected function postEmploymentVerification()
	{
		$data = Input::all();

		if( count($data) > 0 ) {

			try {

				$notes 			= '';
				$contactResult  = '';

				if( $data['SelfEmployed'] == 'no' ) { 

					ApplicantEmploymentInfo::newEmploymentInfo($this->userId, $data );

					//Set Document Obj
					$this->documentObj = array(
						'Task_Doc_Upload_URL_Txt' => '', 
						'Reqd_File_Cnt' => 0
					);
 
					//Split the the name to two parts for firstname an last name 
					$contactName = explodeStr( $data['ContactPersonName'], ' ' );
					$contact = array(
						'UserId'           => $this->userId,
						'LastName'         => (isset($contactName[0])) ? $contactName[0] : '',
						'FirstName'        => (isset($contactName[1])) ? $contactName[1] : '', 
						'CompanyName'      => $data['EmployerName'], 
						'EmployerName'     => $data['EmployerName'], 
						'StreetAddress1'   => $data['StreetAddress1'], 
						'StreetAddress2'   => $data['StreetAddress2'], 
						'Position'         => '', 
						'City'             => $data['City'],
						'State'            => $data['State'],
						'ZipCode'          => $data['Zip'], 
						'PhoneNr'          => $data['PhoneNr'], 
						'EmailAddress1'    => '', 
						'Tenureship'       => $data['Tenureship'], 
						'EmploymentStatus' => 'EMPLOYEE'
					);

					//Insert Contact
					$contactResult = $this->createNLSAddressBook( $contact );
					//Tenureship text Values
					$tenureshipValues = array(
						'',
						'Less than 6 months',
						'6-12 months',
						'1-2 years',
						'2 years'
					);
					
					$TaskComment = array(
						'commentStr' => '
							Confirm Employee Information
							1. Employee First Name : '. $this->applicant['firstName'] .'
							2. Employee Last Name : '. $this->applicant['lastName'] .'
							3. Employee Work Phone # : '. $data['PhoneNr'] .'
							4. Company Name : '. $data['EmployerName'] .'
							5. Company Phone # : '. $data['WorkPhoneNr'] .'
							6. Company Address 1: '. $data['StreetAddress1'] .'
							7. Company Address 2: '. $data['StreetAddress2'] .'
							8. City : '. $data['City'] .'
							9. Zip Code : '. $data['State'] .' '. $data['Zip'] .'
							10. Supervisor Name : '. $data['ContactPersonName'] .'
							11. Supervisor Phone # : '. $data['SupervisorPhoneNr'] .'
							12. Employment Title : ' . $data['JobTitleName'] . '
							13. Time in Position : '. $tenureshipValues[$data['Tenureship']] .'
							14. Income Type : '. $data['IncomeType'] .'
							15. Pay Frequency : '. $data['PayFrequency'],
						'files'	=> ''		
					);

				} else {

					$notes = 'Self Employed';
					$TaskComment = array( 
						'commentStr' => 'Confirm Employee Information : Self Employed',
						'files'	=> '' 
					);
				}

				
				$loanAppPhaseNr = 7;
				$subPhaseCode   = 'EmploymentVerificationSumbitted';
 
				//Set Loan Application Phase to  Final Offer - Email Verified
				setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

				$loanAppVerId = 3;

				//Set NLS Task Template Number
				$this->taskTemplateNr = 1;

				//Set ODSVerification ID
				$this->ODSVerificationId = $loanAppVerId;

				//update Completed Application Verification
				$this->updateCompletedAppVer($loanAppVerId);

				//Create Salesforce Object
				$sfObject = $this->createSFVerificationObject('Employment_Verification__c');

				//Set SF Verification Id
				$this->setSFVerificationId($sfObject);

				//set a timeout to get the salesforce verification Id
				sleep(2);
			 	
				//Insert NLS TASK 
				$this->createTask( self::TASK_NAME_EMPLOYMENT, $TaskComment , $notes );

				//Create an Appliction data
				$this->createApplVerification($loanAppVerId);

				return Response::json( array(
					'result'       => 'success', 
					'data'         => $data, 
					'contact'      => $contactResult,
					'message'      => showNotification(300),
					'sfObject'     => $sfObject,
					'verification' => $applicantVerification, 
					'debugMode'    => Config::get('system.Ascend.DebugMode') 
				), 200 );

			} catch(Exception $e) {

				return Response::json( array(
						'result'  => 'failed',
						'data'    => $data, 
						'message' => $e->getMessage()
					), 200 );

			}

		}else{
			return Response::json(showInvalidParameterError());
		}
	}

	/**
	 * Upload Income Verification Files
	 * 
	 * @return
	 */
	protected function uploadIncomeVerification()
	{
		$this->layout = "";

		$data = Input::all();

		if( isset($data['files2']) ){
			$options['files'] 	   = $data['files2'];
			$options['param_name'] = 'files2';
		}elseif ( isset($data['files3']) ) {
			$options['files'] 	   = $data['files3'];
			$options['param_name'] = 'files3';		
		} else{
			$options = $data;
		}

		$options['upload_dir'] =  dirname($_SERVER['SCRIPT_FILENAME']).'/files/income/' . $this->loanAppNr . '/';
        $options['upload_url'] = url() . '/files/income/'.$this->loanAppNr . '/';
 
		$uploadHandler = new UploadHandler($options);
	}

	/**
	 * Storing of Income Verification Data
	 *
	 *  - Storing Income Information
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce
	 *  
	 * @return
	 */
	protected function postIncomeVerification()
	{ 
		$data = Input::all();

		$Pay_Type_Desc 			= '';
		$Pay_Frequency_Desc		= '';
		$Employer_Name			= '';
		$Street_Addr_1_Txt		= '';
		$Contact_Person_Name	= '';

		if( count($data) > 0 ) {

			try { 
				//convert json data to array
				$images = json_decode( $data['CheckImageData'], true );

				$imageURL = array();
				$docs     = array(); 
				$dirName  = dirname($_SERVER['SCRIPT_FILENAME']).'/files/income/'; 

				if( count($images) > 0 ) {
					foreach ($images as $key => $image) {
						$docs[]     = $dirName .$this->loanAppNr. '/' . $image ;
						$imageURL[] = $this->s3URL . 'income/' . $this->loanAppNr . '/' . $image; 
					}
				}

				$imageUrlTxt  = json_encode($imageURL);

				$this->documentObj = array(
					'Task_Doc_Upload_URL_Txt' => $imageUrlTxt, 
					'Reqd_File_Cnt' => count($images)
				);
 
				$data['userId']      = $this->userId; 
				$data['loanAppNr']   = $this->loanAppNr; 
				$data['imageUrlTxt'] = $imageUrlTxt;

 				ApplicantIncomeVerification::newIncomeVerification( $data );
				 
				//@todo need to put this in constant
				$loanAppVerId = 4;

				//update Completed Application Verification
				$this->updateCompletedAppVer( $loanAppVerId );

				//insert Comment Task
				$this->createNLSComment( self::TASK_NAME_INCOME, $docs, self::COMMENT_CATEGORY_INCOME );							

				$appEmpInfo = ApplicantEmploymentInfo::find($this->userId);

				if( count($appEmpInfo) > 0 ){
					$Pay_Type_Desc 			= $appEmpInfo['Pay_Type_Desc'];
					$Pay_Frequency_Desc		= $appEmpInfo['Pay_Frequency_Desc'];
					$Employer_Name			= $appEmpInfo['Employer_Name'];
					$Street_Addr_1_Txt		= $appEmpInfo['Street_Addr_1_Txt'];
					$Contact_Person_Name	= $appEmpInfo['Contact_Person_Name'];
				}

				//Get Applicant Information
				$isRow = true;
				$appIncomeInfo = getApplicantFinancialInfo(
									$this->userId, 
									array('Annual_Gross_Income_Amt'), 
									$isRow
								);

				$TaskComment = array(
					'commentStr' => '
						Confirm income 
						(this is a paystub well confirm applicant’s 
						data and the employer data in this step)
						
						1. Applicant First Name : '. $this->applicant['firstName'] .' 
						2. Applicant Last Name : '. $this->applicant['lastName'] .' 
						3. Applicant Address : '. $this->applicant['address'] .'
						4. Income Type : '. $Pay_Type_Desc .'
						5. Payment Frequency : '. $Pay_Frequency_Desc .'
						6. Annual Salary reported in application : N/A
						7. Name of Employer : '. $Employer_Name .'
						8. Address of Employer : '. $Street_Addr_1_Txt .'
						9. Confirming Employer : '. $Contact_Person_Name .'
						10. Confirm Income Amount: '. $appIncomeInfo->Annual_Gross_Income_Amt ,

					'files'	=> $this->prepareCommentFiles($docs, self::COMMENT_CATEGORY_INCOME)
				);


				//Set NLS Task Template Number
				$this->taskTemplateNr = 3;

				//Set ODSVerification ID
				$this->ODSVerificationId = $loanAppVerId;

				//Create Salesforce Verification Object
				$sfObject = $this->createSFVerificationObject('Income_Verification__c');
				
				$this->setSFVerificationId($sfObject);

				//set a timeout to get the salesforce verification Id
				sleep(2);
 
				//insert NLS TASK 
				$this->createTask( self::TASK_NAME_INCOME, $TaskComment, '' );

				//Create an Appliction data
				$this->createApplVerification($loanAppVerId); 

				$loanAppPhaseNr = 7;
				$subPhaseCode   = 'IncomeVerificationSubmitted';

				//Set Loan Application Phase to  Final Offer - Email Verified
				setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );	

				//Upload Images to AWS - S3
				if( count($images) > 0 ) {
					foreach ($images as $key => $image) { 
						$s3path  = 'income/' .$this->loanAppNr. '/' . $image ;

						if($this->debugMode == 'true'){
							$s3path  = 'staging/income/' .$this->loanAppNr. '/' . $image ;
						}

						$imgPath = $dirName .$this->loanAppNr. '/' . $image ;
						$this->uploadToS3($s3path, $imgPath);
					}
				}
 				 
				return Response::json( array(
					'result'    => 'success', 
					'data'      => $data, 
					'message'   => showNotification(300) ,
					'debugMode' => Config::get('system.Ascend.DebugMode')
				), 200 );

			} catch(Exception $e) {
				return Response::json( array(
						'result'  => 'failed',
						'data'    => $data, 
						'message' => $e->getMessage()
					), 200 );
			}

		}else{
			return Response::json(showInvalidParameterError());
		}
	}

	/**
	 * Show Payment Verfication
	 * 
	 * @return
	 */
	protected function showIncomeVerification()
	{ 
		$loanAppVerId = 4; //App verifcation ID

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');

		$steps = array();

		$stepsData = $this->getCompletedAppVer($this->userId, $this->loanAppNr);
		
		if($stepsData)
			$steps = unserialize( $stepsData->Verification_Phase_Status_Txt );

		$appData = getApplicantData();		
		$isSelfEmployed = ApplicantEmploymentInfo::find($appData['userId']);

		$data['selfEmployed'] 	= (!$isSelfEmployed) ? true : false;		
		
		if(isset($steps['3']))
			$data['employmentStep'] = ($steps['3']['value'] == 'y')? true : false;
		else
			$data['employmentStep'] = false;
		
		$this->layout->content = View::make('blocks.verification.income', $data);	
	}

	/**
	 * Show Identity Document
	 * 
	 * @return
	 */
	protected function showIdentityDocumentVerification()
	{ 
		$loanAppVerId = 5; //App verifcation ID

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');

		$data['states'] = getStatesArray();
		$this->layout->content = View::make('blocks.verification.document', $data);	
	}

	/**
	 * Show Identity Verfication
	 * 
	 * @return
	 */
	protected function showIdentityVerification()
	{ 
		$loanAppVerId = 7; //App verifcation ID

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');

		$data['test'] = '';
		$this->layout->content = View::make('blocks.verification.identity', $data);	
	}

	/**
	 * Upload Identification 
	 * 
	 * @return
	 */
	protected function uploadIdentification()
	{
		$this->layout = "";

		$options = Input::all();
		$options['upload_dir'] =  dirname($_SERVER['SCRIPT_FILENAME']).'/files/identification/' . $this->loanAppNr . '/';
        $options['upload_url'] = url() . '/files/identification/' . $this->loanAppNr . '/';
 
		$uploadHandler = new UploadHandler($options);

	}

	/**
	 * Identity Verification
	 *
	 *  - Storing Income Information
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce
	 *  
	 * @return
	 */
	protected function postIdentityVerification($taskStatus = 'DENIED')
	{
		
		$data = Input::all();

		$loanAppVerId = 7;

		//update Completed Application Verification
		$this->updateCompletedAppVer($loanAppVerId);

		// Idology Result
		$idologyResult = IdologyResult::where('Idology_Id_Nr', '=', $data['idNumber'])
				->orderBy('Idology_Result_Dttm', 'DESC')
				->first();	

		$commentStr = '
			Summmary Results				
				ID Number :    '. $data['idNumber'] .'
				IP Address :   '. $idologyResult->IP_Addr_Id .'
				KBA Flag :     '. $idologyResult->Idology_Result_Desc .'
				KBA Results :  '. $idologyResult->Idology_Reason_Txt .'
				Answer Details';

		//@todo - please use isset instead of array_key_exists
		if( array_key_exists('question1Type', $data) ){
           $commentStr .= '
            	question1 : '.$data['question1Type'].'
            	answer1   : '.$data['question1Answer'];
        }

        if(array_key_exists('question2Type', $data)){
            $commentStr .= '
            	question2 : '.$data['question2Type'].'
            	answer2   : '.$data['question2Answer'];
        }

        if(array_key_exists('question3Type', $data)){
            $commentStr .= '
            	question3 : '.$data['question3Type'].'
            	answer3   : '.$data['question3Answer'];
        }

        if(array_key_exists('question4Type', $data)){
            $commentStr .= '
            	question4 : '.$data['question4Type'].'
            	answer4   : '.$data['question4Answer'];
        }

        if(array_key_exists('question5Type', $data)){
            $commentStr .= '
            	question5 : '.$data['question5Type'].'
            	answer5   : '.$data['question5Answer'];
        } 

       	$TaskComment = array(
			'commentStr'	=> $commentStr,
			'files'	=> ''	 
		);
 
		$loanAppPhaseNr = 7;
		$subPhaseCode   = 'IdentityVerificationSubmitted';

		//Set Document Obj
		$this->documentObj = array(
			'Task_Doc_Upload_URL_Txt' => '', 
			'Reqd_File_Cnt' => 0
		);

		//Set Loan Application Phase to  Final Offer - Email Verified
		setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

		//Set NLS Task Template Number
		$this->taskTemplateNr = 8;

		//Set ODSVerification ID
		$this->ODSVerificationId = $loanAppVerId;

		//Create Salesforce Verification Object
		$sfObject = $this->createSFVerificationObject('Identity_Verification__c');

		$this->setSFVerificationId($sfObject);

		//set a timeout to get the salesforce verification Id
		sleep(2);

		//Create NLS Task
		$task =  $this->createTask( self::TASK_NAME_IDENTITY, $TaskComment, '', $taskStatus );

		//Create an Appliction data
		$this->createApplVerification($loanAppVerId); 
 				
		return Response::json( array(
			'result'  => 'success', 
			'data'    => $data, 
			'message' => showNotification(300) ,
			'debugMode'  => Config::get('system.Ascend.DebugMode')
		), 200 );
	}


	/**
	 * Idology Result
	 *
	 *
	 * @todo  transfer this to idology model
	 * @param  string $type
	 * @return 
	 */
    public function idologyResult( $type = 'abandoned' )
    {

		$this->layout  = null;
		$data          = Input::all();
		$userId        = getUserSessionID();
		$loanAppNr     = getLoanAppNr();
		
		//get the client real ip address       
        $ip = getRealClientIpAddress();

        $idologyResult = new IdologyResult();

		// insert idology transaction table	        
        $idologyResult->User_Id              = $userId;
        $idologyResult->Idology_Id_Nr        = $data['idNumber'][0];
        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
        $idologyResult->IP_Addr_Id     		 = $ip;
        $idologyResult->Idology_Result_Desc  = 'Fail';
        $idologyResult->Idology_KBA_Flag     = 0;
        $idologyResult->Idology_Reason_Txt   = $type;
        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
        $idologyResult->Created_By_User_Id   = $userId;
        $idologyResult->Create_Dt            = date('Y-m-d');
        $idologyResult->save();

        setLoanAppPhase( $userId , $loanAppNr, 7, 'FraudCheckFail'  );

		//insert NLS TASK 
		$task =  $this->createTask( self::TASK_NAME_IDENTITY, array(), '', 'DENIED' );


	}

	/**
	 * Close Idology Result
	 *
	 *
	 * @todo   transfer this to idology model
	 * @param  int $idNumber
	 * 
	 * @return 
	 */
    public function closeIdology( $idNumber = 0 )
    {

		$this->layout  = null;
		$userId        = getUserSessionID();
		$loanAppNr     = getLoanAppNr();
		
		//get the client real ip address       
        $ip = getRealClientIpAddress();

        $idologyResult = new IdologyResult();

		// insert idology transaction table	        
        $idologyResult->User_Id              = $userId;
        $idologyResult->Idology_Id_Nr        = $idNumber;
        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
        $idologyResult->IP_Addr_Id     		 = $ip;
        $idologyResult->Idology_Result_Desc  = 'Fail';
        $idologyResult->Idology_KBA_Flag     = 0;
        $idologyResult->Idology_Reason_Txt   = 'Closed page';
        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
        $idologyResult->Created_By_User_Id   = $userId;
        $idologyResult->Create_Dt            = date('Y-m-d');
        $idologyResult->save();

        setLoanAppPhase( $userId , $loanAppNr, 7, 'FraudCheckFail'  );

		//insert NLS TASK 
		$task =  $this->createTask( self::TASK_NAME_IDENTITY, array(), '', 'DENIED' );

	}

	/**
	 * Identity Document Verification
	 * 
	 *  - Storing Identity Document
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce 
	 *  
	 * @return
	 */
	protected function postIdentityDocumentVerification()
	{
		$data = Input::all();

		if( count($data) > 0 ) {

			$AppIdInfo = new ApplicantIdentityInfo();

			//get applicant city name
			$appAddressInfo = getApplicantAddress( $this->userId , array('City_Name') );

			//Set s3 URL and ODS image URL
			$dirName     = dirname($_SERVER['SCRIPT_FILENAME']).'/files/identification/';
			$keyParam    = $this->loanAppNr . '/' . $data['ImageData']; 

			//Convert the document URL to s3 path
			$images = array(
				$this->s3URL . 'identification/' . $keyParam
			);

			$imageUrlTxt = json_encode( $images );
			$docPath     = $dirName . $keyParam ;

			//Set Document Obj
			$this->documentObj = array(
				'Task_Doc_Upload_URL_Txt' => $imageUrlTxt, 
				'Reqd_File_Cnt' => 1
			);

			//Store Applicant Identity 
			$AppIdInfo->User_Id            = $this->userId;
			$AppIdInfo->Loan_App_Nr        = $this->loanAppNr;
			$AppIdInfo->Id_Nr    		   = $data['IdNumber'];
			$AppIdInfo->State_Cd           = $data['State'];
			$AppIdInfo->Id_Image_URL_Txt   = Gfauth::encryptFields( $imageUrlTxt ); 
			$AppIdInfo->Created_By_User_Id = $this->userId;
			$AppIdInfo->Create_Dt          = date('Y-m-d'); 
			$AppIdInfo->save();
 
			$loanAppVerId = 5;

			//Set NLS Task Template Number
			$this->taskTemplateNr = 7;

			//Set ODSVerification ID
			$this->ODSVerificationId = $loanAppVerId;

			//Update Completed Application Verification
			$this->updateCompletedAppVer($loanAppVerId);

			//Insert NLS Comment and Task 
			$comment = $this->createNLSComment( self::TASK_NAME_DOCUMENTS, $docPath, self::COMMENT_CATEGORY_IDENTITY );
			
			//NLS Task Comments
			$TaskComment = array(
				'commentStr'	=> '
					Confirm ID
					
					1. First Name : '. $this->applicant['firstName'] .'
					2. Last Name : '. $this->applicant['lastName'] .'
					3. Address : '. $this->applicant['address'] .'
					4. ID Number: '. $data['IdNumber'] .'
					5. City : '. $appAddressInfo->City_Name ,
				'files'	=> $this->prepareCommentFiles($docPath, self::COMMENT_CATEGORY_IDENTITY)
			);

			//Create SF Verification Object
			$sfObject = $this->createSFVerificationObject('Identity_Doc_Verification__c');

			// _pre($sfObject);

			$this->setSFVerificationId($sfObject);

			//set a timeout to get the salesforce verification Id
			sleep(2);
			
			//insert NLS TASK 
			$task =  $this->createTask( self::TASK_NAME_DOCUMENTS, $TaskComment, '' );

			//Create an Application Verification Data
			$this->createApplVerification($loanAppVerId); 
			
			$loanAppPhaseNr = 7;
			$subPhaseCode   = 'IdentityDocumentsSubmitted';

			//Set Loan Application Phase
			setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

			$s3path = 'identification/' . $keyParam;

			if( $this->debugMode == 'true')
				$s3path = 'staging/identification/' . $keyParam;

			//Upload the document to S3
			$this->uploadToS3($s3path, $docPath);
 
			return Response::json( 
					array(
						'result'    => 'success', 
						'message'   => showNotification(300), 
						'comment'   => $comment, 
						'task'      => $task,
						'data'      => $data,
						'verObject' => $sfObject,
						'debugMode' => Config::get('system.Ascend.DebugMode') 
					), 200 
				);

		}else{
			return Response::json(showInvalidParameterError());
		}
	}

	/**
	 * Show Funding Verfication
	 * 
	 * @return
	 */
	protected function showFundingVerification()
	{  
		$loanAppVerId = 2; //App verifcation ID

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');

		$this->layout->content = View::make('blocks.verification.funding');	
	}

	/**
	 * Upload Employment Check
	 * 
	 * @return
	 */
	protected function uploadFundingCheck()
	{

		$this->layout = "";

		$options = Input::all();
		$options['upload_dir'] =  dirname($_SERVER['SCRIPT_FILENAME']).'/files/funding/'.$this->loanAppNr.'/';
        $options['upload_url'] = url() . '/files/funding/'. $this->loanAppNr . '/';

		$uploadHandler = new UploadHandler($options);
 
	}

	/**
	 * Post Funding
	 * 
	 *  - Storing Funding Information
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce
	 *  
	 * @return json
	 */
	protected function postFundingVerification()
	{
		$data = Input::all();

		if( count($data) > 0 ) {

			$AppFundVer = new ApplicantFundingVerification();

			$dirName  = dirname($_SERVER['SCRIPT_FILENAME']).'/files/funding/';
			$keyParam = $this->loanAppNr .'/' . $data['CheckImageData'];
			$docPath  = $dirName . $keyParam;
			$s3path   = 'funding/'. $keyParam;

			//Convert Image to JSON encode
			$images = array(
				$this->s3URL . 'funding/' . $keyParam
			);

			$imageUrlTxt = json_encode($images);

			//Set Document Obj
			$this->documentObj = array(
				'Task_Doc_Upload_URL_Txt' => $imageUrlTxt, 
				'Reqd_File_Cnt' => 1
			);


			if( $this->debugMode == 'true' )
				$s3path =  'staging/funding/'. $this->loanAppNr . '/' . $data['CheckImageData'] ;

			//prepare data to be stored
			$AppFundVer->User_Id             = $this->userId;
			$AppFundVer->Loan_App_Nr         = $this->loanAppNr;
			$AppFundVer->Acct_First_Name     = Gfauth::encryptFields($data['AcctFirstName']);
			$AppFundVer->Acct_Last_Name      = Gfauth::encryptFields($data['AcctLastName']);
			$AppFundVer->Bank_Routing_Nr     = Gfauth::encryptFields($data['BankRoutingNr']);
			$AppFundVer->Check_Image_URL_Txt = Gfauth::encryptFields($imageUrlTxt);
			$AppFundVer->Acct_Nr             = Gfauth::encryptFields($data['AcctNr']);
			$AppFundVer->Created_By_User_Id  = $this->userId;
			$AppFundVer->Create_Dt           = date('Y-m-d'); 
			$AppFundVer->save();

 			$data['BankName'] = 'Temp Bank Name';
			$data['Amount']   = 0; 

			if( !empty( $this->loanDetail ) )
				$data['Amount'] = $this->loanDetail->Loan_Amt;

			$loanAppVerId = 2; // Funding Verification Id

			//update Completed Application Verification
			$this->updateCompletedAppVer($loanAppVerId);

			//Create CIF Vendor
			$vendor =  array(
				'UserId'        => $this->userId,
				'PayeeName'     => $data['AcctFirstName'] . ' ' . $data['AcctLastName'], 
				'AcctNr'        => $data['AcctNr'], 
				'BankRoutingNr' => $data['BankRoutingNr']
			);

			$CIFVendor = $this->createNLSCIFVendor( $vendor );

			$TaskComment = array(
				'commentStr'	=> '
					Confirm Bank Account

					1. ABA Number : '. $data['AcctNr'] .'
					2. Routing Number : ' . $data['BankRoutingNr'] . '
					3. First Name : '. $this->applicant['firstName'] .'
					4. Last Name : ' . $this->applicant['lastName'] . '
					5. Address : ' . $this->applicant['address'],
				'files'		 	=> $this->prepareCommentFiles($docPath, self::COMMENT_CATEGORY_FUNDING )
			);

			//Set NLS Task Template Number
			$this->taskTemplateNr = 4;

			//Set ODSVerification ID
			$this->ODSVerificationId = $loanAppVerId;

			//Create Salesforce Verification Object
			$sfObject = $this->createSFVerificationObject('Funding_Account_Verification__c');

			$this->setSFVerificationId($sfObject);

			//Delay the creation of Task and Application Verification 
			sleep(2);

			//insert NLS Task
			$task = $this->createTask( self::TASK_NAME_FUNDING, $TaskComment , '' );

			//Create an Appliction data
			$this->createApplVerification($loanAppVerId); 

			//insert NLS comment 
			$comment = $this->createNLSComment( self::TASK_NAME_FUNDING, $docPath, self::COMMENT_CATEGORY_FUNDING );

			$loanAppPhaseNr = 7;
			$subPhaseCode   = 'FundingAccountInformationSubmitted';

			//Set Loan Application Phase to  Final Offer - Email Verified
			setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

			//Upload Document to S3
			$this->uploadToS3($s3path, $docPath);

			return Response::json( 
					array(
						'result'  	=> 'success', 
						'message' 	=> showNotification(300), 
						'comment' 	=> $comment, 
						'task'    	=> $task,
						'vendor'  	=> $CIFVendor,
						'data'    	=> $data,
						'sfObject'  => $sfObject,
						'debugMode' => Config::get('system.Ascend.DebugMode')
					), 200 
				);
		}else{
			return Response::json(showInvalidParameterError());
		}
	}

	/**
	 * Get List of Verification Steps
	 * 
	 * @return
	 */
	protected function getCompletedAppVer( $userId, $loanAppNr )
	{
	  	return CompletedApplicantVerification::getCompletedAppVer($userId, $loanAppNr);
	}

	/**
	 * Check if Applicant Verification is Complete
	 * 
	 * @return
	 */
	protected function isAppVerCompleted( $appVerid )
	{
	  	$cav = CompletedApplicantVerification::getCompletedAppVer($this->userId, $this->loanAppNr);

		if( isset($cav->Verification_Phase_Status_Txt) ){

			$unserializeSteps = unserialize($cav->Verification_Phase_Status_Txt);
			
			$ret = ( $unserializeSteps[$appVerid]['value'] == 'y' )? true : false;

		}else
			$ret = false;	  	

	  	return $ret; 
	}


	/**
	 * Update Completed Verification
	 * 
	 * @param  integer $appVerId
	 * @return
	 */
	protected function updateCompletedAppVer( $appVerId = 1 )
	{
	 
	 	return CompletedApplicantVerification::setCompletedApplicantVerification(
	 		$this->userId, 
	 		$this->loanAppNr, 
	 		$appVerId
	 	);
	} 

	/**
	 * Show Payment Verification 
	 * 
	 * @return
	 */
	protected function showPaymentVerification()
	{
		
		$loanAppVerId = 6; //App verifcation ID

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');


		$fundingVerStepId = 2 ; 
		$unserializeSteps = array();

		$data['monthlyPayment'] = "";
		$data['bankAccntNr']    = "";
		$data['bankRoutingNr']  = "";
		$data['companyName']    = "";
 
		$steps = $this->getCompletedAppVer($this->userId, $this->loanAppNr );
  
		if( isset($steps->Verification_Phase_Status_Txt) ){
			$unserializeSteps = unserialize($steps->Verification_Phase_Status_Txt);
		}	

		$data['isFundingStepComplete'] = false;

		if( count($unserializeSteps) > 0 && isset( $unserializeSteps[$fundingVerStepId] ) ) {

			//Get Funding Detail
			$fundingDetail  = ApplicantFundingVerification::getData(
								array('Bank_Routing_Nr', 'Acct_Nr', 'Acct_First_Name', 'Acct_Last_Name'), 
								array('Loan_App_Nr' => $this->loanAppNr ),
								true
							);
			
			if( !empty( $fundingDetail ) ) {
				
				$companyName = self::ACH_AL_COMPANY_NAME;

				if( $this->loanDetail->Loan_Prod_Id == 2 )
					$companyName = self::ACH_RR_COMPANY_NAME;
 
				$data['monthlyPayment'] = $this->loanDetail->Monthly_Payment_Amt;
				$data['bankAccntNr']    = $fundingDetail->Acct_Nr; 
				$data['bankRoutingNr']  = $fundingDetail->Bank_Routing_Nr;
				$data['companyName']    = $companyName;
				$data['isFundingStepComplete'] = true;
			}
		}
	 
		$this->layout->content = View::make('blocks.verification.payment', $data );	
	}

	/**
	 * Submit Payment Verfication Data 
	 * 
	 *  - Storing Payment Information
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce
	 *  
	 * @return
	 */
	protected function postPaymentVerification()
	{

		$data = Input::all();

		if( count($data) > 0 ) {

			try{

				$AppPaymentMethodVerf = ApplicantPaymentMethodVerification::firstOrNew(
								 		array(
								 			'User_Id' 		=> $this->userId,
								 			'Loan_App_Nr'	=> $this->loanAppNr
								 		)
							 	); 
		 
				$AppPaymentMethodVerf->User_Id            = $this->userId;
				$AppPaymentMethodVerf->Loan_App_Nr        = $this->loanAppNr;
				$AppPaymentMethodVerf->Payment_Method_Id  = $data['paymentMethodId'];
				$AppPaymentMethodVerf->Created_By_User_Id = $this->userId;
				$AppPaymentMethodVerf->Create_Dt          = date('Y-m-d');
				$AppPaymentMethodVerf->Updated_By_User_Id = $this->userId;
				$AppPaymentMethodVerf->Update_Dt          = date('Y-m-d'); 

				if( $AppPaymentMethodVerf->exists == 0 )
					$AppPaymentMethodVerf->save(); 

				//Set Document Obj
				$this->documentObj = array(
					'Task_Doc_Upload_URL_Txt' => '', 
					'Reqd_File_Cnt' => 0
				);
				
				$loanAppVerId = 6; 

				//update Completed Application Verification
				$this->updateCompletedAppVer($loanAppVerId);
				
				$TaskComment = array(
					'commentStr' => 'TASK_NAME_PAYMENT',
					'files'	=> ''
				);

				//Set NLS Task Template Number
				$this->taskTemplateNr = 5;

				//Set ODSVerification ID
				$this->ODSVerificationId = $loanAppVerId;

				//Create Salesforce Verification Object
				$sfObject = $this->createSFVerificationObject('Payment_Method_Verification__c');

				$this->setSFVerificationId($sfObject);

				//Delay the creation of Task and Application Verification 
				sleep(2);

				//create NLS task
				$this->createTask(self::TASK_NAME_PAYMENT, $TaskComment , '');

				//Create an Appliction data
				$this->createApplVerification( $loanAppVerId );

				$contractDt = $this->loanDetail['Contract_Dt'];

				if( empty($contractDt) )
					$contractDt = date('Y-m-d');

				$billingStartDate = date('m/d/Y', strtotime('+30 day' . $contractDt) );

				$achResult = "";

				//Create an ACH Record if the applicant choose the EFT ( Electronic Fund Transfer )
				if( $data['paymentMethodId'] == 1 ) {
				
					$ach = array(
						'BankName'         => $data['companyName'],
						'BankRoutingNr'    => $data['bankRoutingNr'],
						'AcctNr'           => $data['bankAccntNr'],
						'Amount'           => $data['monthlyPayment'], 
						'BillingStartDate' => $billingStartDate
					);
  
					$achResult = $this->createNLSACH($ach);		
				}

				$loanAppPhaseNr = 7;
				$subPhaseCode   = 'PaymentVerificationSubmitted';

				//Set Loan Application Phase to  Final Offer - Email Verified
				setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

				return Response::json( array(
								'result'    => 'success', 
								'message'   => showNotification(300),
								'data'      => $data,
								'debugMode' => Config::get('system.Ascend.DebugMode'), 
								'achResult' => $achResult
							 ) 
						); 
			 
			}catch(Exception $e){
				return Response::json( array(
							 'result'  => 'failed', 
							 'message' => $e->getMessage() 
							 ) 
						);
			}
 
		} 
	}

	/**
	 * Show Verification Result
	 * 
	 * @return
	 */
	protected function showVerificationResult()
	{	

		$method = Request::method();

		if ( Session::get('secure') && isset( $this->userId ) && isset( $this->loanAppNr ) )
		{ 

			//double check the verification validation
			$forApproval = TRUE;

			//get completed application verification
			$steps = $this->getCompletedAppVer($this->userId, $this->loanAppNr);

			if( isset( $steps->Verification_Phase_Status_Txt ) ){
				$data['complete'] = unserialize( $steps->Verification_Phase_Status_Txt );
			}
	 			
	 		//check if all validation steps are complete
	 		if( count( $data['complete'] ) > 0 ) {

				foreach ($data['complete'] as $key => $step) {
					if( $step['value'] == 'n' ){
						$forApproval = FALSE;
						break;
					}
				}

			} 
 
			if( $forApproval == FALSE )
				return Redirect::to('verification/steps');


			$data['result'] = 'success'; 

			setActiveStepSession(6);

			$loanAppPhaseNr = 8;
			//Set Loan Application Phase to  Final Offer - Email Verified
			setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr );
			 
			//get Two Working Days to Process the Loan
			$data['processedDate'] = date ( 'M d, Y' , strtotime ( '2 weekdays' ) );

			$this->layout->content = View::make('blocks.verification.result', $data );

		} else{
			return Redirect::to('verification/steps');
		} 
	}

	/**
	 * Show Bank Account Verification 
	 * 
	 * @return
	 */
	protected function showBankAccountVerification()
	{
		$loanAppVerId = 8; //App verifcation ID

		if($this->isAppVerCompleted($loanAppVerId))
			return Redirect::to('verification/steps');


		//Each Reload Needs to generate new Fast Link Url
		$Yodlee = new Yodlee();
		$Yodlee->createYodleeAccount(); 
		$Yodlee->generateFastLinkUrl();

		$data['fastLinkUrl'] = ''; 
		
		if( Session::get('fastLinkUrl') ) 
			$data['fastLinkUrl'] = Session::get('fastLinkUrl');
		else
			$data['error'] = 'Error on generating Fast Link Form.';

		$this->layout->content = View::make('blocks.verification.bank', $data );
	}

	/**
	 * Upload Bank Verification Files
	 * 
	 * @return
	 */
	protected function uploadBankVerification()
	{
		$this->layout = "";

		$data = Input::all();

		if( isset($data['files2']) ){
			$options['files'] 	   = $data['files2'];
			$options['param_name'] = 'files2';
		}else if( isset($data['files3']) ) {
			$options['files'] 	   = $data['files3'];
			$options['param_name'] = 'files3';		
		}else if( isset($data['files1']) ) {
			$options['files'] 	   = $data['files1'];
			$options['param_name'] = 'files1';		
		} else {
			$options = $data;	
		}

		$options['upload_dir'] =  dirname($_SERVER['SCRIPT_FILENAME']).'/files/bank/' . $this->loanAppNr . '/';
        $options['upload_url'] = url() . '/files/bank/'.$this->loanAppNr . '/';
 
		$uploadHandler = new UploadHandler($options);

	}

	/**
	 * Process Bank Account Verification 
	 *
	 * @return
	 */
	protected function processBankAccountVerification()
	{
				
		$data = Input::all();

		$loanAppVerId = 8;

		//update Completed Application Verification
		$this->updateCompletedAppVer($loanAppVerId);
		$this->createNLSComment( self::TASK_NAME_BANK, '', self::COMMENT_CATEGORY_BANK );	

		$AppBankInfo = new ApplicantBankVerification();

		//prepare data to be stored
		$AppBankInfo->User_Id                  = $this->userId;
		$AppBankInfo->Loan_App_Nr              = $this->loanAppNr;
		$AppBankInfo->Bank_Type_Cd       	   = 'Yodlee';
		$AppBankInfo->Member_Site_Id 		   = 0;
		$AppBankInfo->Statement_URL_Txt 	   = Gfauth::encryptFields(''); 
		$AppBankInfo->Created_By_User_Id       = $this->userId; 
		$AppBankInfo->Create_Dt                = date('Y-m-d');  
		$AppBankInfo->save(); 

		return Redirect::to('verification/steps');
	}

	/**
	 * Post Bank Account Verification 
	 * 
	 *  - Storing Bank Account Information
	 *  - Create NLS Task & Comments
	 *  - Update Loan Application Phase
	 *  - Upload Document to S3
	 *  - Create Applicant Verification 
	 *  - Create Task on ODS
	 *  - Create Task on Salesforce
	 *  
	 * @return
	 */
	protected function postBankVerification()
	{
	
		$data = Input::all();

		$Pay_Type_Desc 			= '';
		$Pay_Frequency_Desc		= '';
		$Employer_Name			= '';
		$Street_Addr_1_Txt		= '';
		$Contact_Person_Name	= '';

		if( count($data) > 0 ) {

			try {
				
				$AppBankInfo = new ApplicantBankVerification();

				//convert json data to array
				$images    = json_decode( $data['CheckImageData'], true );
				$docs      = array(); 
				$imagesURL = array();

				$dirName = dirname($_SERVER['SCRIPT_FILENAME']).'/files/bank/'; 

				if( count($images) > 0 ) {
					foreach ($images as $key => $image) {
						$docs[]      = $dirName . $this->loanAppNr. '/' . $image ;
						$imagesURL[] = $this->s3URL . 'bank/' .$this->loanAppNr. '/' . $image ;
					}
				}

				$imageUrlTxt = json_encode( $imagesURL );

				//Set Document Obj
				$this->documentObj = array(
					'Task_Doc_Upload_URL_Txt' => $imageUrlTxt, 
					'Reqd_File_Cnt' => count($images)
				);

 				
				$bankData = BankAccountDetail::where('User_Id','=',$this->userId)
											  ->where('Loan_App_Nr','=',$this->loanAppNr)
											  ->orderBy('Yodlee_Access_Dt', 'DESC')
											  ->first();


				//@todo - please refactor this one
				if( count($bankData) > 0 ){
					//prepare data to be stored
					$AppBankInfo->User_Id                  = $this->userId;
					$AppBankInfo->Loan_App_Nr              = $this->loanAppNr;
					$AppBankInfo->Bank_Type_Cd       	   = 'Yodlee';
					$AppBankInfo->Member_Site_Id 		   = $bankData->Member_Site_Id;
					$AppBankInfo->Statement_URL_Txt 	   = Gfauth::encryptFields($imageUrlTxt); 
					$AppBankInfo->Created_By_User_Id       = $this->userId; 
					$AppBankInfo->Create_Dt                = date('Y-m-d');  
					$AppBankInfo->save(); 
				} else {
					//prepare data to be stored
					$AppBankInfo->User_Id                  = $this->userId;
					$AppBankInfo->Loan_App_Nr              = $this->loanAppNr;
					$AppBankInfo->Bank_Type_Cd       	   = 'Bank Statements';
					$AppBankInfo->Member_Site_Id 		   = 0;
					$AppBankInfo->Statement_URL_Txt 	   = Gfauth::encryptFields($imageUrlTxt); 
					$AppBankInfo->Created_By_User_Id       = $this->userId; 
					$AppBankInfo->Create_Dt                = date('Y-m-d');  
					$AppBankInfo->save(); 
				}

				$loanAppVerId = 8;

				//update Completed Application Verification
				$this->updateCompletedAppVer( $loanAppVerId );

				$TaskComment = array(
					'commentStr' => 'Upload Statement	',
					'files'	=> $this->prepareCommentFiles($docs, self::COMMENT_CATEGORY_BANK)
				);
				

				//Set NLS Task Template Number
				$this->taskTemplateNr = 9;

				//Set ODSVerification ID
				$this->ODSVerificationId = $loanAppVerId;

				//Create Salesforce Verification Object
				$sfObject = $this->createSFVerificationObject('Bank_Account_Verification__c');
				
				$this->setSFVerificationId($sfObject);

				//set a timeout to get the salesforce verification Id
				sleep(2);

				//insert NLS TASK 
				$this->createTask( self::TASK_NAME_BANK, $TaskComment, 'APPROVED' );

				//Create an Appliction data
				$this->createApplVerification( $loanAppVerId );

				$loanAppPhaseNr = 7;
				$subPhaseCode   = 'BankVerificationSubmitted';

				//Set Loan Application Phase to  Final Offer - Email Verified
				setLoanAppPhase( $this->userId , $this->loanAppNr, $loanAppPhaseNr, $subPhaseCode  );	

				if( count($images) > 0 ) {
					foreach ($images as $key => $image) { 
						$s3path  = 'bank/' .$this->loanAppNr. '/' . $image ;

						if($this->debugMode == 'true')
							$s3path  = 'staging/bank/' .$this->loanAppNr. '/' . $image ;

						$imgPath = $dirName .$this->loanAppNr. '/' . $image ;
						$this->uploadToS3($s3path, $imgPath);
					}
				} 

				return Response::json( array(
						'result'    => 'success', 
						'data'      => $data, 
						'sfObject'  => $sfObject, 
						'message'   => showNotification(300) ,
						'debugMode' => Config::get('system.Ascend.DebugMode')
				), 200 );

			} catch(Exception $e) {
				return Response::json( array(
						'result'  => 'failed',
						'data'    => $data, 
						'message' => $e->getMessage()
					), 200 );
			}

		}else{
			return Response::json(showInvalidParameterError());
		}
	}

	/**
	 * Create SF Verification Object
	 * 
	 * @param  string $SalesforceVerObj
	 * @return array
	 */
	protected function createSFVerificationObject( $SalesforceVerObj = '' )
	{
		$sfObject = array();

		if( !empty($SalesforceVerObj) && !empty($this->userId) ) {
			$SalesforceMod    = new SalesforceMod(); 
			$sfObject = $SalesforceMod->createVerObject( $this->userId, $SalesforceVerObj );	
		}else{
			
		}

		return $sfObject;
	}

	/**
	 * Create Applicant Verification Record
	 * 
	 * @param  integer $loanAppVerId
	 * @return
	 */
	protected function createApplVerification( $loanAppVerId )
	{
		if( !empty( $loanAppVerId ) ) {

			return ApplicantVerification::createData(
				array(
					'User_Id'             => $this->userId, 
					'Loan_App_Nr'         => $this->loanAppNr,
					'ODS_Verification_Id' => $loanAppVerId, 
					'SF_Verification_Id'  => $this->sfVerificationId,
					'Verification_Status_Desc' => 'OPEN',
					'NLS_Task_Id'         => $this->NLSTaskId, 
					'Assigned_CSR_Id'     => $this->assignee->CSR_Id
				)
			);
		}
	}

	/**
	 * Prepare Comment Files for NLS Tasl
	 * 
	 * @param  string $docPath
	 * @param  string $category
	 * @param  string $filename
	 * @return string
	 */
	public function prepareCommentFiles($docPath, $category = "", $filename = '')
	{
		$files = '';

	    //check if the docPath is an array
        if( is_array( $docPath ) && count($docPath) > 0 ) {

            $cleanCategory = preg_replace('/ /', '-', $category); 
            
            foreach ($docPath as $key => $image ) {
                $extension   = pathinfo($image, PATHINFO_EXTENSION);
                $hexImage[]  = array(
                    'DocImage' => $this->convertImageToHex( $image ), 
                    'Filename' => $cleanCategory . $key . '.' . $extension
                );
            }

        } else{

            if( $filename == '' ) 
                $cleanCategory = preg_replace('/ /', '-', $category);   
            else 
                $cleanCategory = $filename;

            $extension = pathinfo($docPath, PATHINFO_EXTENSION); 
            
            $hexImage[0] = array(
                'DocImage' => $this->convertImageToHex( $docPath ), 
                'Filename' => $cleanCategory . '.' . $extension
            );
        }

        //Prepare Document Binary
        if( count($hexImage) > 0 ) {
            foreach ($hexImage as $key => $value) {
               $files .= '<DOCBINARY Filename="'.$value['Filename'].'" DocImage="'.$value['DocImage'].'"/>'; 
            }
        }

        return $files;

	}

	/**
	 * Upload Image to S3
	 * 
	 * @param  string $imgObjectPath
	 * @return
	 */
	protected function uploadToS3( $key, $imgObjectPath = '' )
	{
		$s3 = AWS::get('s3');

		try{

			if( File::exists( $imgObjectPath) ) {

				$s3 =  $s3->putObject(array(
				    'Bucket'     => 'files.ascendloan.com',
				    'Key'        => $key,
				    'SourceFile' => $imgObjectPath
				));

				//Delete System Copy
				if( !File::isWritable($imgObjectPath) )
					File::makeDirectory($imgObjectPath, 0777, true);

				File::delete($imgObjectPath);

				return $s3;
			}

		}catch(Exception $e){

			writeLogEvent('S3 - Upload Image Failure',
				array(
					'DocumentPath' => $imgObjectPath
				)
			); 
		}
	}

}