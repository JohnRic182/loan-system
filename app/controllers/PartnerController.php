<?php

/**
 * Developers Notes
 *
 * Updated:
 *    - creditPull validation
 *    - working on exclusion
 *    - update show loan rate
 *    - production qa user
 * 
 */
class PartnerController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Partner Controller
	|--------------------------------------------------------------------------
	|
	| All Partners API methods 
	|
	*/

	/**
	 * Default Loan Amount
	 */
	const DEFAULT_LOAN_AMT     = 5000;
	const DEFAULT_MIN_LOAN_AMT = 2600;
	const DEFAULT_MAX_LOAN_AMT = 10000;

	/**
	 * Default User Role Id
	 *    - Customer
	 */
	const DEFAULT_USER_ROLE_ID = 2;

	/**
	 * Default Credit description
	 */
	const DEFAULT_CREDIT_TXT = null;

	/**
	 * Default Product Id 
	 *    - Ascend Loan 
	 */
	const DEFAULT_PRODUCT_ID   = 1;

	/**
	 * Default Product description
	 */
	const DEFAULT_PRODUCT_TXT  = 'Ascend Loan';

	/**
	 * The layout that should be used for responses
	 * 
	 * @var string 
	 */
	public $layout  =  'layouts.partner';

	/**
	 * List of Errors
	 * @var array
	 */
	protected $_error      = array(); 

	/**
	 * Request Status Code
	 * @var integer
	 */
	protected $_statusCode = 200;

	/**
	 * Status Flag
	 * @var boolean
	 */
	protected $_status     = true;

	/**
	 * Set User Id
	 * 
	 * @var
	 */
	protected $_userId;

	/**
	 * Set Loan App Nr
	 * @var
	 */
	protected $_loanAppNr;

	/**
	 * Response 
	 * 
	 * @var
	 */
	protected $_response;

	/**
	 * ARS Score
	 * 
	 * @var integer
	 */
	protected $_ARSScore;

	/**
	 * LendingTree Api User
	 * 
	 * @var string
	 */
	protected $lt_user;

	/**
	 * LendingTree api password
	 * 
	 * @var
	 */
	protected $lt_pass;

	/**
	 * LendingTree API Id
	 * 
	 * @var string
	 */
	protected $lt_lenderId;

	/**
	 * Partner Name
	 * 
	 * @var string
	 */
	protected $partnerName = '';

	/**
	 * Email Address
	 * 
	 * @var
	 */
	private $email;

	/**
	 * Email Subject
	 * 
	 * @var string
	 */
	protected $subject;



	const ERR_MISSING_CODE    = 101;
	const ERR_VALIDATION_CODE = 102;
	const ERR_FRAUD_CHECK     = 103;
	const ERR_SP_EXCLUSION    = 104;
	const ERR_SP_DECISIONING  = 105;
	const ERR_TU_CREDIT_PULL  = 106;
	const ERR_CONTACT_CREATE  = 107;
	const ERR_LOAN_OBJ        = 108;
	const ERR_PERMISSION      = 109;
	const ERR_ACCOUNT_EXIST   = 110;
	const NO_OFFER_REQUEST    = 201;
	const OFFER_REQUEST       = 200;

	const DEFAULT_RENT_AMT    = 800;

	/**
	 * Class Construct
	 */
	public function __construct()
	{
		//parent::__construct();
    	$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);

    	//lending Tree Config
    	$this->lt_user 		= Config::get('system.LendingTree.UserName');
		$this->lt_pass 		= Config::get('system.LendingTree.Password');
		$this->lt_lenderId 	= Config::get('system.LendingTree.LenderId');

  	}
 	
 	/**
 	 * Set User Id
 	 * 
 	 * @param integer $userId
 	 */
	protected function setUserId( $userId )
	{
		$this->_userId = $userId;
	}

	/**
	 * Get User Id 
	 * 
	 * @return integer
	 */
	public function getUserId()
	{
		return $this->_userId;
	}

	/**
	 * Get Loan Applicaiton Number
	 * 
	 * @return
	 */
	public function getLoanAppNr()
	{
		return $this->_loanAppNr;
	}

	/**
	 * Set Loan Application Number
	 * 
	 * @param integer $loanAppNr
	 */
	public function setLoanAppNr( $loanAppNr )
	{
		$this->_loanAppNr = $loanAppNr;
	}

	/**
	 * Get Response
	 * 
	 * @return
	 */
	protected function getResponse()
	{
		return $this->_response;
	}

	/**
	 * Set JSON Response
	 * 
	 * @param array $msg
	 */
	protected function setJSONResponse( $msg = array() )
	{
		$this->_response = json_encode($msg);
	}

	/**
	 * Set Array Reponse
	 * 
	 * @param array $msg
	 */
	protected function setArrayResponse( $msg = array() )
	{
		$this->_response = $msg;
	}

	
	protected function setXMLResponse()
	{

	}

	/**
	 * Get error Message
	 * 
	 * @return array
	 */
	protected function getErrorMsg()
	{
		return $this->_error;
	}

	/**
	 * Set Error Message
	 * 
	 * @param array $msg
	 */
	protected function setError( $code, $desc = "" )
	{
		$this->_error[] = array(
			'code' => $code,  
			'msg'  => $desc
		);
	}

	/**
	 * Index Action
	 * - Show Landing Pages for Lending Tree, Mint, Google
	 *
	 * @version v1.0.4.21
	 * @return
	 */
	protected function showIndex($partnerType = '', $landingPage = '')
	{  

		//get testMode for iovation
		$data['ioTestMode'] = Config::get('system.iovation.TestMode');

		//instantiate PartnersUserAcct
		$partnerUserAcct = new PartnerUserAcct(); 

		//Set the relative URL for the partner hompage
		setHompageRelativeUrl();

		$prodFullNameArray   = array();

		$data['isProductionQA'] = FALSE;
		$isProductionBypass = TRUE;

		//@Override Production QA
		if( Input::get('case') == 'test' ){
			$data['isProductionQA'] = TRUE;
		}

		//@override bypass
		if( Input::get('bypass') == 0 ){
			$isProductionBypass = FALSE;
		}

		//Set Bypass session
		Session::put("loan.isProductionBypass", $isProductionBypass);
		$data['default_slider_val'] = 5500;

	  	if( isset( $_GET['UID']) ){
			//set slider value			
			$partnerUserInfo = $partnerUserAcct->getData( 
					array( '*', 
						'Email_Id', 
						'Street_Addr_1_Txt', 
						'City_Name', 
						'Zip_Cd',
						'Social_Security_Nr'
					), 
					array(
						'Partner_User_Id'  => Input::get('UID')
					), true 
				);
		
			$data['default_slider_val'] = (count($partnerUserInfo) > 0 )? $partnerUserInfo->Offered_Loan_Amt : 5000; 
		}		

		$partners = Session::get('partner');

		$data['partnerFlag']	   = 0;		
		$this->layout->partnerFlag = 0;

		// Check if Partner exist
		if( $partnerType != '' ){
			// get Partner data
			$partner = Partner::where('Logo_Image_File_Name','=',$partnerType)
								->first();
			if(count($partner) > 0){
				$data['partnerImage'] = $partner->Logo_Image_URL_Txt;		
				//Check and store CID and PID int the session for later use in the regitration
				trackPartnerIds($partner->Partner_Id);
			}else{
				$data['partnerImage'] = '/img/lending-tree.png';		
			}			

			$this->layout->partnerFlag = 1;
			$data['partnerFlag'] = 1;	
		}

		$data['partnerType']  = $partnerType;

		$partnerTypeStr = strtolower($partnerType);
		 		
		if( $partnerTypeStr == 'lendingtree'){
			
			$Monthly_Payment_Amt         = 0;
			$data['Loan_Amt']            = 0;
			$data['Terms']               = '36';
			$data['APR_Val']             = 0; 
			$data['Monthly_Payment_Amt'] = 0;
			$data['landingpageDefault']  = false;
			$data['Loan_Purpose_Id']     = 0;
			$data['Email_Id']            = '';
			$data['streetAddress1']      = '';
			$data['city']                = '';
			$data['zip']                 = '';

			if( isset( $_GET['UID']) ){
				$data['Loan_Amt'] 			= number_format( $partnerUserInfo->Offered_Loan_Amt, 0 );
				$data['Terms'] 				= '36';
				$data['APR_Val'] 			= abs( $partnerUserInfo->Offered_APR_Val );
				$Monthly_Payment_Amt 		= pmt($data['APR_Val'], $data['Terms'], $partnerUserInfo->Offered_Loan_Amt);
			    $data['Monthly_Payment_Amt'] = number_format( $Monthly_Payment_Amt , 2 ) ;	    
			    $data['landingpageDefault'] = ($landingPage == 'lp1')? true : false;
			    $data['Loan_Purpose_Id'] 	= $partnerUserInfo->Loan_Purpose_Id;
			    $data['Email_Id']			= $partnerUserInfo->Email_Id; 
			    $data['streetAddress1']		= $partnerUserInfo->Street_Addr_1_Txt;
			    $data['city']				= $partnerUserInfo->City_Name;
			    $data['zip']				= $partnerUserInfo->Zip_Cd;

			    //save email to session
			    Session::put('email', $data['Email_Id']); 
			}			
		}		
		
	   	$data['User_Id'] 			= '';
	    $data['Loan_App_Nr'] 		= '';
	    $data['Campaign_Id'] 		= $partners['CID'];
	    $data['Partner_Id'] 		= $partners['PID'];
	    $data['Lead_Id'] 			= $partners['UID']; 
	    $data['Partner_Type_Id'] 	= $partners['PID'];
	    

	    //Query String for State Code
		if( !empty( Input::get('state') ) ){
			$stateCode = Input::get('state');
		} else{
			$stateCode = getStateCodeViaIp();
		}

		$data['sliderMinLoanAmt'] = self::DEFAULT_MIN_LOAN_AMT;
		$data['sliderMaxLoanAmt'] = self::DEFAULT_MAX_LOAN_AMT;
 	
 		//Select Loan Amount Range Depending on State
		if( !empty($stateCode) ) {
			$rateMap = LoanProdRateMap::getLoanAmtRange($stateCode);
			if( count($rateMap) > 0 ) {
				$data['sliderMinLoanAmt'] = $rateMap->Absolute_Min_Loan_Amt;
				$data['sliderMaxLoanAmt'] = $rateMap->Absolute_Max_Loan_Amt;
			}
		}	
		 
		$stateCode = getStateCodeViaIp();

		//get state config
		$data['stateConfig'] = stateConfigurations($stateCode);
		$data['housingSit']  = getHousingSituation();
		$data['delinquent']  = getDelinquencyPeriod();

		if( isset( $data['Loan_Purpose_Id'] ) ){
			$data['Loan_Purpose_Txt'] = LoanPurpose::getLoanPurposeName( $data['Loan_Purpose_Id'] );
		} else{
			$data['Loan_Purpose_Txt'] = "";
			$data['Loan_Purpose_Id']  = "";
		}
		
		//Get loan data
		$loanData['loanProductId']  = self::DEFAULT_PRODUCT_ID;  
		$loanData['creditquality'] 	= self::DEFAULT_CREDIT_TXT; 
		$loanData['loanProductTxt'] = self::DEFAULT_PRODUCT_TXT;
		$loanData['userRoleId']  	= self::DEFAULT_USER_ROLE_ID;
		$loanData['loanPurposeTxt'] = $data['Loan_Purpose_Txt'];
		$loanData['loanAmt'] 		= $data['default_slider_val']; 
		$loanData['loanPurposeId'] 	= $data['Loan_Purpose_Id'];

		setLoanSessionData($loanData);

		$data['creditquality'] 	= self::DEFAULT_CREDIT_TXT;
		$data['User_Role_Id']	= self::DEFAULT_USER_ROLE_ID;

	    $this->layout->homepage = true;
	    
	    if( $partnerTypeStr == 'lendingtree' ){
	    	if ($landingPage == 'rt1'){
	    		$this->captureRTparams(Input::all());
	    		$this->layout->content  = View::make('blocks.partner.lendingTree', $data );	    		
	    	}else{

	    		//Lending Tree Landing Page Versions
	    		$ltVarations = array('lp5', 'lp6');

	    		if(in_array($landingPage, $ltVarations)){
	    			$this->layout->content  = View::make('blocks.partner.lending-tree.'.$landingPage, $data );
	    		}else{
		    		$this->layout->content  = View::make('blocks.partner.generic', $data );	
	    		}
	    	}
	    }else{
		    if ( $landingPage == 'lp1' ){
	    		$this->layout->content  = View::make('blocks.partner.index', $data );
		    }else {
	    		$this->layout->content  = View::make('blocks.partner.index-v2', $data );	
		    }
	    } 
	}

	/**
	 * Validate Parnter User
	 * 
	 * @param  array  $post
	 * @return
	 */
	public function validateParnterUser( $post = array() )
	{
		$isAllowed = FALSE;

		if(!isset($post['Application'])){
			$this->setError(self::ERR_MISSING_CODE, 
				$this->setNotFoundFlds('Application'));
		} else{

			$data = $post['Application'];
 
			if( isset($data['Partner']['Username'])
				&& isset($data['Partner']['APIKey']) ){

				$Partner = new Partner();

				$isExist = $Partner->isPartnerExist(
					$data['Partner']['Username'], 
					$data['Partner']['APIKey'],
					$data['Partner']['PID']
				);

				if( $isExist == 1 ){
					$isAllowed = TRUE;
				} else {
					$this->setError(
						self::ERR_PERMISSION, 
						'Permission Denied!, Please make sure you entered correct PID, Username and API Key'
					);
				}
			} else{
				$this->setError(
					self::ERR_MISSING_CODE, 
					'Missing Partner.Username or Partner.APIKey Fields'
				);
			}
		}

		return $isAllowed;
	}

	/**
	 * Prepare Credit Check 
	 * 
	 * @return
	 */
	protected function prepareCreditCheck()
	{

		$input = Input::all();

		$this->layout = null;

		//get both IDs
		$ascendUserId 	= getUserSessionID();
		$partnerUserId 	= Session::get('partner.UID');

		$partnerUserAcct 	= new PartnerUserAcct();
		$financialData 		= ApplicantFinancialInfo::where('User_Id', '=', $ascendUserId )->get();		

		$fields = array(
			'*',
			'Email_Id',
			'Street_Addr_1_Txt',
			'Street_Addr_2_Txt',
			'City_Name',
			'Zip_Cd',
			'Social_Security_Nr',
			'First_Name',
			'Last_Name',
			'Birth_Dt',
			'Phone_Nr'
		);

		$partnerUserInfo = $partnerUserAcct->getData( $fields, ['Partner_User_Id'  => $partnerUserId ], true )->toArray();		

		$DOB = explode('-', $partnerUserInfo['Birth_Dt']);

		$data = array(
			'_token'		=> (isset($input['_token']))? $input['_token'] : '',
		    'zestimate' 	=> '',
		    'ioBB' 			=> (isset($input['ioBB']))? $input['ioBB'] : '',
		    'fpBB' 			=> (isset($input['fpBB']))? $input['fpBB'] : '',
		    'uid' 			=> $ascendUserId,
		    'firstName' 	=> $partnerUserInfo['First_Name'],
		    'lastName' 		=> $partnerUserInfo['Last_Name'],
		    'phoneNumber' 	=> $partnerUserInfo['Phone_Nr'],
		    'mobilePhoneNr' => '',
		    'address' 		=> $partnerUserInfo['Street_Addr_1_Txt'],
		    'city'  		=> $partnerUserInfo['City_Name'],
		    'state' 		=> $partnerUserInfo['State_Cd'],
		    'zip' 			=> $partnerUserInfo['Zip_Cd'],
		    'ssn' 			=> (App::environment() == 'local')? '666587433' : $partnerUserInfo['Social_Security_Nr'],
		    'month' 		=> $DOB[1],
		    'day' 			=> $DOB[2],
		    'year' 			=> $DOB[0],
		    'age' 			=> getAgeFromDOB( $partnerUserInfo['Birth_Dt'] ),
		    'creditQuality' => '',
		    'annualGrossIncome' => ( isset( $input['annualGrossIncome']) )? $input['annualGrossIncome'] : $partnerUserInfo['Annual_Gross_Income_Amt'],
		    'term' 				=> '0', // default to zero for annual gross income value
		    'employeeStatus' 	=> $partnerUserInfo['Employment_Status_Id'],
		    'housingSit' 		=> ( isset($input['housingSit']) )? $input['housingSit'] : $partnerUserInfo['Housing_Sit_Id']
		);
		
		if( isset($input['monthlyMortage']) )
			$data['monthlyMortage'] = $input['monthlyMortage'];

		if( isset($input['rentAmount']) )
			$data['rentAmount'] = $input['rentAmount'];

		if( isset($input['contactName']) )
			$data['contactName'] = $input['contactName'];

		if( isset($input['contactPhoneNr']) )
			$data['contactPhoneNr'] = $input['contactPhoneNr'];

		if( isset($input['delinquencyPeriod']) )
			$data['delinquencyPeriod'] = $input['delinquencyPeriod'];

		if( sizeof( $financialData ) > 0 )
			$data['zestimate'] = $financialData[0]->Zillow_Rent_Est_Amt;

		return Response::json($data);

	}

	/**
	 * Apply Loan 
	 * 
	 * @return
	 */
	protected function apply()
	{ 
 		// $this->layout = NULL;
 		$isSuccess = FALSE;
 
		$data = Input::all();
		$post = array();

		//Validate User
		$isAllowed = $this->validateParnterUser($data);

		if( $isAllowed == FALSE ) {

			$result = array( 
				'error'  => $this->_error,
				'result' => 'FAILED'
			);

			//Set JSON Response
			$this->setJSONResponse( $result ); 

			return $this->getResponse(); 
		}

		//validate Post Arrays
		$this->validatePostArrays($data);
		
		//Check if there's an error in validation 
		if( empty( $this->_error ) ) { 

			$post = $data['Application'];

			//Validate for Existing Lona Application
			$user = User::getDataByEmail(array('User_Id'), $post['Customer']['Personal']['Email']);
 			
 			$isAllowed = TRUE; 

 			if( count($user) > 0 ) { 
 				//Check if it has Loan Application 
 				$loanAppCnt = LoanDetail::isUserHaveLoanApp($user->User_Id);

 				if( $loanAppCnt > 0 ) {
 					$this->setError(self::ERR_ACCOUNT_EXIST, $post['Customer']['Personal']['Email'] . ' already exist');	
 					$isAllowed = FALSE; 
 				} 
 			}

 			//Continue if Applicant is Allowed to Apply
 			if( $isAllowed ==  TRUE ) {
	 			
				$username = generateUserName($post['Customer']['Personal']['Email']);
				$password = generatePassword(8);
				$token    = md5($post['Customer']['Personal']['Email']);

				//Compute annual gross income
				$annualGrossIncome = $post['Customer']['LoanApp']['MonthlyIncome'] * 12;

				$post['Customer']['LoanApp']['AnnualGrossIncome'] = $annualGrossIncome;

				//set partner username, password and token
				$post['Partner']['NLSUsername'] = $username;
				$post['Partner']['Password']    = $password;
				$post['Partner']['Token']       = $token;

				//Contact and Loan Object Creation 
				$isContactCreated = $this->createContact($post);

				if( $isContactCreated == TRUE ) { 

					$userId = $this->getUserId();
					$isLoanCreated = $this->createLoan( $userId, $post );
 
					if( $isLoanCreated == TRUE ) {
						//Get loan app number
						$loanAppNr = $this->getLoanAppNr();
						//echo 'Loan APP NUMBER' . $loanAppNr;
						//Calling TU credit Pull
						if( !empty($loanAppNr) && !empty($userId) ){

							$isCreditProfileExist = 0;
							
							//delay calling of TU Attribute and Credit Pull 
							for ( $ctr=0; $ctr < 4; $ctr++ ) { 
								// echo 'Calling TU Pull'; 
								$tu = $this->callTUCreditPull( $userId, $loanAppNr );
								$isCreditProfileExist = NlsCreditProfile::isCreditProfileExist($userId);

								if( $isCreditProfileExist == 1 ){
									break;
								}else{
									//Sleep for 3 seconds
									sleep(3);
								}									
							}

							//Check if credit profile exist
						 	if( $isCreditProfileExist == 1 ) {
								//Call SP Exclusion 
								$exclusion = $this->callSPExclusion( $userId, $loanAppNr ); 
								$personal  = $post['Customer']['Personal'];

								if( $exclusion == FALSE ){

									$this->setError(self::ERR_SP_EXCLUSION, 'EXCLUSION FAILED'); 
									$this->rejectApplication($loanAppNr, $userId, $personal);

								}else{
																										
									$ARSScore  	= $this->getARSScore(); //Call SP Loan Decisioning

									// save to partner user account if SelfReportedCredit is present
									if( isset($post['Customer']['LoanApp']['SelfReportedCredit']) ){
										$selfReportedFico = $post['Customer']['LoanApp']['SelfReportedCredit'];
										// update the fico value on the partner user account
									 	$updateFICOData = array('Self_Rpt_Credit_Score_Val' => $selfReportedFico);
									 	PartnerUserAcct::updateData($userId, $updateFICOData);									
									}

									$productId = Config::get('nls.NLS_DEFAULT_LOAN_ID');

									$isBankAcctLinked = 'N';
									$isFinalCall      = 'Y';

									//Call Loan Decisioning SP
									$decision  = $this->callSPLoanDecision( 
													$userId, 
													$ARSScore, 
													$productId, 
													$isBankAcctLinked, 
													$personal['State'], 
													$isFinalCall
												);

									if( $decision == FALSE ){ 
										$this->setError(self::ERR_SP_DECISIONING, 'LOAN DECISIONING FAILED');
										$this->rejectApplication($loanAppNr, $userId, $personal);
									}
								}
							} else {
								$this->setError(self::ERR_TU_CREDIT_PULL, 'CREDIT PULL FAILED');
							}
						}	
					}else{
						$this->setError(self::ERR_LOAN_OBJ, 'NLS LOAN OBJECT CREATION FAILED');
					} 	 
				} else {
					$this->setError(self::ERR_CONTACT_CREATE, 'NLS CONTACT OBJECT CREATION FAILED');
				} 
			}
			
		}

		$responsePost = array();

		$result = array();

		//Recheck for the error
		if( !empty( $this->_error ) ){

			//Send No Offer Response 
			$response = $this->partnersAPINotice('fail', $data );

			$responsePost = $response; 

			$result = array(
				'error' => $this->_error,
				'partnerResponse' => $response
			);

			//Set JSON Response
			$this->setJSONResponse( $result ); 
		}else{

			//Get loanAppNr and userId
			$data['userId']    = $this->getUserId();
			$data['loanAppNr'] = $this->getLoanAppNr();

			//Send No Offer Response 
			$response = $this->partnersAPINotice('success', $data );

			$responsePost = $response; 

			//Set JSON Response
			$this->setJSONResponse( $response ); 

			$isSuccess = TRUE;
		}

		$request = array();
 
		if( $isSuccess == TRUE && isset( $data['Application'] ) ){  
			$request['RequestResponseTxt'] = json_encode($data);
			$request['code'] = self::OFFER_REQUEST;
			$request['msg']  = $this->getResponse();

		} else{
			$request['RequestResponseTxt'] = json_encode($data);
			$request['code'] = self::NO_OFFER_REQUEST;
			$request['msg']  = json_encode($result);
		}

		//track Request 
		$this->trackApiData($data, $request, 'request'); 

		if( isset($post['Partner']['ResponseEndPoint']) ){ 
			$endPoint = $post['Partner']['ResponseEndPoint'];
			$this->postParnterResponse($data, $responsePost, $endPoint);
		}

		return $this->getResponse();
	}
	/**
	 * Reject Loan Application 
	 * 
	 * @param  integer $loanAppNr
	 * @param  array  $personal
	 * @return
	 */
	public function rejectApplication( $loanAppNr, $userId, $personal = array())
	{ 
		if( !empty($loanAppNr) && count($personal) > 0 ) {

			$NLS = new NLS();

			//Reject the Application
			updateLoanStatus($loanAppNr, 'APPLICATION_REJECTED');

			$loanTemplate  = LoanTemplate::find(Config::get('nls.NLS_LOAN_TEMPLATE_ID'));
			$loanGroup     = LoanGroup::find(Config::get('nls.NLS_LOAN_GROUP_ID'));
			$loanPortfolio = LoanPortfolio::find(Config::get('nls.NLS_LOAN_PORTFOLIO_ID'));
			 
			$nlsParam = array(
				'loanStatusCode'   => 'APPLICATION_REJECTED', 
				'nlsLoanTemplate'  => $loanTemplate->Loan_Template_Name,
				'nlsLoanGroup'     => $loanGroup->Loan_Grp_Name, 
				'nlsLoanPortfolio' => $loanPortfolio->Loan_Portfolio_Name, 
				'lastName'         => $personal['LastName'] ,
				'firstName'        => $personal['FirstName'], 
				'contactId'        => $userId, 
				'loanId'           => $loanAppNr
			);
			
			//Reject the Application in NLS
			$NLS->nlsUpdateLoanStatus($nlsParam);
		} 
	}


	/**
	 * Set Not Found Fields 
	 * 
	 * @param string $fieldName
	 * @return string
	 */
	protected function setNotFoundFlds( $fieldName = '')
	{
		return $fieldName  . ' field is required';
	}

	/**
	 * Validate POST fields
	 * 
	 * @param  array  $postArr
	 * @return
	 */
	protected function validatePostArrays( $post = array() )
	{	

		if(!isset($post['Application'])){
			$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds('Application'));
		} else{
			$postArr = $post['Application'];
		
			//Validate Parnter Fields
			if( !isset($postArr['Partner']) ) {
				$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds('Partner'));
			}else{
				
				$rules  = array();
				$fields = array();

				$partners = $postArr['Partner']; 
				$partnersRqrdFields = $this->getParnterReqFields();
	 
				//Validate Parnter Require Fields 
				foreach( $partnersRqrdFields  as $key => $val ) {
					if( !isset($partners[$key] ) ){
						$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds($key));
					}else{
						$rules[$key]  = $val['rule'];
						$fields[$key] = $partners[$key];
					}
				}

				$this->validateFieldsByRules($fields, $rules);
			}

			//Validate Customer Fields
			if( !isset($postArr['Customer']) ){
				$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds('Customer'));
			}else{

				$rules  = array();
				$fields = array();

				$customer = $postArr['Customer']; 
				$customerReqFields = $this->getCustomerReqFields();

				//validate missing fields CUSTOMER => LOANAPP
				if( !isset($customer['LoanApp']) ){
					$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds('LoanApp'));
				}else{
					//Validate Parnter Require Fields 
					foreach( $customerReqFields['LoanApp'] as $key => $val ) {
						if( !isset($customer['LoanApp'][$key] ) ){
							$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds($key));
						}else{
							$rules[$key]  = $val['rule'];
							$fields[$key] = $customer['LoanApp'][$key];
						}
					}
				}

				//validate missing fields CUSTOMER => PERSONAL
				if( !isset($customer['Personal']) ){
					$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds('Personal'));
				}else{
					//Validate Parnter Require Fields 
					foreach( $customerReqFields['Personal'] as $key => $val ) {
						if( !isset($customer['Personal'][$key] ) ){
							$this->setError(self::ERR_MISSING_CODE, $this->setNotFoundFlds($key));
						}else{
							$rules[$key]  = $val['rule'];
							$fields[$key] = $customer['Personal'][$key];
						}
					}
				}
				
				$this->validateFieldsByRules($fields, $rules);
			}
		} 
	}

	/**
	 * Call Idology Fraud Check
	 * 
	 * @return
	 */
	protected function callFraudCheck( $data = array() )
	{
		$result = 'failed';
		
		if( count($data) > 0 ) {

			$Idology = new Idology();

			//format data
			$idologyData = array(
				'firstName' => $data['FirstName'], 
				'lastName'  => $data['LastName'], 
				'address'   => $data['StreetAddress'], 
				'city'      => $data['City'], 
				'state'     => $data['State'], 
				'zip'       => $data['Zip'], 
				'ssn'       => $data['SSN']
			); 

			$profile = 'ascendInit';

			$idologyResult = $Idology->sendRequest($idologyData, $profile );

			// _pre($idologyResult);

			if( count($idologyResult) > 0 ){
				if( isset( $idologyResult['result']) 
					&& $idologyResult['result'] == 'success' ) 
				{
					$result = 'success';
				} 
			}
		}

		return $result;
	}
  
	/**
	 * Call TU credit Pull Data
	 * 
	 * @return
	 */
	protected function callTUCreditPull( $userId, $loanAppNr )
	{

		$NLS = new NLS();

		$isCreditProfileExist = NlsCreditProfile::isCreditProfileExist($userId);
		
		if( $isCreditProfileExist == 0 ){
			// echo 'Saving NLS Data';
			$NLS->saveNLS( $userId, $loanAppNr );
		}
 
		//TU Credit
		$isTUCreditExist = TUCredit::isTUCreditExist($loanAppNr, $userId);

		if( $isTUCreditExist == 0 ){
			// echo 'Pulling TU Credit';
			$NLS->parseXML( $userId, $loanAppNr, 'initial' );  	
		}
	}

	/**
	 * Set ARS Score 
	 * 
	 * @param integer $score
	 */
	protected function setARSSCore( $score = 0 )
	{
		$this->_ARSScore = $score;
	}

	/**
	 * Get ARS Score
	 * 
	 * @return integer
	 */
	protected function getARSScore()
	{
		return $this->_ARSScore;
	}
	/**
	 * Call SP Exclusion
	 * 
	 * @return
	 */
	protected function callSPExclusion( $userId, $loanAppNr, $type = 'initial' )
	{

		$isPassed = TRUE;
		$score    = 0;

		$adverseActions =  array();

		//Get TU Credit Data
		$tuData = TUCredit::where('User_Id', '=', $userId)
							->where('Credit_Pull_Type_Desc', '=', trim($type))
							->first();
		///////////////////////////////////////
		//      SCORING ALGORITHM            //
		///////////////////////////////////////
		if( sizeOf($tuData) > 0 ){
			$scoreResults   = ApplicantRiskInfo::getCreditScore( $userId, $loanAppNr, $type );
			$score          = $scoreResults['score']; 
			//set ARS Score
			$this->setARSSCore($score);
		} else {
			$isPassed = FALSE;
		}

		$exclusions    = ExclusionVersion::getCreditExclusion($userId);
		$approvedLoans = LoanDetail::getApprovedLoans($userId, $loanAppNr);
		
        //Existing loan not paid off
        if( count( $approvedLoans ) > 0)
        	array_push($exclusions, self::A16);

		$data['exclusions'] = $exclusions;

		// _pre($data);

		if( count($data['exclusions']) > 0 ){
			
			foreach ($data['exclusions'] as $key => $value) {
				$this->setError( self::ERR_SP_EXCLUSION, $value );
			} 

			$isPassed = FALSE;
		}
 	
 		return $isPassed;
	}

	/**
	 * Call SP Loan Decisioning 
	 * 
	 * @return
	 */
	protected function callSPLoanDecision( $userId, $score, $loanProductId, $isBankAcctLinked = 'N', $stateCode = 'CA', $isFinalCall = 'Y')
	{	
		
		$isPassed = FALSE;
 
		//We will not be saving the data into Loan_Detail table
		//Get Loan Decisioning Results using ODS.dbo.usp_Loan_Decision
 		$isLoanApproved = DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?,?,?", 
								array(
									$stateCode
									,$userId
									,$score
									,$loanProductId
									,$isBankAcctLinked
									,$isFinalCall
								)
							);
 
 		//Check Whether the Applicant is Approved or Not
		if( count($isLoanApproved) > 0 && isset($isLoanApproved[0]->Loan_Qual_Flag ) ) {
			if( $isLoanApproved[0]->Loan_Qual_Flag == 1) {
 				$isPassed = TRUE;
 			}
		}

		return $isPassed;
	}

	/**
	 * Create Contact Object in ODS and NLS
	 * 
	 * @param  array  $data
	 * @return boolean
	 */
	protected function createContact( $data = array())
	{
		$isPassed = FALSE;

		if( count($data) > 0 ){

			$User = new User(); 
			$NLS  = new NLS();	

			$personal = $data['Customer']['Personal'];
			$partner  = $data['Partner'];
			$loanApp  = $data['Customer']['LoanApp'];

			//Validate email address
			$isEmailExist = $User->isUniqueEmail( $personal['Email'] );
 
			// pre($isEmailExist);

			if( $isEmailExist == 1 ){

				$user = $User::whereRaw('CONVERT(VARCHAR(100), DECRYPTBYKEY(Email_Id)) = ?', array($personal['Email']))
							->select('User_Id')
							->first();
				 
				$this->setUserId( $user->User_Id );

				$isPassed = TRUE;

			}else{

				//Format user account data 
				$formattedData = array(
					'username'     => $partner['NLSUsername'],
					'password'     => $partner['Password'],
					'_token'       => $partner['Token'],
					'email'        => $personal['Email'], 
					'IP_Addr_Id'   => $personal['Ipaddress'], 
					'User_Role_Id' => 2
				);

				//Save User data
				$result = $User->saveUser($formattedData);

				//Format NLS Fields
				$NLSFields = array(
					'contactId'   => $result->User_Id,
					'username'    => $partner['NLSUsername'],
					'password'    => $partner['Password'],
					'email'       => $personal['Email'],
					'hint1'       => "",
					'hint1Answer' => "",
					'hint2'       => "",
					'hint2Answer' => "" 
				);
				
				//Create NLS Contact
				$nlsContact = $NLS->NLSCreateContact($NLSFields);

				// _pre($nlsContact);

				$this->setUserId( $result->User_Id );

				$isPassed = TRUE;
			}
		}

		return $isPassed;
	}

	/**
	 * Save Partner User Account Data
	 * 
	 * @param  array  $data
	 * @return
	 */
	protected function savePartnerUserAcctData( $data = array() )
	{
 		
 		if( count($data) > 0 ) {

	 		$PartnerUserAcct = new PartnerUserAcct();

	 		$selfReportedCredit = DB::raw("DEFAULT");
	 		//Filter Self Reported 
	 		if (isset($data['SelfReportedCredit']) )
	 			$selfReportedCredit = $data['SelfReportedCredit'];

			$applicantData = array(
				'PID'                => $data['partnerID'],
				'CID'                => $data['campaignID'],
				'trackingNr'         => $data['userId'],
				'firstName'          => $data['firstName'] ,
				'lastName'           => $data['lastName'],
				'city'               => $data['city'],
				'state'              => $data['state'],
				'zip'                => $data['zip'],
				'phoneNr'            => $data['homePhoneNumber'],
				'email'              => $data['email'],
				'score'              => '0',
				'income'             => $data['annualGrossIncome'],
				'APR'                => '0',
				'loanAmt'            => $data['loanAmount'],
				'dob'                => $data['birthDate'],
				'ssn'                => $data['ssn'],
				'address1'           => $data['streetAddress1'],
				'address2'           => DB::raw("DEFAULT"),
				'zip4'               => $data['zip4'],
				'housingSit'         => $data['housingSit'],
				'empStatus'          => $data['employeeStatus'],
				'montRent'           => $data['rentAmount'],
				'loanPurpose'        => $data['loanPurposeId'], 
				'userId'             => $data['userId'],
				'selfReportedCredit' => $selfReportedCredit,
				'term'               => $data['term']
			); 

			$PartnerUserAcct->saveApplicantData($applicantData);
		}
	}
 	
 	/**
 	 * Capture RT parameters
 	 * 
 	 * @param  array  $fields
 	 * @return
 	 */
	protected function captureRTparams( $fields = array() )
	{

		$partnerUserAcct = new PartnerUserAcct();

		$LoanPurpose = (isset($fields['LoanPurpose']))? $fields['LoanPurpose'] : DB::raw("DEFAULT");
		$LoanPurpose = strtoupper( $LoanPurpose );
		$empStatus 	 = (isset($fields['EmploymentStatus']))? $fields['EmploymentStatus'] : "Other";

	    //capture LT Rate Table Parameters	    			
		$rtParameter = array( 
			'PID'                => (isset($fields['PID']))? $fields['PID'] : DB::raw("DEFAULT"),
			'trackingNr'         => (isset($fields['SID']))? $fields['SID'] : DB::raw("DEFAULT"),
			'CID'                => (isset($fields['CID']))? $fields['CID'] : DB::raw("DEFAULT"),
			'loanPurpose'        => $this->loanPurposeLT( $LoanPurpose ),
			'loanAmt'            => (isset($fields['LoanAmount']))? $fields['LoanAmount'] : DB::raw("DEFAULT"),
			'APR'                => (isset($fields['APR']))? $fields['APR'] : DB::raw("DEFAULT"),
			'term'               => (isset($fields['Term']))? $fields['Term'] : DB::raw("DEFAULT"),
			'empStatus'          => getEmployeeStatusIdViaDesc( $empStatus ),
			'selfReportedCredit' => (isset($fields['SelfStatedCreditScore']))? $fields['SelfStatedCreditScore'] : DB::raw("DEFAULT"),
			'zip'                => (isset($fields['PostalCode']))? $fields['PostalCode'] : DB::raw("DEFAULT"),
			'state'              => (isset($fields['State']))? $fields['State'] : DB::raw("DEFAULT")
		);

		$partnerUserAcct->saveApplicantData($rtParameter);	

	}

	/**
	 * Create Loan Object into ODS and NLS
	 * 
	 * @param  array  $data
	 * @return
	 */
	protected function createLoan( $userId, $data = array() )
	{
		
		$isPassed = FALSE;

		if( count($data) > 0 && !empty($userId) ) {

			$partner  = $data['Partner'];
			$personal = $data['Customer']['Personal'];
			$loanApp  = $data['Customer']['LoanApp'];

			$rentAmount = 0;

			//Check for Rent Amount
			if( isset($personal['RentAmount']) ){
				$rentAmount = ($personal['RentAmount'] == 0 ) ? self::DEFAULT_RENT_AMT : $personal['RentAmount'];
			}

			$fields = array(
				'partnerID'          => $partner['PID'],
				'Funding_Partner_Id' => $partner['PID'], 
				'campaignID'         => $partner['CID'],
				'Campaign_Id'		 => $partner['CID'],
				'yodleeUserName'     => $partner['NLSUsername'], 
				'yodleePassword'     => $partner['Password'],
				'firstName'          => trim($personal['FirstName']),
				'middleName'         => ' ',
				'lastName'           => trim($personal['LastName']),
				'city'               => trim($personal['City']),
				'streetAddress1'     => trim($personal['StreetAddress']),
				'state'              => trim($personal['State']),
				'birthDate'          => $personal['DOB'],
				'age'                => getAgeFromDOB($personal['DOB']),
				'rentAmount'         => trim($rentAmount),
				'annualGrossIncome'  => trim($loanApp['AnnualGrossIncome']),
				'homePhoneNumber'    => trim($personal['Phone']),
				'ssn'                => $personal['SSN'],
				'zip'                => trim($personal['Zip']),
				'token'              => trim($partner['Token']),
				'nlsLoanPortfolio'   => Config::get('nls.NLS_LOAN_PORTFOLIO_ID'),
				'nlsLoanTemplate'    => Config::get('nls.NLS_LOAN_TEMPLATE_ID'),
				'nlsLoanGroup'       => Config::get('nls.NLS_LOAN_GROUP_ID'),
				'loanAmount'         => $loanApp['LoanAmount'],
				'loanPurposeId'      => $loanApp['ReasonForLoan'],
				'loanProductId'      => Config::get('nls.NLS_DEFAULT_LOAN_ID'),
				'housingSit'         => $personal['RentOrOwn'], 
				'ip'                 => $personal['Ipaddress'],
				'term'               => 36,
				'loanStatusCode'     => 'APPLICATION_STEP1',
				'loanPurpose'        => LoanPurpose::getLoanPurposeName( $loanApp['ReasonForLoan']), 
				'creditBureauHeader' => Config::get('system.NLS.InitialPull'),
				'employeeStatus'     => $personal['EmploymentStatus'], 
				'contactPhoneNr'	 => DB::raw('DEFAULT'),
				'contactName' 		 => DB::raw('DEFAULT'), 
				'monthlyMortage'	 => 0, 
				'leadId'			 => $partner['LeadId'],
				'email'			     => $personal['Email']
			);

			//Check for bureauCredit
			if (isset($loanApp['BureauProvidedCredit'])) {
				if (!empty($loanApp['BureauProvidedCredit']) && !is_numeric($loanApp['BureauProvidedCredit'])) {
					$this->setError(self::ERR_VALIDATION_CODE, 'BureauProvidedCredit should be numeric value only.');
					return $isPassed;
				}
				else {
					$fields['bureauProvidedCredit'] = $loanApp['BureauProvidedCredit'];
				}
			}

			//Get Employment status code
			if(!isset($fields['employeeStatus']))
				$fields['employeeStatus'] = 2;	
			
			$fields['employeeStatusDescription'] = '';

			//Get Employment Status
			$empStat = EmployeeStatus::find( $fields['employeeStatus'] );

			if( count( $empStat ) > 0 )
				$fields['employeeStatusDescription'] = $empStat->Employment_Status_Desc;
			
			//Set State and Contact Id for NLS
			$fields['State']     = $fields['state'];
			$fields['contactId'] = $userId;
			$fields['userId']	 = $userId;

			// insert to Applicant table
			$applicant = new Applicant();

			//Initialize NLS
			$NLS  = new NLS();

			$NLS->NLSUpdateContact( $fields );
 
			$creditPull = $NLS->IsNLSCreditPullExist($userId);

			if( $creditPull == 'true' ){
				
				//No Records will be created if Credit Pull Request is FAILED. 
				//--APPLICANT TABLE
				//Check if the Applicant Already Exist
				$updateFlag = 0;

				if( Applicant::find( $userId ) )
					$updateFlag = 1;


				//Save Applicant Information
				$applicant->saveApplicant($fields, $updateFlag);

				//Set Applicant Information into Session
				setApplicantData( $userId );

				//--APPLICANT ADDRESS
				//Save Applicant Address Information
				$address = new ApplicantAddress();

				$fields['zip4'] = '';
			
				if( strpos( $fields['zip'], '-' ) != false ){
					$zipArr = explode('-', $fields['zip']);
					if( isset($zipArr[0]) )
						$fields['zip']  = $zipArr[0];
					if( isset($zipArr[1]) )
						$fields['zip4'] = $zipArr[1];
				}

				//Get the substring of zip code 
				$fields['zip']  = substr( $fields['zip'], 0, 5 );
				$fields['zip4'] = substr( $fields['zip4'], 0, 4 );

				$address->saveApplicantAddress($fields, $updateFlag);
					
				//Save Applicant Contact Information
				$contact = new ApplicantContactInfo();
				$contact->saveApplicantContact($fields, $updateFlag);
		
				ApplicantFinancialInfo::updateOrCreateAppFinancialInfo($fields, $updateFlag);

				$loanAppNr = $this->getExistingLoanAppNr($userId);
			 
				//Update Or Create Loan Application Number
				LoanApplication::updateOrCreateLoanApp( $loanAppNr, $fields , $updateFlag );

				$newCreatedLoanAppNr = $this->getExistingLoanAppNr($userId);
 
	  			//Update or Create Loan Detail 
				LoanDetail::updateOrCreateLoanDetail($fields, $newCreatedLoanAppNr, $updateFlag ); 
	 			
	 			$subPhase  = Config::get('subphase.CreditPullSuccess');

	 			$fields['Loan_Sub_Phase_Nr']    = $subPhase['Loan_Sub_Phase_Id'];
	 			$fields['Loan_Sub_Phase_Desc']  = $subPhase['Loan_Sub_Phase_Desc'];

				//Update or Create Loan App Detail	
	 			LoanApplicationDetail::updateOrCreateLoanAppDetail($fields, $newCreatedLoanAppNr );

	 			$this->setLoanAppNr( $newCreatedLoanAppNr );

	 			//Save Partner Information
	 			$fields['userId'] =  $userId; //update the partner user ID
	 			$this->savePartnerUserAcctData( $fields );

	 			//Get Loan Portfolio Name, Group and Template
				$nlsLoanPortfolio = LoanTemplate::getLoanTemplateName($fields['nlsLoanPortfolio']);
				$nlsLoanGroup     = LoanTemplate::getLoanTemplateName($fields['nlsLoanGroup']);
				$nlsLoanTemplate  = LoanTemplate::getLoanTemplateName($fields['nlsLoanTemplate']);

				//@override
				$fields['loanId']           =  $newCreatedLoanAppNr;
				$fields['nlsLoanPortfolio'] =  $nlsLoanPortfolio;
				$fields['nlsLoanGroup']     =  $nlsLoanGroup;
				$fields['nlsLoanTemplate']  =  $nlsLoanTemplate;
 				$fields['employeeStatus']   =  $fields['employeeStatusDescription'];

	 			$NLS->NLSCreateLoan( $fields );

	 			$isPassed = TRUE;	

	 		}else{
	 			$isPassed = FALSE;
	 		}

		}
 
		return $isPassed;
	}

	/**
	 * Get Existing Loan Applicatoin Number
	 * 
	 * @param  integer $userId
	 * @return
	 */
	protected function getExistingLoanAppNr( $userId )
	{
		$existingLoan = LoanApplication::hasExistingLoan($userId);
 
		if( isset( $existingLoan->Loan_App_Nr ) )
			$loanAppNr = $existingLoan->Loan_App_Nr;
		else
			$loanAppNr = NULL;

		return $loanAppNr;
	}

	/**
	 * Send Partner Notice Response
	 * 
	 * @param  string $type
	 * @param  array $data
	 * @return
	 */
	protected function partnersAPINotice( $type = 'fail', $param = array())
	{	
		$response = array();

		if( isset($param['Application']) ) {

			$data = $param['Application'];

			if( $type == 'fail'){

				$response = array(
					'PID'      => $data['Partner']['PID'],
					'CID'      => $data['Partner']['CID'],
					'LeadId'   => $data['Partner']['LeadId'], 
					'Decision' => 'NO OFFER'
				);
			}else{

				$landingPageUrl = $this->formatLandingPageUrl( $param );

				$response = array(
					'PID'         => $data['Partner']['PID'],
					'CID'         => $data['Partner']['CID'],
					'LeadId'      => $data['Partner']['LeadId'], 
					'Decision'    => 'OFFER',
					'LandingPage' => $landingPageUrl
				);
			}
		}
 
		return $response;
	}

	/**
	 * Track API Data
	 * 
	 * @param  integer $userId
	 * @param  array   $data
	 * @param  string  $type
	 * @return
	 */
	protected function trackApiData( $data = array(), $request = array(), $type = 'request')
	{

		if( isset($data['Application']) && count($request) > 0 ){

			$post = $data['Application'];
			
			$PartnerAPITracking = new PartnerAPITracking();

			$requestData = array(
				'Partner_Id'                 => $post['Partner']['PID'],
				'Tracking_Nr'                => $post['Partner']['LeadId'],
				'API_Txn_Type_Desc'          => $type,
				'API_Txn_Dt'                 => date('Y-m-d H:i:s'),
				'Return_Cd'                  => $request['code'],
				'Return_Msg_Txt'             => $request['msg'],
				'API_Req_Res_File_Type_Desc' => 'JSON',
				'API_Req_Res_Txt'            => $request['RequestResponseTxt']
			);

			// _pre($requestData);

			$PartnerAPITracking->saveApiInfo($requestData);
		}
	}
	/**
	 * Format Landing Page Url
	 * 
	 * @param  array  $data
	 * @return
	 */
	protected function formatLandingPageUrl( $data = array() )
	{	 
		$landingPageUrl = "";

		if( isset($data['userId']) && isset($data['loanAppNr']) ){

			$app  = $data['Application'];
			//Get Partner Type Id
			$partner = Partner::find( $app['Partner']['PID'], array('Utm_Medium_Txt', 'Utm_Source_Txt', 'Utm_Content_Txt', 'Utm_Campaign_Txt', 'API_URL_Key_Name'));

			$landingPageUrl  = url() . '/LP/'. $partner->API_URL_Key_Name .'/lp1/' . $data['userId']; 
			$landingPageUrl .= '/' . $data['loanAppNr']; 
			$landingPageUrl .= '/' . $app['Partner']['LeadId']; 
			$landingPageUrl .= '?PID=' . $app['Partner']['PID']; 
			$landingPageUrl .= '&CID=' . $app['Partner']['CID']; 
			$landingPageUrl .= '&utm_source=' . $partner->Utm_Source_Txt; 
			$landingPageUrl .= '&utm_medium=' . $partner->Utm_Medium_Txt; 
			$landingPageUrl .= '&utm_content=' . $partner->Utm_Content_Txt; 
			$landingPageUrl .= '&utm_campaign=' . $partner->Utm_Campaign_Txt; 
		}

		return $landingPageUrl;
	}
 
	/**
	 * Show Landing Pages for Complex Partners
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  string $leadId
	 * @return
	 */
	public function showComplexPartnerPage($userId, $loanAppNr, $leadId )
	{ 
		//Get Partner Id and Campaign Id from Query String
		$input = Input::all(); 
		$this->showLandingPage($userId, $loanAppNr, $leadId, $input);
	}

	/**
	 * Show Partner Landing Page
	 * - Dot818 and Market Leads
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return 
	 */ 
	protected function showLandingPage( $userId = NULL, $loanAppNr = NULL, $leadId = NULL, $input = array() )
	{	

		if( empty($userId) )
			return Redirect::to('/');

		$fields = array(
			'Borrower_User_Id', 
			'Monthly_Payment_Amt', 
			'APR_Val', 
			'Loan_Amt'
		);

		$loanDetail = LoanDetail::getLoanDetail(
			$userId, 
			$loanAppNr, 
			$fields
		);

		if( !isset($input['PID']) || !isset($input['CID']) )
			return Redirect::to('/');

		//Select Loan Purpose 
		$loanAppDetail = LoanApplication::getLatestApplication( $userId );

		//Get Partner Type Id
		$partnerType = Partner::find( $input['PID'] , array('Partner_Type_Id'));

		// dd($partnerType);

		if( count($loanDetail) > 0 ) {
			$data = array(
				'Loan_Amt'             => number_format( $loanDetail['Loan_Amt'], 0 ), 
				'Monthly_Payment_Amt'  => number_format( $loanDetail['Monthly_Payment_Amt'], 2 ), 
				'Terms'                => 36, 
				'APR_Val'              => $loanDetail['APR_Val'] * 100, 
				'User_Id'              => $userId, 
				'Loan_App_Nr'          => $loanAppNr, 
				'Campaign_Id'          => $input['CID'], 
				'Partner_Id'           => $input['PID'], 
				'Lead_Id'              => $leadId,
				'Loan_Purpose_Id'      => $loanAppDetail->Loan_Purpose_Id, 
				'Partner_Type_Id'      => $partnerType->Partner_Type_Id
			);

			setPartnerSession($data);

		}else{
			return Redirect::to('/');
		}
  		$this->layout->partnerFlag = 1; 
		$this->layout->homepage = true;
		$this->layout->mainlogo = true;
		$this->layout->content  = View::make('blocks.partner.complex', $data );
	}

	/**
	 * Post the Response to Partners API End Point
	 * 
	 * @param  array $request  Partners Request
	 * @param  array $response System's Response
	 * @param  string $parnterEndPointURL
	 * @return
	 */
	public function postParnterResponse($request = array(), $response = array() , $parnterEndPointURL )
	{
		if( count($request) > 0 && !empty($parnterEndPointURL) && count($response) > 0 ) {

			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL, $parnterEndPointURL); 
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			curl_setopt($ch, CURLOPT_TIMEOUT, 4); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $response ); 
		 
			$info = curl_exec($ch); 
			 
			//Check for errors ( again optional )
			if ( curl_errno($ch) ) {
				$result = array(
					'code' 		=> curl_errno($ch),
					'response'  => curl_error($ch)
				);
			} else {
				$result = array(
					'code'     => (int)curl_getinfo($ch, CURLINFO_HTTP_CODE), 
					'response' => $info
				);
			}
			 
			//Close the handle
			curl_close($ch);

			$response['RequestResponseTxt'] = json_encode($response);
			$response['code'] = $result['code'];
			$response['msg']  = $result['response'];

			$this->trackApiData($request, $response, 'response');

		} else{
			$result = array(
				'code' => self::ERR_MISSING_CODE, 
				'response'  => 'Empty parameter or response url not found.'
			);
		} 
	}
 
	/**
	 * Validate Field By Rules
	 * 
	 * @param  mixed $val
	 * @param  string $rules
	 * @return
	 */
	public function validateFieldsByRules( $fields = array(), $rules = array() ) 
	{
		
		$messages = array();

		if( is_array($fields)  && is_array($rules) ) {

			// pre($rules);
			// pre($fields);  
			$validator =  Validator::make(
			    $fields, 
			    $rules
			);

			if ($validator->fails()) {
			    $messages = $validator->messages();
			}

			if ( count($messages) > 0 ) {
				foreach ($messages->all() as $key => $value) {   
					//format SSN 
					$value = str_replace('s sn', 'ssn', $value);
					$this->setError(self::ERR_VALIDATION_CODE, $value );
				}				
			}
		}
	}

	/**
	 * Get Customer Required Fiels
	 * 
	 * @return array
	 */
	public function getCustomerReqFields()
	{
		return Config::get('partnerFields.Application.Customer');
	}

	/**
	 * Get Partner Required Fields 
	 * 
	 * @return array
	 */
	public function getParnterReqFields()
	{
		return Config::get('partnerFields.Application.Partner');
	} 

	/**
	 * Post Test End Point URL 
	 * 
	 * @return
	 */
	public function postTestEndPoint()
	{
		$this->layout = NULL;
		$this->setupLayout();
		return Response::json( Input::all(), 200 ) ;
	}
  	
  	/**
  	 * Partner Test Harness
  	 * 
  	 * @return
  	 */
	public function showPartnerTest()
	{
		$this->layout->content  = View::make('blocks.harness.partnersTest' ); 
	}

	/**
	 * Home A/B Google Experiment
	 */
	protected function showGAExperiment()
	{

		//forget the relative URL on homepage only
		Session::forget('HPURL');

		$data['isProductionQA'] = FALSE;
		$data['isProductionBypass'] = TRUE;

		//@Override Production QA
		if( Input::get('case') == 'test' )
			$data['isProductionQA'] = TRUE;
		if( Input::get('bypass') == 0 )
			$data['isProductionBypass'] = FALSE;

		//get url parameters
		$PID = trim(Input::get('PID'));

		//Check and store CID and PID int the session for later use in the registration
		trackPartnerIds($PID);

		//set slider value
		$partnerUserAcct = new PartnerUserAcct(); 
		$partnerUserInfo = $partnerUserAcct->getData( ['Offered_Loan_Amt'] , ['Partner_User_Id'  => Session::get('partner.UID')], true );
		
		$data['defaultSliderAmt'] = (count($partnerUserInfo) > 0 )? $partnerUserInfo->Offered_Loan_Amt : 5000; 

		$this->layout = View::make('layouts.experiment');
		// set debugMode value
		$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);
 
		//Query String for State Code
		if( !empty( Input::get('state') ) ){
			$stateCode = Input::get('state');
		} else{
			$stateCode = getStateCodeViaIp();
		}

		$data['sliderMinLoanAmt'] = self::DEFAULT_MIN_LOAN_AMT;
		$data['sliderMaxLoanAmt'] = self::DEFAULT_MAX_LOAN_AMT;
 
		if( !empty($stateCode) ) {
			//Select Loan Amount Range Depending on State
			$rateMap = LoanProdRateMap::getLoanAmtRange($stateCode);

			if( count($rateMap) > 0 ) {
				$data['sliderMinLoanAmt'] = $rateMap->Absolute_Min_Loan_Amt;
				$data['sliderMaxLoanAmt'] = $rateMap->Absolute_Max_Loan_Amt;
			}
		}	

		$this->layout->homepage = true;
		$this->layout->content  = View::make('blocks.experiment.index', $data );
	}


	// ====================================== LENDING TREE ============================================

	/**
	 * Lending tree request endpoint 
	 */
	public function showLendingTree()
	{
		//get inputs if availbale
		$input = Input::all();

		$this->layout = NULL;

		// for debugng response
		$showResponse = isset( $input['showLTresponse'] ) ? true : false;

		$CID_Filter_val = '';
		$lpVarationNum 	= 1; //default to landing page 1

		//track partner xml data
		$PartnerAPITracking = new PartnerAPITracking();

		// partner name
		$this->partnerName = 'lendingtree';
		
		$loanStat = array(
			'S1' => array('MAX_LOAN_AMT' => 15000, 	'APR' => 0.27 ),
			'S2' => array('MAX_LOAN_AMT' => 10000, 	'APR' => 0.30 ),
			'S3' => array('MAX_LOAN_AMT' => 5000, 	'APR' => 0.33 )
		);

		//Get LT Configurations
		$LTCfg = new LTCampaignCfg();
		$CID_Filter = $LTCfg->getCIDFilters();
		$UTM_Content_LowScore = $LTCfg->getValue('UTM_Content_Low');
		$UTM_Content_HighScore = $LTCfg->getValue('UTM_Content_High');
 
		$xml       = file_get_contents('php://input');
		$xmlObject = simplexml_load_string($xml) or die("Error: Cannot create xml object");
		
		$data['FilterRoutingID'] 	= $xmlObject->PartnerProfileInformation->FilterRoutingID;
		$data['FilterName'] 		= $xmlObject->PartnerProfileInformation->FilterName;

		$data['User_Id']      = $xmlObject->TrackingNumber;
		$data['creditScore']  = $xmlObject->ConsumerProfileInformation->Credit->AssignedCreditValue;
		$data['AnnualSalary'] = $xmlObject->ConsumerProfileInformation->ProductProfileInformation->AnnualIncome;
		$data['loanAmt']      = trim($xmlObject->LoanInformation->LoanAmount);
		
		//check if the FilterRoutingID matches then return the correct CID
		foreach ($CID_Filter as $key => $value)			
			if(in_array($data['FilterRoutingID'], $value))
				$CID_Filter_val = $key;
 
		// if CID_Filter_val is present asign else switch to detecting CID via Scoring

		if($CID_Filter_val || $CID_Filter_val != '')
			$data['partner']['CID']  = $CID_Filter_val;
		else
			$data['partner']['CID']  = ($data['creditScore'] <= 620 )? 6 : 7 ;

		// get the landing page varation from the CID
		$lpVarationNum = $this->getLTlpNum( $data['partner']['CID'] );

		//Retrieve partner ID
		$partner = Partner::where('Logo_Image_File_Name','=',$this->partnerName)->first();
		
		$data['partner']['PID']         = (count($partner) > 0)	? $partner->Partner_Id : 0;	
		$data['partner']['PNAME']       = $this->partnerName;
		$data['partner']['UID']         = $xmlObject->TrackingNumber; 
		$data['partner']['utm_content'] = ($data['creditScore'] <= 620 )? $UTM_Content_LowScore : $UTM_Content_HighScore ;
		
		$scoreIncome =  $this->scoreIncomeMatrix( $data['creditScore'], $data['AnnualSalary'] );
		
		//acknowledegement response success
		$data['ack']['QFNAME']           = $data['User_Id'];
		$data['ack']['ERRORNUM']         = '0';
		$data['ack']['ERRORDESCRIPTION'] = 'Success';		

		// if( $scoreIncome && $filter ):
		if( $scoreIncome ){

			//format offer url 
			$offerURL = $this->getLTofferURL(
					$data['partner']['UID'],
					$data['partner']['CID'],
					$data['partner']['utm_content'],
					$lpVarationNum
				); 
			
			$consumerInfo = $xmlObject->ConsumerContactInformation;
			$consumerProfInfo = $xmlObject->ConsumerProfileInformation;
 	
 			$data['responseData']['LoanAmount'] = $data['loanAmt'];

			if( $data['loanAmt'] > $loanStat[$scoreIncome]['MAX_LOAN_AMT'] )	
				$data['responseData']['LoanAmount'] = $loanStat[$scoreIncome]['MAX_LOAN_AMT'] ;

			$data['responseData']['OfferUrl']          = $offerURL;
			$data['responseData']['LoanApplicationID'] = $xmlObject->TrackingNumber;
			$data['responseData']['applicantName']     = $consumerInfo->FirstName .' '. $consumerInfo->LastName; 
			$data['responseData']['InterestRate']      = $loanStat[$scoreIncome]['APR'] / 12;
			$data['responseData']['APR']               = $loanStat[$scoreIncome]['APR'] * 100;
			$MonthlyPayment                            = pmt( $data['responseData']['APR'] , 36, $data['responseData']['LoanAmount'] ); 
			$data['responseData']['MonthlyPayment']    = number_format( $MonthlyPayment , 2 ) ;			
			$data['responseData']['ExpirationDate']    = date('Y/m/d', strtotime(date('Y/m/d'). ' + 7 days'));

			//gather email notification data
			$emailData['FIRSTNAME']   		= $consumerInfo->FirstName;
			$emailData['LOAN_AMOUNT'] 		= $data['responseData']['LoanAmount'];
			$emailData['OFFER_URL']   		= $data['responseData']['OfferUrl'];
			$emailData['APR']         		= $data['responseData']['APR'] . "%";
			$emailData['TERMS']		  		= 36;
			$emailData['MONTHLY_PAYMENT'] 	= $data['responseData']['MonthlyPayment'];

			//send email notification
			$email = $consumerInfo->EmailAddress;
			settype($email, "string");			
			$this->sendPartnerNotification( $email , $emailData );

			//save applicant data
			$PartnerUserAcct = new PartnerUserAcct();

			//process zip code
			$zipRaw = explode('-', $consumerInfo->ContactZip );

			//get housing situation
			$housingSitLT = $this->housingSitLT( 
										$consumerProfInfo
											->ProductProfileInformation
											->ResidenceType 
									);

			//get employee status
			$empStatusLT =  $this->empStatusLT( 
										$consumerProfInfo
											->ProductProfileInformation
											->EmploymentStatus 
									);

			//get loan purpose
			$loanPurpose = $this->loanPurposeLT(
										$xmlObject->LoanInformation
											->LoanRequestPurpose 
									);

			$applicantData = array(
				'PID' 			=> $data['partner']['PID'],
				'CID'			=> $data['partner']['CID'],
				'trackingNr' 	=> $data['User_Id'],
				'firstName' 	=> $emailData['FIRSTNAME'] ,
				'lastName' 		=> $consumerInfo->LastName,
				'city' 			=> $consumerInfo->ContactCity,
				'state' 		=> $consumerInfo->ContactState,
				'zip' 			=> (isset($zipRaw[0]))? $zipRaw[0] : "",
				'phoneNr' 		=> $consumerInfo->ContactPhone,
				'email' 		=> $email,
				'score' 		=> $data['creditScore'],
				'income' 		=> $data['AnnualSalary'],
				'APR' 			=> $data['responseData']['APR'],
				'loanAmt' 		=> $data['responseData']['LoanAmount'],
				'dob'			=> $consumerProfInfo->DateOfBirth,
				'ssn'			=> $consumerProfInfo->SSN,
				'address1'		=> $consumerInfo->ContactAddress,
				'address2'		=> '',	
				'zip4'			=> (isset($zipRaw[1]))? $zipRaw[1] : "",
				'housingSit'	=> $housingSitLT,
				'empStatus'		=> $empStatusLT,
				'montRent'		=> $MonthlyPayment,
				'loanPurpose'	=> $loanPurpose,
				'term'			=> 36, 
				'selfReportedCredit' => $data['creditScore']
			);
			
			//if express funnel set the flag
			if( $data['partner']['CID'] != 1 )
				$applicantData['LTExpressFunnelFlag'] = 1;

			$PartnerUserAcct->saveApplicantData($applicantData);

			//Save API Information
			$requestData = array(
				'Partner_Id'                 => $data['partner']['PID'],
				'Tracking_Nr'                => $data['User_Id'],
				'API_Txn_Type_Desc'          => 'request',
				'API_Txn_Dt'                 => date('Y-m-d H:i:s'),
				'Return_Cd'                  => $data['ack']['ERRORNUM'],
				'Return_Msg_Txt'             => $data['ack']['ERRORDESCRIPTION'],
				'API_Req_Res_File_Type_Desc' => 'XML',
				'API_Req_Res_Txt'            => $xml
			);

			$PartnerAPITracking->saveApiInfo($requestData);

		}

		echo $this->lt_ack($data['ack']);

		//get the appropiate response based on the scoring
		$LTresponse = ($scoreIncome && getPartnerStatus( $data['partner']['PID'] ) )? $this->approved_LendingTreeResponse($data['responseData']) : $this->noOffer_LendingTreeResponse($xmlObject->TrackingNumber);

		// for testing to show the xml response to be sent to LT
		if($showResponse)
			echo "\n\n response for LT >>>>>>>>>> endPoint : https://lenderweb.staging.lendingtree.com/offers/Personalofferpost.aspx \n\n" . $LTresponse;

		// prepare response data for store
		$reponseData = array(

			'Partner_Id' 		=> $data['partner']['PID'],
			'Tracking_Nr' 		=> $data['User_Id'],
			'API_Txn_Type_Desc' => 'response',
			'API_Txn_Dt'		=> date('Y-m-d H:i:s'),
			'API_Req_Res_File_Type_Desc' => 'XML',
			'API_Req_Res_Txt'   => $LTresponse

		);		
	
		//send the response
		$lt_acknowledgement = $this->sendLTResponse($LTresponse);

		// track lt response
		$lt_ackXmlObject = simplexml_load_string($lt_acknowledgement) or die("Error: Cannot create xml object");
		$reponseData['Return_Cd'] 		= $lt_ackXmlObject->ERRSTATUS->ERRORNUM;
		$reponseData['Return_Msg_Txt'] 	= $lt_ackXmlObject->ERRSTATUS->ERRORDESCRIPTION;
		$PartnerAPITracking->saveApiInfo($reponseData);

		//for testing
		// echo $scoreIncome;echo $LTresponse;

	}

	/**
	 * get the landing page varation from Campaign ID
	 * @param  [int] $cid campaign Id
	 * @return [int] 
	 * @notes : should be set up on database on ticket TECH 5
	 */
	public function getLTlpNum($cid = 1)
	{
		$varationNum['1'] 	=  '1';
		$varationNum['6'] 	=  '5';
		$varationNum['7'] 	=  '5';
		$varationNum['15'] 	=  '6';

		return $varationNum[$cid];

	}

	/**
	 * Get LT offer URL
	 * 
	 * @param  integer $uid
	 * @param  integer $cid
	 * @param  string $utmContent
	 * @return string
	 */
	public function getLTofferURL( $uid, $cid, $utmContent, $lpNum = 1 )
	{
		//Get LT configurations
		$ltCfg = new LTCampaignCfg();
		$utmSource = $ltCfg->getValue('UTM_Source');
		$utmMedium = $ltCfg->getValue('UTM_Medium');
		$utmCampaign = $ltCfg->getValue('UTM_Campaign.' . $cid);

		$url = URL::to('/').'/LP/'.$this->partnerName;
		$url .= '/lp'. $lpNum .'?UID='.$uid.'&amp;CID='; 
		$url .= $cid; 
		$url .= '&amp;utm_source=' . $utmSource;
		$url .= '&amp;utm_medium=' . $utmMedium;
		$url .= '&amp;utm_content=' . $utmContent;
		$url .= '&amp;utm_campaign=' . $utmCampaign;

		return $url;
	}

	/**
	 * Lending tree harnes for testing API request
	 * 
	 * @return
	 */
	public function showLendingTreeHarness()
	{
		return View::make('blocks.harness.lendingTreeTest');
	}


	/**
	 * Lending tree harnes for testing API response
	 * 
	 * @return xml
	 */
	public function lendingTreeHarness()
	{	
		$this->layout = NULL;

		$url = Input::get('uri');
		$xml = Input::get('req');

		$post_string = '<?xml version="1.0" encoding="UTF-8"?>';
		$post_string .= $xml;

		$headers = array(
		    "Content-type: text/xml",
		    "Content-length: " . strlen($post_string),
		    "Connection: close",
		);

		//Initialize handle and set options
		// $username = 'dolepaddy';
		// $password = 'secret123';
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		// curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 12); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 
		//Execute the request and also time the transaction ( optional )
		// $start = array_sum(explode(' ', microtime()));
		$result = curl_exec($ch); 
		// $stop = array_sum(explode(' ', microtime()));
		// $totalTime = $stop - $start;
		 
		//Check for errors ( again optional )
		if ( curl_errno($ch) ) {
		    $result = 'ERROR --> ' . curl_errno($ch) . ': ' . curl_error($ch);
		} else {
		    $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    switch($returnCode){
		        case 200:
		            break;
		        default:
		            $result = 'HTTP ERROR --> ' . $returnCode;
		            break;
		    }
		}
		 
		//Close the handle
		curl_close($ch);
		 
		//Output the results and time
		// echo 'Total time for request: ' . $totalTime . "\n";
		echo $result; 
	}

	/**
	 * Sending Lending Tree Repsponse 
	 * 
	 * @param  xml $xml
	 * @return xml
	 */
	public function sendLTResponse($xml)
	{
		// $url = 'https://lenderweb.staging.lendingtree.com/offers/Personalofferpost.aspx';
		$url = Config::get('system.LendingTree.Endpoint');
		//$xml = Input::get('req');

		$post_string = '<?xml version="1.0" encoding="UTF-8"?>';
		$post_string .= $xml;

		$headers = array(
		    "Content-type: text/xml",
		    "Content-length: " . strlen($post_string),
		    "Connection: close",
		);

		//Initialize handle and set options
		// $username = 'dolepaddy';
		// $password = 'secret123';
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		// curl_setopt($ch, CURLOPT_USERPWD, $username.':'.$password);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 4); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string); 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 
		//Execute the request and also time the transaction ( optional )
		// $start = array_sum(explode(' ', microtime()));
		$result = curl_exec($ch); 
		// $stop = array_sum(explode(' ', microtime()));
		// $totalTime = $stop - $start;
		
		//Check for errors ( again optional )
		if ( curl_errno($ch) ) {
		    $result = 'ERROR -> ' . curl_errno($ch) . ': ' . curl_error($ch);
		} else {
		    $returnCode = (int)curl_getinfo($ch, CURLINFO_HTTP_CODE);
		    switch($returnCode){
		        case 200:
		            break;
		        default:
		            $result = 'HTTP ERROR -> ' . $returnCode;
		            break;
		    }
		}
		 
		//Close the handle
		curl_close($ch);		 
		//Output the results and time
		//echo 'Total time for request: ' . $totalTime . "\n";
		// echo $result; 

		$xmlObject = simplexml_load_string($result) or die("Error: Cannot create xml object");

		if($xmlObject->ERRSTATUS->QFNAME){

			$LendingTreeacktemp = new LendingTreeacktemp();
			$LendingTreeacktemp->QFNAME 			= $xmlObject->ERRSTATUS->QFNAME;
			$LendingTreeacktemp->ERRORNUM 			= $xmlObject->ERRSTATUS->ERRORNUM;
			$LendingTreeacktemp->ERRORDESCRIPTION 	= $xmlObject->ERRSTATUS->ERRORDESCRIPTION;
			$LendingTreeacktemp->save();
		
		}

		return $result;
	}


	/**
	 * Get PMT Function 
	 *
	 *
	 * @todo   use the pmt helper @see  helper.php
	 * @param  integer $apr
     * @param  integer $np
     * @param  integer $pv
	 */
	private function getPMT($apr, $np, $pv)
	{

		$ir = $apr / 12;

		$pvif = pow(1 + $ir, $np);
		$pmt = - $ir * $pv * $pvif / ($pvif - 1);

		return floor(abs($pmt));
	}


	/**
	 * Get the FICO score based on the creditLimit ang the income
	 * 
	 * @param  integer $score  FICO score
	 * @param  integer $income income annual
	 * @return string
	 */
	private function scoreIncomeMatrix( $score, $income )
	{
		if($income > 35000){

			
			if( in_array($income, range(35000, 50000)) )
				$stat = ( in_array($score, range(640, 660)) )? 'S2' : 'S3';

			if( in_array($income, range(50000, 75000)) )
				if( in_array($score, range(580, 620)) )
					$stat = 'S3';
				elseif( in_array($score, range(620, 640)) )
					$stat = 'S2';
				elseif( in_array($score, range(640, 1000)) )
					$stat = 'S1';

			if( $income > 75000 )
				if( in_array($score, range(580, 600)) )
					$stat = 'S3';
				elseif( in_array($score, range(600, 620)) )
					$stat = 'S2';
				elseif( in_array($score, range(620, 1000)) )
					$stat = 'S1';

			/*
			//save old settings
			if( in_array($income, range(35000, 50000)) )
				$stat = ( in_array($score, range(580, 620)) )? 'S3' : 'S2';

			if( in_array($income, range(50000, 75000)) )
				$stat = ( in_array($score, range(580, 620)) )? 'S2' : 'S1';

			if( $income > 75000 )
				$stat = 'S1';*/


		}else{
			$stat = false;
		}
		
		return $stat;
	}

	/**
	 * Acknowledgement response if the request is successull or has an error
	 * 
	 * @param  array  $ackData
	 * @return xml
	 */
	private function lt_ack( $ackData = array() )
	{

		$ackXML = '<?xml version="1.0" encoding="utf-8" ?>
					<LTACK>
						<ERRSTATUS>
							<QFNAME>'.$ackData['QFNAME'].'</QFNAME>
							<ERRORNUM>'.$ackData['ERRORNUM'].'</ERRORNUM>
							<ERRORDESCRIPTION>'.$ackData['ERRORDESCRIPTION'].'</ERRORDESCRIPTION>
						</ERRSTATUS>
					</LTACK>';

		return $ackXML;
	}

	/**
	 * No offer response from lending tree
	 * 
	 * @param  integer
	 * @return xml
	 */
	private function noOffer_LendingTreeResponse($loanApplicationID)
	{

		$responseXML = '<LenderResponse>
							<UserName>'. $this->lt_user .'</UserName>
							<Password>'. $this->lt_pass .'</Password>
							<Mode>2</Mode>
							<LoanApplicationID>'. $loanApplicationID .'</LoanApplicationID>
							<LenderID>'. $this->lt_lenderId .'</LenderID>
						</LenderResponse>';

		return $responseXML;
	}

	
	/**
	 * Succefull offer response for lending treee
	 * 
	 * @param  array $responseData
	 * @return xml               
	 */
	private function approved_LendingTreeResponse( $responseData = array() )
	{
		$ltCfg = new LTCampaignCfg();
		$offerDesc = $ltCfg->getValue('OfferDescription');
		$msgBorrower = $ltCfg->getValue('CustomizedMessageToBorrower');

		$responseXML = '<LenderResponse>
						    <UserName>'. $this->lt_user .'</UserName>
						    <Password>'. $this->lt_pass .'</Password>
						    <Mode>1</Mode>
						    <LoanApplicationID>'. $responseData['LoanApplicationID'] .'</LoanApplicationID>
						    <LenderID>'. $this->lt_lenderId .'</LenderID>
						    <TransactionType>3</TransactionType>
						    <Offers>
						        <Offer>
						            <Offerid>1</Offerid>
						            <Offertype>Fixed</Offertype>
						            <LoanProgramName>Ascend Personal Loan</LoanProgramName>
						            <LoanAmount>'. $responseData['LoanAmount'] .'</LoanAmount>
						            <InterestRate>'. $responseData['InterestRate'] .'</InterestRate>
						            <Term>36</Term>
						            <TermConstant>1</TermConstant>
						            <DiscountPoints>0</DiscountPoints>
						            <MonthlyPayment>'. $responseData['MonthlyPayment'] .'</MonthlyPayment>
						            <MonthlyPaymentOption>2</MonthlyPaymentOption>
						            <APR>'. $responseData['APR'] .'</APR>
						            <Annualfee>0</Annualfee>
						            <ExpirationDate>'. $responseData['ExpirationDate'] .'</ExpirationDate>
						        </Offer>
						    </Offers>
						    <LenderContactInformation>
						        <Name>Ascend Consumer Finance, Inc.</Name>
						        <Email>support@ascendloan.com</Email>
						        <WorkPhone>8004975314</WorkPhone>
						        <WorkphoneExtension>1</WorkphoneExtension>
						        <Fax>8004975314</Fax>
						    </LenderContactInformation>
						    <PersonalizedURL>'. $responseData['OfferUrl'] .'</PersonalizedURL>
						    <OtherFees>
						        <Fee>
						            <FeeAmount>0</FeeAmount>
						            <FeeDescription>0</FeeDescription>
						        </Fee>
						    </OtherFees>
						    <OfferDescription>'. $offerDesc .'</OfferDescription>
						    <CustomizedMessageToBorrower>'. $msgBorrower .'</CustomizedMessageToBorrower>
						    <CannedText> Legal disclosure: This does not constitute a commitment to lend or an offer to extend credit. All loans are subject to credit approval and require additional documentation to allow us to verify your income, identity, and financial condition. Any offer for which you are approved will depend on your credit quality, loan amount, credit history, and other factors. Your actual rate and loan terms will be shown to you as part of the online application process. To receive a loan, you must be at least 18 years old and a U.S. citizen or permanent resident. See our loan agreement for all terms and conditions.</CannedText> 
						</LenderResponse>';


		return $responseXML;
	}

	/**
 	 * Send partner notification 
 	 *      - send email for the loan offer url
 	 *      
 	 * @param  string $email
 	 * @param  array  $data
 	 * @return
 	 */
	private function sendPartnerNotification( $email, $data = array() )
	{

		try{
			$this->email 	= $email;
			$this->subject  = 'Your loan request from LendingTree ';

			Mail::send('emails.auth.partners', $data, function($message)
			{ 
				
				$message->to( $this->email, 'Customer');

			    if( Config::get('system.Ascend.DebugMode') == 'true' ){
			    	$message->cc('chris@ascendloan.com')
			    			->cc('jdizon@vicomm.com')			    			
			    			->cc('sharanya@ascendloan.com')
			    			->cc('mbesina@vicomm.com')
			    			->cc('fcorugda@vicomm.com');
			    }

			   	$message->subject($this->subject);
			   
			});

		} catch(Exception $e) {

			echo $e->getMessage();

			writeLogEvent('Email Partner Notification Sending', 
					array( 
						'Message'     => $e->getMessage()
					),
					'warning'
				); 
		}
	}


	/**
	 * Map employement status from lending tree to our employment status
	 * 
	 * @param  string $ltValue
	 * @return integer
	 */
	private function empStatusLT( $ltValue )
	{
		
		$ltValue = trim($ltValue);

		$empStat = array(
			'NOTPROVIDED' 		=> 8,
			'FULLTIME'			=> 2,
			'PARTTIME'			=> 3,
			'SELFEMPLOYED'		=> 4,
			'HOMEMAKER'			=> 4,
			'STUDENT'			=> 6,
			'RETIRED'			=> 5,
			'OTHER'				=> 8,
			'UNEMPLOYED'		=> 1
		);

		if( isset($empStat[$ltValue]) )
			return $empStat[$ltValue];
		else 
			return $empStat['OTHER'];
	}

	/**
	 * Map housign from LENDing Tree situation to our ID
	 * 
	 * @param  string $ltValue
	 * @return integer
	 */
	private function housingSitLT( $ltValue )
	{

		$ltValue = trim($ltValue);

		$housingSit = array(
			'OWN' 	=> 3,
			'RENT' 	=> 1,
			'OTHER' => 4,
		);

		if( isset($housingSit[$ltValue]) )
			return $housingSit[$ltValue];
		else 
			return $housingSit['OTHER'];
	}

	/**
	 * Map loan purpose from lendign tree
	 * 
	 * @param  string $ltValue
	 * @return
	 */
	private function loanPurposeLT( $ltValue )
	{

		$ltValue = trim($ltValue);

		$loanPurpose = array(

			'DEBTCONSOLIDATION' => 1,
			'OTHER'				=> 8,
			'MAJORPURCHASE'		=> 6,
			'HOMEIMP'			=> 7,
			'CCREFI'			=> 8,
			'HOMEBUYING'		=> 8,
			'CARFINANCING'		=> 8,
			'BUSINESS'			=> 8,
			'VACATION'			=> 3,
			'WEDDING'			=> 8,
			'MOVING'			=> 8,
			'MEDICAL'			=> 4

		);

		if( isset($loanPurpose[$ltValue]) )
			return $loanPurpose[$ltValue];
		else
			return $loanPurpose['OTHER'];
	}
}