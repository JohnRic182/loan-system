<?php

class RaterewardLoanController extends BaseController {
	/*
	|--------------------------------------------------------------------------
	| Raterewards Loan Program Controller
	|--------------------------------------------------------------------------
	*/
  
	protected $layout =  'layouts.raterewards';
	

	protected $cert_file;
	protected $key_file;
	protected $url;
	protected $TU_systemID;
	protected $TU_systemPassword;
	protected $TU_sslPassword;
	protected $TU_industryCode;
	protected $TU_memberCode;
	protected $TU_inquirySubscriberPrefixCode;
	protected $TU_password;
	protected $TU_processingEnvironment;
	protected $TU_RR_pullCounter;


	const BANK_SAVINGS_INCREASE_AMT  = 50;
	const BANK_SAVINGS_PERCENTAGE_RT = 2.5;
	const BANK_SAVINGS_MAX_PERCT_RT  = 10;

	/**
	 * Initialize default fields
	 * 
	 * @return
	 */
	public function __construct()
	{
		$this->cert_file                      = Config::get('system.TU_CertificateName');
		$this->key_file                       = Config::get('system.TU_KeyName');
		$this->url                            = Config::get('system.TU_URL');
		$this->TU_systemID                    = Config::get('system.TU_SystemID');
		$this->TU_systemPassword              = Config::get('system.TU_SystemPassword');
		$this->TU_sslPassword                 = Config::get('system.TU_SystemPassword');
		$this->TU_industryCode                = Config::get('system.TU_SubscriberIndustryCode');
		$this->TU_memberCode                  = Config::get('system.TU_SubscriberMemberCode');
		$this->TU_inquirySubscriberPrefixCode = Config::get('system.TU_SubscriberPrefixCode');
		$this->TU_password                    = Config::get('system.TU_SubscriberPassword');
		$this->TU_processingEnvironment       = Config::get('system.TU_processingEnvironment');	
		$this->TU_RR_pullCounter			  = Config::get('system.TU_RR_pullCounter');		
	}

 
	/**
	 * Show Rewards Page
	 * 
	 * @param  int $loanNumber
	 * @param  int $cifno
	 * @param  int $cifNumber	
	 * @return
	 */
	public function rewards($loanNumber = 0, $cifno = 0, $cifNumber = 0)
	{		
		$userId         				= $cifNumber;
		$userRefNumber  				= $cifno;
		$loanNumber 					= $loanNumber;		
		$RRInterest     				= 0;
		$finance        				= 0;
		$reward         				= 0;
		$cdbMonth       				= 0;
		$currentMonth   				= date('M');	
		$cdbMonthsArr   				= array();
		$tccsMonthsArr  				= array();
		$qualifier      				= '<span class="red">No</span>';
		$data['qualifier'] 				= '<span class="red">No</span>';
		$data['qualifierTrigger'] 		= 0;
		$data['qualifierLimit'] 		= 3;
		$data['qualifierMonths'] = 0;
		$data['qualifierMonthsClass'] 	= 'red';
		$data['savingsSummary']         = '';
		$data['fastLinkUrl'] 			= ''; 
		$data['uid'] 					= $userId;
		$data['lid'] 					= $loanNumber;
		$data['rda']              		= 0;
		$data['adminView'] 				= 0;		
		$data['cdbSpendingColor'] 		= 0;   // CDB
		$data['cdbHit']           		= 0;
		$data['cdbPercent']       		= 0;
		$data['cdbRewardPercent'] 		= 0;
		$data['cdbRewardMonths'] 		= 0;
		$data['tccsHit']           		= 0;   // TCCS
		$data['tccsPercent']       		= 0;
		$data['tccsRewardMonths'] 		= 0;
		$data['tccsRewardPercent'] 		= 0;
		$data['tccsMonthBalance']  		= 0;
		$data['tccsSpending'] 			= 0;	
		$data['tccsMonthDiff']     		= 0;  
		$data['sabMonthReward']   		= '';   // SAB
		$data['sabMonthBalance']  		= 0;
		$data['sabMonthDiff']     		= 0;
		$data['sabHit']           		= 0;
		$data['sabPercent']       		= 0;
		$data['sabRewardPercent'] 		= 0;
		$data['finance'] 				= 0;
		$data['totalpaymentamount'] 	= 0;
		$data['atPercent'] 				= 0;    // Auto Title
		$data['atDate']					= date('Y/m/d');
		$data['cdbMonthReward']   		= date('M');
		$data['cdbRewardYear']   		= date('Y');
		$data['totalMonthlyReward'] 	= 0;
		$data['increaseSavings']  		= false;
		$data['banks']					= array();
		$data['width'] 					= '98%';
		$data['error']					= '';		
 		$next_payment_total_amount 		= 0;		
 		$totalTrigger					= 0;
		$DebugMode 						= Config::get('system.Ascend.DebugMode');

		$tuRRLists = TURateReward::where('User_Id','=',$userId)
									->where('Loan_App_Nr','=',$loanNumber)
									->orderBy('Trans_Union_Access_Dttm', 'DESC')
									->first();
		

		// Check if User Logged is an Administrator or Customer
		if(isLoggedIn()){
			$userRole = User::where('User_Id', '=', getUserSessionID())->first();
			if($userRole->User_Role_Id == 1){
				$data['adminView'] = 1;
			}
		}		
		
		try{
				
			if( count($tuRRLists) > 0 ){
				$data['tccsSpending'] = round($tuRRLists->TCCS);
				//$tuRRLists->AT36S = 1;
				// Verify qualifier
				if($tuRRLists->AT36S > $data['qualifierLimit']){
					$data['qualifier'] = '<span class="green">Yes</span>';
					$data['qualifierTrigger'] = 1;
					$data['qualifierMonthsClass'] = 'green';
				}

				$data['qualifierMonths'] = $tuRRLists->AT36S;
				//$data['qualifierMonths'] = 999;
				
				// CDB results
				$cdbRewardRecord = BorrowerRewardTrigger::where('User_Id','=',$userId)
										->where('Loan_App_Nr','=',$loanNumber)
										->where('Trigger_Id','=','CDB')
										->where('RateReward_Ver_Nr','=','1.0')
										->orderBy('Billing_Statement_Dt', 'DESC')
										->first();
				if( count($cdbRewardRecord) > 0 ){			
					$data['cdbHit']           = $cdbRewardRecord->Month_Attainment_Cnt;
					$data['cdbPercent']       = $cdbRewardRecord->Interest_Discount_Pct * 100;
					$data['cdbRewardPercent'] = $cdbRewardRecord->Interest_Discount_Pct;
					$date 					  = new DateTime($cdbRewardRecord->Billing_Statement_Dt);
					$data['cdbMonthReward']   = $date->format('M');		
		        }

		        $tucCDB = 0;
				// First TU Credit Pull
				$tuLists = TUCredit::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->orderBy('Trans_Union_Access_Dttm', 'DESC')
					->first();
				if(count($tuLists) > 0){
					$diff = $tuLists->RE33 - $tuRRLists->RE33S;
					if($diff > 50){
						$data['cdbSpendingColor'] = 'red';
						if($tuRRLists->RE33S > $tuLists->RE33){
							$diff = '+'.$diff;
						}else{
							$diff = '-'.$diff;
						}
					}else{
						$data['cdbSpendingColor'] = 'green';
						if($tuRRLists->RE33S > $tuLists->RE33){
							$diff = '+'.$diff;
						}else{
							$diff = '-'.$diff;
						}
					}

					// initial tu pull CDB
					array_push($cdbMonthsArr, array(
						'timestamp' => strtotime($tuLists->Create_Dt),
						'month' 	=> date('M', strtotime($tuLists->Create_Dt)).' '.date('Y', strtotime($tuLists->Create_Dt)),
						'balance' 	=> round($tuLists->RE33,2),
						'reduction' => '',
						'color' 	=> 'green'
					));
					$tucCDB = round($tuLists->RE33,2);
				}

				//pre($cdbMonthsArr);

				$cdbCtr = 0;
				// CDB Months
				$cdbRewardMonths = BorrowerRewardDetail::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->where('Trigger_Id','=','CDB')
					->where('RateReward_Ver_Nr','=','1.0')
					->orderBy('Billing_Statement_Dt', 'ASC')
					->get();
				$data['cdbRewardMonths'] = count($cdbRewardMonths);

				if( count($cdbRewardMonths) > 0){
					$reductionCtr = 1;
					$month = '';

					foreach($cdbRewardMonths as $cdbm){
						$cdbCtr++;
						if ($month != date('M', strtotime($cdbm->Billing_Statement_Dt))) {
							$month = date('M', strtotime($cdbm->Billing_Statement_Dt));
							if($cdbCtr == 1){
								$cdbBalance = $tucCDB - round($cdbm->Bank_Acct_Field_Val, 2);
							}else{
								$cdbBalance = round($cdbm->Bank_Acct_Field_Val, 2);
							}

							if($reductionCtr >= 2){
								$cdbBalance = $cdbBalance - round($cdbm->Bank_Acct_Field_Val, 2);
							}
							//print $cdbBalance; die;
							array_push($cdbMonthsArr, array(
								'timestamp' => strtotime($cdbm->Billing_Statement_Dt),
								'month' 	=> date('M', strtotime($cdbm->Billing_Statement_Dt)).' '.date('Y', strtotime($cdbm->Billing_Statement_Dt)),
								'balance' 	=> round($cdbm->Bank_Acct_Field_Val, 2),
								//'reduction' => ($reductionCtr>1) ? is_null($cdbm->RateReward_Trigger_Val) ? 0 : $cdbBalance : '',
								'reduction' => round($cdbm->RateReward_Trigger_Val, 2),
								'color' 	=> ($cdbm->RateReward_Trigger_Val >= 0) ? 'green' : 'red'
							));
							$reductionCtr++;
						}
					}
				}		

				$cdbMonthsArr = array_reverse($cdbMonthsArr);
				
				$data['cdbMonthsArr'] = $cdbMonthsArr;

				// TCCS results						
				$tccsRewardRecord = BorrowerRewardTrigger::where('User_Id','=',$userId)
										->where('Loan_App_Nr','=',$loanNumber)
										->where('Trigger_Id','=','TCCS')
										->where('RateReward_Ver_Nr','=','1.0')
										->orderBy('Billing_Statement_Dt', 'DESC')
										->first();
				if( count($tccsRewardRecord) > 0 ){ 
					$data['tccsHit']           = $tccsRewardRecord->Month_Attainment_Cnt;
					$data['tccsPercent']       = $tccsRewardRecord->Interest_Discount_Pct * 100;
					$data['tccsRewardPercent'] = $tccsRewardRecord->Interest_Discount_Pct;
		        	$date 					= new DateTime($tccsRewardRecord->Billing_Statement_Dt);
					$data['tccsMonthReward'] = $date->format('M');
					$data['tccsMonthBalance'] = $tuRRLists->TCCS;
					$data['tccsMonthDiff'] = $tccsRewardRecord->Interest_Discount_Amt;					
		        }

		        // TCCS Months
		        $tccsRewardMonths = BorrowerRewardDetail::where('User_Id','=',$userId)
										->where('Loan_App_Nr','=',$loanNumber)
										->where('Trigger_Id','=','TCCS')
										->where('RateReward_Ver_Nr','=','1.0')
										->orderBy('Billing_Statement_Dt', 'DESC')
										->get();
				$data['tccsRewardMonths'] = count($tccsRewardMonths);	
				if( count($tccsRewardMonths) > 0){
					foreach($tccsRewardMonths as $tccsm){
						$credit = is_null($tccsm->RateReward_Trigger_Val) ? '' : (round($tccsm->RateReward_Trigger_Val,2) < 50) ? 'pass' : 'fail';
						array_push($tccsMonthsArr, array(
							'month' 	=> date('M', strtotime($tccsm->Billing_Statement_Dt)).' '.date('Y', strtotime($tccsm->Billing_Statement_Dt)),
							'credit' 	=> $credit,
							'color' 	=> ($credit == 'pass') ? 'green' : 'red'
						));
					}
					$data['tccsMonthsArr'] = $tccsMonthsArr;
				}


				//Get Loan Application Detail
		 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
										->where('Loan_Id','=',$loanNumber)
										->first();
		
				// Finance Charge
		        $rewardCalculationResults = BorrowerRewardCalculation::where('User_Id','=',$userId) 
										->where('Loan_App_Nr','=',$loanNumber)
										->where('RateReward_Ver_Nr','=','1.0')
										->orderBy('Billing_Statement_Dt', 'DESC')
										->first();	
			 	$data['finance'] =  number_format((float)$rewardCalculationResults->Finance_Charge_Amt,2,'.','');		

				// Get Auto Title
		    	$autoTitle = RewardAutoCollateral::where('Borrower_User_Id', '=', $userId)->first();
				
				if(count($autoTitle) > 0){
					$data['atPercent'] = 0.20;
					$data['atDate']    = $autoTitle->Pledge_Dt;
				}	

				// Get Total Monthly Reward
				if($tuRRLists->AT36S > 3){
					$data['totalMonthlyReward'] = $data['cdbPercent'] + $data['tccsPercent'] + $data['sabPercent'];
				}

				// FastLink kuno
				//Bank Account Validation			 	
			 	$isBankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanNumber);
			 	if(  $isBankAcctLinked == 'Y' ) {
					$data['increaseSavings'] = true;
					$data['savingsSummary']  = $this->checkBankSavings($userId, $loanNumber);
				}

				// Get Total Monthly Reward
				if($tuRRLists->AT36S > 3 ){	
					
					$savingsSummaryrewardPrct = 0; 

					if( isset($data['savingsSummary']['rewardPrct']) ){
						$savingsSummaryrewardPrct = $data['savingsSummary']['rewardPrct'];
					}

					// Get Reward Amount
					$totalTrigger = $data['cdbPercent'] + $data['tccsPercent'] + $data['sabPercent'] + $savingsSummaryrewardPrct;
					$data['rda'] = round((($totalTrigger / 100) * $data['finance']), 2);
				}
				
				$this->setUserId($userId);

				//Each Reload Needs to generate new Fast Link Url
				$Yodlee = new Yodlee();
				$Yodlee->createYodleeAccount();
				$Yodlee->generateFastLinkUrl();			

				if( Session::get('fastLinkUrl') ) 
					$data['fastLinkUrl'] = Session::get('fastLinkUrl');
				else
					$data['error'] = 'Error on generating Fast Link Form.';

				//Set Session of Loan Application Number
				Session::set('loanAppNr', $loanNumber );
				//Set Session of User Id
				Session::set('uid', $userId );

			}
			

		} catch( Exception $e ){

			//pre($e->getMessage());
			writeLogEvent('Ratereward reward page', 
					array( 
						'User_Id'     => $userId,
						'Loan_App_Nr' => $loanNumber,
						'Message'     => $e->getMessage()
					),
					'info'
				);
		}

		// _pre($data);
		// dd();
		$this->layout->content = View::make('raterewards.index' , $data );
	}


	/**
	 * Set User Id
	 * 
	 * @param integer $loanAppNr
	 */
	public function setUserId($userId)
	{
		Session::put('uid', $userId ); 
	}

	/**
	 * Get User Id
	 * @return [type]
	 */
	protected function getUserId()
	{
		return Session::get('uid');
	}

	/**
	 * Get Loan App Number
	 * 
	 * @return integer
	 */
	protected function getLoanAppNr()
	{
		return Session::get('loanAppNr');
	}

	/**
	 * Set Loan Application Number
	 * 
	 * @param integer $loanAppNr
	 */
	public function setLoanAppNr($loanAppNr)
	{
		Session::put('loanAppNr', $loanAppNr );
	}

	/**
	 * Get Bank Account Summary View
	 * 
	 * @return
	 */
	protected function getBankAcctSummaryView()
	{

		$this->layout = NULL;

		$userId     = Session::get('uid');
		$loanNumber = Session::get('loanAppNr');

		//Bank Account Validation
	 	$data['increaseSavings']  = false;

	 	$isBankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanNumber);
	  
		if( $isBankAcctLinked == 'Y' ) {
			$data['increaseSavings'] = true;
			$data['savingsSummary']  = $this->checkBankSavings($userId, $loanNumber);
		}

		$html = array(
			'summary' => View::make('raterewards.banks.bank-summary' , $data )->render(), 
			'rewards' => View::make('raterewards.banks.banks-rewards' , $data )->render()
		);
		 
		return Response::json($html);
	}

	/**
	 * Check Bank Savings 
	 * 	
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return array
	 */
	protected function checkBankSavings( $userId, $loanAppNr, $acctType = 'checking')
	{ 
		$result =  array(
			'balance'    => array(), 
			'rewardPrct' => 0,
			'targetCnt'  => 0
		);

		//Get Bank Account History
		$bankAccount = BankAccountDetail::getBankAccountHistory($userId, $loanAppNr, $acctType);

		if( count($bankAccount) > 0 ) {
			
			$account  = array();

			$previousBalAmt = 0;
			$hittingTarget  = 0;

			foreach ($bankAccount as $key => $value) {
 			 
				//Set the increase to zero if it is first PULL 
				if( $key == 0 ) {
					$increase = 0;
				} else {
					$increase = $value->Current_Bal_Amt - $previousBalAmt;
				}

				//set the previous balance
				$previousBalAmt = $value->Current_Bal_Amt;

				$account[$value->Yodlee_Access_Dt]['increaseAmt'] = number_format($increase, 2);
				$account[$value->Yodlee_Access_Dt]['balanceAmt']  = number_format($value->Current_Bal_Amt, 2 );
				$account[$value->Yodlee_Access_Dt]['monthYr']     = date('M Y', strtotime($value->Yodlee_Access_Dt));
				$account[$value->Yodlee_Access_Dt]['qualified']   = false;

				if( $increase > self::BANK_SAVINGS_INCREASE_AMT ) {
					$account[$value->Yodlee_Access_Dt]['qualified'] = true;
					$hittingTarget++;
				}
 				
			} 

			//Calculate the percentage rate
			$rewardPercentage = self::BANK_SAVINGS_PERCENTAGE_RT * $hittingTarget;

			//Check if reward percentage is greater than the maximum set 
			if( $rewardPercentage > self::BANK_SAVINGS_MAX_PERCT_RT ) 
				$rewardPercentage  = self::BANK_SAVINGS_MAX_PERCT_RT;

			$result =  array(
				'balance' 		=> $account, 
				'rewardPrct' 	=> $rewardPercentage, 
				'targetCnt'     => $hittingTarget
			);

		}

		return  $result; 
	}


	public function cron(){
		$userId    = getUserSessionID();
		$loanAppNr = getLoanAppNr();
		$loanAcctResults = DB::connection('nls')->select("SELECT TOP 1 acctrefno, cifno, loan_number,next_statement1_date FROM loanacct where loan_number='".$loanAppNr."'");
            
	}

	public function getAutoTitle()
	{
		
		$AT_percent = 0;
		
		if( count($autoTitle) > 0 ){
			$AT_percent = 0.20;
		}

		return $AT_percent;
	}

	public function getRRTest($loanNumber = 1413, $userId = 578)
	{
		$fields = Input::all();
		//pre($fields); 
		$cdb = 0;
		$tccs = 0;
		$sab = 0;
		$op = 0;
		$at = 0;
		$rdr = 0;
		$rda = 0;
		$rrInterest = 0;
		$cdbNewMonthlyAttainmentCnt = 0;
		$tccsNewMonthlyAttainmentCnt = 0;
		$sabNewMonthlyAttainmentCnt = 0;
		$cdbDiscountPct = 0;
		$cdbDiscountAmt = 0;
		$flag = 'pass';
		$RRInterest = 0;	
		$next_payment_total_amount = 0;	
		try{
			// get On time payment
			if( $fields['otp1'] > 3){
				$op = 1;
				// compute for CDB
				$cdbDifference = $fields['cdb0'] - $fields['cdb1'];
				if($cdbDifference > 50){
					// get CDB Reward Records
					$rewardRecord = RewardCalculation::where('User_Id','=',$userId)
						->where('Trigger_Id','=','CDB')
						->where('Reward_Ver_Nr','=','1.0')
						->orderBy('Billing_Statement_Dt', 'DESC')
						->first();
					if(count($rewardRecord)>0){
						$cdbNewMonthlyAttainmentCnt = $rewardRecord->Month_Attainment_Cnt + 1;
					}else{
						$cdbNewMonthlyAttainmentCnt = 1;
					}				
					//$cdb  = $cdbNewMonthlyAttainmentCnt * 0.025; // 2.5%;
					if($cdbNewMonthlyAttainmentCnt>=4){
						$cdb  = 0.1; // 10%
					}else{
						$cdb  = $cdbNewMonthlyAttainmentCnt * 0.025; // 2.5%;
					}	
					$cdbDiscountAmt = $fields['cdb1'] * ($cdb);
					// Save CDB to ODS
					$reward = new RewardCalculation();
			        $reward->User_Id   = $userId;
			        $reward->Reward_Ver_Nr            = '1.0';
			        $reward->Trigger_Id  = 'CDB';
			        $reward->Billing_Statement_Dt        = DB::raw("CAST(GETDATE() AS DATE)"); 	
			        $reward->Mktg_Program_Cd   = '01';
			        $reward->Month_Attainment_Cnt        = $cdbNewMonthlyAttainmentCnt;
			        $reward->Interest_Discount_Pct     = $cdb;
			        $reward->Interest_Discount_Amt     = $cdbDiscountAmt;
			        $reward->Notes_Txt     = strval($fields['cdb1']);
			        $reward->Created_By_User_Id = $userId;
			        $reward->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 				            
			        $reward->save();  
				}else{
					// Save CDB to ODS
					$reward = new RewardCalculation();
			        $reward->User_Id   = $userId;
			        $reward->Reward_Ver_Nr            = '1.0';
			        $reward->Trigger_Id  = 'CDB';
			        $reward->Billing_Statement_Dt        = DB::raw("CAST(GETDATE() AS DATE)"); 	
			        $reward->Mktg_Program_Cd   = '01';
			        $reward->Month_Attainment_Cnt        = $cdbNewMonthlyAttainmentCnt;
			        $reward->Interest_Discount_Pct     = $cdbDiscountPct;
			        $reward->Interest_Discount_Amt     = $cdbDiscountAmt;
			        $reward->Created_By_User_Id = $userId;
			        $reward->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 				            
			        $reward->save();  
				}

				// compute for TCCS			
				if($fields['tccs1'] < 50){

					// get CDB Reward Records
					$rewardRecord = RewardCalculation::where('User_Id','=',$userId)
						->where('Trigger_Id','=','TCCS')
						->where('Reward_Ver_Nr','=','1.0')
						->orderBy('Billing_Statement_Dt', 'DESC')
						->first();
				    if(count($rewardRecord)>0){
						$tccsNewMonthlyAttainmentCnt = $rewardRecord->Month_Attainment_Cnt + 1;
					}else{
						$tccsNewMonthlyAttainmentCnt = 1;
					}	
					if($tccsNewMonthlyAttainmentCnt>=4){				
						$tccs  = 0.1; // 10%
						//print $tccs.'==1';
					}else{
						$tccs  = $tccsNewMonthlyAttainmentCnt * 0.025; // 2.5%;
						//print $tccs.'==2';
					}	
					$tccsDiscountAmt = $fields['tccs1'] * $tccs;
					
					// Save TCCS to ODS
					$reward = new RewardCalculation();
		            $reward->User_Id   = $userId;
		            $reward->Reward_Ver_Nr            = '1.0';
		            $reward->Trigger_Id  = 'TCCS';
		            $reward->Billing_Statement_Dt        = DB::raw("CAST(GETDATE() AS DATE)"); 
		            $reward->Mktg_Program_Cd   = '01';
		            $reward->Month_Attainment_Cnt      = $tccsNewMonthlyAttainmentCnt;
		            $reward->Interest_Discount_Pct     = $tccs;
		            $reward->Interest_Discount_Amt     = $tccsDiscountAmt;
		            $reward->Created_By_User_Id = $userId;
		            $reward->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 
		            $reward->save();   
				}else{
					// Save TCCS to ODS
					$reward = new RewardCalculation();
			        $reward->User_Id   = $userId;
			        $reward->Reward_Ver_Nr            = '1.0';
			        $reward->Trigger_Id  = 'TCCS';
			        $reward->Billing_Statement_Dt        = DB::raw("CAST(GETDATE() AS DATE)"); 	
			        $reward->Mktg_Program_Cd   = '01';
			        $reward->Month_Attainment_Cnt        = $tccsNewMonthlyAttainmentCnt;
			        $reward->Interest_Discount_Pct     = 0;
			        $reward->Interest_Discount_Amt     = 0;
			        $reward->Created_By_User_Id = $userId;
			        $reward->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 				            
			        $reward->save();  
				}

				// compute for SAB
				$sabDifference = $sab['sab0'] - $sab['sab1'];
				if($sabDifference > 50){
					// get SAB Reward Records
					$rewardRecord = RewardCalculation::where('User_Id','=',$userId)
						->where('Trigger_Id','=','SAB')
						->where('Reward_Ver_Nr','=','1.0')
						->orderBy('Billing_Statement_Dt', 'DESC')
						->first();
					if(count($rewardRecord)>0){
						$sabNewMonthlyAttainmentCnt = $rewardRecord->Month_Attainment_Cnt + 1;
					}else{
						$sabNewMonthlyAttainmentCnt = 1;
					}	
					//$sabNewMonthlyAttainmentCnt = $rewardRecord->Month_Attainment_Cnt++;
					//$sab  = $sabNewMonthlyAttainmentCnt * 0.025; // 2.5%;
					if($sabNewMonthlyAttainmentCnt>=4){
						$sab  = 0.1; // 10%
					}else{
						$sab  = $sabNewMonthlyAttainmentCnt * 0.025; // 2.5%;				
					}	
					$sabDiscountAmt = $fields['sab1'] * ($sab);
					// Save SAB to ODS
					$reward = new RewardCalculation();
		            $reward->User_Id   = $userId;
		            $reward->Reward_Ver_Nr            = '1.0';
		            $reward->Trigger_Id  = 'SAB';
		            $reward->Billing_Statement_Dt        = DB::raw("CAST(GETDATE() AS DATE)"); 
		            $reward->Mktg_Program_Cd   = '01';
		            $reward->Month_Attainment_Cnt      = $sabNewMonthlyAttainmentCnt;
		            $reward->Interest_Discount_Pct     = $sab;
		            $reward->Interest_Discount_Amt     = $sabDiscountAmt;
		            $reward->Created_By_User_Id = $userId;
		            $reward->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 
		            $reward->save();   
				}else{
					// Save SAB to ODS
					$reward = new RewardCalculation();
			        $reward->User_Id   = $userId;
			        $reward->Reward_Ver_Nr            = '1.0';
			        $reward->Trigger_Id  = 'SAB';
			        $reward->Billing_Statement_Dt        = DB::raw("CAST(GETDATE() AS DATE)"); 	
			        $reward->Mktg_Program_Cd   = '01';
			        $reward->Month_Attainment_Cnt        = $sabNewMonthlyAttainmentCnt;
			        $reward->Interest_Discount_Pct     = 0;
			        $reward->Interest_Discount_Amt     = 0;
			        $reward->Created_By_User_Id = $userId;
			        $reward->Create_Dt          = DB::raw("CAST(GETDATE() AS DATE)"); 				            
			        $reward->save();  
				}
				
			}else{
				$flag = 'fail';
			}		

			// get auto title
			if( $fields['at'] ){
				$at = 0.20;
			}

			$rdr = (($cdb + $sab + $tccs) * ($op)) + $at;

			$rda = $rdr * $fields['mir'];

			// // Update NLS LoanDetail2 and Update Payment Amount

			$whereParam = array('User_Id' => $userId);	

			$applicant = new Applicant();
			$applicant_fields = array(
				'First_Name',
				'Middle_Name',
				'Last_Name',
				'Birth_Dt',
				'Social_Security_Nr'
			);
			
			$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

			$applicantAddress = new ApplicantAddress();
			$applicantAddress_fields = array(
				'Street_Addr_1_Txt',
				'City_Name',
				'State_Cd',
				'Zip_Cd'
			);
			$addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);

			// Update RDR and RDA
			RewardCalculation::where('User_Id', '=', $userId)
				->orwhere('Trigger_Id', '=', 'CDB')
				->orwhere('Trigger_Id', '=', 'SAB')
				->orwhere('Trigger_Id', '=', 'TCCS')
				->where('Reward_Ver_Nr', '=', '1.0')
				->where('Mktg_Program_Cd','=','1.0')
				->where('Create_Dt','=',DB::raw("CAST(GETDATE() AS DATE)"))
				->update(
				array(
					'Total_RDR_Val'  => $rdr,
					'Calculated_RDA_Val'    => $rda
				)
			); 

		} catch( Exception $e ){

			writeLogEvent('Ratereward harness calculation - getRRTest', 
					array( 
						'User_Id'     => $userId,
						'Loan_App_Nr' => $loanNumber,
						'Message'     => $e->getMessage()
					),
					'info'
				);
		}		

		return Response::json(
				array(
					'rdr'  => round($rdr, 3),
					'rda' => round($rda, 3),
					'cdb' => round($cdb, 3),
					'sab' => round($sab, 3),
					'tccs' => round($tccs, 3),
					'op' => round($op, 3),
					'at' => round($at, 3),
					'interest' => $fields['mir'],
					'flag' => $flag
				), 200
			);
	}

	public function tuRRTest()
	{
		$this->layout = null;	  	
		return View::make('blocks.harness.rateRewardTest');
	}

	public function rrTUPull($loanNumber = 1413, $userId = 578){

		$tuLists = 0;
		$cdb0 = 0;
		$cdb1 = 0;
		$sab0 = 0;
		$sab1 = 0;
		$tccs0 = 0;
		$tccs1 = 0;
		$otp0 = 0;
		$otp1 = 0;
		$at = 0;
		$monthlyInterestRate = 0;
		$result = 'Fail';

		/* TransUnion credentials  */
		$tuData = array(
			'cert_file' => $this->cert_file,
			'key_file' => $this->key_file,
			'url' => $this->url,
			'TU_systemID' => $this->TU_systemID,
			'TU_systemPassword' => $this->TU_systemPassword,
			'TU_sslPassword' => $this->TU_systemPassword,
			'TU_industryCode' => $this->TU_industryCode,
			'TU_memberCode' => $this->TU_memberCode,
			'TU_inquirySubscriberPrefixCode' => $this->TU_inquirySubscriberPrefixCode,
			'TU_password' => $this->TU_password,
			'TU_processingEnvironment' => $this->TU_processingEnvironment
		);

		try{

			$whereParam = array('User_Id' => $userId);	
			$applicant = new Applicant();
			$applicant_fields = array(
				'First_Name',
				'Middle_Name',
				'Last_Name',
				'Birth_Dt',
				'Social_Security_Nr'
			);
			
			$applicantData = $applicant->getData($applicant_fields,$whereParam, true );


			$whereParam = array('User_Id' => $userId);	
			$applicant = new Applicant();
			$applicant_fields = array(
				'First_Name',
				'Middle_Name',
				'Last_Name',
				'Birth_Dt',
				'Social_Security_Nr'
			);
			
			$applicantData = $applicant->getData($applicant_fields,$whereParam, true );


			$applicantAddress = new ApplicantAddress();
			$applicantAddress_fields = array(
				'Street_Addr_1_Txt',
				'City_Name',
				'State_Cd',
				'Zip_Cd'
			);
			$addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);
	    	
			$tuData['TU_lastName']			= strtoupper($applicantData->Last_Name);
			$tuData['TU_middleName']		= strtoupper($applicantData->Middle_Name);
			$tuData['TU_firstName']			= strtoupper($applicantData->First_Name);
			$tuData['TU_birthDate']			= $applicantData->Birth_Dt;
			$tuData['TU_ssn']				= $applicantData->Social_Security_Nr;
			$tuData['TU_street']			= $addressData->Street_Addr_1_Txt;
			$tuData['TU_city']				= $addressData->City_Name;
			$tuData['TU_state']				= $addressData->State_Cd;
			$tuData['TU_zipCode']			= $addressData->Zip_Cd;
			$TUResponse = $this->prepareTURequest($tuData);
			//pre($TUResponse); die;
			$parseBureauObject = simplexml_load_string($TUResponse);
		    //pre($parseBureauObject); die;
		    $tuAttributesArr = array();
		    //pre($parseBureauObject); die;

		    if(property_exists($parseBureauObject->product,'error')){
		    	$result = $parseBureauObject->product->error->description->{0};
		    }else{

				if($TUResponse!='error'){
				
					$result = 'Pass';
					// RR Pull results
					$tuRRLists = TURateReward::where('User_Id','=',$userId)
											->where('Loan_App_Nr','=',$loanNumber)
											->orderBy('Trans_Union_Access_Dttm', 'DESC')
											->first();

					if( count($tuRRLists) > 0){
						$cdb0 = $tuRRLists->RE33S;
						$otp0 = $tuRRLists->AT36S;	
						$tccs0 = $tuRRLists->TCCS;	
					}else{
						$tuLists = TUCredit::where('User_Id','=',$userId)
											->where('Loan_App_Nr','=',$loanNumber)
											->orderBy('Trans_Union_Access_Dttm', 'DESC')
											->first();
						if(count($tuLists) > 0){					
							$cdb0 = $tuLists->RE33;
							$otp0 = $tuLists->AT36;			
						}	


						$bankData = BankAccountDetail::where('User_Id','=',$userId)->where('Loan_App_Nr','=',$loanNumber)->get();
				        if(count($bankData)>0){
				        	$sab0 = $bankData[0]->Current_Bal_Amt;
				        }
					}
					//pre($TUResponse); die;
					// RR Pull XML results
					$pullResult = $this->processTUResponse($TUResponse,$userId, $loanNumber);

					if(is_array($pullResult)){
						$cdb1 = $pullResult[30]; // RE33S
						$tccs1 = $pullResult[40]; // TCCS
						$otp1 = $pullResult[4]; // AT36S				
					}				

				}else{
					print 'No Ratereward TU Pull Data';
				}
			}

			//Get Loan Application Detail
	 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
									->where('Loan_Id','=',$loanNumber)
									->first();

		    if(count($loanDetail) > 0){
				// Get Interest Rate
				$loanacct_payment = DB::connection('nls')->select("SELECT next_interest_due_amount FROM loanacct_payment where acctrefno = ".$loanDetail->Loan_Ref_Nr."");
			    
			    if(count($loanacct_payment) > 0){
			    	$monthlyInterestRate = $loanacct_payment[0]->next_interest_due_amount;
			    }	
		    }		

		} catch( Exception $e ){

			writeLogEvent('Ratereward harness pull TU - rrTUPull', 
					array( 
						'User_Id'     => $userId,
						'Loan_App_Nr' => $loanNumber,
						'Message'     => $e->getMessage()
					),
					'info'
				);
		}	

		return Response::json(
				array(
					'cdb0'  => $cdb0,
					'cdb1' => $cdb1,
					'sab0'  => $sab0,
					'sab1' => $sab1,
					'tccs0' => $tccs0,
					'tccs1' => $tccs1,
					'otp0' => $otp0,
					'otp1' => $otp1,
					'at' => $at,
					'mir' => $monthlyInterestRate,
					'result' => $result
				), 200
			);

	}

	/**
	 * Reward Process
	 * 
	 * @param  int $uid
	 * @param  int $lid 
	 * @param  int $test 
	 * @return 
	 */
	public function process($uid=0, $lid=0, $test=0){

		$CDB          = 0;
		$SAB          = 0;
		$TCCS         = 0;
		$OTP          = 0; /* On Time Payment */
		$AT           = 0; /* Auto Title */
		$RDR          = 0;
		$RDA          = 0;
		$RRInterest   = 0;
		$CDB_percent  = 0;
		$SAB_percent  = 0;
		$TCCS_percent = 0;
		$AT_percent   = 0;
		$next_payment_total_amount = 0;
		$flag 		  = 'pass';
		$NLS 		  = new NLS();
		
		/* TransUnion credentials  */
		$tuData = array(
			'cert_file'                      => $this->cert_file,
			'key_file'                       => $this->key_file,
			'url'                            => $this->url,
			'TU_systemID'                    => $this->TU_systemID,
			'TU_systemPassword'              => $this->TU_systemPassword,
			'TU_sslPassword'                 => $this->TU_systemPassword,
			'TU_industryCode'                => $this->TU_industryCode,
			'TU_memberCode'                  => $this->TU_memberCode,
			'TU_inquirySubscriberPrefixCode' => $this->TU_inquirySubscriberPrefixCode,
			'TU_password'                    => $this->TU_password,			
			'TU_processingEnvironment'       => $this->TU_processingEnvironment
		);
		
		$body_contents = '';


        // check NLS table to get the next_statement1_date 
        $loanAcctResults = DB::connection('nls')->select("SELECT acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where DATEADD(DAY, -1, CAST(next_statement1_date AS DATE)) = CAST(GETDATE() AS DATE) and portfolio_code_id = 1 and loan_group_no IN (SELECT loan_group_no FROM loan_group WHERE loan_group LIKE '%-RR') ");
        //$loanAcctResults = DB::connection('nls')->select("SELECT acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where CAST(next_statement1_date AS DATE) = '2015-11-09' and portfolio_code_id = 1 and loan_group_no IN (SELECT loan_group_no FROM loan_group WHERE loan_group LIKE '%-RR') ");
        //$loanAcctResults = DB::connection('nls')->select("SELECT acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where DATEADD(MONTH, -1, CAST(next_statement1_date AS DATE)) = '2015-10-22' and loan_number='539' and portfolio_code_id = 1 and loan_group_no IN (SELECT loan_group_no FROM loan_group WHERE loan_group LIKE '%-RR'); ");
    	//$loanAcctResults = DB::connection('nls')->select("SELECT top 4 acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where DATEADD(MONTH, -1, CAST(next_statement1_date AS DATE)) < '2015-10-25' and portfolio_code_id = 1 and loan_group_no IN (SELECT loan_group_no FROM loan_group WHERE loan_group LIKE '%-RR') order by loan_number ");
    	
    	$body_contents .= 'Number of Loans: '.count($loanAcctResults);
    	
    	$body_contents.= '<ul>';

        if(count($loanAcctResults) > 0){

			foreach($loanAcctResults as $loan){

				// Saving of Loans from NLS 
				$user = User::where('User_Ref_Nr','=',$loan->cifno)->first();			
				$nlsTracking = new NLSLoanTracking();
		        $nlsTracking->User_Id              = $user->User_Id;
		        $nlsTracking->Loan_App_Nr          = $loan->loan_number;
		        $nlsTracking->Billing_Statement_Dt = $loan->next_statement1_date;
		        $nlsTracking->Created_By_User_Id = $user->User_Id;
		        $nlsTracking->Create_Dt          = date('Y-m-d');
		        $nlsTracking->save();

        		// 1. Pull credit using RR_ACCOUNT_REVIEW header
	        	
	        	$body_contents.= '
		        	<li class="reward" id="'.$loan->loan_number.'"><strong>Loan Number</strong>: '.$loan->loan_number.' ||
		        	<strong>Contact Reference</strong>: '.$loan->cifno.' ||';	
		        //$user = User::where('User_Ref_Nr','=',$loan->cifno)->first();
	        	$body_contents.= '<strong>User ID</strong>: '.$user['User_Id'].'
	        	<div class="rwd reward'.$loan->loan_number.'">
	        	';	

	        	if(count($user) > 0){	
	        		try
		        		{
				        	$whereParam = array('User_Id' => $user->User_Id);	

							$applicant = new Applicant();
							$applicant_fields = array(
								'First_Name',
								'Middle_Name',
								'Last_Name',
								'Birth_Dt',
								'Social_Security_Nr'
							);
							
							$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

							$applicantAddress = new ApplicantAddress();
							$applicantAddress_fields = array(
								'Street_Addr_1_Txt',
								'City_Name',
								'State_Cd',
								'Zip_Cd'
							);

							$addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);
				        	    	
							$tuData['TU_lastName']			= strtoupper($applicantData->Last_Name);
							$tuData['TU_middleName']		= strtoupper($applicantData->Middle_Name);
							$tuData['TU_firstName']			= strtoupper($applicantData->First_Name);
							$tuData['TU_birthDate']			= $applicantData->Birth_Dt;
							$tuData['TU_ssn']				= $applicantData->Social_Security_Nr;
							$tuData['TU_street']			= $addressData->Street_Addr_1_Txt;
							$tuData['TU_city']				= $addressData->City_Name;
							$tuData['TU_state']				= $addressData->State_Cd;
							$tuData['TU_zipCode']			= $addressData->Zip_Cd;
							
							// Pull Credit Report from TU
							$TUResponse = $this->prepareTURequest($tuData);
							$TUXML = $this->processTUResponse($TUResponse,$user->User_Id,$loan->loan_number);
							//print '<br/>TU Pull<br/>';

							// Update NLS standard payment with Monthly Payment from Loan Detail - 08/20/15 1:59 PM
							//Get Loan Application Detail
					 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $user->User_Id)
													->where('Loan_Id','=',$loan->loan_number)
													->first();
		       			    $paymentAmount = $loanDetail->Monthly_Payment_Amt;	
		       			    // Get Loan Template
							$LoanTemplate = LoanTemplate::find( $loanDetail->Loan_Template_Id );		
							$paymentfields = array(
								'nlsLoanTemplate'      => $LoanTemplate->Loan_Template_Name,
								'contactId'            => $user->User_Id,
								'loanId'               => $loan->loan_number,
								'firstName'            => $applicantData->First_Name,
								'lastName'             => $applicantData->Last_Name,
								'paymentAmount'        => $paymentAmount // update Payment Amount
							);		 	  	
							$NLS->nlsUpdateLoan($paymentfields);
							print '<br/>Done Update to Original Monthly Payment<br/>';


						} catch( Exception $e ){

							writeLogEvent('TU Pull',
									array( 
										'User_Id'     => $user->User_Id,
										'Loan_App_Nr' => $loan->loan_number,
										'Message'     => $e->getMessage()
									),
									'info'
								);
						}

						$body_contents .=  '<br /><strong>Pulling Yodlee Information</strong><br />';

						//Yodlee Pull Information
						$this->pullBankTransactions( $user->User_Id, $loan->loan_number );
						print '<br/>Yodlee Pull<br/>';
					
					}else{
						 $body_contents .='<br />'.$loan->cifno.' does not exist on UserAcct';
					}	
				//}
				$body_contents.= '</div></li>';	

	        }

	    }

	    print '<br/>After Pull<br/>';
		$ctr = 0;
	    while ( $ctr <= $this->TU_RR_pullCounter)  {
            $tuRRQuery   = 'SELECT TOP 1 * FROM TU_Rate_Reward_Attribute WHERE CAST(Trans_Union_Access_Dttm AS DATE) = CAST(GETDATE() AS DATE)' ;
            $tuRRResults = DB::connection('ascend')->select( $tuRRQuery );
            if( sizeOf( $tuRRResults ) > 0 )            	
                break;
            $ctr++;
        }    
        print '<br/>before SP<br/>';
	    if(count($loanAcctResults) > 0){
	    	try{
			    // RUN reward SP
				$rewardResults = DB::select("EXEC ODS.dbo.usp_RateRewards"); 
				pre($rewardResults);	
			} catch( Exception $e ){
				writeLogEvent('RR SP Issue', 
						array( 
							'User_Id'     => $user->User_Id,
							'Loan_App_Nr' => $loan->loan_number,
							'Message'     => $e->getMessage()
						),
						'info'
					);
			}		
		}
		print '<br/>SP Run<br/>';
		//Update NLS
		if(count($loanAcctResults) > 0){ 
        	
	        foreach($loanAcctResults as $loan){
	        	$user = User::where('User_Ref_Nr','=',$loan->cifno)->first();
	        	if(count($user) > 0){	
	        		try{
	        			
	        			$whereParam = array('User_Id' => $user->User_Id);	

						$applicant = new Applicant();
						$applicant_fields = array(
							'First_Name',
							'Middle_Name',
							'Last_Name',
							'Birth_Dt',
							'Social_Security_Nr'
						);
						
						$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

	        			//Get Borrower Reward Calculation
				 	  	$rewardCalculation = BorrowerRewardCalculation::where('User_Id', '=', $user->User_Id)
				 	  							->orderBy('Billing_Statement_Dt', 'DESC')
												->first();
						if( count($rewardCalculation) > 0 ){
							$RDA = $rewardCalculation->Calculated_RDA_Val;
						}		

						//Get Loan Application Detail
				 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $user->User_Id)
												->where('Loan_Id','=',$loan->loan_number)
												->first();
						if(count($loanDetail)>0){
							//Get Loan Template
							$LoanTemplate = LoanTemplate::find( $loanDetail->Loan_Template_Id );
							//Get Loan Group
							$loanGroup = LoanGroup::find( $loanDetail->Loan_Grp_Id );
							//Get Loan Portfolio
							$loanPortfolio = LoanPortfolio::find( $loanDetail->Loan_Portfolio_Id );
							//Get Monthly Payment Amount from NLS
							$loanacct_payment = DB::connection('nls')->select("SELECT next_payment_total_amount FROM loanacct_payment where acctrefno = ".$loanDetail->Loan_Ref_Nr."");
						   	if(count($loanacct_payment) > 0){
						   		$next_payment_total_amount = $loanacct_payment[0]->next_payment_total_amount;
						   	}

							$paymentAmount = 0;
						   	if($RDA > 0 ){
						   		$paymentAmount = $next_payment_total_amount - $RDA;
						   	}else{
						   		$paymentAmount = $loanDetail->Monthly_Payment_Amt;
						   	}

				 	  		$rewardfields = array(
								'nlsLoanTemplate'      => $LoanTemplate->Loan_Template_Name,
								'contactId'            => $user->User_Id,
								'loanId'               => $loan->loan_number,
								'firstName'            => $applicantData->First_Name,
								'lastName'             => $applicantData->Last_Name,
								'paymentAmount'        => $paymentAmount // update Payment Amount
							);
					 	  	$NLS->nlsUpdateLoan($rewardfields);
							print '<br/>NLS Update Payment Amount<br/>';
						}

						if($RDA > 0){			
							// Update Transaction NLS
							$transFields = array(
								'TransactionCode' => 920,
								'LoanNumber' => $loan->loan_number,
								'Amount' => $RDA
								);
							$NLS->createTransaction($transFields);
							print '<br/>NLS Create Transaction<br/>';
						}
        			} catch( Exception $e ){

						writeLogEvent('RR Update NLS', 
								array( 
									'User_Id'     => $user->User_Id,
									'Loan_App_Nr' => $loan->loan_number,
									'Message'     => $e->getMessage()
								),
								'info'
							);
					}	
	        	}else{
	        		print '<br/>No User<br/>';
	        	}
	        }
	    }

	    $body_contents.= '</ul>';
	    $data['body_contents'] = $body_contents;
	    $this->layout->content = View::make('blocks.harness.rateRewardHarness', $data);

	}


	/**
	 * Pull Bank Transactions
	 * 
	 * @param  integer $userId
	 * @return 
	 */
	public function pullBankTransactions( $userId, $loanAppNr )
	{
		$yodlee = new Yodlee();

		$isBankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanAppNr);

		if( $isBankAcctLinked == 'Y' ) {

			//Set User Id and Loan Application Number into Session
			$this->setUserId( $userId );
			$this->setLoanAppNr($loanAppNr);

			$loginAccount = $yodlee->createYodleeAccount();

			if( isset($loginAccount['result']) && $loginAccount['result'] == 'success') { 

				//Get the items summary of the account
				//and get the itemid that has container type of bank
				$itemsSummary = $yodlee->getItemSummariesWithoutItemData();

				$memSiteAccId = NULL;
				$itemId       = NULL;
				$siteId 	  = NULL;

				if( isset($itemsSummary['Body']) && count($itemsSummary['Body']) > 0 ) { 
					foreach ($itemsSummary['Body'] as $key => $value) {
						if( isset($value->contentServiceInfo)
							&& $value->contentServiceInfo->containerInfo->containerName == 'bank') { 
							$siteId       = $value->contentServiceInfo->siteId;
							$memSiteAccId = $value->memSiteAccId;
							$itemId       = $value->itemId;
						}
					}
				}
 
				//Get Account Level of the Item
				if( !empty( $itemId ) ) {

					$monthNr = 1; 

					$bankAccountDetail = new BankAccountDetail();
					$bankAccountInfo   = new BankAccountInfo(); 

					$bankAcctHistory = array();
					$bankAcctInfo    = array();

					//We will remove the transaction data on this cron 
					//to lessen the load of the process in one single 
					//method
					// pre('Saving Transactional Information');
					//Save Transactional Level Information
					// $transactions = $yodlee->saveUserTransactions($memSiteAccId, $siteId, $monthNr );
	
					//Get summary Information					
					$item = $yodlee->getItemsSummary1($itemId);

					if( isset($item['Body']->itemData) )	{

						$contentSrvcInfo = $item['Body']->contentServiceInfo;
						$accounts        = $item['Body']->itemData->accounts;

						foreach ($accounts as $key => $account) {

							$accountHolder = isset($account->accountHolder) ? $account->accountHolder : "";
							$accountNumber = isset($account->accountNumber) ? $account->accountNumber : "";

							$accountNumber = cleanNonNumeric($accountNumber);
								
							$bankAcctHistory[] = array(
								'User_Id'                 => $this->getUserId(),
								'Loan_App_Nr'             => $this->getLoanAppNr(),
								'Yodlee_Access_Dt'        => date('Y-m-d'), 
								'Member_Site_Id'          => $memSiteAccId,
								'Item_Type_Id'			  => $itemId,
								'Site_Id'                 => $contentSrvcInfo->siteId,
								'Acct_Holder_Name'        => $bankAccountDetail->encryptFields($accountHolder),
								'Acct_Nr'                 => $bankAccountDetail->encryptFields($accountNumber),
								'Acct_Nr_Raw'			  => $accountNumber,
								'Acct_Type_Desc'		  => $account->acctType,
								'Current_Bal_Amt'         => $account->currentBalance->amount,
								'Available_Bal_Amt'       => $account->availableBalance->amount,
								'Created_By_User_Id'      => $this->getUserId(),				
								'Create_Dt'               => date('Y-m-d'),
							);  

							$bankAcctInfo[] = array(
								'User_Id'            => $this->getUserId(), 
								'Site_Id'            => $contentSrvcInfo->siteId, 
								'Acct_Holder_Name'   => $bankAccountInfo->encryptField($accountHolder),  
								'Acct_Type_Desc'     => $account->acctType,  
								'Acct_Nr'            => $bankAccountInfo->encryptField($accountNumber), 
								'Created_By_User_Id' => $this->getUserId(), 
								'Create_Dt'          => date('Y-m-d'),  
							); 
						}  
					}

					//Store Bank Account Information
					if( count($bankAcctInfo) > 0 ) {
						
						try{  
							pre('Saving Bank Account Information');
							$bankAccountInfo::insertBatch( $bankAcctInfo );
						}catch(Exception $e) {
							writeLogEvent('RateReward - Saving Bank Account Details', 
								array( 
									'User_Id'     => $this->getUserId(),
									'Loan_App_Nr' => $this->getLoanAppNr(),
									'Message'     => $e->getMessage()
								) 
							); 
						}
					}

					//Store Bank Account History
					if( count($bankAcctHistory) > 0 ){
						
						try{  
							pre('Saving Bank Account History');
							$bankAccountDetail::insertBatch( $bankAcctHistory ); 
						}catch(Exception $e) {
							writeLogEvent('RateReward - Saving Bank Account Details', 
								array( 
									'User_Id'     => $this->getUserId(),
									'Loan_App_Nr' => $this->getLoanAppNr(),
									'Message'     => $e->getMessage()
								) 
							); 
						}
					} 
				} else {
					writeLogEvent('RateReward - Pulling Bank Transactions', 
						array( 
							'User_Id'     => $this->getUserId(),
							'Loan_App_Nr' => $this->getLoanAppNr(),
							'Message'     => 'Can\'t pull transaction'
						) 
					); 	
				}
			}
		} else{
			writeLogEvent('RateReward - Pulling Bank Transactions', 
				array( 
					'User_Id'     => $this->getUserId(),
					'Loan_App_Nr' => $this->getLoanAppNr(),
					'Message'     => 'Bank Account is not connected'
				) 
			); 		
		}
	}

  

  /**
   * TU prepare Request
   * 
   * @param  $fields array
   * @return TU XML Response
   */
  public function prepareTURequest( $fields = array() )
  {

      $xml = '<?xml version="1.0" encoding="utf-8" ?>
        <xmlrequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope" xmlns="http://www.netaccess.transunion.com/namespace">
        <systemId>'.$fields['TU_systemID'].'</systemId>
        <systemPassword>'.$fields['TU_systemPassword'].'</systemPassword>
        <productrequest>
          <creditBureau xmlns="http://www.transunion.com/namespace" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
              <document>request</document> 
              <version>2.12</version> 
              <transactionControl> 
                <userRefNumber>L2C THIN-U#01 FMP-SUB1</userRefNumber> 
                <subscriber> 
                  <industryCode>'.$fields['TU_industryCode'].'</industryCode> 
                  <memberCode>'.$fields['TU_memberCode'].'</memberCode> 
                  <inquirySubscriberPrefixCode>'.$fields['TU_inquirySubscriberPrefixCode'].'</inquirySubscriberPrefixCode> 
                  <password>'.$fields['TU_password'].'</password> 
                </subscriber> 
                <options> 
                  <processingEnvironment>'.$fields['TU_processingEnvironment'].'</processingEnvironment> 
                  <country>us</country> 
                  <language>en</language> 
                  <contractualRelationship>individual</contractualRelationship> 
                  <pointOfSaleIndicator>none</pointOfSaleIndicator> 
                </options>                                                                     
              </transactionControl> 
              <product> 
                <code>07000</code> 
                <subject> 
                  <number>1</number> 
                  <subjectRecord> 
                  <indicative> 
                    <name> 
                      <person> 
                        <first>'.$fields['TU_firstName'].'</first> 
                        <middle>'.$fields['TU_middleName'].'</middle>
                        <last>'.$fields['TU_lastName'].'</last> 
                      </person> 
                    </name> 
                    <address> 
                      <status>current</status>
                                <street>
                                    <unparsed>'.$fields['TU_street'].'</unparsed>
                                </street>
                                <location>
                                    <city>'.$fields['TU_city'].'</city>
                                    <state>'.$fields['TU_state'].'</state>
                                    <zipCode>'.$fields['TU_zipCode'].'</zipCode>
                                </location>
                    </address> 
                    <socialSecurity> 
                      <number>'.$fields['TU_ssn'].'</number> 
                    </socialSecurity> 
                    <dateOfBirth>'.$fields['TU_birthDate'].'</dateOfBirth> 
                  </indicative> 
                  <addOnProduct> 
                    <code>00N05</code> 
                    <scoreModelProduct>true</scoreModelProduct> 
                  </addOnProduct> 
                  </subjectRecord> 
                </subject> 
                <responseInstructions> 
                  <returnErrorText>true</returnErrorText> 
                  <document/>
                </responseInstructions> 
                <permissiblePurpose> 
                  <inquiryECOADesignator>individual</inquiryECOADesignator> 
                </permissiblePurpose> 
              </product>
            </creditBureau> 
          </productrequest>
        </xmlrequest>
        '; 
        
        // For reprocessing
        /*
        <tracking>
		  <transactionTimeStamp>2015-07-24T01:43:56.627-05:00</transactionTimeStamp>
		</tracking> 
		*/

      try{
	      $ch =  curl_init(); 
	      curl_setopt($ch, CURLOPT_URL, $fields['url']);
	      curl_setopt($ch, CURLOPT_PORT, 443); 
	      curl_setopt($ch, CURLOPT_HEADER, 0 );
	      curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	      curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/documents/'.$fields['cert_file']);
	      curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $fields['TU_sslPassword']);
	      curl_setopt($ch, CURLOPT_SSLKEY, getcwd().'/documents/'.$fields['key_file']);
	      curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $fields['TU_sslPassword']);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


	      $data = curl_exec($ch);
	      $curl_errno = curl_errno($ch);
	      $curl_error = curl_error($ch);

	      if ($curl_errno > 0) {
	          echo "cURL Error ($curl_errno): $curl_error\n";
	          $data = 'error';
	      }

	      curl_close($ch);
	      //die;
	  }catch(Exception $e) {
	  		//pre($e->getMessage());
	  }    
      return $data;
  }


  /**
  * Process TU Response, parse and save to ODS
  * @param XML string, user id, loan number
  * @return TU XML Response
  */
  public function processTUResponse($xmlResponse = '', $uid = 0, $lid = 0){
  	  $flag = 0;
      $parseBureauObject = simplexml_load_string($xmlResponse);
      //pre($parseBureauObject); die;
      $tuAttributesArr = array();
      //pre($parseBureauObject); die;
      try{
	      if(property_exists($parseBureauObject->product,'subject')){
	      	 if(property_exists($parseBureauObject->product->subject,'subjectRecord')){
	      	 	if(property_exists($parseBureauObject->product->subject->subjectRecord,'addOnProduct')){
	      	 		$tuAttributes = $parseBureauObject->product->subject->subjectRecord->addOnProduct;
	      	 		if(count($tuAttributes) > 0){

					        foreach($tuAttributes as $tu){
					            // TU attributes          
					            if(property_exists($tu,'scoreModel')){
					           		if(property_exists($tu->scoreModel,'characteristic')){
					           			foreach($tu->scoreModel->characteristic as $row){
							                  if(property_exists($row,'value')){
							                      array_push($tuAttributesArr, (int)$row->value);
							                  }else{
							                      array_push($tuAttributesArr, -1);
							                  }
							              }
							              $this->saveTUresponse($tuAttributesArr, $uid , $lid);
							              $flag = $tuAttributesArr;		
					           		}else{
					           			$flag = 'TU Response error: No characteristic Node';

					           		}				              		              
					            }else{
					             $flag = 'TU Response error: No scoreModel Node';
					            }
					        }  
					      }else{
					        $flag = 'TU Response error: No addOnProducts Node';
					      }
	      	 	}else{
	      	 		$flag = 'TU Response error: No addOnProduct Node.';
	      	 	}
	      	 }else{
	      	 	$flag = 'TU Response error: No subjectRecord Node.';
	      	 }
	      }else{
	      	$flag = 'TU Response error: No subject Node.';
	      }
	  } catch( Exception $e ){
	  	//pre($e->getMessage());
	  	//print $flag;
		// writeLogEvent('Ratereward process TU Response', 
		// 		array( 
		// 			'User_Id'     => $uid,
		// 			'Loan_App_Nr' => $lid,
		// 			'Message'     => $e->getMessage()
		// 		),
		// 		'info'
		// 	);
	 }	    
            
    return $flag;  
  }

   /**
  * Save TU Response
  * @param TU array, user id, loan number
  */
  public function saveTUresponse($tuAttributesArr = array(), $uid = 0, $lid = 0)
  {

	$tu = new TURateReward();
	$tu->User_Id = $uid;
	$tu->Loan_App_Nr = $lid;
	
	// for reprocess
	//$cdate = new DateTime();
	//$cdate->setDate(2015, 11, 9);

	$tu->Trans_Union_Access_Dttm = date('Y-m-d H:i:s'); //$cdate->format('Y-m-d H:i:s');
	$tu->AT03S  = $tuAttributesArr[0];
	$tu->AT20S  = $tuAttributesArr[1];
	$tu->AT33A  = $tuAttributesArr[2];
	$tu->AT33B  = $tuAttributesArr[3];
	$tu->AT36S  = $tuAttributesArr[4];
	$tu->ATAP01 = $tuAttributesArr[5];
	$tu->BC06S  = $tuAttributesArr[6];
	$tu->BC09S  = $tuAttributesArr[7];
	$tu->BC12S  = $tuAttributesArr[8];
	$tu->BC31S  = $tuAttributesArr[9];
	$tu->BC98A  = $tuAttributesArr[10];
	$tu->G058S  = $tuAttributesArr[11];
	$tu->G068S  = $tuAttributesArr[12];
	$tu->G093S  = $tuAttributesArr[13];
	$tu->G094S  = $tuAttributesArr[14];
	$tu->G217S  = $tuAttributesArr[15];
	$tu->G237S  = $tuAttributesArr[16];
	$tu->HIAP01 = $tuAttributesArr[17];
	$tu->HRAP01 = $tuAttributesArr[18];
	$tu->IN06S  = $tuAttributesArr[19];
	$tu->IN09S  = $tuAttributesArr[20];
	$tu->IN12S  = $tuAttributesArr[21];
	$tu->IN21S  = $tuAttributesArr[22];
	$tu->IN28S  = $tuAttributesArr[23];
	$tu->IN33S  = $tuAttributesArr[24];
	$tu->IN34S  = $tuAttributesArr[25];
	$tu->INAP01 = $tuAttributesArr[26];
	$tu->MT12S  = $tuAttributesArr[27];
	$tu->MT28S  = $tuAttributesArr[28];
	$tu->MTAP01 = $tuAttributesArr[29];
	$tu->RE33S  = $tuAttributesArr[30];
	$tu->RE34S  = $tuAttributesArr[31];
	$tu->REAP01 = $tuAttributesArr[32];
	$tu->S062S  = $tuAttributesArr[33];
	$tu->S064A  = $tuAttributesArr[34];
	$tu->S064B  = $tuAttributesArr[35];
	$tu->S071A  = $tuAttributesArr[36];
	$tu->S071B  = $tuAttributesArr[37];
	$tu->S073A  = $tuAttributesArr[38];
	$tu->S073B  = $tuAttributesArr[39];
	$tu->TCCS   = $tuAttributesArr[40];
	$tu->Created_By_User_Id = $uid;
	$tu->Create_Dt = date('Y-m-d'); //$cdate->format('Y-m-d');
	$tu->save();
  }

}