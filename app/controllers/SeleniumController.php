<?php

/**
 * Developers Notes
 *
 * Updated:
 *   - Added testVerify Email
 */

class SeleniumController extends BaseController {


	/*
	|--------------------------------------------------------------------------
	| Selenium Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'SeleniumController@index');
	|
	*/

	protected $layout =  'layouts.application';

	/**
	 * Construct
	 */
	public function __construct() 
	{
		$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);		
	}

	/**
	 * Test Verify Email 
	 * We're using this for Selenium Automation
	 * 
	 * @return
	 */
	public function testVerifyEmail()
	{	

		$this->layout = NULL;

		$userId = getUserSessionID();
 
		if( !empty($userId) ) {
			//Get User Token
			$user = User::where(
				'User_Id' , '=', $userId
			)
			->select('Remember_Token_Txt')
			->first();

			if( isset($user->Remember_Token_Txt) ){

				$data['firstName']       = '';
				$data['verificationUrl'] = route('verifyApplicant', array( 'token' => $user->Remember_Token_Txt ) );

				return View::make('emails.auth.email-verification', $data );
			}
		} else {
			echo 'No user found!';
		}
	}
}