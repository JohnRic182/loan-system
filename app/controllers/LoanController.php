<?php

/**
 * Developers Notes
 *
 * Updated:
 *    - creditPull validation
 *    - working on exclusion
 *    - update show loan rate
 *    - production qa user
 * 
 */

class LoanController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Loan Controller
	|--------------------------------------------------------------------------
	|
	| All Loan Specific method should be put here like Loan Details
	| Loan Product, Loan Portofolio
	|
	*/

	// Exclusion Rules
	const ANNUAL_INCOME_RULE                       = 35000;
	const TOTAL_BALANCE_OPEN_BANK_REVOLVING_TRADES = 20000;
	const TOTAL_BALANCE_OPEN_REVOLVING_TRADES      = 30000;
	const TOTAL_BALANCE_OPEN_MORTGAGE_TRADES       = 450000;
	const MONTHS_SINCE_OLDEST_TRADE                = 36;
	const NUMBER_OPEN_TRADE                        = 2;
	const NUMBER_BANKCARD_TRADE                    = 2;
	const NUMBER_INQUIRIES                         = 3;
	const DAYS_SINCE_LAST_NSF                      = 30;
	const MO_PERCENTAGE                            = 75;
	
	//LOAN DECISIONING
	const ABS_MIN_LOAN_AMT                         = 2600;
	const MAX_DTIR_PERCENTAGE                      = 0.41;
	const RR_DEFAULT_RATE                          = 0.5;
	
	//Bank Savings
	const BANK_SAVINGS_DISCOUNT_RATE               = 0.01;
	const DEFAULT_BANK_SAVING_ARP    			   = 0.28;


	const MIN_ANNUAL_INCOME 					   = 35000;

	const MIN_DELINQUENT_PERIOD					   = 2; // less than 3 months
	/**
	 * Exclusion Codes
	 */
	const A03  = 'Unable to verify employment';
	const A05  = 'Income insufficient for amount of credit requested';
	const A16  = 'Existing loan not paid off';
	const A17  = 'Time since recent declined application is too short';
	const X001 = 'No credit file';
	const X002 = 'Credit file mismatch';
	const X021 = 'Bankruptcy filing reported';
	const X023 = 'Delinquency on accounts';
	const X066 = 'Number of derogatory public record';
	const X063 = 'Number of accounts with recent delinquency';
	const X082 = 'Proportion of balance to limit on bank revolving accounts is too high';
	const X009 = 'Amount owed on bank revolving accounts is too high';
	const X019 = 'Amount owed on revolving account is too high';
	const X008 = 'Amount owed on accounts is too high';
	const X042 = 'Length of time accounts have been established';
	const X089 = 'Too few accounts with balances';
	const X109 = 'Too many recently opened accounts with balances';
	const X100 = 'Too many installment accounts';
	const D01  = 'Frequency of NSFs';
	const D02  = 'Recent NSF';
	const D03  = 'Negative or low balance';
	const D04  = 'Not enough funds';
	const D05  = 'Significant deposits decline'; 
	const X062 = 'Number of accounts with delinquency';
	const X101 = 'Too many inquiries last 12 months';
	const X071 = 'Number of established accounts';
	const X072 = 'Number of open installment loans';
	const X086 = 'Proportion of loan balances to loan amounts is too high';
	const X090 = 'Too few active accounts';
	const X022 = 'Date of last inquiry too recent';
	const X047 = 'Length of time reported mortgage accounts have been established';
	const X051 = 'Time since delinquency is too recent or unknown';
	const X092 = 'Too few installment accounts';
	const X038 = 'No mortgage loans reported';
	const X055 = 'Time since most recent bank revolving account opening is too short';
	const X110 = 'Too many recently opened bank revolving accounts';
	const X099 = 'Too many consumer finance company accounts';
	const X000 = 'No Action Code';

	const QA_User_Role_Id       = 3;

	const DEFAULT_ARS_SCORE     = 9; 

	const QA_DEFAULT_ARS_SCORE  = 350;

	const DRA_ADMIN 	= "OLTP"; // for iovation

	/**
	 * NLS Tasks Code
	 */
	const NLS_STATUS_CODE_NOT_STARTED   = '100001';

	/**
	 * Fraud Check 1
	 * @var integer
	 */
	public $fraudCheck1 = 0;

	/**
	 * Fraud Check 2
	 * @var integer
	 */
	public $fraudCheck2 = 0;

	/**
	 * Subject
	 * @var string
	 */
	protected $subject;

	/**
	 * Applicant Email 
	 * 
	 * @var string
	 */
	private $_email = '';
 	
 	/**
 	 * Loan Portofolio Name
 	 * 
 	 * @var string
 	 */
 	protected $loanPortfolioName;

 	/**
 	 * Loan Group Name
 	 * 
 	 * @var string
 	 */
 	protected $loanGroupName;

 	/**
 	 * Loan Template Name
 	 * 
 	 * @var string
 	 */
 	protected $loanTemplateName;

	/**
	 * The layout that should be used for responses
	 * @var string 
	 */
	public $layout  =  'layouts.application';
	
	/**
	 * Class Constructor
	 */
	public function __construct() 
	{
		$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);	
	}
  
	///////////////////////////////////////////////////////////////////////////////////
	//                        Pre-Qualification Process                              //
	///////////////////////////////////////////////////////////////////////////////////
	 
	/**
	 * Register User
	 *
	 * @version v1.0.4.21
	 *     - Removed SSN Validation
	 *     - Enable the Applicant to do Credit Pull as long 
	 *       as the Application is NOT disqualified or already APPLIED
	 *       
	 * @return
	 */	
	protected function postCreditCheck()
	{
		//Set layout to NULL
		$this->layout = NULL;

		$data = Input::all(); 

		//Get Current User Id
		$userId = getUserSessionID();
 
		//Get primary loan session data
		$loan = getLoanSessionData();

		//Combined birthdate
		$m = trim(Input::get('month'));
		$d = trim(Input::get('day'));
		$y = trim(Input::get('year'));

		$rentAmount = isset( $data['rentAmount'] ) ? str_replace(',', '', $data['rentAmount']) : 0 ;

		//get gross income; 0=annual 1=monthly
		if($data['term'] == 1)
			$data['annualGrossIncome'] = $data['annualGrossIncome'] * 12;

		$monthlyMortage = 0;

		if( isset($data['monthlyMortage']) && $data['monthlyMortage'] != "" )
			$monthlyMortage = $data['monthlyMortage'];

		$zestimate_result['zestimate'] = '';
		$zestimate_result['xml'] = '';

		//Get Rent Estimate 
		if( $data['housingSit'] == 1 )
			$zestimate_result = getRentZestimate(trim($data['address']), sprintf('%s %s %s', trim($data['city']), trim($data['state']), trim($data['zip'])));
            	
		$fields = array(
			'partnerID' 	 	=> (Session::has('partner.PID'))? Session::get('partner.PID') : 0, //default partner Id
			'campaignID' 	 	=> (Session::has('partner.CID'))? Session::get('partner.CID') : 0, //default campaign id 
			'yodleeUserName'    => Session::get('userName'),
			'yodleePassword'    => generatePassword(8),
			'firstName' 	 	=> trim($data['firstName']), //applicant first name 
            'middleName'  	 	=> ' ', //applicant middle name
            'lastName'  	 	=> trim($data['lastName']), //applicant last name
            'city' 			 	=> trim($data['city']),
            'streetAddress1' 	=> trim($data['address']),
            'state' 		 	=> trim($data['state']),
            'birthDate' 	 	=> date('m/d/Y', strtotime($m.'/'.$d.'/'.$y)),
            'age'				=> $data['age'],
            'rentAmount' 	 	=> trim($rentAmount),
            'annualGrossIncome' => trim(str_replace(',', '', $data['annualGrossIncome'])),
            'homePhoneNumber'   => trim($data['phoneNumber']),
            'mobilePhoneNr' 	=> trim($data['mobilePhoneNr']),
            'employeeStatus' 	=> trim($data['employeeStatus']),
            'ssn' 				=> trim($data['ssn']),
            'zip' 				=> trim($data['zip']),//set zip code
            'token'				=> trim($data['_token']),//set form token
            'nlsLoanPortfolio'	=> Config::get('nls.NLS_LOAN_PORTFOLIO_ID'),//default NLS Loan Portfolio Id
            'nlsLoanTemplate'	=> Config::get('nls.NLS_LOAN_TEMPLATE_ID'),//default NLS Loan Template
            'nlsLoanGroup'		=> Config::get('nls.NLS_LOAN_GROUP_ID'),//default NLS Loan Group
            'loanAmount' 		=> str_replace(',', '', $loan['loanAmt'] ),//set slider amount 
            'loanPurposeId'		=> $loan['loanPurposeId'],//set loan purpose
            'loanProductId'		=> trim($loan['loanProductId']),//set loan type
            'housingSit'		=> $data['housingSit'],//set housing situation id
            'contactPhoneNr'	=> isset($data['contactPhoneNr'])  && !empty( $data['contactPhoneNr'] ) ? $data['contactPhoneNr'] : '' ,
            'contactName'		=> isset($data['contactName']) && !empty( $data['contactName'] ) ? $data['contactName'] : '', 
            'monthlyMortage'	=> $monthlyMortage,
            'creditQuality'     => trim($data['creditQuality']),
			'zestimate'			=> $zestimate_result['zestimate'],
			'zestimateXML'		=> $zestimate_result['xml']
		);

		if( isset($data['delinquencyPeriod']) )
			$fields['delinquencyPeriod'] = $data['delinquencyPeriod'];

		//set user id 
		$fields['userId'] = getUserSessionID();

		// insert to Applicant table
		$applicant = new Applicant();
		
		//Initialize NLS
		$NLS  = new NLS();
		
		//Set State and Contact Id for NLS
		$fields['State']     = $fields['state'];
		$fields['contactId'] = $userId;

		//Update NLS Contact Object
		$NLS->NLSUpdateContact( $fields );

		$pullFlag = 0;

		//Fraud Checking
		$idology 		= new Idology();
		$idologyFields 	= array(
				'firstName' => $fields['firstName'],
                'lastName'  => $fields['lastName'],
                'address'   => $fields['streetAddress1'],
                'city'      => $fields['city'],
                'state'     => $fields['state'],
                'zip'       => $fields['zip'],
                'ssn'       => $fields['ssn']
			);
		$phaseFields 	= array(
				'loanAppPhaseNr' 	 => 2,
				'subPhaseCodePass' 	 => 'FraudCheckPassCredit',
				'subPhaseCodeFail' 	 => 'FraudCheckFailCredit'
			);

		$idologyResult = $idology->idologyCall( $idologyFields, 'ascendInit', $phaseFields );
 
		//Check if the Applicant Already Exist
		$updateFlag = 0;
		$doSSNCheck = false;

		if ( Applicant::find( $fields['userId'] ) ) $updateFlag = 1;

		//If update, check if SSN is still the same
		//Else do SSN check
		if ($updateFlag) {
			$newSSN = $fields['ssn'];
			$oldSSN = $applicant->getData(array('Social_Security_Nr'), array('User_Id' => $fields['userId']), true);

			if ($newSSN != $oldSSN['Social_Security_Nr']) $doSSNCheck = true;
		}
		else {
			// Exclude QA from this checking
			if (!Session::get('loan.isProductionBypass')) $doSSNCheck = true;
		}

		//If do SSN check
		if ($doSSNCheck && $this->hasApplicationWithinTimePeriod($data['ssn'])) {
			return Response::json(
				array(
					'result'  => 'failed time range',
					'type'    => 'creditCheck',
					'message' => showNotification('applicationPeriodFailed')
				), 200
			);
		}

		//If Idology Fraud Checking Fail Set the Credit pull to TRUE 
		//Then return the Fraud Checking Fail 
		if( $idologyResult == 'fail' ){			
			$creditPull = 'true';
		} else{
			//Credit Pull Checking
			$creditPull = $NLS->IsNLSCreditPullExist($userId);
		}
		
 
		if( $creditPull == 'true' ){
			
			$fields['employeeStatusDescription'] = '';

			//Get Employment Status
			$empStat = EmployeeStatus::find( $fields['employeeStatus'] );

			if( count( $empStat ) > 0 )
				$fields['employeeStatusDescription'] = $empStat->Employment_Status_Desc;

			//Save Applicant Information
			$applicant->saveApplicant($fields, $updateFlag);

			//Set Applicant Information into Session
			setApplicantData( $fields['userId'] );

			//--APPLICANT ADDRESS
			//Save Applicant Address Information
			$address = new ApplicantAddress();

			$fields['zip4'] = '';
		
			if( strpos( $fields['zip'], '-' ) != false ){
				$zipArr = explode('-', $fields['zip']);
				if( isset($zipArr[0]) )
					$fields['zip']  = $zipArr[0];
				if( isset($zipArr[1]) )
					$fields['zip4'] = $zipArr[1];
			}

			//Get the substring of zip code 
			$fields['zip']  = substr( $fields['zip'], 0, 5 );
			$fields['zip4'] = substr( $fields['zip4'], 0, 4 );

			$address->saveApplicantAddress($fields, $updateFlag);
				
			//Save Applicant Contact Information
			$contact = new ApplicantContactInfo();
			$contact->saveApplicantContact($fields, $updateFlag);
	 
			ApplicantFinancialInfo::updateOrCreateAppFinancialInfo($fields, $updateFlag);

			//Update Or Create Loan Application Number
			$this->updateOrCreateLoanApp( $fields , $updateFlag );
 
			//Get the Update Loan Application Number
			$loanAppNr = getLoanAppNr();
				  			
  			//Lead Conversion
  			if( !empty( Cookie::get('Partner_LT') ) ) {

  				$cookie = UserAcctCookie::find( Cookie::get('Partner_LT') , array('Partner_Id', 'Tracking_Nr', 'Campaign_Id', 'User_Id') );

  				if( count($cookie) > 0 ) {

  					$fields['Campaign_Id']			= $cookie->Campaign_Id;
		  			$fields['Funding_Partner_Id'] 	= $cookie->Partner_Id;

		  			$this->saveLeadConversion($loanAppNr, $userId, $cookie );	
  				}
	  		}  

			//Update or Create Loan Detail
			LoanDetail::updateOrCreateLoanDetail($fields, $loanAppNr, $updateFlag ); 
 			
 			$subPhase  = Config::get('subphase.CreditPullSuccess');

 			//Set the credit pull to fail when Idology result fail
 			if( $idologyResult == 'fail' ){
 				$subPhase  = Config::get('subphase.CreditPullFail/Null');
 			}

 			$fields['Loan_Sub_Phase_Nr']    = $subPhase['Loan_Sub_Phase_Id'];
 			$fields['Loan_Sub_Phase_Desc']  = $subPhase['Loan_Sub_Phase_Desc'];

			//Update or Create Loan App Detail	
 			LoanApplicationDetail::updateOrCreateLoanAppDetail($fields, $loanAppNr );

 			//Update Fraud Checking Loan Application Detail
 			if( $idologyResult == 'fail' ){
 				setLoanAppPhase( $userId , $loanAppNr, $phaseFields['loanAppPhaseNr'], $phaseFields['subPhaseCodeFail']  );
 				return Response::json(
				 	array(
				 		'result'  => 'failed fraud check',
				 		'type'    => 'fraudCheck',
				 		'message' => 'failed fraud check'
				 	), 200
				);
 			}else{
 				setLoanAppPhase( $userId , $loanAppNr, $phaseFields['loanAppPhaseNr'], $phaseFields['subPhaseCodePass']  );
 			}

			//Update Application Consent
			$consent = getConsentSessionData();

			// Log::info('Consent Data:', $consent);
			if( count($consent) >  0 ){
				foreach ($consent as $key => $value)
					$this->updateConsent($value);
			}
				
			//Create / Update SF Object
			importSFAccountObj( $userId, $loanAppNr );

			//Create / Update SF Loan Object
			importSFLoanObj( $userId, $loanAppNr );
			
			//TODO: We will transfer welcome email to Next User Integration
			//SEND WELCOME EMAIL
			//$this->sendWelcomeEmail( Session::get('email'), $data['firstName'] );
			
			//destroy partner session
			Session::forget('partner');

			//sign on the user
			Session::put('logged', 1);

			//IOVATION
			if( !empty($data['ioBB']) && !empty($data['fpBB']) ){
			
				$iovation = new IOvation();

				$rule_set 	= 'loan_application';
				$ip 		= getRealClientIpAddress();
				$blackbox 	= $data['ioBB'] . ";" . $data['fpBB'];

				$iovationResult = $iovation->checkClient($userId, $blackbox, $ip, $rule_set, $fields);				
				
				writeLogEvent('Iovation Result Log Event', 
                    array(
                    	'User_Id' => $userId,
                        'Iovation' => $iovationResult
                    ), 
                    'info'
                );

				if( isset($iovationResult[ 'details']) ) {
					foreach ( $iovationResult[ 'details']  as $detail ) {

			  	  		if ( is_array( $detail ) ) {
			  	    		foreach ( $detail as $pair ) { 
			            		$ioArr[$pair->name] = $pair->value;
			            	}
			            }

				        $ioArr['result'] = $iovationResult['result'];
				        $ioArr['reason'] = $iovationResult['reason'];
				        $ioArr['trackingnumber'] = $iovationResult['trackingnumber'];
				        $ioArr['endblackbox'] = $iovationResult['endblackbox'];
				        $ioArr['userId'] = $userId;

				        IovationResponse::saveIovationResponse($ioArr);
			    	}
			    }

			} 

			return Response::json(
				array(
					'result'  => 'success', 
					'type'    => 'creditCheck', 
					'message' => 'Successfully created Loan Object and Credit Pull'
				), 200
			); 	
		}else{

			$logFile = Config::get('log.application');
			Log::useDailyFiles(storage_path().'/logs/' . $logFile );
			//Store Error Log Information
			Log::warning(
					'Credit Pull'
					, array(
						'User_Id' => $userId, 
						'CreditPullResponse' => $creditPull 
					)
				);
  
			return Response::json(
			 	array(
			 		'result'  => 'failed pull',
			 		'type'    => 'creditCheck',
			 		'message' => showNotification('noCreditPullData')
			 	), 200
			);
		} 
	}

	/**
	 * Checks if user has applied for loan within the 6 months period.
	 *
	 * @param $ssn
	 * @param $monthPeriod
	 * @return bool
	 */
	public function hasApplicationWithinTimePeriod($ssn, $monthPeriod = 6)
	{
		$applicant = new Applicant();

		//If partnerID is DOT818 or LeadsMarket, exempt from SSN check
		$partnerId = (Session::has('partners.Partner_Id')) ? Session::get('partners.Partner_Id') : 0;
		$excludedPartners = array(3, 6, 8);

		if (!in_array($partnerId, $excludedPartners)) {

			// Gets recent application by SSN
			$latestApplicationBySSN = $applicant->getApplicationBySSN($ssn);

			// Gets the last update date field
			if ($latestApplicationBySSN) {
				$baseTime = (!is_null($latestApplicationBySSN->Update_Dt)) ? strtotime($latestApplicationBySSN->Update_Dt) : strtotime($latestApplicationBySSN->Create_Dt);
				$interval = date_diff(new DateTime(date('Y-m-d H:i:s')), new DateTime(date('Y-m-d H:i:s', $baseTime)));

				// If last application is less than 6 months ago, send error message
				return ($interval->format('%m') < $monthPeriod);
			}
		}

		return false;
	}


	/**
	 * save iovaionResponse
	 *
	 *
	 * @todo   transfer this to model
	 * @param  array $data
	 * @return
	 */
	public function saveIovationResponse( $data = array() )
	{

		$ioObject = new IovationResponse();

		$ioObject->Result_Cd 		= (isset($data['result'])) ? $data['result'] : DB::raw("DEFAULT");
		$ioObject->Reason_Desc_Txt 	= (isset($data['reason'])) ? $data['reason'] : DB::raw("DEFAULT");
		$ioObject->End_Blackbox_Txt = (isset($data['endblackbox'])) ? $data['endblackbox'] : DB::raw("DEFAULT");
		$ioObject->Tracking_Nr 		= (isset($data['trackingnumber'])) ? $data['trackingnumber'] : DB::raw("DEFAULT");
		
		$ioObject->Device_Alias_Cd 					= (isset($data['device.alias'])) ? $data['device.alias'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Conf_Language_Cd 	= (isset($data['device.browser.configuredlang'])) ? $data['device.browser.configuredlang'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Language_Cd 		= (isset($data['device.browser.lang'])) ? $data['device.browser.lang'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Type_Cd 			= (isset($data['device.browser.type'])) ? $data['device.browser.type'] : DB::raw("DEFAULT");
		$ioObject->Device_Browser_Version_Nr 		= (isset($data['device.browser.version'])) ? $data['device.browser.version'] : DB::raw("DEFAULT");
		$ioObject->Device_Cookie_Enabled_Flag 	= (isset($data['device.cookie.enabled'])) ? $data['device.cookie.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_Firstseen_Ts 			= (isset($data['device.firstseen'])) ? $data['device.firstseen'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Enabled_Flag 	= (isset($data['device.flash.enabled'])) ? $data['device.flash.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Installed_Flag 	= (isset($data['device.flash.installed'])) ? $data['device.flash.installed'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Storage_Enabled_Flag = (isset($data['device.flash.storage.enabled'])) ? $data['device.flash.storage.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_Flash_Version_Nr 	= (isset($data['device.flash.version'])) ? $data['device.flash.version'] : DB::raw("DEFAULT");
		$ioObject->Device_Js_Enabled_Flag 	= (isset($data['device.js.enabled'])) ? $data['device.js.enabled'] : DB::raw("DEFAULT");
		$ioObject->Device_New_Flag 		= (isset($data['device.new'])) ? $data['device.new'] : DB::raw("DEFAULT");
		$ioObject->Device_Os_Version_Nr = (isset($data['device.os'])) ? $data['device.os'] : DB::raw("DEFAULT");
		$ioObject->Device_Screen_Txt 	= (isset($data['device.screen'])) ? $data['device.screen'] : DB::raw("DEFAULT");
		$ioObject->Device_Type_Cd 		= (isset($data['device.type'])) ? $data['device.type'] : DB::raw("DEFAULT");
		$ioObject->Device_Tz 			= (isset($data['device.tz'])) ? $data['device.tz'] : DB::raw("DEFAULT");

		$ioObject->IP_Addr_Id 					= (isset($data['ipaddress'])) ? $data['ipaddress'] : DB::raw("DEFAULT");
		$ioObject->Real_IP_Addr_Id 				= (isset($data['realipaddress'])) ? $data['realipaddress'] : DB::raw("DEFAULT");
		$ioObject->Real_IP_Addr_Id_Source_Cd 	= (isset($data['realipaddress.source'])) ? $data['realipaddress.source'] : DB::raw("DEFAULT");
		$ioObject->Ruleset_Rulesmatched_Val 	= (isset($data['ruleset.rulesmatched'])) ? $data['ruleset.rulesmatched'] : DB::raw("DEFAULT");
		$ioObject->Ruleset_Score_Val 			= (isset($data['ruleset.score'])) ? $data['ruleset.score'] : DB::raw("DEFAULT");

		$ioObject->Evidence_Types_Txt 	= '';
		$ioObject->Created_By_User_Id 	= $data['userId'];
		$ioObject->Create_Dt 			= date('Y-m-d');
		$ioObject->Updated_By_User_Id 	= $data['userId'];
		$ioObject->Update_Dt 			= date('Y-m-d');

		$ioObject->save();
	
	}

	/**
	 * Save Lead Conversion
	 * 
	 * @param  integer $loanAppNr
	 * @param  integer $userId
	 *
	 * @return
	 */
	public function saveLeadConversion( $loanAppNr = NULL, $userId = NULL , $cookie = array() )
	{

		if( !empty($loanAppNr) && !empty($userId) && count($cookie) > 0 ) {

			$leadId  = $cookie->Tracking_Nr; 

			//Assign the Cookie Id as Lead_Id if UID is empty
			if( empty($leadId) &&  Cookie::has('Partner_LT') )
				$leadId = Cookie::get('Partner_LT'); 

			//Prepare Lead Conversion Data
			$leadData = array(
				'Lead_Id'              => $leadId, 
				'Campaign_Id'          => $cookie->Campaign_Id, 
				'Loan_Id'              => $loanAppNr, 
				'Created_By_User_Id'   => $userId, 
				'Create_Dt'            => date('Y-m-d'), 
				'Funding_Partner_Id'   => 0, //DB::raw("DEFAULT"),
				'Marketing_Partner_Id' => 0, //DB::raw("DEFAULT"), 
				'Loan_Funded_Dt'       => DB::raw("DEFAULT")
			);

			//Get partner type
			$isRow = true;
			$partnerData = Partner::getData( 
						array('Partner_Type_Id'), 
						array('Partner_Id' => $cookie->Partner_Id )
						, $isRow 
					);

			if( $partnerData['Partner_Type_Id'] == 2 )
				$leadData['Funding_Partner_Id']   = $cookie->Partner_Id; 
			else 
				$leadData['Marketing_Partner_Id'] = $cookie->Partner_Id; 				

			LeadConversion::saveLeadConversion($leadData);
		}

	}

	/** 
	 * Send Welcome Email 
	 * 
	 * @param  strng $email
	 * @param  array $data
	 * @todo  
	 * 		- need to create email template table to store subject
	 * 		- and other information
	 * @return
	 */
	protected function sendWelcomeEmail( $email = '', $firstName = '')
	{

		if( !empty( $email ) ) {
  			
  			try{
  				//set global email
				$this->email 	= $email;
				$this->subject  = 'Thank you for your interest in Ascend Consumer Finance';

	  			$data = array('firstName' => $firstName ); 

				Mail::send('emails.auth.credit-check', $data, function($message)
				{ 
				    $message->to( $this->email, 'Customer')
				    		// ->cc('jjurilla@global-fusion.net')
				    		// ->cc('fcorugda@global-fusion.net')
				    		->subject($this->subject);
				});

  			}catch(Exception $e) {
  				pre($e->getMessage());
  			} 
		} 
 	}

 	
	/**
	 * Update Or Create Loan Application
	 * 
	 * @param  array   $fields
	 * @param  integer $updateFlag
	 * @return
	 */
	protected function updateOrCreateLoanApp( $fields = array(), $updateFlag )
	{ 
		$loanAppNr = getLoanAppNr();  
		LoanApplication::updateOrCreateLoanApp($loanAppNr, $fields, $updateFlag); 
	}
  
	/**
	 * Show Bank Account Form - Step 3.0 Loan Application
	 * 
	 * @return
	 */
	protected function showBankAccountFormLink()
	{

		$loanTerm = 36; 
		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat();
	
		if( !empty($redirect) )
			return Redirect::to($redirect);

		$userId    = getUserSessionID();
		$loanAppNr = getLoanAppNr();

		//set the application step	
		setActiveStepSession('3');
 	 
		$exclusionLink = 'exclusionCheck/initial';
 
		//Update NLS Loan Status to APPLICATION_STEP2 
		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
								  ->where('Loan_Id','=', $loanAppNr)
								  ->first();

		//TODO: Need to set Loan Template, Loan Portfolio 
		//and Loan Group into Session
		$loanTemplate      = LoanTemplate::where('Loan_Template_Id', '=', $loanDetail->Loan_Template_Id)->first();	
		$loanTemplateName  = $loanTemplate->Loan_Template_Name;
		
		$loanGroup         = LoanGroup::where('Loan_Grp_Id', '=', $loanDetail->Loan_Grp_Id)->first();	
		$loanGroupName     = $loanGroup->Loan_Grp_Name;
		
		$loanPortfolio     = LoanPortfolio::where('Loan_Portfolio_Id', '=', $loanDetail->Loan_Portfolio_Id)->first();	
		$loanPortfolioName = $loanPortfolio->Loan_Portfolio_Name;

		$whereParam = array('User_Id' => $userId);	

		$applicant = new Applicant();
		$applicant_fields = array(
			'User_Id',
			'First_Name',
			'Middle_Name',
			'Last_Name',	
			'Age_Cnt',
			'Birth_Dt',
			'Social_Security_Nr',
			'Yodlee_User_Name'
		);
		
		//Get Applicant Data
		$applicantData = $applicant->getData(
									  $applicant_fields
									, $whereParam
									, true
								);

		$fields = array(
				'nlsLoanPortfolio' 	=> $loanPortfolioName,
				'nlsLoanGroup'     	=> $loanGroupName,
				'nlsLoanTemplate'  	=> $loanTemplateName,
				'contactId' 		=> $userId,
				'loanId' 			=> $loanAppNr,
				'firstName' 		=> $applicantData->First_Name,
				'lastName' 			=> $applicantData->Last_Name,
				'loanStatusCode' 	=> 'APPLICATION_STEP2'
		);

		$NLS = new NLS();
		$NLS->nlsUpdateLoanStatus($fields); 
 		
 		//Enable Debug Mode for Exclusion Override
		if( strtolower(Config::get('system.Ascend.ExclusionOverride')) == 'true'){
			$exclusionLink = 'exclusionScoringTest/initial/debugMode';
		}
		
		//Set Loan Application Phase Number
		$loanAppPhaseNr = 3; 
 		setLoanAppPhase( $userId, $loanAppNr, $loanAppPhaseNr );
 		 
 		//set Exclusion Link
		$data['link'] = $exclusionLink;

		//get the loan product discount rate
		// $rate = $loanProduct[abs($loanDetail->Loan_Prod_Id)];
		
		//is user from express funnel
		$data['LTExpress'] = isLTExpressFunnelUser();

		//get the savings amount
		$savings = $this->calculateLinkBankAcctSavings( $loanDetail->Loan_Amt , $loanTerm );

		$data['savings'] = '$' . round($savings);

		$this->layout->content = View::make('blocks.application.bank-link', $data);
	}

	/**
	 * Calculate Savings if Applicant Link His/Her Bank Account
	 * 
	 * @param  integer $loanAmt  - the amount of loan to be borrowed
	 * @param  float   $rate     - discount rate
	 * @return float
	 */
	protected function calculateLinkBankAcctSavings( $loanAmt , $loanTerm )
	{ 
		$savingsAPR    = ( self::DEFAULT_BANK_SAVING_ARP - self::BANK_SAVINGS_DISCOUNT_RATE ) * 100 ;
		$absAPR        = self::DEFAULT_BANK_SAVING_ARP * 100;
 
		$absAPRPMT     = pmt($absAPR , $loanTerm, $loanAmt ); 
		$savingsAPRPMT = pmt( $savingsAPR , $loanTerm, $loanAmt );
		
		$absAPRPMT     = $absAPRPMT * 36 ; 
		$savingsAPRPMT = $savingsAPRPMT * 36 ;

		return $absAPRPMT - $savingsAPRPMT;
	}


	/**
	 * Show Bank Account Form (Yodlee) - Step 3.1 Loan Application
	 * 
	 * @deprecated version 1.15
	 * @return
	 */
	protected function showBankAccountForm()
	{ 
			
		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat();
		
		$data = array();

		if( !empty($redirect) )
			return Redirect::to($redirect);
  
		// _pre($data);
		// if( Session::has('YodleeUserName') && Session::has('YodleePassword') ){

			//set the application step	
			setActiveStepSession('3');
 
			$data['message']  = Session::get('message');
			$data['banks']    = getTopBanks();

			$this->layout->content = View::make('blocks.application.bank', $data);
			
		// } else {
			// return Redirect::to('linkBankAccount');
		// }		
	}

	/**
	 * Show Bank Account Form (Yodlee) - Step 3.1 Loan Application
	 * 
	 * @return
	 */
	protected function showFastLinkBankAccount()
	{ 
		$json 		= false;
		$bahFlag 	= $this->checkBankAccountHistory($json);

		if($bahFlag)
			return Redirect::to('exclusionCheck/initial');

		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat();
		
		$data = array();

		if( !empty($redirect) )
			return Redirect::to($redirect);
 
		$data['fastLinkUrl'] = Session::get('fastLinkUrl');

		// _pre($data);
		
		//Each Reload Needs to generate new Fast Link Url
		// $Yodlee = new Yodlee();
		// $Yodlee->generateFastLinkUrl();

		// $data['fastLinkUrl'] = ''; 

		// if( Session::get('fastLinkUrl') ) 
		// 	$data['fastLinkUrl'] = Session::get('fastLinkUrl');
		// else
		// 	$data['error'] = 'Error on generating Fast Link Form.';

		 
		// _pre($data);
		if( Session::has('YodleeUserName') && Session::has('YodleePassword') ){

			//set the application step	
			setActiveStepSession('3');
 	
			$data['message']  = Session::get('message');
			$data['banks']    = getTopBanks();

			$this->layout->content = View::make('blocks.harness.fastlink', $data);
			
		} else {
			return Redirect::to('linkBankAccount');
		}		
	}

	/**
	 * Save Credit Profile and TU Data
	 * 
	 * @param integer $userId
	 * @param integer $loanAppNr
	 * @return
	 */
	protected function saveCreditProfileAndTU( $userId, $loanAppNr )
	{
		//Credit Profile
		$isCreditProfileExist = NlsCreditProfile::isCreditProfileExist($userId);
 
		if(  $isCreditProfileExist == 0 ){
			// echo 'Saving NLS Data';
			$this->saveNLS();
		}

		//TU Credit
		$isTUCreditExist = TUCredit::isTUCreditExist($loanAppNr, $userId);
	 
		if( $isTUCreditExist == 0 ){
			// echo 'Pulling TU Credit';
			$this->parseXML( $userId, $loanAppNr, 'initial' );  	
		}
	}
 
	/**
	 * Show Loan Rate - Step 4 Loan Application
	 * 	  - Use usp_Loan_Decision
	 * 	  - Check ARS Scoring
	 * 	  - Calculate Debt to Income Ratio
	 * 	  - Calculate Loan Amount
	 * 	  - Calculate Loan Payment
	 * 
	 * @return
	 */
	protected function showLoanRate()
	{	 
		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat();
	
		if( !empty($redirect) )
			return Redirect::to($redirect);

		$userId    = getUserSessionID();
		$loanAppNr = getLoanAppNr(); 

		//Get Loan Application Data based on Request Method
		if (Request::isMethod('post')){

			$post = Input::all(); 	
  			$loan['loanProductId'] = ( isset($post['rr']) && $post['rr'] == 'yes') ? 2 : 1;
			$loan['loanAmt']       =  $post['loanAmt']; 
			//Update Loan Product Id
			Session::put('loan.loanProductId', $loan['loanProductId']);

			//Update Loan Session Data
			setLoanSessionData(
				array(
					'loanAmt' => $loan['loanAmt'], 
					'loanProductId' => $loan['loanProductId']
				)
			); 

		} else{
			$loan = getLoanSessionData(); 			
		}


		$isBankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanAppNr);
   
		//Check for the ARS Score
		$score = Session::get('ARSScore'); 
		$score = (int) $score;

		//Redirect Session If Score Is Empty
		if( empty($score) )
			return Redirect::to('disqualification');

 		//Set Active Step in Session
		setActiveStepSession(4);

		//Set Loan Application Phase
		$loanAppPhaseNr = 5; 
		setLoanAppPhase( $userId , $loanAppNr, $loanAppPhaseNr );

		//Update Loan Amount in Loan Detail
 		LoanDetail::where( array(
					'Borrower_User_Id' => $userId, 
					'Loan_Id'		   => $loanAppNr
				)
			)->update(
				array(
					'Loan_Amt'     => $loan['loanAmt'], 
					'Loan_Prod_Id' => $loan['loanProductId']
				)
			);
	
		$whereParam = array('User_Id' => $userId);  
        $applicantAddress = new ApplicantAddress();
        $applicantAddress_fields = array( 'State_Cd' );
        $addressData = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);

		//Get Loan Decisioning Results using ODS.dbo.usp_Loan_Decision for Standard Loan
 		$stloanDetail = DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?,?,?", 
							array(
								$addressData->State_Cd
								,$userId
								,$score
								,1
								,$isBankAcctLinked,
								'N'
							)
						); 	
		
		//Get RR Loan Decisioning Results using ODS.dbo.usp_Loan_Decision for RR Loan
 		$rrloanDetail = DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?,?,?", 
							array(
								$addressData->State_Cd
								,$userId
								,$score
								,2
								,$isBankAcctLinked,
								'N'
							)
						); 	
 
 		//Call Loan Decisioning Results using ODS.dbo.usp_Loan_Decision
 		$isLoanApproved = DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?,?,?", 
					array(
						$addressData->State_Cd
						,$userId
						,$score
						,$loan['loanProductId']
						,$isBankAcctLinked
						,'Y'
					)
				); 	

 		//Get Loan Application Detail
 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
								->where('Loan_Id','=',$loanAppNr)
								->first();				

		//Get Applicant Information
		$param = array('User_Id' => $userId);	
		$isRow = true;
		
		$applFields = array(
			'First_Name',
			'Last_Name',
			'Social_Security_Nr'
		);
		
		$applData = Applicant::getData( $applFields, $param, $isRow );
		
		//Get Applicant Address Information
		$applAddressFields = array(
			'Street_Addr_1_Txt',
			'City_Name',
			'State_Cd',
			'Zip_Cd',
			'Zip_4_Cd'
		);

		$applAddressData = ApplicantAddress::getData( $applAddressFields, $param, $isRow );

		//Get Loan Template
		$LoanTemplate = LoanTemplate::find( $loanDetail->Loan_Template_Id );

		//Get Loan Group
		$loanGroup = LoanGroup::find( $loanDetail->Loan_Grp_Id );

		//Get Loan Portfolio
		$loanPortfolio = LoanPortfolio::find( $loanDetail->Loan_Portfolio_Id );

		//NLS Loan Status Updates Param
		$nlsParam = array(
			'nlsLoanTemplate' => $LoanTemplate->Loan_Template_Name,
			'contactId' 	  => $userId,
			'nlsLoanGroup' 	  => $loanGroup->Loan_Grp_Name,
			'nlsLoanPortfolio'=> $loanPortfolio->Loan_Portfolio_Name,
			'loanId' 		  => $loanAppNr,
			'firstName'		  => $applData->First_Name,
			'lastName' 		  => $applData->Last_Name, 
		);

		//Check Whether the Applicant is Approved or Not
		if( count($isLoanApproved) > 0 && isset($isLoanApproved[0]->Loan_Qual_Flag ) ) {
			if( $isLoanApproved[0]->Loan_Qual_Flag != 1) {
				$this->rejectApplication( $nlsParam , $loanAppNr );	
 				return Redirect::to('disqualification');
 			}
		}
  
		//Estimated Monthly Payment After Reward 		
 		$estMnthlyPymntAfrRwrd  = $this->calculateRRPaymentAfterReward(
 											$loanDetail->Loan_Amt, 
 											$loanDetail->Monthly_Payment_Amt 
 										);

 		//Get Loan Decisioning Results using ODS.dbo.usp_Loan_Decision for Standard Loan
 		
		$data['stLoanAmount'] 			 = $stloanDetail[0]->Loan_Amt;
		$data['stInterestRate']     	 = $stloanDetail[0]->APR_Val * 100;
		$data['stMonthlyPayment']   	 = number_format($stloanDetail[0]->Monthly_Payment_Amt, 2);
		$data['stEstMnthlyPymntAfrRwrd'] = number_format($estMnthlyPymntAfrRwrd, 2);						

		//Get RR Loan Decisioning Results using ODS.dbo.usp_Loan_Decision for RR Loan 		
		$data['rrLoanAmount'] 			 = $rrloanDetail[0]->Loan_Amt;
		$data['rrInterestRate']     	 = $rrloanDetail[0]->APR_Val * 100;
		$data['rrMonthlyPayment']   	 = number_format($rrloanDetail[0]->Monthly_Payment_Amt, 2);
		$data['rrEstMnthlyPymntAfrRwrd'] = number_format($estMnthlyPymntAfrRwrd, 2);
 
		//For QAT Automation
		$data['qatUserId']  = $userId;
 
 		//Get Loan Rate Map
 		$rateMap   = $this->getARSScoreToRateMap( $score, $loan['loanProductId'], $isBankAcctLinked, $addressData->State_Cd); 
  
		$data['loanAmount']            = $loanDetail->Loan_Amt;
		$data['interestRate']          = $loanDetail->APR_Val * 100;
		$data['lowRiskScoreVal'] 	   = $rateMap['Low_Risk_Score_Val'];
		$data['highRiskScoreVal']	   = $rateMap['High_Risk_Score_Val'];
		$data['minLoanAmount']         = $rateMap['Absolute_Min_Loan_Amt']; 
		$data['maxLoanAmount']         = $loanDetail->Max_Loan_Amt; 
		$data['affrLoanAmount']		   = $loanDetail->Affordable_Loan_Amt; 
		$data['monthlyPayment']        = number_format($loanDetail->Monthly_Payment_Amt, 2);
		$data['EstRRDiscount']         = 0.45; 							// to be implemented
		$data['EstMnthlyPymntAfrRwrd'] = number_format($estMnthlyPymntAfrRwrd, 2); 		// to be implemented
		$data['loanType'] 			   = $loan['loanProductId'];
		$data['firstName'] 	 		   = $applData->First_Name;
		$data['lastName']  	 		   = $applData->Last_Name;
		$data['ssn'] 				   = $applData->Social_Security_Nr;
		$data['city'] 			 	   = $applAddressData->City_Name;
        $data['streetAddress1'] 	   = $applAddressData->Street_Addr_1_Txt;
        $data['State'] 		 		   = $applAddressData->State_Cd;
        $data['zip'] 		 	       = $applAddressData->Zip_Cd;


		//Idology Data
 	  	$data['testMode'] = (strtolower(Config::get('system.Ascend.DebugMode') ) == 'true') ? 1 : 0;
 	  	$data['idologyTestMode'] = (strtolower(Config::get('system.Idology.TestMode') ) == 'true') ? 1 : 0;

 	  	//NLS Loan Application Updates
 	  	$fields = array(
				'nlsLoanTemplate' => $LoanTemplate->Loan_Template_Name,
				'contactId' 	  => $userId,
				'loanId' 		  => $loanAppNr,
				'firstName'		  => $applData->First_Name,
				'lastName' 		  => $applData->Last_Name,
				'loanStatusCode'  => 'APPLICATION_STEP3',
				'loanAmount'	  => $loanDetail->Loan_Amt,
				'interestRate'	  => $loanDetail->APR_Val * 100,
				'paymentAmount'	  => $loanDetail->Monthly_Payment_Amt,
				'loanType'		  => ( $loanDetail->Loan_Prod_Id == 2 )? 'RR' : 'APL', // loanProdname
				'LOANDETAIL1' 	  => 1,
				'LoanPortfolioName' => '<DEFAULT>'
			);

 	  	$NLS = new NLS();
		$NLS->nlsUpdateLoan($fields);

		//Display Result depending on REQUEST
		if ( Request::isMethod('post') ){

			$result = array(
				'loanAmount'            => number_format($loanDetail->Loan_Amt, 0 ),
				'sliderLoanAmt'         => $loanDetail->Loan_Amt,
				'interestRate'          => $loanDetail->APR_Val * 100,
				'lowRiskScoreVal' 		=> $rateMap['Low_Risk_Score_Val'],
				'highRiskScoreVal'		=> $rateMap['High_Risk_Score_Val'],
				'minLoanAmount'         => number_format($rateMap['Absolute_Min_Loan_Amt'],0,'',''), 
				'maxLoanAmount'         => number_format($loanDetail->Max_Loan_Amt, 0,'',''),
				'affrLoanAmount'		=> $loanDetail->Affordable_Loan_Amt, 
				'monthlyPayment'        => number_format($loanDetail->Monthly_Payment_Amt, 2 ),
				'EstRRDiscount'         => 0.45,
				'EstMnthlyPymntAfrRwrd' => number_format($estMnthlyPymntAfrRwrd, 2 )
			);

			$result['rrLoanAmount'] 		   = number_format($data['rrLoanAmount'], 0);
			$result['rrInterestRate']     	   = $data['rrInterestRate'];
			$result['rrMonthlyPayment']   	   = number_format($data['rrMonthlyPayment'], 2);
			$result['rrEstMnthlyPymntAfrRwrd'] = number_format($estMnthlyPymntAfrRwrd, 2 );

			return Response::json( $result );

		} else {
			$this->layout->content = View::make('blocks.application.rates', $data );
		}

	}

	/**
	 * Show Loan Rate - Step 4 Loan Application
	 * 	  - Use usp_Loan_Decision
	 * 	  - Check ARS Scoring
	 * 	  - Calculate Debt to Income Ratio
	 * 	  - Calculate Loan Amount
	 * 	  - Calculate Loan Payment
	 * 
	 * @return
	 */
	protected function insertLoanRate()
	{	
		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat();
	
		if( !empty($redirect) )
			return Redirect::to($redirect);

		$userId    = getUserSessionID();
		$loanAppNr = getLoanAppNr(); 
		//pre(getLoanSessionData()); die;
		//Get Loan Application Data based on Request Method
		if (Request::isMethod('post')){

			$post = Input::all(); 	

			$loan['loanAmt']  	   = str_replace(',', '', $post['loanAmt']);
			$loan['loanProductId'] = ( isset($post['rr']) && $post['rr'] == 'yes') ? 2 : 1;
			//Update Loan Product Id
			Session::put('loan.loanProductId', $loan['loanProductId']);

			//Update Loan Session Data
			setLoanSessionData(
				array(
					'loanAmt' => $loan['loanAmt'], 
					'loanProductId' => $loan['loanProductId']
				)
			); 

		} else{
			$loan = getLoanSessionData(); 
		}
 		//pre(getLoanSessionData()); die;
		$isBankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanAppNr);
   
		//Check for the ARS Score
		$score = Session::get('ARSScore'); 
		$score = (int) $score;

		//Redirect Session If Score Is Empty
		if( empty($score) ){
			return Redirect::to('disqualification');
		}

 		//Set Active Step in Session
		setActiveStepSession(4);

		//Set Loan Application Phase
		$loanAppPhaseNr = 5; 
		setLoanAppPhase( $userId , $loanAppNr, $loanAppPhaseNr );

		//Override the Loan Detail Records
		if (Request::isMethod('post')){

	 		LoanDetail::where( array(	
						'Borrower_User_Id' => $userId, 
						'Loan_Id'		   => $loanAppNr
					)
				)->update(
					array(
						'Loan_Amt'     => $loan['loanAmt'],
						'Loan_Prod_Id' => $loan['loanProductId']
					)
				);
		}


		$whereParam = array('User_Id' => $userId);  
        $applicantAddress = new ApplicantAddress();
        $applicantAddress_fields = array(
            'State_Cd'
        );
        $addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);

		//Get Loan Decisioning Results using ODS.dbo.usp_Loan_Decision
 		$isLoanApproved = DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?,?,?", 
								array(
									$addressData->State_Cd
									,$userId
									,$score
									,$loan['loanProductId']
									,$isBankAcctLinked
									,'Y'
								)
							); 		
		//Get Loan Application Detail
 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
								->where('Loan_Id','=',$loanAppNr)
								->first();

 		
		//Get Applicant Information
		$param = array('User_Id' => $userId);	
		$isRow = true;
		
		$applFields = array(
			'First_Name',
			'Last_Name',
			'Social_Security_Nr'
		);
		
		$applData = Applicant::getData( $applFields, $param, $isRow );
		
		//Get Applicant Address Information
		$applAddressFields = array(
			'Street_Addr_1_Txt',
			'City_Name',
			'State_Cd',
			'Zip_Cd',
			'Zip_4_Cd'
		);

		$applAddressData = ApplicantAddress::getData( $applAddressFields, $param, $isRow );

		//Get Loan Template
		$LoanTemplate = LoanTemplate::find( $loanDetail->Loan_Template_Id );

		//Get Loan Group
		$loanGroup = LoanGroup::find( $loanDetail->Loan_Grp_Id );

		//Get Loan Portfolio
		$loanPortfolio = LoanPortfolio::find( $loanDetail->Loan_Portfolio_Id );

		//NLS Loan Status Updates Param
		$nlsParam = array(
			'nlsLoanTemplate' => $LoanTemplate->Loan_Template_Name,
			'contactId' 	  => $userId,
			'nlsLoanGroup' 	  => $loanGroup->Loan_Grp_Name,
			'nlsLoanPortfolio'=> $loanPortfolio->Loan_Portfolio_Name,
			'loanId' 		  => $loanAppNr,
			'firstName'		  => $applData->First_Name,
			'lastName' 		  => $applData->Last_Name, 
		);

		//Check Whether the Applicant is Approved or Not
		if( count($isLoanApproved) > 0 && isset($isLoanApproved[0]->Loan_Qual_Flag ) ) {
			if( $isLoanApproved[0]->Loan_Qual_Flag != 1) {
				$this->rejectApplication( $nlsParam , $loanAppNr );	
 				return Redirect::to('disqualification');
 			}
		}

		//Estimated Monthly Payment After Reward 		
 		$estMnthlyPymntAfrRwrd  = $this->calculateRRPaymentAfterReward(
 											$loanDetail->Loan_Amt, 
 											$loanDetail->Monthly_Payment_Amt 
 										);

 		//Get Loan Rate Map
 		$rateMap   = $this->getARSScoreToRateMap( $score, $loan['loanProductId'], $isBankAcctLinked , $applAddressData->State_Cd ); 

 	  	$data = array(
			'loanAmount'            => $loanDetail->Loan_Amt,
			'interestRate'          => $loanDetail->APR_Val * 100,
			'lowRiskScoreVal' 		=> $rateMap['Low_Risk_Score_Val'],
			'highRiskScoreVal'		=> $rateMap['High_Risk_Score_Val'],
			'minLoanAmount'         => $rateMap['Absolute_Min_Loan_Amt'], 
			'maxLoanAmount'         => $loanDetail->Max_Loan_Amt, 
			'affrLoanAmount'		=> $loanDetail->Affordable_Loan_Amt, 
			'monthlyPayment'        => number_format($loanDetail->Monthly_Payment_Amt, 2),
			'EstRRDiscount'         => 0.45, 							// to be implemented
			'EstMnthlyPymntAfrRwrd' => number_format($estMnthlyPymntAfrRwrd, 2), 		// to be implemented
			'loanType' 				=> $loan['loanProductId'],
			'firstName' 	 		=> $applData->First_Name,
			'lastName'  	 		=> $applData->Last_Name,
			'ssn' 					=> $applData->Social_Security_Nr,
			'city' 			 		=> $applAddressData->City_Name,
            'streetAddress1' 		=> $applAddressData->Street_Addr_1_Txt,
            'State' 		 		=> $applAddressData->State_Cd,
            'zip' 		 	    	=> $applAddressData->Zip_Cd
		);

		//Idology Data
 	  	$data['testMode'] = (strtolower(Config::get('system.Ascend.DebugMode') ) == 'true') ? 1 : 0;
 	  	$data['idologyTestMode'] = (strtolower(Config::get('system.Idology.TestMode') ) == 'true') ? 1 : 0;
 	  		
 	  	//NLS Loan Application Updates
 	  	$fields = array(
				'nlsLoanTemplate' => $LoanTemplate->Loan_Template_Name,
				'contactId' 	  => $userId,
				'loanId' 		  => $loanAppNr,
				'firstName'		  => $applData->First_Name,
				'lastName' 		  => $applData->Last_Name,
				'loanStatusCode'  => 'APPLICATION_STEP3',
				'loanAmount'	  => $loanDetail->Loan_Amt,
				'interestRate'	  => $loanDetail->APR_Val * 100,
				'paymentAmount'	  => $loanDetail->Monthly_Payment_Amt,
				'loanType'		  => ( $loanDetail->Loan_Prod_Id == 2 )? 'RR' : 'APL', // loanProdname
				'LOANDETAIL1' 	  => 1,
				'LoanPortfolioName' => '<DEFAULT>'
			);

 	  	$NLS = new NLS();
		$NLS->nlsUpdateLoan($fields);

		$result = array(
			'loanAmount'            => number_format($loanDetail->Loan_Amt, 0 ),
			'sliderLoanAmt'         => $loanDetail->Loan_Amt,
			'interestRate'          => $loanDetail->APR_Val * 100,
			'lowRiskScoreVal' 		=> $rateMap['Low_Risk_Score_Val'],
			'highRiskScoreVal'		=> $rateMap['High_Risk_Score_Val'],
			'minLoanAmount'         => number_format($rateMap['Absolute_Min_Loan_Amt'],0,'',''), 
			'maxLoanAmount'         => number_format($loanDetail->Max_Loan_Amt, 0,'',''),
			'affrLoanAmount'		=> $loanDetail->Affordable_Loan_Amt, 
			'monthlyPayment'        => number_format($loanDetail->Monthly_Payment_Amt, 2 ),
			'EstRRDiscount'         => 0.45,
			'EstMnthlyPymntAfrRwrd' => number_format($estMnthlyPymntAfrRwrd, 2 )
		);

		return Response::json( $result );
	}

	/**
	 * Calculate RateRewards Payment After Reward
	 * 
	 * @param  integer  $loanAmount
	 * @param  integer  $monthlyPayment
	 * @param  integer  $loanTerm
	 * @return
	 */
	protected function calculateRRPaymentAfterReward( $loanAmount , $monthlyPayment, $loanTerm = 36 )
	{
		$discount = ($monthlyPayment * $loanTerm) - $loanAmount;
		$discount = ( $discount *  self::RR_DEFAULT_RATE ) / $loanTerm;  

		return $monthlyPayment - $discount;
	}
 
	/**
	 * Reject Loan Application and Update status code in both
	 * NLS and ODS to APPLICATION_REJECTED
	 * 
	 * @param  array  $nlsParam
	 * @return void
	 */
	protected function rejectApplication( $nlsParam = array(), $loanAppNr )
	{

		if( count($nlsParam) > 0 ) {

			$NLS = new NLS(); 

			$nlsParam['loanStatusCode']  = 'APPLICATION_REJECTED';

			//Update NLS Loan Status Code
			$NLS->nlsUpdateLoanStatus($nlsParam);

			//Update ODS Loan Status Code
			updateLoanStatus($loanAppNr, 'APPLICATION_REJECTED');

			//Set Loan Status Code into Session
			setLoanSessionData( array( 'statusDesc' => 'APPLICATION_REJECTED' ) ); 
			
		}
	}

	/**
	 * Get Maturity Date
	 * 
	 * @param  integer $terms  - Number of Months
	 * @param  string  $startingDate
	 * @return 
	 */
	public function getMaturityDate( $terms = 36, $startingDate = "" )
	{
		return date('Y-m-d', strtotime("+36 months", strtotime($startingDate) ) );
	}

	/**
	 * Get Current Intereset Balance base on Loan Amount
	 * 
	 * @param  integer $loanAmount
	 * @param  integer $loanRate
	 * @return float
	 */
	public function getCurrentInterestBal( $loanAmount = 0 , $loanPaymentTotalAmt = 0 )
	{
		// return $loanAmount - ( $loanAmount * $loanRate ); 
		return ( $loanPaymentTotalAmt - $loanAmount ); 
	}
 
	/**
	 * Update Loan Session Data
	 * 
	 * @return
	 */
	protected function updateLoanSessionData()
	{
		$data =  Input::all();
		setLoanSessionData($data);

		return Response::json(getLoanSessionData());
	}

	/**
	 * Update Consent Data
	 * 
	 * @param  array  $consentData
	 * @return
	 */
	protected function updateConsent( $consentData = array() )
	{
		if( count( $consentData ) > 0 ) { 

			$ApplConsAccpt = new ApplicantConsentAcceptance();
			$ApplConsAccpt->Applicant_User_Id   	= Session::get('uid');
			$ApplConsAccpt->Loan_App_Nr  	    	= Session::get('loanAppNr');
			$ApplConsAccpt->Consent_Version_Nr  	= $consentData['consentVersionNr'];
			$ApplConsAccpt->State_Cd  				= $consentData['stateCd'];
			$ApplConsAccpt->Consent_Type_Id			= $consentData['consentTypeId'];
			$ApplConsAccpt->Consent_Acceptance_Dt	= date('Y-m-d');
			$ApplConsAccpt->Created_By_User_Id		= Session::get('uid');
			$ApplConsAccpt->Create_Dt 				= date('Y-m-d');
  			$ApplConsAccpt->save();
		}
	}

	///////////////////////////////////////////////////////////////
	//                      LOAN DECISIONING                     //
	///////////////////////////////////////////////////////////////
	/**
	 * Get ARS Scores and other Loan Decisioning Details from Loan_Prod_Rate_Map table
	 * 
	 * @param  integer $scoreRange
	 * @param  integer $loanProductId
	 * @param  integer $isLinkBankAcct
	 * @return array
	 */
	protected function getARSScoreToRateMap( $scoreRange = 0 , $loanProductId = 0, $isLinkBankAcct = 'N', $stateCd = 'CA' )
	{ 
		return LoanProdRateMap::getLoanProdRateMap(
			$loanProductId, 
			$scoreRange, 
			$isLinkBankAcct,
			$stateCd
		);
	}

	/**
	 * Calculate Affordable Loan Amount
	 *
	 * @param  double $monthlyIncome
	 * @param  double $monthlyExpenses
	 * @return
	 */
	protected function calculateAffordablePaymentAmt( $monthlyIncome = 0, $monthlyExpenses = 0 )
	{ 
		//get current debt to income ration 
		$currDebToIncomeRatio = $monthlyExpenses / $monthlyIncome;
 
		//maximum det to incom ratio
		$maxDebtToIncomeRatio = self::MAX_DTIR_PERCENTAGE;
 
		//affordable monthly payment 
		$affMonthlyPayment =  ( $maxDebtToIncomeRatio - $currDebToIncomeRatio  ) * $monthlyIncome;

		return array(
			'maxDebtToIncomeRatio' => $maxDebtToIncomeRatio, 
			'currDebToIncomeRatio' => $currDebToIncomeRatio, 
			'affMonthlyPayment'	   => $affMonthlyPayment
		); 
	}

	/** 
	 * Get Monthly Expenses by an Applicant
	 * 
	 * @param  integer $userId
	 * @return
	 */
	protected function getMonthlyExpenses( $userId = NULL )
	{
		$expense = 0; 
		
		$result  =  TUCredit::where('User_Id', '=', $userId)
							->where('Credit_Pull_Type_Desc', '=', 'initial')
							->select('Credit_Monthly_Payment_Amt')
							->first();		

		if( !empty( $result ) ){
			$expense = ( $result->Credit_Monthly_Payment_Amt != "" ) ? $result->Credit_Monthly_Payment_Amt  : 0 ;
		}

		return $expense;
	}

	///////////////////////////////////////////////////////////////
	//                      LOAN RATE TESTING                    //
	///////////////////////////////////////////////////////////////
  
	/**
	 * Test Harness for LoanRate
	 */
	public function testLoanRate()
	{	
		// $userId    = getUserSessionID();
		// $loanAppNr = 1453;

		//Get Bank Account Linked Flag 
		$isBankAcctLinked = 'N';
		$BankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanAppNr);

		if( isset( $BankAcctLinked->Bank_Acct_Linked_Flag) && $BankAcctLinked->Bank_Acct_Linked_Flag == 'Y' ) {
			$isBankAcctLinked = $BankAcctLinked->Bank_Acct_Linked_Flag;
		}

		_pre($isBankAcctLinked);


		//Checkf for the ARS Score
		$score          = Session::get('ARSScore'); 
		$score          = 319.36;
		$isLinkBankAcct = 'N';

		//redirect to home page if no score 
		//parameter passed
		// if( empty($score) || $score < 100)
		// 	return Redirect::to('disqualification');

		//get loan, user id and loan app number
		//from session. Assume these data have 
		//been set when the user logged in
		$loan      = getLoanSessionData();
		$userId    = getUserSessionID();
		$loanAppNr = getLoanAppNr(); 

		pre($loan);

		echo $userId;

		// dd();
		$loanDecisioningSP = DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?", 
								array(
									 $userId
									,$score
									,$loan['loanProductId']
									,'N'
								)
							);



		if( count($loanDecisioningSP) > 0 && isset($loanDecisioningSP[0]->Loan_Qual_Flag )) {
			//Redirect to Disqualification
			if( $loanDecisioningSP[0]->Loan_Qual_Flag != 1) {
				$this->rejectApplication( $nlsParam , $loanAppNr );	
 				return Redirect::to('disqualification');
 			} else {

 			}
		}


		$rateMap   = $this->getARSScoreToRateMap( $score, $loan['loanProductId'], $isLinkBankAcct ); 

		_pre($loanDecisioningSP);
 	

		//If no ARS Score Rate Map Match, Reject the Application
		if( count($rateMa)  == 0 )
			return Redirect::to('disqualification');

		//Get Applicant Financial Information
		$ApplFinancial = getApplicantFinancialInfo(
							  $userId
							, array('Annual_Gross_Income_Amt', 'Monthly_Rent_Amt')
							, true 
						);
 	
 		//Calculate Monthly Income
		$monthlyIncome        = $ApplFinancial->Annual_Gross_Income_Amt / 12 ;
		//Get Credit Profile Monthly Expenses
		$crProfMntlyExpnse    = $this->getMonthlyExpenses($userId);
		//Calculate Monthly Expenses
		$monthlyExpenses      = $crProfMntlyExpnse + $ApplFinancial->Monthly_Rent_Amt;
		//Get Affordable Monthly Payment
		$affMonthlyPayment    = $this->calculateAffordablePaymentAmt($monthlyIncome, $monthlyExpenses);

		// dd($affMonthlyPayment)

		//Get Applicant and Application Address 
		$param = array('User_Id' => $userId);	
		$isRow = true;
		
		$applFields = array(
			'First_Name',
			'Last_Name',
			'Social_Security_Nr'
		);
		
		$applData = Applicant::getData( $applFields, $param, $isRow );
		
		$applAddressFields = array(
			'Street_Addr_1_Txt',
			'City_Name',
			'State_Cd',
			'Zip_Cd',
			'Zip_4_Cd'
		);

		$applAddressData   = ApplicantAddress::getData( $applAddressFields, $param, $isRow );

		//Get Loan Application Detail
 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
								->where('Loan_Id','=',$loanAppNr)
								->first();

		$LoanTemplate  = LoanTemplate::find( $loanDetail->Loan_Template_Id );
		$loanGroup     = LoanGroup::find( $loanDetail->Loan_Grp_Id );
		$loanPortfolio = LoanPortfolio::find( $loanDetail->Loan_Portfolio_Id );

		//NLS Loan Status Updates Param
		$nlsParam = array(
			'nlsLoanTemplate' => $LoanTemplate->Loan_Template_Name,
			'contactId' 	  => $userId,
			'nlsLoanGroup' 	  => $loanGroup->Loan_Grp_Name,
			'nlsLoanPortfolio'=> $loanPortfolio->Loan_Portfolio_Name,
			'loanId' 		  => $loanAppNr,
			'firstName'		  => $applData->First_Name,
			'lastName' 		  => $applData->Last_Name, 
		);
   
		//Set MaxLoan Amount
		$absoluteMaxLoanAmt   = $rateMap['Absolute_Max_Loan_Amt'];
 		//Set APR for Loan
		$APR                  = $rateMap['APR_Val'];
		//Get the interest loan rate
		$absAPR		          = abs($APR * 100);

		//calcukate the monthly interest
		$monthlyInterestRate  = $APR / 12;

		//Get Affordable Loan Factory
		$affordableLoanFactor = AffordableLoanFactor::where('Interest_Rate_Val', '=', $absAPR )->first();
		//Set Loan Term. @todo - we might need to have a Loan Term Table
		$loanTerm   		  = 36;
		//Loan Term * Affordable Monthly Payment * Principal Percentage
		$computedLoanAmount      = $affMonthlyPayment['affMonthlyPayment'] * $loanTerm * $affordableLoanFactor->Principal_Pct; 

		// dd($computedLoanAmount);
		
		if( $computedLoanAmount > $absoluteMaxLoanAmt ) {
			$loanAmount         = $absoluteMaxLoanAmt; 
			$computedLoanAmount = $absoluteMaxLoanAmt;
		} else{
			$loanAmount = $computedLoanAmount;
		}
 	
 		//Check if the final amount is less than self::ABS_MIN_LOAN_AMT = 2600
		if( $loanAmount < self::ABS_MIN_LOAN_AMT ){
 			// $this->rejectApplication( $nlsParam , $loanAppNr );	
 			// return Redirect::to('disqualification');
		}else{
			
			//Comparison for Loan Application ( CREATE )
			if( isset( $loan['loanAmt'] ) && !empty( $loan['loanAmt'] ) ) {
				if( $loan['loanAmt'] < $loanAmount ){
					$loanAmount = $loan['loanAmt'];
				}
			}
		}
 
		//Calculate Monthly Payment  
		$monthlyPayment  = pmt( $absAPR, 36, $loanAmount );
		//Calculate Loan Payment Total Amount
		$loanPaymentTotalAmt = $monthlyPayment * $loanTerm; 
		//Calculate Current Interest Balanace Amount
		$currentInterestBalAmt = $this->getCurrentInterestBal( $loanAmount, $loanPaymentTotalAmt );
 
 
		$data['actual']  = array(
			'AnnualGrossIncome' => $ApplFinancial->Annual_Gross_Income_Amt,
			'TotalMonthlyExp'   => $monthlyExpenses,
			'ARS'   			=> number_format( $score, 0 ), 
			'LoanTypeId' 		=> $loan['loanProductId'],
		);
 
 	  	$data['result'] = array(
			'Annual Gross Income'                                                      => '$' . number_format( $ApplFinancial->Annual_Gross_Income_Amt, 2 ),
			'Monthly Income'                                                           => '$' . number_format( $monthlyIncome, 2),
			'Monthly Rent'                                                             => '$' . number_format( $ApplFinancial->Monthly_Rent_Amt, 2), 
			'Credit Profile Monthly Expenses'                                          => '$' . number_format( $crProfMntlyExpnse, 2 ), 
			'Total Monthly Expenses (Credit Profile Monthly Expenses + Monthly Rent )' => '$' . number_format( $monthlyExpenses, 2), 
			'Max Debt to Income Ratio'                                                 => $affMonthlyPayment['maxDebtToIncomeRatio'],
			'Current Debt to Income Ratio ( Monthly Expenses / Monthly Income )'       => $affMonthlyPayment['currDebToIncomeRatio'],
			'Affordable Monthly Payment'                                               => '$' . number_format( $affMonthlyPayment['affMonthlyPayment'], 2),
			'Monthly Payment'                                                          => '$' . number_format($monthlyPayment, 2),
			'Principal Base Amount'                                                    => '$' . number_format( $affordableLoanFactor->Principal_Pct, 2 ),
			'Affordable Loan Amount'                                                   => '$' . number_format( $computedLoanAmount, 2 ), 
			'Loan Amount'                                                              => '$' . number_format( $loanAmount, 2 ), 
			'Loan Term'                                                                => $loanTerm,
			'ARS (Score)'                                                              => number_format( $score, 0 ),
			'Annual Percentage Rate (APR)'   										   => $absAPR . '%',
			'Monthly Interest Rate'													   => ( $monthlyInterestRate * 100 ) . '%',
			'Low Risk Score Value'                                                     => $rateMap['Low_Risk_Score_Val'],
			'High Risk Score Value'                                                    => $rateMap['High_Risk_Score_Val'],
			//'EstRRDiscount'                                                          => 0.45, //Estimated RR Discount
			//'EstMnthlyPymntAfrRwrd'                                                  => 0,    //$affMonthlyPayment['affMonthlyPayment'] - ( $affMonthlyPayment['affMonthlyPayment'] * 0.45 ),  // to be implemented
			'Loan Type Id'                                                             => $loan['loanProductId'],
		);
 		
 		$this->layout->content = View::make('blocks.harness.rates', $data );
	}
	
	///////////////////////////////////////////////////////////////
	//                     NLS                                   //
	///////////////////////////////////////////////////////////////
	/**
	 * NLS Loan process / Credit Check
	 *
	 * @param  
	 * @return Response
	 */
	public function createLoan() 
	{	
		$this->layout = NULL; 

		$userId        = getUserSessionID();
		$loan          = getLoanSessionData();
		$loanAppNr	   = getLoanAppNr();
		$flag          = false;
		$loanDetail    = '';
		$loanPortfolio = '';
		$loanTemplate  = '';
		$loanGroup     = '';

		$NLS = new NLS();

		//Fetch User Information
		$user = new User();
		$userFields = array(
				'Password_Txt',
				'Email_Id',
				'Alt_Email_Id'
			);
		$whereParam = array('User_Id' => $userId);	
		$userData = $user->getData( $userFields, $whereParam, true );
			
		//Fetch Applicant Information
		$applicant = new Applicant();
		$applicantFields = array(
			'User_Id',
			'First_Name',
			'Middle_Name',
			'Last_Name',	
			'Age_Cnt',
			'Birth_Dt',
			'Social_Security_Nr',
			'Yodlee_User_Name'
		);
			
		$applicantData = $applicant->getData( $applicantFields, $whereParam, true );
		
		//Fetch Applicant Address Information
		$applicantAddress = new ApplicantAddress();
		$applicantAddressFields = array(
			'Street_Addr_1_Txt',
			'Street_Addr_2_Txt',
			'City_Name',
			'State_Cd',
			'Zip_Cd',
			'Zip_4_Cd',
			'Validity_Start_Dt',
			'Validity_End_Dt'
		);
		$addressData   = $applicantAddress->getData( $applicantAddressFields, $whereParam, true );

		//Fetch Applicant Contact Information
		$applicantContact = new ApplicantContactInfo();
		$applicantContactFields = array(
			'Home_Phone_Nr',
			'Mobile_Phone_Nr',
			'Validity_Start_Dt',
			'Validity_End_Dt'
		);

		$contactData   = $applicantContact->getData( $applicantContactFields, $whereParam, true );

		//Fetch Applicatn Financial Information
		$financialData = ApplicantFinancialInfo::where('User_Id', '=', $userId)->first();

		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
				->where('Loan_Id','=',$loanAppNr)
				->first();



		//Check whether the NLS Loan Object is 
		//Already Created
		if( NLSLoanAcct::isLoanAcctExist($loanAppNr) > 0 ) {

			//update Loan Object
			$fields = array(
				'nlsLoanGroup' 		=> $loanDetail->Loan_Grp_Id,
				'loanId' 			=> $loanDetail->Loan_Id,
 				'nlsLoanTemplate'	=> $loanDetail->Loan_Template_Id,
				'nlsLoanPortfolio'	=> $loanDetail->Loan_Portfolio_Id,
				'lastName'			=> $applicantData['First_Name'],
				'firstName'			=> $applicantData['Last_Name'],
				'contactId'			=> $loanDetail->Borrower_User_Id,
				'LOANDETAIL1'		=> 1,
				'loanPurpose'		=> $loan['loanPurposeTxt']

			);

			$nlsUpdateStatus = $NLS->nlsUpdateLoan($fields);					
			

		}

		//Return an Error if there's is no Loan Detail Record
		if( count($loanDetail) > 0 ) {
	 
			$lt = LoanTemplate::where('Loan_Template_Id', '=', $loanDetail->Loan_Template_Id)->first();	
			$loanTemplate = $lt->Loan_Template_Name;
			
			$lg = LoanGroup::where('Loan_Grp_Id', '=', $loanDetail->Loan_Grp_Id)->first();	
			$loanGroup = $lg->Loan_Grp_Name;

			$lp = LoanPortfolio::where('Loan_Portfolio_Id', '=', $loanDetail->Loan_Portfolio_Id)->first();	
			$loanPortfolio = $lp->Loan_Portfolio_Name;

			
	        $fields = array(
				'contactId' 		=> $userId,
				'loanId' 		    => $loanAppNr,
				'age'				=> $applicantData->Age_Cnt,
	            'email' 			=> $userData->Email_Id,
				'firstName' 	 	=> $applicantData->First_Name,
	            'middleName'  	 	=> $applicantData->Middle_Name,
	            'lastName'  	 	=> $applicantData->Last_Name,
	            'birthDate' 	 	=> $applicantData->Birth_Dt,
	            'ssn' 				=> $applicantData->Social_Security_Nr,
	            'city' 			 	=> $addressData->City_Name,
	            'streetAddress1' 	=> $addressData->Street_Addr_1_Txt,
	            'streetAddress2' 	=> $addressData->Street_Addr_2_Txt,
	            'State' 		 	=> $addressData->State_Cd,
				'zip' 		 	    => $addressData->Zip_Cd,
	            'homePhoneNumber'   => $contactData->Home_Phone_Nr,
	            'mobilePhoneNr' 	=> $contactData->Mobile_Phone_Nr,
				'rentOwn' 		 	=> ($financialData->Rented_Home_Flag) ? 1 : 3,
	            'rent_amount' 	 	=> $financialData->Monthly_Rent_Amt,
	            'annualGrossIncome' => $financialData->Annual_Gross_Income_Amt,
	            'employeeStatus' 	=> $financialData->Employment_Status_Desc,
	            'loanStatusCode' 	=> 'APPLICATION_STEP1',
	            'loanAmount' 		=> (int)$loan['loanAmt'],
	            'loanPurpose'	 	=> $loan['loanPurposeTxt'],
	            'loanProductId' 	=> $loan['loanProductId'],
	            'creditBureauHeader'=> Config::get('system.NLS.InitialPull'),
	        	'nlsLoanPortfolio'	=> $loanPortfolio,
	            'nlsLoanTemplate'	=> $loanTemplate,
	            'nlsLoanGroup'		=> $loanGroup,
	            'term'				=> 36,
	            'ip'				=> $_SERVER['REMOTE_ADDR']
			);
	         
		
			$NLS->NLSUpdateContact($fields);

			if( $NLS->NLSCreateLoan( $fields ) ){
				

				//Evaluate Account Delinquencies
				if( $financialData->Last_Bank_Acct_Delinquency_Id > self::MIN_DELINQUENT_PERIOD  )
				{
					writeLogEvent('account delinquencies Failed', 
						array( 
							'User_Id'     => $loanDetail->Borrower_User_Id,
							'Loan_App_Nr' => $loanDetail->Loan_Id,
							'Message'     => $financialData->Annual_Gross_Income_Amt 
						),
						'warning'
					);

					return Response::json(
						array(
							'result'  => 'failed credit', 
						), 200
					);

				}


				//Evaluate Annual Income 
				if( $fields['annualGrossIncome'] < self::MIN_ANNUAL_INCOME ) {

					writeLogEvent('Annual Gross Income Failed', 
						array( 
							'User_Id'     => $userId,
							'Loan_App_Nr' => $loanAppNr,
							'Message'     => $fields['annualGrossIncome']
						),
						'warning'
					);

					return Response::json(
						array(
							'result'  => 'failed income', 
						), 200
					);
				} else {
					return Response::json(
						array(
							'result'  => 'success',
							'gross'   => $fields['annualGrossIncome'],
							'max'     => self::MIN_ANNUAL_INCOME
						), 200
					);
				}
				
			} else {
				return Response::json(
						array(
							'result'  => 'failed loan'
						), 200
					);
			}		
		} else {
			return Response::json(
					array(
						'result'  => 'failed loan'
					), 200
				);
		}	
	}

	/**
	 * Save NLS Object
	 * 
	 * @return
	 */
	public function saveNLS()
	{
		$userId    = getUserSessionID();
		$loanAppNr = getLoanAppNr();
		//save NLS data
		$NLS  = new NLS(); 
		$NLS->saveNLS($userId, $loanAppNr); 
	}

	/**
	 * Final Credit Pull
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return
	 */
	protected function finalCreditPull( $userId, $loanAppNr )
	{

		$NLS       = new NLS();
		$applicant = new Applicant();

		$applicantFields = array( 'First_Name','Last_Name');
		$applicantData   = getApplicant(  $userId, $applicantFields, TRUE );
		
        $fields = array(
			'contactId'          => $userId,
			'loanId'             => $loanAppNr,
			'nlsLoanTemplate'    => $this->loanTemplateName,
			'loanId'             => $this->loanGroupName,
			'lastName'           => $applicantData->Last_Name,
			'firstName'          => $applicantData->First_Name,
			'creditBureauHeader' => Config::get('system.NLS.HardPull')
		);

		$NLS->NLSCreditPull($fields);

		$this->parseXML($userId, $loanAppNr, 'final');
	}
	/**
	 * Display the credit soft pull data based on the User ID of the login person
	 * Used for testers
	 * @param  
	 * @return Response
	 */
	public function showCreditPullInfo()
	{
		
		if (Input::hasFile('IdolofileToUpload'))
		{
		    $file = Input::file('fileToUpload');
		    Input::file('fileToUpload')->move($destinationPath);
		}

		$uid = 0;
		$uid = getUserSessionID();

		// dd();

		if($uid == 0){
			return Redirect::to('login');
		}else{
			print $uid;

			// fetch all the user data

			$user = new User();
			$user_fields = array(
					'User_Name', 'User_Id', 'Password_Txt', 'Remember_Token_Txt', 'Email_Id', 'Alt_Email_Id', 'User_Role_Id', 'User_Ref_Nr', 'Credit_Pull_Auth_Flag', 'Created_By_User_Id', 'Create_Dt'
				);
			
			$whereParam = array('User_Id' => $uid);	
			$userData = $user->getData($user_fields, $whereParam);
			print '<pre>';
			print '<br />User Account Details:<br />';
			print_r($userData[0]);
			
			$applicant = new Applicant();
			$applicant_fields = array(
				'User_Id',
				'First_Name',
				'Middle_Name',
				'Last_Name',	
				'Age_Cnt',
				'Birth_Dt',
				'Social_Security_Nr',
				'Yodlee_User_Name'
			);
			$applicantData = $applicant->getData($applicant_fields, $whereParam, true);
			
			print '<br />Applicant Details:<br />';
			print_r($applicantData);


			$applicantAddress = new ApplicantAddress();
			$applicantAddress_fields = array(
				'Street_Addr_1_Txt',
				'Street_Addr_2_Txt',
				'City_Name',
				'State_Cd',
				'Zip_Cd',
				'Zip_4_Cd',
				'Validity_Start_Dt',
				'Validity_End_Dt'
			);
			$addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);

			$applicantContact = new ApplicantContactInfo();
			$applicantContact_fields = array(
				'Home_Phone_Nr',
				'Mobile_Phone_Nr',

			);
			$contactData   = $applicantContact->getData($applicantContact_fields,$whereParam,true);

			print '<br />Applicant Address:<br />';
			print_r($addressData);
		
			print '<br />Applicant Contact:<br />';
			print_r($contactData);
			
			$financialData = ApplicantFinancialInfo::where('User_Id', '=', $uid)->get();

			print '<br />Applicant Financial:<br />';
			print_r($financialData[0]['attributes']);


			$fields = array(
					'firstName'  => $applicantData->First_Name,
					'middleName' => $applicantData->Middle_Name,
					'lastName'   => $applicantData->Last_Name
				);
			//$creditProfileResults = DB::connection('nls')->select("SELECT * FROM CreditProfile where FirstName='".$fields['firstName']."' and Surname='".$fields['lastName']."'");
			
			$cifResult = DB::connection('nls')->select("SELECT top 1 cifno FROM cif where cifnumber='".$uid."'");
        
        	$creditProfileResults = DB::connection('nls')->select('SELECT * FROM CreditProfile where cifno='.$cifResult[0]->cifno);

			//$creditProfileResults = NlsCreditProfile::where("FirstName",'=',$fields['firstName'])->where("Surname",'=',$fields['lastName'])->get();
			print '<br />Credit Profile:<br />';
			print_r($creditProfileResults); 
			$CreditProfileID = 0;
			foreach($creditProfileResults as $creditProfile){
	            $CreditProfileID = $creditProfile->CreditProfileID;
	        }

			//$creditTradeLineResults = DB::connection('nls')->select('SELECT * FROM CreditTradeLine where CreditProfileID='.$CreditProfileID.'');
            $creditTradeLineResults = NlsCreditTradeLine::where('CreditProfileID', '=', $CreditProfileID)->get();
            print '<br />Credit Tradeline:<br />';
			print_r($creditTradeLineResults); 

			$creditCollectionResults = DB::connection('nls')->select('SELECT * FROM CreditCollection where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditCollectionResults:<br />';
			print_r($creditCollectionResults); 

			$creditRiskModelResults = DB::connection('nls')->select('SELECT * FROM CreditRiskModel where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditRiskModelResults:<br />';
			print_r($creditRiskModelResults); 

			$creditBankruptcyResults = DB::connection('nls')->select('SELECT * FROM CreditBankruptcy where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditBankruptcyResults:<br />';
			print_r($creditBankruptcyResults); 

			$creditProfilePropertiesResults = DB::connection('nls')->select('SELECT * FROM CreditProfileProperties where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditProfilePropertiesResults:<br />';
			print_r($creditProfilePropertiesResults); 

			$creditProfileSSNResults = DB::connection('nls')->select('SELECT * FROM CreditProfileSSN where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditProfileSSNResults:<br />';
			print_r($creditProfileSSNResults); 

			$creditEmploymentInformationResults = DB::connection('nls')->select('SELECT * FROM CreditEmploymentInformation where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditEmploymentInformationResults:<br />';
			print_r($creditEmploymentInformationResults); 

			$creditAddressInformationResults = DB::connection('nls')->select('SELECT * FROM CreditProfileIdentity where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditAddressInformationResults:<br />';
			print_r($creditAddressInformationResults); 

			$creditTradeLineResults = DB::connection('nls')->select('SELECT * FROM CreditAddressInformation where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditTradeLineResults:<br />';
			print_r($creditTradeLineResults); 

			$creditPublicRecordResults = DB::connection('nls')->select('SELECT * FROM creditPublicRecord where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit creditPublicRecordResults:<br />';
			print_r($creditPublicRecordResults); 

			$creditLegalItemResults = DB::connection('nls')->select('SELECT * FROM CreditLegalItem where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit CreditLegalItem:<br />';
			print_r($creditLegalItemResults); 

			$creditInquiryResults = DB::connection('nls')->select('SELECT * FROM CreditInquiry where CreditProfileID='.$CreditProfileID.'');
	        print '<br />Credit CreditInquiry:<br />';
			print_r($creditInquiryResults);  
		}
	}
 
	/**
	 * Parse NLS TU Attributes Data
	 * 
	 * @param  integer $userId
	 * @param  string  $type
	 * @return
	 */
	public function parseXML( $userId = 0, $loanAppNr = 0,  $type = 'initial' )
	{
		$NLS = new NLS();
		$NLS->parseXML($userId, $loanAppNr, $type );
    }
  
    /** 
     * Perform Exclusion checks
     * 
	 *    1. Application Exclusion check 
	 *    2. Credit Bureau Exclusion - after soft credit pull from TU
	 *    3. Checking Account Exclusion(DDA) - from Yodlee stuff
	 *    4. Ascend Risk Score Exclusion - happens if the ARS > 30
	 *
	 * @param  string  $type
	 * @param  integer $test
	 * @param  array   $testData
	 */
	public function execCreditExclusion( $type = 'initial', $test = 0, $testData = array() )
	{
		$this->layout   = NULL;
		$exclusions     = array();
		$adverseActions = array();
		$score          = 0;
		$isPassed       = TRUE;
		$bankFlag       = 0;
		
		Session::put('type', $type);

		$NLS = new NLS();
		
		$uid = getUserSessionID();
		$lid = getLoanAppNr();

		//Save Credit Profile and TU Attributes data from NLS
		if( $type == 'initial' )
 			$this->saveCreditProfileAndTU( $uid, $lid );

		//Get Loan Detail
		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $uid)
					->where('Loan_Id','=', $lid )
					->first();
 
		$this->loanTemplateName  = LoanTemplate::getLoanTemplateName($loanDetail->Loan_Template_Id);
		$this->loanGroupName     = LoanGroup::getLoanGroupName($loanDetail->Loan_Grp_Id );
		$this->loanPortfolioName = LoanPortfolio::getLoanPortfolioName($loanDetail->Loan_Portfolio_Id);	
		
		//Final Credit Pull
		if( $type == 'final' ){
			try{
 				$this->finalCreditPull($uid, $lid);
			}catch(Exception $e) { 
					writeLogEvent('Final Credit Pull', 
					array( 
						'User_Id'     => $uid,
						'Loan_App_Nr' => $lid,
						'Message'     => $e->getMessage()
					),
					'warning'
				); 
			}
		}

		//Get TU Credit Data
		$tuData = TUCredit::where('User_Id', '=', $uid)
							->where('Credit_Pull_Type_Desc', '=', trim($type))
							->first();

		$applicantFields = array(
			'First_Name',
			'Last_Name'
		);

		$applicantData = getApplicant($uid, $applicantFields, true);

		$applicantAddressFields = array(
			'Street_Addr_1_Txt',
			'Street_Addr_2_Txt', 
			'City_Name', 
			'State_Cd'
		);

		$applicantAddress  = getApplicantAddress($uid, $applicantAddressFields, true);

		///////////////////////////////////////
		//      PRODUCTION QA BYPASS         //
		///////////////////////////////////////
		if( Session::get('userRoleId') == self::QA_User_Role_Id ) {

			$score = self::QA_DEFAULT_ARS_SCORE;
			ApplicantRiskInfo::storeFailedARSScore($lid, $uid, $score);

			Session::put('ARSScore',$score);

			if( $type == 'final' ){
				return Redirect::to('verification/steps');
			}else{
				return Redirect::to('offer');
			}	
		} else{
		///////////////////////////////////////
		//      SCORING ALGORITHM            //
		///////////////////////////////////////
			if( sizeOf($tuData) > 0 ){
				$scoreResults   = $this->getCreditScore($type, $test, $testData );
				$score          = $scoreResults['score'];
				$adverseActions = $scoreResults['adverseActions'];
			} else {
				$score = self::DEFAULT_ARS_SCORE;
				ApplicantRiskInfo::storeFailedARSScore($lid, $uid, $score);
			}
		}
 
		///////////////////////////////////////
		//          BANK CHECKING            //
		/////////////////////////////////////// 
		if( $test == 0 )
			$bankFlag = $this->updateNLSBankAccountInfo($uid, $lid);
 		

		$partnerId = '1';
		$stateCd   = 'CA';
			
		//////////////////////////////////////////
		//         CREDIT EXCLUSION             //
		//////////////////////////////////////////
		$exclusions    = ExclusionVersion::getCreditExclusion($uid);
		$approvedLoans = LoanDetail::getApprovedLoans($uid, $lid);
		
        //Existing loan not paid off
        if( count( $approvedLoans ) > 0)
        	array_push($exclusions, self::A16);

		$data['exclusions'] = $exclusions;
 
		if( count($data['exclusions']) > 0 )
			$isPassed = FALSE;

		$fields = array(
			'nlsLoanTemplate'  => $this->loanTemplateName,
			'nlsLoanGroup'     => $this->loanGroupName,
			'nlsLoanPortfolio' => $this->loanPortfolioName,
			'loanProduct'      => (trim($this->loanGroupName) == 'ASCEND1') ? 'Ascend' : 'Raterewards',  //
			'contactId'        => $uid,
			'loanId'           => $lid,
			'firstName'        => $applicantData->First_Name,
			'lastName'         => $applicantData->Last_Name,
			'address'		   => $applicantAddress->Street_Addr_1_Txt, 
			'city'		   	   => $applicantAddress->City_Name, 
			'state'		   	   => $applicantAddress->State_Cd, 
		);
 
		//Application is REJECTED due to Exclusions
        if( $isPassed == FALSE ){

        	$fields['loanStatusCode'] = 'APPLICATION_REJECTED';

			//Update NLS Loan Status
			$NLS->nlsUpdateLoanStatus($fields);

			//Update ODS table status 
			updateLoanStatus($lid, 'APPLICATION_REJECTED');
			
			//Update Loan Status in Session
			setLoanSessionData( array( 'statusDesc' => 'APPLICATION_REJECTED' ) );

			$user = new User();
			$userFields = array('Email_Id');
			$whereParam = array('User_Id' => $uid);	
			$userData = $user->getData($userFields,$whereParam, true);

			$this->email = $userData->Email_Id;

			$subPhaseCode   = '';

			if( $type == 'final'){
				$loanAppPhaseNr = 6;   //VERIFICATION
			} else {
				$loanAppPhaseNr = 4;   //EXCLUSION AND SCORING
				$subPhaseCode   = 'ExclusionFail';
			}
			
			setLoanAppPhase( $uid, $lid, $loanAppPhaseNr, $subPhaseCode);

			//Set ARS Score to Session
			Session::put('ARSScore',$score);

			if( $type == 'final' ) { 
				$this->sendExclusionScoreToMail($exclusions, $score, $fields, $adverseActions );
				return Redirect::to('disqualification');
			}else{
				
				if ( Config::get('system.Ascend.DebugMode') == 'true' )
					$this->sendExclusionScoreToMail($exclusions, $score, $fields, $adverseActions );
 
				return Redirect::to('disqualification');
			}

		}else{

			//Set ARS Score
			Session::put('ARSScore',$score);

			//Set Loan Status Code
			if( $type == 'final' ){
				$fields['loanStatusCode'] = 'APPLICATION_STEP4';
			}else{
				$fields['loanStatusCode'] = 'APPLICATION_STEP3';
			}

			//Update NLS Loan Status
			$NLS->nlsUpdateLoanStatus($fields);
			
			//Update ODS Loan Status Code
			updateLoanStatus($lid, $fields['loanStatusCode']);

			//Redirection
			if( $type == 'final' ){

				$loanAppPhaseNr = 5;
				$subPhaseCode   = 'OfferAccepted';

				//Set Loan Application Phase to  Final Offer - Email Verified
				setLoanAppPhase( $uid , $lid, $loanAppPhaseNr, $subPhaseCode  );

				return Redirect::to('verification/steps' );
			}else{
				
				//Set Loan Application Phase to Exclusion and Scoring
				$loanAppPhaseNr = 4;
				$subPhaseCode   = 'ExclusionPass';
				setLoanAppPhase( $uid , $lid, $loanAppPhaseNr, $subPhaseCode  );

				return Redirect::to('offer');

			}	
		}
		print 'done';		
	}

	/**
	 * Send Exclusion Score to Email 
	 * 
	 * @param  array $exclusions
	 * @param  float $score
	 * @param  array  $fields
	 * @param  array  $adverseActions
	 * @return 
	 */
	public function sendExclusionScoreToMail( $exclusions , $score, $fields = array(), $adverseActions = array() )
	{
		//Exclusion Information
		$mailData = array(
			'exclusions'    => $exclusions,
			'address'       => $fields['address'].', '.$fields['city'].', '.$fields['state'],
			'date'          => date('m-d-Y'),
			'applicant'     => $fields['firstName'].' '.$fields['lastName'],
			'requestedDate' => date('m-d-Y'),
			'score'         => round($score, 2),
			'description'   => $fields['loanProduct'].' Loan',
			'low'           => ' ',
			'high'          => ' ',
			'phone'         => '800-497-5314',
			'fax'           => '630-578-2342 ',
			'site'          => 'ascendloan.com'
		);
			
		//Prepare Scoring List
	    if (count($adverseActions) > 0) {
	      $scoringList = array(
	          $this->convertToReasonStatement($adverseActions[0]->Adverse_Action_Cd),
	          $this->convertToReasonStatement($adverseActions[1]->Adverse_Action_Cd),
	          $this->convertToReasonStatement($adverseActions[2]->Adverse_Action_Cd),
	          $this->convertToReasonStatement($adverseActions[3]->Adverse_Action_Cd)
	        );
	    }else {
      		$scoringList = array();

			$mailData['scoringList']=  $scoringList;

			Mail::send('emails.auth.scoring-notification', $mailData, function($message)
			{
			    $message->to($this->email, 'Customer')
			    		->subject('Scoring Notification - Ascend Loan');
			});
		}
	}
  
	/**
	 * Disqualification 
	 * 
	 * @param  string $debug
	 * @return
	 */
	public function disqualification( $debug = 'debugMode' )
	{

		$data['debug'] = $debug;

		if( $debug == 'debugMode' )
			$data['debug'] = '';

		if( Session::has('type') )
			$data['type'] = Session::get('type');

		$uid = getUserSessionID();
		$lid = getLoanAppNr();

		// Update Task if KBA or identity
		if($debug == 'identity' || $debug == 'KBA'  ){

			$loanApplication  =  LoanDetail::where('Loan_Id', '=', $lid )->get();

			if( count( $loanApplication ) > 0 ) {
				foreach ($loanApplication as $key => $loan) {
					$status = array(
						'userId' 		=> $uid,
						'NLSRefno' 		=> $loan->Loan_Ref_Nr
					);
					updateNotStartedTasks($lid , $status, self::NLS_STATUS_CODE_NOT_STARTED );
				}
			}
			
		}

		//This will store the type of Disqualification
		writeLogEvent(
			'Disqualification Event', 
			array(
				'User_Id'          => $uid, 
				'Loan_Id'          => $lid, 
				'Disqualification' => isset($data['type']) ? $data['type'] : "", 
				'Debug'            => $debug
			), 
			'warning'
		);

		//Update NLS Loan Status
		$this->updateNLSLoanStatus($uid, $lid, 'APPLICATION_REJECTED');

		//Set Loan Status Code into Session
		setLoanSessionData( array( 'statusDesc' => 'APPLICATION_REJECTED' ) ); 

		//Update ODS Loan Status Code
		updateLoanStatus($lid, 'APPLICATION_REJECTED');		

		$this->layout->content = View::make('blocks.application.disqualification', $data);
	}
  
	/**
	 * Update NLS Bank Account Information
	 * 
	 * @param  integer $userId
	 * @param  integer $loaAppNr
	 * @return
	 */
	protected function updateNLSBankAccountInfo( $userId, $loaAppNr )
	{
		$bankFlag = FALSE;
		
		//Get Bank Account Detail
		$bankData = BankAccountDetail::getBankAccount( $userId, $loaAppNr ); 

		//Get Applicant Data
		$applicantFields = array( 'First_Name', 'Last_Name'); 
		$applicantData   = getApplicant( $userId, $applicantFields,  TRUE );

		$NLS = new NLS();

		$fields = array(
			'nlsLoanTemplate'  => $this->loanTemplateName,
			'nlsLoanGroup'     => $this->loanGroupName,
			'nlsLoanPortfolio' => $this->loanPortfolioName,
			'contactId'        => $userId,
			'loanId'           => $loaAppNr,
			'firstName'        => $applicantData->First_Name,
			'lastName'         => $applicantData->Last_Name,
			'loanStatusCode'   => 'APPLICATION_STEP3'
		);

		if( count($bankData) > 0 ){ 
			$bankFlag = TRUE;
			$fields['totalNSFs']           = $bankData->Total_NSF_Cnt;
			$fields['DaysSinceNSF']        = $bankData->Days_Since_Last_NSF_Cnt;
			$fields['MoDepositToAverage']  = $bankData->Avg_Monthly_Deposit_Amt;
			$fields['avgBalPmtDate']       = $bankData->Avg_Bal_On_Pmt_Dt_Amt;
			$fields['currentBalance']      = $bankData->Current_Bal_Amt;
			$fields['numLowBalanceEvents'] = $bankData->Low_Bal_Event_Cnt;
			$fields['numLowBalanceDays']   = $bankData->Low_Bal_Day_Cnt;
			$fields['LOANDETAIL1']         = 1;   
		}

		$NLS->nlsUpdateLoanStatus($fields);

		updateLoanStatus($loaAppNr, 'APPLICATION_STEP3');
		
		return $bankFlag;
	}
	
	/**
	 * Compute Applicant Credit Score
	 * 
	 * @param  string  $type
	 * @param  integer $test
	 * @param  array   $testData
	 * @return integer
	 */
	protected function getCreditScore( $type = 'initial', $isTest = 0, $testData = array())
	{
		$uid = getUserSessionID();
		$lid = getLoanAppNr();

		return ApplicantRiskInfo::getCreditScore( $uid, $lid, $type, $isTest, $testData );
	}    

   /**
    * Compute Applicant Credit Score
    * 
    * @param  string  $type
    * @param  string  $riskScoreNumber
    * @param  string  $partnerID
    * @param  string  $stateCd
    * @param  integer $test
    * @param  array   $data
    * @return 
    */
	public function computeCreditScore( $type = 'initial', $riskScoreNumber = '1', $partnerID = 'xx', $stateCd = 'xx', $test = 0, $data = array() )
	{	

		$uid = getUserSessionID(); 
		$lid = getLoanAppNr();

		//reset uid if test mode
		if( $test )
			$uid    = 0; 
    	
 		return ApplicantRiskInfo::computeCreditScore($uid, $lid, $type, $riskScoreNumber, $partnerID, $stateCd, $test, $data );
	}

	/**
	 * Update NLS Loan Status
	 *
	 * @todo   transfer this to helper files 
	 * @param  integer $userId
	 * @param  integer $loanAppPhaseNr
	 * @param  string $status
	 * @return
	 */
	protected function updateNLSLoanStatus( $userId, $loanAppNr, $status )
	{
		 
		//Initialize NLS
		$NLS = new NLS();

		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
			->where('Loan_Id','=',$loanAppNr)
			->first();

		if( count($loanDetail) > 0 ) {
			//Get Loan Template Name
			$loanTemplate  = LoanTemplate::getLoanTemplateName($loanDetail->Loan_Template_Id);
			//Get Loan Group Name
			$loanGroup     = LoanGroup::getLoanGroupName($loanDetail->Loan_Grp_Id );
			//Get Loan Portfolio Name
			$loanPortfolio = LoanPortfolio::getLoanPortfolioName($loanDetail->Loan_Portfolio_Id);	

			$whereParam = array('User_Id' => $userId);	

			$applicant = new Applicant();
			$applicant_fields = array(
				'First_Name',
				'Middle_Name',
				'Last_Name',
				'Birth_Dt',
				'Social_Security_Nr'
			);
			
			$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

			$fields = array(
				'nlsLoanTemplate'  => $loanTemplate,
				'nlsLoanGroup'     => $loanGroup,
				'nlsLoanPortfolio' => $loanPortfolio,
				'contactId'        => $userId,
				'loanId'           => $loanAppNr,
				'firstName'        => $applicantData->First_Name,
				'lastName'         => $applicantData->Last_Name,
				'loanStatusCode'   => $status
			);
			
			$NLS->nlsUpdateLoanStatus($fields);
		}
	}
 
 	/**
 	 * Idology Run
 	 * 
 	 * @param  string $type
 	 * @return
 	 */
    public function idologyRun($type = 'ascendInit')
    {

		$this->layout  = null;
		$data          = array();
		$returnArray   = array();
		$userId        = getUserSessionID();
		$loanAppNr     = getLoanAppNr();
		$idology       = new Idology();
		$NLS  		   = new NLS();

		if($type == 'final'){
			$type = 'ascend';
		}	
		$result        = $idology->sendRequest($data, $type);
		$idologyResult = new IdologyResult();

		if( $result['result'] == 'success' ){

			// insert idology transaction table	 
			$idologyResult->Idology_Result_Desc  = 'Pass';
	        $idologyResult->Call_Type_Cd  		 = 'ascendFinal';
	        $idologyResult->Idology_KBA_Flag     = 0;
			$subPhaseCode   					 = 'FraudCheckPass';
			$loanAppPhaseNr 					 = 7;

			$returnArray = array(
				'result'  => $result['result']
			);

		}else if( $result['result'] == 'questionTriggered' ){

			// insert idology transaction table	        
	        $idologyResult->Idology_Result_Desc  = $result['flag'];
	        $idologyResult->Call_Type_Cd  		 = null;
	        $idologyResult->Idology_KBA_Flag     = 1;
			$subPhaseCode   					 = 'FraudCheckKBA';
			$loanAppPhaseNr 					 = 7;

			$returnArray = array(
				'result'  	=> $result['result'],
				'questions' => $result['questions'],
				'idNumber'  => $result['idNumber']
			);

		}else{

			$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
					->where('Loan_Id','=',$loanAppNr)
					->first();

			$lt = LoanTemplate::where('Loan_Template_Id', '=', trim($loanDetail->Loan_Template_Id))->first();	
			$loanTemplate = $lt->Loan_Template_Name;
			
			$lg = LoanGroup::where('Loan_Grp_Id', '=', trim($loanDetail->Loan_Grp_Id))->first();	
			$loanGroup = $lg->Loan_Grp_Name;

			$lp = LoanPortfolio::where('Loan_Portfolio_Id', '=', trim($loanDetail->Loan_Portfolio_Id))->first();	
			$loanPortfolio = $lp->Loan_Portfolio_Name;
			
			$fields = array(
				'nlsLoanTemplate'  => $loanTemplate,
				'nlsLoanGroup'     => $loanGroup,
				'nlsLoanPortfolio' => $loanPortfolio,
				'contactId'        => $userId,
				'loanId'           => $loanAppNr,
				'loanStatusCode'   => 'APPLICATION_REJECTED'
			);

			$whereParam = array('User_Id' => $userId);  
            $applicant = new Applicant();
            $applicant_fields = array(
                'First_Name',
                'Middle_Name',
                'Last_Name',
                'Birth_Dt',
                'Social_Security_Nr'
            );
            
            $applicantData = $applicant->getData($applicant_fields,$whereParam, true );

            $whereParam = array('User_Id' => $userId);  
            $applicant = new Applicant();
            $applicant_fields = array(
                'First_Name',
                'Middle_Name',
                'Last_Name',
                'Birth_Dt',
                'Social_Security_Nr'
            );
            
            $applicantData = $applicant->getData($applicant_fields,$whereParam, true );

            $applicantAddress = new ApplicantAddress();
            $applicantAddress_fields = array(
                'Street_Addr_1_Txt',
                'City_Name',
                'State_Cd',
                'Zip_Cd'
            );
            $addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);
            
            $fields['lastName']          = $applicantData->Last_Name;
            $fields['firstName']         = $applicantData->First_Name;
            $fields['ssn']               = $applicantData->Social_Security_Nr;
            $fields['address']           = $addressData->Street_Addr_1_Txt;
            $fields['city']              = $addressData->City_Name;
            $fields['state']             = $addressData->State_Cd;
            $fields['zip']               = $addressData->Zip_Cd;

			$NLS->nlsUpdateLoanStatus($fields);

	        $idologyResult->Idology_Result_Desc  = 'Fail';
	        $idologyResult->Call_Type_Cd  		 = null;
	        $idologyResult->Idology_KBA_Flag     = 0;
	        $loanAppPhaseNr = 7;
			$subPhaseCode   = 'FraudCheckFail';			

			$returnArray = array(
				'result'  => 'Failed',
				'note'    => 'Unfortunately we cannot positively verify your identity against the information you have provided and therefore cannot offer you a loan at this time.  If you think this has been in error, please contact our support team atsupport@AscendLoan.com or by calling 800-497-5314. <br /><br /> Thank You.'
			);
		}

		$idologyResult->User_Id              = $userId;
        $idologyResult->Idology_Id_Nr        = $result['idNumber'];
        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
        $idologyResult->IP_Addr_Id     		 = $result['ipAddress'];
        $idologyResult->Idology_Reason_Txt   = $result['message'];
        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
        $idologyResult->Created_By_User_Id   = $userId;
        $idologyResult->Create_Dt            = date('Y-m-d');
		$idologyResult->save();

		setLoanAppPhase( $userId , $loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

		return Response::json($returnArray,  200);
	}

	/**
	 * Idology Submit Answers
	 * 
	 * @return json Response
	 */
	public function idologySubmitAnswers()
	{
	
		$this->layout 	= null;
		$idology 		= new Idology();
		$data 			= Input::all();
		$returnArray	= array();
		$userId 		= getUserSessionID();
		$loanAppNr      = getLoanAppNr();
		$idologyResult  = new IdologyResult();
		$result 		= $idology->sendAnswers($data);
		$loanAppPhaseNr = 7;
		
		if($result['result']=='Pass'){

	        $idologyResult->Idology_Result_Desc  = 'Pass'; 
	        $subPhaseCode   					 = 'FraudCheckPass';		
	        $returnArray = array(
					'result'  => 'Pass'
				);
		}else{

	        $idologyResult->Idology_Result_Desc  = 'Fail';
	        $subPhaseCode   					 = 'FraudCheckFail';		
	        $returnArray = array(
				'result'  => 'Fail',
				'error'   => $result['message'],
				'note'    => 'Unfortunately we cannot positively verify your identity against the information you have provided and therefore cannot offer you a loan at this time.  If you think this has been in error, please contact our support team atsupport@AscendLoan.com or by calling 800-497-5314. <br /><br /> Thank You.'
			);
		}

		// insert idology transaction table	        
        $idologyResult->User_Id              = $userId;
        $idologyResult->Idology_Id_Nr        = $data['idNumber'];
        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
        $idologyResult->Idology_KBA_Flag     = 0;
        $idologyResult->IP_Addr_Id     		 = $result['ipAddress'];
        $idologyResult->Call_Type_Cd  		 = 'ascendFinal';
        $idologyResult->Idology_Reason_Txt   = $result['message'];
        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
        $idologyResult->Created_By_User_Id   = $userId;
        $idologyResult->Create_Dt            = date('Y-m-d');
        $idologyResult->save();

		setLoanAppPhase( $userId , $loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

		return Response::json( $returnArray, 200 );

	}
	
	/**
	 * Idology Test
	 * 
	 * @return json Response
	 */
	public function idologyRunTest()
	{

		$this->layout = null;
		$data =  Input::all();

		$idology = new Idology();
		$idology->test = Input::get('test');
		$userId = getUserSessionID();
		$result = $idology->sendRequest($data);

		$idologyResult = new IdologyResult();

		// insert idology transaction table	        
	        $idologyResult->User_Id              = $userId;
	        $idologyResult->Idology_Id_Nr        = $result['idNumber'];
	        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
	        $idologyResult->IP_Addr_Id     		 = $result['ipAddress'];
	        $idologyResult->Idology_Reason_Txt   = $result['message'];
	        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
	        $idologyResult->Created_By_User_Id   = $userId;
	        $idologyResult->Create_Dt            = date('Y-m-d');
	        

		if($result['result'] == 'success'){
			// insert idology transaction table	        
	        $idologyResult->User_Id              = $userId;
	        $idologyResult->Idology_Id_Nr        = $result['idNumber'];
	        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
	        $idologyResult->IP_Addr_Id     		 = $result['ipAddress'];
	        $idologyResult->Idology_Result_Desc  = 'Pass';
	        $idologyResult->Idology_KBA_Flag     = 0;
	        $idologyResult->Idology_Reason_Txt   = $result['message'];
	        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
	        $idologyResult->Created_By_User_Id   = $userId;
	        $idologyResult->Create_Dt            = date('Y-m-d');
	        $idologyResult->save();
			return Response::json(
				array(
					'result'  => $result['result']
				), 200
			);
		}else if($result['result'] == 'questionTriggered'){
			// insert idology transaction table	        
	        $idologyResult->User_Id              = $userId;
	        $idologyResult->Idology_Id_Nr        = $result['idNumber'];
	        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
	        $idologyResult->Idology_Result_Desc  = 'Fail';
	        $idologyResult->Idology_KBA_Flag     = 1;
	        $idologyResult->IP_Addr_Id     		 = $result['ipAddress'];
	        $idologyResult->Idology_Reason_Txt   = $result['message'];
	        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
	        $idologyResult->Created_By_User_Id   = $userId;
	        $idologyResult->Create_Dt            = date('Y-m-d');
	        $idologyResult->save();

			return Response::json(
				array(
					'result'  	=> $result['result'],
					'questions' => $result['questions'],
					'idNumber' => $result['idNumber']
				), 200
			);

		}else{
			// insert idology transaction table	        
	        $idologyResult->User_Id              = $userId;
	        $idologyResult->Idology_Id_Nr        = $result['idNumber'];
	        $idologyResult->Idology_Access_Dt    = date('Y-m-d');
	        $idologyResult->Idology_Result_Desc  = 'Fail';
	        $idologyResult->Idology_KBA_Flag     = 0;
	        $idologyResult->Idology_Reason_Txt   = $result['message'];
	        $idologyResult->IP_Addr_Id     		 = $result['ipAddress'];
	        $idologyResult->Idology_Result_Dttm  = date('Y-m-d H:i:s');
	        $idologyResult->Created_By_User_Id   = $userId;
	        $idologyResult->Create_Dt            = date('Y-m-d');
	        $idologyResult->save();
			
			return Response::json(
				array(
					'result'  => 'Failed',
					'message' => $result['message'],
					'note'    => 'Unfortunately we cannot positively verify your identity against the information you have provided and therefore cannot offer you a loan at this time.  If you think this has been in error, please contact our support team atsupport@AscendLoan.com or by calling 800-497-5314. <br /><br /> Thank You.'
				), 200
			);
		}
	}

	/**
	 * Idology Test harness
	 * 
	 */
	public function idologyTest(){
    	$this->layout->content = View::make('blocks.harness.idologyTest');
    }

	
	/**
	 * Convert Adverse Action code To Reason Statement
	 * 
	 * @param  string $adverseCode
	 * @return
	 */
	public function convertToReasonStatement( $adverseCode = '' )
	{
		$adverseActionCd = $adverseCode;
		eval('$adverseActionCd = self::X'.$adverseActionCd.';');
		return $adverseActionCd;
	}

	/**
	 * Iovation Client Checking
	 * 
	 * @param  integer $user_name
	 * @param  string $blackbox
	 * @param  string $ip
	 * @param  string $rules     
	 * @return
	 */
	public function iovationCheckClient( $user_name, $blackbox, $ip, $rules, $data )
	{
	  
	  	//get testMode for iovation
		$data['ioTestMode'] = Config::get('system.iovation.TestMode');

		if($data['ioTestMode']){
			$CTDurl 		= Config::get('system.iovation.test.CTDurl');
			$Subscriber 	= Config::get('system.iovation.test.Subscriber');
			$Password 		= Config::get('system.iovation.test.Password');
		}else{
			$CTDurl 		= Config::get('system.iovation.CTDurl');
			$Subscriber 	= Config::get('system.iovation.Subscriber');
			$Password 		= Config::get('system.iovation.Password');
		}


	  try {
		   $client = new SoapClient(null, 
		                          array( 'connection_timeout' => 3,
		                                 'location' => $CTDurl ,
		                                 'style'    => SOAP_RPC,
		                                 'use'      => SOAP_ENCODED,
		                                 'uri'      => $CTDurl ."#CheckTransactionDetails") );

	     // create list of transaction properties we want to send. It is important to do a SoapVar around each property as you add
	     // it to the array to ensure you don't get extra tags added to the XML. You must also convert the arrays to objects otherwise
	     // the indexing wrappers (key and val) will be added to the XML as well)
	     // also note that SoapParam will want to add extra structure so we need to use SoapVar to add to the array
	     $txn_props = (object) array (
	                         new SoapVar( (object) array( 'name' => 'onlineId', 'value' => $data['userId'] ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'Email', 'value' => Session::get('email') ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'BillingStreet', 'value' => $data['streetAddress1'] ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'BillingCity', 'value' => $data['city'] ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'BillingPostalCode', 'value' => $data['zip'] ), SOAP_ENC_OBJECT, null, null, 'property' )
	                     );
	                     
		    //Snare Vars               
	      $retArr = $client->CheckTransactionDetails( 
	                               new SoapParam( $user_name, 'accountcode' ),
	                               new SoapParam( $ip, 'enduserip' ),
	                               new SoapParam( $blackbox,  'beginblackbox' ),
	                               new SoapParam( $Subscriber , 'subscriberid' ),
	                               new SoapParam( self::DRA_ADMIN, 'subscriberaccount' ),
	                               new SoapParam( $Password, 'subscriberpasscode' ) ,
	                               new SoapParam( $rules, 'type' ),
	                               new SoapParam( $txn_props, 'txn_properties' )     
	                             );
	   } catch ( SoapFault $e ) {
	          unset( $client );
	          return array( "status"=> -1, "err"=>$e );
	   }

	   unset($client);
	   return $retArr;
	}

	/**
	 * Check Bank Account History Exist
	 * 
	 * @param  boolean $json
	 * @return   
	 */
	public function checkBankAccountHistory( $json = true )
	{

		$date = date('Y-m-d');
		$date = strtotime($date);
		$date = strtotime("-5 day", $date);
		$dateMin5 = date('Y-m-d', $date);

		$userId 	= getUserSessionID();
		$loanAppNr  = getLoanAppNr();

		$data = BankAccountDetail::checkBankAccount($userId, $loanAppNr, $dateMin5);		
		
		$data = array( 
			'resultCount' => count($data)
		);

		if($json)
			return Response::json($data);
		else
			return ($data['resultCount'] > 0 )? true : false ;
	}
}