<?php

class BorrowerPortalController extends BaseController {

	/**
	 * The layout that should be used for responses
	 * @var string 
	 */

	public $layout;

	const BANK_SAVINGS_INCREASE_AMT  = 50;
	const BANK_SAVINGS_PERCENTAGE_RT = 2.5;
	const BANK_SAVINGS_MAX_PERCT_RT  = 10;

	public function __construct() 
	{
		$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);	

    	if( !isLoggedIn() ){
    		return Redirect::to('/');
    	}
    	//Get User Account Information
		$userAccount = User::getData(array('User_Name','Email_Id'), array('User_Id' => getUserSessionID()), true );
		View::share('username', $userAccount->Email_Id);
	}

	/**
	 * Set User Id
	 * 
	 * @param integer $loanAppNr
	 */
	public function setUserId($userId)
	{
		Session::put('uid', $userId ); 
	}

	/**
	 * Get User Id
	 * @return [type]
	 */
	protected function getUserId()
	{
		return Session::get('uid');
	}

	/**
	 * Get Loan App Number
	 * 
	 * @return integer
	 */
	protected function getLoanAppNr()
	{
		return Session::get('loanAppNr');
	}

	/**
	 * Set Loan Application Number
	 * 
	 * @param integer $loanAppNr
	 */
	public function setLoanAppNr($loanAppNr)
	{
		Session::put('loanAppNr', $loanAppNr );
	}

	/**
	 * Show Portal main page
	 *
	 * @return void
	 */
	public function showIndex()
	{
		//Get Current User Id
		$userId = getUserSessionID();
 	
 		if( isLoggedIn() ){
			$data = $this->getSummaryDetails($userId);
			$data['styleTrigger'] = '';
			$data['styleNav'] 	  = 'style="margin-top: 70px"';

			// set session loan number
			setLoanAppNr($data['loanNumber']);
			$this->layout = View::make('layouts.portal');
			$this->layout->content = View::make('portal.index', $data );	
		}else{
			return Redirect::to('/');
		}
	}

	/**
	 * Show Portal Admin page
	 * @param  int $userId
	 * @param  int $loanId	
	 * @return void
	 */
	public function showAdminIndex($userId = 0, $loanId = 0){

		$data = $this->getSummaryDetails($userId, 1);
		$data['styleTrigger'] = 'style="margin-top: 20px"';
		$data['styleNav'] 	  = 'style="display: none; "';

		// set session loan number
		setLoanAppNr($data['loanNumber']);

		$this->layout = View::make('layouts.adminPortal');
		$this->layout->content = View::make('portal.index', $data );	
	}

	
	/**
	 * Show History page
	 *
	 * @return void
	 */
	public function showHistory()
	{
		//Get Current User Id
		$userId = getUserSessionID();

		$principalBal = 0;

		// test
 		if(App::environment() == 'local')
			$userId = 1426;

		if( isLoggedIn() ){

			//Get Loan Id
			$loanId = getLoanAppNr();
			
			// test
			if(App::environment() == 'local')
				$loanId = 1077;

			$data = $this->getHistory($userId, $loanId);

			// merge transaction by date
			$historyByDate = array();
			foreach ($data['loanHistory'] as $key => $value){

				//get principal advance CODE 100
				if($value->Txn_Cd == 100){
					$historyByDate[ $value->Txn_Dt ] = array(
						'Desc' 		=> 'Principal Advance',
						'Payment' 	=> '',
						'RR' 		=> '',
						'Fees' 		=> '',
						'Interest' 	=> '',
						'Principal' => '',
						'Balance' 	=> $value->Txn_Amt
					);

					// set principal
					$principalBal = $value->Txn_Amt;

				}

				// get NSF fee code 280
				if($value->Txn_Cd == 280 || $value->Txn_Cd == 180){

					$principalBal  = $principalBal + $value->Txn_Amt;

					$historyByDate[ $value->Txn_Dt ] = array(
						'Desc' 		=> 'NSF Fee',
						'Payment' 	=> '',
						'RR' 		=> '',
						'Fees' 		=> $value->Txn_Amt,
						'Interest' 	=> '',
						'Principal' => '',
						'Balance' 	=> $principalBal 
					);
				}

				// get NSF fee code 250
				if($value->Txn_Cd == 250 || $value->Txn_Cd == 150){
					
					$principalBal  = $principalBal + $value->Txn_Amt;

					$historyByDate[ $value->Txn_Dt ] = array(
						'Desc' 		=> 'Late Fee',
						'Payment' 	=> '',
						'RR' 		=> '',
						'Fees' 		=> $value->Txn_Amt,
						'Interest' 	=> '',
						'Principal' => '',
						'Balance' 	=> $principalBal
					);
				}

				// get Payment code 124
				if($value->Txn_Cd == 124){
					
					$payment = $this->getInvoice($value->Txn_Ref_Nr);

					if($payment){

						$principalBal  = $principalBal - $payment['principal'];

						$historyByDate[ $value->Txn_Dt ] = array(
							'Desc' 		=> 'Payment',
							'Payment' 	=> $value->Txn_Amt,
							'RR' 		=> $payment['rr'],
							'Fees' 		=> $payment['fees'],
							'Interest' 	=> $payment['interest'],
							'Principal' => $payment['principal'],
							'Balance' 	=> $principalBal
						);

					}

				}
			
			}


			$data['history'] = $historyByDate;

			$data['styleTrigger'] = '';
			$data['styleNav'] 	  = 'style="margin-top: 70px"';
			$this->layout = View::make('layouts.portal');
			$this->layout->content = View::make('portal.history', $data );	
		}else{
			return Redirect::to('/');
		}
	}

	/**
	 * get the payment from several tccode
	 * @param  [int] $assocRefNr 
	 * @return [type]        
	 */
	public function getInvoice($assocRefNr){

		$userId = getUserSessionID();
		
		//test
 		if(App::environment() == 'local')
			$userId = 1426;

		$loanId = getLoanAppNr();
		// test
		if(App::environment() == 'local')
			$loanId = 1077;

		//declare null
		$data = array('rr' => 0, 'principal' => 0, 'interest' => 0, 'fees' => 0 );

		//fieldnames and txn code
		$fields = array( 
			'920' => 'rr', 
			'206' => 'principal', 
			'204' => 'interest', 
			'150' => 'fees', 
			'250' => 'fees', 
			'180' => 'fees', 
			'280' => 'fees'
		);

		//get the loan history
		$payment = LoanTransactionHistory::where('User_Id', '=', $userId)
				->where('Loan_Id','=',$loanId)
				->where('Associated_Ref_Nr', '=', $assocRefNr)
				->get();

		if( count($payment) ){			

			foreach ($payment as $key => $value)				
				$data[$fields[$value->Txn_Cd]] = $data[$fields[$value->Txn_Cd]] + $value->Txn_Amt;

			return $data;

		}else
			return false;
	}

	/**
	 * Show Admin History page
	 * @param  int $userId
	 * @param  int $loanId	
	 * @return void
	 */
	public function showAdminHistory($userId = 0, $loanId = 0)
	{
		$data = $this->getHistory($userId, $loanId);	
		$data['styleTrigger'] = 'style="margin-top: 20px"';
		$data['styleNav'] 	  = 'style="display: none; "';
		$this->layout = View::make('layouts.adminPortal');
		$this->layout->content = View::make('portal.history', $data );	
	}


	/**
	 * Get Summary Details
	 * @param  int $userId
	 * @param  int $loanId	
	 * @return data array
	 */
	public function getSummaryDetails($userId = 0, $adminId = 0){		

		// Initialize default values
		$data = array(
			'initialBalance' => 0,
			'APR' 			 => 0,
			'monthlyPayment' => 0,
			'maturityDate' 	 => date('Y-m-d'),
			'currentBalance' => 0,
			'nextPaymentDate'=> date('Y-m-d'),
			'basePayment' 	 => 0,
			'monthReward' 	 => date('F'),
			'monthRewardAmt' => 0,
			'paymentDue' 	 => 0,
			'loanNumber'	 => 0,
			'prodId'		 => 1
		);

		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
				->where('Open_Maturity_Dttm','!=','')
				->first();

		if(count($loanDetail) > 0){

			$nlsResult = DB::connection('nls')->select("SELECT 
						loanacct.acctrefno, 
						cifno, 
						loan_number,
					    curr_maturity_date,
					    current_principal_balance,	
					    loanacct_payment.current_principal_payment_date,
					    loanacct_payment.next_principal_payment_date,
					    total_current_due_balance,
					    current_interest_balance,
					    next_statement1_date,
					    loanacct.original_note_amount,
					    loanacct.curr_maturity_date
					FROM loanacct
					INNER JOIN loanacct_payment ON loanacct.acctrefno = loanacct_payment.acctrefno
					where loan_number = '".$loanDetail->Loan_Id."'");
			


			if( count($nlsResult) > 0){				

				foreach($nlsResult as $nls){

					$TxnHistory = LoanTransactionHistory::where(array('User_Id' => $userId, 'Loan_Id' => $loanDetail->Loan_Id, 'Txn_Cd' => 100 ))->first();

					$next_date = new DateTime($nls->curr_maturity_date);
					$maturity_date = new DateTime($nls->curr_maturity_date);
					$data['maturityDate'] 	 = $maturity_date->format('Y-m-d');;
					$data['currentBalance']  = $this->getCurrentBalance($userId, $loanDetail->Loan_Id);
					$data['nextPaymentDate'] = $next_date->format('Y-m-d');
					$data['initialBalance']  = (count( $TxnHistory ) > 0)? round($TxnHistory->Txn_Amt, 2) : 0 ;
				}
			}		

			//Get Borrower Reward Calculation
		 	$rewardCalculation = BorrowerRewardCalculation::where('User_Id', '=', $userId)
		 						->where('Loan_App_Nr','=',$loanDetail->Loan_Id)
 	  							->orderBy('Create_Dt', 'DESC')
								->first();
	
			$data['APR'] 			= $loanDetail->APR_Val * 100;
			$data['monthlyPayment'] = round($loanDetail->Monthly_Payment_Amt, 2);
			$data['loanNumber']     = $loanDetail->Loan_Id;
			$data['prodId']			= $loanDetail->Loan_Prod_Id;

			$data['basePayment']     = round($loanDetail->Monthly_Payment_Amt, 2); // next /total  reg payment
			$data['monthReward']     = (count($rewardCalculation) > 0) ? date('F', strtotime($rewardCalculation->Create_Dt)) : date('F');
			$data['monthRewardAmt']  = (count($rewardCalculation) > 0) ? round($rewardCalculation->Calculated_RDA_Val, 2) : 0;
			$data['paymentDue']      = $data['basePayment'] - $data['monthRewardAmt'];

			if(!$adminId){
				// set Loan Number to session
				setLoanAppNr($loanDetail->Loan_Id);
			}			

		}	
		return $data;	
	}

	/**
	 * get the current balance add 3 balances
	 * @param  [type] $userId [description]
	 * @param  [type] $loanId [description]
	 * @return [type]         [description]
	 */
	public function getCurrentBalance($userId, $loanId){
		
		$balance = 0;

		$where 	= array('User_Id' => $userId, 'Loan_App_Nr' => $loanId);
		$data 	= DailyLoanBalance::where($where)
									->orderBy('Trial_Bal_Dt','desc')
									->first();
		if(count($data) > 0)
			$balance = $data->Principal_Bal_Amt + $data->Interest_Bal_Amt + $data->Fees_Bal_Amt;

		return $balance;
	}

	/**
	 * Get History Details
	 * @param  int $userId
	 * @param  int $loanId	
	 * @return data array
	 */	
	public function getHistory($userId = 0, $loanId = 0){
		$data = array( 'loanHistory' => array() );
		$loanHistory = LoanTransactionHistory::where('User_Id', '=', $userId)
				->where('Loan_Id','=',$loanId)
				->orderBy('Txn_Dt','asc')
				->get();

		$loanSummary = LoanSummary::where('User_Id', '=', $userId)
				->where('Loan_Id','=',$loanId)
				->first();	

		$data['outstandingBalance'] = (count($loanSummary) > 0) ? $loanSummary->Loan_Bal_Amt : 0;		

		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
				->where('Loan_Id','=',$loanId)
				->first();			
		
		$data['prodId'] = 1; //default value    
		
		if(count($loanDetail) > 0)
			$data['prodId'] = $loanDetail->Loan_Prod_Id;

	    if(count($loanHistory) > 0){
			$data['loanHistory'] = $loanHistory;
		}		
		
		return $data;			
	}

	/**
	 * Show Payment page
	 *
	 * @return void
	 */
	public function showPayment()
	{
		//Get Current User Id
		$userId = getUserSessionID();

		if( isLoggedIn() ){

		    $data['prodId'] = $this->getProductId($userId);
			$data['styleTrigger'] = '';
			$data['styleNav'] 	  = 'style="margin-top: 70px"';
			$this->layout = View::make('layouts.portal');
			$this->layout->content = View::make('portal.payment' , $data );	
		}else{
			return Redirect::to('/');
		}
	}

	/**
	 * Show Admin Payment page
	 * @param  int $userId
	 * @param  int $loanId	
	 * @return void
	 */
	public function showAdminPayment($userId = 0, $loanId = 0){

		$data['prodId'] = $this->getProductId($userId);
		$data['styleTrigger'] = 'style="margin-top: 20px"';
		$data['styleNav'] 	  = 'style="display: none; "';
		$this->layout = View::make('layouts.adminPortal');
		$this->layout->content = View::make('portal.payment', $data );	
	}

	/**
	 * Show Account page
	 *
	 * @return void
	 */
	public function showAccount()
	{
		//Get Current User Id
		$userId = getUserSessionID();

		if( isLoggedIn() ){

			$data['prodId'] = $this->getProductId($userId);
			$data['styleTrigger'] = '';
			$data['styleNav'] 	  = 'style="margin-top: 70px"';
			$this->layout = View::make('layouts.portal');
			$this->layout->content = View::make('portal.account' , $data );	
		}else{
			return Redirect::to('/');
		}

	}

	/**
	 * Show Admin Account page
	 *
	 * @return void
	 */
	public function showAdminAccount($userId = 0, $loanId = 0){

		$data['prodId'] = $this->getProductId($userId);
		$data['styleTrigger'] = 'style="margin-top: 20px"';
		$data['styleNav'] 	  = 'style="display: none; "';
		$this->layout = View::make('layouts.adminPortal');
		$this->layout->content = View::make('portal.account' , $data );	
	}


	/**
	 * Show Statement page
	 *
	 * @return void
	 */
	public function showStatement()
	{
		//Get Current User Id
		$userId = getUserSessionID();

		if( isLoggedIn() ){

			$data['prodId'] = $this->getProductId($userId);
			$data['styleTrigger'] = '';
			$data['styleNav'] 	  = 'style="margin-top: 70px"';
			$this->layout = View::make('layouts.portal');
			$this->layout->content = View::make('portal.statement' , $data );	
		}else{
			return Redirect::to('/');
		}
	}

	/**
	 * Show Admin Statement page
	 * @param  int $userId
	 * @param  int $loanId	
	 * @return void
	 */
	public function showAdminStatement($userId = 0, $loanId = 0){

		$data['prodId'] = $this->getProductId($userId);
		$data['styleTrigger'] = 'style="margin-top: 20px"';
		$data['styleNav'] 	  = 'style="display: none; "';
		$this->layout = View::make('layouts.adminPortal');
		$this->layout->content = View::make('portal.statement', $data );	
	}


	/**
	 * Show RateRewards Page
	 * @return
	 */
	public function showRateRewards()
	{
		$this->layout = View::make('layouts.portal');
		$data = $this->getStatements(getUserSessionID(), getLoanAppNr(), 0);	
		$this->layout->content = View::make('portal.rewards' , $data );
	}

	/**
	 * Show Admin RateRewards Page
	 * @param  int $userId
	 * @param  int $loanId	 
	 * @return
	 */
	public function showAdminRateRewards($userId = 0, $loanId = 0)
	{
		// $this->layout = View::make('layouts.adminPortal');
		$this->layout = null;
		$data = $this->getStatements($userId, $loanId, 1);	
		return View::make('raterewards.rewards-admin' , $data );
	}

	/**
	 * Get Rewards Page
	 * @param  int $userId
	 * @param  int $loanId	
	 * @param  int $adminFlag	  
	 * @return
	 */
	public function getStatements($userId = 0, $loanId = 0, $adminFlag = 0)
	{
		$userId         				= $userId;
		$loanNumber 					= $loanId;
		$RRInterest     				= 0;
		$finance        				= 0;
		$reward         				= 0;
		$cdbMonth       				= 0;
		$currentMonth   				= date('M');
		$cdbMonthsArr   				= array();
		$tccsMonthsArr  				= array();
		$qualifier      				= '<span class="red">No</span>';
		$data['qualifier'] 				= '<span class="red">No</span>';
		$data['qualifierTrigger'] 		= 0;
		$data['qualifierLimit'] 		= 3;
		$data['qualifierMonths'] 		= 0;
		$data['qualifierMonthsClass'] 	= 'red';
		$data['savingsSummary']         = '';
		$data['fastLinkUrl'] 			= '';
		$data['uid'] 					= $userId;
		$data['lid'] 					= $loanNumber;
		$data['rda']              		= 0;
		$data['adminView'] 				= 0;
		$data['cdbSpendingColor'] 		= 0;   // CDB
		$data['cdbHit']           		= 0;
		$data['cdbPercent']       		= 0;
		$data['cdbRewardPercent'] 		= 0;
		$data['cdbRewardMonths'] 		= 0;
		$data['tccsHit']           		= 0;   // TCCS
		$data['tccsPercent']       		= 0;
		$data['tccsRewardMonths'] 		= 0;
		$data['tccsRewardPercent'] 		= 0;
		$data['tccsMonthBalance']  		= 0;
		$data['tccsSpending'] 			= 0;
		$data['tccsMonthDiff']     		= 0;
		$data['sabMonthReward']   		= '';   // SAB
		$data['sabMonthBalance']  		= 0;
		$data['sabMonthDiff']     		= 0;
		$data['sabHit']           		= 0;
		$data['sabPercent']       		= 0;
		$data['sabRewardPercent'] 		= 0;
		$data['finance'] 				= 0;
		$data['totalpaymentamount'] 	= 0;
		$data['atPercent'] 				= 0;    // Auto Title
		$data['atDate']					= date('Y/m/d');
		$data['cdbMonthReward']   		= date('M');
		$data['cdbRewardYear']   		= date('Y');
		$data['totalMonthlyReward'] 	= 0;
		$data['increaseSavings']  		= false;
		$data['banks']					= array();
		$data['width'] 					= '98%';
		$data['error']					= '';
		$data['hasReward']				= false;
		$data['InterestDiscountPct']	= 0;
		$data['InterestDiscountAmt']	= 0;
		$next_payment_total_amount 		= 0;
		$totalTrigger					= 0;
		$DebugMode 						= Config::get('system.Ascend.DebugMode');
		$data['prodId']					= 1;

		//Get Loan Application Detail
		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
			->where('Loan_Id','=',$loanNumber)
			->first();

		if(count($loanDetail) > 0)
			$data['prodId'] = $loanDetail->Loan_Prod_Id;	
		
		$tuRRLists = TURateReward::where('User_Id','=',$userId)
			->where('Loan_App_Nr','=',$loanNumber)
			->orderBy('Trans_Union_Access_Dttm', 'DESC')
			->first();

		if(!$adminFlag){
			// Check if User Logged is an Administrator or Customer
			if(isLoggedIn()){
				$userRole = User::where('User_Id', '=', getUserSessionID())->first();
				if($userRole->User_Role_Id == 1){
					$data['adminView'] = 1;
				}
			}else{
				return Redirect::to('/');
			}
			$data['styleTrigger'] = '';
			$data['styleNav'] 	  = 'style="margin-top: 70px"';
		}else{
			$data['styleTrigger'] = 'style="margin-top: 20px"';
			$data['styleNav'] 	  = 'style="display: none; "';
		}
		//pre($tuRRLists);
		try{

			if( count($tuRRLists) > 0 ){
				$data['tccsSpending'] = round($tuRRLists->TCCS);
				$data['hasReward'] = true;
				//$tuRRLists->AT36S = 1;
				// Verify qualifier
				if($tuRRLists->AT36S > $data['qualifierLimit']){
					$data['qualifier'] = '<span class="green">Yes</span>';
					$data['qualifierTrigger'] = 1;
					$data['qualifierMonthsClass'] = 'green';
				}

				$data['qualifierMonths'] = $tuRRLists->AT36S;
				//$data['qualifierMonths'] = 999;

				// CDB results
				$cdbRewardRecord = BorrowerRewardTrigger::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->where('Trigger_Id','=','CDB')
					->where('RateReward_Ver_Nr','=','1.0')
					->orderBy('Billing_Statement_Dt', 'DESC')
					->first();
				if( count($cdbRewardRecord) > 0 ){
					$data['cdbHit']           = $cdbRewardRecord->Month_Attainment_Cnt;
					$data['cdbPercent']       = $cdbRewardRecord->Interest_Discount_Pct * 100;
					$data['cdbRewardPercent'] = $cdbRewardRecord->Interest_Discount_Pct;
					$date 					  = new DateTime($cdbRewardRecord->Billing_Statement_Dt);
					$data['cdbMonthReward']   = $date->format('M');
				}

				$tucCDB = 0;
				$oldBalance   = 0;
				// First TU Credit Pull
				$tuLists = TUCredit::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->orderBy('Trans_Union_Access_Dttm', 'DESC')
					->first();
				if(count($tuLists) > 0){
					$oldBalance   = $tuLists->RE33;
					// initial TU pull CDB
					array_push($cdbMonthsArr, array(
						'timestamp' => strtotime($tuLists->Create_Dt),
						'month' 	=> 'Start Bal',
						'balance' 	=> str_replace( ',', '', number_format($tuLists->RE33, 2 ) ),
						'reduction' => '',
						'color' 	=> 'green'
					));
					$tucCDB = round($tuLists->RE33,2);
				}

				//pre($cdbMonthsArr);

				$cdbCtr = 0;
				// CDB Months
				$cdbRewardMonths = BorrowerRewardDetail::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->where('Trigger_Id','=','CDB')
					->where('RateReward_Ver_Nr','=','1.0')
					->orderBy('Billing_Statement_Dt', 'ASC')
					->get();
				$data['cdbRewardMonths'] = count($cdbRewardMonths);

				if( count($cdbRewardMonths) > 0){
					$reductionCtr = 1;
					
					foreach($cdbRewardMonths as $cdbm){
						$cdbCtr++;
						
						if($cdbm->Bank_Acct_Field_Val < $oldBalance  ){
							$sign = '-';
						}else{
							$sign = '';
						}

						$oldBalance 	= $cdbm->Bank_Acct_Field_Val;

						array_push($cdbMonthsArr, array(
							'timestamp' => strtotime($cdbm->Billing_Statement_Dt),
							'month' 	=> date('M', strtotime($cdbm->Billing_Statement_Dt)).' '.date('Y', strtotime($cdbm->Billing_Statement_Dt)),
							'balance' 	=> str_replace( ',', '', number_format($cdbm->Bank_Acct_Field_Val, 2 ) ),
							'reduction' => $sign.abs(round($cdbm->RateReward_Trigger_Val, 2)),
							'color' 	=> ($cdbm->RateReward_Trigger_Val >= 50) ? 'green' : 'red'
						));
						$reductionCtr++;
					}

				}		

				$data['cdbMonthsArr'] = array_reverse($cdbMonthsArr);

				// TCCS results
				$tccsRewardRecord = BorrowerRewardTrigger::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->where('Trigger_Id','=','TCCS')
					->where('RateReward_Ver_Nr','=','1.0')
					->orderBy('Billing_Statement_Dt', 'DESC')
					->first();

				if( count($tccsRewardRecord) > 0 ){
					$data['tccsHit']           = $tccsRewardRecord->Month_Attainment_Cnt;
					$data['tccsPercent']       = $tccsRewardRecord->Interest_Discount_Pct * 100;
					$data['tccsRewardPercent'] = $tccsRewardRecord->Interest_Discount_Pct;
					$date 					= new DateTime($tccsRewardRecord->Billing_Statement_Dt);
					$data['tccsMonthReward'] = $date->format('M');
					$data['tccsMonthBalance'] = $tuRRLists->TCCS;
					$data['tccsMonthDiff'] = $tccsRewardRecord->Interest_Discount_Amt;
				}

				// TCCS Months
				$tccsRewardMonths = BorrowerRewardDetail::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->where('Trigger_Id','=','TCCS')
					->where('RateReward_Ver_Nr','=','1.0')
					->orderBy('Billing_Statement_Dt', 'DESC')
					->get();
				$data['tccsRewardMonths'] = count($tccsRewardMonths);
				if( count($tccsRewardMonths) > 0){
					foreach($tccsRewardMonths as $tccsm){
						$credit = is_null($tccsm->RateReward_Trigger_Val) ? '' : (round($tccsm->RateReward_Trigger_Val,2) < 50) ? 'pass' : 'fail';
						array_push($tccsMonthsArr, array(
							'month' 	=> date('M', strtotime($tccsm->Billing_Statement_Dt)).' '.date('Y', strtotime($tccsm->Billing_Statement_Dt)),
							'credit' 	=> $credit,
							'color' 	=> ($credit == 'pass') ? 'green' : 'red'
						));
					}
					$data['tccsMonthsArr'] = $tccsMonthsArr;
				}

				// Finance Charge
				$rewardCalculationResults = BorrowerRewardCalculation::where('User_Id','=',$userId)
					->where('Loan_App_Nr','=',$loanNumber)
					->where('RateReward_Ver_Nr','=','1.0')
					->orderBy('Billing_Statement_Dt', 'DESC')
					->first();	
					
				$data['finance'] = 0; 
				if( isset($rewardCalculationResults->Finance_Charge_Amt)  )
					$data['finance'] =  number_format((float)$rewardCalculationResults->Finance_Charge_Amt,2,'.','');

				// Get Auto Title
				$autoTitle = RewardAutoCollateral::where('Borrower_User_Id', '=', $userId)->first();

				if(count($autoTitle) > 0){
					$data['atPercent'] = 0.20;
					$data['atDate']    = $autoTitle->Pledge_Dt;
				}

				// Get Total Monthly Reward
				if($tuRRLists->AT36S > 3){
					$data['totalMonthlyReward'] = $data['cdbPercent'] + $data['tccsPercent'] + $data['sabPercent'];
				}

				// FastLink kuno
				//Bank Account Validation
				$isBankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanNumber);

				if(  $isBankAcctLinked == 'Y' ) {
					$data['increaseSavings'] = true;
					$data['savingsSummary']  = $this->checkBankSavings($userId, $loanNumber);
				}

				// Get Total Monthly Reward
				if($tuRRLists->AT36S > 3 ){

					$savingsSummaryrewardPrct = 0;

					if( isset($data['savingsSummary']['rewardPrct']) ){
						$savingsSummaryrewardPrct = $data['savingsSummary']['rewardPrct'];
					}

					// Get Reward Amount
					$totalTrigger = $data['cdbPercent'] + $data['tccsPercent'] + $data['sabPercent'] + $savingsSummaryrewardPrct;
					$data['rda'] = round((($totalTrigger / 100) * $data['finance']), 2);
				}

				$this->setUserId($userId);

				//Each Reload Needs to generate new Fast Link Url
				$Yodlee = new Yodlee();
				$Yodlee->createYodleeAccount();
				$Yodlee->generateFastLinkUrl();

				if( Session::get('fastLinkUrl') )
					$data['fastLinkUrl'] = Session::get('fastLinkUrl');
				else
					$data['error'] = 'Error on generating Fast Link Form.';
				
				if(!$adminFlag){
					//Set Session of Loan Application Number
					Session::set('loanAppNr', $loanNumber );
					//Set Session of User Id
					Session::set('uid', $userId );
				}

			}

		} catch( Exception $e ){

			//pre($e->getMessage());
			writeLogEvent('Portal Statement Page',
				array(
					'User_Id'     => $userId,
					'Loan_App_Nr' => $loanNumber,
					'Message'     => $e->getMessage()
				),
				'info'
			);
		}

		return $data;
	}


	/**
	 * Check Bank Savings
	 *
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return array
	 */
	protected function checkBankSavings( $userId, $loanAppNr, $acctType = 'checking')
	{
		$result =  array(
			'balance'    => array(),
			'rewardPrct' => 0,
			'targetCnt'  => 0
		);


		//Get Bank Account History
		$bankAccount = BankAccountDetail::getBankAccountRecords($userId, $loanAppNr, $acctType);
		
		$total = count($bankAccount);
		//print 'total'.count($bankAccount);
		if( count($bankAccount) > 0 ) {

			$account  		= array();
			$increase       = 0;
			$previousBalAmt = 0;
			$hittingTarget  = 0;
			$increase       = 0;	
			$monthName 		= 'Jul';			
			$lastMonth 		= 'Jul';	
			$bankCtr   		= 1;
			//pre($bankAccount);
			foreach ($bankAccount as $key => $value) {

				//print $total.'-'.$bankCtr.'-'.$value->Yodlee_Access_Dt.'<br />';
				if( $total == $bankCtr ){
					$lastMonth = date('m', strtotime($value->Yodlee_Access_Dt));
				}
				$bankCtr++;

				//Set the increase to zero if it is first PULL
				if( $key == 0 ) {
					$increaseVal = '';
					$monthYr = 'Start Bal';
				} else {
					$increase = $value->Current_Bal_Amt - $previousBalAmt;
					if($increase >= 50){
						$increaseVal = round($increase, 2);
					}else{
						if($value->Current_Bal_Amt < $previousBalAmt  ){
							$sign = '-';
						}else{
							$sign = '';
						}
						$increaseVal = $sign.abs(round($increase, 2));
					}
					$monthYr     = date('M Y', strtotime($value->Yodlee_Access_Dt));
				}								

				//set the previous balance
				$previousBalAmt = $value->Current_Bal_Amt;

				array_push($account, array(
					'previousBalAmt'=> $previousBalAmt,
					'key'			=> $key,
					'i'				=> $increase,
					'increaseAmt' 	=> $increaseVal,
					'balanceAmt' 	=> str_replace( ',', '', number_format($value->Current_Bal_Amt, 2 ) ),
					'monthYr' 		=> $monthYr,
					'qualified' 	=> ( $increase >= self::BANK_SAVINGS_INCREASE_AMT ) ? true : false,
					'yodleeClass' 	=> ($increase >= 50) ? 'green' : 'red'
				));

				if( $increase >= self::BANK_SAVINGS_INCREASE_AMT ) 
					$hittingTarget++;


			}		

			$account = array_reverse($account);
			//pre($account);
			//Calculate the percentage rate
			$rewardPercentage = self::BANK_SAVINGS_PERCENTAGE_RT * $hittingTarget;
			//Check if reward percentage is greater than the maximum set
			if( $rewardPercentage > self::BANK_SAVINGS_MAX_PERCT_RT )
				$rewardPercentage  = self::BANK_SAVINGS_MAX_PERCT_RT;

			$result =  array(
				'balance' 		=> $account,
				'rewardPrct' 	=> $rewardPercentage,
				'targetCnt'     => $hittingTarget
			);

		}

		return  $result;
	}

	/**
	 * Process Loan Transactions and Summary
	 *
	 * @return void
	 */
	public function processLoanTransactions(){
		$this->layout = null;
		
		// check each loans due date equals to the current date
		// and status is a LOAN_FUNDED
		$loanResults = array();

		try{
			// get loans where statement due date = current date
			$nlsResults = DB::connection('nls')->select("SELECT
	        						loanacct.acctrefno, 
	        						loanacct.cifno, 
	        						loan_number,
	        					    open_maturity_date,
	        					    loanacct.current_principal_balance,	
	        					    loanacct_payment.current_principal_payment_date,	
	        					    loanacct_payment.next_principal_payment_date,		        					    
	        					    loanacct.current_interest_balance,
	        					    loanacct.total_current_due_balance,
	        					    loanacct.original_note_amount,
	        					    next_statement1_date
	        					FROM loanacct
	        					INNER JOIN loanacct_payment ON loanacct.acctrefno = loanacct_payment.acctrefno
	        					INNER JOIN loanacct_statement ON loanacct.acctrefno = loanacct_statement.acctrefno
	        					where loanacct_statement.statement_due_date  = CAST(GETDATE() AS DATE)
	        					order by loanacct_payment.current_principal_payment_date desc");

			// Below is use for old and present transactions
			// INNER JOIN loanacct_payment ON loanacct.acctrefno = loanacct_payment.acctrefno
	  		// order by loanacct_payment.current_principal_payment_date desc");

			if(count($nlsResults) > 0){
				print '<br />NLS Loans:<br />';
				foreach($nlsResults as $nls){
					$loanDetail = LoanDetail::where('Loan_Id','=', $nls->loan_number)
									  ->where('Loan_Status_Desc','=', 'LOAN_FUNDED')
									  ->first();
					pre($nls); 
					if(count($loanDetail) > 0){
						array_push($loanResults, 
								array(
								   	'acctrefno' 				=> $nls->acctrefno,
								   	'cifno' 					=> $loanDetail->Borrower_User_Id,
								   	'loan_number' 				=> $loanDetail->Loan_Id,
								   	'open_maturity_date' 		=> $nls->open_maturity_date,
								   	'current_principal_balance' => $nls->current_principal_balance,
								   	'current_interest_balance' 	=> $nls->current_interest_balance,
								   	'next_statement1_date'		=> $nls->next_principal_payment_date,
								   	'total_current_due_balance' => $nls->total_current_due_balance
								));
					}

				}
			}

	        $start = 0;

	        if(count($loanResults) > 0 ){
	        	foreach($loanResults as $loan){
	        		
	        		$rr = 0;	
					$ir = 0;
	        		$aiDate = date('Y-m-d');
	        		$irDate = date('Y-m-d');
	        	
        			// get Accrued Interest
	        		// get NLS loan Account History for 920 and 206
	        		// if Prod = RR, then Interest Payment + Reward
        			// if Prod = APL, then Interest Payment
	        		$loanAccruedInterest = DB::connection('nls')->select("Select * from loanacct_trans_history where acctrefno = ".$loan['acctrefno']." and transaction_code IN (920, 206) and reversal_transrefno = 0");

	        		if( count($loanAccruedInterest) > 0){
	        			print '<br />Accrued Interest<br />';
	        				
	        			foreach($loanAccruedInterest as $AC){
	        				pre($AC);
	        				if($AC->transaction_code == 206 ){
	        					$ir = $AC->transaction_amount;
	        					$irDate = new DateTime($AC->transaction_date);
	        					$irDate = $irDate->format('Y-m-d');
	        				}
	        				if($AC->transaction_code == 920 ){
	        					$rr = $AC->transaction_amount;
	        					$aiDate = new DateTime($AC->transaction_date);
	        					$aiDate = $aiDate->format('Y-m-d');
	        				}

	        				if(($rr>0) && ($ir>0)){
	        					// get Loan Details
			        			$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $loan['cifno'])
											->where('Loan_Id','=',$loan['loan_number'])
											->first();

	        					$history = new LoanTransactionHistory();
						        $history->User_Id   			= $loan['cifno'];
						        $history->Loan_Id   			= $loan['loan_number'];
						        $history->Txn_Cd   				= 0;
						        $history->Txn_Desc   			= 'ACCRUED INTEREST';
						        $history->Txn_Amt   			= (count($loanDetail) > 0 && $loanDetail->Loan_Prod_Id == 2) ? $rr + $ir : $ir;
						        $history->Txn_Eff_Dt   			= DB::raw("CAST(GETDATE() AS DATE)");
						        $history->Txn_Dt   				= $aiDate;
						        $history->Current_Loan_Bal_Amt  = $loan['current_principal_balance'];
						        $history->Created_By_User_Id   	= $loan['cifno'];
						        $history->Create_Dt          	= DB::raw("CAST(GETDATE() AS DATE)");			            
						        $history->save();
						        $start++;
						        $rr = 0;
						        $ir = 0;
						        $dr = DB::raw("CAST(GETDATE() AS DATE)");
	        				}

	        			}
	        		}
	        		
	        		// get NLS loan Account History records
	        		$loanHistory = DB::connection('nls')->select("Select * from loanacct_trans_history where acctrefno = ".$loan['acctrefno']." and transaction_code IN (100, 206, 204, 150, 180, 920) and reversal_transrefno = 0");

	        		if( count($loanHistory) > 0){
	        			// get Loan Details
	        			$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $loan['cifno'])
									->where('Loan_Id','=',$loan['loan_number'])
									->first();
						
						$dr = DB::raw("CAST(GETDATE() AS DATE)");	
						print '<br />Others<br />';	
	        			foreach($loanHistory as $LH){
	        				pre($LH);
	        				$history = new LoanTransactionHistory();
					        $history->User_Id   			= $loan['cifno'];
					        $history->Loan_Id   			= $loan['loan_number'];
					        $history->Txn_Cd   				= $LH->transaction_code;
					        $history->Txn_Desc   			= $this->getTransaction($LH->transaction_code);
					        $history->Txn_Amt   			= $LH->transaction_amount;
					        $history->Txn_Eff_Dt   			= $LH->effective_date;
					        $history->Txn_Dt   				= $LH->transaction_date;
					        $history->Current_Loan_Bal_Amt  = $loan['current_principal_balance'];
					        $history->Created_By_User_Id   	= $loan['cifno'];
					        $history->Create_Dt          	= DB::raw("CAST(GETDATE() AS DATE)"); 	// use DB::raw("CAST(GETDATE() AS DATE)") for due date = current date	
					        $history->save();
	        			}

	        			//Get Borrower Reward Calculation
					 	$rewardCalculation = BorrowerRewardCalculation::where('User_Id', '=', $loan['cifno'])
					 						->where('Loan_App_Nr','=',$loan['loan_number'])
			 	  							->orderBy('Create_Dt', 'DESC')
											->first();

	        			$summary = new LoanSummary();
	        			$next_date = new DateTime($loan['next_statement1_date']);
						$open_date = new DateTime($loan['open_maturity_date']);						 

	        			$summary->User_Id   			 	= $loan['cifno'];
				        $summary->Loan_Id   			 	= $loan['loan_number'];
				        $summary->Maturity_Dt   			= $open_date->format('Y-m-d');
				        $summary->Loan_Bal_Amt   			= $loan['current_principal_balance'];
				        $summary->Next_Payment_Due_Dt   	= $next_date->format('Y-m-d');
				        $summary->Monthly_Payment_Amt   	= (count($loanDetail) > 0) ? $loanDetail->Monthly_Payment_Amt : 0;
				        $summary->Last_Reward_Month_Name   	= (count($rewardCalculation) > 0) ? date('F', strtotime($rewardCalculation->Create_Dt)) : date('F');
				        $summary->Most_Recent_Reward_Amt   	= (count($rewardCalculation) > 0) ? $rewardCalculation->Calculated_RDA_Val : 0;
				        $summary->Current_Payment_Due_Amt   = $loan['total_current_due_balance'];
				        $summary->Created_By_User_Id   		= $loan['cifno'];
				        $summary->Create_Dt          		= DB::raw("CAST(GETDATE() AS DATE)"); 			
				        $summary->save();
	        		}
	        		
	        	}
	        }	
	    } catch( Exception $e ){

			//pre($e->getMessage());
			writeLogEvent('processLoanTransactions',
				array(
					'Message'  => $e->getMessage()
				),
				'info'
			);
		}    
	}

	/**
	 * Get Custom Transaction Description using the transaction code
	 *
	 * @param integer transactionCode
	 * @return string transactionValue
	 */
	protected function getTransaction($transactionCode = 0){
		$transactionValue = '';
		if($transactionCode > 0){
			switch ($transactionCode) {
				case 100:
					$transactionValue = 'Principal Advance';
					break;
				case 204:
					$transactionValue = 'Principal Payment';
					break;
				case 206:
					$transactionValue = 'Interest Payment';
					break;
				case 920:
					$transactionValue = 'Ratereward Discount';
					break;
				case 150:
					$transactionValue = 'Late Fee';
					break;
				case 180:
					$transactionValue = 'NSF Fee';
					break;		
			}
		}
		return $transactionValue;
	}

	/**
	 * @param $userId
	 * @return int
	 */
	protected function getProductId($userId) {
		$prodId = 1;

		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
			->where('Open_Maturity_Dttm','!=','')
			->first();
		if (count($loanDetail) > 0)
			$prodId = $loanDetail->Loan_Prod_Id;

		return $prodId;
	}

	/**
	 * Reward Process
	 * 
	 * @param  int $uid
	 * @param  int $lid 
	 * @param  int $test 
	 * @return 
	 */
	public function process($uid=0, $lid=0, $test=0){

		$CDB          = 0;
		$SAB          = 0;
		$TCCS         = 0;
		$OTP          = 0; /* On Time Payment */
		$AT           = 0; /* Auto Title */
		$RDR          = 0;
		$RDA          = 0;
		$RRInterest   = 0;
		$CDB_percent  = 0;
		$SAB_percent  = 0;
		$TCCS_percent = 0;
		$AT_percent   = 0;
		$next_payment_total_amount = 0;
		$flag 		  = 'pass';
		$NLS 		  = new NLS();
		
		/* TransUnion credentials  */
		$tuData = array(
			'cert_file'                      => $this->cert_file,
			'key_file'                       => $this->key_file,
			'url'                            => $this->url,
			'TU_systemID'                    => $this->TU_systemID,
			'TU_systemPassword'              => $this->TU_systemPassword,
			'TU_sslPassword'                 => $this->TU_systemPassword,
			'TU_industryCode'                => $this->TU_industryCode,
			'TU_memberCode'                  => $this->TU_memberCode,
			'TU_inquirySubscriberPrefixCode' => $this->TU_inquirySubscriberPrefixCode,
			'TU_password'                    => $this->TU_password,			
			'TU_processingEnvironment'       => $this->TU_processingEnvironment
		);
		
		$body_contents = '';


        // check NLS table to get the next_statement1_date 
        $loanAcctResults = DB::connection('nls')->select("SELECT acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where DATEADD(DAY, -1, CAST(next_statement1_date AS DATE)) = CAST(GETDATE() AS DATE) and portfolio_code_id = 1 and loan_group_no IN (SELECT loan_group_no FROM loan_group WHERE loan_group LIKE '%-RR') ");
        //$loanAcctResults = DB::connection('nls')->select("SELECT acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where DATEADD(DAY, -1, CAST(next_statement1_date AS DATE)) = CAST(GETDATE() AS DATE) and portfolio_code_id = 1 and loan_group_no = 3 AND loan_number IN (3541,3548) ");
        //$loanAcctResults = DB::connection('nls')->select("SELECT acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where DATEADD(MONTH, -1, CAST(next_statement1_date AS DATE)) = '2015-07-30' and portfolio_code_id = 1 and loan_group_no = 3 ");
    	//$loanAcctResults = DB::connection('nls')->select("SELECT top 4 acctrefno, portfolio_code_id,loan_group_no, cifno, loan_number, next_statement1_date FROM loanacct where DATEADD(MONTH, -1, CAST(next_statement1_date AS DATE)) < '2015-10-25' and portfolio_code_id = 1 and loan_group_no IN (SELECT loan_group_no FROM loan_group WHERE loan_group LIKE '%-RR') order by loan_number ");
    	
    	$body_contents .= 'Number of Loans: '.count($loanAcctResults);
    	
    	$body_contents.= '<ul>';

        if(count($loanAcctResults) > 0){

        	foreach($loanAcctResults as $loan){
        		// Saving of Loans from NLS 
				$user = User::where('User_Ref_Nr','=',$loan->cifno)->first();			
				$nlsTracking = new NLSLoanTracking();
		        $nlsTracking->User_Id              = $user->User_Id;
		        $nlsTracking->Loan_App_Nr          = $loan->loan_number;
		        $nlsTracking->Billing_Statement_Dt = $loan->next_statement1_date;
		        $nlsTracking->Created_By_User_Id = $user->User_Id;
		        $nlsTracking->Create_Dt          = date('Y-m-d');
		        $nlsTracking->save();
			}
			foreach($loanAcctResults as $loan){

        		// 1. Pull credit using RR_ACCOUNT_REVIEW header
	        	
	        	$body_contents.= '
		        	<li class="reward" id="'.$loan->loan_number.'"><strong>Loan Number</strong>: '.$loan->loan_number.' ||
		        	<strong>Contact Reference</strong>: '.$loan->cifno.' ||';	
		        $user = User::where('User_Ref_Nr','=',$loan->cifno)->first();
	        	$body_contents.= '<strong>User ID</strong>: '.$user['User_Id'].'
	        	<div class="rwd reward'.$loan->loan_number.'">
	        	';	

	        	if(count($user) > 0){	
	        		try
		        		{
				        	$whereParam = array('User_Id' => $user->User_Id);	

							$applicant = new Applicant();
							$applicant_fields = array(
								'First_Name',
								'Middle_Name',
								'Last_Name',
								'Birth_Dt',
								'Social_Security_Nr'
							);
							
							$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

							$applicantAddress = new ApplicantAddress();
							$applicantAddress_fields = array(
								'Street_Addr_1_Txt',
								'City_Name',
								'State_Cd',
								'Zip_Cd'
							);

							$addressData   = $applicantAddress->getData($applicantAddress_fields,$whereParam,true);
				        	    	
							$tuData['TU_lastName']			= strtoupper($applicantData->Last_Name);
							$tuData['TU_middleName']		= strtoupper($applicantData->Middle_Name);
							$tuData['TU_firstName']			= strtoupper($applicantData->First_Name);
							$tuData['TU_birthDate']			= $applicantData->Birth_Dt;
							$tuData['TU_ssn']				= $applicantData->Social_Security_Nr;
							$tuData['TU_street']			= $addressData->Street_Addr_1_Txt;
							$tuData['TU_city']				= $addressData->City_Name;
							$tuData['TU_state']				= $addressData->State_Cd;
							$tuData['TU_zipCode']			= $addressData->Zip_Cd;
							
							// Pull Credit Report from TU
							$TUResponse = $this->prepareTURequest($tuData);
							$TUXML = $this->processTUResponse($TUResponse,$user->User_Id,$loan->loan_number);
							print '<br/>TU Pull<br/>';

							// Update NLS standard payment with Monthly Payment from Loan Detail - 08/20/15 1:59 PM
							//Get Loan Application Detail
					 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $user->User_Id)
													->where('Loan_Id','=',$loan->loan_number)
													->first();
		       			    $paymentAmount = $loanDetail->Monthly_Payment_Amt;	
		       			    // Get Loan Template
							$LoanTemplate = LoanTemplate::find( $loanDetail->Loan_Template_Id );		
							$paymentfields = array(
								'nlsLoanTemplate'      => $LoanTemplate->Loan_Template_Name,
								'contactId'            => $user->User_Id,
								'loanId'               => $loan->loan_number,
								'firstName'            => $applicantData->First_Name,
								'lastName'             => $applicantData->Last_Name,
								'paymentAmount'        => $paymentAmount // update Payment Amount
							);		 	  	
							//$NLS->nlsUpdateLoan($paymentfields);
							print '<br/>Done Initial NLS Standard Payment<br/>';


						} catch( Exception $e ){

							writeLogEvent('TU Pull',
									array( 
										'User_Id'     => $user->User_Id,
										'Loan_App_Nr' => $loan->loan_number,
										'Message'     => $e->getMessage()
									),
									'info'
								);
						}

						//$body_contents .=  '<br /><strong>Pulling Yodlee Information</strong><br />';

						//Yodlee Pull Information
						$this->pullBankTransactions( $user->User_Id, $loan->loan_number );
						print '<br/>Yodlee Pull<br/>';
					
					}else{
						 $body_contents .='<br />'.$loan->cifno.' does not exist on UserAcct';
					}	
				//}
				$body_contents.= '</div></li>';	

	        }

	    }
        print '<br/>After Pull<br/>';
		$ctr = 0;
	    while ( $ctr <= $this->TU_RR_pullCounter)  {
            $tuRRQuery   = 'SELECT TOP 1 * FROM TU_Rate_Reward_Attribute WHERE CAST(Trans_Union_Access_Dttm AS DATE) = CAST(GETDATE() AS DATE)' ;
            $tuRRResults = DB::connection('ascend')->select( $tuRRQuery );
            if( sizeOf( $tuRRResults ) > 0 )            	
                break;
            $ctr++;
        }    
        print '<br/>before SP<br/>';
	    if(count($loanAcctResults) > 0){
	    	try{
			    // RUN reward SP
				$rewardResults = DB::select("EXEC ODS.dbo.usp_RateRewards"); 
				pre($rewardResults);	
			} catch( Exception $e ){
				writeLogEvent('RR SP Issue', 
						array( 
							'User_Id'     => $user->User_Id,
							'Loan_App_Nr' => $loan->loan_number,
							'Message'     => $e->getMessage()
						),
						'info'
					);
			}		
		}
		print '<br/>SP Run<br/>';
		//Update NLS
		if(count($loanAcctResults) > 0){ 
        	
	        foreach($loanAcctResults as $loan){
	        	$user = User::where('User_Ref_Nr','=',$loan->cifno)->first();
	        	if(count($user) > 0){	
	        		try{
	        			
	        			$whereParam = array('User_Id' => $user->User_Id);	

						$applicant = new Applicant();
						$applicant_fields = array(
							'First_Name',
							'Middle_Name',
							'Last_Name',
							'Birth_Dt',
							'Social_Security_Nr'
						);
						
						$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

	        			//Get Borrower Reward Calculation
				 	  	$rewardCalculation = BorrowerRewardCalculation::where('User_Id', '=', $user->User_Id)
				 	  							->orderBy('Billing_Statement_Dt', 'DESC')
												->first();
						if( count($rewardCalculation) > 0 ){
							$RDA = $rewardCalculation->Calculated_RDA_Val;
						}		

						//Get Loan Application Detail
				 	  	$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $user->User_Id)
												->where('Loan_Id','=',$loan->loan_number)
												->first();
						if(count($loanDetail)>0){
							//Get Loan Template
							$LoanTemplate = LoanTemplate::find( $loanDetail->Loan_Template_Id );
							//Get Loan Group
							$loanGroup = LoanGroup::find( $loanDetail->Loan_Grp_Id );
							//Get Loan Portfolio
							$loanPortfolio = LoanPortfolio::find( $loanDetail->Loan_Portfolio_Id );
							//Get Monthly Payment Amount from NLS
							$loanacct_payment = DB::connection('nls')->select("SELECT next_payment_total_amount FROM loanacct_payment where acctrefno = ".$loanDetail->Loan_Ref_Nr."");
						   	if(count($loanacct_payment) > 0){
						   		$next_payment_total_amount = $loanacct_payment[0]->next_payment_total_amount;
						   	}

							$paymentAmount = 0;
						   	if($RDA > 0 ){
						   		$paymentAmount = $next_payment_total_amount - $RDA;
						   	}else{
						   		$paymentAmount = $loanDetail->Monthly_Payment_Amt;
						   	}

				 	  		$rewardfields = array(
								'nlsLoanTemplate'      => $LoanTemplate->Loan_Template_Name,
								'contactId'            => $user->User_Id,
								'loanId'               => $loan->loan_number,
								'firstName'            => $applicantData->First_Name,
								'lastName'             => $applicantData->Last_Name,
								'paymentAmount'        => $paymentAmount // update Payment Amount
							);
					 	  	//$NLS->nlsUpdateLoan($rewardfields);
							print '<br/>NLS Update Payment Amount<br/>';
						}

						if($RDA > 0){			
							// Update Transaction NLS
							$transFields = array(
								'TransactionCode' => 920,
								'LoanNumber' => $loan->loan_number,
								'Amount' => $RDA
								);
							//$NLS->createTransaction($transFields);
							print '<br/>NLS Create Transaction<br/>';
						}
        			} catch( Exception $e ){

						writeLogEvent('RR Update NLS', 
								array( 
									'User_Id'     => $user->User_Id,
									'Loan_App_Nr' => $loan->loan_number,
									'Message'     => $e->getMessage()
								),
								'info'
							);
					}	
	        	}else{
	        		print '<br/>No User<br/>';
	        	}
	        }
	    }

	    $body_contents.= '</ul>';
	    $data['body_contents'] = $body_contents;
	    $this->layout->content = View::make('blocks.harness.rateRewardHarness', $data);

	}

	/**
	   * TU prepare Request
	   * 
	   * @param  $fields array
	   * @return TU XML Response
	   */
	  public function prepareTURequest( $fields = array() )
	  {

	      $xml = '<?xml version="1.0" encoding="utf-8" ?>
	        <xmlrequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope" xmlns="http://www.netaccess.transunion.com/namespace">
	        <systemId>'.$fields['TU_systemID'].'</systemId>
	        <systemPassword>'.$fields['TU_systemPassword'].'</systemPassword>
	        <productrequest>
	          <creditBureau xmlns="http://www.transunion.com/namespace" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
	              <document>request</document> 
	              <version>2.12</version> 
	              <transactionControl> 
	                <userRefNumber>L2C THIN-U#01 FMP-SUB1</userRefNumber> 
	                <subscriber> 
	                  <industryCode>'.$fields['TU_industryCode'].'</industryCode> 
	                  <memberCode>'.$fields['TU_memberCode'].'</memberCode> 
	                  <inquirySubscriberPrefixCode>'.$fields['TU_inquirySubscriberPrefixCode'].'</inquirySubscriberPrefixCode> 
	                  <password>'.$fields['TU_password'].'</password> 
	                </subscriber> 
	                <options> 
	                  <processingEnvironment>'.$fields['TU_processingEnvironment'].'</processingEnvironment> 
	                  <country>us</country> 
	                  <language>en</language> 
	                  <contractualRelationship>individual</contractualRelationship> 
	                  <pointOfSaleIndicator>none</pointOfSaleIndicator> 
	                </options>                                                        
	              </transactionControl> 
	              <product> 
	                <code>07000</code> 
	                <subject> 
	                  <number>1</number> 
	                  <subjectRecord> 
	                  <indicative> 
	                    <name> 
	                      <person> 
	                        <first>'.$fields['TU_firstName'].'</first> 
	                        <middle>'.$fields['TU_middleName'].'</middle>
	                        <last>'.$fields['TU_lastName'].'</last> 
	                      </person> 
	                    </name> 
	                    <address> 
	                      <status>current</status>
	                                <street>
	                                    <unparsed>'.$fields['TU_street'].'</unparsed>
	                                </street>
	                                <location>
	                                    <city>'.$fields['TU_city'].'</city>
	                                    <state>'.$fields['TU_state'].'</state>
	                                    <zipCode>'.$fields['TU_zipCode'].'</zipCode>
	                                </location>
	                    </address> 
	                    <socialSecurity> 
	                      <number>'.$fields['TU_ssn'].'</number> 
	                    </socialSecurity> 
	                    <dateOfBirth>'.$fields['TU_birthDate'].'</dateOfBirth> 
	                  </indicative> 
	                  <addOnProduct> 
	                    <code>00N05</code> 
	                    <scoreModelProduct>true</scoreModelProduct> 
	                  </addOnProduct> 
	                  </subjectRecord> 
	                </subject> 
	                <responseInstructions> 
	                  <returnErrorText>true</returnErrorText> 
	                  <document/>
	                </responseInstructions> 
	                <permissiblePurpose> 
	                  <inquiryECOADesignator>individual</inquiryECOADesignator> 
	                </permissiblePurpose> 
	              </product>
	            </creditBureau> 
	          </productrequest>
	        </xmlrequest>
	        '; 
	        // test XML
	        $xml2 =  '<?xml version="1.0" encoding="utf-8" ?>
				<xmlrequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope" xmlns="http://www.netaccess.transunion.com/namespace">
				<systemId>ASCENDC1</systemId>
				<systemPassword>N0rthr0p1967</systemPassword>
				<productrequest>
				
					<creditBureau xmlns="http://www.transunion.com/namespace" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"> 
							<document>request</document> 
							<version>2.12</version> 
							<transactionControl> 
								<userRefNumber>L2C THIN-U#01 FMP-SUB1</userRefNumber> 
								<subscriber> 
									<industryCode>F</industryCode> 
									<memberCode>7796193</memberCode> 
									<inquirySubscriberPrefixCode>0622</inquirySubscriberPrefixCode> 
									<password>U4N2</password> 
								</subscriber> 
								<options> 
									<processingEnvironment>standardTest</processingEnvironment> 
									<country>us</country> 
									<language>en</language> 
									<contractualRelationship>individual</contractualRelationship> 
									<pointOfSaleIndicator>none</pointOfSaleIndicator> 
								</options> 
								<tracking>
								  <transactionTimeStamp>2015-07-24T01:43:56.627-05:00</transactionTimeStamp>
								</tracking> 
							</transactionControl> 
							<product> 
								<code>07000</code> 
								<subject> 
									<number>1</number> 
									<subjectRecord> 
									<indicative> 
										<name> 
											<person> 
												<first>TYLER</first> 
												<middle />
												<last>OEHMAN</last> 
											</person> 
										</name> 
										<address> 
											<status>current</status>
						                    <street>
						                        <unparsed>17586 CAMINITO HENO</unparsed>
						                    </street>
						                    <location>
						                        <city>SAN DIEGO</city>
						                        <state>CA</state>
						                        <zipCode>92127</zipCode>
						                    </location>
										</address> 
										<socialSecurity> 
											<number>666-20-7680</number> 
										</socialSecurity> 
										<dateOfBirth>01/01/1962</dateOfBirth> 
									</indicative> 
									<addOnProduct> 
										<code>00N05</code> 
										<scoreModelProduct>true</scoreModelProduct> 
									</addOnProduct> 
									</subjectRecord> 
								</subject> 
								<responseInstructions> 
									<returnErrorText>true</returnErrorText> 
									<document/>
								</responseInstructions> 
								<permissiblePurpose> 
									<inquiryECOADesignator>individual</inquiryECOADesignator> 
								</permissiblePurpose> 
							</product>
						</creditBureau> 
					</productrequest>
				</xmlrequest>
	        ';       
	       // pre($xml);
	       //  die;

	      $ch =  curl_init(); 
	      curl_setopt($ch, CURLOPT_URL, $fields['url']);
	      curl_setopt($ch, CURLOPT_PORT, 443); 
	      curl_setopt($ch, CURLOPT_HEADER, 0 );
	      curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	      curl_setopt($ch, CURLOPT_SSLCERT, getcwd().'/'.$fields['cert_file']);
	      curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $fields['TU_sslPassword']);
	      curl_setopt($ch, CURLOPT_SSLKEY, getcwd().'/'.$fields['key_file']);
	      curl_setopt($ch, CURLOPT_SSLKEYPASSWD, $fields['TU_sslPassword']);
	      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


	      $data = curl_exec($ch);
	      $curl_errno = curl_errno($ch);
	      $curl_error = curl_error($ch);

	      if ($curl_errno > 0) {
	          echo "cURL Error ($curl_errno): $curl_error\n";
	          $data = 'error';
	      }

	      curl_close($ch);
	      //pre($data);
	      //die;
	      return $data;
	  }


	  /**
	  * Process TU Response, parse and save to ODS
	  * @param XML string, user id, loan number
	  * @return TU XML Response
	  */
	  public function processTUResponse($xmlResponse = '', $uid = 0, $lid = 0){
	  	  $flag = 0;
	      $parseBureauObject = simplexml_load_string($xmlResponse);
	      //pre($parseBureauObject); die;
	      $tuAttributesArr = array();
	      //pre($parseBureauObject); die;
	      try{
		      if(property_exists($parseBureauObject->product,'subject')){
		      	 if(property_exists($parseBureauObject->product->subject,'subjectRecord')){
		      	 	if(property_exists($parseBureauObject->product->subject->subjectRecord,'addOnProduct')){
		      	 		$tuAttributes = $parseBureauObject->product->subject->subjectRecord->addOnProduct;
		      	 		if(count($tuAttributes) > 0){

						        foreach($tuAttributes as $tu){
						            // TU attributes          
						            if(property_exists($tu,'scoreModel')){
						           		if(property_exists($tu->scoreModel,'characteristic')){
						           			foreach($tu->scoreModel->characteristic as $row){
								                  if(property_exists($row,'value')){
								                      array_push($tuAttributesArr, (int)$row->value);
								                  }else{
								                      array_push($tuAttributesArr, -1);
								                  }
								              }
								              $this->saveTUresponse($tuAttributesArr, $uid , $lid);
								              $flag = $tuAttributesArr;		
						           		}else{
						           			$flag = 'TU Response error: No characteristic Node';

						           		}				              		              
						            }else{
						             $flag = 'TU Response error: No scoreModel Node';
						            }
						        }  
						      }else{
						        $flag = 'TU Response error: No addOnProducts Node';
						      }
		      	 	}else{
		      	 		$flag = 'TU Response error: No addOnProduct Node.';
		      	 	}
		      	 }else{
		      	 	$flag = 'TU Response error: No subjectRecord Node.';
		      	 }
		      }else{
		      	$flag = 'TU Response error: No subject Node.';
		      }
		  } catch( Exception $e ){

			writeLogEvent('Ratereward process TU Response', 
					array( 
						'User_Id'     => $uid,
						'Loan_App_Nr' => $lid,
						'Message'     => $e->getMessage()
					),
					'info'
				);
		 }	    	            
	    return $flag;  
	}

	/**
	 * Pull Bank Transactions
	 * 
	 * @param  integer $userId
	 * @return 
	 */
	public function pullBankTransactions( $userId, $loanAppNr )
	{
		$yodlee = new Yodlee();

		$isBankAcctLinked = LoanDetail::isBankAcctLinked($userId, $loanAppNr);

		if( $isBankAcctLinked == 'Y' ) {

			//Set User Id and Loan Application Number into Session
			$this->setUserId( $userId );
			$this->setLoanAppNr($loanAppNr);

			$loginAccount = $yodlee->createYodleeAccount();

			if( isset($loginAccount['result']) && $loginAccount['result'] == 'success') { 

				//Get the items summary of the account
				//and get the itemid that has container type of bank
				$itemsSummary = $yodlee->getItemSummariesWithoutItemData();

				$memSiteAccId = NULL;
				$itemId       = NULL;
				$siteId 	  = NULL;

				if( isset($itemsSummary['Body']) && count($itemsSummary['Body']) > 0 ) { 
					foreach ($itemsSummary['Body'] as $key => $value) {
						if( isset($value->contentServiceInfo)
							&& $value->contentServiceInfo->containerInfo->containerName == 'bank') { 
							$siteId       = $value->contentServiceInfo->siteId;
							$memSiteAccId = $value->memSiteAccId;
							$itemId       = $value->itemId;
						}
					}
				}
 
				//Get Account Level of the Item
				if( !empty( $itemId ) ) {

					$monthNr = 1; 

					$bankAccountDetail = new BankAccountDetail();
					$bankAccountInfo   = new BankAccountInfo(); 

					$bankAcctHistory = array();
					$bankAcctInfo    = array();

					//We will remove the transaction data on this cron 
					//to lessen the load of the process in one single 
					//method
					// pre('Saving Transactional Information');
					//Save Transactional Level Information
					// $transactions = $yodlee->saveUserTransactions($memSiteAccId, $siteId, $monthNr );
	
					//Get summary Information					
					$item = $yodlee->getItemsSummary1($itemId);

					if( isset($item['Body']->itemData) )	{

						$contentSrvcInfo = $item['Body']->contentServiceInfo;
						$accounts        = $item['Body']->itemData->accounts;

						foreach ($accounts as $key => $account) {

							$accountHolder = isset($account->accountHolder) ? $account->accountHolder : "";
							$accountNumber = isset($account->accountNumber) ? $account->accountNumber : "";

							$accountNumber = cleanNonNumeric($accountNumber);
								
							$bankAcctHistory[] = array(
								'User_Id'                 => $this->getUserId(),
								'Loan_App_Nr'             => $this->getLoanAppNr(),
								'Yodlee_Access_Dt'        => date('Y-m-d'), 
								'Member_Site_Id'          => $memSiteAccId,
								'Item_Type_Id'			  => $itemId,
								'Site_Id'                 => $contentSrvcInfo->siteId,
								'Acct_Holder_Name'        => $bankAccountDetail->encryptFields($accountHolder),
								'Acct_Nr'                 => $bankAccountDetail->encryptFields($accountNumber),
								'Acct_Nr_Raw'			  => $accountNumber,
								'Acct_Type_Desc'		  => $account->acctType,
								'Current_Bal_Amt'         => $account->currentBalance->amount,
								'Available_Bal_Amt'       => $account->availableBalance->amount,
								'Created_By_User_Id'      => $this->getUserId(),				
								'Create_Dt'               => date('Y-m-d'),
							);  

							$bankAcctInfo[] = array(
								'User_Id'            => $this->getUserId(), 
								'Site_Id'            => $contentSrvcInfo->siteId, 
								'Acct_Holder_Name'   => $bankAccountInfo->encryptField($accountHolder),  
								'Acct_Type_Desc'     => $account->acctType,  
								'Acct_Nr'            => $bankAccountInfo->encryptField($accountNumber), 
								'Created_By_User_Id' => $this->getUserId(), 
								'Create_Dt'          => date('Y-m-d'),  
							); 
						}  
					}

					//Store Bank Account Information
					if( count($bankAcctInfo) > 0 ) {
						
						try{  
							pre('Saving Bank Account Information');
							$bankAccountInfo::insertBatch( $bankAcctInfo );
						}catch(Exception $e) {
							writeLogEvent('RateReward - Saving Bank Account Details', 
								array( 
									'User_Id'     => $this->getUserId(),
									'Loan_App_Nr' => $this->getLoanAppNr(),
									'Message'     => $e->getMessage()
								) 
							); 
						}
					}

					//Store Bank Account History
					if( count($bankAcctHistory) > 0 ){
						
						try{  
							pre('Saving Bank Account History');
							$bankAccountDetail::insertBatch( $bankAcctHistory ); 
						}catch(Exception $e) {
							writeLogEvent('RateReward - Saving Bank Account Details', 
								array( 
									'User_Id'     => $this->getUserId(),
									'Loan_App_Nr' => $this->getLoanAppNr(),
									'Message'     => $e->getMessage()
								) 
							); 
						}
					} 
				} else {
					writeLogEvent('RateReward - Pulling Bank Transactions', 
						array( 
							'User_Id'     => $this->getUserId(),
							'Loan_App_Nr' => $this->getLoanAppNr(),
							'Message'     => 'Can\'t pull transaction'
						) 
					); 	
				}
			}
		} else{
			writeLogEvent('RateReward - Pulling Bank Transactions', 
				array( 
					'User_Id'     => $this->getUserId(),
					'Loan_App_Nr' => $this->getLoanAppNr(),
					'Message'     => 'Bank Account is not connected'
				) 
			); 		
		}
	}

	/**
	  * Save TU Response
	  * @param TU array, user id, loan number
	  */
	  public function saveTUresponse($tuAttributesArr = array(), $uid = 0, $lid = 0)
	  {

		$tu = new TURateReward();
		$tu->User_Id = $uid;
		$tu->Loan_App_Nr = $lid;
		$tu->Trans_Union_Access_Dttm = date('Y-m-d H:i:s');
		$tu->AT03S  = $tuAttributesArr[0];
		$tu->AT20S  = $tuAttributesArr[1];
		$tu->AT33A  = $tuAttributesArr[2];
		$tu->AT33B  = $tuAttributesArr[3];
		$tu->AT36S  = $tuAttributesArr[4];
		$tu->ATAP01 = $tuAttributesArr[5];
		$tu->BC06S  = $tuAttributesArr[6];
		$tu->BC09S  = $tuAttributesArr[7];
		$tu->BC12S  = $tuAttributesArr[8];
		$tu->BC31S  = $tuAttributesArr[9];
		$tu->BC98A  = $tuAttributesArr[10];
		$tu->G058S  = $tuAttributesArr[11];
		$tu->G068S  = $tuAttributesArr[12];
		$tu->G093S  = $tuAttributesArr[13];
		$tu->G094S  = $tuAttributesArr[14];
		$tu->G217S  = $tuAttributesArr[15];
		$tu->G237S  = $tuAttributesArr[16];
		$tu->HIAP01 = $tuAttributesArr[17];
		$tu->HRAP01 = $tuAttributesArr[18];
		$tu->IN06S  = $tuAttributesArr[19];
		$tu->IN09S  = $tuAttributesArr[20];
		$tu->IN12S  = $tuAttributesArr[21];
		$tu->IN21S  = $tuAttributesArr[22];
		$tu->IN28S  = $tuAttributesArr[23];
		$tu->IN33S  = $tuAttributesArr[24];
		$tu->IN34S  = $tuAttributesArr[25];
		$tu->INAP01 = $tuAttributesArr[26];
		$tu->MT12S  = $tuAttributesArr[27];
		$tu->MT28S  = $tuAttributesArr[28];
		$tu->MTAP01 = $tuAttributesArr[29];
		$tu->RE33S  = $tuAttributesArr[30];
		$tu->RE34S  = $tuAttributesArr[31];
		$tu->REAP01 = $tuAttributesArr[32];
		$tu->S062S  = $tuAttributesArr[33];
		$tu->S064A  = $tuAttributesArr[34];
		$tu->S064B  = $tuAttributesArr[35];
		$tu->S071A  = $tuAttributesArr[36];
		$tu->S071B  = $tuAttributesArr[37];
		$tu->S073A  = $tuAttributesArr[38];
		$tu->S073B  = $tuAttributesArr[39];
		$tu->TCCS   = $tuAttributesArr[40];
		$tu->Created_By_User_Id = $uid;
		$tu->Create_Dt = date('Y-m-d');
		$tu->save();
	  }


	/**
	 * cron use to populate the transaction history table
	 * @return none
	 */
	public function populateTransactionHistory(){
		$this->layout = null;
		$result = DB::statement('EXEC ODS.dbo.usp_etl_Loan_Txn_History');
	}

}
