<?php

/**
 * Developers Notes
 *
 * Updated:
 *   - Add Agreement Page
 *
 *
 * Todo
 *    - create only 1 view for all consent
 */

class PageController extends BaseController {


	/*
	|--------------------------------------------------------------------------
	| Page Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'PageController@index');
	|
	*/

	protected $layout =  'layouts.application';

	public function __construct() 
	{
		$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);		
	}

	/**
	 * Get Consent Data
	 * 
	 * @param  integer $consentTypeId
	 * @return
	 */
	public function getConsent( $consentTypeId )
	{
		$ConsentVersion  = new ConsentVersion(); 
		return $ConsentVersion->getDataByConsentId($consentTypeId); 
	}

	/**
	 * Get Latest Agreement Page
	 * 
	 * @return
	 */
	public function getSMSAuth()
	{  
		$data['consent'] = $this->getConsent(1);
		$this->layout->content = View::make('pages.sms-auth', $data );
	}

	/**
	 * Credit Report 
	 *
	 * @return
	 */
	public function getCreditPullAuth()
	{
		$data['consent'] = $this->getConsent(3);
		$this->layout->content = View::make('pages.credit-report-auth', $data);	
	}

	/**
	 * Get E-Signature Auth
	 * 
	 * @return
	 */
	public function getEsignatureAuth()
	{	
		$data['consent'] = $this->getConsent(2);
		$this->layout->content = View::make('pages.e-signature-consent', $data);	
	}

	/**
	 * Get E-Signature Auth
	 * 
	 * @return
	 */
	public function getFundingAuth()
	{
		$data['consent'] = $this->getConsent(4);
		$this->layout->content = View::make('pages.payment-auth', $data);	
	}

	/**
	 * Show 404 Page
	 * 
	 * @return
	 */
	public function show404()
	{
		$this->layout->content = View::make('errors.404');
	}

	/**
	 * Show Default Error Page
	 * 
	 * @param  object $exception
	 * @return
	 */
	public function showDefaultError( $exception )
	{	 
		$data['exception']  = $exception;
		$this->layout->content = View::make('errors.default', $data);
	}

	/**
	 * Rates and Terms
	 * 
	 * @return
	 */
	public function ratesTerms($stateCode = ''){
		
		$cmsAlias = (empty( $stateCode )) ? 'ratesterms' : 'ratesterms-'.$stateCode;
		$cmsAlias = strtolower($cmsAlias);

		$data['contents'] = getCmsContent($cmsAlias);

		$this->layout->content = View::make('pages.rate-terms', $data);

	}

	/**
	 * Contact Us
	 * 
	 * @return
	 */
	public function contactUs()
	{	 
		$contents = DB::connection('joomla')->select('SELECT introtext FROM ascend_content where id = 5');
		$data['contents'] = $contents[0]->introtext;
		$this->layout->content = View::make('pages.contact-us', $data );

	}

	/**
	 * FAQs
	 * 
	 * @return
	 */
	public function faq()
	{	
		$data['faq'] = array(

			array('faq_name' => 'Who is Ascend Consumer Finance?', 'faq_desc' => 'We’re an online lender dedicated to helping borrowers improve their financial lives. Our transparent and fair lending products encourage borrowers to demonstrate responsible credit management behaviors and reward them by lowering the cost of borrowing.' ),
			array('faq_name' => 'What loan products does Ascend Consumer Finance offer?', 'faq_desc' => 'We offer unsecured personal loans that you can customize by adding the RateRewards programs: <ul> <li>Ascend Personal Loan. A standard personal loan, with a low APR and a monthly payment that will never change.</li> <li>RateRewards Program. An innovative rewards program that allows borrowers to lower their interest cost up to 50% each month by demonstrating responsible financial behaviors.</li> </ul>' ),
			array('faq_name' => 'What are eligibility requirements for an Ascend Personal Loan?', 'faq_desc' => 'To qualify for an Ascend Personal Loan, you must:<br> <ul> <li>Be at least 18 years old</li> <li>Have a valid email account</li> <li>Have a verifiable name, date of birth, and social security number</li> <li>Have a bank account with a depository institution with a routing number</li> </ul> You must also display the following credit characteristics:<br> <ul> <li>Have verifiable income over $35,000</li> <li>Have a minimum of 580 FICO credit score</li> <li>Have no more than 6 inquires in last 12 months</li> <li>Have no delinquencies in last 3 months</li> <li>Have no bankruptcy in last 12 months </li> <li>Meet our other underwriting scoring criteria that use a variety of credit report and other financial factors to assess and score your credit risk</li> </ul>'), 
			array('faq_name' => 'How does RateRewards work?', 'faq_desc' => 'RateRewards is an optional rewards program targeted to better borrowers that you can add to your Ascend Personal Loan to earn a discount up to 50% off your interest cost.  When you upgrade your Ascend Personal Loan with RateRewards, you’ll be given a base APR and payment schedule. This represents the most you’ll ever pay per month. From there, you can earn up to a 50% discount off your interest cost. <br><br> Best of all, you can start earning rewards right out of the gate by doing the following:<br><br> <ul> <li>Paying on-time for all your debt accounts</li> <li>Increasing your savings balances</li> <li>Paying off your overall debt</li> <li>Spending on debit instead of credit</li> <li>Pledging an auto title as collateral against your loan</li> </ul> Based on how you do each month, you’ll earn a monthly RateReward discount rate.  This discount is applied to your interest cost for the month, reducing your interest cost and monthly payment by that amount.  <a href="'.URL::to('/howItWorks').'">Learn more</a>' ),
			array('faq_name' => 'How much can I borrow?', 'faq_desc' => 'At Ascend Consumer Finance, we match your loan offer to your unique financial situation. Your loan offer may range from $2,600 to $15,000 based on your credit score, employment history, and ability to repay.' ),
			array('faq_name' => 'What are the fees?', 'faq_desc' => "We are committed to a transparent and easy-to-understand fees. The only fees you will ever pay is the interest accrued on your loan (and late fees if applicable). We'll never charge you additional or hidden loan fees like application fees, membership fees, or prepayment fees. <br>Late fees may apply if a scheduled payment is missed." ),
			array('faq_name' => 'How can I repay my loan?', 'faq_desc' => "At Ascend Consumer Finance, we make it easy and convenient to pay back your loan through electronic fund transfer from the checking account you designate. We’ll pull the money on the schedule you choose so you never have to worry about forgetting a due date again." ),
			array('faq_name' => 'Can I make an early or additional payment without penalties?', 'faq_desc' => "Absolutely. There are NEVER any penalties for early payments." ),
			array('faq_name' => 'How do I update my password or personal information?', 'faq_desc' => 'To update your personal information, just follow these simple steps:<br> <ul> <li>Sign into your account</li> <li>Click on the "Setting" tab</li> <li>Click on the “Change Password” or “Change of Address” link</li> <li>Enter your new information and save your changes</li> <ul>' ), 
			array('faq_name' => 'What are your security policies?  Is my data secure?', 'faq_desc' => 'We use 256-bit encryption to protect the data that you have provided as part of your loan process.  Our data access policies ensure that only employees who would need to interact with this data can access it—all others can only see summary data.  Wherever possible, personally identifiable data is linked to sensitive information through an encrypted token, ensuring that any data used for analytics cannot be matched to any individual borrower.' ),
			array('faq_name' => 'What happens if I choose to pledge my auto title against my RateReward loan?', 'faq_desc' => 'If you own your car and have possession of your title, you can pledge it against your RateReward loan to reduce your interest expense by 20% each month.  To sign up for this reward, call our customer service line at 800-497-5314.   They will have you send in your title and we will register it against your loan.  While your loan is outstanding, Ascend will hold on to your title.  When your loan is paid off, we will return your title.' ),
			array('faq_name' => 'How quickly will I receive my loan proceeds?', 'faq_desc' => 'We validate information such as your employment history and income.  We make every effort to do this as quickly as possible.  Once your information has been validated, we submit an order to fund your loan.  If this is processed by 3:00 Eastern Time, your loan is usually funded the next day.' ),

		);
		
		$contents = DB::connection('joomla')->select('SELECT introtext FROM ascend_content where id = 6');
		$data['contents'] = $contents[0]->introtext;

		$this->layout->content = View::make('pages.faq', $data );

	}

	/**
	 * Privacy Policy
	 * 
	 * @return
	 */
	public function privacyPolicy()
	{

		$data = array();
		$this->layout->content = View::make('pages.privacy-policy', $data );

	}

	/**
	 * Terms and Condition
	 * 
	 * @return
	 */
	public function terms()
	{

		$data = array();
		$this->layout->content = View::make('pages.terms', $data );
		
	}

	/**
	 * How it Works
	 * 
	 * @return
	 */
	public function howItWorks()
	{

		$contents = DB::connection('joomla')->select('SELECT introtext FROM ascend_content where id = 2');
		$data['contents'] = $contents[0]->introtext;

		$this->layout->content = View::make('pages.how-it-works', $data );
	}

	/**
	 * Press Release
	 * 
	 * @return
	 */
	public function pressRelease()
	{	
		$contents = DB::connection('joomla')->select('SELECT introtext FROM ascend_content where id = 4');
		$data['contents'] = $contents[0]->introtext;
		$this->layout->content = View::make('pages.press', $data );
	}
	
}