<?php

namespace Api; 

use \Input as Input;
use \Response as Response;
use \Lang as Lang;

/**
 * Verification Controller
 * 
 * @package Salesforce
 * @subpackage Verification
 * @version 1
 */
class VerificationController extends ApiController{


	public function index()
	{
		echo 'verification api index'; 
	}

	/**
	 * Approved Verification
	 * 
	 * @return
	 */
	protected function approve()
	{
		$data   = Input::all();
		$result = array(); 
		$option = array();

		if( count($data) > 0 ) {
			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);
			$option['Params'] = $data;
			if( $appCnt > 0 ){
				$result = $this->updateVerObj( $data , 'COMPLETE');	
			}else{
				$result = $this->formatMsgResponse( 'APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			}  
		}else{
			$result = $this->formatMsgResponse('MISSING_PARAMS' , 'failed', $option);
		}

		return Response::json( $result, 200 );
	}


	/**
	 * Decline Verification 
	 * 
	 * @return
	 */
	protected function decline()
	{
		$data = Input::all();
		$result = array();
		$option = array();

		if( count($data) > 0 ){
			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);
			$option['Params'] = $data;
			if( $appCnt > 0 ){
				$result = $this->updateVerObj( $data , 'DENIED');
			}else{
				$result = $this->formatMsgResponse( 'APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			}  
		}else{
			$result = $this->formatMsgResponse('MISSING_PARAMS' , 'failed', $option);
		}
		
		return Response::json( $result, 200 );
	}

	/**
	 * Assign Verification
	 * 
	 * @return
	 */
	protected function assign()
	{
		$data = Input::all();
		$result = array();
		$option = array();

		if( count($data) > 0 ){
			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);
			$option['Params'] = $data;
			if( $appCnt > 0 ){
				$option['ODS'] = \VerificationAssignment::assign( $data );
				$result = $this->formatMsgResponse('LOAN_ASSIGN' , 'success', $option);
			}else{
				$result = $this->formatMsgResponse( 'APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			}
		}else{
			$result = $this->formatMsgResponse('MISSING_PARAMS' , 'failed', $option);
		}

		return Response::json( $result, 200 );
	}

	/**
	 * Update Verification Object
	 * 
	 * @param  array  $data
	 * @param  string $status
	 * 
	 * @return
	 */
	protected function updateVerObj( $data = array(), $status = 'OPEN' )
	{
		 
		$result = array();
		$option = array();

		if( count($data) > 0) {

			//Check wether the application exist
			$appCnt = \LoanDetail::where(
				array(
					'Borrower_User_Id' => $data['User_Id'], 
					'Loan_Id' => $data['Loan_Id']
				)
			)->count();

			  
			if( $appCnt > 0 ) {

				//Get Applicant Verification Object
				$verificationCnt = \ApplicantVerification::where(
					array(
						'User_Id'             => $data['User_Id'], 
						'Loan_App_Nr'         => $data['Loan_Id'], 
						'ODS_Verification_Id' => $data['ODS_Verification_Id']
					)
				)->count();

				//This is an old table that we need to get rid off
				//Get Completed Applicant Verification
				$CompletedApplicantVerification = new \CompletedApplicantVerification();

				$verificationOldCnt = $CompletedApplicantVerification::where(
					array(
						'User_Id' => $data['User_Id'], 
						'Loan_App_Nr' => $data['Loan_Id']
					)
				)->count();

				 
				if( $verificationCnt > 0 || $verificationOldCnt > 0 ) {

					//Get Applicant Verification Object
					$verification = \ApplicantVerification::where(
						array(
							'User_Id'             => $data['User_Id'], 
							'Loan_App_Nr'         => $data['Loan_Id'], 
							'ODS_Verification_Id' => $data['ODS_Verification_Id']
						)
					)->first();

					//Get the Applicant Completed Verification Record
					$completedVerification = $CompletedApplicantVerification::getCompletedAppVer(
											$data['User_Id'], 
											$data['Loan_Id']
										);

					try{

						//Update Application Verification
						$option['ODS'] = \ApplicantVerification::where(
							array(
								'User_Id'             => $data['User_Id'], 
								'Loan_App_Nr'         => $data['Loan_Id'],
								'ODS_Verification_Id'  => $data['ODS_Verification_Id']
							)
						)->update(
							array(
								'Verification_Status_Desc' => $status
							)
						);

						$nlsStatus = 'NOT STARTED'; 

						if( $status == 'COMPLETE' ){
							$verificationStatus = 'y';
							$nlsStatus = 'APPROVED';
						}else{
							$verificationStatus = 'n';
							$nlsStatus = $status;
						}

						//Set Competed Applicant Verification Steps for OLD Table
						$CompletedApplicantVerification::setCompletedApplicantVerification(
							$data['User_Id'], 
							$data['Loan_Id'], 
							$data['ODS_Verification_Id'], 
							$verificationStatus
						);
   
						//@TODO - we need to create new table for NLS Task Template
						//Set Task Template Code
						$taskTemplate = array(
							1 => 'REVIEW_CONTRACT', 
							2 => 'CONFIRM_PMT_ACCT', 
							3 => 'CONFIRM_EMPLOYMENT', 
							4 => 'CONFIRM_INCOME', 
							5 => 'CONFIRM_ID', 
							6 => 'REVIEW_PAYMENT_METHOD', 
							7 => 'ID_VERIFIED', 
							8 => 'CONFIRM_BANK_ACCOUNT',
							9 => 'FINAL_APPROVAL'
						);
						
						$applicantInfo = \Applicant::find( $data['User_Id'] );
						 
						$firstName = '';
						$lastName  = '';

						if( isset($applicantInfo['firstName']) ){
							$firstName = $applicantInfo['firstName']; 
						}

						if( isset($applicantInfo['lastName']) ){
							$lastName = $applicantInfo['lastName']; 
						}

						$applName =  $firstName  . ' ' . $lastName;

						$taskTemplateName =  $taskTemplate[$data['ODS_Verification_Id']];

						$NLSTaskId = "";

						if( count($verification) > 0 ){ 
							$NLSTaskId = $verification->NLS_Task_Id;
						} else{
							//Get Verification Task Reference No 
							$taskTemplate = \Config::get("NLSTaskTemplate.$taskTemplateName");

							//Get the NLS Task Id
							$NLSTask = new \NLSTask();

							//Get Loan Reference Number
							$loanDetail = \LoanDetail::getLoanDetail(
								$data['User_Id'], 
								$data['Loan_Id'],
								array('Loan_Ref_Nr')
							);

							//Get the Task Reference Number by
							//Loan Reference Number and Task Template No
							$taskRefNo = $NLSTask->getTasksByRefNoTemplateNo(
								$loanDetail->Loan_Ref_Nr, 
								$taskTemplate['TemplateNo']
							);

							if( count($taskRefNo) > 0 ){
								$NLSTaskId = $taskRefNo->task_refno;
							}
						}

						$nlsParam = array(
							'UpdateFlag'       => 1, 
							'TaskTemplateName' => $taskTemplateName,
							'TaskRefno' 	   => $NLSTaskId,
							'PriorityCodeName' => 'NORMAL',
							'NLSType'          => 'Loan', 
							'StatusCodeName'   => $nlsStatus,
							'NLSRefNumber'     => $data['Loan_Id'],
							'CreatorUID'       => 0, 
							'OwnerUID'         => 0,
							'OwnerUserName'    => 'Wayde',  
							'StartDate'        => date('m/d/Y'), 
							'DueDate'          => date('m/d/Y'),
							'Subject'          => $taskTemplateName . ' ' . $applName, 
						); 
					 
						$NLS = new \NLS();
						$option['NLS'] = $NLS->updateTask( $nlsParam );

						$option['Params'] = $data;

						if( $status == 'COMPLETE'){
							$result = $this->formatMsgResponse('VERIFICATION_APPROVED' , 'fail', $option);
						}else{

							//Assign the Application the RISK CSR
							$SFCustomerServiceRep = \SFCustomerServiceRep::getCSRByType( 'RISK', true);

							if( count($SFCustomerServiceRep) > 0 ){
								//Update Applicant Verification Assignee
								$option['ODS'] = \ApplicantVerification::where(
									array(
										'User_Id'             => $data['User_Id'], 
										'Loan_App_Nr'         => $data['Loan_Id'],
										'ODS_Verification_Id' => $data['ODS_Verification_Id']
									)
								)->update(
									array(
										'Assigned_CSR_Id' => $SFCustomerServiceRep->CSR_Id
									)
								);
							}
 
							//@NOTES: Declining of the Application will be triggered by the RISK CSR
							//Reject the Application 
							// \LoanDetail::where(
							// 	array(
							// 		'Borrower_User_Id' => $data['User_Id'], 
							// 		'Loan_Id' => $data['Loan_Id']
							// 	)
							// )->update(
							// 	array(
							// 		'Update_Dt' => date('Y-m-d'), 
							// 		'Updated_By_User_Id' => $data['Loan_Id'], 
							// 		'Loan_Status_Desc' => 'APPLICATION_REJECTED'
							// 	)
							// );

							//Rejected NLS Status 
							// \NLSHelper::updateNLSLoanStatus(
							// 	$data['User_Id'], 
							// 	$data['Loan_Id'],
							// 	'APPLICATION_REJECTED'
							// );

							$result = $this->formatMsgResponse('VERIFICATION_DECLINED' , 'fail', $option);
						}

					}catch(Exception $e) {
						$result = $this->formatMsgResponse('INTERNAL_ERROR' , 'fail', $option);	
					}
				}else{
					$option['Params'] = $data;
					$result = $this->formatMsgResponse('NO_RECORD_MATCH' , 'fail', $option);
				}

			} else{ 
				$result = $this->formatMsgResponse('MISSING_PARAMS' , 'fail', array() );
			}
		}
		// _pre($result);
		return $result;
	}
}