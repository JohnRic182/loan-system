<?php

namespace Api; 

use \Input as Input;
use \Response as Response;
use \Lang as Lang;

/**
 * Loan Controller
 * 
 * @package Salesforce
 * @subpackage Loan
 * @version 1
 */

class LoanController extends ApiController{

	/**
	 * Constructor
	 * 
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->authenticate();
	}

	/**
	 * Update Salesforce Object
	 *  
	 * @param  string  $objectId
	 * @param  string  $objectType
	 * @return array
	 */
	public function update( $objectId = '', $userId,  $loanAppNr, $objectType = 'Account')
	{  
		if( !empty($objectId) ) {
			
			$SalesforceMod = new SalesforceMod();

			try{

				//Account Object
				if( $objectType == 'Account'){
					
					return $SalesforceMod->updateAccount(	
						$userId, 
						$loanAppNr, 
						$objectId 
					);
				}

				//Loan Object
				if( $objectType == 'Loan' ) {

					return $SalesforceMod->importAccountObj(	
						$userId, 
						$loanAppNr, 
						$objectId 
					);
					
				}
			} catch(Exception $e){
				return $e->getMessage();
			}
		}
	}
	
	/**
	 * Create Saleforce Object
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @param  string  $objectType
	 * @return array
	 */
	public function create( $userId, $loanAppNr )
	{
		$result = array();

		$SalesforceMod = new SalesforceMod(); 

		try{
  
			$account = $SalesforceMod->createAccount($userId, $loanAppNr );

			$result['account'] = $account;

			if( is_array($account) && isset( $account[0]->success) ) {

				if( $account[0]->success == 1 ) { 
					//Create Salesforce Loan Object
					$result['loan'] = $SalesforceMod->importSFLoanObj(
						$userId, 
						$loanAppNr,
						$account[0]->id
					);  
				}
			}

		} catch( Exception $e){
			writeLogEvent(
				'SalesForce', 
				array(
					'userId' => $userId,
					'message' => $e->getMessage()
				),
				'warning'
			);
		}

		return $result;
	}
	
	/**
	 * Loan Decline 
	 * 
	 * @return
	 */
	protected function decline()
	{
		$data   = Input::all();
		$result = array();
		$option = array();

		if( count($data) > 0 ) { 
			$result = $this->updateLoanAppStatus($data);
		} else{
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'failed', $option );
		}
 
		return Response::json( $result, 200 );
	}

	/**
	 * Loan Approve
	 * 
	 * @return
	 */
	protected function approve()
	{ 
		$data   = Input::all();
		$result = array();
		$option = array();

		if( count($data) > 0 ) {
			$result = $this->updateLoanAppStatus($data);
		} else{
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'failed', $option );
		}

		return Response::json( $result, 200 );
	} 

	/**
	 * New Task
	 * 
	 * @return
	 */
	protected function newTask()
	{
		$data   = Input::all();
		$result = array();
		$option = array();
 
		if( count($data) > 0 ) {

			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);

			if( $appCnt > 0 ){

				try{

					//Determine if the application is in Verification Status
					$loanDetail = \LoanDetail::getLoanDetail(
						$data['User_Id'], 
						$data['Loan_Id'],
						array(
							'Loan_Status_Desc', 
							'Loan_Ref_Nr'
						)
					);

					//@todo - we need to confirm if they can create task 
					//if Loan_Status_Desc != 'VERIFICATION'
					if( $loanDetail->Loan_Status_Desc == 'VERIFICATION' ){
						  
						//Create new NLS Task
						$task = array(
							'UpdateFlag'       => 0, 
							'TaskTemplateName' => $data['NLS_Task_Template'],
							'PriorityCodeName' => 'NORMAL',
							'NLSType'          => 'Loan', 
							'NLSRefNumber'     => $data['Loan_Id'],
							'CreatorUID'       => 0, 
							'OwnerUID'         => 0,
							'OwnerUserName'    => 'Wayde',  
							'StartDate'        => date('m/d/Y'), 
							'DueDate'          => date('m/d/Y'),
							'Subject'          => $data['Subject'], 
							'Notes'			   => '',
						); 	

						$comments = array(); 

						$NLS = new \NLS();
						$option['NLS'] = $NLS->addTask($task, $comments);

						$templateName = $data['NLS_Task_Template'];
						$taskTemplate = \Config::get("NLSTaskTemplate.$templateName");

						//Get the NLS Task Id
						$NLSTask = new \NLSTask();

						$taskRefNo = $NLSTask->getTasksByRefNoTemplateNo(
							$loanDetail->Loan_Ref_Nr, 
							$taskTemplate['TemplateNo']
						);

						$param = array(
							'User_Id'            => $data['User_Id'], 
							'Loan_App_Nr'        => $data['Loan_Id'], 
							'Task_Title_Txt'     => $data['Task_Name'],
							'Task_Id'			 => $taskRefNo->task_refno,
							'NLS_Task_Id'		 => $taskRefNo->task_refno,
							'Task_Name'          => $data['Subject'], 
							'ODS_Verification_Id'=> $data['ODS_Verification_Id'], 
							'SF_Verification_Id' => $data['SF_Verification_Id'],
							'Task_Status_Desc'   => 'OPEN',
							'SF_Task_Id'		 => $data['SF_Task_Id'],
							'Doc_Name'           => $data['Document_Name'],
							'Task_Type_Desc'     => $data['Task_Type_Desc'],
							'Reqd_File_Cnt'      => $data['Reqd_File_Cnt'], 
							'Assigned_CSR_Id'    => $data['Assigned_CSR_Id'], 
							'Comments_Txt'       => $data['Comments'],
							'Subject_Txt'        => $data['Subject'], 
							'Notes_Txt'          => '',
							'Create_Dt'          => date('Y-m-d'), 
							'Created_By_User_Id' => $data['User_Id'],
							'Task_Doc_Upload_URL_Txt' => ''
						);

						//Create New ODS task
						$option['ODS'] = \Task::newTask( $param ); 
						$option['Params'] = $data;
						$result = $this->formatMsgResponse('TASK_CREATED', 'success', $option );
					}
				}catch(Exception $e) {
					$option['Params'] = $data;
					$option['ErrorMessage'] = $e->getMessage();
					$result = $this->formatMsgResponse('INTERNAl_ERROR', 'fail', $option );
				}
			}else{
				$option['Params'] = $data;
				$result = $this->formatMsgResponse( 'APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			} 
		}else{
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'failed', $option );
		}

		return Response::json( $result, 200 );
	}

	/**
	 * Loan Recalculation Method 
	 * 
	 * @return
	 */
	protected function recalc()
	{
		$data = Input::all();

		$result = array();

		if( count($data) > 0 ){

			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);

			if( $appCnt > 0 ){
				
				//Update Applicant Financial Information
				\ApplicantFinancialInfo::where(
					array(
						'User_Id' => $data['User_Id']
					)
				)->update(
					array(
						'Verified_Addtl_Debt_Amt' => $data['Verified_Addtl_Debt_Amt'],
						'Verified_Annual_Gross_Income_Amt' => $data['Verified_Annual_Gross_Income_Amt']
					)
				);

				$whereParam = array(
									'Borrower_User_Id' => $data['User_Id'], 
									'Loan_Id' => $data['Loan_Id']
								);

				//Get the Loan Details
				$loanDetail = \LoanDetail::where($whereParam)
							  ->select(
							  	array(
							  		'Loan_Amt', 
							  		'Loan_Prod_Id',
							  		'Max_Loan_Amt',
							  		'Loan_Template_Id',
							  		'Bank_Acct_Linked_Flag'
							  	))->first();

				//Get the Loan Score
				$riskScore = \ApplicantRiskInfo::where(
					array(
						'User_Id' => $data['User_Id'], 
						'Loan_App_Nr' => $data['Loan_Id']
					)
				)->select('Risk_Score_Val')
				->first();

				//Get Address Data
				$address = \ApplicantAddress::where( array('User_Id' => $data['User_Id']))
						   ->select('State_Cd')
						   ->first();

				//Set the Current Loan Amount
				$currentLoanAmt = $loanDetail->Loan_Amt;

				//Set Bank Account Link Flag to N if empty
				if( $loanDetail->Bank_Acct_Linked_Flag == "" ) {
					$loanDetail->Bank_Acct_Linked_Flag = 'N';
				}

				//Get the Loan Product Rate Map
				$rateMap = \LoanProdRateMap::getLoanProdRateMap( 
						$loanDetail->Loan_Prod_Id,
						$riskScore->Risk_Score_Val,  
						$loanDetail->Bank_Acct_Linked_Flag, 
						$address->State_Cd
				);
  	   
				// Re-run the SP for the Loan Calculation
				$isLoanApproved = \DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?,?,?", 
					array(
						$address->State_Cd,
						$data['User_Id'],
						$riskScore->Risk_Score_Val,
						$loanDetail->Loan_Prod_Id,
						$loanDetail->Bank_Acct_Linked_Flag,
						'Y'
					)
				);
  
				//Retrieve the updated Loan Detail record
				$updatedLoanDetail = \LoanDetail::where($whereParam)
							  ->select(
							  	array(
							  		'Loan_Amt', 
							  		'Monthly_Payment_Amt', 
							  		'Current_Interest_Bal_Amt',
							  		'Current_Principal_Bal_Amt', 
							  		'Loan_Payment_Total_Amt', 
							  		'Current_Debt_Income_Ratio_Val',
							  		'Affordable_Loan_Amt',
							  		'Max_Debt_Income_Ratio_Val' 
							  	))->first();

				$option['Params'] = $data;

				//Check Whether the Applicant is Approved or Not
				if( count($isLoanApproved) > 0 && isset($isLoanApproved[0]->Loan_Qual_Flag ) ) {
					if( $isLoanApproved[0]->Loan_Qual_Flag != 1) {

						if( $updatedLoanDetail->Current_Debt_Income_Ratio_Val > $updatedLoanDetail->Max_Debt_Income_Ratio_Val ) {
						 	$option['Recalc'] = array(
								'User_Id'             => $data['User_Id'], 
								'Loan_Id'             => $data['Loan_Id'], 
								'Loan_Amt'            => $updatedLoanDetail->Loan_Amt, 
								'Response_Flag'       => 'DTI_DISQUALIFICATION',
								'Affordable_Loan_Amt' => $updatedLoanDetail->Affordable_Loan_Amt
						 	);
						 	$result = $this->formatMsgResponse( 'DTI_DISQUALIFICATION', 'success', $option );
					 	}

		 			} else {

		 				$option['Recalc'] = array(
								'User_Id'             => $data['User_Id'], 
								'Loan_Id'             => $data['Loan_Id'], 
								'Loan_Amt'            => $updatedLoanDetail->Loan_Amt,
								'Current_DTIR'        => $updatedLoanDetail->Current_Debt_Income_Ratio_Val,
								'Affordable_Loan_Amt' => $updatedLoanDetail->Affordable_Loan_Amt
						 	);

		 				if( $updatedLoanDetail->Loan_Amt != $currentLoanAmt ) {
							$option['Recalc']['Response_Flag']        = 'DTI_LOAN_AMT_CHANGE';
							$option['Recalc']['Update_Contract_Flag'] = 1;	
							$result = $this->formatMsgResponse( 'DTI_LOAN_AMT_CHANGE', 'success', $option );
		 				} else{  
		 					$option['Recalc']['Response_Flag'] = 'DTI_LOAN_AMT_NO_CHANGE';
		 					$option['Recalc']['Update_Contract_Flag'] = 0; 
		 					$result = $this->formatMsgResponse( 'DTI_LOAN_AMT_NO_CHANGE', 'success', $option );
		 				}  
		 				//@todo
						//Reopen Contract Verification 
		 			}
				}else{
					$result = $this->formatMsgResponse( 'LOAN_SP_FAIL', 'fail', $option );
				}
		   
				//Get Applicant FirstName and Last Name
				$applFields = array(
					'First_Name',
					'Last_Name', 
				);
				
				$applData = \Applicant::getData( 
					$applFields, 
					array('User_Id' => $data['User_Id'] ), 
					true 
				);

				//Get Loan Template
				$loanTemplate = \LoanTemplate::find( $loanDetail->Loan_Template_Id );

				//Update the NLS data
		 	  	$nlsParam = array(
					'nlsLoanTemplate' => $loanTemplate->Loan_Template_Name,
					'contactId' 	  => $data['User_Id'],
					'loanId' 		  => $data['Loan_Id'],
					'firstName'		  => $applData->First_Name,
					'lastName' 		  => $applData->Last_Name,
					'loanAmount'	  => $updatedLoanDetail->Loan_Amt,
					'paymentAmount'	  => $updatedLoanDetail->Monthly_Payment_Amt
				);

		 	  	$NLS = new \NLS();
				$NLS->nlsUpdateLoan($nlsParam);
			 
			}else{
				$result = $this->formatMsgResponse( 'APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			}
			
		}else{
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'fail' );
		}

		return Response::json( $result, 200 );
	}

	/**
	 * Update Loan Amount
	 * 
	 * @return
	 */
	protected function updateLoanAmt()
	{
		$data   = Input::all();
		$result = array();
		$option = array();

		if( count($data) > 0 ) {

			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);

			if( $appCnt > 0 ){

				$whereParam = array(
								'Borrower_User_Id' => $data['User_Id'], 
								'Loan_Id' => $data['Loan_Id']
							);

				//Get the Loan Details
				$loanDetail = \LoanDetail::where($whereParam)
							  ->select(
							  	array(
							  		'Loan_Amt', 
							  		'Loan_Prod_Id',
							  		'Max_Loan_Amt',
							  		'Loan_Template_Id',
							  		'Bank_Acct_Linked_Flag'
							  	))->first();

				//Get the Loan Score
				$riskScore = \ApplicantRiskInfo::where(
					array(
						'User_Id' => $data['User_Id'], 
						'Loan_App_Nr' => $data['Loan_Id']
					)
				)->select('Risk_Score_Val')
				->first();

				//Get Address Data
				$address = \ApplicantAddress::where( array('User_Id' => $data['User_Id']))
						   ->select('State_Cd')
						   ->first();

				 
				//Comparison of new Loan Amount and Old Loan Amount 
				if( $loanDetail->Loan_Amt != $data['Loan_Amt'] ) {

					if( $loanDetail->Bank_Acct_Linked_Flag == "" ) {
						$loanDetail->Bank_Acct_Linked_Flag = 'N';
					}

					//Get the Loan Product Rate Map
					$rateMap = \LoanProdRateMap::getLoanProdRateMap( 
							$loanDetail->Loan_Prod_Id,
							$riskScore->Risk_Score_Val,  
							$loanDetail->Bank_Acct_Linked_Flag, 
							$address->State_Cd
					);

					//Validate the New Loan Amount
					if( $data['Loan_Amt'] > $loanDetail->Max_Loan_Amt 
					|| $data['Loan_Amt'] < $rateMap->Absolute_Min_Loan_Amt ){
						
						$option['RateMap'] = array(
							'Max_Loan_Amt' => $loanDetail->Max_Loan_Amt, 
							'Min_Loan_Amt' => $rateMap->Absolute_Min_Loan_Amt
						);

						$result = $this->formatMsgResponse('INCORRECT_LOAN_AMOUNT', 'fail', $option );

					}else{

						// Update the Loan Amount
						$option['ODS'] = \LoanDetail::where( $whereParam )->update(
							array(
								'Loan_Amt' => $data['Loan_Amt']
							)
						);

						// Re-run the SP for the Loan Calculation
						$option['SP'] = \DB::select("EXEC ODS.dbo.usp_Loan_Decision ?,?,?,?,?,?", 
							array(
								$address->State_Cd,
								$data['User_Id'],
								$riskScore->Risk_Score_Val,
								$loanDetail->Loan_Prod_Id,
								$loanDetail->Bank_Acct_Linked_Flag,
								'Y'
							)
						);

						//Get Applicant FirstName and Last Name
						$applFields = array(
							'First_Name',
							'Last_Name', 
						);
						
						$applData = \Applicant::getData( 
							$applFields, 
							array('User_Id' => $data['User_Id'] ), 
							true 
						);

						//Get Loan Template
						$loanTemplate = \LoanTemplate::find( $loanDetail->Loan_Template_Id );

						//Retrieve the updated Loan Detail record
						$updatedLoanDetail = \LoanDetail::where($whereParam)
									  ->select(
									  	array(
									  		'Loan_Amt', 
									  		'Monthly_Payment_Amt', 
									  		'Current_Interest_Bal_Amt',
									  		'Current_Principal_Bal_Amt', 
									  		'Loan_Payment_Total_Amt', 
									  		'Current_Debt_Income_Ratio_Val'
									  	))->first();

						//Update the NLS data
				 	  	$nlsParam = array(
							'nlsLoanTemplate' => $loanTemplate->Loan_Template_Name,
							'contactId' 	  => $data['User_Id'],
							'loanId' 		  => $data['Loan_Id'],
							'firstName'		  => $applData->First_Name,
							'lastName' 		  => $applData->Last_Name,
							'loanAmount'	  => $updatedLoanDetail->Loan_Amt,
							'paymentAmount'	  => $updatedLoanDetail->Monthly_Payment_Amt
						);

				 	  	$NLS = new \NLS();
						$NLS->nlsUpdateLoan($nlsParam);

						//Prepare the response 
						$option['Params'] = $data;
						$option['UpdatedData']   = array(
							'Loan_Amt'                      => $updatedLoanDetail->Loan_Amt, 
							'Monthly_Payment_Amt'           => $updatedLoanDetail->Monthly_Payment_Amt,
							'Current_Interest_Bal_Amt'      => $updatedLoanDetail->Current_Interest_Bal_Amt,
							'Current_Principal_Bal_Amt'     => $updatedLoanDetail->Current_Principal_Bal_Amt,
							'Loan_Payment_Total_Amt'        => $updatedLoanDetail->Loan_Payment_Total_Amt,
							'Current_Debt_Income_Ratio_Val' => $updatedLoanDetail->Current_Debt_Income_Ratio_Val
						);

						$result = $this->formatMsgResponse( 'LOAN_AMT_UPDATE_SUCCESS', 'success', $option );
					} 	
				}else{
					$option['Params'] = $data;
					$option['UpdatedData'] = array(
						'Loan_Amt' => $loanDetail->Loan_Amt
					);
					$result = $this->formatMsgResponse( 'LOAN_AMT_UNCHANGE', 'success', $option );		
				} 
			}else{
				$result = $this->formatMsgResponse( 'APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			}
			
			
		}else{
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'success', $option );
		}

		return Response::json( $result, 200 );
	}

	/**
	 * Loan Reopen
	 * 
	 * @return
	 */
	protected function reopen()
	{
		$data = Input::all();
		$result = array();
		$option = array();

		if( count($data)  > 0 ) {

			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);

			if( $appCnt > 0 ){

				try{

					//Set the Loan_Status_Desc to VERIFICATION
					$option['Loan'] = \LoanDetail::where(
						array(
							'Borrower_User_Id' => $data['User_Id'],
							'Loan_Id' => $data['Loan_Id']
						)
					)->update(
						array(
							'Loan_Status_Desc' => 'VERIFICATION'
						)
					);
		 	
					//Reopen all Verification Object to OPEN
					$option['Verification'] = \ApplicantVerification::updateData(
						array(
							'User_Id' => $data['User_Id'],
							'Loan_App_Nr' => $data['Loan_Id']
						), 
						array(
							'Verification_Status_Desc' => 'OPEN'
						)
					);

					//Get Loan Reference Number
					$loanDetail = \LoanDetail::getLoanRefNr( $data['User_Id'], $data['Loan_Id'] );

					//Update all NLS task and change them to NOT STARTED
					$NLSTask = new \NLSTask();
					$option['NLS'] = $NLSTask->updateAllNLSTasks(
						$loanDetail->Loan_Ref_Nr, 
						$data['Loan_Id'], 
						$data['User_Id'], 
						'NOT STARTED'
					);

					$option['Params']   = $data;

					$result = $this->formatMsgResponse( 'LOAN_REOPEN', 'success', $option );

				} catch(Exception $e){
					$option['Exception'] = $e->getMessage();
					$result = $this->formatMsgResponse( 'INTERNAl_ERROR', 'fail', $option );
				}
			} else{
				$result = $this->formatMsgResponse( 'APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			}

		} else {
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'failed', $option );
		}

		return Response::json( $result, 200 );
	}


	/**
	 * Update Bank Status
	 * 
	 * @return
	 */
	protected function bankstatus()
	{
		$data   = Input::all();  
		$result = array();
		$option = array();

		if( count($data) > 0 ) {

			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);

			if( $appCnt > 0 ) {

				//Update Applicant Funding Verification

				$option['Params'] = $data;
				$result = $this->formatMsgResponse('BANK_STATUS_UPDATE_SUCCESS', 'success', $option );				
			}else{

				$option['Params'] = $data;
				$result = $this->formatMsgResponse('APPLICATION_DOES_NOT_EXIST', 'fail', $option );
			}

			//Check if the records Exist
			// $bankRecordCnt = BankAccountDetail::where(
			// 	array(
			// 		'User_Id' => $data['User_Id'], 
			// 		'Loan_App_Nr' => $data['Loan_Id'], 
			// 		'Acct_Nr' => $data['Acct_Nr'], 
			// 		'Acct_Type_Desc' => $data['Acct_Type_Desc']
			// 	)
			// )->count();

			//Update Bank Account History Record
			// if( $bankRecordCnt > 0 ) {

			// 	$update = BankAccountDetail::where(
			// 		array(
			// 			'User_Id' => $data['User_Id'], 
			// 			'Loan_App_Nr' => $data['Loan_Id'], 
			// 			'Acct_Nr' => $data['Acct_Nr'], 
			// 			'Acct_Type_Desc' => $data['Acct_Type_Desc']
			// 		)
			// 	)->update(
			// 		array(
			// 			'Current_Bal_Amt' => $data['Current_Bal_Amt'], 
			// 			'Available_Bal_Amt' => $data['Available_Bal_Amt'], 
			// 			'Total_NSF_Cnt' => $data['Total_NSF_Cnt'], 
			// 			'Days_Since_Last_NSF_Cnt' => $data['Days_Since_Last_NSF_Cnt']
			// 		)
			// 	);
			// }

			

		}else {
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'failed', $option );
		}

		return Response::json( $result, 200 );
	}

	/**
	 * New Contract Method
	 * 
	 * @return
	 */
	protected function newContract()
	{
		$data   = Input::all();
		$result = array();
		$option = array();

		if( count($data) > 0 ) {

			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);

			$option['Params'] = $data; 

			if( $appCnt > 0 ) {

				//Loan Status Desc Update
				$option['Loan_Status_Desc_Update'] = \LoanDetail::where(
					array(
						'Borrower_User_Id' => $data['User_Id'], 
						'Loan_Id' => $data['Loan_Id']
					)
				)->update(
					array(
						'Loan_Status_Desc' => 'VERIFICATION'
					)
				);

				//Verification Loan App Phase ID
				$appPhaseId = 7;

				//Update Actv Loan App Phase Number
				LoanApplication::where(
					array(
						'Loan_App_Nr' => $data['Loan_Id'], 
						'Applicant_User_Id' => $data['User_Id']
					)
				)->update(
					array(
						'Actv_Loan_App_Phase_Nr' => $appPhaseId
					)
				);

				//Update Applicant Verifiaction (CONTRACT)
				ApplicantVerification::where(
					array(
						'Loan_App_Nr' => $data['Loan_Id'], 
						'User_Id' => $data['User_Id'], 
						'ODS_Verification_Id' => $data['ODS_Verification_Id']
					)
				)->update(
					array(
						'Verification_Status_Desc' => 'OPEN'
					)
				);
 
				//Update NLS Contract Task to NOT STARTED
				$templateName = ''
				$taskTemplate = \Config::get("NLSTaskTemplate.$templateName");

				//Get the NLS Task Id
				$NLSTask = new \NLSTask();

				$taskRefNo = $NLSTask->getTasksByRefNoTemplateNo(
					$loanDetail->Loan_Ref_Nr, 
					$taskTemplate['TemplateNo']
				);

				

			}else{
				$result = $this->formatMsgResponse('APPLICATION_DOES_NOT_EXIST', 'fail' );
			}

		}else {
			$result = $this->formatMsgResponse( 'MISSING_PARAMS', 'failed', $option );
		}

		return Response::json($result, 200);
	} 

	/**
	 * Update Loan Application Status
	 * 
	 * @param  array  $data
	 * @return
	 */
	protected function updateLoanAppStatus( $data = array() )
	{
		$result = array();

		if( count($data) > 0 ) {

			$appCnt = \LoanDetail::applicationCnt($data['User_Id'], $data['Loan_Id']);

			if( $appCnt > 0 ) {
				
				$update['Loan_Status_Desc'] = $data['Loan_Status_Desc'];

				//Store Adverse Action Code if Application is Decline
				if( $data['Loan_Status_Desc'] == 'APPLICATION_REJECTED') {
					$update['Adverse_Action_Cd'] = $data['Adverse_Action_Cd'];
				} 

				//Set update time and user id
				$update['Update_Dt'] = date('Y-m-d');
				$update['Updated_By_User_Id'] = $data['User_Id'];

				//Update ODS status Desc
				$result = \LoanDetail::where(array(
							'Borrower_User_Id' => $data['User_Id'], 
							'Loan_Id' => $data['Loan_Id']
					))->update($update);
 
				//Update NLS status Desc
				\NLSHelper::updateNLSLoanStatus(
					$data['User_Id'], 
					$data['Loan_Id'],
					$data['Loan_Status_Desc']
				);

				//Get Loan Reference Number
				$loanDetail = \LoanDetail::getLoanRefNr( $data['User_Id'], $data['Loan_Id'] );

				$option['Params'] = $data;

				$NLSTask = new \NLSTask(); 

				//Create a Final Approval Task if the application 
				//has been approved else close all related tasks 
				//for that application
				if( $data['Loan_Status_Desc'] == 'LOAN_APPROVED') {

					$NLSTask->createFinalApprovalTask( 
						$loanDetail->Loan_Ref_Nr, 
						$data['Loan_Id'], 
						$data['User_Id'],
						'APPROVED'
					);

					//Complete All Verification Steps
					\ApplicantVerification::where(
						array(
							'User_Id' => $data['User_Id'],
							'Loan_App_Nr' => $data['Loan_Id']
						)
					)->update(
						array(
							'Verification_Status_Desc' => 'COMPLETE', 
							'Updated_By_User_Id' => $data['User_Id'], 
							'Update_Dt' => date('Y-m-d H:m:s')
						)
					);

					$result = $this->formatMsgResponse('LOAN_APPROVED', 'success', $option );

				}else{

					$NLSTask->updateAllNLSTasks(
						$loanDetail->Loan_Ref_Nr, 
						$data['Loan_Id'],
						$data['User_Id']
					);

					$result = $this->formatMsgResponse('LOAN_DECLINED', 'success', $option );
				}
			}else{
				$result = $this->formatMsgResponse('APPLICATION_DOES_NOT_EXIST', 'fail' );
			}
		}

		return $result;
	}

	/**
	 * Create Loan Object
	 * 
	 * @param  integer $limit
	 * @param  integer $skip
	 * @param  array   $status
	 * @param  boolean $isUpdateAllowed
	 * @return
	 */
	protected function import( $limit = 100, $skip = 0, $status = array(), $isUpdateAllowed = false ) 
	{
 		
 		$result = array(); 

		$loanDetail = LoanDetail::where('Loan_Status_Desc', 'VERIFICATION')
								  ->select('Borrower_User_Id', 'Loan_Id')
								  ->take($limit)
								  ->skip($skip)
								  ->get();

 
		$userIds =  $loanDetail->toArray();

		if( count($userIds) > 0 ) {
			foreach ($userIds as $key => $value) {
				$result[$value['Loan_Id']] = $this->create( 
					$value['Borrower_User_Id'], 
					$value['Loan_Id'] 
				);
			}
		}else {
			$result = array(
				'records' => 0, 
				'message' => 'no records found!'
			);
		}
		 
 		return $result;
	}
}