<?php

namespace Api;

use \Lang as Lang;
use \Salesforce as Salesforce;
use \stdClass as stdClass;
use \View as View;
use \BaseController as BaseController;
/**
 * SalesForce Controller
 */
class ApiController extends BaseController
{

	/**
	 * Constructor
	 */
	public function __construct()
	{

	}

	/**
	 * Authenticate User
	 * 
	 * @return
	 */
	public function authenticate()
	{
		//
	}

	/**
	 * Index 
	 * @return
	 */
	public function index()
	{
		echo 'This is Salesforce API'; 
 	}


	/**
	 * Missing Methods
	 * 
	 * @param  array  $parameters
	 * @return
	 */
	public function missingMethod($parameters = array())
	{
	    echo 'method does not exist.';
	}
  
	/**
	 * Cron
	 * @return
	 */
	public function cron()
	{	

		$importCnt = DB::table('tmp_SalesForce_Import')
				->first();

		$limit = 1; 
		// $skip  = 0;
		$skip  = $importCnt->Total_Item_Imported;

		$loanDetail = LoanDetail::whereNotIn('Loan_Status_Desc', array(
							'VERIFICATION', 'LOAN_FUNDED'	
						)
					)
				  	->select('Borrower_User_Id', 'Loan_Id')
				  	->take($limit)
				  	->skip($skip)
				  	->get();
 
		$userIds =  $loanDetail->toArray();
		
		$SalesforceMod = new SalesforceMod();

		if( count($userIds) > 0 ) {

			foreach ($userIds as $key => $value) {

				$accountId = $SalesforceMod->getAccountId( $value['Borrower_User_Id'] );
				// $accountId = ''; 

				if( !empty( $accountId ) ) {
					$result[$value['Loan_Id']] = $this->update(
						$accountId, 
						$value['Borrower_User_Id'], 
						$value['Loan_Id'],
						'Account'
					);
				} else{
					$result[$value['Loan_Id']] = $this->create( 
						$value['Borrower_User_Id'], 
						$value['Loan_Id'] 
					);	
				}
			}

			//Update Import Count Records
			//Subtracting the Limit			
			$totalItemCnt   = $importCnt->Total_Item_Cnt - $limit;
			$totatItemImptd = $importCnt->Total_Item_Imported + $limit;

			DB::table('tmp_SalesForce_Import')->update(
				array(
					'Total_Item_Cnt' => $totalItemCnt,
					'Total_Item_Imported' => $totatItemImptd, 
					'Import_Dt' => date('Y-m-d')
				)
			);

		}else {
			$result = array(
				'records' => 0, 
				'message' => 'no records found!'
			);
		}
		 
 		return $result;
	}

	public function showAPIHarness()
	{
		$this->layout = 'layouts.application'; 
		$this->setupLayout();
		$this->layout->content = \View::make('blocks.harness.salesforce.api-harness', array());
	}

	/**
	 * Format Message Response
	 * 
	 * @param  string $code
	 * @param  string $type
	 * @param  array  $option
	 * @return
	 */
	protected function formatMsgResponse( $code, $type, $option = array() ) 
	{
		try{
			return $result = array(
				'Status'   => $type, 
				'StatusCd' => $code,
				'Result'   => $option, 
				'Message'  => Lang::get("salesforce.$code")
			);
		}catch(Exception $e){
			//
		}
	}

	/**
	 * Validate API input
	 * 
	 * @return
	 */
	protected function validate( $fields = array(), $rules = array() )
	{ 
		$messages = array();

		if( is_array($fields)  && is_array($rules) ) {

			$validator =  Validator::make(
			    $fields, 
			    $rules
			);

			if ($validator->fails()) {
			    $messages = $validator->messages();
			}
		}

		return $messages;
	}
}