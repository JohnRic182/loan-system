<?php

/**
 * Developer's Note
 *
 * @version v1.0.4.21
 * 
 * @notes
 * 		- Transfer Yodlee Methods to a Package Library
 */

class YodleeController extends BaseController  {
  
 	/**
 	 * Bank Account Layout
 	 * 
 	 * @var string
 	 */
	public $layout  =  'layouts.application';

	/**
	 * Yodlee Object 
	 * 
	 * @var class
	 */
	public $yodlee;

	/**
	 * User Id 
	 * 
	 * @var integer
	 */
	public $userId; 

	/**
	 * Loan App Number
	 * 
	 * @var integer
	 */
	public $loanAppNr; 
  
 	/**
 	 * Constructor
 	 */
	public function __construct()
	{
	 	$this->yodlee  =  new Yodlee();
	}

	/**
	 * Set User Id
	 * 
	 * @param 		string $userId
	 * @deprecated  v1.0.4.21
	 */
	public function setUserId($userId)
	{
		Session::put('uid', $userId);
	}

	/**
	 * Get User Id
	 * 
	 * @return integer
	 * @deprecated  v1.0.4.21
	 */
	public function getUserId()
	{
		return Session::get('uid');
	}

	/**
	 * Get Loan Application Number
	 * 
	 * @return integer
	 */
	public function getLoanAppNr()
	{
		return Session::get('loanAppNr');
	}

	/**
	 * Set Loan Application Number
	 * 
	 * @param integer $loanAppNr
	 */
	public function setLoanAppNr($loanAppNr)
	{
		Session::put('loanAppNr', $loanAppNr );
	}
 
	/**
	 * Save Loan Application Phase
	 * 
	 * @return
	 */
	protected function saveLoanAppPhase()
	{
		$subPhaseCode = Input::get('subPhaseCode'); 

		if( !empty($subPhaseCode) ) { 
			$loanAppPhaseNr = 3; 
			setLoanAppPhase( $this->getUserId() , $this->getLoanAppNr(), $loanAppPhaseNr, $subPhaseCode  );
		} 
	}
    
	/**
	 * Put MFA Request For Sites
	 * 
	 * @return
	 */
	protected function putMFARequestForSite()
	{
		$postData = Input::all(); 

		if( count($postData) > 0 ) 
			return $this->yodlee->putMFARequestForSite($postData);
	}
     

	/**
	 * Register New Customer 
	 *
	 * @param  array $param
	 * @return json
	 */
	protected function register( $param = array() )
	{	
		if( count($param) > 0 )
			return $this->yodlee->register($param);
	}

	 
	/**
	 * Save Yodlee Data 
	 * 
	 * @return
	 */
	protected function putYodleeData()
	{
		$method = Request::method();

		//request validation
		if (!Request::isMethod('post'))
		{
		  	return Response::json( array(
					'errorOccured' => true, 
					'errorMsg'     => 'POST method is required!'
				));
		}

		
		$data = Input::all();

		if( count($data) > 0 )
			return $this->yodlee->putYodleeData($data);
	}
   
	/**
	 * Is Yodlee Access Date Exist
	 * 
	 * @param  string  $yodleeAccessDt
	 * @param  integer $userId
	 * @return boolean
	 */
	protected function isYodleeAccessDtExist($yodleeAccessDt, $userId )
	{
		if(!empty($yodleeAccessDt) && !empty($userId) )
			return BankAccountTxnDetails::whereRaw("Yodlee_Access_Dt = $yodleeAccessDt and User_Id = $userId" )->count();
	}

	/**
	 * Create Yodlee Account
	 * 
	 * @return
	 */
 	protected function createYodleeAccount()
 	{	
 		return Response::json( 
 			$this->yodlee->createYodleeAccount()
 		);
 	}
	
	/**
	 * Addsite Account - POST Method
	 *
	 * @param string $siteId
	 * @param array $componentList
	 * @param array $credential
	 */
	protected function postAddSiteAccount()
	{
		$postData = Input::all();
		return $this->yodlee->postAddSiteAccount($postData);
	}

	/**
	 * Get Site Login Form 
	 * 
	 * @param  integer $siteIds
	 * @return
	 */
	protected function getSiteLoginForm( $siteId = NULL )
	{	
		if( !empty($siteId) )
			return $this->yodlee->getSiteLoginForm($siteId);
	}

	/**
	 * Search Bank Information 
	 * 
	 * @return json
	 */
	protected function searchBank()
	{
	 	$postData = Input::all();

	 	if( count($postData) > 0 )
	 		return $this->yodlee->searchBank($postData);
	}
      
	/**
	 * Validate Bank Account  
	 * 
	 * @return
	 */
	protected function validateBankAcctFlag()
	{	
		
		$userId    = $this->getUserId();
		$loanAppNr = $this->getLoanAppNr();

		if( !empty( $userId ) && !empty($loanAppNr) ){

			$flag = LoanDetail::isBankAcctLinked($userId, $loanAppNr);

			return Response::json($flag);
		}
	}

    /** 
     * Sync Local Data
	 * 
	 * @return
	 */
	protected function syncLocalData()
	{

	}


	/**
	 * Show Bank Account Form (Yodlee) - Step 3.1 Loan Application
	 * 
	 * @return
	 */
	protected function showTestBankAccountForm()
	{ 
		$fields  = array('Site_Id', 'Site_Name', 'Popularity_Idx_Val', 'Image_Url_Txt');

		$data['message']  = Session::get('message');
		$data['banks']    = FinancialOrganization::getTopBanks($fields);
  
		$this->layout->content = View::make('blocks.harness.bank', $data);
	 	
	}

	/**
	 * Generate Fast Link Url 
	 * 
	 * @return string
	 */
	public function generateFastLinkUrl()
	{
		$Yodlee = new Yodlee(); 

		return $Yodlee->generateFastLinkUrl();
		
		// return View::make('blocks.harness.fastlink-2-test', $data);
	 
	}

	/**
	 * Fast Link CallBack
	 * 
	 * @return
	 */
	public function fastlinkCallBack()
	{

		$this->layout = null; 
		$this->setupLayout();

		$data['width'] = '95%';
		$input = Input::all();

		if( count($input) > 0 ){ 

			//Generate the Fast Link Again if token expired
			if( isset($input['oauth_error_code']) ) {

				if( $input['oauth_error_code'] == 414 ){
					$this->createYodleeAccount();
				}

				Session::forget('OauthToken');

				$data['link'] = $this->generateFastLinkUrl(); 

				// _pre($data);

			}else{

				if( isset($input['status']) && $input['status'] == 'success') {

					$yodlee = new Yodlee();

					//Rename variables 
					if( isset($input['account_id']) )
						$input['memSiteAccId'] = $input['account_id'];

					$input['isRRSection'] = 'N';
					$input['siteId'] = $yodlee->getSiteId($input['memSiteAccId']);

					if( Session::get('isRRSection') )
						$input['isRRSection'] = 'Y';
 
		 			$transaction = $yodlee->putYodleeData($input);
 
		 			$data = array(
		 				'result' 	=> 'success',
		 				'message'   => 'Member Site Account Id ' . $input['memSiteAccId'] . ' has been successfully added'
		 			);


		 			// dynamic values from calls in Verification/Bank or from LinkBank
					$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $this->getUserId())
											  ->where('Loan_Id','=', $this->getLoanAppNr())
											  ->first()

											  ;
					$data['nextLink'] = '/exclusionCheck/initial';		
					$data['loadMessage'] = 'Redirecting to Exclusion and Scoring';	
					$data['width'] = '95%';

 
					if( count($loanDetail) > 0){
			 			$status = $loanDetail->Loan_Status_Desc;
			 			$data['width'] = '95%';
			 			if(strtolower($status) == 'verification'){
			 				$data['width'] = '75%';
			 				$data['nextLink'] = '/verification/processBank';
			 				$data['loadMessage'] = 'Submitting Information...';	
			 			}
			 		}
		 			
				} else{
 
					$data = array(
		 				'result' 	=> 'failed',
		 				'input'     => $input,
		 				'message'   => 'Adding of bank account failed'
		 			);
	 
				}
			} 
				
		} else{
 
			 $data = array(
 				'result' 	=> 'failed',
 				'input'     => $input,
 				'message'   => 'Adding of bank account failed.'
 			); 
		} 
		 
		return View::make('blocks.application.bank.fastlink-callback', $data );
	} 
}