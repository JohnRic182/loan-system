<?php

/**
 * Developers Notes
 *
 * @todo 
 * 	  - merge export methods into single method
 *    - transfer SQL Queries to Model
 */

class AdminController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Admin Controller
	|--------------------------------------------------------------------------
	*/

	/**
	 * Layout Application
	 * 
	 * @var string
	 */
	protected $layout =  'layouts.admin';

	protected $encryptedPassword =  '5k_Us3r_4cct_P455w0rd';

	protected $email;

	/**
	 * NLS Tasks Code
	 */
	const NLS_STATUS_CODE_NOT_STARTED   = '100001';

	public function __construct()
	{

	}

	/**
	 * Login Page
	 * 
	 * @return
	 */
	public function showIndex()
	{ 
		$this->checkPermission();
		return View::make('admin.login');
	}

	/**
	 * Check Admin Permission
	 * 
	 * @return
	 */
	private function checkPermission()
	{
	   if( Session::get('adminLogged') == TRUE ){
	   		echo 'test';
			return Redirect::route('adminReport');
	   }
	}

	/**
	 * Show Partners
	 * 
	 * @return
	 */
	public function showPartner()
	{
		$data['partners'] = Partner::all();

  		View::share('adminNav', $this->generateAdminNav('partner'));
		$this->layout->content = View::make('admin.partner', $data );  	
	}

	/**
	 * Show Funding 
	 * 
	 * @return
	 */
	public function showFunding()
	{

		Gfauth::decryptData(); 

		$fields = Gfauth::encryptFieldArray(array('First_Name', 'Last_Name' ), array('First_Name','Last_Name','Loan_Prod_Name','Loan_Detail.Loan_Id','Loan_Detail.Loan_Type_Desc','Loan_Detail.Loan_Amt','Pre_Application_Info.Loan_Amt AS Requested_Amt'));

		$data['loans'] = DB::table('Loan_Detail')
			->join('Applicant', 'Loan_Detail.Borrower_User_Id', '=', 'Applicant.User_Id' )
			->join('Loan_Product', 'Loan_Detail.Loan_Prod_Id', '=', 'Loan_Product.Loan_Prod_Id' )
			->leftJoin('Pre_Application_Info', 'Loan_Detail.Borrower_User_Id', '=', 'Pre_Application_Info.Applicant_User_Id')
            ->select($fields)
            ->where('Loan_Status_Desc', '=', 'LOAN_APPROVED')
            ->get();

        $data['loan_group'] = getloanGroup();

        $data['nlsErrors'] = (Session::has('nlsErrors')) ? Session::get('nlsErrors') : array();

		View::share('adminNav', $this->generateAdminNav('funding'));
		$this->layout->content = View::make('admin.report_funding', $data );
	}

	/**
	 * Show Report Tab
	 * 
	 * @return
	 */
	public function showReport()
	{	
	  	// for report type select button
	  	$data['reportType'] = array(
	  		'Report',
	  		'Exclusion Report',
	  		'Marketing Funnel Report',
	  		'Verification Activity Report',
	  		'SPV Loan Report',
	  		'SPV Loan Transfer Report',
	  		'Funded Loan Report'
	  	);

	  	View::share('adminNav', $this->generateAdminNav('report'));
		$this->layout->content = View::make('admin.reports', $data );
	}

	
	/**
	 * Save Parnter Status
	 * 
	 * @return
	 */
	public function savePartnerStatus()
	{
	
		$pid 	= trim(Input::get('Partner_Id'));
		$status = trim(Input::get('Actv_Flag'));

		updatePartnerStatus($pid, $status);

		return Response::json( array('result' => 'success' ) );

	}

	/**
	 * Export Exclusion Report to XLS
	 * 
	 * @param  date $startDate 
	 * @param  date $endDate   
	 * @return
	 */
	public function exportExclusionReport($startDate = null, $endDate = null)
	{

		$dates = array('startDate' => $startDate, 'endDate' => $endDate);

		$docTitle = "ExclusionReport_$startDate-$endDate";		

		Excel::create($docTitle, function($excel) use (&$dates) {
		    $excel->sheet('Exclusion Report', function($sheet) use (&$dates) {

		    	$exclusionData = new VwRptUnderwritingRpt();

		    	$data = $exclusionData->getExclusiontData($dates['startDate'], $dates['endDate'] );

		    	$header = getTableColumns('vw_Rpt_Underwriting_Rpt');

		    	//freeze header
		    	$sheet->freezeFirstRow();

		    	// Append multiple rows
				$sheet->appendRow($header);

				$sheet->cells('A1:BG1', function($cells) {
					// Set font weight to bold
					$cells->setFontWeight('bold');
				});

				foreach ($data as $row) {
					$rowData = (array) $row;
					$sheet->appendRow($rowData);
				}

		    });

		})->export('xls');
		
	}

	/**
	 * Export Marketing Funnel Report to XLS
	 * 
	 * @param  date $startDate 
	 * @param  date $endDate   
	 * @return
	 */
	public function exportMarketingFunnelReport($startDate = null, $endDate = null)
	{

		$dates = array('startDate' => $startDate, 'endDate' => $endDate);

		$docTitle = "MarketingFunnelReport_$startDate-$endDate";		

		Excel::create($docTitle, function($excel) use (&$dates) {
		    $excel->sheet('Marketing Funnel Report', function($sheet) use (&$dates) {

		    	$mktgFunnel = new VwRptMktgFunnel();

		    	$data = $mktgFunnel->getMktgFunnelData($dates['startDate'], $dates['endDate'] );

		    	$header = getTableColumns('vw_Rpt_Mktg_Funnel');

		    	//freeze header
		    	$sheet->freezeFirstRow();

		    	// Append multiple rows
				$sheet->appendRow($header);

				$sheet->cells('A1:AI1', function($cells) {
					// Set font weight to bold
					$cells->setFontWeight('bold');
				});

				foreach ($data as $row) {
					$rowData = (array) $row;
					$sheet->appendRow($rowData);
				}

		    });

		})->export('xls');
		
	}

	/**
	 * Export Verification Activity 
	 * 
	 * @param  date $startDate 
	 * @param  date $endDate   
	 * @return
	 */
	public function exportVerificationActivity($startDate = null, $endDate = null)
	{

		$dates = array('startDate' => $startDate, 'endDate' => $endDate);

		$docTitle = "VerificationActivityReport_$startDate-$endDate";		

		Excel::create($docTitle, function($excel) use (&$dates) {
		    $excel->sheet('Verification Activity Report', function($sheet) use (&$dates) {

		    	$reportData = new VwRptVerificationActivity();

		    	$data = $reportData->getData($dates['startDate'], $dates['endDate'] );

		    	$header = getTableColumns('vw_Rpt_Verification_Activity');

		    	//freeze header
		    	$sheet->freezeFirstRow();

		    	// Append multiple rows
				$sheet->appendRow($header);

				$sheet->cells('A1:P1', function($cells) {
					// Set font weight to bold
					$cells->setFontWeight('bold');
				});

				foreach ($data as $row) {
					$rowData = (array) $row;
					$sheet->appendRow($rowData);
				}

		    });

		})->export('xls');
		
	}

	/**
	 * Export Loan report to XLS
	 * 
	 * @param  date $startDate 
	 * @param  date $endDate   
	 * @return
	 */
	public function exportLoanReport($startDate = null, $endDate = null)
	{

		$dates = array('startDate' => $startDate, 'endDate' => $endDate);

		$docTitle = "LoanReport_$startDate-$endDate";		

		Excel::create($docTitle, function($excel) use (&$dates) {
		    $excel->sheet('Loan Report', function($sheet) use (&$dates) {

		    	$reportData = new VwSpvLoan();

		    	$data = $reportData->getData($dates['startDate'], $dates['endDate'] );

		    	$header = getTableColumns('vw_Rpt_SPV_Loan_Report');

		    	//freeze header
		    	$sheet->freezeFirstRow();

		    	// Append multiple rows
				$sheet->appendRow($header);

				$sheet->cells('A1:M1', function($cells) {
					// Set font weight to bold
					$cells->setFontWeight('bold');
				});

				foreach ($data as $row) {
					$rowData = (array) $row;
					$sheet->appendRow($rowData);
				}

		    });

		})->export('xls');
		
	}

	
	/**
	 * Export Loan Transfer
	 * 
	 * @param  date $startDate 
	 * @param  date $endDate   
	 * @return
	 */
	public function exportLoanTransferReport($startDate = null, $endDate = null)
	{

		$dates    = array('startDate' => $startDate, 'endDate' => $endDate);
		$docTitle = "LoanTransferReport_$startDate-$endDate";		

		Excel::create($docTitle, function($excel) use (&$dates) {
		    $excel->sheet('Loan Transfer Report', function($sheet) use (&$dates) {

		    	$reportData = new VwSpvLoanTransfer();

		    	$data = $reportData->getData($dates['startDate'], $dates['endDate'] );

		    	$header = getTableColumns('vw_Rpt_SPV_Loan_Transfer');

		    	//freeze header
		    	$sheet->freezeFirstRow();

		    	// Append multiple rows
				$sheet->appendRow($header);

				$sheet->cells('A1:I1', function($cells) {
					// Set font weight to bold
					$cells->setFontWeight('bold');
				});

				foreach ($data as $row) {
					$rowData = (array) $row;
					$sheet->appendRow($rowData);
				}

		    });

		})->export('xls');
		
	}

	/**
	 * Export Loan Funded Report
	 * 
	 * @param  date $startDate 
	 * @param  date $endDate   
	 * @return
	 */
	public function exportFundedLoanReport($startDate = null, $endDate = null)
	{

		$dates    = array('startDate' => $startDate, 'endDate' => $endDate);
		$docTitle = "FundedLoanReport_$startDate-$endDate";		

		Excel::create($docTitle, function($excel) use (&$dates) {
		    $excel->sheet('Loan Transfer Report', function($sheet) use (&$dates) {

		    	$reportData = new VwRptFundedLoanRpt();

		    	$data = $reportData->getData($dates['startDate'], $dates['endDate'] );

		    	$header = getTableColumns('vw_Rpt_Funded_Loan_Rpt');

		    	//freeze header
		    	$sheet->freezeFirstRow();

		    	// Append multiple rows
				$sheet->appendRow($header);

				$sheet->cells('A1:N1', function($cells) {
					// Set font weight to bold
					$cells->setFontWeight('bold');
				});

				foreach ($data as $row) {
					$rowData = (array) $row;
					$sheet->appendRow($rowData);
				}

		    });

		})->export('xls');
		
	}


	/**
	 * Marketing Funnel 
	 * 	
	 * @return
	 */
	public function loadMarketingFunnelTable()
	{

		$startDate 	= Input::get('startDate');
		$endDate 	= Input::get('endDate');

		$mktgFunnel = new VwRptMktgFunnel();

		$data['columnsNames'] 	= getTableColumns('vw_Rpt_Mktg_Funnel');

		$data['applicant_data'] = $mktgFunnel->getMktgFunnelData($startDate, $endDate);

		return View::make('reports.admin_report', $data);

	} 

	/**
	 * Exclusion 
	 * 
	 * @return
	 */
	public function loadExclusionTable()
	{

		$startDate 	= Input::get('startDate');
		$endDate 	= Input::get('endDate');

		$exclusionData = new VwRptUnderwritingRpt();

		$data['columnsNames'] 	= getTableColumns('vw_Rpt_Underwriting_Rpt');
		$data['applicant_data'] = $exclusionData->getExclusiontData($startDate, $endDate);

		return View::make('reports.admin_report', $data);

	} 

	/**
	 * Report Table 
	 * 	
	 * @return
	 */
	public function loadReportTable()
	{

		$startDate 					= Input::get('startDate');
		$endDate 					= Input::get('endDate');
		$data['applicant_data'] 	= array();

		return View::make('reports.admin_report', $data);

	} 

	/**
	 * Loan Verification Activity 
	 * 	
	 * @return
	 */
	public function loadVerificationActivityTable()
	{

		$startDate 	= Input::get('startDate');
		$endDate 	= Input::get('endDate');

		$reportData = new VwRptVerificationActivity();

		$data['columnsNames'] 	= getTableColumns('vw_Rpt_Verification_Activity');
		$data['applicant_data'] = $reportData->getData($startDate, $endDate);

		return View::make('reports.admin_report', $data);

	}

	/**
	 * SPV Loan Table 
	 *  		
	 * @return
	 */
	public function loadSpvLoanTable()
	{

		$startDate 	= Input::get('startDate');
		$endDate 	= Input::get('endDate');

		$reportData = new VwSpvLoan();

		$data['columnsNames'] 	= getTableColumns('vw_Rpt_SPV_Loan_Report');
		$data['applicant_data'] = $reportData->getData($startDate, $endDate);

		return View::make('reports.admin_report', $data);

	} 

	/**
	 * SPV Loan Transfer Table 
	 * 	
	 * @return
	 */
	public function loadSpvLoanTransferTable()
	{

		$startDate 	= Input::get('startDate');
		$endDate 	= Input::get('endDate');

		$reportData = new VwSpvLoanTransfer();

		$data['columnsNames'] 	= getTableColumns('vw_Rpt_SPV_Loan_Transfer');
		$data['applicant_data'] = $reportData->getData($startDate, $endDate);

		return View::make('reports.admin_report', $data);

	}

	/**
	 * Funded Loan report
	 * 	
	 * @return
	 */
	public function loadFundedLoanReport()
	{

		$startDate 	= Input::get('startDate');
		$endDate 	= Input::get('endDate');

		$reportData = new VwRptFundedLoanRpt();

		$data['columnsNames'] 	= getTableColumns('vw_Rpt_Funded_Loan_Rpt');
		$data['applicant_data'] = $reportData->getData($startDate, $endDate);

		return View::make('reports.admin_report', $data);

	}

	/**
	 * Do Admin Login 
	 * 
	 * @return 
	 */
	protected function doLogIn()
	{

		$post  = Input::all();
		$match = FALSE;

		if ( count($post) > 0 ) {

			$whereParam = array(
				'User_Name'    => $post['User_Name'], 
				'Password_Txt' => $post['Password_Txt'],
				'User_Role_Id' => 1
			);

			$User    = new User(); 
			$userCnt = $User->checkUser(
							$whereParam, 
							array('User_Name', 'Password_Txt', 'User_Role_Id'), 
							FALSE
						);
			
			if( $userCnt == 1 ){
				Session::put('adminLogged', TRUE );
				return Redirect::to('/admin/report');
			} else{
				return  Redirect::to('admin')
							     ->with( array('error' => 'Invalid Log in Credentials..') 
							);
			}
		} 
	}


	/**
	 * Update NLS Loan Status to LOAN_FUNDED
	 * 
	 * @return
	 */
	public function updateFunding()
	{

		$NLS  = new NLS();

		$post = Input::all();
		$nlsAccountError = array();

		//list of loans aside from RR
		$mailData['ascendPersonalLoans'] = array('ASCEND1', 'SPV1');


		foreach ($post['funded'] as $key => $loanId) {

			//get loan template
			$LD = LoanDetail::find($loanId);
			//userID
			$userID = $LD->Borrower_User_Id;			

			//getemail address via userID 
			$userFields = array('Email_Id', 'Alt_Email_Id');
			$whereParam = array('User_Id' => $userID );	
			$isRow = true;
			$userData   = User::getData( $userFields, $whereParam, $isRow );
			$emailId = $userData->Email_Id;

			//as per chris, update portfolio to Ascend_V1
			$LP = LoanPortfolio::find( '4' );
			$applicant   = getApplicant( $userID , array('First_Name', 'Middle_Name', 'Last_Name', 'Social_Security_Nr') , true );			
			$LT = LoanTemplate::find($LD->Loan_Template_Id);


			//get Risk Segment info 
			$where 			= array('User_Id' => $userID, 'Loan_App_Nr' => $loanId );
			$ariData 		= ApplicantRiskInfo::where($where)->first();
			$riskSegment 	= ( isset($ariData->Risk_Segment_Nr) && $ariData->Risk_Segment_Nr > 0 )? 'S' . $ariData->Risk_Segment_Nr : '';

			//update NLS records
			$fields = array(
				'nlsLoanGroup' 		=> LoanGroup::getLoanGroupName( $post['loan_group'][$loanId] ) ,
				'loanId' 			=> $loanId,
				'loanStatusCode' 	=> 'LOAN_FUNDED',
				'nlsLoanTemplate'	=> $LT->Loan_Template_Name,
				'nlsLoanPortfolio'	=> $LP->Loan_Portfolio_Name,
				'lastName'			=> $applicant['First_Name'],
				'firstName'			=> $applicant['Last_Name'],
				'contactId'			=> $LD->Borrower_User_Id,
				'riskSegment'		=> $riskSegment
			);

			$nlsUpdateStatus = $NLS->nlsUpdateLoanStatus($fields);
			
			// update ods record			
			$LD->Loan_Status_Desc 	= 'LOAN_FUNDED';
			$LD->Loan_Grp_Id 		= $post['loan_group'][$loanId];
			$LD->Loan_Portfolio_Id	= 4;
			
			if($nlsUpdateStatus)
				$LD->save();
			else
				$nlsAccountError[] = $loanId;

			//Update Loan Funded Date on Lead Conversion
			LeadConversion::where( array('Loan_Id' => $loanId ) )->update(array('Loan_Funded_Dt' => date('Y-m-d')));


			//send automated mail here
			$mailData['loanGroup'] = LoanGroup::getLoanGroupName( $post['loan_group'][$loanId] ); 
			
			$mailSubject = ( in_array( $mailData['loanGroup'] , $mailData['ascendPersonalLoans'] ) )? 'Welcome to your new Ascend Personal Loan' : 'Welcome to your new RateRewards Loan';
			// $mailSubject = 'Ascend Consumer Finance Loan Apporval';
			$mailAddress = $emailId;
			$this->sendLoanStatusNotification($mailAddress, $mailSubject, $mailData);


		}

		if( count($nlsAccountError) > 0 )
			Session::flash('nlsErrors', $nlsAccountError);
		else
			Session::flash('successMsg', 'Success!');

		return Response::json(array('result'  => 'success', 'nlsError' => $nlsAccountError, 'nlsResult' => $fields ));


	}


	/**
	 * Generate Navigation Bar
	 * 
	 * @param  stirng $active
	 * @return
	 */
	protected function generateAdminNav($active)
	{

		$nav = array('report', 'funding', 'partner', 'borrower', 'reopen');

		$ret = '<ul class="nav nav-pills">';
		foreach ($nav as $key => $value)
			if($value == $active)
				$ret .= '<li role="presentation" class="active" >' . HTML::link('admin/' . $value, ucfirst($value)) . '</li>';
			else
				$ret .= '<li role="presentation" >' . HTML::link('admin/' . $value, ucfirst($value)) . '</li>';

    	$ret .= '</ul>';
		
		return $ret;

	}

	/**
 	 * Send funding loan status notification 
 	 *      
 	 * @param  string $email
 	 * @param  array  $data
 	 * @return
 	 */
	private function sendLoanStatusNotification( $email, $subject, $data = array() )
	{

		try{
			$this->email 	= $email;
			$this->subject  = $subject;

			Mail::send('emails.auth.funding-notification', $data, function($message)
			{ 
				
				$message->to( $this->email, 'Customer');

			    if( Config::get('system.Ascend.DebugMode') == 'true' ){
			    	$message->cc('chris@ascendloan.com')
			    			->cc('jdizon@vicomm.com')
			    			->cc('fcorugda@vicomm.com');
			    }

			   	$message->subject($this->subject);
			   
			});

		} catch(Exception $e) {

			echo $e->getMessage();

			writeLogEvent('Email Partner Notification Sending', 
					array( 
						'Message'     => $e->getMessage()
					),
					'warning'
				); 
		}
	}

	/**
	 * Show Borrowers
	 * 
	 * @return
	 */
	public function showBorrowers()
	{
		View::share('adminNav', $this->generateAdminNav('borrower'));
		$this->layout->content = View::make('admin.borrower' );
	}

	/**
	 * Show Borrower List based on the searchKey
	 * 
	 * @return
	 */
	public function showBorrowerList($searchKey = '')
	{
		$borrowers = array();
		Gfauth::decryptData(); 

		$fields = Gfauth::encryptFieldArray(array('First_Name', 'Last_Name' ), array('First_Name','Last_Name','Loan_Detail.Loan_Id','Loan_Detail.Borrower_User_Id','Loan_Detail.Loan_Prod_Id'));

		if($searchKey != ''){
			$borrowers = DB::table('Loan_Detail')
				->join('Applicant', 'Loan_Detail.Borrower_User_Id', '=', 'Applicant.User_Id' )
	            ->select($fields)
	            ->where('Loan_Status_Desc', '=', 'LOAN_FUNDED')
	            ->where(function($query) use ($searchKey){
	                 $query->where(DB::raw("CONVERT(VARCHAR(100), DECRYPTBYKEY(First_Name))"), 'like', '%'.$searchKey.'%')->orwhere(DB::raw("CONVERT(VARCHAR(100), DECRYPTBYKEY(Last_Name))"), 'like', '%'.$searchKey.'%');
	             	})
	            ->get();
	            //->where(DB::raw("CONVERT(VARCHAR(100), DECRYPTBYKEY(Last_Name))"), 'like', '%'.$searchKey.'%')
	           
		}		

        return $borrowers;  
	}

	/**
	 * Show Reopen Page
	 * 
	 * @return
	 */
	public function showReopenLoans()
	{
		View::share('adminNav', $this->generateAdminNav('reopen'));
		$this->layout->content = View::make('admin.reopen' );
	}

	/**
	 * Show Borrower List based on the searchKey
	 * 
	 * @return
	 */
	public function getLoanDetails($searchKey = '')
	{
		$this->layout = null;
		$borrowers = array();
		Gfauth::decryptData(); 

		$fields = Gfauth::encryptFieldArray(array('First_Name', 'Last_Name' ), array('First_Name','Last_Name','Loan_Detail.Loan_Id','Loan_Detail.Borrower_User_Id','Loan_Detail.Loan_Prod_Id','Loan_Detail.Current_Interest_Bal_Amt','Loan_Detail.Current_Principal_Bal_Amt','Loan_Detail.Create_Dt','Loan_Detail.Loan_Status_Desc'));

		if($searchKey != ''){
			
			// check searchKey if Number or String
			if(is_string($searchKey)){
				$borrowers = DB::table('Loan_Detail')
				->join('Applicant', 'Loan_Detail.Borrower_User_Id', '=', 'Applicant.User_Id' )
	            ->select($fields)
	            ->where(function($query) use ($searchKey){
	                 $query->where(DB::raw("CONVERT(VARCHAR(100), DECRYPTBYKEY(Last_Name))"), 'like', '%'.$searchKey.'%');
	             	})
	            ->where('Loan_Status_Desc','=','APPLICATION_REJECTED')
	            //->take(1)
	            ->get();	 
			}else{
				$borrowers = DB::table('Loan_Detail')
				->join('Applicant', 'Loan_Detail.Borrower_User_Id', '=', 'Applicant.User_Id' )
	            ->select($fields)
	            ->where(function($query) use ($searchKey){
	                 $query->where('Loan_Detail.Borrower_User_Id', '=', $searchKey);
	             	})
	            ->where('Loan_Status_Desc','=','APPLICATION_REJECTED')
	            //->take(1)
	            ->get();	 
			}

		}		
        return $borrowers;  
	}

	/**
	 * Start Reopen Process
	 * 
	 * @return
	 */
	public function doReopenProcess(){
		// select latest/last status on the loan application detail table
		// check if Verification if LOAN_APP_PHASE_NR = 7
		// if not then should be Initial Process
		$this->layout = null;
		$post 		  = Input::all(); 
		$newStatus 	  = '';
		// $uid  = 5850;
		// $lid  = 4987;
		$NLS  = new NLS();
		
		if(isset($post['uid']) && isset($post['lid'])){
			$uid  = $post['uid'];
			$lid  = $post['lid'];
			// check if from Verification 
			$loanAppDetail = LoanApplicationDetail::where('Applicant_User_Id','=',$uid)
										->where('LOAN_APP_PHASE_NR','=',7)
										->get();
			if(count($loanAppDetail) > 0){
				//print 'Update to Verification'; die;
				//Update ODS Loan Status Code
				updateLoanStatus($lid, 'VERIFICATION');	

				//Update NLS Loan Status
				$this->updateNLSDetails($uid, $lid, 'VERIFICATION');

				// RESET all NLS Tasks that have been created to NOT STARTED
				$this->resetCompletedAppVer($uid, $lid);

				$loanDetail = LoanDetail::where('Borrower_User_Id','=',$uid)
										->where('Loan_Id','=',$lid)
										->get();
				if(count($loanAppDetail) > 0){
					// RESET all NLS Tasks that have been created to NOT STARTED
					$taskFields = array(
							'userId'   => $uid,
							'NLSRefno' => $loanDetail->Loan_Ref_Nr
						);
					updateNotStartedTasks($lid , $taskFields, '', self::NLS_STATUS_CODE_NOT_STARTED);

				}
				
				// Delete Identity verification records from loan_application_detail
				$loanDetail = LoanApplicationDetail::where('Applicant_User_Id','=',$uid)
							->where('Loan_App_Nr','=',$lid)
							->where('LOAN_APP_PHASE_NR','=',7)
							->where('Sub_Phase_Status_Desc','=','Identity Verification Submitted')
							->delete();
					
				$newStatus = 'VERIFICATION';

			}else{
				//print 'Update to APPLICATION_STEP1'; die;
				// else check if from Initial process
				$loanAppDetail = LoanApplicationDetail::where('Applicant_User_Id','=',$uid)
										->where('LOAN_APP_PHASE_NR','!=',7);

				if(count($loanAppDetail) > 0){
					// Update ODS Loan Status Code
					updateLoanStatus($lid, 'APPLICATION_STEP1');	
					// Update NLS Loan Status
					$this->updateNLSDetails($uid, $lid, 'APPLICATION_STEP1');
					// Delete ALL loan_application_detail records for the user
					$loanDetail = LoanApplicationDetail::where('Applicant_User_Id','=',$uid)
							->where('Loan_App_Nr','=',$lid)
							->delete();
					// Delete all APPLICANT_EXCLUSION records for the user	
					$loanDetail = ApplicantExclusion::where('Applicant_User_Id','=',$uid)
							->where('Loan_App_Nr','=',$lid)
							->delete();	
					$newStatus = 'APPLICATION_STEP1';		
				}	
			}

			return Response::json(
			 	array(
			 		'result'  => 'success',
			 		'status'  => $newStatus,
			 		'message' => 'Succesfully Reopened loan!'
			 	), 200
			);
		}
	}


	/**
	 * Reset Completed Verification
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return
	 */
	protected function resetCompletedAppVer( $userId = 0, $loanAppNr = 0 )
	{
	 
 		$compAppVerData = CompletedApplicantVerification::where(
		 					array(
		 						 'User_Id' 	   => $userId
		 						,'Loan_App_Nr' => $loanAppNr
		 					)
		 				)->first();
		 
		if(	$compAppVerData != null ) {
		 	$completedAppVer = unserialize($compAppVerData->Verification_Phase_Status_Txt);
			
			//@override the current data and set the value to NO
			foreach($completedAppVer as $key => $value){
				
				$completedAppVer[$key]['value'] = 'n';
			}
			
			//encode back to json before updating
			$completedAppVerJSON = serialize($completedAppVer);
  			
  			//update data on DB
  			$compAppVerData->where(array(
		 						 'User_Id' 	   => $userId
		 						,'Loan_App_Nr' => $loanAppNr
		 					))->update(array( 
								'Update_Dt'           =>date('Y-m-d'),
								'Updated_By_User_Id'  => $userId,
								'Verification_Phase_Status_Txt' => $completedAppVerJSON
		 					));
		} 
	} 

	/**
	 * Update NLS Loan Status
	 * 
	 * @param  integer $uid
	 * @param  integer $lid
	 * @param  string $status
	 * @return
	 */
	public function updateNLSDetails( $uid, $lid, $status = ''){
		
		$NLS = new NLS();

		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $uid)
					->where('Loan_Id','=',$lid)
					->first();

		if(count($loanDetail) > 0){
			//Get Loan Template Name
			$loanTemplate  = LoanTemplate::getLoanTemplateName($loanDetail->Loan_Template_Id);
			//Get Loan Group Name
			$loanGroup     = LoanGroup::getLoanGroupName($loanDetail->Loan_Grp_Id );
			//Get Loan Portfolio Name
			$loanPortfolio = LoanPortfolio::getLoanPortfolioName($loanDetail->Loan_Portfolio_Id);	

			$whereParam = array('User_Id' => $uid);	

			$applicant = new Applicant();
			$applicant_fields = array(
				'First_Name',
				'Middle_Name',
				'Last_Name',
				'Birth_Dt',
				'Social_Security_Nr'
			);
			
			$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

			$fields = array(
				'nlsLoanTemplate'  => $loanTemplate,
				'nlsLoanGroup'     => $loanGroup,
				'nlsLoanPortfolio' => $loanPortfolio,
				'contactId'        => $uid,
				'loanId'           => $lid,
				'firstName'        => $applicantData->First_Name,
				'lastName'         => $applicantData->Last_Name,
				'loanStatusCode'   => $status
			);
			
			$NLS->nlsUpdateLoanStatus($fields);	
		}
		
	}


}