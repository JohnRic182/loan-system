<?php

/**
 * Developers Notes
 *
 * Updated:
 *   - Add Agreement Page
 *
 *
 * Todo
 *    - create only 1 view for all consent
 */

class HelloSignController extends BaseController {


	/**
	 * HelloSign API Callback
	 *     - We're not using any External Document 
	 *       Signing so we can just empty the method
	 *       as of the moment.
	 * @return
	 */
	public function documentCallback()
	{
		$data = Input::all();

		echo 'Hello API Event Received';
	}
}