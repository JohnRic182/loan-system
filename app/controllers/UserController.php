<?php

/**
 * Developers Notes
 *
 * @todo 
 * 	  - use lang->en->messages for any success and error notifications
 *    - update credit check page
 */


class UserController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| User Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'UserController@index');
	|
	*/

	/**
	 * The layout that should be used for responses
	 * 
	 * @var string 
	 */
	public $layout  =  'layouts.master';

	/**
	 * Email Address
	 * 
	 * @var string
	 */
	protected $email   =  '';

	/**
	 * SessioN Data
	 * 
	 * @var
	 */
	protected $sessionData;

	/**
	 * Default Product Id 
	 *    - Ascend Loan 
	 */
	const DEFAULT_PRODUCT_ID   = 1;
	/**
	 * Default Product description
	 */
	const DEFAULT_PRODUCT_TXT  = 'Ascend Loan';

	/**
	 * Default Loan Amount
	 */
	const DEFAULT_LOAN_AMT     = 5000;
	const DEFAULT_MIN_LOAN_AMT = 2600;
	const DEFAULT_MAX_LOAN_AMT = 10000;

	/**
	 * Default Purpose Id
	 */
	const DEFAULT_PURPOSE_ID   = 1; 

	/**
	 * Default Loan Purpose description
	 */
	const DEFAULT_PURPOSE_TXT  = 'Debt Consolidation'; 

	/**
	 * Default Credit description
	 */
	const DEFAULT_CREDIT_TXT = null;

	/**
	 * Default User Role Id
	 *    - Customer
	 */
	const DEFAULT_USER_ROLE_ID = 2;
		
	public function __construct() 
	{
		$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);		

	}

	/**
	 * Index Action
	 *
	 *
	 * @version v1.0.4.21
	 * @return
	 */
	protected function index( $variation = '' )
	{
	
		if( !empty( $variation ) ){
			setHompageRelativeUrl(); //Set the relative URL for the partner hompage
		} else{
			Session::forget('HPURL'); //forget the relative URL on homepage only
		}

		$prodFullNameArray   = array();

		$data['isProductionQA']     = FALSE;
		$data['isProductionBypass'] = TRUE;

		//@Override Production QA
		if( Input::get('case')      == 'test' )
			$data['isProductionQA']     = TRUE;
		if( Input::get('bypass')    == 0 )
			$data['isProductionBypass'] = FALSE;

		//get url parameters
		$PID = trim(Input::get('PID'));

		//Check and store CID and PID int the session for later use in the regitration
		trackPartnerIds($PID);

		//Get All Product Available
		$loanProduct = getLoanProductList();

		foreach( $loanProduct as $list ) {

			$prodNameArray = explode(" ", $list['Loan_Prod_Name']);
			$prodFullName  = '';

			foreach( $prodNameArray as $name ) {
				
				$others = explode($name[0], $name);
				$length = strlen($name);

				if( $name == 'RateReward' ){
					$prodFullName .= '<span class="prodName">'.$name[0].'</span>'.$others[1].'<span class="prodName">R</span>'.'eward';
				} else {
					$prodFullName .= '<span class="prodName">'.$name[0].'</span>'.$others[1].'';
				}

				$prodFullName .= ' ';
						
			}

			$newProd = array(
				'Loan_Prod_Id'        => $list['Loan_Prod_Id']  ,
				'Loan_Prod_Name'      => $list['Loan_Prod_Name']  ,
				'Loan_Prod_Full_Name' => $prodFullName,
				'Loan_Prod_Desc'      => $list['Loan_Prod_Desc']  ,
				'Funding_Partner_Id'  => $list['Funding_Partner_Id']  ,  
			);

			array_push($prodFullNameArray, $newProd);
		}

		$data['productListNames'] = $prodFullNameArray;

		//set slider value
		$partnerUserAcct = new PartnerUserAcct(); 
		$partnerUserInfo = $partnerUserAcct->getData( ['Offered_Loan_Amt'] , ['Partner_User_Id'  => Session::get('partner.UID')], true );
		
		$data['defaultSliderAmt'] = (count($partnerUserInfo) > 0 )? $partnerUserInfo->Offered_Loan_Amt : 5000; 

		//Query String for State Code
		if( !empty( Input::get('state') ) ){
			$stateCode = Input::get('state');
		} else{
			$stateCode = getStateCodeViaIp();
		}

		$data['sliderMinLoanAmt'] = self::DEFAULT_MIN_LOAN_AMT;
		$data['sliderMaxLoanAmt'] = self::DEFAULT_MAX_LOAN_AMT;

		if( !empty($stateCode) ) {
			//Select Loan Amount Range Depending on State
			$rateMap = LoanProdRateMap::getLoanAmtRange($stateCode);

			if( count($rateMap) > 0 ) {
				$data['sliderMinLoanAmt'] = $rateMap->Absolute_Min_Loan_Amt;
				$data['sliderMaxLoanAmt'] = $rateMap->Absolute_Max_Loan_Amt;
			}
		}
  
		//Set as homepage  
		$this->layout->homepage = true;

		if( !empty($variation) ){
			$this->layout->content = View::make('blocks.user.index-'.$variation , $data );
		} else {
			$this->layout->content = View::make('blocks.user.index', $data );	
		}
	}

  
	/**
	 * Show Login Form 
	 * 
	 * @return
	 */
	protected function showLogin()
	{  

		// dd(Cookie::has('Partner_LT'));
		// _pre(Cookie::get('Partner_LT'));

		$this->layout = 'layouts.application';
		$this->setupLayout();

		//Get Security Question
		$fields   = array('Security_Question_Txt', 'Security_Question_Id'); 
		$security = SecurityQuestion::getData($fields);
		
		$data['security'] = array();

		foreach ($security as $value){
			$data['security'][$value->Security_Question_Id] = $value->Security_Question_Txt;
		}

		$this->layout->content = View::make('blocks.user.login', $data);
	}
 
 	/**
	 * Save Security Answers
	 * 
	 * 	- Save answers to security question on ODS and NLS
	 *
	 * @deprecated 
	 * @return
	 */
 	protected function saveSecurityQuestion()
 	{
 		$data = Input::all();

 		$uid   = Session::get('uid');

 		$NLS   = new NLS();

		//Get User Account Information
		$userAccount = User::getData(array('User_Name','Password_Txt'), array('User_Id' => $uid), true );
		
		$securityData = array(); 

		for ( $i=1; $i < 3; $i++ ) { 
			$securityData[] = array( 	
				'User_Id'              => $uid,
				'Security_Question_Id' => $data[ 'securityQuestion'. $i ],
				'Answer_Txt'           => Gfauth::encryptFields($data['securityQuestionAnswer'. $i ]),
				'Created_By_User_Id'   => $uid,
				'Create_Dt'			   => date('Y-m-d')
			);
		}
			
		//Store Security Info
		if( count($securityData) > 0 ) 
		UserSecurityAnswer::insert($securityData);	

		//SECURITY QUESTION
		$security           = new SecurityQuestion();

		$securityFields1    = array( 'Security_Question_Txt');
		$securityWhereParam = array('Security_Question_Id' => $data['securityQuestion1']);	
		$securityQuestions1 = $security->getData($securityFields1,$securityWhereParam);

		$securityFields2    = array('Security_Question_Txt');
		$securityWhereParam = array('Security_Question_Id' => $data['securityQuestion2']);	
		$securityQuestions2 = $security->getData( $securityFields2, $securityWhereParam );
		
		// UserSecurityAnswer
		//Create NLS contact object, verify Username and Password and returns error if any				

		$NLS->NSLUpdateWebCredentials($uid, array(
			'UserName' 		=> $userAccount->User_Name, 
			'UserPassword' 	=> $userAccount->Password_Txt,
			'Hint1' 		=> str_replace('"',"'",$securityQuestions1[0]->Security_Question_Txt),
			'Hint1Answer' 	=> $data['securityQuestionAnswer1'],
			'Hint2' 		=> str_replace('"',"'",$securityQuestions2[0]->Security_Question_Txt),
			'Hint2Answer' 	=> $data['securityQuestionAnswer2']
		));

		return Response::json(
			array(
				'result'     		=> 'success',
				'message'    		=> 'success',
				'username'   		=> $userAccount->User_Name,
				'password'   		=> $userAccount->Password_Txt
			), 200
		);
		
 	}
 
 
	/**
	 * User Login
	 * 
	 * 	- Get the Loan Application Phase Data
	 * 	- Get the Pre Application Phase Data
	 * 	- Get the ARS Scoring
	 * 	
	 * @return
	 */
	protected function doLogin()
	{ 

		$input = Input::all();
		$User  = new User(); 

		$userObj = $User->checkUser(
			array( 
				$input['Email'], 
				$input['Password'] 
			),
			array(
				 'User_Id'
				,'Email_Id'
				,'User_Name'
				,'User_Role_Id'
			)
		);
	
		if( count( $userObj ) > 0 ){

			Session::put('logged', 1);
			Session::put('uid', $userObj->User_Id);
			Session::put('userName', $userObj->User_Name);
			Session::put('email', $userObj->Email_Id);
			Session::put('userRoleId', $userObj->User_Role_Id);
			
			setApplicantData( $userObj->User_Id );

			//Store User Login History
			$this->updateUserLoginHistory( $userObj->User_Id );
 	
			//Get the latest application that which is done by the applicant
			$latestLoanApp = LoanApplication::getLatestApplication( $userObj->User_Id );

			$loanPhaseUrl = array();

			//We will use the Loan App Phase to determine the last step 
			//where the user left if there is an existing application 
			//else we use the Pre App Phase based on the User Account	
			if( count($latestLoanApp) > 0 ){

				$loanDetails = LoanDetail::getLoanDetail( $userObj->User_Id, $latestLoanApp->Loan_App_Nr ); 

 				//Check if there is no data in Loan Detail
 				if( count($loanDetails) > 0 ) {

 					//Get Loan Application Text
					$LoanPurpose = LoanPurpose::find( $latestLoanApp->Loan_Purpose_Id);

					//Set Loan Detail into Session
					setLoanSessionData(array(
						'recoLoanProdId'  => $latestLoanApp->Reco_Loan_Prod_Id,
						'applicantUserId' => $latestLoanApp->Applicant_User_Id,
						'statusDesc'	  => $loanDetails->Loan_Status_Desc,
						'loanProductId'   => $loanDetails->Loan_Prod_Id,
						'loanAmt'   	  => $loanDetails->Loan_Amt,
						'loanPurposeId'	  => $LoanPurpose->Loan_Purpose_Id, 
 						'loanPurposeTxt'  => $LoanPurpose->Loan_Purpose_Desc
					));

					if($loanDetails->Loan_Status_Desc == 'LOAN_FUNDED'){	

						// check if theres an existing security answers
						$securityAnswerResults = UserSecurityAnswer::where('User_Id', '=', $userObj->User_Id)->get();
						if( count($securityAnswerResults) > 0){
							return Response::json(
								array(
									'result'     		=> 'success',
									'message'    		=> showNotification('userLoginSuccess'),
									'loanAppUrl' 		=> 'borrowerportal',
									'username'   		=> $userObj->User_Name,
									'password'   		=> $input['Password'],
									'action'			=> 'proceedToBorrowerPortal'
								), 200
							);
						}else{
							return Response::json(
								array(
									'result'     		=> 'success',
									'message'    		=> showNotification('userLoginSuccess'),
									'loanAppUrl' 		=> 'borrowerportal',
									'username'   		=> $userObj->User_Name,
									'password'   		=> $input['Password'],
									'action'			=> 'showSecurityQuestion'
								), 200
							);	
						}
						
					}

					//Check if Loan_Status_Desc is REJECTED,
					//Redirect the Applicant to Disqualification Page
					if( $loanDetails->Loan_Status_Desc == 'APPLICATION_REJECTED' ) {
						 
						return Response::json(
							array(
								'result'     => 'success',
								'message'    => showNotification('userLoginSuccess'),
								'loanAppUrl' => 'disqualification'
							), 200
						);
					}

				} else {
					//Get Applicant Pre Application Phase Information
					$preAppPhaseInfo = getApplPreAppPhaseInfo( $userObj->User_Id );

					//Set Pre Application Phase Info
					if( count( $preAppPhaseInfo ) > 0 ) {

						//Get Loan Application Text
						$LoanPurpose = LoanPurpose::find( $preAppPhaseInfo->Loan_Purpose_Id );

						//Loan Product and Loan Purpose into Session
						setLoanSessionData(array(
								'loanProductId' => $preAppPhaseInfo->Loan_Prod_Id, 
								'loanAmt'   	=> $preAppPhaseInfo->Loan_Amt,
								'loanPurposeId'	=> $preAppPhaseInfo->Loan_Purpose_Id, 
								'loanPurposeTxt'=> $LoanPurpose->Loan_Purpose_Desc
							)
						); 
						
					}
				}

				 
				//Get Loan App Phase Url
				$loanPhaseUrl = $this->getPreviousApplicationPhase(
									    $userObj->User_Id, 
									    $latestLoanApp->Loan_App_Nr
									);
				
				//Set Loan Application URL
				if( count($loanPhaseUrl) > 0 )
					setLoanAppNr($latestLoanApp->Loan_App_Nr);

				

				//Get ARS Score Value for Applicaton Risk Info Table
				$RiskInfo  = ApplicantRiskInfo::getRiskScore( $latestLoanApp->Loan_App_Nr );

				if( isset( $RiskInfo->Risk_Score_Val ) ) {
					Session::set('ARSScore', $RiskInfo->Risk_Score_Val );
				}

			} else {

				//Get Applicant Pre Application Phase Information
				$preAppPhaseInfo = getApplPreAppPhaseInfo( $userObj->User_Id );

				if( count( $preAppPhaseInfo ) > 0 ) {

					//Redirect to Credit Check and assign Loan Amount
					//Loan Product and Loan Purpose into Session
					setLoanSessionData(array(
							'loanProductId' => $preAppPhaseInfo->Loan_Prod_Id, 
							'loanAmt'   	=> $preAppPhaseInfo->Loan_Amt,
							'loanPurposeId'	=> $preAppPhaseInfo->Loan_Purpose_Id
						)
					);

					$loanPhaseUrl = Config::get('loan.'.'LOAN_APP_PHASE1') ;
				}
			}
 	
			return Response::json(
				array(
					'result'     => 'success',
					'message'    => showNotification('userLoginSuccess'),
					'loanAppUrl' => $loanPhaseUrl
				), 200
			);

		} else { 

		 	return Response::json(
				array(
					'result'  	 => 'failed',
					'message' 	 => showNotification('userLoginFailed'), 
					'loanAppUrl' => ''
				), 200
			);
		} 
	}

	/**
	 * Get Previous Loan Application Phase
	 *  
	 * @param  integer $userId
	 * @param  integer $loanAppNr
	 * @return
	 */
	protected function getPreviousApplicationPhase( $userId , $loanAppNr )
	{

		$loanAppUrl = '';

		if( !empty($userId) && !empty($loanAppNr) ) {
			
			//Get the Loan Application Details to Get the Application Phase
			$loanApp = LoanApplication::getActiveLoanAppPhase(
										$userId, 
										$loanAppNr
									);
			
			//Set redirection based on Loan Application URL
			if( !empty( $loanApp ) ) {

				if( !empty( $loanApp->Actv_Loan_App_Phase_Nr ) ){
					$loanAppUrl = Config::get('loan.'.'LOAN_APP_PHASE'. $loanApp->Actv_Loan_App_Phase_Nr) ;
				}
			}
		}

		return $loanAppUrl;
	}

	/**
	 * Do Logout 
	 * 
	 * @return
	 */
	protected function doLogout()
	{
		logout();
		return Redirect::to('/');
	}


	/**
	 * Show Forgot Password Page
	 *
	 * @todo  
	 * 	  - we need to change the handling of boolean
	 * @return
	 */
	protected function showForgotPassword()
	{

		$this->layout = 'layouts.application';
		$this->setupLayout();

		$requestStatus 		= Request::segment(2);

		$data['statusMsg'] 	= null;
		$data['form'] = array(
			'title' => 'Passowrd Recovery',
			'id'	=> 'ForgetPasswordForm',
			'url'	=> 'forgotPassword'
		);

		if( isLoggedIn() )
			return Redirect::to('/');
		else{

			switch ($requestStatus) {
				case 'EXPIRED':
					$data['statusMsg'] = "Your request to reset your password has expired. Please request a new one.";
					break;
				
				case 'IN_PROGRESS':
					$data['statusMsg'] = "You still have a pending request to reset your password please check your mail";
					break;
			}		

		}

		$this->layout->content = View::make('blocks.user.forgot-password', $data);
	}

	/**
	 * Show forgot username Page
	 *
	 * @return
	 */
	protected function showForgotUser()
	{

		$this->layout = 'layouts.application';
		$this->setupLayout();

		$requestStatus 		= Request::segment(2);

		$data['statusMsg'] 	= null;
		$data['form'] = array(
			'title' => 'Usename Recovery',
			'id'	=> 'ForgetUsernameForm',
			'url'	=> 'forgotUsername'
		);

		if( isLoggedIn() )
			return Redirect::to('/');
		else{

			switch ($requestStatus) {
				case 'EXPIRED':
					$data['statusMsg'] = "Your request to reset your username has expired. Please request a new one.";
					break;
				
				case 'IN_PROGRESS':
					$data['statusMsg'] = "You still have a pending request to reset your username please check your mail";
					break;
			}		

		}

		$this->layout->content = View::make('blocks.user.forgot-password', $data);
	}

	/**
	 * Home A/B Google Experiment
	 */
	protected function showHomeExperiment()
	{

		$data['isProductionQA'] = FALSE;
		$data['isProductionBypass'] = TRUE;

		//@Override Production QA
		if( Input::get('case') == 'test' )
			$data['isProductionQA'] = TRUE;
		if( Input::get('bypass') == 0 )
			$data['isProductionBypass'] = FALSE;

		//get url parameters
		$PID    = trim(Input::get('PID'));

		//Check and store CID and PID int the session for later use in the registration
		trackPartnerIds($PID);

		//set slider value
		$partnerUserAcct = new PartnerUserAcct(); 
		$partnerUserInfo = $partnerUserAcct->getData( ['Offered_Loan_Amt'] , ['Partner_User_Id'  => Session::get('partner.UID')], true );
		
		$data['default_slider_val'] = (count($partnerUserInfo) > 0 )? $partnerUserInfo->Offered_Loan_Amt : 5000; 

		$this->layout = View::make('layouts.experiment');
		$this->layout->homepage = true;
		$this->layout->content  = View::make('blocks.experiment.index', $data );
	}

	/**
	 * Send Forgot Password Email
	 * 
	 * @return
	 */
	protected function sendPassword()
	{	
		
		$this->layout = 'layouts.application';
		$this->setupLayout();

		$requestStatus = '';

		$user  		= new User();
		$resetInfo 	= new PasswordResetInfo();

		$this->email = trim(Input::get('Email'));

		$userData = $user->getDataByEmail(array('User_Id', 'Create_Dt'), $this->email);
		//create reset key
		$resetKey = generateRandomKey(30);

		if( count( $userData ) > 0 ):			

			$resetData = $resetInfo->getData( array('*'), array('Password_Reset_Status_Desc' => 'IN_PROGRESS', 'User_Id' => $userData->User_Id) );

			if( count( $resetData ) > 0 ){
				$requestStatus = "IN_PROGRESS";
				return Redirect::to('forgotPassword/'.$requestStatus);
			}else{

				//save to password reset info
				$resetInfo->User_Id 					= $userData->User_Id;
				$resetInfo->Reset_Verification_Cd 		= $resetKey;
				$resetInfo->Password_Reset_Status_Desc 	= 'IN_PROGRESS';
				$resetInfo->Create_Dt 					= date('Y-m-d');
				$resetInfo->Update_Dt 					= date('Y-m-d');
				$resetInfo->Created_By_User_Id			= $userData->User_Id;
				$resetInfo->Updated_By_User_Id			= $userData->User_Id;
				$resetInfo->save();

				$url = url('/reset-password/'.$resetKey);

					// send mail for the reset URL

					Mail::send('emails.auth.forgot-password', array('url' => $url), function($message){

					    $message->to($this->email, 'Customer')
					    		->subject('Password Recovery Notification - Ascend Loan');
					    		// ->cc('jricana@global-fusion.net')
					    		// ->cc('fcorugda@global-fusion.net');
					});
				
			}
		

		endif;

		$data['notice'] = 'password';
		$this->layout->content = View::make('blocks.user.forgot-password-notice', $data);
		
	}

	
	/**
	 * Send Forgot Username Email
	 * 
	 * @return
	 */
	protected function sendUsername()
	{

		$this->layout = 'layouts.application';
		$this->setupLayout();
		
		$user  		= new User();
		$resetInfo 	= new PasswordResetInfo();

		$requestStatus = '';

		$this->email = trim(Input::get('Email'));


		$userData = $user->getDataByEmail(array('User_Id', 'Create_Dt'), $this->email);

		//create reset key
		$resetKey = generateRandomKey(30);


		if( count( $userData ) > 0 ):			

			$resetData = $resetInfo->getData( array('*'), array('Password_Reset_Status_Desc' => 'IN_PROGRESS', 'User_Id' => $userData->User_Id) );

			if( count( $resetData ) > 0 ){
				$requestStatus = "IN_PROGRESS";
				return Redirect::to('forgotUser/'.$requestStatus);
			}else{

				//save to password reset info
				$resetInfo->User_Id 					= $userData->User_Id;
				$resetInfo->Reset_Verification_Cd 		= $resetKey;
				$resetInfo->Password_Reset_Status_Desc 	= 'IN_PROGRESS';
				$resetInfo->Create_Dt 					= date('Y-m-d');
				$resetInfo->Update_Dt 					= date('Y-m-d');
				$resetInfo->Created_By_User_Id			= $userData->User_Id;
				$resetInfo->Updated_By_User_Id			= $userData->User_Id;
				$resetInfo->save();

				$url = url('/reset-user/'.$resetKey);

					// send mail for the reset URL

					Mail::send('emails.auth.forgot-username', array('url' => $url), function($message){

					    $message->to($this->email, 'Customer')
					    		->subject('Username Recovery Notification - Ascend Loan')
					    		// ->cc('jdizon@vicomm.com');
					    		->cc('fcorugda@vicomm.com');
					});
				
			}
		

		endif;


		$data['notice'] = 'username';
		$this->layout->content = View::make('blocks.user.forgot-password-notice', $data);

	}


	/**
	 * Show reset password page
	 * 
	 * @return
	 */
	protected function showResetPassword()
	{

		$this->layout = 'layouts.application';
		$this->setupLayout();

		//this is where you check if the password has expired
		$data['key'] 			= Request::segment(2);
		$today 					= date('Y-m-d');
		$data['requestStatus'] 	= '';

		$resetInfo = new PasswordResetInfo();

		$resetData = $resetInfo->getData(array('*'), array('Reset_Verification_Cd' => $data['key']));

		// pre($resetData);

		$numDays = countDays($resetData->Create_Dt, $today);

		if($resetData->Password_Reset_Status_Desc == 'COMPLETE')
			return Redirect::to('forgotPassword/');		

		if($numDays > 3){
			$data['requestStatus'] = "EXPIRED";
			// Update Status
			$resetData->Password_Reset_Status_Desc = $data['requestStatus'];
			$resetData->save();
			return Redirect::to('forgotPassword/'.$data['requestStatus']);
		}
		
		$this->layout->content = View::make('blocks.user.reset-password', $data);
	}

	/**
	 * Process Password Reset
	 * 
	 * @return
	 */
	protected function resetPassword()
	{

		$this->layout = 'layouts.application';
		$this->setupLayout();

		$key    = trim(Input::get('key'));
		$date     = trim(Input::get('date'));
		$email    = trim(Input::get('email'));
		$password = trim(Input::get('password'));

		//check if password is valid
		if( !ctype_alnum(trim($password)))
		{
		    return Response::json(array(
					'result'        => 'failed',
					'errorMessage'  => showNotification('invalidPassword')
				));
		}

		//@todo 
		//	-transfer the query to model 
		$user 		= new User();
		$resetInfo 	= new PasswordResetInfo();

		$resetData = $resetInfo->getData( array('*'), array('Reset_Verification_Cd' => $key) );

		$root = url('/');

		if( $user->updatePassword( $resetData->User_Id, $password ) == true ) {

			$resetInfo->updateStatus($resetData->Reset_Verification_Cd, 'COMPLETE');

			$data['root'] = $root;
			$data['notice'] = "password";

			if( Session::has('resetEmail') )
	          	Session::forget('resetEmail');

	        Session::flush();

			$this->layout->content = View::make('blocks.user.reset-password-success', $data );
		} 
	}
  	 
  	/**
	 * Show reset password page
	 * 
	 * @return
	 */
	protected function showResetUsername()
	{

		$this->layout = 'layouts.application';
		$this->setupLayout();

		//this is where you check if the password has expired
		$data['key'] 			= Request::segment(2);
		$today 					= date('Y-m-d');
		$data['requestStatus'] 	= '';

		$resetInfo = new PasswordResetInfo();

		$resetData = $resetInfo->getData(array('*'), array('Reset_Verification_Cd' => $data['key']));

		// pre($resetData);

		$numDays = countDays($resetData->Create_Dt, $today);

		if($resetData->Password_Reset_Status_Desc == 'COMPLETE')
			return Redirect::to('forgotUser/');		

		if($numDays > 3){
			$data['requestStatus'] = "EXPIRED";
			// Update Status
			$resetData->Password_Reset_Status_Desc = $data['requestStatus'];
			$resetData->save();
			return Redirect::to('forgotPassword/'.$data['requestStatus']);
		}
		
		$this->layout->content = View::make('blocks.user.reset-username', $data);
	}

	/**
	 * Process Username Reset
	 * 
	 * @return
	 */
	protected function resetUsername()
	{

		$this->layout = 'layouts.application';
		$this->setupLayout();

		$key      = trim(Input::get('key'));
		$date     = trim(Input::get('date'));
		$email    = trim(Input::get('email'));
		$username = trim(Input::get('username'));

		$user 		= new User();
		$resetInfo 	= new PasswordResetInfo();
		$NLS  		= new NLS();

		$resetData = $resetInfo->getData( array('*'), array('Reset_Verification_Cd' => $key) );

		$root = url('/');

		if( $user->updateUsername( $resetData->User_Id, $username ) == true ) {

			//Get User Account Information
			$userAccount = User::getData(array('Password_Txt'), array('User_Id' => $resetData->User_Id), true );


			$resetInfo->updateStatus($resetData->Reset_Verification_Cd, 'COMPLETE');

			$NLS->NSLUpdateWebCredentials($resetData->User_Id, array('UserName' => $username, 'UserPassword' => $userAccount->Password_Txt ));

			$data['root'] = $root;
			$data['notice'] = "username";

			if( Session::has('resetEmail') )
	          	Session::forget('resetEmail');

	        Session::flush();        
						
			$this->layout->content = View::make('blocks.user.reset-password-success', $data );
		} 
	}

	/**
	 * Get Partner User Data
	 * 
	 * @param  integer $partnerUserId
	 * @return
	 */
	protected function getPartnerUserData( $partnerUserId )
	{
		//Auto Populate the Partner's data
		$partnerUserAcct = new PartnerUserAcct(); 
		$partnerFields = array(
			'Partner_User_Id', 
			'First_Name', 
			'Last_Name',
			'City_Name',
			'Birth_Dt',
			'Street_Addr_1_Txt',
			'Street_Addr_2_Txt',
			'State_Cd',
			'Zip_Cd',
			'Zip_4_Cd',
			'Phone_Nr',
			'Email_Id',
			'Housing_Sit_Id',
			'Fico08_Val',
			'Social_Security_Nr',
			'Employment_Status_Id',
			'Annual_Gross_Income_Amt',
			'Employment_Status_Id',
			'Offered_APR_Val',
			'Offered_Loan_Amt',
			'Loan_Purpose_Id'
		);
		
		$isRow = TRUE;  

		$partnerData = $partnerUserAcct->getData( $partnerFields , ['Partner_User_Id'  => $partnerUserId ], $isRow );

		//check Name and email address, for form population
		return ( !empty( $partnerData['First_Name'] ) && !empty( $partnerData['Last_Name'] ) && !empty( $partnerData['Email_Id'] ) )? $partnerData : array();
	}


	/**
	 * Show Loan Application Step1 
	 *
	 * @deprecated
	 * @return html
	 */
	protected function showApply()
	{
 	
 		$this->layout = 'layouts.application';
		$this->setupLayout();

		$data  = array();
		$final = array();

		// pre(Session::all());

		// Validate Current Application Status if there's
		// an Existing Loan Application. 
		$redirect = validateExistingLoanAppStat();

		if( !empty($redirect) )
			return Redirect::to($redirect);

		$userId     = getUserSessionID();
		$loanAppNr  = getLoanAppNr(); 

		//Set the application step to 1 
		setActiveStepSession('1');

		$input = Input::all();
 
 		if( isset($input['IsPartner']) && $input['IsPartner'] == '1' ) {
			$userId    = $input['User_Id'];
			$loanAppNr = $input['Loan_App_Nr'];

			//set Loan Application Number
			setLoanAppNr($loanAppNr); 
			Session::put('uid', $userId); 
		}


		$data['housingSitNotification'] = '';
		
		$data['logged'] = isLoggedIn();

		if( empty($userId) && empty($loanAppNr) ){

			//Set Loan_Product_Id = 1
			if ( !isset( $input['loanProductId'] ) )
				$input['loanProductId'] = self::DEFAULT_PRODUCT_ID; 

			//Set Loan Purpose 
			if ( !isset( $input['loanPurposeId']) || $input['loanPurposeId'] == '' )
				$input['loanPurposeId'] = self::DEFAULT_PURPOSE_ID;

			//Set Loan Purpose 
			if ( !isset( $input['loanPurposeTxt']) || $input['loanPurposeTxt'] == '' )
				$input['loanPurposeTxt'] = self::DEFAULT_PURPOSE_TXT;

			//Set loan  price to $5,000  
			if ( !isset( $input['loanAmt'] ) )
				$input['loanAmt'] = self::DEFAULT_LOAN_AMT;

			//Set default product text
			if( !isset( $input['loanProductTxt'] ) ) 
				$input['loanProductTxt'] = self::DEFAULT_PRODUCT_TXT;

			//Set User Role Production QA
			if( isset( $input['isProductionQA'] ) && $input['isProductionQA'] == 1 )
				$data['userRoleId']  = 3;
			else
				$data['userRoleId']  = self::DEFAULT_USER_ROLE_ID;
	 
			// the default loan purpose  selection - value set to -1 to avoid default value
			$data['loanPurposeId']	 = 1;

			//Set Loan Session Data
			setLoanSessionData($input);
			
			$data['loan'] = $input; 
 
		}

		$data['update'] = FALSE;

		//Retrieved the Parnter Information
		if( isset($input['IsPartner']) && $input['IsPartner'] == '1' ) {

			$latestLoanApp = LoanApplication::getLatestApplication( $input['User_Id'] );
			$loanDetails   = LoanDetail::getLoanDetail( $input['User_Id'], $input['Loan_App_Nr']); 		
			$LoanPurpose   = LoanPurpose::find( $latestLoanApp->Loan_Purpose_Id);

			setLoanSessionData(array(
				'statusDesc'	  => $loanDetails->Loan_Status_Desc,
				'loanProductId'   => $loanDetails->Loan_Prod_Id,
				'loanAmt'   	  => $loanDetails->Loan_Amt,
				'loanPurposeId'	  => $LoanPurpose->Loan_Purpose_Id, 
				'loanPurposeTxt'  => $LoanPurpose->Loan_Purpose_Desc
			)); 

			$data['update'] = TRUE;

		}
		
		$loanDetail = getLoanSessionData();
		

		if( count($loanDetail) > 0 )
			$data['loanPurposeId'] = $loanDetail['loanPurposeId'];

		//Get the list of Loan Purpose
		$data['loanPurposeArr'] = getLoanPurposeList();
		$data['empStatus']      = getEmployeeStatus();
		$data['states']         = array('CA' => 'California');
		$data['months']         = getMonthsArray();
		$data['days']           = getDaysArray();
		$data['housingSit']     = getHousingSituation();
		$data['years']          = getYearArray();
		$data['uid']            = $userId;

		//Load Default Value
		$default = $this->loadDefaultInfo($loanDetail, $userId);
		$final   = array_merge($data, $default);

   		if( empty($userId) ) {
 
			//Check for the Partner Session
			if( !empty( Session::get('partner.UID') ) ){
				$partnerUserData = $this->getPartnerUserData( Session::get('partner.UID') );
				//@override the Default Values for Application
				if( count($partnerUserData) > 0 ){
					$default = $this->loanPartnerInfo($partnerUserData);
					$final = array_merge($final, $default);
				} 
			}
		} else {

			$account = $this->loadAccountInfo( $userId );
			$final = array_merge($final, $account);

			$data['update'] = TRUE;
		}
		// _pre($final);
		//housing  situation default label		
		if( $final['rentOwn'] == 4 )		
			$final['housingSitNotification'] = 'If you live with family, friends or have a situation where you pay no rent, please provide a contact who can verify your situation.';		
		else		
			$final['housingSitNotification'] = 'We validate mortgage and home ownership against credit bureau databases.'; 

		$this->layout->content = View::make('blocks.application.create-account', $final);	

	}

	/**
	 * Show Credit Check - Step 2 Loan Application Form
	 * @return
	 */
	protected function showCreditCheck()
	{	
 		
		//user from express funnel will be redirected to linkBankAcount
		$expressFunnelUser = isLTExpressFunnelUser();
		if( $expressFunnelUser )
			return Redirect::to('linkBankAccount');

 		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat();
	
		if( !empty($redirect) )
			return Redirect::to($redirect);

		$loanDetail = getLoanSessionData();  
		$userId     = getUserSessionID();

		$this->layout = 'layouts.application';
		$this->setupLayout();

		//Get Session Credit Quality value
		$data['creditQuality'] = Session::get('creditQuality');

		//Redirect the Applicant to Homepage if User or
		//Applicant Session is empty.
		if( empty( $loanDetail ) || empty( $userId ) )
			return Redirect::to('/');

		$data['housingSitNotification'] = '';
		
		//Set Loan Application to 
		setActiveStepSession('2');

		if( count($loanDetail) > 0 )
			$data['loanPurposeId'] = $loanDetail['loanPurposeId'];

		//Get the list of Loan Purpose
		$data['loanPurposeArr'] = getLoanPurposeList();
		$data['empStatus']      = getEmployeeStatus();
		$data['states']         = array('' => 'Select State') + getStateOffered();
		$data['months']         = getMonthsArray();
		$data['days']           = getDaysArray();
		$data['housingSit']     = getHousingSituation();
		$data['years']          = getYearArray();
		$data['uid']            = $userId;

		//Load Default Value
		$default = $this->loadDefaultInfo($loanDetail, $userId);
		
		$final   = array_merge($data, $default);

   		if( !empty($userId) ) {

			//Check for the Partner Session
			if( !empty( Session::get('partner.UID') ) ){
				$partnerUserData = $this->getPartnerUserData( Session::get('partner.UID') );
				//@override the Default Values for Application
				if( count($partnerUserData) > 0 ){
					$default = $this->loanPartnerInfo($partnerUserData);
					$final = array_merge($final, $default);
				} 
			}else{

				$account = $this->loadAccountInfo( $userId );
				$final = array_merge($final, $account);

				$data['update'] = TRUE;
			}
		}
		 
		// iovation BlackBox
		if(Session::has('fpBB')){
			$blackBox = array( 
				'ioBB' => Session::get('ioBB'),
				'fpBB' => Session::get('fpBB')
			);

			$final = array_merge($final, $blackBox);
		}

		//housing  situation default label		
		if( $final['rentOwn'] == 4 )		
			$final['housingSitNotification'] = 'If you live with family, friends or have a situation where you pay no rent, please provide a contact who can verify your situation.';		
		else		
			$final['housingSitNotification'] = 'We validate mortgage and home ownership against credit bureau databases.'; 

		// pre($final);

		$this->layout->content = View::make('blocks.application.credit-check', $final);
	}

	/**
	 * Load Partner Info
	 * 
	 * @param  array  $partnerUserData
	 * @return
	 */
	protected function loanPartnerInfo( $partnerUserData = array() )
	{
		$data = array(); 

		if ( count($partnerUserData) > 0 ) {

			$zipCd  = trim($partnerUserData->Zip_4_Cd);
			$zipCd4 = '';

			//Format Zip 4 code
			if ( $zipCd != "" && $zipCd != 0 )	
				$zipCd4 = "-" . $partnerUserData->Zip_4_Cd;

			//@appData
			$birth                     = explode('-', $partnerUserData->Birth_Dt);
			$data['m']                 = (isset($birth[1])) ? abs($birth[1]) : '';
			$data['d']                 = (isset($birth[2])) ? abs($birth[2]) : '';
			$data['y']                 = (isset($birth[0])) ? abs($birth[0]) : '';
			// $data['age']            = date('Y') - $data['y']; 
			$data['firstName']         = $partnerUserData->First_Name;
			// $data['middleName']     = $applData->Middle_Name;
			$data['lastName']          = $partnerUserData->Last_Name;  
			//@addressData
			$data['city']              = $partnerUserData->City_Name;
			$data['streetAddress1']    = $partnerUserData->Street_Addr_1_Txt;
			$data['streetAddress2']    = $partnerUserData->Street_Addr_2_Txt;
			$data['state']             = $partnerUserData->State_Cd;
			$data['zip']               = $partnerUserData->Zip_Cd;
			$data['zip']               .= $zipCd4;
			//@contactData
			$data['homePhoneNumber']   = $partnerUserData->Phone_Nr;
			$data['mobilePhoneNumber'] = "";
			//@financialData
			$data['rentOwn']           = $partnerUserData->Housing_Sit_Id;
			$data['rent_amount']       = $partnerUserData->Monthly_Rent_Amt;
			$data['annualGrossIncome'] = $partnerUserData->Annual_Gross_Income_Amt;
			$data['employeeStatus']    = $partnerUserData->Employment_Status_Id;
			//@userData
			$data['email']             = $partnerUserData->Email_Id;
			$data['age']               = getAgeFromDOB( $partnerUserData->Birth_Dt );  
			$data['partnerUserInfo']   = $partnerUserData;
			$data['loan']['loanAmt']   = abs($partnerUserData->Offered_Loan_Amt);
			$data['loanPurposeId']     = $partnerUserData->Loan_Purpose_Id;

		} else{
			$data['partnerUserData'] = array();
		}

		return $data;
	}

	/**
	 * Load Default Info
	 * 
	 * @param  array $loanDetail
	 * @param  integer $userId
	 * @return
	 */
	protected function loadDefaultInfo( $loanDetail, $userId )
	{
		
		$data['loan']              = $loanDetail;
		$data['firstName']         = '';
		$data['middleName']        = '';
		$data['lastName']          = '';
		$data['birthDate']         = '';
		$data['ssn']               = '';
		$data['city']              = '';
		$data['streetAddress1']    = '';
		$data['streetAddress2']    = '';
		$data['state']             = '';
		$data['zip']               = '';
		$data['homePhoneNumber']   = '';
		$data['mobilePhoneNumber'] = '';
		$data['rentOwn']           = 1;
		$data['rent_amount']       = '';
		$data['annualGrossIncome'] = '';
		$data['employeeStatus']    = '';
		$birth                     = '';
		$ssn                       = '';
		$data['ssn']               = ''; 
		$data['age']               = 0;
		$data['contactPhoneNr']    = '';
        $data['contactName'] 	   = '';
		$data['zestimate']		   = 0;
		 
		return $data;
	}

	/**
	 * Loan Existing Account Information
	 * 
	 * @param  integer $userId
	 * @return array
	 */
	protected function loadAccountInfo( $userId )
	{
		$data = array();

		$isRow    = TRUE;  

		//Get User Account Information 
		$userFields = array( 'Password_Txt', 'Email_Id', 'Alt_Email_Id', 'User_Role_Id');
		$whereParam = array('User_Id' => $userId );	
		$userData   = User::getData( $userFields, $whereParam, $isRow );
		
		//Get Applicant Information 
		$applFields = array(
			'User_Id',
			'First_Name',
			'Middle_Name',
			'Last_Name',	
			'Age_Cnt',
			'Birth_Dt',
			'Social_Security_Nr',
			'Yodlee_User_Name'
		);

		$applData = Applicant::getData( $applFields, $whereParam, $isRow);
		
		//Get Applicant Address Information 
		$applicantAddressFields = array(
			'Street_Addr_1_Txt',
			'Street_Addr_2_Txt',
			'City_Name',
			'State_Cd',
			'Zip_Cd',
			'Zip_4_Cd',
			'Validity_Start_Dt',
			'Validity_End_Dt'
		);
		$addressData   = ApplicantAddress::getData($applicantAddressFields, $whereParam, $isRow );
			
			//Get Contact Data
		$applicantContactFields = array(
			'Home_Phone_Nr',
			'Mobile_Phone_Nr',
			'Validity_Start_Dt',
			'Validity_End_Dt'
		); 

		$contactData   = ApplicantContactInfo::getData( $applicantContactFields, $whereParam, $isRow ); 

		//Get Financial Data
		$financialData = ApplicantFinancialInfo::where('User_Id', '=', $userId )->get();

		// _pre($applData->Birth_Dt);
		//@override the Default Values for Application
		if( sizeof( $applData ) > 0 ) {

			$birth              = explode('/', $applData->Birth_Dt);
			$data['m']          = date('m',strtotime($applData->Birth_Dt));
			$data['d']          = date('d',strtotime($applData->Birth_Dt));
			$data['y']          = date('Y',strtotime($applData->Birth_Dt));
			$data['age']        = date('Y') - $data['y']; 
			$data['firstName']  = $applData->First_Name;
			$data['middleName'] = $applData->Middle_Name;
			$data['lastName']   = $applData->Last_Name; 
			$data['ssn']		= $applData->Social_Security_Nr;
		}

		//@override the Default Values for Application Address
		if( sizeof( $addressData ) > 0 ) {
			$data['city']           = $addressData->City_Name;
			$data['streetAddress1'] = $addressData->Street_Addr_1_Txt;
			$data['streetAddress2'] = $addressData->Street_Addr_2_Txt;
			$data['state']          = $addressData->State_Cd;
			$data['zip']            = $addressData->Zip_Cd;

			if( !empty($addressData->Zip_4_Cd ) && $addressData->Zip_4_Cd != 0 )
				$data['zip'] = $addressData->Zip_Cd . '-' . $addressData->Zip_4_Cd;
		}

		//@override the Default Values for Application Contact
		if( sizeof( $contactData ) > 0 ) {
			$data['homePhoneNumber']   = $contactData->Home_Phone_Nr;
			$data['mobilePhoneNumber'] = $contactData->Mobile_Phone_Nr;
		}

		//@override the Default Values for Application Fincial Data
		if( sizeof( $financialData ) > 0 ) {
			$data['rentOwn'] 		 	= $financialData[0]->Housing_Sit_Id;
	        $data['rent_amount'] 	 	= $financialData[0]->Monthly_Rent_Amt;
	        $data['annualGrossIncome']  = $financialData[0]->Annual_Gross_Income_Amt;
	        $data['employeeStatus'] 	= $financialData[0]->Employment_Status_Id;
	        $data['contactPhoneNr'] 	= $financialData[0]->Contact_Phone_Nr;
	        $data['contactName'] 		= $financialData[0]->Contact_Name;
			$data['zestimate']			= $financialData[0]->Zillow_Rent_Est_Amt;
		}

		//@override the Default Values for User Account Data
		if( sizeof( $userData ) > 0 ) {
			$data['email'] = $userData->Email_Id;
			$data['userRoleId'] = $userData->User_Role_Id;
		}



		return $data;
	}

  	/**
	 * Show Loan Application Step1 
	 *
	 * 
	 * @return html
	 */
	protected function showRegister()
	{

		//get testMode for iovation
		$data['ioTestMode'] = Config::get('system.iovation.TestMode');

		//Validate Current Application Status if there's
		//an Existing Loan Application.
		$redirect = validateExistingLoanAppStat();
		
		if( !empty($redirect) )
			return Redirect::to($redirect);

		$this->layout = 'layouts.application';
		$this->setupLayout();

		//Set the application step to 1 
		setActiveStepSession('1');

		$input = Input::all();

		if( isset($input['IsPartner']) && $input['IsPartner'] == '1' ) {
			
			$userId    = $input['User_Id'];
			$loanAppNr = $input['Loan_App_Nr'];

			//set Loan Application Number
			setLoanAppNr($loanAppNr); 
			Session::put('uid', $userId); 
		}
  
		$data['logged']   = isLoggedIn();

		//Get Loan Purpose List
		$data['loanPurposeArr'] = getLoanPurposeList();

		array_unshift($data['loanPurposeArr'], 'Select One');
 
		if( !isLoggedIn() ){ 

			$data['userRoleId']  = self::DEFAULT_USER_ROLE_ID;
	  
			//Set Loan_Product_Id = 1
			if ( !isset( $input['loanProductId'] ) )
				$input['loanProductId'] = self::DEFAULT_PRODUCT_ID; 

			//Set Loan Purpose 
			if ( !isset( $input['loanPurposeId']) || $input['loanPurposeId'] == '' )
				$input['loanPurposeId'] = self::DEFAULT_PURPOSE_ID;

			//Set Credit Quality
			if ( !isset( $input['creditquality']) || $input['creditquality'] == '' )
				$input['creditquality'] = self::DEFAULT_CREDIT_TXT;

			//Set Loan Purpose 
			if ( !isset( $input['loanPurposeTxt']) || $input['loanPurposeTxt'] == '' )
				$input['loanPurposeTxt'] = self::DEFAULT_PURPOSE_TXT;

			//Set loan  price to $5,000  
			if ( !isset( $input['loanAmt'] ) )
				$input['loanAmt'] = self::DEFAULT_LOAN_AMT;

			//Set default product text
			if( !isset( $input['loanProductTxt'] ) ) 
				$input['loanProductTxt'] = self::DEFAULT_PRODUCT_TXT;

			//Set User Role Production QA
			if( isset( $input['isProductionQA'] ) && $input['isProductionQA'] == 1 ){
				$data['userRoleId']  = 3;
				$input['userRoleId'] = 3;
			}else{
				$input['userRoleId'] = 2;
			}

			if ( isset( $input['isProductionBypass'] ) ) {
				$input['isProductionBypass'] = ($input['isProductionBypass'] == 1) ? 1 : 0;
			}

			// the default loan purpose  selection - value set to -1 to avoid default value
			$data['loanPurposeId']	 = 0;
			
			//Set Loan Session Data
			setLoanSessionData($input);
			
			$data['loan'] = $input;
			$data['partnerUserInfo'] = array();
			$data['uid']  = "";
  	
  			if( Session::get('partner.UID') ){
				//Auto Populate the Partner's data
				$partnerUserAcct = new PartnerUserAcct(); 
				$partnerUserInfo = $partnerUserAcct->getData( ['Email_Id', 'Loan_Purpose_Id','Offered_Loan_Amt'] , ['Partner_User_Id'  => Session::get('partner.UID')], true );

				//Override Default Data
				if( $partnerUserInfo ){
					$data['loan']['Email_Id'] = $partnerUserInfo->Email_Id;
					$data['loan']['loanAmt']  = abs($partnerUserInfo->Offered_Loan_Amt);
					$data['loanPurposeId']    = $partnerUserInfo->Loan_Purpose_Id;
				}
			}

			//Retrieved the Parnter Information
			if( isset($input['IsPartner']) && $input['IsPartner'] == '1' ) {

				$latestLoanApp = LoanApplication::getLatestApplication( $input['User_Id'] );
				$loanDetails   = LoanDetail::getLoanDetail( $input['User_Id'], $input['Loan_App_Nr']); 		
				$LoanPurpose   = LoanPurpose::find( $latestLoanApp->Loan_Purpose_Id);
 
				setLoanSessionData(array(
					'statusDesc'	  => $loanDetails->Loan_Status_Desc,
					'loanProductId'   => $loanDetails->Loan_Prod_Id,
					'loanAmt'   	  => $loanDetails->Loan_Amt,
					'loanPurposeId'	  => $LoanPurpose->Loan_Purpose_Id, 
					'loanPurposeTxt'  => $LoanPurpose->Loan_Purpose_Desc
				)); 
  				
  				//Get User Account Information 
				$userFields = array('Email_Id');
				$whereParam = array('User_Id' => $input['User_Id'] );	
				$result    = User::getData( $userFields, $whereParam, TRUE );
		 
  				//Override Loan Amount and Loan Purpose Id
  				$data['loan']['loanAmt']       = $loanDetails->Loan_Amt;
  				$data['loan']['Email_Id'] 	   = $result->Email_Id;

				$data['loanPurposeId'] = $latestLoanApp->Loan_Purpose_Id;
				
				//Override User Id
				$data['uid']  = $input['User_Id'];
  
				//Set Applicant Registration Data
				$userData = $this->setAppltRegstrtnData($input);

				array_merge($data, $userData); 
			}

		}else{ 

			//Set Applicant Registration Data
			$userData = $this->setAppltRegstrtnData($input);
			$data = array_merge_recursive($data, $userData); 

			if ( isset( $input['creditquality']))
				$data['loanPurposeId'] = $input['loanPurposeId'];

			if ( !isset($data['loan']['creditquality']) )
				$data['loan']['creditquality'] = '';

		}

		$stateCode = getStateCodeViaIp();
		//get state config
		$data['stateConfig'] = stateConfigurations($stateCode);

		$this->layout->content = View::make('blocks.application.index', $data);	 	
	}

	/**
	 * Set Applicant Registration Data
	 * 
	 * @param array $input
	 */
	public function setAppltRegstrtnData( $input = array() )
	{ 	

		$data = array();

		$loan = getLoanSessionData(); 
 
 
		if( count($input) >  0)
			$data['loan'] = $input; 
		else 
			$data['loan'] = $loan; 

		$data['loanPurposeId']    = $data['loan']['loanPurposeId']; 
		$data['userRoleId']       = Session::get('userRoleId');
		$data['uid']              = getUserSessionID();
		 
		return $data;
	}

	/**
	 * username check for duplicate
	 * 
	 * @return
	 */
	public function usernameCheck()
	{
		$User  = new User();

		$param  = array( 'User_Name' => Input::get('username') );

		//check for unique username
		$isUserNameExist = $User->isRecordExist($param);

		//Check for Unique UserName
		if( $isUserNameExist > 0 ) 
			return Response::json(array('result' => 'failed'));
		else
			return Response::json(array('result' => 'ok'));

	}

	/**
	 * Post Register
	 * 
	 * @return
	 */
	protected function postRegister()
	{
		$isSuccess = FALSE; 

		if( Request::isMethod('post') ) {

			$data  			= Input::all();  
			$User  			= new User();   
			$NLS   			= new NLS();
			$UserPartnerSrc = new UserPartnerSrc();

			$data['email'] = (isset($data['email']))? $data['email'] : Session::get('email');

			// Set session credit quality
			Session::put('creditQuality', $data['creditQuality']);

			//Get primary loan data
			$loan = getLoanSessionData();

			if( count($data) > 0 ) { 

				if( isset($data['uid']) && $data['uid'] == "" ) {

					//Password validation
					if( !ctype_alnum($data['password'])) {
					    return Response::json(array(
								'result'        => 'failed',
								'errorMessage'  => showNotification('invalidPassword')
							));
					}
					
					//Generate Random User Name
					$data['username'] = generateUserName( $data['email'] );

					//Email Validation
					$isEmailExist = $User->isUniqueEmail( $data['email'] );
	 
					if( $isEmailExist == 1 ) {
						return Response::json(array(
								'result'        => 'failed',
								'errorMessage'  => showNotification('registrationFailEmailExist')
							));
					} else {
						 
						//Retrieve Applicant Ip Address
						$data['IP_Addr_Id'] = $this->getApplicantIp();

						//Store User Account Data
						$result = $User->saveUser($data);

						if( count($result) > 0 ) {
	 
							//Track partner data
							if( Session::has('partner') ){

								$partnerData = array(
									'User_Id' 					=> $result->User_Id,
									'Partner_Id' 				=> Session::get('partner.PID'),
									'Campaign_Id' 				=> Session::get('partner.CID'),
									'Partner_Src_Reason_Desc' 	=> 'User Registration',
									'Created_By_User_Id' 		=> $result->User_Id,
									'Create_Dt' 				=> date('Y-m-d')
								);

								$UserPartnerSrc->trackPartner($partnerData);

								//update userID associate with the trackingNr/UID from partner
								PartnerUserAcct::where( 'Partner_User_Id', '=' , Session::get('partner.UID') )
												 ->update(
												 	array( 
												 		'Ascend_User_Id' => $result->User_Id,
												 		'Created_By_User_Id' => $result->User_Id
												 	)
												 );

								//update User if cookie is available	
								if( Cookie::has('Partner_LT') ){
									$cookieId   = Cookie::get('Partner_LT');
									UserAcctCookie::where( 'Cookie_Id', '=', $cookieId )
												->update(
													array(
														'User_Id' 				=> $result->User_Id,
														'Created_By_User_Id' 	=> $result->User_Id
													 )
												);
								}
								
							}

							//Create NLS contact object, verify Username 
							//and Password and returns error if any
							$fields = array(
								'contactId'   => $result->User_Id,
								'username'    => $data['username'],
								'password'    => $data['password'],
								'email'       => $data['email'],
								'hint1'       => "", //$securityQuestions1[0]->Security_Question_Txt,
								'hint1Answer' => "", //$data['securityQuestionAnswer1'],
								'hint2'       => "", //$securityQuestions2[0]->Security_Question_Txt,
								'hint2Answer' => ""  //$data['securityQuestionAnswer2']
							);							
							
							//Create NLS Contact
							$nlsContact = $NLS->NLSCreateContact($fields);

							if( isset( $nlsContact['result'] ) && $nlsContact['result'] ){
							
								//Update user login history 
								$this->updateUserLoginHistory($result->User_Id);

								//Sign on the User.
								Session::put('logged', 1);
								Session::put('uid', $result->User_Id);
								Session::put('userName', $result->User_Name);
								Session::put('email', $data['email']);
								Session::put('userRoleId', $data['User_Role_Id']);

								//Set consent session data
								$this->setSessionConsentData();

								//Set Applicant Pre Application Phase Info
								setApplPreAppPhaseInfo(
									$result->User_Id,
									array(
										'loanProductId' => $loan['loanProductId'], 
										'loanPurposeId' => $loan['loanPurposeId'],
										'loanAmt'       => $loan['loanAmt'], 
										'loanTerm'		=> 36 
									)
								);

								//Update account cookie flag that use is registered
								//Get the cookie ID or if exist
								if( Cookie::has('Partner_LT') ){
									$cookieId = Cookie::get('Partner_LT');
									$userAcctCookie	= new UserAcctCookie();						
									$userAcctCookie->updateRegistered($cookieId, 1);
								}

								$isSuccess = TRUE;
							} 
						}
					}
				}else{

					$data['IP_Addr_Id'] = $this->getApplicantIp();

					//Generate Random User Name
					if( isset($data['email']) )
						$data['username'] = generateUserName( $data['email'] );

					//Update User Account Information
					if( isset($data['uid']) )
						$User->updateUser( $data['uid'], $data );
					
					//Sign on the User.
					Session::put('logged', 1);
					if( isset($data['uid']) )
						Session::put('uid', $data['uid']);
					if( isset($data['username']) )
						Session::put('userName', $data['username']);
					if( isset($data['email']) )
						Session::put('email', $data['email']);
					if( isset($data['User_Role_Id']) )
						Session::put('userRoleId', $data['User_Role_Id']);
 					
 					//For Partners (Dot818 and MarketLeads)
					$partner = getPartnerSession();

					//Update Leads Conversion
					if( count($partner) > 0 ) {

						//Override the user id for those partners that doesn't have
						//uid in the query string
						if( !isset($data['uid']) ) 
							$data['uid'] = 0; 

						$leadData = array(
							'Lead_Id'              => $partner['Lead_Id'], 
							'Campaign_Id'          => $partner['Campaign_Id'], 
							'Loan_Id'              => $partner['Loan_App_Nr'], 
							'Created_By_User_Id'   => $data['uid'], 
							'Create_Dt'            => date('Y-m-d'), 
							'Funding_Partner_Id'   => 0, //DB::raw("DEFAULT"),
							'Marketing_Partner_Id' => 0, //DB::raw("DEFAULT"), 
							'Loan_Funded_Dt'       => DB::raw("DEFAULT")
						);

						if( $partner['Partner_Type_Id'] == 2 ){
							$leadData['Funding_Partner_Id']   = $partner['Partner_Id']; 
						} else {
							$leadData['Marketing_Partner_Id'] = $partner['Partner_Id']; 
						}
 
						LeadConversion::saveLeadConversion($leadData);
					}

					$isSuccess = TRUE;
				}
			}


			//Save Email Age 
			if(isset($result->User_Id))
				$this->saveEmailAge( $data['email'], $result->User_Id);

 
			if( $isSuccess == TRUE ){

				//iovation blockbox pass to creditcheck
				Session::flash( 'ioBB', $data['ioBB'] );
				Session::flash( 'fpBB', $data['fpBB'] );

				return Response::json(
							array(
								'result' => 'success', 
								'successMessage' => showNotification('registrationSuccess')
							)
						);
			}

			return Response::json(
					array(
						'result'        => 'failed',
						'errorMessage'  => showNotification('registrationFailUserNameExist')
					)
				);
		} 
	}

	/**
	 * Set Consent Session Data
	 */
	protected function setSessionConsentData()
	{
		//Set Consent Session
		$ConsentVersion = new ConsentVersion();
		$consent =  $ConsentVersion->getData();

		//Assign Consent Data
		if( count($consent) > 0 ) {

			$consentArr = array();

			foreach($consent as $key => $value )
				$consentArr[] = array(
					'consentVersionNr' => $value->Consent_Version_Nr,
					'stateCd'          => $value->State_Cd,
					'consentTypeId'    => $value->Consent_Type_Id,
				);

			setConsentSessionData($consentArr);
		}
	}	

	/**
	 * Update User Login History 
	 * 
	 * @param  integer $userId
	 * @return
	 */
	protected function updateUserLoginHistory( $userId )
	{
		$loginHistory = new UserLoginHistory();
		$loginHistory->User_Id     = $userId;
		$loginHistory->IP_Addr_Id  = $_SERVER['REMOTE_ADDR'];
		$loginHistory->Domain_Name = $_SERVER['HTTP_HOST'];
		$loginHistory->Login_Dt    = date('Y-m-d');
		$loginHistory->save();
	}

	/**
	 * Validate Email Address using Mashape (Kickboxio)
	 *
	 * @see  https://www.mashape.com/kickboxio/email-verification
	 * @return JSON
	 */
	protected function validateEmailAddress()
	{

		$email =  Input::get('email');

		$result = array('result' => 'invalid');

		if( !empty($email) ) {
			
			$email = urldecode($email);

			$url = Config::get('mashape.url') . '?email=' . $email ;

			$header = array(Config::get('mashape.header'));

			$ch =  curl_init(); 
			curl_setopt($ch, CURLOPT_REFERER, $url);
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, 0);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	

			$data = curl_exec($ch);
			curl_close($ch);

			return $data;
		}

		return Response::json($result);
	}
	
	/**
	 * Verify Applicant Status
	 *    - Verifying Applicant account for HelloSign
	 *    
	 * @return
	 */
	protected function verifyApplicant()
	{
		
		$this->layout =  'layouts.application';
		$this->setupLayout();

		$token     = Input::get('token');
		$applicant = getApplicantData();
		$loanAppNr = getLoanAppNr();

		$data = array(
			'result'    => 'failed', 
			'firstName' => ''
		);

		if( isset( $applicant['token'] ) && !empty( $applicant['token']) ) {

			//redirect to homepage if the user is verified
			if ( isset( $applicant['verified'] ) && $applicant['verified'] == 1 ) {
				// return Redirect::to('/');
			}

			//check if token is not empty
			if( !empty( $token ) ) {  

				//check if the token is equal to the token of the user loggedin
				if ( $token  == $applicant['token'] ) {
					
					$data['result']    = 'success';
					$data['firstName'] = $applicant['firstName'];

					//Update Verified Account
					Applicant::where('User_Id', $applicant['userId'] )
							  ->update(array('Applicant_Verified_Flag' => 1 ));

					//Update Applicant Session
					Session::put('applicant.verified', 1 ); 
 
					$loanAppPhaseNr = 5;
					$subPhaseCode   = 'EmailVerified';

					//Set Loan Application Phase to  Final Offer - Email Verified
					setLoanAppPhase( $applicant['userId'] , $loanAppNr, $loanAppPhaseNr, $subPhaseCode  );

				}
			}
		}

 		$this->layout->content = View::make('blocks.application.email-verification-result', $data );
	}

	/**
	 * Get Applicant Ip Address
	 * 
	 * @return string $ip
	 */
	protected function getApplicantIp()
	{
		$ip = ""; 

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}

		$ips = explode(',', $ip);

		return $ips[0];
	}

	/**
	 * Save Email Age
	 *
	 *
	 * @todo  - transfer this to Model
	 * 
	 * @param  array  $data
	 * @param  integer $userId
	 */
	protected function saveEmailAge( $email = "" , $userId = "" )
	{

		//Request for Email Age
		try{

			$wrapper = new Emailage( $email . '+' .$this->getApplicantIp() );
			$wrapper->performRequest();
			$data = $wrapper->getResponseData();
			
			//Check if there's a result
			if( isset($data['query']['results']) && count($data['query']['results']) > 0 ) {

				//get only the first item
				$resultRaw  = $data['query']['results'][0];
				$query   = $data['query'];
				$result  = array();

				//set default value for null field in Result
				foreach ($resultRaw as $key => $value) {

					if( empty($value) ) 
						$result[$key] = DB::raw('DEFAULT');
					else
						$result[$key] = $value;
				}

				$emailAge = new EmailageResult();	
				$emailAge->User_Id              			= $userId;
				$emailAge->Loan_App_Nr             			= 0;
				$emailAge->Email_Id             			= Gfauth::encryptFields($query['email']);
				$emailAge->Query_Type_Cd             		= $query['queryType'];
				$emailAge->Submitted_Cnt             		= (!is_null(trim($query['count'])) )? trim($query['count']) : 0;
				$emailAge->Create_Ts             			= $query['created'];
				$emailAge->Language_Id            			= $query['lang'];
				$emailAge->Response_Cnt            		 	= (!is_null( trim( $query['responseCount']) ))? $query['responseCount'] : 0;
				$emailAge->IP_Addr_Id             			= $result['ipaddress'];
				$emailAge->E_Name             				= Gfauth::encryptFields($result['eName']);
				$emailAge->Email_Age_Txt             		= $result['emailAge'];
				$emailAge->Domain_Age_Ts             		= $result['domainAge'];
				$emailAge->First_Verification_Ts            = $result['firstVerificationDate'];
				$emailAge->Last_Verification_Ts             = $result['lastVerificationDate'];
				$emailAge->Status_Cd             			= $result['status'];
				$emailAge->Country_Cd           			= $result['country'];
				$emailAge->Fraud_Risk_Cd             		= $result['fraudRisk'];
				$emailAge->EA_Score_Val             		= (!is_null(trim($result['EAScore']) )) ? $result['EAScore'] : 0;
				$emailAge->EA_Reason_Desc             		= $result['EAReason'];
				$emailAge->EA_Status_Id             		= (!is_null(trim($result['EAStatusID']) ))? $result['EAStatusID'] : 0;
				$emailAge->EA_Reason_Id             		= (!is_null(trim( $result['EAReasonID']))) ? $result['EAReasonID'] : 0;
				$emailAge->EA_Advice_Id             		= (!is_null(trim($result['EAAdviceID']))) ? $result['EAAdviceID'] : 0;
				$emailAge->EA_Advice_Desc             		= $result['EAAdvice'];
				$emailAge->EA_Risk_Band_Id             		= (!is_null(trim($result['EARiskBandID']))) ? $result['EARiskBandID'] : 0;
				$emailAge->EA_Risk_Band_Desc             	= $result['EARiskBand'];
				$emailAge->Birth_Dt            				= Gfauth::encryptFields($result['dob']);
				$emailAge->Gender_Cd             			= $result['gender'];
				$emailAge->Location_Txt             		= $result['location'];
				$emailAge->Sm_Friends_Cnt             		= $result['smfriends'];
				$emailAge->Total_Hits_Cnt             		= $result['totalhits'];
				$emailAge->Unique_Hits_Cnt             		= (!is_null(trim($result['uniquehits']))) ? $result['uniquehits'] : 0;
				$emailAge->Image_URL_Txt             		= $result['imageurl'];
				$emailAge->Email_Exists_Cd             		= $result['emailExists'];
				$emailAge->Domain_Exists_Cd             	= $result['domainExists'];
				$emailAge->Company_Name             		= $result['company'];
				$emailAge->Title_Txt            			= $result['title'];
				$emailAge->Domain_Name             			= $result['domainname'];
				$emailAge->Domain_Company_Name             	= $result['domaincompany'];
				$emailAge->Domain_Country_Name             	= $result['domaincountryname'];
				$emailAge->Domain_Category_Cd             	= $result['domaincategory'];
				$emailAge->Domain_Corporate_Cd             	= $result['domaincorporate'];
				$emailAge->Domain_Risk_Level_Cd             = $result['domainrisklevel'];
				$emailAge->Domain_Relevant_Info_Txt         = $result['domainrelevantinfo'];
				$emailAge->Domain_Risk_Level_Id             = (!is_null(trim($result['domainrisklevelID']))) ? $result['domainrisklevelID'] : 0;
				$emailAge->Domain_Relevant_Info_Id          = (!is_null(trim($result['domainrelevantinfoID'])))? $result['domainrelevantinfoID'] : 0;
				//$emailAge->Sm_Links_Txt            		= $result['smlinks'];
				$emailAge->Sm_Links_Txt            			= 'dd';
				$emailAge->Ip_Risk_Level_Id             	= (!is_null(trim($result['ip_risklevelid']))) ? $result['ip_risklevelid'] : 0;
				$emailAge->Ip_Risk_Level_Desc             	= $result['ip_risklevel'];
				$emailAge->Ip_Risk_Reason_Id             	= (!is_null(trim($result['ip_riskreasonid']))) ? $result['ip_riskreasonid'] : 0;
				$emailAge->Ip_Risk_Reason_Desc             	= $result['ip_riskreason'];
				$emailAge->Ip_Reputation_Cd            		= $result['ip_reputation'];
				$emailAge->Ip_Anonymous_Detected_Cd         = $result['ip_anonymousdetected'];
				$emailAge->Ip_Isp_Txt             			= $result['ip_isp'];
				$emailAge->Ip_Org_Txt             			= $result['ip_org'];
				$emailAge->Ip_User_Type_Cd             		= $result['ip_userType'];
				$emailAge->Ip_Net_Speed_Cell_Cd            	= $result['ip_netSpeedCell'];
				$emailAge->Ip_Corporate_Proxy_Cd            = $result['ip_corporateProxy'];
				$emailAge->Ip_Continent_Cd             		= $result['ip_continentCode'];
				$emailAge->Ip_Country_Name             		= $result['ip_country'];
				$emailAge->Ip_Country_Cd             		= $result['ip_countryCode'];
				$emailAge->Ip_Region_Name             		= $result['ip_region'];
				$emailAge->Ip_City_Name             		= $result['ip_city'];
				$emailAge->Ip_Calling_Cd             		= (!is_null(trim($result['ip_callingcode']))) ? $result['ip_callingcode'] : 0;
				$emailAge->Ip_Metro_Cd             			= (!is_null(trim($result['ip_metroCode'])))? $result['ip_metroCode'] : 0;
				$emailAge->Ip_Latitude_Val             		= (!is_null(trim($result['ip_latitude']))) ? $result['ip_latitude'] : 0 ;
				$emailAge->Ip_Longitude_Val            		= (!is_null(trim($result['ip_longitude'])) )? $result['ip_longitude'] : 0;
				$emailAge->Ip_Map_Txt             			= $result['ip_map'];
				$emailAge->Ip_Country_Match_Cd             	= $result['ipcountrymatch'];
				$emailAge->Ip_Risk_Country_Cd             	= $result['ipriskcountry'];
				$emailAge->Ip_Distance_Km_Val             	= (!is_null(trim($result['ipdistancekm']))) ? $result['ipdistancekm'] : 0;
				$emailAge->Ip_Distance_Mil_Val             	= (!is_null(trim($result['ipdistancemil'])) )? $result['ipdistancemil'] : 0;
				$emailAge->Ip_Accuracy_Radius_Val           = (!is_null(trim($result['ipaccuracyradius'])) )? $result['ipaccuracyradius'] : 0;
				$emailAge->Ip_Timezone_Txt             		= $result['iptimezone'];
				$emailAge->Ip_Asnum_Txt             		= $result['ipasnum'];
				$emailAge->Ip_Domain_Name             		= $result['ipdomain'];
				$emailAge->Cust_Phone_In_Billing_Loc_Txt    = $result['custphoneInbillingloc'];
				$emailAge->Ship_Forward_Txt             	= $result['shipforward'];
				$emailAge->City_Postal_Match_Cd             = $result['citypostalmatch'];
				$emailAge->Ship_City_Postal_Match           = $result['shipcitypostalmatch'];
				$emailAge->Response_Status_Cd             	= $data['responseStatus']['status'];
				$emailAge->Response_Error_Cd             	= (trim(!is_null($data['responseStatus']['errorCode'])))? $data['responseStatus']['errorCode'] : 0;
				$emailAge->Response_Desc             		= $data['responseStatus']['description'];
				$emailAge->Created_By_User_Id             	= $userId;
				$emailAge->Create_Dt             			= date('Y-m-d');
				$emailAge->save();

			}
 
			// _pre($emailAge);
	        
		}catch(Exception $e){ 
           //pre($e->getMessage());
           //Store Error Log Information
			Log::warning('EmailAge'	, array(
						'User_Id' => $userId, 
						'ErrorMessage' => $e->getMessage()
			));
		}
	

		// _pre($data);			
		
		

	}


	/**
	 * Set Rent Zestimate for Applicants
	 */
	public function updateAllZestimate() {

		$recordCount = (isset($_GET['pull']) && is_numeric($_GET['pull'])) ? $_GET['pull'] : 10;	//address to pull
		Gfauth::decryptData();

		$encryptedFields = array('Street_Addr_1_Txt');
		$fields = Gfauth::encryptFieldArray($encryptedFields, array('User_Id', 'Street_Addr_1_Txt', 'City_Name', 'State_Cd', 'Zip_Cd'));

		//Get User_Ids with no Zestimate
		$noZestimates = ApplicantFinancialInfo::select(array('User_Id'))
			->where('Zillow_Rent_Est_Amt', null)
			->where('Zillow_API_Response_Txt', null)
			->whereIn('Housing_Sit_Id', array(1, 3, 4))
			->take($recordCount)->get()->toArray();

		//Get the address for Zillow pull
		$data = DB::table('Applicant_Address')
			->select($fields)
			->whereIn('User_Id', $noZestimates)
			->get();

		echo "<h2>Pulling Rent Zestimate for {$recordCount} records</h2>";

		foreach ($data as $user) {
			$zestimate = getRentZestimate($user->Street_Addr_1_Txt, sprintf('%s %s %s', $user->City_Name, $user->State_Cd, $user->Zip_Cd));

			$updateFields = array(
				'Zillow_Rent_Est_Amt' => (!empty($zestimate['zestimate'])) ? $zestimate['zestimate'] : null,
				'Zillow_API_Response_Txt' => (!empty($zestimate['xml'])) ? $zestimate['xml'] : null
			);

			//For debugging purposes
			echo '<pre>';
			print_r($user);
			echo '<strong>UPDATE FIELDS</strong><br>';
			print_r($updateFields);
			echo '</pre>';

			//Exclude if message code 7 (reached is maximum number of calls), etc
			if (!in_array($zestimate['message_code'], array(1, 2, 3, 4, 7, 505)))
				ApplicantFinancialInfo::where('User_Id', '=', $user->User_Id)->update($updateFields);
		}
	}
}
