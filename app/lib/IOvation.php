<?php

/**
 * Developer's Note
 *
 * @version 1.0
 * 
 * @notes
 * 		- IOvation Library
 */

class IOvation{

const DRA_ADMIN 	= "OLTP"; 

/**
 * Check Transaction Details URL
 * @var string
 */
protected $CTDurl;
/**
 * Subscriber ID provided by IOvation
 * @var int
 */
protected $Subscriber;
/**
 * Password for the API provided by IOvation
 * @var string
 */
protected $Password;
/**
 * test mode flag
 * @var int 1-0
 */
protected $ioTestMode;


public function __construct(){

	//get iovation config for testmode
	$ioTestMode = Config::get('system.iovation.TestMode');

	if($ioTestMode){
		$this->CTDurl 			= Config::get('system.iovation.test.CTDurl');
		$this->Subscriber 		= Config::get('system.iovation.test.Subscriber');
		$this->Password 		= Config::get('system.iovation.test.Password');
	}else{
		$this->CTDurl 			= Config::get('system.iovation.CTDurl');
		$this->Subscriber 		= Config::get('system.iovation.Subscriber');
		$this->Password 		= Config::get('system.iovation.Password');
	}

}

public function checkClient( $user_name, $blackbox, $ip, $rules, $data ){

	  try {
		   $client = new SoapClient(null, 
		                          array( 'connection_timeout' => 3,
		                                 'location' => $this->CTDurl ,
		                                 'style'    => SOAP_RPC,
		                                 'use'      => SOAP_ENCODED,
		                                 'uri'      => $this->CTDurl ."#CheckTransactionDetails") );

	     // create list of transaction properties we want to send. It is important to do a SoapVar around each property as you add
	     // it to the array to ensure you don't get extra tags added to the XML. You must also convert the arrays to objects otherwise
	     // the indexing wrappers (key and val) will be added to the XML as well)
	     // also note that SoapParam will want to add extra structure so we need to use SoapVar to add to the array
	     $txn_props = (object) array (
	                         new SoapVar( (object) array( 'name' => 'onlineId', 'value' => $data['userId'] ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'Email', 'value' => Session::get('email') ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'BillingStreet', 'value' => $data['streetAddress1'] ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'BillingCity', 'value' => $data['city'] ), SOAP_ENC_OBJECT, null, null, 'property' ),
	                         new SoapVar( (object) array( 'name' => 'BillingPostalCode', 'value' => $data['zip'] ), SOAP_ENC_OBJECT, null, null, 'property' )
	                     );
	                     
		    //Snare Vars               
	      $retArr = $client->CheckTransactionDetails( 
	                               new SoapParam( $user_name, 'accountcode' ),
	                               new SoapParam( $ip, 'enduserip' ),
	                               new SoapParam( $blackbox,  'beginblackbox' ),
	                               new SoapParam( $this->Subscriber , 'subscriberid' ),
	                               new SoapParam( self::DRA_ADMIN, 'subscriberaccount' ),
	                               new SoapParam( $this->Password, 'subscriberpasscode' ) ,
	                               new SoapParam( $rules, 'type' ),
	                               new SoapParam( $txn_props, 'txn_properties' )     
	                             );
	   } catch ( SoapFault $e ) {
	          return array( "status"=> -1, "err"=>$e );
	   }

	   unset($client);
	   return $retArr;
	}

}