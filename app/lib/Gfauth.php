<?php

class Gfauth {

	/**
	 * Encryption Database Password
	 * 
	 * @var string
	 */
	protected static $encryptedPassword = '5k_Us3r_4cct_P455w0rd';
  	
  	/**
  	 * DecryptData
  	 * 
  	 * @return
  	 */
	public static function decryptData()
	{
		return DB::statement("OPEN SYMMETRIC KEY sk_User_Acct DECRYPTION BY ASYMMETRIC KEY ak_User_Acct WITH PASSWORD = '". self::$encryptedPassword ."';");
	}

	/**
	 * Format Encrypt Fields
	 * 
	 * @param  array  $fields
	 * @return [type]
	 */
	public static function encryptFields( $field = '' )
	{
		return DB::raw("ENCRYPTBYKEY(KEY_GUID('sk_User_Acct'), '".$field."')");
	}

	/**
	 * Decrypt Single Field
	 * 
	 * @param  string $field
	 * @return
	 */
	public static function decryptField( $field = '')
	{
		return DB::raw("CONVERT(VARCHAR(300), DECRYPTBYKEY([$field])) AS [$field]");
	}

	/**
	 * Decrypt Single Field
	 * 
	 * @param  string $field
	 * @return
	 */
	public static function decryptLongField( $field = '', $alias = '')
	{
		if( $alias == "" )
			$alias = $field; 
		
		return DB::raw("CONVERT(VARBINARY(8000), DECRYPTBYKEY([$field])) AS [$alias]");
	}


	/**
	 * Format Encrypted Fields
	 * 
	 * @param  array  $fields
	 * @param  array  $encryptedFields
	 * @return string
	 */
	public static function formatEncryptedFields( $encryptedFields = array(), $fields =  array() )
	{
		if( count($fields) > 0 ) {
			foreach($fields as $key => $field ){
				if( in_array($field, $encryptedFields ) )
	  				$fields[$key] = DB::raw("CONVERT(VARCHAR(300), DECRYPTBYKEY([$field])) AS [$field]");
	  		}

  			return implode(',', $fields );
  		}
	}

	/**
	 * Format Encrypted Fields
	 * 
	 * @param  array  $fields
	 * @param  array  $encryptedFields
	 * @return string
	 */
	public static function encryptFieldArray( $encryptedFields = array(), $fields =  array() )
	{
		if( count($fields) > 0 ) {
			foreach($fields as $key => $field ){
				if( in_array($field, $encryptedFields ) )
	  				$fields[$key] = DB::raw("CONVERT(VARCHAR(300), DECRYPTBYKEY($field)) AS $field");
	  		}
  		}
  		
  		return $fields;
	}

	/**
	 * Format Encrypted Fields with for large datatype lenght; using varbinary 8000 max length
	 * 
	 * @param  array  $fields
	 * @param  array  $encryptedFields
	 * @return string
	 */
	public static function encryptLargeFieldArray( $encryptedFields = array(), $fields =  array() )
	{
		if( count($fields) > 0 ) {
			foreach($fields as $key => $field ){
				if( in_array($field, $encryptedFields ) )
	  				$fields[$key] = DB::raw("CONVERT(VARBINARY(8000), DECRYPTBYKEY($field)) AS $field");
	  		}
  		}
  		
  		return $fields;
	}


}
