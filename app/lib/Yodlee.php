<?php

/**
 * Developer's Note
 *
 * @version v1.0.4.21
 * 
 * @notes
 * 		- Yodlee Library
 */

class Yodlee{

	/*
	|--------------------------------------------------------------------------
	| Yodlee Library
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'YodleeController@index');
	|  
	|   @todo - we need to make this package
	|
	*/
	
	const URL_COBRAND_SESSION_TOKEN    = "/authenticate/coblogin";
	const URL_USER_SESSION_TOKEN       = "/authenticate/login";
	const URL_SEARCH_SITES             = "/jsonsdk/SiteTraversal/searchSite";
	const URL_GET_ITEM_SUMMARIES       = "/jsonsdk/DataService/getItemSummaries";
	const URI_GET_ITEM_SUMMARY         = "/jsonsdk/DataService/getItemSummariesForSite";
	const URI_GET_ITEM_SUMMARY_WO_DATA = "/jsonsdk/DataService/getItemSummariesWithoutItemData";
	const URL_GET_SITE_LOGIN_FORM      = "/jsonsdk/SiteAccountManagement/getSiteLoginForm"; 
	const URL_ADD_SITE_ACCOUNT         = "/jsonsdk/SiteAccountManagement/addSiteAccount1";	
	const URI_GET_SITE_ACCOUNTS        = '/jsonsdk/SiteAccountManagement/getSiteAccounts';
	const URI_REMOVE_ACCOUNT           = '/jsonsdk/SiteAccountManagement/removeSiteAccount';
	const URL_REGISTER_CUSTOMER        = '/jsonsdk/UserRegistration/register3';
	const URL_GET_USER_TRANSACTION     = '/jsonsdk/TransactionSearchService/getUserTransactions';
	const URL_SEARCH_REQUEST           = '/jsonsdk/TransactionSearchService/executeUserSearchRequest';
	const URL_VALIDATE_USERNAME        = '/jsonsdk/Login/validateUser';
	const URL_REFRESH_SITE_INFO        = '/jsonsdk/Refresh/getSiteRefreshInfo';
	const URL_MFA_RESPONSE             = '/jsonsdk/Refresh/getMFAResponseForSite';
	const URL_MFA_PUT_REQUEST          = '/jsonsdk/Refresh/putMFARequestForSite';
	const URI_GET_REFRESH_USER_ITEMS   = '/jsonsdk/Refresh/getRefreshedUserItems';
	const URI_GET_ITEM_SUMMARY1        = '/jsonsdk/DataService/getItemSummaryForItem1';
	const URI_GET_OAUTH_ACCESS_TOKEN   = '/jsonsdk/OAuthAccessTokenManagementService/getOAuthAccessToken';
	
	const BRIDGET_APP_ID               = '10003200';
	const FAST_LINK_APP_KEY            = 'a458bdf184d34c0cab7ef7ffbb5f016b'; 
	const FAST_LINK_APP_TOKEN          = '1ece74e1ca9e4befbb1b64daba7c4a24';
	// const FAST_LINK_URL                = 'https://fastlink.yodlee.com/appscenter/fastlinksb/linkAccount.fastlinksb.action';
	
	const FAST_LINK_URL           	   = 'https://fl.yodlee.com/appscenter/private-raterewards/linkAccount.pfmmasterfl.action';

	const FAST_LINK_URL_AUTH 		   = 'authenticator/token';

	/**
	 * Service Base URL
	 * 
	 * @var string
	 */
	protected $serviceBaseUrl;

	/**
	 * Cob Login Username
	 * @var string
	 */
	protected $cobLoginUsername;

	/**
	 * Cob Login Password
	 * 
	 * @var string
	 */
	protected $cobLoginPassword;
 	

	/**
	 * Refresh Status
	 */
	const REFRESH_NEVER_INITIATED                       = 0;
	const REFRESH_TRIGGERED                             = 1;
	const LOGIN_SUCCESS                                 = 2;
	const LOGIN_FAILURE                                 = 3;
	const PARTIAL_COMPLETE                              = 4;
	const REFRESH_COMPLETED                             = 5;
	const REFRESH_TIMED_OUT                             = 6;
	const REFRESH_CANCELLED                             = 7;
	const REFRESH_COMPLETED_WITH_UNCERTAIN_ACCOUNT      = 8;
	const REFRESH_ALREADY_IN_PROGRESS                   = 9;
	const SITE_CANNOT_BE_REFRESHED                      = 10;
	const REFRESHED_TOO_RECENTLY                        = 11;
	const REFRESH_COMPLETED_ACCOUNTS_ALREADY_AGGREGATED = 12;
		
	/**
	 * MFA Type
	 */
	const MFA_TYPE_MFA    = 1;
	const MFA_TYPE_NORMAL = 2;

	/**
	 * Multi Factor Authentication Refresh Type
	 */
	const MFA_TOKEN    = "com.yodlee.core.mfarefresh.MFATokenResponse";
	const MFA_QUESTION = "com.yodlee.core.mfarefresh.MFAQuesAnsResponse";
	const MFA_IMAGE    = "com.yodlee.core.mfarefresh.MFAImageResponse";

	/**
	 * Multi Factor Authentication
	 */
	const MFA_FIELD_NONE              = "none";
	const MFA_FIELD_TOKEN_ID          = "TOKEN_ID";
	const MFA_FIELD_IMAGE             = "IMAGE";
	const MFA_FIELD_MASTER_COOKIE     = "MASTER_COOKIE";
	const MFA_FIELD_SECURITY_QUESTION = "SECURITY_QUESTION";
	const MFA_FIELD_MULTI_LEVEL       = "MULTI_LEVEL";


	/**
	 * Account Type
	 */
	const CONT_TYPE_BANK            = "bank";
	const CONT_TYPE_BILL            = "bills";
	const CONT_TYPE_BILL_PAYMENT    = "bill_payment";
	const CONT_TYPE_CABLE_SATELLITE = "cable_satellite";
	const CONT_TYPE_CREDIT_CARD     = "credits";
	const CONT_TYPE_INSURANCE       = "insurance";
	const CONT_TYPE_INVESTMENT      = "stocks";
	const CONT_TYPE_ISP             = "isp";
	const CONT_TYPE_LOAN            = "loans";
	const CONT_TYPE_MORTGAGE        = "mortgage";
	const CONT_TYPE_ORDER           = "orders";
	const CONT_TYPE_PREPAY          = "prepay";
	const CONT_TYPE_REALESTATE      = "RealEstate";
	const CONT_TYPE_MILE            = "miles";
	const CONT_TYPE_TELEPHONE       = "telephone";

	/**
	 * Errors and Statuses
	 */
	const ERROR_EMPTY                                              = -1;
	const ERROR_ALL_FINE                                           = 0;
	const ERROR_REFRESH_NEVER_DONE                                 = 801;
	const ERROR_REFRESH_NEVER_DONE_AFTER_CREDENTIALS_UPDATE        = 802;
	const ERROR_STATUS_SITE_UNAVAILABLE                            = 409;
	const ERROR_STATUS_SITE_OUT_OF_BUSINESS                        = 411;
	const ERROR_STATUS_SITE_APPLICATION_ERROR                      = 412;
	const ERROR_STATUS_SITE_TERMINATED_SESSION                     = 415;
	const ERROR_STATUS_SITE_SESSION_ALREADY_ESTABLISHED            = 416;
	const ERROR_STATUS_HTTP_DNS_ERROR_EXCEPTION                    = 418;
	const ERROR_STATUS_ACCT_INFO_UNAVAILABLE                       = 423;
	const ERROR_STATUS_SITE_DOWN_FOR_MAINTENANCE                   = 424;
	const ERROR_STATUS_CERTIFICATE_ERROR                           = 425;
	const ERROR_STATUS_SITE_BLOCKING                               = 426;
	const ERROR_STATUS_SITE_CURRENTLY_NOT_SUPPORTED                = 505;
	const ERROR_STATUS_PROPERTY_RECORD_NOT_FOUND                   = 510;
	const ERROR_STATUS_HOME_VALUE_NOT_FOUND                        = 511;
	const ERROR_STATUS_LOGIN_FAILED                                = 402;
	const ERROR_STATUS_USER_ABORTED_REQUEST                        = 405;
	const ERROR_STATUS_PASSWORD_EXPIRED                            = 406;
	const ERROR_STATUS_ACCOUNT_LOCKED                              = 407;
	const ERROR_STATUS_NO_ACCT_FOUND                               = 414;
	const ERROR_STATUS_DATA_MODEL_NO_SUPPORT                       = 417;
	const ERROR_STATUS_SITE_MERGED_ERROR                           = 420;
	const ERROR_STATUS_UNSUPPORTED_LANGUAGE_ERROR                  = 421;
	const ERROR_STATUS_ACCOUNT_CANCELED                            = 422;
	const ERROR_STATUS_SPLASH_PAGE_EXCEPTION                       = 427;
	const ERROR_STATUS_TERMS_AND_CONDITIONS_EXCEPTION              = 428;
	const ERROR_STATUS_UPDATE_INFORMATION_EXCEPTION                = 429;
	const ERROR_STATUS_SITE_NOT_SUPPORTED                          = 430;
	const ERROR_STATUS_REGISTRATION_PARTIAL_SUCCESS                = 433;
	const ERROR_STATUS_REGISTRATION_FAILED_ERROR                   = 434;
	const ERROR_STATUS_REGISTRATION_INVALID_DATA                   = 435;
	const ERROR_REGISTRATION_ACCOUNT_ALREADY_REGISTERED            = 436;
	const ERROR_NEW_LOGIN_INFO_REQUIRED_FOR_SITE                   = 506;
	const ERROR_NEW_MFA_INFO_REQUIRED_FOR_AGENTS                   = 518;
	const ERROR_MFA_INFO_NOT_PROVIDED_TO_YODLEE_BY_USER_FOR_AGENTS = 519;
	const ERROR_MFA_INFO_MISMATCH_FOR_AGENTS                       = 520;
	const ERROR_ENROLL_IN_MFA_AT_SITE                              = 521;
	const ERROR_MFA_INFO_NOT_PROVIDED_IN_REAL_TIME_BY_USER_VIA_APP = 522;
	const ERROR_INVALID_MFA_INFO_IN_REAL_TIME_BY_USER_VIA_APP      = 523;
	const ERROR_USER_PROVIDED_REAL_TIME_MFA_DATA_EXPIRED           = 524;
	const ERROR_INVALID_MFA_INFO_OR_CREDENTIALS                    = 526;
	const ERROR_NO_CONNECTION                                      = 401;
	const ERROR_INTERNAL_ERROR                                     = 403;
	const ERROR_LOST_REQUEST                                       = 404;
	const ERROR_DATA_EXPECTED                                      = 408;
	const ERROR_REQUIRED_FIELD_UNAVAILABLE                         = 413;
	const ERROR_LOGIN_NOT_COMPLETED                                = 419;
	const ERROR_BETA_SITE_WORK_IN_PROGRESS                         = 507;
	const ERROR_INSTANT_REQUEST_TIMEDOUT                           = 508;
	const ERROR_TOKEN_ID_INVALID                                   = 509;
	const ERROR_GENERAL_EXCEPTION_WHILE_GATHERING_MFA_DATA         = 517;
	const ERROR_MFA_INFO_NOT_PROVIDED_IN_REAL_TIME_BY_GATHERER     = 525;

	/**
	 * Debug Mode
	 * @var integer
	 */
	private $isDebug = 1;
 	
	/**
	 * Member Site Account Id 
	 * @var int
	 */
	private $memSiteAccId;
 
	/**
	 * Top Banks Limit 
	 * 
	 * @var integer
	 */
	protected $topBanksLimit = 0;


	/**
	 * Days Since Last NSF Count
	 * @var string
	 */
	protected $daysSinceLastNSFCnt = 0;
	
	/**
	 * Total Not suffecient Fund count
	 * @var integer
	 */
	protected $totalNSFCnt = 0;
	
	/**
	 * Average Monthly Deposited Amount
	 * @var integer
	 */
	protected $avgMonthlyDepositAmt = 0;
	
	/**
	 * Average Balance on Payment Date Amount
	 * @var integer
	 */
	protected $avgBalOnPmtDtAmt = 0;
	
	/**
	 * Low Balance Event Count
	 * @var integer
	 */
	protected $lowBalEventCnt = 0;
	
	/**
	 * Low Balance Days Count
	 * @var integer
	 */
	protected $lowBalDayCnt = 0;
 
	const REFRESH_CNT_LOOP = 2;

 	/**
 	 * Constructor
 	 */
	public function __construct()
	{
	 	
		//Set Yodlee Base URL, UserName, Password and Debug Mode
		$this->isDebug    = Config::get('system.Yodlee.TestMode'); 
		$serviceBaseUrl   = Config::get('system.Yodlee.ProdRESTURL');
		$cobLoginUsername = Config::get('system.Yodlee.ProdUserName');
		$cobLoginPassword = Config::get('system.Yodlee.ProdUserPassword');

		//Debug Mode override
		if( $this->isDebug ==  1 ) {
			//@override the service base url
			$serviceBaseUrl   = Config::get('system.Yodlee.TestRESTURL');
			$cobLoginUsername = Config::get('system.Yodlee.TestUserName');
			$cobLoginPassword = Config::get('system.Yodlee.TestUserPassword');
		}
 
		//set user name, password and rest url
		$this->setCobLoginUsername( $cobLoginUsername );
		$this->setCobLoginPassword( $cobLoginPassword );
		$this->setServiceBaseUrl( $serviceBaseUrl );
 
		//check if the cobrand credentials
		$this->authorize();

		$debugMode = Config::get('system.Ascend.DebugMode');
    	View::share('debugMode', $debugMode);		

	}

	/**
	 * Set Cobraband Login UserName
	 * 
	 * @param string $userName
	 */
	public function setCobLoginUsername( $userName )
	{
		$this->cobLoginUsername = $userName;
	}

	/**
	 * Get CobLogin UserName
	 * @return object
	 */
	public function getCobLoginUsername()
	{
		return $this->cobLoginUsername;
	}

	/**
	 * Set CobLogin Password
	 * 
	 * @param string $password
	 */
	public function setCobLoginPassword( $password )
	{
		$this->cobLoginPassword = $password;
	}

	/**
	 * Get Coblogin Password
	 * 
	 * @return string
	 */
	public function getCobLoginPassword()
	{
		return $this->cobLoginPassword;
	}


	/**
	 * Set Service Base Url 
	 * 
	 * @param string $baseUrl
	 */
	public function setServiceBaseUrl( $baseUrl )
	{
		$this->serviceBaseUrl = $baseUrl;
	}

	/**
	 * Get Service Base URL 
	 * 
	 * @return string
	 */
	public function getServiceBaseUrl()
	{
		return $this->serviceBaseUrl;
	}

	/**
	 * Set User Id
	 * 
	 * @param 		string $userId
	 * @deprecated  v1.0.4.21
	 */
	public function setUserId($userId)
	{
		Session::put('uid', $userId);
	}

	/**
	 * Get User Id
	 * 
	 * @return integer
	 * @deprecated  v1.0.4.21
	 */
	public function getUserId()
	{
		return Session::get('uid');
	}

	/**
	 * Get Loan Application Number
	 * 
	 * @return integer
	 */
	public function getLoanAppNr()
	{
		return Session::get('loanAppNr');
	}

	/**
	 * Set Loan Application Number
	 * 
	 * @param integer $loanAppNr
	 */
	public function setLoanAppNr($loanAppNr)
	{
		Session::put('loanAppNr', $loanAppNr );
	}

	/**
	 * Set Site Id to Session
	 * 
	 * @param int $siteId
	 */
	public function setSiteIdToSession($siteId){
		Session::put('siteId', $siteId);
	}

	/**
	 * Get Site Id From Session 
	 * 
	 * @return
	 */
	public function getSiteIdFromSession()
	{
		return Session::get('siteId');
	}
	
	/**
	 * Set Top Banks Limit
	 * 
	 * @param integer $limit
	 */
	public function setTopBanksLimit($limit)
	{
		$this->topBanksLimit = $limit;
	}

	/**
	 * Get Top Banks Limit
	 * 
	 * @return int
	 */
	public function getTopBanksLimit()
	{
		return $this->topBanksLimit;
	}

	/**
	 * Set User Session Token 
	 * 
	 * @param string $token
	 */
	public function setUserSessionToken($token)
	{
		Session::put('userSessionToken', $token );
	}

	/**
	 * Get User Session Token 
	 * 
	 * @return
	 */
	public function getUserSessionToken()
	{
		return Session::get('userSessionToken');
	}

	/**
	 * Set Session Token 
	 * 
	 * @param string $token
	 */
	public function setCobSessionToken($token)
	{
		Session::put('cobSessionToken', $token );
	}

	/**
	 * Get Session Token 
	 *
	 * @return
	 */
	public function getCobSessionToken()
	{
		return Session::get('cobSessionToken');
	}
 
	/**
	 * Get Errors
	 * 
	 * @param object $errors
	 * @return array
	 */
	private function _getErrors($errors)
	{
		$Errors = array();
		
		foreach ($errors->Error as $key => $value) {
			$Errors[]= $value->errorDetail;
		}

		return $Errors; 
	}

	/**
	 * Post Method
	 * 
	 * @param string $url
	 * @param array  $parameters
	 */
	protected function Post( $url, $parameters = array() ) 
	{

		$return_values = array();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);

		if(count($parameters)>0){
			curl_setopt($ch, CURLOPT_POST, count($parameters));
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
		}

		$response = curl_exec($ch);
		if (curl_errno($ch)) {
               $return_values['Error'] = "Failed to reach $url.";
        } else {
			if ($response) {
				if(gettype($response) == "string"){
					$result = json_decode($response);
					if($result){
						$exitsError = array_key_exists("Error", $result);
						if($exitsError){
							$return_values["Body"] = self::_getErrors($result);
						}else{
							$return_values["Body"] = $result;
						}
					}else{
						// $result = simplexml_load_string($response);
						$return_values["Body"] = $response;
					}
	        	}else{
	        		$result = json_decode($response);
	        		if($result === null) {
						$return_values['Body'] = "The request does not return any value.";
					} else {
						$return_values["Body"] = $result;
					}
	        	}
			}else{
				$return_values['Error'] = "Failed to reach $url.";
			}
        }
		curl_close($ch);
		return $return_values;
	}

	public function getRsessionToken()
	{
		$config = array(
			"url" => $this->serviceBaseUrl.self::FAST_LINK_URL_AUTH,
			"parameters"       => array(
				"cobSessionToken" => $this->getCobsessionToken(),
				"rsession"        => $this->getUserSessionToken(), 
				"finAppId"        => '10003600'
			)
		); 

		// _pre($config);
 
		$result = $this->Post($config['url'], $config['parameters']);

		return $result;
	}

	/**
	 * Get OAuth Access Token
	 * 
	 * @param  string $bridgetAppId
	 * @return
	 */
	public function getOAuthAccessToken( $bridgetAppId )
	{
		$config = array(
			"url" => $this->serviceBaseUrl.self::URI_GET_OAUTH_ACCESS_TOKEN,
			"parameters"       => array(
				"cobSessionToken"  => $this->getCobsessionToken(),
				"userSessionToken" => $this->getUserSessionToken(), 
				"bridgetAppId"	   => $bridgetAppId
			)
		); 

		// _pre($config);
 
		$result = $this->Post($config['url'], $config['parameters']);

		return $result;

	}
 
	/**
	 * Cobra Login 
	 * 
	 * @param  string $login
	 * @param  string $password
	 * @param  string $cobSessionToken
	 * @return
	 */
	protected function login($login = '', $password = '', $cobSessionToken = '')
	{

		$response = array();

		$config = array(
			"url_cobrand_login" => $this->serviceBaseUrl.self::URL_USER_SESSION_TOKEN,
				"cobrand_login" => array(
				"login"           => $login,
				"password"        => $password,
				"cobSessionToken" => $cobSessionToken
			)
		);

		$responseToRequest = $this->Post($config["url_cobrand_login"], $config["cobrand_login"]);
		$isErrorLocalExist = array_key_exists("Error", $responseToRequest);

		if( $isErrorLocalExist ) {
			$response = array(
				'isValid' 	  => false, 
				'errorServer' => $responseToRequest['Error']
			);
		} else {
			$response = array(
				"isValid"      => true,
				"Body"         => $responseToRequest["Body"]
			);
		}

		return $response;
	}

	/**
	 * Get Cobra And Session Token
	 * 
	 * @param  string $login
	 * @param  string $password
	 * @return
	 */
	protected function getCobraAndSessionToken($login, $password)
	{

		$config = array(
			"url_cobrand_login" => $this->serviceBaseUrl.self::URL_COBRAND_SESSION_TOKEN,
				"cobrand_login" => array(
				"cobrandLogin"    => $login,
				"cobrandPassword" => $password
			)
		);

		$responseToRequest = $this->Post($config["url_cobrand_login"], $config["cobrand_login"]);

		$isErrorLocalExist = array_key_exists("Error", $responseToRequest);

		if($isErrorLocalExist) {
			$response = array(
				'isValid' 	  => false, 
				'errorServer' => $responseToRequest['Error']
			);
		}else{
			$response = array(
				"isValid"      => true,
				"Body"         => $responseToRequest["Body"]
			);
		}
		
		return $response;

	}

	/**
	 * Add Site Account 
	 * 
	 * @param mixed $componentList
	 * @param array  $param
	 */
	protected function addSiteAccount( $componentList, $param = array() )
	{


		$result = array();
 
		if( count($componentList) > 0 && !empty($param['siteId']) ) {

			try{

				$config['url'] = $this->serviceBaseUrl.self::URL_ADD_SITE_ACCOUNT;

				$creds = array();

				//remove current id
				Session::forget('siteId');

				//set site Id on session 
				$this->setSiteIdToSession($param['siteId']);

				if( $this->getCobsessionToken() == "") 
					$this->authorize();

				$creds['cobSessionToken']  = $this->getCobsessionToken();
				$creds['userSessionToken'] = $this->getUserSessionToken(); 
				$creds['siteId']		   = $param['siteId'];
				$creds['credentialFields.enclosedType'] = 'com.yodlee.common.FieldInfoSingle';
		 
				foreach ($componentList as $key => $componentList) {

						$value = isset($param[$componentList->name]) ? $param[$componentList->name] : "";

						$creds['credentialFields['.$key .'].displayName']        = $componentList->displayName;
						$creds['credentialFields['.$key .'].fieldType.typeName'] = $componentList->fieldType->typeName;
						$creds['credentialFields['.$key .'].helpText']           = $componentList->helpText;
						$creds['credentialFields['.$key .'].maxlength']          = $componentList->maxlength;
						$creds['credentialFields['.$key .'].name']               = $componentList->name;
						$creds['credentialFields['.$key .'].size']               = $componentList->size;
						$creds['credentialFields['.$key .'].value']              = $value;//
						$creds['credentialFields['.$key .'].valueIdentifier']    = $componentList->valueIdentifier;
						$creds['credentialFields['.$key .'].valueMask']          = $componentList->valueMask;
						$creds['credentialFields['.$key .'].isEditable']         = $componentList->isEditable; 
	 
				}	

				$config['parameters'] = $creds;

				$result = $this->Post($config['url'], $config['parameters']);

			}catch(Exception $e) {

				writeLogEvent('Yodlee - addSiteAccount', 
					array( 
						'User_Id'     => $this->getUserId(),
						'Loan_App_Nr' => $this->getLoanAppNr(),
						'Message'     => $e->getMessage() 
					) 
				);
			} 
		}

		return $result;
	}

	
	/**
	 * Get Site Accounts 
	 * 
	 * @param  array  $memSiteAccId
	 * @return [type]
	 */
	protected function getSiteAccounts($memSiteAccId = array())
	{
		if( $this->getCobsessionToken() == "" && $this->getUserSessionToken() == "" )
			$this->authorize();

		$config = array(
				"url" => $this->serviceBaseUrl.self::URI_GET_SITE_ACCOUNTS,
				"parameters"       => array(
					"cobSessionToken"  => $this->getCobsessionToken(),
					"userSessionToken" => $this->getUserSessionToken()
				)
		); 

		$result = $this->Post($config['url'], $config['parameters']);

		return $result;
	}

	/**
	 * Get Site Login Form 
	 * 
	 * @param  string $cobSessionToken
	 * @param  string $userSessionToken
	 * @param  integer $siteId
	 * @return
	 */
	public function getSiteLoginForm($siteId)
	{
		$this->saveYodleeLog( $siteId );

		if($this->getCobsessionToken() == "" )
			$this->authorize();

		$config = array(
				"url" => $this->serviceBaseUrl.self::URL_GET_SITE_LOGIN_FORM,
				"parameters"       => array(
					"cobSessionToken"  => $this->getCobsessionToken(),
					"siteId"           => $siteId
				)
		); 

		$result = $this->Post($config['url'], $config['parameters']);

		return $result;
	}

	/**
	 * Get User Transactions
	 * 
	 * @return
	 */
	public function getUserTransactions( $siteAccountId, $months = 3 )
	{	
		try{

			if( $this->getCobsessionToken() == "") 
				$this->authorize();

			//execute user search transaction 
			//to get search  identifier
			$searchIdentifier = $this->executeUserSearchRequest( $siteAccountId, $months );

			$searchIdentifierString = "";
	 
			if(isset($searchIdentifier['Body']->searchIdentifier->identifier))
				$searchIdentifierString = $searchIdentifier['Body']->searchIdentifier->identifier;	

 
			if( $searchIdentifier != "" ) {

				$parameters = array(
					'cobSessionToken'                                  => $this->getCobsessionToken(),
					'userSessionToken'                                 => $this->getUserSessionToken(), 
					'searchFetchRequest.searchIdentifier.identifier'   => $searchIdentifierString,
					'searchFetchRequest.searchResultRange.startNumber' => '1',
					'searchFetchRequest.searchResultRange.endNumber'   => '1000'
				);

				$config = array(
					'url' 		 => $this->serviceBaseUrl.self::URL_GET_USER_TRANSACTION,
					'parameters' => $parameters
				);

				$result = $this->Post($config['url'], $config['parameters']);
 
				return $result;

			} else {

				writeLogEvent('Yodlee Get User Transactions', 
					array( 
						'User_Id'     => $this->getUserId(),
						'Loan_App_Nr' => $this->getLoanAppNr(),
						'Message'     => 'Search identifier is empty'
					) 
				);

				return array('error' => 'Search identifier is empty!');
			}

		} catch(Exception $e){

			writeLogEvent('Yodlee Get User Transactions', 
					array( 
						'User_Id'     => $this->getUserId(),
						'Loan_App_Nr' => $this->getLoanAppNr(),
						'Message'     => $e->getMessage() 
					) 
				);
		} 
	}

	/**
	 * Execute User Search Request
	 * 
	 * @param  int $siteAccountID
	 * @return 
	 */
	public function executeUserSearchRequest( $siteAccountID, $months = 3 )
	{
		
		$response = array();

		$fromDate = strtotime( date('Y-m-d') . "-$months months");

  		if( !empty($this->getCobSessionToken() ) && !empty($this->getUserSessionToken()) ) {

			$config = array(
					'url' => $this->serviceBaseUrl.self::URL_SEARCH_REQUEST,
					'parameters' => array(
						'cobSessionToken'												 => $this->getCobSessionToken(),
						'userSessionToken'												 => $this->getUserSessionToken(),
						'transactionSearchRequest.containerType'                         => 'bank',
						'transactionSearchRequest.higherFetchLimit'                      => '500',
						'transactionSearchRequest.lowerFetchLimit'                       => '1',
						'transactionSearchRequest.resultRange.endNumber'                 => '100',
						'transactionSearchRequest.resultRange.startNumber'               => '1',
						// 'transactionSearchRequest.searchClients.clientId'                => '1',
						// 'transactionSearchRequest.searchClients.clientName'              => 'DataService',
						// 'transactionSearchRequest.userInput'                             => 'rent',
						'transactionSearchRequest.ignoreUserInput'                       => 'true',
						'transactionSearchRequest.searchFilter.currencyCode'             => 'USD',
						'transactionSearchRequest.searchFilter.postDateRange.fromDate'   => (string) date('m-d-Y', $fromDate),
						'transactionSearchRequest.searchFilter.postDateRange.toDate'     => (string) date('m-d-Y'),
						// 'transactionSearchRequest.searchFilter.transactionSplitType'     => 'ALL_TRANSACTION',
						// 'transactionSearchRequest.searchFilter.itemAccountId.identifier' => $siteAccountID,  
					) 
				);
			 
			$response = $this->Post($config['url'], $config['parameters']);
		}

		return $response;
	}

	/**
	 * Remove Site Account 
	 * 
	 * @param  array $memSiteAccId
	 * @return
	 */
 	protected function removeSiteAccount($memSiteAccId)
 	{

 		if( empty( $this->getCobsessionToken() ) && empty($this->getUserSessionToken()))
 			$this->authorize();

 		$memSiteAccIdArr = array($memSiteAccId);

 		$siteAccount =  $this->getSiteAccounts($memSiteAccIdArr);

 		if( !isset($siteAccount['Body']->key) && isset($siteAccount['Body']) ) {

	 		foreach ($siteAccount['Body'] as $key => $value) {

				$config = array(
						"url"        => $this->serviceBaseUrl.self::URI_REMOVE_ACCOUNT,
						"parameters" =>  array(
							'cobSessionToken'   => $this->getCobsessionToken(),
							'userSessionToken'	=> $this->getUserSessionToken(),
							'memSiteAccId'		=> $value->siteAccountId
						)

					);

				$result  = $this->Post($config["url"], $config["parameters"]);
	 		}
 		}

 	  
		$siteAccount = $this->getSiteAccounts($memSiteAccIdArr);

		return$siteAccount;
 	}

 	/**
	 * Get Item Summaries Without Item Data
	 * 
	 * @return array
	 */
	public function getItemSummariesWithoutItemData()
	{
		if( $this->getCobsessionToken() == "" )
			$this->authorize();

		$config  = array(
			'url' => $this->serviceBaseUrl . self::URI_GET_ITEM_SUMMARY_WO_DATA,
			'parameters' => array(
				'cobSessionToken'  => $this->getCobsessionToken(), 
				'userSessionToken' => $this->getUserSessionToken()
			)
		);

		$result = $this->Post($config['url'], $config['parameters']);

		return $result;
	}


	/**
	 * Get Item Summaries for Site
	 * 
	 * @param  int $itemId
	 * @return
	 */
	public function getItemsSummary1($itemId) 
	{

		if( $this->getCobsessionToken() == "" )
			$this->authorize();

		$config  = array(
			'url' => $this->serviceBaseUrl . self::URI_GET_ITEM_SUMMARY1,
			'parameters' => array(
				'cobSessionToken'     => $this->getCobsessionToken(), 
				'userSessionToken'    => $this->getUserSessionToken(),
				'itemId'              => $itemId, 
				'dex.startLevel'      => 0, 
				'dex.endLevel'        => 0, 
				'dex.extentLevels[0]' => 4, 
				'dex.extentLevels[1]' => 4, 
			)
		);

		$result = $this->Post($config['url'], $config['parameters']);

		return $result;
	}

	/**
	 * Get Site Id Based on Member Site Account Id
	 * 
	 * @param  integer $memSiteAccId
	 * @return
	 */
	public function getSiteId( $memSiteAccId )
	{
		$items = $this->getItemSummariesForSite( $memSiteAccId );

		$siteId = "";

		if( isset($items['Body']) && count($items['Body']) >  0 ) {
			foreach($items['Body'] as $k => $item) {
				if( isset($item->contentServiceInfo->siteId) ){
					$siteId = $item->contentServiceInfo->siteId;
					break;
				}
			}
		}

		return $siteId;
	}

	
	/**
	 * Get Item Summaries for Site
	 * 
	 * @param  int $memSiteAccId
	 * @return
	 */
	public function getItemSummariesForSite($memSiteAccId) 
	{

		if( $this->getCobsessionToken() == "" )
			$this->authorize();

		$config  = array(
			'url' => $this->serviceBaseUrl . self::URI_GET_ITEM_SUMMARY,
			'parameters' => array(
				'cobSessionToken'  => $this->getCobsessionToken(), 
				'userSessionToken' => $this->getUserSessionToken(),
				'memSiteAccId'     => $memSiteAccId
			)
		);

		$result = $this->Post($config['url'], $config['parameters']);

		return $result;
	}

 
	/**
	 * Put MFA Request For Sites
	 *
	 * @param  array $postData
	 * @return
	 */
	public function putMFARequestForSite( $postData = array())
	{ 
		$fieldInfo = json_decode(urldecode($postData['fieldInfo']));
		
		if($this->getCobsessionToken() == "")
			$this->authorize();

		$memSiteAccId = $postData['memSiteAccId'];
 
		//get the object type istance 
		if( isset($fieldInfo->mfaFieldInfoType) && $fieldInfo->mfaFieldInfoType == 'TOKEN_ID' ){
			$objectInstanceType  = 'token';
			$objectInstanceValue =  $postData['token'];
		} else if( isset($fieldInfo->questionAndAnswerValues)){
			$objectInstanceType  = 'questionAndAnwer';
		} else {
			$objectInstanceType  = 'image';
			$objectInstanceValue =  $postData['token'];
		}

		$config = array(
				"url"         => $this->serviceBaseUrl.self::URL_MFA_PUT_REQUEST,
				"parameters"  =>  array(
					'cobSessionToken'    => $this->getCobsessionToken(), 
					'userSessionToken'   => $this->getUserSessionToken(), 
					'memSiteAccId'       => $memSiteAccId
				)
			);
		
		//override object type instance and value
		if( $objectInstanceType == 'image'){ 
			$config["parameters"]['userResponse.objectInstanceType'] = 'com.yodlee.core.mfarefresh.MFAImageResponse';  
			$config["parameters"]['userResponse.token'] = $objectInstanceValue; 

		}elseif($objectInstanceType == 'questionAndAnwer'){
			 
			$config["parameters"]['userResponse.objectInstanceType'] = 'com.yodlee.core.mfarefresh.MFAQuesAnsResponse';  
			//display question and answer parameters here.  
			if( isset($fieldInfo->questionAndAnswerValues ) &&  count($fieldInfo->questionAndAnswerValues ) > 0) {
				foreach($fieldInfo->questionAndAnswerValues as $key => $val ) { 
					$config["parameters"]['userResponse.quesAnsDetailArray[' . $key .'].answer']            = $postData[$val->metaData];
					$config["parameters"]['userResponse.quesAnsDetailArray[' . $key .'].answerFieldType']   = $val->responseFieldType;
					$config["parameters"]['userResponse.quesAnsDetailArray[' . $key .'].metaData']          = $val->metaData;
					$config["parameters"]['userResponse.quesAnsDetailArray[' . $key .'].question']          = $val->question;
					$config["parameters"]['userResponse.quesAnsDetailArray[' . $key .'].questionFieldType'] = $val->questionFieldType; 
				}
			}

		}else{

			$config["parameters"]['userResponse.objectInstanceType'] = 'com.yodlee.core.mfarefresh.MFATokenResponse';  
			$config["parameters"]['userResponse.token'] = $objectInstanceValue;  
		}	


		$result  = $this->Post($config["url"], $config["parameters"]);
		$result['memSiteAccId'] = $memSiteAccId;
		
		return Response::json($result);
	 
	}

	

	/**
	 * Get MFA Response For Site
	 * 
	 * @return json
	 */
	protected function getMFAResponseForSite($siteAccountId)
	{
 	
 		$result = array();

		if( !empty($siteAccountId )  && !empty($this->getCobsessionToken()) ) {

			$config = array(
					"url"        => $this->serviceBaseUrl.self::URL_MFA_RESPONSE,
					"parameters" =>  array(
						'cobSessionToken'  => $this->getCobsessionToken(), 
						'userSessionToken' => $this->getUserSessionToken(), 
						'memSiteAccId'     => $siteAccountId
					)
				);
 
 			$x = 10;

			while ( $x != 0 ) {
				
				$MFAResponse = $this->Post($config["url"], $config["parameters"]);

				// pre($MFAResponse);
				$x--;
			 
				if( isset($MFAResponse['Body']) ) {
 						
					//show error that is not 0
					if( isset($MFAResponse['Body']->errorCode )  ){
						$x = 0;
						$result = $MFAResponse['Body'];
					}else{
 
						//check if message available
						$isMessageAvailable = isset($MFAResponse['Body']->isMessageAvailable) ? $MFAResponse['Body']->isMessageAvailable : FALSE ; 

						// var_dump($isMessageAvailable);

						//return MFA additional fields
						if( $isMessageAvailable ) {
							$x = 0;
							$result = $MFAResponse['Body'];
						} else {
							sleep(2);
						}
					}
				} else {
					$x = 0;
					$result = $MFAResponse; 
				}
			} 	
		}
 
		return $result; 
	}


	/**
	 * loopGetRefreshInfoCall
	 * 
	 * @param  string $memSiteAccId
	 * @return
	 */
	protected function loopGetRefreshInfoCall($memSiteAccId)
	{
		
		$result = array();
		$x      = self::REFRESH_CNT_LOOP;
		$code   = "";
 
		while ( $x != 0 ) {

			$x--;

			//Get Site Refresh Info
			$siteRefreshInfo   = $this->getSiteRefreshInfo($memSiteAccId); 	 						

			//Get the Site Refresh Status
			$siteRefreshStatus = $siteRefreshInfo['Body']->siteRefreshStatus->siteRefreshStatus;
			
			if( isset($siteRefreshInfo['Body']->code) )
				$code = $siteRefreshInfo['Body']->code;

			// echo $code;
			if( $code != 801 ){ 
				
				if( $code == 0 ){
					if( $siteRefreshStatus == "REFRESH_COMPLETED" || $siteRefreshStatus == "REFRESH_TIME_OUT" )
						$x = 0;	
					else
						sleep(2);

					$result  = $siteRefreshInfo['Body']; 
				}else{ 
					// echo 'code '. $code;
					$x = 0;
					$result = $siteRefreshInfo['Body'];
				}
			} else {
				// echo 'code '. $code;
				sleep(2);
			}

			// $result['test'][$x]  = $siteRefreshInfo['Body'];
		}

		return $result;
	}

	/**
	 * Get Site Refresh Infom 
	 * 
	 * @param  int $memSiteAccId
	 * @return json
	 */
	protected function getSiteRefreshInfo( $memSiteAccId ) 
	{
		if(!empty($memSiteAccId) ){			
			$config = array(
					"url"        => $this->serviceBaseUrl.self::URL_REFRESH_SITE_INFO,
					"parameters" =>  array(
						'cobSessionToken'  => $this->getCobsessionToken(), 
						'userSessionToken' => $this->getUserSessionToken(), 
						'memSiteAccId'     => $memSiteAccId
					)
				);

			$result  = $this->Post($config["url"], $config["parameters"]);
 		
 			return $result;

		}
	}

	/**
	 * Register New Customer 
	 *
	 * @param  array $param
	 * @return json
	 */
	public function register( $param = array() )
	{	

		try{
			if( !empty( $this->getCobsessionToken() )) {

				$param = array(
					'cobSessionToken'                    => $this->getCobsessionToken(),
					'userCredentials.loginName'          => $param['loginName'],
					'userCredentials.password'           => $param['password'],
					'userCredentials.objectInstanceType' => 'com.yodlee.ext.login.PasswordCredentials',
					'userProfile.emailAddress' 			 => $param['emailAddress'], 
					'userProfile.firstName'				 => isset($param['firstName']) ? $param['firstName'] : "",
					'userProfile.lastName'				 => isset($param['lastName']) ? $param['lastName']: "",
					'userProfile.middleInitial'			 => 'V', //isset($param['middleInitial']) ? $param['middleInitial']: "Test",
					'userProfile.objectInstanceType'	 => 'com.yodlee.core.usermanagement.UserProfile',
					'userProfile.address1'				 => isset($param['address1']) ? $param['address1']: "",
					'userProfile.address2' 			     => isset($param['address1']) ? $param['address1']: "",
					'userProfile.city'				 	 => isset($param['city']) ? $param['city']: "",
					'userProfile.country'		     	 => isset($param['country']) ? $param['country']: "",
				);

				// pre($param);

				$config = array(
						"url"        => $this->serviceBaseUrl.self::URL_REGISTER_CUSTOMER,
						"parameters" =>  $param
					);

				$result  = $this->Post($config["url"], $config["parameters"]);

				return $result;
			}
		}catch(Exception $e) { 
			writeLogEvent('Yodlee Add Site Account', array( 'Message' => $e->getMessage() ) );  
		}
		
	}

	/**
	 * Get Refreshed User Items
	 * 
	 * @param  string $startDate
	 * @param  string $endDate
	 * @return
	 */
	protected function getRefreshedUserItems( $startDate, $endDate )
	{

		// echo $this->getCobsessionToken();

		// dd();//
		$config = array(
			'url' => $this->serviceBaseUrl . self::URI_GET_REFRESH_USER_ITEMS,
			'parameters' => array(
				'cobrandSessionToken' 			   => $this->getCobsessionToken(),
				'refreshDataFilter.requiredAll'    => false,
				'refreshDataFilter.startDate'      => '06-17-2014T22:05:01', //formatDateByLocale($startDate),
				'refreshDataFilter.endDate'        => '01-17-2015T22:05:01' //formatDateByLocale($endDate),
			)
		); 

		
 		$result = $this->Post($config['url'], $config['parameters']);

		return $result;
	}

	
	/**
	 * Save Yodlee Data 
	 *
	 * @param  array $data
	 * @return
	 */
	public function putYodleeData( $data = array() )
	{  

		if( count($data) > 0 )  {
			//Bank Account has been successfully linked.
			$this->updateYodleeLog($data['siteId'], '1', '0');

			return Response::json(
				array(
					'bankTransaction' => $this->saveBankTransactionInfo(
												  $data['memSiteAccId']
												, $data['siteId']
												, $data['isRRSection']
											), 
					'userTransaction' => $this->saveUserTransactions(
												  $data['memSiteAccId']
												, $data['siteId']
											)
					
				)
			);
		}
	}

	/**
	 * Save Bank Account Detail
	 * 
	 * @param  array  $data
	 * @return
	 */
	protected function saveBankAccountDetail( $data = array() , $isRRSection = 'N' )
	{

	 	$BankAccountDetail = new BankAccountDetail();
	 	$BankAccountDetail->decryptData();

	 	$bankDataHistory = array();

	 	if( count($data) > 0 ) {

	 		foreach($data as $k => $site) {
			 
				if( isset($site->contentServiceInfo->containerInfo->containerName) 
					&&  $site->contentServiceInfo->containerInfo->containerName == 'bank') {

					//save each accounts per banks
					foreach ($site->itemData->accounts as $key => $account) {

						//Double Check Account Holder since other bank does not return Account Holder
						$accountHolder = isset($account->accountHolder) ? $account->accountHolder : "";
						$accountNumber = isset($account->accountNumber) ? $account->accountNumber : "";

						$acctNr = cleanNonNumeric($accountNumber);

 						try{

							$bankDataHistory[] = array(
								'User_Id'                 => $this->getUserId(),
								'Loan_App_Nr'             => $this->getLoanAppNr(),
								'Yodlee_Access_Dt'        => date('Y-m-d'), 
								'Member_Site_Id'		  => $site->memSiteAccId,
								'Item_Type_Id'            => $site->itemId,
								'Site_Id'                 => $site->contentServiceInfo->siteId,
								'Acct_Holder_Name'        => $BankAccountDetail->encryptFields($accountHolder),
								'Acct_Nr'                 => $BankAccountDetail->encryptFields($acctNr),
								'Acct_Type_Desc'		  => $account->acctType,
								'Current_Bal_Amt'         => $account->currentBalance->amount,
								'Available_Bal_Amt'       => $account->availableBalance->amount,
								'Created_By_User_Id'      => $this->getUserId(),
								'Create_Dt'               => date('Y-m-d'),
							);

						}catch(Exception $e) {
							writeLogEvent('Yodlee Save Bank Account Details', 
								array( 
									'User_Id'     => $this->getUserId(),
									'Loan_App_Nr' => $this->getLoanAppNr(),
									'Message'     => $e->getMessage()
								) 
							);
						}
					}
				}
			}

			// _pre($bankDataHistory);

			if( count($bankDataHistory) > 0 ){
				
				try{

					$BankAccountDetail::insert( $bankDataHistory ); 

					$currentBalance = 0; 
 					//Get current Balance
 					if( isset($bankDataHistory[0]['Current_Bal_Amt']) )
 						$currentBalance = $bankDataHistory[0]['Current_Bal_Amt'];

 					//Update NLS information
					if( $isRRSection == 'N' ) {
  						$this->updateNLSInfo( $currentBalance ); 
					}

					$linkBankAcctFlag = 'Y';

					//Update Bank Account Linked Flag
					LoanDetail::updateBankAcctLinked( 
						$this->getUserId(), 
						$this->getLoanAppNr(), 
						$linkBankAcctFlag 
					);


					if( Session::get('VerificationFastlink') == true ) {
						$loanAppPhaseNr = 7; 
						$subPhaseCode   = 'BankVerificationSubmitted';						
					}else{
						$loanAppPhaseNr = 3; 
						$subPhaseCode   = 'BankAccountLinkSuccess';	
					}

					//Save Loan Application Phase 
					setLoanAppPhase( 
						$this->getUserId(), 
						$this->getLoanAppNr(), 
						$loanAppPhaseNr, 
						$subPhaseCode  
					);

				}catch(Exception $e) {
					//Write Every MFA Request
					writeLogEvent('Yodlee Save Bank Account Details', 
						array( 
							'User_Id'     => $this->getUserId(),
							'Loan_App_Nr' => $this->getLoanAppNr(),
							'Message'     => $e->getMessage()
						) 
					); 
				}

				
				
			}
	 	} 
	}

	/**
	 * Update NLS Information 
	 *
	 * @todo
	 * 	 - please use the NLS Helper
	 * 	 - avoid using repeatitive functions
	 * 	 
	 * @return
	 */
	protected function updateNLSInfo( $currentBalance = 0 )
	{
	 
		$loanDetail = LoanDetail::where(
				array(
					'Borrower_User_Id' => $this->getUserId(), 
					'Loan_Id'  => $this->getLoanAppNr()
				)
			)->first();

		$whereParam = array('User_Id' => $this->getUserId() ); 
		$applFields = array('First_Name','Middle_Name','Last_Name',	); 
		$applData = Applicant::getData( $applFields, $whereParam, true);		 
		$lt = LoanTemplate::where('Loan_Template_Id', '=', $loanDetail->Loan_Template_Id)->first();	
		$loanTemplate = $lt->Loan_Template_Name;
		
		$NLS = new NLS();
 	  	
 	  	$fields = array(
			'nlsLoanTemplate'     => $loanTemplate,
			'contactId'           => $this->getUserId(),
			'loanId'              => $this->getLoanAppNr(),
			'firstName'           => $applData->First_Name,
			'lastName'            => $applData->Last_Name,
			'loanStatusCode'      => 'APPLICATION_STEP3',
			'totalNSFs'           => $this->getTotalNSFCnt(),
			'DaysSinceNSF'        => $this->getDaysSinceNSF(),
			'MoDepositToAverage'  => number_format($this->getAvgMonthlyDepositAmt(), 2, '.', ''),
			'avgBalPmtDate'       => number_format($this->getAvgBalOnPmtDtAmt(), 2 ,'.', ''),
			'currentBalance'      => $currentBalance,
			'numLowBalanceEvents' => number_format($this->getLowBalEventCnt(), 2, '.', ''),
			'numLowBalanceDays'   => number_format($this->getLowBalDayCnt(), 2, '.', ''),
			'LOANDETAIL1'         => 1
		);

		$NLS->nlsUpdateLoan($fields);

		if( $loanDetail->Bank_Acct_Linked_Flag != 'Y') {
			// task comment attributes
			$TaskComment = array(
				'commentStr'	=> 'Attached with Yodlee',
				'files'	=> ''	 
			);

			createNLSTask( 'CONFIRM_BANK_ACCOUNT', $TaskComment, '' ); 
		}
	}

	/**
	 * Save Bank Transactions
	 * 
	 * @param  int $memSiteAccId
	 * @return
	 */
	protected function saveBankTransactionInfo($memSiteAccId, $siteId, $isRRSection = 'N' )
	{

		if( !empty($memSiteAccId) ) {
			
			//@overrdie side id from session if siteId param is empty.
			if( empty( $siteId) )
				$siteId  = $this->getSiteIdFromSession();

			$siteInfo = $this->getItemSummariesForSite($memSiteAccId);

			// _pre($siteInfo);
			
			//filter bank accounts only
			//save only bank accounts
			if( isset($siteInfo["Body"])) {

				$data = array(); 

				//call BankAccountInfo Model
				$bankAccountInfo = new BankAccountInfo();
				$bankAccountInfo->decryptData();

				if( count($siteInfo["Body"]) > 0 ) {
					//save Bank Account Detail 
					$this->saveBankAccountDetail($siteInfo["Body"], $isRRSection ); 

					foreach($siteInfo["Body"] as $k => $site) {
					
						if( isset($site->contentServiceInfo->containerInfo->containerName) 
							&&  $site->contentServiceInfo->containerInfo->containerName == 'bank') {  
							//save each accounts per banks
							foreach ($site->itemData->accounts as $key => $account) {
								//Double Check Account Holder since other bank does not return Account Holder
								$accountHolder = isset($account->accountHolder) ? $account->accountHolder : "";
								$accountNumber = isset($account->accountNumber) ? $account->accountNumber : "";

								$acctNr = cleanNonNumeric($accountNumber);

								$data[] = array(
									'User_Id'            => $this->getUserId(), 
									'Site_Id'            => $site->contentServiceInfo->siteId, 
									'Acct_Holder_Name'   => $bankAccountInfo->encryptField($accountHolder),  
									'Acct_Type_Desc'     => $account->acctType,  
									'Acct_Nr'            => $bankAccountInfo->encryptField($accountNumber), 
									'Created_By_User_Id' => $this->getUserId(), 
									'Create_Dt'          => date('Y-m-d'),  
								); 
							}

						}
					}
				} else {
					return array(
						'errorOccured' => true, 
						'errorMsg'     => 'No transactions records found!'
					);
				}
 
				//save transaction to ODS
				if( count($data) > 0 ){
					try{

						$bankAccountInfo::insert($data);	

						return array(
							'isSaved' => true, 
							'transactionsCount' => count($data)
						);

					}catch(Exception $e) {
						//Write Every MFA Request
						writeLogEvent('Yodlee Save Bank Account Details', 
							array( 
								'User_Id'     => $this->getUserId(),
								'Loan_App_Nr' => $this->getLoanAppNr(),
								'Message'     => $e->getMessage()
							) 
						);
					}
				}
			}else {
				return array(
					'errorOccured' => true, 
					'errorMsg'     => 'No transactions records found!'
				);
			} 
		}
	}


	/**
	 * Is Yodlee Access Date Exist
	 * 
	 * @param  string  $yodleeAccessDt
	 * @param  integer $userId
	 * @return boolean
	 */
	protected function isYodleeAccessDtExist( $yodleeAccessDt, $userId )
	{
		if(!empty($yodleeAccessDt) && !empty($userId) )
			return BankAccountTxnDetails::whereRaw("Yodlee_Access_Dt = $yodleeAccessDt and User_Id = $userId" )->count();
	}

	/**
	 * Save User Transactions
	 * 
	 * @param  int $siteAccountId
	 * @return
	 */
	public function saveUserTransactions($memSiteAccId, $siteId, $months = 3 ) 
	{
		$response = array();

		if( !empty($this->getCobsessionToken()) && !empty($this->getUserSessionToken()) ) {

			//@overrdie side id from session if siteId param is empty.
			if(empty( $siteId) )
				$siteId  = $this->getSiteIdFromSession();

			//Get User Transactions based on Member Site Account Id
			$transactions = $this->getUserTransactions($memSiteAccId, $months );
 	
			if( isset($transactions["Body"]->transactions) ) {

				$data = array();

				//Bank Account Transaction Details
				$BankAccountTxnDetails = new BankAccountTxnDetails();

				//run the decryption query
				$BankAccountTxnDetails->decryptData();

				if( count($transactions["Body"]->transactions) > 0 ) {

					$this->getBnkAcctDetailsBasedOnTrans($transactions["Body"]->transactions);
					
					try{
						//prepare the bulk transaction
						foreach($transactions["Body"]->transactions  as $transaction) {
							
							$transactionDate       = ''; 
							$postingDate           = '';
							$merchantName          = ''; 
							$transactionTypeDesc   = '';
							$categoryName          = '';
							$categorizationKeyword = '';
							$transactionId         = '';
							$transactionDesc  	   = '';

							//Evaluate Transaction Date
							if( isset($transaction->transactionDate) ){
								$transactionDateObj = new DateTime($transaction->transactionDate);
								$transactionDate    = $transactionDateObj->format('Y-m-d H:i:s');
							}
						
							//Evaluate Post Date
							if( isset($transaction->postDate) ) {
								$pDateObj    = new DateTime($transaction->postDate); 
								$postingDate =  $pDateObj->format('Y-m-d H:i:s');	
							}

							//Assign Transaction Date same as Post Date if
							//Transaction Date is EMPTY						

							if( $transactionDate == "")
								$transactionDate = $postingDate;

							$NSFFlag = 0;
							
							//Set Not Suffecient Fund if Account Balance is 
							//Less than Zero				
							if( $transaction->account->accountBalance->amount <= 0 ) 
								$NSFFlag = 1;

							//Merchant Name
							if( isset($transaction->description->merchantName) )
								$merchantName = $transaction->description->merchantName;

							//transaction type description
							if( isset($transaction->description->transactionTypeDesc) )
								$transactionTypeDesc = $transaction->description->transactionTypeDesc;

							//category name
							if( isset($transaction->category->categoryName) )
								$categoryName = $transaction->category->categoryName; 

							//cagetorization desc		
							if( isset($transaction->categorizationKeyword) )
								$categorizationKeyword  = $transaction->categorizationKeyword;

							//transaction id 
							if( isset($transaction->viewKey->transactionId) ) 
								$transactionId =  $transaction->viewKey->transactionId; 

							//transaction description
							if( isset($transaction->description->description) ) 
								$transactionDesc = $transaction->description->description;

							$acctNr = cleanNonNumeric($transaction->account->accountNumber);
						
							$data[] = array(
								'Site_Id'               => $siteId, 
								'User_Id'				=> $this->getUserId(),
								'Acct_Holder_Name'      => $BankAccountTxnDetails->encryptFields($transaction->account->accountName),
								'Acct_Type_Desc'        => $transaction->viewKey->containerType,
								'Acct_Nr'               => $BankAccountTxnDetails->encryptFields($acctNr),
								'Yodlee_Access_Dt'      => date('Y-m-d'),
								'Merchant_Name'         => $merchantName, 
								'Txn_Categ_Name'        => $categoryName,
								'Txn_Categ_Keyword_Txt' => $categorizationKeyword, 
								'Txn_Dttm'              => $transactionDate,
								'Txn_Id'                => $transactionId,
								'Txn_Base_Type_Cd'      => $transaction->transactionType,
								'Txn_Status_Desc'       => $transaction->status->description,
								'Txn_Type_Desc'			=> $transactionTypeDesc,
								'Txn_Desc'              => $transactionDesc,
								'Txn_Amt'               => $transaction->amount->amount,
								'Posting_Dttm'          => $postingDate,
								'NSF_Flag'              => $NSFFlag,
								'Created_By_User_Id'    => $this->getUserId(),
								'Create_Dt'             => date('Y-m-d')
							);
						}
					}catch(Exception $e) {
						// _pre($e->getMessage());
					}
				}

				//save transaction to ODS
				if( count($data) > 0 ){

					try{

						$BankAccountTxnDetails::insert($data);

						//Bank Account has been successfully linked.
						$this->updateYodleeLog($siteId, '1', '1');

						//Calling Bank Summary SP
						$this->execBankSummarySP($this->getUserId());
						
						return array(
							'isSaved'           => true, 
							'transactionsCount' => count($data), 
							'spBankAcctSummary' => $spBankAcctSummary
						);

					}catch(Exception $e) {
						//Write Every MFA Request
						writeLogEvent('Yodlee Save User Transactions', 
							array( 
								'User_Id'     => $this->getUserId(),
								'Loan_App_Nr' => $this->getLoanAppNr(),
								'Message'     => $e->getMessage()
							) 
						);
					}
				}

			} else {

				//Write Every MFA Request
				writeLogEvent('Yodlee Save User Transactions', 
					array( 
						'User_Id'     => $this->getUserId(),
						'Loan_App_Nr' => $this->getLoanAppNr(),
						'Message'     => 'No User Transactions Found!'
					) 
				);

				return array(
					'errorOccured' => true, 
					'errorMsg'     => 'No transactions records found!' );
			} 
		}

	}

	/**
     * Execute Bank Summary SP 
     * 
     * @param  integer $userId
     * @return 
     */
    protected function execBankSummarySP( $userId = NULL )
    {
    	if( !empty($userId) ) {
			return DB::select("EXEC ODS.dbo.usp_Bank_Acct_Summary ?", 
				array( $userId ) 
			);
		}
    }

	/**
	 * Test Login User
	 * 
	 * @param string $token
	 */
	protected function TestLoginUser($token)
	{
 
		if( empty($this->getUserSessionToken()) && !empty($token) ) {
		
			$userResult = $this->login( 
							  'sbMemtaras.shramrr31'
							, 'sbMemtaras.shramrr31#123'
							, $token
						);

			if($userResult['isValid'] == true ) { 

				if( isset($userResult['Body']->userContext->conversationCredentials->sessionToken)) {
					
					//set the user token and redirect 
					$this->setUserSessionToken( 
												$userResult['Body']
													->userContext
													->conversationCredentials
													->sessionToken
											);
					return true;

				}
				return false;
			}
			return false;
		}
		return true;	
	}

	public function setOauthAccessToken( $token )
	{
		Session::set('OauthToken', $token );
	}

	public function generateLaunchFastLink($token)
	{

		$config = array(
			"url" => 'https://node.developer.yodlee.com/authenticate/restserver/',
			"parameters"       => array( 
				"rsession" => $this->getUserSessionToken(), 
				"app"      => '10003600', 
				"token"    => $token,
				"redirectReq" => false, 
				"extraParams" => ""
			)
		); 

		// _pre($config);
 
		$result = $this->Post($config['url'], $config['parameters']);

		// _pre($result);

		return $result;


		
	}

	/**
	 * Generate FastLink Url 
	 *
	 * @param  string $section
	 * @return string
	 */
	public function generateFastLinkUrl( $section = 'LBA')
	{

		// if( !empty( Session::get('rsessionToken')) ) {
			// $rsession =  Session::get('rsessionToken');
		// }else{
			$rsession = $this->getRsessionToken();
			// Session::set('rsessionToken', $rsession );
		// }

		// _pre($rsession);
  		 
		$tokenId = $rsession['Body']->finappAuthenticationInfos[0]->token;
		$finappId = 10003600;

		// $fastLink = $this->generateLaunchFastLink($tokenId);

		// _pre($fastLink);
 	 	 
 	//  	if( empty( Session::get('fastLinkUrl') ) ) {
		// 	$fastLink = $this->generateLaunchFastLink($tokenId);
		// 	$fastLinkUrl = 'https://node.developer.yodlee.com' . $fastLink['Body']->finappAuthenticationInfos[0]->finappURL;
		// 	Session::put('fastLinkUrl' , $fastLinkUrl);
		// }
  
		$data = array(
			'rsession'     => $this->getUserSessionToken(), 
			'token'        => $tokenId, 
			'node_url'     => 'https://node.developer.yodlee.com/authenticate/restserver/', 
			'finapp_id'    => $finappId, 
			'extra_params' => '', 
		); 

		Session::set('fastLinkUrl', $data );
	 	 
	 	// _pre($data);
 
		// return $fastLinkUrl;
		return $data;

		// dd();
  		// _pre($test);

		//Generate Oauth Token
		// if( !Session::get('OauthToken') ){
		// 	$token =  $this->getOAuthAccessToken( self::BRIDGET_APP_ID ); 
		// 	$this->setOauthAccessToken($token);
		// }else{
		// 	$token = Session::get('OauthToken');
		// }
 
		// //Regenerate New Token		 
		// if( isset($token['Body']->errorCode) && $token['Body']->errorCode == 415 ) {
		// 	$this->createYodleeAccount();
		// 	$token =  $this->getOAuthAccessToken( self::BRIDGET_APP_ID ); 
		// 	$this->setOauthAccessToken($token);
		// }
    
 	// 	//Generate Fast Link URL
		// if( isset($token['Body']) && !isset($token['Body']->errorCode) ) { 

		// 	$date       = new DateTime();  
		// 	$timestamp  = $date->getTimestamp();
		// 	$oauthNonce = generateRandomChar(11) . $timestamp;
		// 	$method     = 'GET';

		// 	$url  = cleanHttpParam(self::FAST_LINK_URL);

		// 	//clean http parameters
		// 	$param = array(
		// 		'access_type'            => 'oauthdeeplink', 
		// 		'displayMode'            => 'desktop',
		// 		'oauth_callback'		 => url(). '/fastlinkCallBack', 
		// 		'oauth_consumer_key'     => self::FAST_LINK_APP_KEY,
		// 		'oauth_nonce'            => $oauthNonce,
		// 		'oauth_signature_method' => 'HMAC-SHA1', 
		// 		'oauth_timestamp'        => $timestamp,
		// 		'oauth_token'            => $token['Body']->token, 
		// 		'oauth_version'          => '1.0',

		// 	);

		// 	$paramClean = cleanHttpParam(http_build_query($param));
			//generate the base string 			
			$baseStringParam = array( $method, $url,  $paramClean );
			$baseString      = implode('&', $baseStringParam); 
			$signatureId     = base64_encode(hash_hmac('sha1', $baseString, self::FAST_LINK_APP_TOKEN . '&' . $token['Body']->tokenSecret, true));
			$fastLinkUrl     = self::FAST_LINK_URL . '?' . http_build_query($param) . '&oauth_signature=' . $signatureId;

			//Set Fastlink Verification
			if( $section == 'VERIFICATION' ){
				Session::put('VerificationFastlink' , true );
			}

			//Set Fast Link URL
		  	Session::put('fastLinkUrl' , $fastLinkUrl);
 
		//   	return $fastLinkUrl;
		// }
		// 


		//https://node.developer.yodlee.com//finapp/10003600/?brand=10010352&id=10003600&appId=3A4CAE9B71A1CCD7FF41F51006E9ED00&channelId=-1&version=9.13&status=published&c=csit_key_0:rvC9JMX0Ij1u4ZeHBbtkUGFEYhg=&finappCDNURL=&resturl=https%3A%2F%2F172.17.25.88%2Fservices%2Fsrest%2Frestserver&l=
	
		//https://node.developer.yodlee.com/finapp/10003600/?brand=10010352&id=10003600&appId=3A4CAE9B71A1CCD7FF41F51006E9ED00&channelId=-1&version=9.13&status=published&c=csit_key_0:19KN9FUrLc83m5HWWy9Byc3pnrk=&finappCDNURL=&resturl=https%3A%2F%2F172.17.25.88%2Fservices%2Fsrest%2Frestserver&l=&Keyword=dag
		// 
		// https://node.developer.yodlee.com/finapp/10003600/?brand=10010352&id=10003600&appId=3A4CAE9B71A1CCD7FF41F51006E9ED00&channelId=-1&version=9.13&status=published&c=csit_key_0:NZyzbY87K3LO+ya8Sijzqg2PG/M=&finappCDNURL=&resturl=https%3A%2F%2F172.17.25.88%2Fservices%2Fsrest%2Frestserver&l=
	} 

	/**
	 * Create Yodlee Account
	 * 
	 * @return
	 */
 	public function createYodleeAccount()
 	{	
 		if( $this->isDebug == 1 ) {

			Session::put('YodleeUserName', 'sbMemtaras.shramrr31' );
			Session::put('YodleePassword', 'sbMemtaras.shramrr31#123' );

			//authorize the yodlee user
			$login = $this->cobUserAuthorize( $this->getCobsessionToken() );

			if( $login['result'] == 'success' ){ 

				return array(
						'result'   => 'success', 
						'code'	   => 'AccountAlreadyExist',
						'message'  => 'Account already exist',
						'debugMode'  => Config::get('system.Ascend.DebugMode'),
						'dubeg'  => 'testddd'
					);
			} else{ 

				return array(
						'result'   => 'failed', 
						'code'	   => 'RedirectToExclusion',
						'message'  => 'Account Creation Failured. Redirecting to Scoring.'
					);
			}

 		}else{

	 		if( !empty($this->getCobSessionToken()) ) {
 
	 		 	if( empty($this->getUserSessionToken()) ){
					$userId = Session::get('uid');
	 
					if( !empty($userId) ) {
			 	
						$acctfields = array( 'Email_Id', 'User_Id', 'Password_Txt', 'User_Name');
						$appFields  = array('First_Name', 'Last_Name', 'Middle_Name', 'Yodlee_User_Name', 'Yodlee_Password_Txt');
						$whereParam = array('User_Id' => $userId); 

						//Get user information, applicant and applicant address
						$userData   = User::getData( $acctfields, $whereParam, true ); 
						$appData    = Applicant::getData($appFields, $whereParam, true );
	 						
						$param = array(); 
	 
						if( !empty( $userData ) ) {
			
							//Check if Yodlee Account already Exist			
							$isUserExist =  $this->validateUser( $this->getCobsessionToken(), $userData->User_Name );
	 	
							if ( $isUserExist ) {
								//Set Yodlee User Name and Password to Session
								Session::put('YodleeUserName', $userData->User_Name );
								Session::put('YodleePassword', $appData->Yodlee_Password_Txt );

								//Try Authorize the user using the created account
								$login = $this->cobUserAuthorize( $this->getCobsessionToken() );
	  
								if( $login['result'] == 'success' ){
									 
									return array(
											'result'    => 'success', 
											'code'      => 'AccountAlreadyExist',
											'message'   => 'Account already exist',
											'debugMode' => Config::get('system.Ascend.DebugMode') 
										);

								}else{ 

									return array(
												'result'   => 'failed', 
												'code'	   => 'RedirectToExclusion',
												'message'  =>  $login['message'] . ' .Account Creation Failured. Redirecting to Scoring.'
											);
								}
								
							} else{  
								return $this->newYodleeAccount($userId, $userData, $appData); 
							}

						} else {
							return array(
												'result'   => 'failed', 
												'code'	   => 'RedirectToExclusion',
												'message'  => 'Account Creation Failured. Redirecting to Scoring.'
											);
						}
					}
				} else{
					return array(
							'result'    => 'success', 
							'code'      => 'AccountAlreadyExist',
							'message'   => 'Account already exist',
							'debugMode' => Config::get('system.Ascend.DebugMode') 
						);
	
				}

			} else {
				return array(
								'result'   => 'failed', 
								'code'	   => 'RedirectToExclusion',
								'message'  => 'No Cob Session Found!.'
							);
			}
		}
 	}

 	/**
 	 * Create New Yodlee Account
 	 * 
 	 * @param  object $userData
 	 * @param  object $appData
 	 * @return
 	 */
 	protected function newYodleeAccount( $userId, $userData, $appData )
 	{

 		$param['loginName']  	= $userData->User_Name;  
		$param['emailAddress']  = $userData->Email_Id; 

		try{ 

			if( $appData ) {

				$param['firstName']     = $appData->First_Name; 
				$param['lastName']      = $appData->Last_Name; 
				$param['middleInitial'] = substr($appData->Middle_Name, 0 , 1); 
				$param['password']   	= $appData->Yodlee_Password_Txt;
			}
				 
			$appAddDataSql = ApplicantAddress::getData(
				array(
					'Street_Addr_1_Txt', 
					'Street_Addr_2_Txt', 
					'City_Name'
				), 
				array(
					'User_Id' => $userId
				), 
				TRUE
			);
 

			if( $appAddDataSql ) {
				 
				$param['address1'] = $appAddDataSql->Street_Addr_1_Txt;
				$param['address2'] = $appAddDataSql->Street_Addr_2_Txt;
				$param['city']     = $appAddDataSql->City_Name;
				$param['country']  = 'United States';   //create a default country 
			} 

			//account creation 
			if( count($param) > 0 ) {

				try{	
 
					//register the customer 
					$isRegistered =  $this->register($param);
 
					Session::put('YodleeUserName', $userData->User_Name );
					Session::put('YodleePassword', $appData->Yodlee_Password_Txt );

					//authorize the yodlee user
					$login = $this->cobUserAuthorize( $this->getCobsessionToken() );
 

					if( $login['result'] == 'success' ){
						return array(
								'result'   => 'success', 
								'code'	   => 'AccountAlreadyExist',
								'message'  => 'Account already exist',
								'debugMode'  => Config::get('system.Ascend.DebugMode')
							);
					}

					//redirect to Exlusion page
					return array(
								'result'   => 'failed', 
								'code'	   => 'RedirectToExclusion',
								'message'  => 'Account Creation Failured. Redirecting to Exclusion and Scoring.'
							);

				} catch(Exception $e) {

					writeLogEvent('Yodlee Add Site Account', 
						array( 
							'User_Id' => $userId, 
							'Message' => 'Adding Yodlee Account Failed.', 
							'Result'  => $e->getMessage()
						) 
					);

					//redirect to Exlusion page
					return array(
								'result'   => 'failed', 
								'code'	   => 'RedirectToExclusion',
								'message'  => $e->getMessage() . '. Account Creation Failured. Redirecting to Exclusion and Scoring.'
							);
				}
			} else {
				//redirect to Exlusion page
				return array(
								'result'   => 'failed', 
								'code'	   => 'RedirectToExclusion',
								'message'  => 'Parameter is empty. Account Creation failed. Redirecting to Exclusion and Scoring.'
							);
			}

		} catch(Exception $e) {

			writeLogEvent('Yodlee Add Site Account', 
				array( 
					'User_Id' => $userId, 
					'Message' => 'Adding Yodlee Account Failed.', 
					'Result'  => $e->getMessage()
				) 
			);

			return array(
						'result'   => 'failed', 
						'code'	   => 'RedirectToExclusion',
						'message'  => $e->getMessage() . '. Account Creation Failured. Redirecting to Exclusion and Scoring.'
					);
		}
 	}
 
	/**
	 * Addsite Account - POST Method
	 *
	 * @param array $postData
	 */
	public function postAddSiteAccount( $postData = array())
	{
 
		$loginFormBody = json_decode($postData['body'] );
		$siteId        = $postData['siteId']; 
		$componentList = $loginFormBody->Body->componentList;
 			
 		//try login
 		$siteAccountLogin  = $this->addSiteAccount($componentList, $postData);

 		// return Response::json($siteAccountLogin); 

  		//assign memSiteAccId
  		$memSiteAccId = (isset($siteAccountLogin["Body"]->siteAccountId)) ? $siteAccountLogin["Body"]->siteAccountId:"";

 		//validate member site account id 
 		if( $memSiteAccId ) {

 			//check for error code
 			$errorCode = isset( $siteAccountLogin->errorCode)  ? $siteAccountLogin->errorCode : "" ;

 			//if error code found return error
 			//else process bank authentication
 			if( !empty($errorCode) ){
 				return Response::json( array('error' => $siteAccountLogin->errorCode)); 
 			}else{
 				return Response::json( $this->processAuthentication(
 								$siteAccountLogin, 
 								$memSiteAccId 
 						) 
 				);
 			}

 		} else {

 			if( isset( $siteAccountLogin->errorCode ))
 				return Response::json($siteAccountLogin->errorCode); 
 			else
 				return Response::json($siteAccountLogin); 
 		}
	}

	/**
	 * Process MFA Or Normal Bank Authentication 
	 * 
	 * @param  int $siteAccountLogin
	 * @return array
	 */
	protected function processAuthentication($siteAccountLogin, $memSiteAccId)
	{

		$response =  array(); 

		//validate site refresh info object
		if( isset($siteAccountLogin['Body']->siteRefreshInfo) ){ 

			$siteRefreshStatus   = $siteAccountLogin['Body']->siteRefreshInfo->siteRefreshStatus;
			$siteRefreshMode     = $siteAccountLogin['Body']->siteRefreshInfo->siteRefreshMode;
		
			// _pre($siteRefreshStatus);


			//Check Refresh Status Id			
			if( $siteRefreshStatus->siteRefreshStatusId == self::REFRESH_TRIGGERED ) {
 	
 				// echo $siteRefreshStatus->siteRefreshStatusId;
 				// echo $siteRefreshMode->refreshModeId;

				//Check for Site Refresh Mode
				if( $siteRefreshMode->refreshModeId == self::MFA_TYPE_MFA){

					//Get mfaAuthentication
					$MFAResponse = $this->mfaAuthentication($memSiteAccId);

					try{

						$response = $MFAResponse;

						if( gettype($response) == 'object')
							$response->memSiteAccId = $memSiteAccId;

					}catch(Exception $e){

						//Write Every MFA Request
						writeLogEvent('Yodlee Process Authentication', 
							array( 
								'User_Id'     => $this->getUserId(),
								'Loan_App_Nr' => $this->getLoanAppNr(),
								'Message'     => $e->getMessage()
							) 
						);

					}
  
				} else{
					$response = $this->normalAuthentication($memSiteAccId);
					
					//Check for the response type
					if( gettype($response) == 'object')
						$response->memSiteAccId = $memSiteAccId;

				}
			}
		}

		// _pre($response);

		return $response;
	}

	/**
	 * MFA Authentication
	 * 
	 * @param  int $memSiteAccId
	 * @return
	 */
	protected function mfaAuthentication($memSiteAccId) 
	{

		$result = array();

		//get MFA response for site
		$MFAResponse = $this->getMFAResponseForSite($memSiteAccId);

		// _pre($MFAResponse);

		if( isset($MFAResponse->errorCode) ) {
			if($MFAResponse->errorCode == self::ERROR_ALL_FINE )
				$result = $this->normalAuthentication($memSiteAccId);
			else
				$result =  array('error' => showYodleeError($MFAResponse->errorCode));
		} else {
			$result = $MFAResponse;	
		}

		return $result;
	}

	/**
	 * Normal Authentication 
	 * 
	 * @param  int $memSiteAccId
	 * @return [type]
	 */
	protected function normalAuthentication($memSiteAccId) 
	{ 
		$result = $this->loopGetRefreshInfoCall($memSiteAccId);

		if( isset($result->code) && $result->code != self::ERROR_ALL_FINE )
			$result  = array('error' => showYodleeError($result->code));

		return $result;
	}


	/**
	 * Get Financial Organization 
	 * 
	 * @param  string  $bankName
	 * @param  integer $limit
	 * @param  array   $fields
	 * @return
	 */
	protected function getFinOrg($bankName, $limit, $fields = array() )
	{
		
 		$finOrgs = FinancialOrganization::getFinancialOrganization(
 								  $bankName
 								, $limit
 								, $fields
 							);
 		return $finOrgs;
	}

	/**
	 * Search Bank Information 
	 * 
	 * @return json
	 */
	public function searchBank( $postData = array() )
	{
	 	if( $postData ) {

	 		if( !isset($postData['bankName'])|| empty($postData['bankName']))
	 			return false;


	 		if( $this->isDebug == 0 ) {

		 		$fields = array('Site_Name', 'Site_Id','Popularity_Idx_Val', 'Image_Url_Txt');

		 		//get search banks 
		 		$finOrgs = $this->getFinOrg(
		 								  $postData['bankName']
		 								, $this->topBanksLimit
		 								, $fields
		 							);
	 
				$result['cobSessionToken']  = $this->getCobsessionToken();
				$result['userSessionToken'] = $this->getUserSessionToken();
				$result['sites']            = array();
	 
	 
				if($finOrgs) {
			 		foreach($finOrgs as $finOrg) {
		 
			 			$result['sites'][] = array(
			 				'siteId'   => $finOrg->Site_Id,
			 				'bankName' => $finOrg->Site_Name,
			 				'imageUrl' => $finOrg->Image_Url_Txt
			 			);
			 		} 
		 		}
	 
		 		return Response::json($result);
 			
 			} else {
	 		
 				$this->TestLoginUser($this->getCobsessionToken());

			 	//search bank
			 	$site = $this->searchSite(
			 		$this->getCobsessionToken(), 
			 		$this->getUserSessionToken(), 
			 		$postData['bankName']
			 	);

			 	//get list of site Id's
			 	$siteIds = array(); 

			 	try{
				 	
				 	if(isset($site['Body']) && count($site['Body']) > 0 && $site['Body'] != '[]' ) {
				 		foreach ($site['Body'] as $key => $value) { 
				 			$siteIds[] = array(
				 				'siteId'  => $value->siteId,
				 				'bankName' =>$value->defaultDisplayName
				 			);
				 		}
				 	}
 
			 	}catch(Exception $e){
			 		//Write Every MFA Request
					writeLogEvent('Yodlee - searchBank', 
						array( 
							'User_Id'     => $this->getUserId(),
							'Loan_App_Nr' => $this->getLoanAppNr(),
							'Message'     => $e->getMessage()
						) 
					);
			 	}

			 	$result = array(
					'cobSessionToken'  => $this->getCobsessionToken(),
					'userSessionToken' => $this->getUserSessionToken(),
					'sites'            => $siteIds
			 	);

			 	return Response::json($result);
		 	}
	 	} 
	}

	/**
	 * Validate Yodlee user 
	 * 
	 * @param  string $cobSessionToken
	 * @param  string $userName
	 * @return
	 */
	protected function validateUser($cobSessionToken, $userName)
	{

		if(!empty($cobSessionToken) && !empty($userName) ){
			$config = array(
					"url"        => $this->serviceBaseUrl.self::URL_VALIDATE_USERNAME,
					"parameters" =>  array(
						'cobSessionToken' => $cobSessionToken, 
						'userName'		  => $userName
					)
				);

			$result  = $this->Post($config["url"], $config["parameters"]);

			if( isset($result['Body'] ))
				return $result['Body']->primitiveObj; 
			else 
				return false;

		} else {
			return false;
		}
	}

	/**
	 * Cobrand User Login
	 * 
	 * @return
	 */
	protected function cobLogin(){

		$response = $this->getCobraAndSessionToken(
				  $this->cobLoginUsername
				, $this->cobLoginPassword
			);

		if( $response['isValid'] == true ){

			$body = $response['Body'];

			//set cob user token
			if($body->cobrandConversationCredentials)
				$token = $body->cobrandConversationCredentials->sessionToken;

			//set user token 
			$this->setCobSessionToken($token);

		}

		return $response;
	}


	/**
	 * Authorize Yodlee User
	 *  
	 * @return json
	 */
	public function authorize()
	{

		$token =  '';
 	 
		$response = $this->getCobraAndSessionToken(
				  $this->cobLoginUsername
				, $this->cobLoginPassword
			);

		if( $response['isValid'] == true ){

			$body = $response['Body'];

			//set cob user token
			if($body->cobrandConversationCredentials)
				$token = $body->cobrandConversationCredentials->sessionToken;
	
			//set user token 
			$this->setCobSessionToken($token);
  		
		} else{
			return $response;
		}
	}

	/**
	 * Cob User Authorize 
	 * 
	 * @param  string $token
	 * @return 
	 */
	protected function cobUserAuthorize( $token )
	{

		if( !empty($token) ) {

			$userResult = $this->login( 
							  Session::get('YodleeUserName')
							, Session::get('YodleePassword')
							, $token
						);


			// _pre($userResult);

		 	
			if( $userResult['isValid'] == true ) { 

				if( isset($userResult['Body']->userContext->conversationCredentials->sessionToken)) {
					
					//set the user token and redirect 
					$this->setUserSessionToken( 
												$userResult['Body']
													->userContext
													->conversationCredentials
													->sessionToken
											);
					return array(
							'result' => 'success',
							'message'  => $userResult['Body']->userContext->conversationCredentials->sessionToken
						);

				} else{
					return array(
							'result'   => 'failed',
							'message'  => isset($userResult['Body'][0]) ? $userResult['Body'][0] : ""
						);
				}

			}else{

			return array(
							'result'   => 'failed',
							'message'  => 'Login failed'
						);
			}
		} else {

			return array(
							'result'   => 'failed',
							'message'  => 'Missing Token'
						);
		}
	}

	/**
	 * Search Site
	 * 
	 * @param  string $cobSessionToken
	 * @param  string $userSessionToken
	 * @param  string $siteSearchString
	 * @return
	 */
	protected function searchSite($cobSessionToken, $userSessionToken,  $siteSearchString)
	{
		if(!empty($siteSearchString) ) {

			$config = array(
					'url' => $this->serviceBaseUrl.self::URL_SEARCH_SITES, 
					'parameters' => array(
						'cobSessionToken'  => $cobSessionToken, 
						'userSessionToken' => $userSessionToken,
						'siteSearchString' => $siteSearchString,
					)
				);

			$result = $this->Post($config['url'], $config['parameters']);

			return $result;
		}	
	}

	/**
	 * Get Bacnk Acct Detail Based on 90 days Transaction
	 * 
	 * @param  array $transactions
	 * @return 
	 */
	protected function getBnkAcctDetailsBasedOnTrans($transactions)
	{
		
		$NSFCnt            = 0;
		$lastDate          = "";
		$lowBalDayCnt      = 0;
		$lowBalEventCnt    = 0;
		$daysSinceNSF      = "";
		$totalPMTCnt       = 0;
		$totalMODepositCnt = 0;
		$totalPMTAmt       = 0;
		$totalMODepositAmt = 0;
 
		if( count($transactions) > 0 ) {

			foreach( $transactions as $key => $transaction ) {

				if($transaction->viewKey->containerType == 'bank') { 

					//track deposited transaction count
					//@todo , make the transaction type as global
					//this is for calculation of average PMT
					if( $transaction->transactionType == 'credit' ){
						$totalPMTCnt++;
						$totalPMTAmt += (float)$transaction->account->accountBalance->amount;
					}

					//this is for calculation of total MO Deposit
					if( $transaction->transactionType == 'debit'){
						$totalMODepositCnt++;
						$totalMODepositAmt += (float)$transaction->account->accountBalance->amount; 
					}

					//check for the current Balance in each transactions
					if( $transaction->account->accountBalance->amount < 0 ){
						$NSFCnt++;
						// $daysSinceNSF = $NSFCnt;
						//assume that the transaction are ordered in latest date
						//set the daysSinceNSF value to first
						// if( $daysSinceNSF ==  "" && isset($transaction->transactionDate) )
						// 	$daysSinceNSF = $transaction->transactionDate;
						// elseif( $daysSinceNSF ==  "" && isset($transaction->postDate) ) 
						// 	$daysSinceNSF = $transaction->postDate;
						// else
						// 	$daysSinceNSF = DB::raw("DEFAULT");	
					}

					//check if the current balance in each 
					if( $transaction->account->accountBalance->amount < 50 ) {
						$lowBalEventCnt++; 

						if( isset($transaction->transactionDate) ) 
							$transactionDate = $transaction->transactionDate; 
						else
							$transactionDate = $transaction->postDate;

						if( $lastDate != $transactionDate )
							$lowBalDayCnt++; 
					}
				} 
			}	

			//calculate Avg Monthly Deposit Amount
			$avgMonthlyDepositAmt = $this->calcAvgMonthlyDepositAmt($totalMODepositAmt, $totalMODepositCnt);
			$avgBalOnPmtDtAmt 	  = $this->calcAverageBalancePMTAmt($totalPMTAmt, $totalPMTCnt);
		
			$this->setTotalNSFCnt($NSFCnt);
			$this->setDaysSinceNSF($NSFCnt);
			// $this->setDaysSinceNSF($daysSinceNSF);
			$this->setLowBalDayCnt($lowBalDayCnt);
			$this->setLowBalEventCnt($lowBalEventCnt);
			$this->setAvgBalOnPmtDtAmt($avgBalOnPmtDtAmt);
			$this->setAvgMonthlyDepositAmt($avgMonthlyDepositAmt);

		}
	  
	}

	/**
	 * Calculate Avg Monthly Deposit
	 * 
	 * @param  [type] $totalMODepositAmt
	 * @param  [type] $totalMODepositCnt
	 * @return [type]
	 */
	public function calcAvgMonthlyDepositAmt($totalMODepositAmt, $totalMODepositCnt)
	{
		return (float)$totalMODepositAmt / (int)$totalMODepositCnt;
	}

	/**
	 * Calculate Average Balance PMT Amount
	 * @param  [type] $totalPMTAmt
	 * @param  [type] $totalPMTCnt
	 * @return [type]
	 */
	public function calcAverageBalancePMTAmt( $totalPMTAmt, $totalPMTCnt )
	{
		return (float)$totalPMTAmt / (int)$totalPMTCnt;
	}

	/**
	 * Get Average Balance On PMT Amount
	 * 
	 * @return
	 */
	public function getAvgBalOnPmtDtAmt()
	{
	 	return $this->avgBalOnPmtDtAmt;
	}

	/**
	 * Set Avg Balance On PMT Date Amount
	 * 
	 * @param float $avgBalOnPmtDtAmt
	 */
	public function setAvgBalOnPmtDtAmt($avgBalOnPmtDtAmt)
	{
		$this->avgBalOnPmtDtAmt = $avgBalOnPmtDtAmt;
	}

	/**
	 * Get Average Monthly Deposite Amount
	 * 
	 * @return float
	 */
	public function getAvgMonthlyDepositAmt()
	{
		return $this->avgMonthlyDepositAmt;
	}

	/**
	 * Set Average Monthly Deposit Amount
	 * 
	 * @param float $avgMonthlyDepositAmt
	 */
	public function setAvgMonthlyDepositAmt($avgMonthlyDepositAmt)
	{
		$this->avgMonthlyDepositAmt =  $avgMonthlyDepositAmt;
	}

	/**
	 * Set Total NSF Count
	 * 
	 * @param $totalNSFCnt
	 */
	public function setTotalNSFCnt($totalNSFCnt)
	{
		$this->totalNSFCnt = $totalNSFCnt;
	}

	/**
	 * Get Total Not Suffecient Funds
	 * 
	 * @return
	 */
	public function getTotalNSFCnt()
	{
		return $this->totalNSFCnt;		 
	}

	/**
	 * Set Days since NSF
	 * 
	 * @param string $date
	 */
	public function setDaysSinceNSF($date)
	{
		$this->daysSinceLastNSFCnt = $date;
	}

	/**
	 * Get DaysSince NSF
	 * 
	 * @return string
	 */
	public function getDaysSinceNSF()
	{
		return $this->daysSinceLastNSFCnt;
	}

	/**
	 * Set Low Balance Day Count
	 * 
	 * @param integer $lowBalDayCnt
	 */
	public function setLowBalDayCnt($lowBalDayCnt)
	{
		$this->lowBalDayCnt = $lowBalDayCnt;
	}

	/**
	 * Get Num Low Balance Days
	 * 
	 * @return
	 */
	public function getLowBalDayCnt()
	{
		return $this->lowBalDayCnt;
	}

	/**
	 * Set Low Balance Event Count
	 * 
	 * @param integer $lowBalEventCnt
	 */
	public function setLowBalEventCnt($lowBalEventCnt)
	{
		$this->lowBalEventCnt = $lowBalEventCnt;
	}

	/**
	 * Get Number Low Balance Events
	 * 
	 * @return
	 */
	public function getLowBalEventCnt()
	{
		return $this->lowBalEventCnt;
	}


	/**
	 * Save Yodlee Log into Yodlee_Access_Log
	 * 
	 * @return
	 */
	protected function saveYodleeLog( $siteId, $successfulLinkingFlag = '0', $successful90DayPullFlag = '0' )
	{
		// writeLogEvent('Saving Log Event', array('test' => 'test' ));
	
		if( !empty($siteId) ) {

			YodleeAccessLog::insertLog( array(
					'userId'                  => $this->getUserId(), 
					'loanAppNr'               => $this->getLoanAppNr(),
					'siteId'                  => $siteId, 
					'successfulLinkingFlag'   => $successfulLinkingFlag, 
					'successful90DayPullFlag' => $successful90DayPullFlag 
			    )
			);
		}
	}

	/**
	 * Update Yodlee Log
	 * 
	 * @return
	 */
	protected function updateYodleeLog( $siteId, $successfulLinkingFlag = '0', $successful90DayPullFlag = '0' )
	{
		if( !empty($siteId) ) {

			YodleeAccessLog::updateLog( array(
					'userId'                  => $this->getUserId(), 
					'loanAppNr'               => $this->getLoanAppNr(),
					'siteId'                  => $siteId, 
					'successfulLinkingFlag'   => $successfulLinkingFlag, 
					'successful90DayPullFlag' => $successful90DayPullFlag 
			    )
			);
		}
	}
  
}