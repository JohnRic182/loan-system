<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/




/**
 * Developer's Notes:
 *
 *  - Created CustomAuth Filters @see filters.php
 *
 * @note
 *  - Need to configure the permission per page
 * 
 */



/* --------------------------
 * Route Patterns
 *---------------------------*/

Route::pattern('post', '[0-9][a-z]+');


/* --------------------------
 * Route  Model
 *---------------------------*/
Route::model('user', 'User');


/* --------------------------
 * Main Page
 *---------------------------*/
Route::get('/', array('uses' => 'PartnerController@showGAExperiment'));
 
/* --------------------------
 * Secured Loan Application Steps
 *---------------------------*/

Route::group(array('before' => 'auth.check'), function()
{
	Route::get('checkcredit', array('uses' => 'UserController@showCreditCheck'));

	Route::get('updateAllZestimate', array('uses' => 'UserController@updateAllZestimate'));

	Route::post('prepareCreditCheck', array('uses' => 'PartnerController@prepareCreditCheck'));

});

Route::group(array('before' => 'auth.check|auth.loan'), function()
{ 
	/* --------------------------
	 * Loan Application
	 *---------------------------*/
	Route::get('linkBankAccount', array('uses' => 'LoanController@showBankAccountFormLink'));
	Route::get('verifyBankAccount', array('uses' => 'LoanController@showBankAccountForm'));
	Route::get('fastLink', array('before' => 'no-cache', 'uses' => 'LoanController@showFastLinkBankAccount'));
	Route::get('offer', array('as' => 'offer', 'uses' => 'LoanController@showLoanRate'));
	Route::get('incomeVerifier', array('uses' => 'LoanController@showIncomeVerification'));
	Route::get('setLoanDetails', array('uses' => 'LoanController@setLoanDetails'));
	Route::get('scoring', array('uses' => 'LoanController@scoring'));
	Route::get('checkBankAccountHistory', array('uses' => 'LoanController@checkBankAccountHistory')); 

	Route::post('loanRate', array('uses' => 'LoanController@showLoanRate'));
	Route::post('insertLoanRate', array('uses' => 'LoanController@insertLoanRate'));


	

	/* --------------------------
	 * Verification Process
	 *---------------------------*/
	Route::get('verification/steps/{result?}', array('as' => 'verification', 'uses' => 'VerificationController@showVerificationSteps'));
	Route::get('verification/contract', array('before' => 'no-cache', 'uses' => 'VerificationController@showLoanDocument'));
	Route::get('verification/funding', array('before' => 'no-cache', 'uses' => 'VerificationController@showFundingVerification'));
	Route::get('verification/bankdetails', array('uses' => 'VerificationController@showFundingVerification'));
	Route::get('verification/employment', array( 'before' => 'no-cache', 'uses' => 'VerificationController@showEmploymentVerification'));
	Route::get('verification/income', array('before' => 'no-cache','uses' => 'VerificationController@showIncomeVerification'));
	Route::get('verification/identitydocs', array('before' => 'no-cache', 'uses' => 'VerificationController@showIdentityDocumentVerification'));
	Route::get('verification/identity', array('before' => 'no-cache', 'uses' => 'VerificationController@showIdentityVerification')); // final fraud check w KBA
	Route::get('verification/payment', array('before' => 'no-cache', 'uses' => 'VerificationController@showPaymentVerification'));
	Route::get('verification/bank', array('before' => 'no-cache', 'uses' => 'VerificationController@showBankAccountVerification'));
	Route::get('verification/processBank', array('uses' => 'VerificationController@processBankAccountVerification'));
	Route::get('verification/updateLoanDocument', array('uses' => 'VerificationController@updateLoanDocument'));
	Route::get('verifyAccount', array('as' => 'verifyAccount' , 'uses' => 'VerificationController@showEmailVerificationStep'));
	Route::get('verification/success', array('uses' => 'VerificationController@showVerificationResult'));
	Route::get('verification/closeIdology/{idNumber}', array('uses'   => 'VerificationController@closeIdology'));
	
	Route::post('verification/postFundingVerification', array('uses' => 'VerificationController@postFundingVerification'));
	Route::post('verification/postEmploymentVerification', array('uses' => 'VerificationController@postEmploymentVerification')); 
	Route::post('verification/postIdentityDocumentVerification', array('uses' => 'VerificationController@postIdentityDocumentVerification')); 
	Route::post('verification/postIdentityVerification/{taskStatus}', array('uses' => 'VerificationController@postIdentityVerification')); 
	Route::post('verification/postIncomeVerification', array('uses' => 'VerificationController@postIncomeVerification')); 	
	Route::post('verification/uploadFundingCheck', array('uses' => 'VerificationController@uploadFundingCheck')); 
	Route::post('verification/uploadIdentification', array('uses' => 'VerificationController@uploadIdentification')); 
	Route::post('verification/uploadIncomeVerification', array('uses' => 'VerificationController@uploadIncomeVerification')); 	
	Route::post('verification/postPaymentVerification', array('uses' => 'VerificationController@postPaymentVerification'));
	Route::post('verification/uploadBankVerification', array('uses' => 'VerificationController@uploadBankVerification')); 	
	Route::post('verification/postBankVerification', array('uses' => 'VerificationController@postBankVerification')); 	
	Route::post('verification/idologyResult/{type?}', array('uses'   => 'VerificationController@idologyResult'));
	
});

/* --------------------------
 * Borrower Portal
 *---------------------------*/
Route::get('portal/', array('uses'   => 'BorrowerPortalController@showIndex'));
Route::get('portal/history', array('uses'   => 'BorrowerPortalController@showHistory'));
Route::get('portal/payment', array('uses'   => 'BorrowerPortalController@showPayment'));
Route::get('portal/account', array('uses'   => 'BorrowerPortalController@showAccount'));
Route::get('portal/statement', array('uses'   => 'BorrowerPortalController@showStatement'));
Route::get('portal/raterewards', array('uses'   => 'BorrowerPortalController@showRateRewards'));
Route::get('processLoanTransactions', array('uses'   => 'BorrowerPortalController@processLoanTransactions'));
Route::get('portal/admin/summary/{userId}/{loanId}', array('uses'   => 'BorrowerPortalController@showAdminIndex'));
Route::get('portal/admin/history/{userId}/{loanId}', array('uses'   => 'BorrowerPortalController@showAdminHistory'));
Route::get('portal/admin/statement/{userId}/{loanId}', array('uses'   => 'BorrowerPortalController@showAdminStatement'));
Route::get('portal/admin/payment/{userId}/{loanId}', array('uses'   => 'BorrowerPortalController@showAdminPayment'));
Route::get('portal/admin/account/{userId}/{loanId}', array('uses'   => 'BorrowerPortalController@showAdminAccount'));
Route::get('portal/admin/raterewards/{userId}/{loanId}', array('uses'   => 'BorrowerPortalController@showAdminRateRewards'));


/* --------------------------
 * Bank Account Check (Yodlee)
 *---------------------------*/
Route::post('verifyBankAccount', array('uses' => 'YodleeController@searchBank'));
Route::post('addSiteAccount', array('uses' => 'YodleeController@postAddSiteAccount'));
Route::post('getMFAResponseForSite', array('uses' => 'YodleeController@getMFAResponseForSite'));
Route::post('putMFARequestForSite', array('uses'  => 'YodleeController@putMFARequestForSite'));
Route::post('saveYodleeData', array('uses' => 'YodleeController@putYodleeData'));
Route::post('saveLoanAppPhase', array('uses' => 'YodleeController@saveLoanAppPhase'));


Route::get('validateBankAcctFlag', array('uses' => 'YodleeController@validateBankAcctFlag'));
Route::get('createYodleeAccount', array('uses' => 'YodleeController@createYodleeAccount'));
Route::get('getSiteLoginForm/{siteId}', array('uses' => 'YodleeController@getSiteLoginForm'));
Route::get('cobLogin', array('uses' => 'YodleeController@cobLogin'));
Route::get('getItemSummariesForSite/{memSiteAccId}', array('uses' => 'YodleeController@getItemSummariesForSite'));
Route::get('getItemSummariesWithoutItemData', array('uses' => 'YodleeController@getItemSummariesWithoutItemData'));
Route::get('removeSiteAccount/{memSiteAccId}', array('uses' => 'YodleeController@removeSiteAccount'));
Route::get('syncLocalData', array('uses' => 'YodleeController@syncLocalData'));

Route::get('generateFastLinkUrl', array('uses' => 'YodleeController@generateFastLinkUrl'));
Route::get('fastlinkCallBack', array('uses' => 'YodleeController@fastlinkCallBack'));
Route::post('fastlinkCallBack', array('uses' => 'YodleeController@fastlinkCallBack'));


/* --------------------------
 * Guest 
 *---------------------------*/
Route::group( array('before' => 'guest.check'), function(){
	Route::get('login', array('uses'  => 'UserController@showLogin'));
});


/* --------------------------
 * Page Controller
 *---------------------------*/
Route::get('consent/sms', array('uses' => 'PageController@getSMSAuth'));
Route::get('consent/creditPull', array('uses' => 'PageController@getCreditPullAuth'));
Route::get('consent/eSignature', array('uses' => 'PageController@getEsignatureAuth'));
Route::get('consent/funding', array('uses' => 'PageController@getFundingAuth'));
Route::get('contact', array('uses' => 'PageController@ContactUs'));
Route::get('ratesTerms/{stateCode?}', array('uses' => 'PageController@ratesTerms'));
Route::get('faqs', array('uses' => 'PageController@faq'));
Route::get('privacyPolicy', array('uses' => 'PageController@privacyPolicy'));
Route::get('terms', array('uses' => 'PageController@terms'));
Route::get('howItWorks', array('uses' => 'PageController@howItWorks'));
Route::get('inThePress', array('uses' => 'PageController@pressRelease'));

/* --------------------------
 * User Controller
 *---------------------------*/
// Route::get('apply', array('uses' => 'UserController@showApply'));
Route::get('register', array('uses' => 'UserController@showRegister'));
Route::get('ssnCheck/{ssn}', array('uses' => 'UserController@isUniqueSSN'));
Route::get('emailCheck/{email}', array('uses' => 'UserController@isUniqueEmail'));
Route::get('logout', array('uses'  => 'UserController@doLogout'));
Route::get('forgotPassword/{requestStatus?}', array('uses' => 'UserController@showForgotPassword'));
Route::get('forgotUser/{requestStatus?}', array('uses' => 'UserController@showForgotUser'));
Route::get('reset-password/{key}/', array('uses' => 'UserController@showResetPassword'));
Route::get('reset-user/{key}/', array('uses' => 'UserController@showResetUsername'));
Route::get('verify', array('as' => 'verifyApplicant', 'uses' => 'UserController@verifyApplicant'));

Route::post('login', array('uses' => 'UserController@doLogin'));
// Route::post('apply', array('uses' => 'UserController@showApply'));
Route::post('register', array('uses' => 'UserController@showRegister'));
Route::post('postRegister', array('uses' => 'UserController@postRegister'));
Route::post('forgotPassword', array('uses' => 'UserController@sendPassword'));
Route::post('forgotUsername', array('uses' => 'UserController@sendUsername'));
Route::post('reset-password', array('uses' => 'UserController@resetPassword'));
Route::post('reset-username', array('uses' => 'UserController@resetUsername'));
Route::post('postCreditCheck', array('uses' => 'LoanController@postCreditCheck'));
Route::post('checkDuplicate', array('uses' => 'UserController@usernameCheck'));
Route::post('validateEmail', array('uses' => 'UserController@validateEmailAddress'));
Route::post('securityQuestion', array('uses' => 'UserController@saveSecurityQuestion'));

/* --------------------------
 * NLS API Call 
 *---------------------------*/
Route::get('/Authenticate', array('uses'   => 'LoanController@Authenticate'));
Route::get('/CreateContact', array('uses'  => 'LoanController@CreateContact'));
Route::get('/createLoan', array('uses'    => 'LoanController@createLoan'));
Route::get('/saveNLS', array('uses'    => 'LoanController@saveNLS'));
Route::post('/createLoan', array('uses'    => 'LoanController@createLoan'));
Route::get('/creditPull', array('uses' => 'LoanController@creditPull'));
Route::get('/parseXML/{uid}', array('uses'    => 'LoanController@parseXML'));
Route::get('/verifyContact', array('uses'    => 'LoanController@verifyContact'));
Route::get('/exclusionCheck/{type}', array('uses'    => 'LoanController@execCreditExclusion'));
Route::get('/testexecCreditExclusion/{type}', array('uses'    => 'LoanController@execCreditExclusion'));


Route::get('/decline/{debug?}', array('as' => 'decline',  'uses' => 'LoanController@disqualification'));
Route::get('/disqualification/{debug?}', array('as' => 'disqualification',  'uses' => 'LoanController@disqualification'));
Route::post('/updateLoanSessionData', array('uses' => 'LoanController@updateLoanSessionData'));

/* --------------------------
 * Idology API Call 
 *---------------------------*/
Route::get('/idologyRun/{type?}', array('uses'   => 'LoanController@idologyRun'));
Route::post('/idologySubmitAnswers', array('uses'   => 'LoanController@idologySubmitAnswers'));

/* --------------------------
 * Admin Panel
 *---------------------------*/

Route::group(array( 'prefix' => 'admin', 'before' => 'guest.admin' ), function(){
	Route::get('/' , array('uses' => 'AdminController@showIndex' ));
	Route::post('login', array('uses' => 'AdminController@doLogIn'));
});

Route::group(array( 'prefix' => 'admin', 'before' => 'auth.admin' ), function(){

	Route::get('funding' , array('uses' => 'AdminController@showFunding' ));
	Route::get('report' , array( 'as' => 'adminReport', 'uses' => 'AdminController@showReport' ));
	Route::get('exclusion' , array('uses' => 'AdminController@showExclusionReport' ));
	Route::get('partner' , array('uses' => 'AdminController@showPartner' ));
	Route::get('/borrower' , array('uses' => 'AdminController@showBorrowers' ));
	Route::get('/borrowerList/{searchKey?}' , array('uses' => 'AdminController@showBorrowerList' ));
	Route::post('updateFunding', array('uses' => 'AdminController@updateFunding'));
	Route::post('savePartnerStatus', array('uses' => 'AdminController@savePartnerStatus'));

	//For Reopen Loan Mechanism
	Route::get('reopen' , array('uses' => 'AdminController@showReopenLoans' ));
	Route::get('/getLoanDetails/{searchKey?}' , array('uses' => 'AdminController@getLoanDetails' ));
	Route::post('doReopenProcess', array('uses' => 'AdminController@doReopenProcess'));

	//ajax for report
	Route::get('loadExclusion', array('uses' => 'AdminController@loadExclusionTable'));
	Route::get('loadMktgFunnel', array('uses' => 'AdminController@loadMarketingFunnelTable'));
	Route::get('loadReport', array('uses' => 'AdminController@loadReportTable'));
	Route::get('loadVerificationActivity', array('uses' => 'AdminController@loadVerificationActivityTable'));
	Route::get('loadSpvLoan', array('uses' => 'AdminController@loadSpvLoanTable'));
	Route::get('loadSpvLoanTransfer', array('uses' => 'AdminController@loadSpvLoanTransferTable'));
	Route::get('loadFundedLoanReport', array('uses' => 'AdminController@loadFundedLoanReport'));
	
	//for exporting reports
	Route::get('exportExclusionReport/{startDate?}/{endDate?}', array('uses' => 'AdminController@exportExclusionReport'));
	Route::get('exportMarketingFunnelReport/{startDate?}/{endDate?}', array('uses' => 'AdminController@exportMarketingFunnelReport'));
	Route::get('exportReport/{startDate?}/{endDate?}', array('uses' => 'AdminController@exportReport'));
	Route::get('exportVerificationActivity/{startDate?}/{endDate?}', array('uses' => 'AdminController@exportVerificationActivity'));
	Route::get('exportLoanReport/{startDate?}/{endDate?}', array('uses' => 'AdminController@exportLoanReport'));
	Route::get('exportLoanTransferReport/{startDate?}/{endDate?}', array('uses' => 'AdminController@exportLoanTransferReport'));
	Route::get('exportFundedLoanReport/{startDate?}/{endDate?}', array('uses' => 'AdminController@exportFundedLoanReport'));

});
 
 
/* --------------------------
 * Ratereward Program 
 *---------------------------*/
Route::group(array('prefix' => 'raterewardLoan' ), function(){	

	Route::get('/' , array('uses' => 'RaterewardLoanController@index' ));
	Route::get('rewards/{loanNumber}/{cifno}/{cifNumber}' , array('uses' => 'RaterewardLoanController@rewards' ));
	Route::get('testTU' , array('uses' => 'RaterewardLoanController@testTU' ));
	Route::get('process' , array('uses' => 'RaterewardLoanController@process' ));
	Route::get('test' , array('uses' => 'RaterewardLoanController@tuRRTest' ));
	Route::post('testTUprocess/{loanNumber}/{userId}' , array('uses' => 'RaterewardLoanController@getRRTest' ));
	Route::get('rewardsTUPull/{loanNumber}/{userId}' , array('uses' => 'RaterewardLoanController@rrTUPull' ));
	Route::get('pullBankTransactions/{userId?}/{loanAppNr?}' , array('uses' => 'RaterewardLoanController@pullBankTransactions' ));

});

/* --------------------------
 * Testing 
 *---------------------------*/

Route::group(array('before' => 'debug.Mode'), function(){
   
	Route::get('showCreditPullInfo', array('uses' => 'LoanController@showCreditPullInfo'));
  
	//Idology Test Harness
	Route::get('idologyTest', array('uses'   => 'LoanController@idologyTest'));
	Route::post('idologyRunTest', array('uses'   => 'LoanController@idologyRunTest'));

	//User Testing 
	Route::get('pullTest', array('uses'    => 'LoanController@pullTest'));
	Route::get('getBankAcctSummaryView', array('uses'    => 'RaterewardLoanController@getBankAcctSummaryView'));

	//Selenium Automation
	Route::get('testVerifyEmail', array('uses'    => 'SeleniumController@testVerifyEmail'));	

});

/* --------------------------
 * Cron Jobs
 *---------------------------*/
Route::group(array('prefix' => 'cron' ), function(){	

	Route::get('processVerificationTasks', array('uses' => 'NLSController@processVerificationTasks'));
	Route::get('processFinalApprovalLoans', array('uses' => 'NLSController@processFinalApprovalLoans'));
	Route::get('rejectExpiredLoan/{limit?}', array('uses' => 'NLSController@rejectExpiredLoanApplication'));
	Route::get('updateNLSRef', array('uses' => 'NLSController@updateNLSRef'));
	Route::get('updateUserRefNo', array('uses' => 'NLSController@updateUserRefNo'));

	/*Borrower Portal*/
	Route::get('populateTxnHistory', array('uses' => 'BorrowerPortalController@populateTransactionHistory'));

});

Route::get('cron/updateNLSRef', array('uses' => 'NLSController@updateNLSRef'));
Route::get('cron/updateUserRefNo', array('uses' => 'NLSController@updateUserRefNo'));

Route::get('getApplicantIp', array('uses' => 'UserController@getApplicantIp'));

/* --------------------------
 * Partners
 *---------------------------*/
Route::group(array('prefix' => 'api/v1'), function(){
	//LENDING TREE
	Route::post('lt', array('uses' => 'PartnerController@showLendingTree'));
	Route::get('ltHarness', array('uses' => 'PartnerController@showLendingTreeHarness'));
	Route::post('ltHarness', array('uses' => 'PartnerController@lendingTreeHarness'));

	//DOT818
	Route::get('partner', array('uses' => 'PartnerController@apply'));
	Route::post('partner', array('uses' => 'PartnerController@apply'));
	Route::post('partnerTest', array('uses' => 'PartnerController@partnersAPITestHarness'));
	Route::get('partnerHarness', array('uses' => 'PartnerController@showPartnerTest'));
	Route::post('testEndpoint', array('uses' => 'PartnerController@postTestEndPoint'));

});

/* --------------------------
 * Partner Co-Brand
 *---------------------------*/
Route::group(array('prefix' => 'LP'), function(){
  	
  	//Landing Page A/B Testing
  	Route::get('{landingPage}', array('uses' => 'UserController@index'));
	//Lending Tree, Mint
	Route::get('{partnerName}/{landingPage}', array('uses' => 'PartnerController@showIndex'));
	//Dot818
	Route::get('SFL/lp1/{userId}/{loanAppNr}/{leadId}', array( 'uses' => 'PartnerController@showComplexPartnerPage') );
	//Market Leads
	Route::get('marketleads/lp1/{userId}/{loanAppNr}/{leadId}', array( 'uses' => 'PartnerController@showComplexPartnerPage' ));
	//Personalloans
	Route::get('personalloans/lp1/{userId}/{loanAppNr}/{leadId}', array( 'uses' => 'PartnerController@showComplexPartnerPage' ));
	
	/* --------------------------
	 * Homepage A/B Google Experiment
	 *---------------------------*/
	Route::get('/v1', function(){
		return Redirect::to('/');
	});
});


/* --------------------------
* SalesForce
*---------------------------*/
Route::group(array('prefix' => 'salesforce/api', 'before' => '' ), function(){

	Route::get('v1/cron', 'Api\ApiController@cron');
	Route::resource('v1', 'Api\ApiController');

	//Verification
	Route::post('v1/verification/approve','Api\VerificationController@approve');
	Route::post('v1/verification/decline','Api\VerificationController@decline');
	Route::post('v1/verification/assign','Api\VerificationController@assign');
	Route::post('v1/verification/stipulate','Api\VerificationController@stipulate');
	//Loan
	Route::post('v1/loan/approve', 'Api\LoanController@approve');
	Route::post('v1/loan/decline','Api\LoanController@decline');
	Route::post('v1/loan/reopen','Api\LoanController@reopen');
	Route::post('v1/loan/recalc','Api\LoanController@recalc');
	Route::post('v1/loan/bankstatus','Api\LoanController@bankstatus');
	Route::post('v1/loan/task','Api\LoanController@newTask');
	Route::post('v1/loan/amount/update','Api\LoanController@updateLoanAmt');

	Route::get('harness', 'Api\ApiController@showAPIHarness');

});

/* --------------------------
* Hello Sign
*---------------------------*/
Route::post('hellosign/documentCallback', array('uses' => 'HelloSignController@documentCallback'));
Route::get('hellosign/documentCallback', array('uses' => 'HelloSignController@documentCallback'));


/* --------------------------
 * Custom Error
 *---------------------------*/
App::error(function( $exception, $code )
{	
	switch ($code) {
		case 404:
			return App::make('PageController')->callAction('show404', array() );
			break;
		
		// default:
		// 	return App::make('PageController')->callAction('showDefaultError', array('exception' => $exception ) );	
		// 	break;
	}

});

/* --------------------------
 * Testing Error
 *---------------------------*/

Route::get('iovationCheckIp', function(){

	// $where = array('User_Id' => 6566, 'Loan_App_Nr' => 10906 );
	// $ARI = ApplicantRiskInfo::where($where)->first();
	// _pre($ARI->Risk_Segment_Nr);

	echo 'requested ips';
	pre($_SERVER);
	echo 'Real IP: ';
	echo $ip = getRealClientIpAddress();

});

// Display all SQL executed in Eloquent for debugging
// Event::listen('illuminate.query', function($query){ echo $query; });
