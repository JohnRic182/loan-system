<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	if( ! Request::secure())
    {
        #return Redirect::secure(Request::path());
    }
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		return Redirect::guest('login');
	}
});


/**
 * Authentication Check 
 */
Route::filter('auth.admin', function() {

	if( Session::get('adminLogged') != TRUE ){ 

  		if (Request::ajax()) {
			return Response::make('Unauthorized', 401);
		}

		return Redirect::to('admin');
  	}
});

/**
 * Admin Guest Login
 */
Route::filter('guest.admin', function()
{
	if ( Session::get('adminLogged') == TRUE ) return Redirect::to('admin/report');
});

/**
 * Authentication Check 
 */
Route::filter('auth.check', function() {

	if( !isLoggedIn() ) {
		
		if (Request::ajax()) {
			return Response::make('Unauthorized', 401);
		}

		return Redirect::to('login');
	}
});

/**
 * Loan Authentication
 */
Route::filter('auth.loan', function() {

	if( getLoanAppNr() == "" ) {
		
		if (Request::ajax()) {
			return Response::make('Unauthorized', 401);
		}

		return Redirect::to('creditCheck');
	}
});

/**
 * Basic Authentication
 */
Route::filter('auth.basic', function()
{
	return Auth::basic();
});


/*
|--------------------------------------------------------------------------
| Build Filter (Dev/Prod)
|--------------------------------------------------------------------------
|
| If Debug Mode is off then the test pages/harness should not be accessible
| Redirect them to homepage
|
*/

Route::filter('debug.Mode', function()
{
	if (Config::get('system.Ascend.DebugMode') == 'false') return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

Route::filter('guest.check', function()
{
	if (isLoggedIn()) return Redirect::to('/');
});


/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
|--------------------------------------------------------------------------
| No Cache and No-store filter 
|--------------------------------------------------------------------------
| Use this to disable caching on all / specsific groups or pages
*/

Route::filter('no-cache', function()
{
	header("Cache-Control: no-store");
  	header("Pragma: no-cache");
  	header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // A date in the past

});
