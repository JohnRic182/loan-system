<?php
/**
 * Common Functions
 *
 * @since  version 0.1
 * @author Global Fusion <ascend@globalfusion.net>
 *
 * Developers Notes:
 * 
 *  LAST UPDATE: 01-26-2014
 *  - Added getStateArray, pre, showQuery
 *  - Added showNotification, showYodleeError
 *  - Added isLoggedIn
 *  - Added logout Method
 */

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        MISCELLANEOUS                                       //
////////////////////////////////////////////////////////////////////////////////////////////////
if( !function_exists('getRealClientIpAddress'))
{
  function getRealClientIpAddress() {
      $ip_keys = array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR');
      foreach ($ip_keys as $key) {
          if (array_key_exists($key, $_SERVER) === true) {
              foreach (explode(',', $_SERVER[$key]) as $ip) {
                  // trim for safety measures
                  $ip = trim($ip);
                  // attempt to validate IP
                  if (validate_ip($ip)) {
                      return $ip;
                  }
              }
          }
      }
      return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : false;
  }
}

/**
 * Ensures an ip address is both a valid IP and does not fall within
 * a private network range.
 */
if( !function_exists('validate_ip'))
{
  function validate_ip($ip)
  {
      if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 | FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) === false) {
          return false;
      }
      return true;
  }
}


/**
 * sextract the number or any integer from a given string
 *
 * @param string $countryCode
 */
if( !function_exists('extractNumbers'))
{ 
  function extractNumbers($str) {
  
    $int = intval(preg_replace('/[^0-9]+/', '', $str), 10);

    return $int;

  }
}


/**
 * Get States Array
 *
 * @param string $countryCode
 */
if( !function_exists('getStatesArray'))
{ 
	function getStatesArray( $countryCode = 'US' ) {
  
    $states   = State::all();
    $stateArr =  array();

    foreach($states as $key => $state )
      $statesArr[$state->State_Cd] = $state->State_Name;

    return $statesArr;
	}
}


/**
 * Get Offered States Array
 */
if( !function_exists('getStateOffered'))
{ 
  function getStateOffered() {
    
    $data = array();

    $stateCodes = LoanLocation::select('Loan_Prod_State_Cd')->distinct()->get()->toArray();
    foreach ($stateCodes as $key => $value)
      $codes[] = $value; 

    $statesOffered = State::whereIn('State_Cd', $codes)->get();

    foreach ($statesOffered as $key => $value)
      $data[$value->State_Cd] = $value->State_Name;

    return $data;
  }
}


/**
 * Get Offered States Array
 *
 * @param  string $stateCode
 */
if( !function_exists('stateConfigurations'))
{ 
  function stateConfigurations($stateCode) {

    $osc = getStateOffered();

    // if states is not in the offered state then default to CA
    $sc = ( array_key_exists($stateCode, $osc) )? $stateCode : 'CA';

    $conf = StateCfg::getStateConfig($sc);

    foreach ($conf as $key => $value)
      $data[$value->Loan_Cfg_Name] = $value->Loan_Cfg_Val;
  
    return $data;
  }
}

/**
 * get the State via IP
 */
if( !function_exists('getStateCodeViaIp'))
{ 
  function getStateCodeViaIp() {

    // When an IP is not given the $_SERVER["REMOTE_ADDR"] is used.
    $geoip = GeoIP::getLocation();
    $stateCode = ($geoip['default'])? 'CA' : $geoip['state'] ;

    return $stateCode;
  }
}

/**
 * Validate Non Numeric String 
 *
 * @param  string $str
 */
if( !function_exists('cleanNonNumeric') )
{
  function cleanNonNumeric( $str = '' ) {
    
    if( !empty($str) )
      $str = preg_replace("/[^0-9,.]/", "", $str);

    return $str;
  }
}

/**
 * Clean and Encode String
 *
 * @param  string $input
 */
if( !function_exists('cleanHttpParam'))
{
  function cleanHttpParam($input) {
    return str_replace('+',' ',str_replace('%7E', '~', rawurlencode($input)));
  }
}


/**
 * load typekit fonr based on the server
 *
 * @deprecated
 * @param string  environemnt 
 */
if( !function_exists('getFontScript'))
{
  function getFontScript() 
  {

    $hostEnvi = $_SERVER['SERVER_NAME'];
    $hostEnvi = str_replace('www.', '', $hostEnvi);
    $hostEnvi = str_replace('WWW.', '', $hostEnvi);

    $host  = array(
      'raterewardsdev'          => 'tqw4yig', // jp local
      'rwards.loc'              => 'nwd7hvx', // seon / janz local
      'raterewards.gfdev.net'   => 'kjc2fqq',
      'staging.ascendloan.com'  => 'qoq7xgd',
      '54.201.101.96'           => 'qoq7xgd',
      'ascendloan.com'          => 'fqu7kzw',
    );

    if( isset($host[$hostEnvi]) )
      return '<script src="//use.typekit.net/'. $host[$hostEnvi] .'.js"></script><script>try{Typekit.load();}catch(e){}</script>';
    else
      return '<script src="//use.typekit.net/fqu7kzw.js"></script><script>try{Typekit.load();}catch(e){}</script>';

  }
}

/**
 * Generate Random Password
 * 
 * @param  integer $length
 * @return
 */
if( !function_exists('generatePassword'))
{ 
  function generatePassword( $length = 8 ) {


    $randomInt      = rand(1, 9);
    $randomChar     = generateRandomChar(1);
    $randomSpecChar = generateRandomChar(1, 'special');
 
    $chars    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr( str_shuffle( $chars ), 0, $length );

    $password = $password . $randomInt . $randomChar. $randomSpecChar;

    
    return $password;
  }
}

/**
 * Generate User Name Based on IP Address
 * 
 * @param  string $emailAddress
 * @return string
 */
if( !function_exists('generateUserName') ) 
{
  function generateUserName( $emailAddress = '')
  {
    if( !empty($emailAddress) ) {

       $emailAddress  = explode('@', $emailAddress);
       
       if( isset($emailAddress[0]) ) {

          $User = new User();
          //User Name Validation
        $isUserNameExist = true; 

        while($isUserNameExist == true ) {

          $randomInt      = rand(1, 9);
          $randomChar     = generateRandomChar(1);
          $randomSpecChar = generateRandomChar(1, 'special');

            $userName = $emailAddress[0] . $randomInt . strtoupper($randomChar) . $randomSpecChar ;
          
          $ODSUserName = $User->isRecordExist( array( 'User_Name' => $userName ) );

          if( $ODSUserName == 0 ){
            $isUserNameExist = false;
            break;
          }
        }

          return $userName;
        }
    }
  }
}
  
/**
 * Generate Random Character
 *
 * @param  integer $length
 * @param  string  $type
 * @return string
 */
if( !function_exists('generateRandomChar'))
{
  function generateRandomChar( $length = 8, $type = 'standard') 
  {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    if( $type == 'special' )
      $chars = '!@#$%^&*()_-=+;:,.?';
    
    return substr( str_shuffle( $chars ), 0, $length );
  }
}

/**
 * Generate Random key
 * 
 * @param  integer $length
 * @return
 */
if( !function_exists('generateRandomKey'))
{ 
  function generateRandomKey( $length = 8 ) {
     return generateRandomChar( $length );
  }
}

/**
 * Get Months
 *
 * @param string $countryCode
 */
if( !function_exists('getMonthsArray'))
{ 
  function getMonthsArray()
  { 

      $monthArr = array();
      $month = strtotime(date('Y').'-'.date('m').'-'.date('j').' - 1 months');
      $end = strtotime(date('Y').'-'.date('m').'-'.date('j').' + 13 months');
      while($month < $end){
          $selected = (date('F', $month)==date('F'))? ' selected' :'';

          $monthArr[abs( date('m', $month) )] = date('F', $month);
 
          $month = strtotime("+1 month", $month);
      }

      ksort($monthArr);

      return $monthArr;
  }
}

/**
 * Get Months
 *
 * @param string $countryCode
 */
if( !function_exists('getDaysArray'))
{ 
  function getDaysArray()
  { 
      $days = array();
      for ($i=1; $i < 32; $i++) { 
          $days[$i] = $i;
      }

      return $days;
  }
}

/**
 * Get Housing Situation List
 *
 * @return  array
 */
if( !function_exists('getHousingSituation'))
{ 
  function getHousingSituation()
  { 
      $result  = HousingSituation::all();

      $housingSit = array();
      
      //Add Select one
      $housingSit['defaultVal'] = "Select One";

      foreach ($result as $key => $value) {
        $housingSit[$value->Housing_Sit_Id] = $value->Housing_Sit_Desc;
      }  

      return $housingSit;
  }
}
 

/**
 * Get Delinquency Period List
 *
 * @return  array
 */
if( !function_exists('getDelinquencyPeriod'))
{ 
  function getDelinquencyPeriod()
  { 
      $result  = DelinquencyPeriod::all();

      $delinquencyPeriod = array();
      
      foreach ($result as $key => $value) {
        $delinquencyPeriod[$value->Delinquency_Period_Id] = $value->Delinquency_Period_Desc;
      }

      return $delinquencyPeriod;
  }
}

/**
 * Get All Loan Group Information
 *
 * @return  array
 */
if( !function_exists('getloanGroup'))
{ 
  function getloanGroup()
  { 
      $result  = LoanGroup::all();

      $loanGroup = array();
      
      foreach ($result as $key => $value) {
        $id = trim($value->Loan_Grp_Id);
        $loanGroup[$id] = $value->Loan_Grp_Name;
      }

      return $loanGroup;
  }
}

/**
 * Get All Employee Status
 *
 * @return  array
 */
if( !function_exists('getEmployeeStatus'))
{ 
  function getEmployeeStatus()
  { 
      $result  = EmployeeStatus::all();

      $empStatus = array();
      
      foreach ($result as $key => $value) {
        $empStatus[$value->Employment_Status_Id] = $value->Employment_Status_Desc;
      }

      return $empStatus;
  }
}

/**
 * Get the id of eplioyement status via one word description
 *
 * @return  array
 */
if( !function_exists('getEmployeeStatusIdViaDesc'))
{ 
  function getEmployeeStatusIdViaDesc($desc)
  { 
    
      $index = strtoupper($desc);

      $EmployeeStatus = EmployeeStatus::all();

      foreach ($EmployeeStatus as $key => $value) {     
          $esId = strtoupper(str_replace('-', '', $value->Employment_Status_Desc));
          $esId = explode(' ', $esId);
          $maxIndex = max(array_keys($esId));
          $es[$esId[$maxIndex]] = $value->Employment_Status_Id;
      }

      return $es[$index]; 

  }
}

/**
 * Finds a property for a specified address.
 * The content returned contains the address for the property or properties as well as the Zillow Property ID (ZPID) and current Zestimate.
 *
 * @return mixed
 */
if ( !function_exists('getRentZestimate'))
{
	function getRentZestimate($address, $citystatezip)
	{
		$zillow_id = getenv('ZILLOW_WSI');

		$url = 'http://www.zillow.com/webservice/GetSearchResults.htm?zws-id=' . $zillow_id . '&address=' . urlencode($address) . '&citystatezip=' . urlencode($citystatezip) . '&rentzestimate=1';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);

		//wait for 6 seconds
		sleep(6);

		$data = simplexml_load_string($result);

		$zestimate = (isset($data->response)) ? (string)$data->response->results->result->rentzestimate->amount : '';
		$log_message = (string)$data->message->text;
		$code = (int)$data->message->code;

		//Write to transaction log for error codes
		if ($code > 0) {
			writeLogEvent('Rent Zestimate Response',
				array(
					'User_Id'     => Session::get('uid'),
					'Address'	  => $address,
					'Zip'		  => $citystatezip,
					'Error_Code'  => $code,
					'Message'     => $result
				),
				'warning'
			);
		}

		return array(
			'zestimate' => $zestimate,
			'message_text' => $log_message,
			'message_code' => $code,
			'xml' => $result
		);
	}
}

/**
 * Get All Loan Purpose for select / dropdown menu
 *
 * @return  array
 */
if( !function_exists('getLoanPurposeList'))
{ 
  function getLoanPurposeList()
  { 
      $result  = LoanPurpose::all();

      $loanPur = array();
      
      foreach ($result as $key => $value) {
        $loanPur[trim($value->Loan_Purpose_Id)] = $value->Loan_Purpose_Desc;
      }

      return $loanPur;
  }
}

/**
 * List of Years
 *
 * @param array
 */
if( !function_exists('getYearArray'))
{ 
  function getYearArray()
  {
	  return array_combine(range(date('Y', strtotime('-18 year')), 1910), range(date('Y', strtotime('-18 year')), 1910));
  }
}

/**
 * get the age from date of birth to present date
 */
if( !function_exists('getAgeFromDOB'))
{ 
  function getAgeFromDOB($birthday)
  { 
     return date_create($birthday)->diff(date_create('today'))->y;
  }
}

/**
 * Print with Format
 */
if( !function_exists('pre'))
{
  function pre($param)
  {
      echo '<pre>';
      print_r($param);
      echo  '</pre>';
  }
}
 
/**
 * Print with Format and exit
 */
if( !function_exists('_pre'))
{
  function _pre($param)
  {
      echo '<pre>';
      print_r($param);
      echo  '</pre>';
      exit();
  }
}
 

/**
 * Show Last Query
 *
 * @return pre formatted query
 */
if( !function_exists('showQuery'))
{
  function showQuery()
  {
    $queries    = DB::getQueryLog();
    $last_query = end($queries);
    pre($last_query);
  }
}

/**
 * Show Application Notification
 *
 * @param  string $messageCode
 * @return string
 */
if( !function_exists('showNotification'))
{
  function showNotification( $messageCode )
  {
    return Lang::get("messages.$messageCode");
  }
}

/**
 * Show Invalid Parameter Notification
 *
 * @param  string $messageCode
 * @return string
 */
if( !function_exists('showInvalidParameterError'))
{
  function showInvalidParameterError()
  {
   return array(
        'result'  => 'failed', 
        'message' => Lang::get('messages.requestParameterInvalid')
    );
  }
}

/**
 * Get Months
 *
 * @param array()
 */
if( !function_exists('countDays'))
{ 
  function countDays($startDate, $endDate)
  { 

    $startTimeStamp = strtotime($startDate);
    $endTimeStamp = strtotime($endDate);
    $timeDiff = abs($endTimeStamp - $startTimeStamp);
    $numberDays = $timeDiff/86400;  // 86400 seconds in one day
    // and you might want to convert to integer
    $numberDays = intval($numberDays);

    return $numberDays;

  }
}

/**
 * PMT - Present Value of Payments
 */
if( !function_exists('pmt'))
{ 
  function pmt($apr, $loanlength, $loanamount)
  {
      $apr = floatval( $apr );
      $loanlength = floatval( $loanlength );
      $loanamount = floatval( $loanamount );

      $apr /= 1200;
      return ($apr * -$loanamount * pow((1 + $apr), $loanlength) / (1 - pow((1 + $apr), $loanlength)));
  }
}

/**
 * Format Date into Local Date Time
 */
if( !function_exists('formatDateByLocale') )
{
  function formatDateByLocale($dateString , $timezone = 'America/Los_Angeles')
  { 
    //dateString = 2011-11-11
    $date = new DateTime($dateString, new DateTimeZone($timezone));
    return  $date->format('Y-m-d:sP') . "\n";
  }
}

/**
 * Custom Explode String
 * @param  string     $string
 * @param  char       $delimiter
 * @param  integer    $segment
 * @return array/string    
 */
if( !function_exists('explodeStr')){
  function explodeStr($string, $delimiter, $segment = NULL){ 
      
    $ret = $result = explode($delimiter, $string);

    if($segment != NULL)
      $ret = $result[$segment];

    return $ret;
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////
//                                        SESSION                                             //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Is User Logged In 
 *
 * @return  bool $flag;
 */
if( !function_exists('isLoggedIn'))
{
  function isLoggedIn()
  {
    $flag = false; 
    if( Session::has('logged') && Session::has('uid') ){
      $flag = true;  
    }
    return $flag;
  }
}

/**
 * Get User Session ID
 */
if( !function_exists('getUserSessionID'))
{
  function getUserSessionID(){
    return Session::get('uid');
  }
}


/**
 * Logout User  - Removing all Session Items
 */
if( !function_exists('logout'))
{
  function logout()
  {
    Session::flush();
  }
}

/**
 * Get Loan Session Data
 */
if( !function_exists('getLoanSessionData'))
{
  function getLoanSessionData()
  {
    return Session::get('loan');
  }
}

/**
 * Set Loan Session Data
 *
 * @param array $data
 */
if( !function_exists('setLoanSessionData'))
{
  function setLoanSessionData($data = array())
  {
    if( count($data) > 0 ) {
      foreach( $data as $key => $value )
        Session::put("loan.$key", trim($value) );
    }
  }
}

/**
 * Get Loan Application Number
 */
if( !function_exists('getLoanAppNr'))
{
  function getLoanAppNr()
  {
    return Session::get('loanAppNr');
  }
}

/**
 * Set Loan Application Number
 *
 * @param array $data
 */
if( !function_exists('setLoanAppNr'))
{
  function setLoanAppNr($data = 0)
  {
    if($data != 0){
      Session::put('loanAppNr', $data);
    }
  }
}

/**
 * Set Consent Session Data
 *
 * @param array $data
 */
if( !function_exists('setConsentSessionData'))
{
  function setConsentSessionData($data = array())
  {
    if( count($data) > 0 ) {
      foreach( $data as $key => $value )
        Session::put("consent.$key", $value);
    }
  }
}


/**
 * Get Consent Session Data
 *
 * @param array $data
 */
if( !function_exists('getConsentSessionData'))
{
  function getConsentSessionData(){
    return Session::get('consent');
  }
}

/**
 * Application steps session
 *
 * @param string $value
 */
if( !function_exists('setActiveStepSession'))
{
  function setActiveStepSession( $value = '' ){
    return Session::put('step', $value );
  }
}

/**
 * Get Active Step Session 
 */
if( !function_exists('getActiveStepSession'))
{
  function getActiveStepSession(){
    return Session::get('step');
  }
}


/**
 * set the hompage relative url
 */
if( !function_exists('setHompageRelativeUrl'))
{
  function setHompageRelativeUrl(){

    $relativeUrl = $_SERVER['REQUEST_URI'];
    return Session::put('HPURL', $relativeUrl );
  
  }
}


/**
 * Get Active Step Session 
 */
if( !function_exists('getHompageRelativeUrl'))
{
  function getHompageRelativeUrl(){
    return (Session::has('HPURL')) ? Session::get('HPURL') : '/';
  }
}

/**
 * Set Applicant Data into Session
 *
 * @param  integer $userId
 */
if( !function_exists('setApplicantData') )
{
  function setApplicantData( $userId )
  {
    $appltnFields = array('First_Name', 'Last_Name', 'Middle_Name', 'Applicant_Verified_Flag');
    $userFields   = array('Email_Id','User_Name', 'User_Id', 'Remember_Token_Txt');
    $where        = array('User_Id' => $userId ); 
   
    //get Applicant information
    $applicantObj = Applicant::getData($appltnFields, $where , true); 
    $userObj      = User::getData($userFields, $where , true );

    $firstname  = isset($applicantObj->First_Name) ? $applicantObj->First_Name : "" ;
    $lastName   = isset($applicantObj->Last_Name) ? $applicantObj->Last_Name : "" ;
    $middleName = isset($applicantObj->Middle_Name) ? $applicantObj->Middle_Name : "" ;
    $verified   = isset($applicantObj->Applicant_Verified_Flag) ? $applicantObj->Applicant_Verified_Flag : "" ;

    $applicant = array(
        'email'      => $userObj->Email_Id, 
        'userName'   => $userObj->User_Name, 
        'userId'     => $userObj->User_Id,
        'token'      => $userObj->Remember_Token_Txt,
        'firstName'  => $firstname,
        'lastName'   => $lastName,
        'middleName' => $middleName, 
        'verified'   => $verified
    );

    Session::put('applicant', $applicant);
  }
}

/**
 * Get Applicant Data from Session
 *
 * @return array
 */
if( !function_exists('getApplicantData') )
{
  function getApplicantData()
  {
     return Session::get('applicant');
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////
//                                        NLS                                                 //
////////////////////////////////////////////////////////////////////////////////////////////////



/**
 * XML Remove Invalid Characters
 */
if( !function_exists('cleanXML'))
{
  function cleanXML($value){

      $ret    = '';
      $current= '';
      $length = strlen($value);

      for ($i=0; $i < $length; $i++) {
          $current = ord($value{$i});
          if (($current == 0x9) ||
              ($current == 0xA) ||
              ($current == 0xD) ||
              (($current >= 0x20) && ($current <= 0xD7FF)) ||
              (($current >= 0xE000) && ($current <= 0xFFFD)) ||
              (($current >= 0x10000) && ($current <= 0x10FFFF)))
          {
            $ret .= chr($current);
          }
          else {
            $ret .= " ";
          }
      }

    return $ret;
  }
}

/**
 * Create NLS Task
 *
 * @updates 
 *       - removing the task template code
 *       - added task status as required for ID Verification step 
 * @param  string $taskName
 * @return
 */
if( !function_exists('createNLSTask'))
{
  function createNLSTask( $taskName = "", $comments = array(), $notes = "", $taskStatus = '' )
  {
    if( !empty($taskName) && !empty(getUserSessionID()) && !empty(getLoanAppNr()) ) {

      //get Applicant Name 
      $applicantInfo = Session::get('applicant'); 

      $firstName = '';
      $lastName  = '';

      if( isset($applicantInfo['firstName']) ) 
        $firstName = $applicantInfo['firstName']; 

      if( isset($applicantInfo['lastName']) ) 
        $lastName = $applicantInfo['lastName']; 

      $applName =  $firstName  . ' ' . $lastName;

      $task = array(
        'UpdateFlag'       => 0, 
        'TaskTemplateName' => $taskName,
        // 'StatusCodeName'   => 'OPEN',
        'PriorityCodeName' => 'NORMAL',
        'NLSType'          => 'Loan', 
        'NLSRefNumber'     => getLoanAppNr(),
        'CreatorUID'       => 0, 
        'OwnerUID'         => 0,
        'OwnerUserName'    => 'Wayde',  
        'StartDate'        => date('m/d/Y'), 
        'DueDate'          => date('m/d/Y'),
        'Subject'          => $taskName . ' ' . $applName, 
        'Notes'        => $notes,
      );

      if($taskStatus == 'APPROVED' || $taskStatus == 'DENIED')
        $task['CompletionDate'] = date('m/d/Y');

      if(isset($taskStatus) && $taskStatus != '')
        $task['StatusCodeName'] = $taskStatus;      
      
      try{
        $NLS = new NLS();
        return $NLS->addTask($task, $comments); 
      }catch(Exception $e) {
        return $e->getMessage();
      }
    }
  }
}

/**
 * Get Task Data by Task Number
 *
 * @param  integer $taskNo
 */
if( !function_exists('getNLSTaskTemplateName'))
{
  function getNLSTaskTemplateName( $taskNo = 0 ){
      $NLSTaskTemplate = new NLSTaskTemplate();
      $tasks = $NLSTaskTemplate->where('task_template_no', '=' , $taskNo )
               ->first();
      return $tasks;
  }
}

/**
 * Update Not Started Tasks
 * 
 * @param  integer $loanAppNr
 * @param  string $status
 * @param  string $statusCode
 * @param  string $newStatusCode
 * @return
 */
if( !function_exists('updateNotStartedTasks'))
{
  function updateNotStartedTasks( $loanAppNr = 0 , $status = '', $statusCode = '', $newStatusCode = '' )
  {
    try{

      $NLS = new NLS();

      $isRow = true;

      //Get the Applicant Information
      $fields     = array( 'First_Name', 'Last_Name' ); 
      $whereParam = array( 'User_Id' => $status['userId'] );

      $applicant  = Applicant::getData($fields, $whereParam, $isRow );

      //Format Applicant Name
      $applName =  $applicant['First_Name']  . ' ' . $applicant['Last_Name'];

      $NLSTask = new NLSTask();

      if( $newStatusCode != '' ){
        $tasks = $NLSTask->getTasksByRefNo( $status['NLSRefno']);
      }else{
        $tasks = $NLSTask->getTasksByRefNoAndStatusCode( $status['NLSRefno'], $statusCode );
        $newStatusCode = 'LOAN_APP_CLOSED';
      }
    
      //@todo - we should be calling updateTask instead of addTask
      if( count($tasks) > 0 ){ 
        foreach ( $tasks as $key => $data ) { 
          $task = array(
            'UpdateFlag'       => 1,  
            'TaskRefno'        => $data['task_refno'],
            'TaskTemplateName' => $data['task_template_name'],
            'PriorityCodeName' => 'NORMAL',
            'StatusCodeName'   => $newStatusCode,
            'NLSRefNumber'     => $data['NLS_refno'],
            'Subject'          => $data['task_template_name'] . ' ' . $applName,
            'CompletionDate'   => date('m/d/Y')
          );  
          $NLS->addTask( $task ); 
        }
      }
    }catch( Exception $e ) {
      writeLogEvent('updateNotStartedTasks', 
        array( 
         'Message'     => $e->getMessage()
        ),
        'warning'
      ); 
    } 
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        YODLEE                                              //
////////////////////////////////////////////////////////////////////////////////////////////////
 /**
  * Show Yodlee Error Notification
  *
  * @param  int $errorCode
  * @return string
  */
if( !function_exists('showYodleeError'))
{
  function showYodleeError( $errorCodeNr )
  {
      if( $errorCodeNr != "") {
        $errorCodeTxt = Config::get("yodlee.".$errorCodeNr); 
        return Lang::get('yodlee.'.$errorCodeTxt);
      }
  }
}


/**
 * Get Top Banks 
 *
 * @deprecated sprint 17
 */
if( !function_exists('getTopBanks') )
{
  function getTopBanks() {

    $fields  = array('Site_Id'
                    , 'Site_Name'
                    , 'Popularity_Idx_Val'
                    , 'Image_Url_Txt'
                  );
    return FinancialOrganization::getTopBanks($fields);
  }
}


////////////////////////////////////////////////////////////////////////////////////////////////
//                                        VERIFICATION                                        //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Clean Up Verification Steps
 *
 * @param  string $string
 */
if( !function_exists('generateVerfnStepsUrl'))
{
    function generateVerfnStepsUrl($string)
    { 

      if( $string != 'Loan Contract' && $string != 'Identity Documents' && $string != 'Identity Verification' && $string != 'Bank Account Details') {

        $urlSplit = explode(' ' , $string);

        if( isset($urlSplit[0]) ) {
            return strtolower($urlSplit[0]);
        }    
      } else if( $string == 'Identity Documents'){
        $string  = 'identitydocs';
      } else if( $string == 'Identity Verification'){
        $string  = 'identity';
      } else if( $string == 'Bank Account Details'){
        $string  = 'bankdetails';
      } else{
        $string  = 'contract';
      }

      return strtolower($string);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        APPLICANT                                           //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Get Applicant By User Id
 *
 * @param  integer $userId
 * @param  array   $fields
 * @param  bool    $isRow
 */
if( !function_exists('getApplicant') ) 
{
  function getApplicant($userId, $fields = array(), $isRow = false ) 
  { 
    return Applicant::getData($fields, array('User_Id' => $userId )  , $isRow);
  }
}

/**
 * Get Applicant Address Information By Id
 *
 * @param  integer $userId
 */
if( !function_exists('getApplicantAddress') ) 
{
  function getApplicantAddress($userId, $fields = array() ) 
  {
    return ApplicantAddress::getData($fields, array('User_Id' => $userId), true );
  }
} 

/**
 * Get Applicant Financial Informatio Address
 *
 * @param  integer $userId
 */
if( !function_exists('getApplicantFinancialInfo') ) 
{
  function getApplicantFinancialInfo($userId, $fields = array(), $isRow = false ) 
  {
    return ApplicantFinancialInfo::getData($fields, array('User_Id' => $userId), $isRow );
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        APPLICATION PHASE                                   //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Set Applicant Pre Application Phase Information
 *
 * @param  integer  $userId
 * @param  array    $data
 */
if( !function_exists('setApplPreAppPhaseInfo') ) 
{
  function setApplPreAppPhaseInfo( $userId, $data = array() )
  {
    
    if( !empty($data) && count($data) > 0 ) {

      $PreApplicationInfo = new PreApplicationInfo();

      $PreApplicationInfo->Applicant_User_Id  = $userId ;
      $PreApplicationInfo->Loan_Amt           = $data['loanAmt'] ;
      $PreApplicationInfo->Loan_Prod_Id       = $data['loanProductId'] ;
      $PreApplicationInfo->Term_In_Months_Cnt = $data['loanTerm'];
      $PreApplicationInfo->Loan_Purpose_Id    = $data['loanPurposeId'] ;
      $PreApplicationInfo->Created_By_User_Id = $userId ;
      $PreApplicationInfo->Create_Dt          = date('Y-m-d') ;
      $PreApplicationInfo->save();

    }
  }
}

/**
 * Get Applicant Pre Application Phase Information
 *
 * @param  integer  $userId
 */
if( !function_exists('getApplPreAppPhaseInfo') ) 
{
  function getApplPreAppPhaseInfo( $userId )
  {
    return PreApplicationInfo::find($userId);
  }
}

/**
 * Set Loan Application Phase
 *
 * @param  integer $userId
 * @param  integer $loanAppNr
 * @param  integer $loanAppPhaseNr
 */
if( !function_exists('setLoanAppPhase') ) 
{  
  function setLoanAppPhase( $userId = NULL, $loanAppNr = NULL, $loanAppPhaseNr = NULL, $subPhaseCode = '', $active = true ) 
  {
    //default Sub Phase Values
    $subPhase['Loan_Sub_Phase_Id']   = "";
    $subPhase['Loan_Sub_Phase_Desc'] = "";

    //Get Sub App Phase
    $subPhaseCfg = Config::get("subphase.$subPhaseCode");

    if( count($subPhaseCfg) > 0 ) {
      $subPhase['Loan_Sub_Phase_Id']   = $subPhaseCfg['Loan_Sub_Phase_Id'];
      $subPhase['Loan_Sub_Phase_Desc'] = $subPhaseCfg['Loan_Sub_Phase_Desc'];
    }    

    if( !empty($subPhaseCfg) ) {
      //Get Loan Application Phase
      $loanAppPhase = Config::get("loan.LOAN_APP_PHASE$loanAppPhaseNr"); 

      $loanAppDetail = new LoanApplicationDetail();
      $loanAppDetail->setLoanAppPhase( $userId,  $loanAppNr, 
          array(
            'loanAppPhaseNr'     => $loanAppPhaseNr, 
            'loanAppPhaseDesc'   => $loanAppPhase['Loan_App_Phase_Desc'],
            'subPhaseNr'         => $subPhase['Loan_Sub_Phase_Id'],
            'subPhaseStatusDesc' => $subPhase['Loan_Sub_Phase_Desc']
          )
      );
    }

    if( $active == true )
      setActiveLoanAppPhase( $userId , $loanAppNr, $loanAppPhaseNr );
 
  }
}

/**
 * Set Active Loan Application Phase
 *
 * @param integer $userId
 * @param integer $loanAppNr
 * @param integer $loanAppPhaseNr
 */
if( !function_exists('setActiveLoanAppPhase') ) 
{  
  function setActiveLoanAppPhase( $userId = NULL, $loanAppNr = NULL, $loanAppPhaseNr = NULL ) 
  {
      LoanApplication::setActiveLoanAppPhase( $userId, $loanAppNr, $loanAppPhaseNr );
  }
}

/**
 * Get Loan Application Phase
 *
 * @param  integer $loanAppPhaseNr
 */
if( !function_exists('getLoanAppPhase') ) 
{  
  function getLoanAppPhase( $loanAppPhaseNr ) 
  {
    return Config::get("loan.LOAN_APP_PHASE$loanAppPhaseNr"); 
  }
}

/**
 * Update Loan Application Status Code
 *
 * @param  integer $loanAppNr
 * @param  string $statusCode 
 * @return void
 */ 
if( !function_exists('updateLoanStatus') ) 
{  
  function updateLoanStatus( $loanAppNr, $statusCode ) 
  {
     if( !empty( $loanAppNr )  ){
        LoanDetail::where('Loan_Id', $loanAppNr )
                 ->update( array('Loan_Status_Desc' => $statusCode  ) 
        );
     }
  }
}

/**
 * Validate Application
 */
if( !function_exists('validateExistingLoanAppStat') ) 
{
  function validateExistingLoanAppStat( $isVerification = FALSE ) {

    $redirectTo    = '';
    $latestLoanApp = getLoanSessionData();

    if( isLoggedIn() && isset( $latestLoanApp['statusDesc'] ) ) {
      //If the current Application is Rejected or for 
      //Verification, redirect them
      if( $latestLoanApp['statusDesc'] == 'APPLICATION_REJECTED' ){
        $redirectTo = 'disqualification';
        return $redirectTo;
      }elseif( $latestLoanApp['statusDesc'] == 'VERIFICATION' ){
        //Do not Redirect the page 
        //when it is already on Verificaton page
        if( $isVerification == FALSE ){
          $redirectTo = 'verification/steps'; 
          return $redirectTo;
        }
      }
    } 
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        LOAN                                                //
////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Get All Loan Product
 *
 * @param  integer $userId
 * @return array
 */
if( !function_exists('getLoanProduct') ) 
{
  function getLoanProductList() 
  {
    return LoanProduct::all();
  }
}


/**
 * Get Loan Product by ID
 *
 * @param  integer $productId
 * @return array
 */
if( !function_exists('getLoanProductById') ) 
{
  function getLoanProductById($productId) 
  {
    return LoanProduct::find($productId);
  }
}
 

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        LOGGING                                             //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Write Transactional Logs
 *
 * @param string  $logTitle
 * @param array   $option
 */
if( !function_exists('writeLogEvent') )
{
	function writeLogEvent($logTitle = '', $option = array(), $logType = 'warning', $logFile = '')
	{
      if( empty( $logFile ) ) 
		    $logFile = Config::get('log.application');

		  Log::useDailyFiles(storage_path().'/logs/' . $logFile );
		  Log::$logType( $logTitle,  $option);
	}
}

	
////////////////////////////////////////////////////////////////////////////////////////////////
//                                        REPORT                                              //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * get table colums 
 *
 * @param  string
 * @return array
 */
if( !function_exists('getTableColumns') ) 
{  
  function getTableColumns( $tableName ){

    $col = array();

    $data = DB::select("select column_name from information_schema.columns where table_name = '$tableName'");

    foreach ($data as $key => $value)
      $col[] = $value->column_name;

    return $col;

  }
}


////////////////////////////////////////////////////////////////////////////////////////////////
//                                        PARTNERS                                            //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * get partner flags status
 *
 * @param  string
 * @return array
 */
if( !function_exists('getPartnerStatus') ) 
{  
  function getPartnerStatus( $partnerId ){

    $partner = Partner::find($partnerId);
    
    return ($partner) ? $partner->Actv_Flag : 0 ;

  }
}

/**
 * update flags
 *
 * @param  string
 * @return array
 */
if( !function_exists('updatePartnerStatus') ) 
{  
  function updatePartnerStatus( $partnerId, $status = 0 ){

    $partner = Partner::find($partnerId);
  
    if($partner){
      $partner->Actv_Flag = $status;
      $partner->save();
    }
  }
}

/**
 * Track Partners Ids
 * 
 * @param  string $partnerId
 * @return
 */
if( !function_exists('trackPartnerIds') ) 
{ 
  function trackPartnerIds( $partnerId ){
    // destroy session to refresh session values
    Session::forget('partner');

    //get url parameters
    $CID = trim(Input::get('CID'));
    $UID = "";

    //Assign User Id
    if( Input::has('UID') )
      $UID = trim(Input::get('UID'));

    //Assign Lead_Id for Amone Partner Tracking
    if( Input::has('partner_sid') )
      $UID = trim(Input::get('partner_sid'));

    if( Input::has('SID') )
      $UID = trim(Input::get('SID'));

  
    //partner ID 
    $PID = $partnerId;

    if( !empty($CID) && !empty($PID) ){

      //Generate Cookie ID
      $userIp   = Request::getClientIp();
      $cookieId = md5($userIp.$CID.$PID.$UID);
 
      //save cookie on response
      Cookie::queue('Partner_LT', $cookieId, 86400);

      //save PID CID to session to ber tracked
      Session::put('partner.CID', $CID );
      Session::put('partner.PID', $PID );
      Session::put('partner.UID', $UID );

      //save cookie details if not found
      $UserAcctCookie = UserAcctCookie::firstOrNew( 
        ['Cookie_Id' => $cookieId, 
        'Cookie_Expired_Flag' => 0, 
        'User_Registered_Flag' => 0]
      );

      if( $UserAcctCookie->exists != 1 ){

        $UserAcctCookie->Cookie_Create_Dttm = date('Y-m-d');
        $UserAcctCookie->Partner_Id = $PID;
        $UserAcctCookie->Campaign_Id = $CID; 
        $UserAcctCookie->Tracking_Nr = $UID;
        $UserAcctCookie->Created_By_User_Id = 0;
        $UserAcctCookie->save();      

      }else{
        $UserAcctCookie->updateDate($cookieId);
      }

    }else{
      //get cookie id from agent
      $cookieId   = Cookie::get('Partner_LT');

      if($cookieId){
        
        //update if user has and expired cookie
        $UserAcctCookie = new UserAcctCookie();
        $UserAcctCookie->updateExpiration($cookieId);
        
        //search if the cookie is tracked
        $UserAcctCookie = UserAcctCookie::firstOrNew( 
          ['Cookie_Id' => $cookieId, 
          'Cookie_Expired_Flag' => 0, 
          'User_Registered_Flag' => 0 ]
        );
        
        if($UserAcctCookie->exists == 1){

          //save to session           
          Session::put('partner.CID', $UserAcctCookie->Campaign_Id );
          Session::put('partner.PID', $UserAcctCookie->Partner_Id);
          Session::put('partner.UID', $UserAcctCookie->Tracking_Nr);

          // update expiry
          $UserAcctCookie->Cookie_Create_Dttm = date('Y-m-d');
          $UserAcctCookie->save();

        }
      }
    }
  }
}

/**
 * Set Parter In Session
 *
 * @param array $data
 */
if( !function_exists('setPartnerSession'))
{
  function setPartnerSession($data = array()){
    if( count($data) > 0 ) {
      foreach( $data as $key => $value )
        Session::put("partners.$key", $value);
    }
  }
}


/**
 * Set Parter In Session
 *
 * @param array $data
 */
if( !function_exists('getPartnerSession'))
{
  function getPartnerSession(){
      return Session::get('partners');
  }
}

/**
 * [determini if the user is from LT express funnel]
 * @return boolean [description]
 */
if( !function_exists('isLTExpressFunnelUser') )
{
  function isLTExpressFunnelUser(){    

    $userid = getUserSessionID(); // get the current user
    $data = PartnerUserAcct::where('Ascend_User_Id', '=', $userid)->first();    
    
    return ($data)? $data->LT_Express_Funnel_Flag : 0;
    
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        CMS                                                 //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * get CMS content via Alias
 *
 * @param array $data
 */
if( !function_exists('getCmsContent'))
{
  function getCmsContent($alias){
      $data = JoomlaCmsContent::getData( array('introtext'), array( 'alias' => $alias ), true);
      return $data->introtext;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
//                                        SALESFORCE                                                 //
////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Import SF Account Object
 *
 * @param  integer $userId 
 * @param  integer $loanAppNr
 */
if( !function_exists('importSFAccountObj') )
{
  function importSFAccountObj( $userId, $loanAppNr )
  {
 
    $isImported    = LoanDetail::isImportedInSalesforce( $userId, $loanAppNr );
    $SalesforceMod = new SalesforceMod(); 

    try{

      //Do update if the sf data is already created
      if( empty( Session::get('sfAccountId') ) ) {

        $account = $SalesforceMod->createAccount($userId, $loanAppNr );
          
        if( is_array($account) && isset( $account[0]->success) ) {
          if( $account[0]->success == 1 ) {
            Session::set('sfAccountId', $account[0]->id); 
            $sfAcctObjId = $account[0]->id;
          }
        }

      } else{  
        $sfAcctObjId = Session::get('sfAccountId');
        $SalesforceMod->updateAccount($userId, $loanAppNr, $sfAccountId ); 
      }
       
    }catch(Exception $e) {

      writeLogEvent('SF Account Create Object', 
        array(
          'User_Id' => $userId,
          'Loan_Id' => $loanAppNr, 
          'Message' => $e->getMessage()
        ),
        'warning'
      );
    }
  }
}

/**
 * Import SF Loan Object
 *
 * @param  integer $userId
 * @param  integer $loanAppNr
 * @return array
 */
if( !function_exists('importSFLoanObj') )
{
  function importSFLoanObj( $userId, $loanAppNr )
  {
    $result = array();

    //Check for Salesforce Flag
    $isImported = LoanDetail::isImportedInSalesforce( $userId, $loanAppNr );

    //Don't do import if it is already Imported
    if( $isImported->SalesforceFlag == 'N' || $isImported->SalesforceFlag == NULL  ) {

      try{
 
        $SalesforceMod = new SalesforceMod(); 
        $sfAccountId   = ""; 

        //Check if the SFAccountId already exist in Session
        if( !empty(Session::get('sfAccountId')) ) {
          $sfAccountId = Session::get('sfAccountId');
        }else{
          //try to find the account id from the salesforce
          //assumes that the applicant session has been destroyed
          $sfAccountId = $SalesforceMod->getAccountId($userId);
        }

        //If Account is not found, try to create new Account
        if( $sfAccountId == "" ) { 
          $account = $SalesforceMod->createAccount($userId, $loanAppNr );

          $result['account'] = $account;  

          if( is_array($account) && isset( $account[0]->success) ) {
            if( $account[0]->success == 1 ) {
              $sfAccountId = $account[0]->id;
              Session::set('sfAccountId', $account[0]->id);  
            }
          } 
        }

        $loanAppSFId = ""; 

        if( !empty(Session::get('loanAppSFId')) )
            $loanAppSFId = Session::get('loanAppSFId');

        if( $sfAccountId != "" ) {
          //Create Salesforce Loan Object
          $loan = $SalesforceMod->importSFLoanObj(    
                      $userId, 
                      $loanAppNr,
                      $sfAccountId, 
                      $loanAppSFId
                    );

          //Store the Loan Application SF Id
          if( is_array($loan) && isset( $loan[0]->success) ) {
            if( $loan[0]->success == 1 )
              Session::set('loanAppSFId', $loan[0]->id); 
          } 

          $result['loan'] = $loan;

          //Update Loan Detail Salesforce Flag
          $result['importFlag'] = LoanDetail::updateSFImportFlag( $userId, $loanAppNr );

           writeLogEvent( 'Creating Salesforce Object',
            array(
              'User_Id' => $userId, 
              'Loan_App_Nr' => $loanAppNr, 
              'Message' => $result
            ),
            'warning'
          );
        }
        
      }catch(Exception $e){

        writeLogEvent( 'Creating Salesforce Object',
          array(
            'User_Id' => $userId, 
            'Loan_App_Nr' => $loanAppNr, 
            'Message' => $e->getMessage()
          ),
          'warning'
        );
      }

      return $result;
    }
  }
}

/**
 * Update Salesforce Loan Status
 *
 * @param  string $loanAppSFId
 * @param  string $status
 */
if( !function_exists('updateSFLoanStatus') )
{
  function updateSFLoanStatus( $loanAppSFId, $status )
  {
    if( !empty($loanAppSFId) ) {

      $loan  = new stdclass(); 
      $loan->Id = $loanAppSFId; 
      $loan->Loan_Status_Desc__c = $status; 

      //Update Account Object
      $account = Salesforce::update(
          array($obj['Account']), 'Account'
      );
    }
  }
}
