<?php

/**
 * Partner Helper
 *
 * @version  1.0
 * @author   Janzell Juril
 */

class PartnerHelper {
	
	public function __construct()
	{

	}
	
	/**
	 * Get partner flags status
	 *
	 * @param  string
	 * @return array
	 */
	public static function getPartnerStatus( $partnerId )
	{
	    $partner = Partner::find($partnerId);
	    return ($partner) ? $partner->Actv_Flag : 0 ;
	}

	/**
	 * Update Partner Flag
	 *
	 * @param  string  $partnerId
	 * @param  integer $status
	 * @return array
	 */
	public static function updatePartnerStatus( $partnerId, $status = 0 )
	{
	    $partner = Partner::find($partnerId);
 
	    if( $partner ){
	      $partner->Actv_Flag = $status;
	      $partner->save();
	    }
	}

	/**
	 * Track Partners Ids
	 * 
	 * @param  string $partnerId
	 * @return
	 */
	public static function trackPartnerIds( $partnerId )
	{
	    // destroy session to refresh session values
	    Session::forget('partner');

	    //get url parameters
	    $CID = trim(Input::get('CID'));
	    $UID = "";

	    //Assign User Id
	    if( Input::has('UID') )
	      $UID = trim(Input::get('UID'));

	    //Assign Lead_Id for Amone Partner Tracking
	    if( Input::has('partner_sid') )
	      $UID = trim(Input::get('partner_sid'));

	    if( Input::has('SID') )
	      $UID = trim(Input::get('SID'));
 
	    //partner ID 
	    $PID = $partnerId;

	    if( !empty($CID) && !empty($PID) ){

	      //Generate Cookie ID
	      $userIp   = Request::getClientIp();
	      $cookieId = md5($userIp.$CID.$PID.$UID);
	 
	      //save cookie on response
	      Cookie::queue('Partner_LT', $cookieId, 86400);

	      //save PID CID to session to ber tracked
	      Session::put('partner.CID', $CID );
	      Session::put('partner.PID', $PID );
	      Session::put('partner.UID', $UID );

	      //save cookie details if not found
	      $UserAcctCookie = UserAcctCookie::firstOrNew( 
	        ['Cookie_Id' => $cookieId, 
	        'Cookie_Expired_Flag' => 0, 
	        'User_Registered_Flag' => 0]
	      );

	      if( $UserAcctCookie->exists != 1 ){

	        $UserAcctCookie->Cookie_Create_Dttm = date('Y-m-d');
	        $UserAcctCookie->Partner_Id = $PID;
	        $UserAcctCookie->Campaign_Id = $CID; 
	        $UserAcctCookie->Tracking_Nr = $UID;
	        $UserAcctCookie->Created_By_User_Id = 0;
	        $UserAcctCookie->save();      

	      }else{
	        $UserAcctCookie->updateDate($cookieId);
	      }
	    }else{
	      //get cookie id from agent
	      $cookieId   = Cookie::get('Partner_LT');

	      if($cookieId){
	        
	        //update if user has and expired cookie
	        $UserAcctCookie = new UserAcctCookie();
	        $UserAcctCookie->updateExpiration($cookieId);
	        
	        //search if the cookie is tracked
	        $UserAcctCookie = UserAcctCookie::firstOrNew( 
	          ['Cookie_Id' => $cookieId, 
	          'Cookie_Expired_Flag' => 0, 
	          'User_Registered_Flag' => 0 ]
	        );
	        
	        if($UserAcctCookie->exists == 1){

	          //save to session           
	          Session::put('partner.CID', $UserAcctCookie->Campaign_Id );
	          Session::put('partner.PID', $UserAcctCookie->Partner_Id);
	          Session::put('partner.UID', $UserAcctCookie->Tracking_Nr);

	          // update expiry
	          $UserAcctCookie->Cookie_Create_Dttm = date('Y-m-d');
	          $UserAcctCookie->save();

	        }
	      }
	    }
	}

	/**
	 * Set Parter In Session
	 *
	 * @param array $data
	 */
	public static function setPartnerSession($data = array())
	{
	    if( count($data) > 0 ) {
	      foreach( $data as $key => $value )
	        Session::put("partners.$key", $value);
	    }
	}


	/**
	 * Set Parter In Session
	 *
	 * @param array $data
	 */
	public static function getPartnerSession()
	{
	    return Session::get('partners');
	}

	/**
	 * Determine if the user is from LT express funnel
	 * 
	 * @return
	 */
	public static function isLTExpressFunnelUser()
	{    
	    $userid = getUserSessionID(); // get the current user
	    $data = PartnerUserAcct::where('Ascend_User_Id', '=', $userid)->first();
	    return ($data)? $data->LT_Express_Funnel_Flag : 0;
	}
}