<?php

/**
 * NLS Helper
 *
 * @version  1.0
 * @author   Janzell Juril
 */

class NLSHelper{


	public function __construct()
	{

	}


	public static function test()
	{
		echo 'This is an NLS Helper';
	}

	/**
	 * Update NLS Loan Status
	 * 
	 * @param  integer $userId
	 * @param  integer $loanAppPhaseNr
	 * @param  string $status
	 * @return
	 */
	public static function updateNLSLoanStatus( $userId, $loanAppNr, $status )
	{
		 
		//Initialize NLS
		$NLS = new NLS();

		$loanDetail = LoanDetail::where('Borrower_User_Id', '=', $userId)
								->where('Loan_Id','=',$loanAppNr)
								->first();

		if( count($loanDetail) > 0 ) {
			//Get Loan Template Name
			$loanTemplate  = LoanTemplate::getLoanTemplateName($loanDetail->Loan_Template_Id);
			//Get Loan Group Name
			$loanGroup     = LoanGroup::getLoanGroupName($loanDetail->Loan_Grp_Id );
			//Get Loan Portfolio Name
			$loanPortfolio = LoanPortfolio::getLoanPortfolioName($loanDetail->Loan_Portfolio_Id);	

			$whereParam = array('User_Id' => $userId);	

			$applicant = new Applicant();
			$applicant_fields = array(
				'First_Name',
				'Middle_Name',
				'Last_Name',
				'Birth_Dt',
				'Social_Security_Nr'
			);
			
			$applicantData = $applicant->getData($applicant_fields,$whereParam, true );

			$fields = array(
				'nlsLoanTemplate'  => $loanTemplate,
				'nlsLoanGroup'     => $loanGroup,
				'nlsLoanPortfolio' => $loanPortfolio,
				'contactId'        => $userId,
				'loanId'           => $loanAppNr,
				'firstName'        => $applicantData->First_Name,
				'lastName'         => $applicantData->Last_Name,
				'loanStatusCode'   => $status
			);
			
			$NLS->nlsUpdateLoanStatus($fields);
		}
	}
}